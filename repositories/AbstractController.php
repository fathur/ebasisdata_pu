<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 19/08/18
 * Time: 08.20
 */

namespace Repositories;


use Model\Eloquent\Dinas;
use Model\Eloquent\Menu;
use Model\Eloquent\User;
use Model\View\UserProvinsi;
use Philo\Blade\Blade;

class AbstractController extends \CI_Controller
{

    /**
     * @var Blade
     */
    protected $blade;

    protected $webHeader;

    protected $menu;

    protected $user;

    protected $dinas;

    public function __construct()
    {
        parent::__construct();

        $views = VIEWPATH;
        $cache = APPPATH . '/cache';

        $this->blade = new Blade($views, $cache);

//        dd($this->session->userdata());

        if (is_null($this->session->userdata('iddinas'))) {
            redirect('/');
        }


        if ((int)$this->session->userdata('idprivilage') == 1) {

            $this->user = User::find($this->session->userdata('iduser'));
            $this->menu = Menu::with(['children'])->top($this->user->privilege->id)->get();
        } elseif (in_array((int)$this->session->userdata('idprivilage'), [2, 3])) {
            $this->user = Dinas::with(['provinsi', 'privilege'])
                ->find($this->session->userdata('iddinas'));

            $this->dinas = $this->user;
            $this->menu = Menu::with(['children'])
                ->top($this->user->privilege->id)
                ->get();

        }


        $this->webHeader = $this->webHeader();

    }


    /**
     * @param $idPrivilege
     * @param $idDinas
     * @return array
     */
    private function webHeader()
    {

        switch ((int)$this->user->privilege->id) {
            case 1:
                $wilayah = 'Pusat';
                $nama = $this->user->nama_dinas;
                $logo = $this->user->logo_pusat;
                break;

            case 2:
                $wilayah = $this->dinas->provinsi->name;
                $nama = $this->dinas->nama_dinas;

                $logo = $this->dinas->provinsi ? $this->dinas->provinsi->logo_prov : '';
                break;
            case 3:
                $wilayah = $this->dinas->kabupatenKota->name;
                $nama = $this->dinas->nama_dinas;

                $logo = $this->dinas->kabupatenKota ? $this->dinas->kabupatenKota->logo_kabkot : '';
                break;
        }

        $avatar = isset($logo) ? site_url("assets/global/img/logo/{$logo}") : "holder.js/35x35?text=logo";

        return [
            'wilayah' => $wilayah,
            'nama' => $nama,
            'logo' => $logo,
            'avatar' => $avatar
        ];
    }

}