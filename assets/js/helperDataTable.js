function deleteDtRow(object) {
    let $this = $(object);
    let action = $this.data('action');
    let table = $this.data('table');

    bootbox.confirm({
        message: "Anda yakin ingin menghapus data ini?",
        buttons: {
            confirm: {
                label: 'Ya',
                className: 'btn-success'
            },
            cancel: {
                label: 'Tidak',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            // Jika ya
            if (result) {
                $.ajax({
                    type: "POST",
                    url: action,
                    success: function (data) {
                        // reload datatables\
                        $('#' + table).DataTable().ajax.reload(null, false);
                    },
                });
            }
        }
    });
}