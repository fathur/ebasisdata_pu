// SCRIPT INI HARUS DITARUH DI HALAMAN

var style = {
   "fillColor": "#66FFCC",
   "fillOpacity": 0.8,
   "stroke": true,
   "color": "#03f",
   "weight": 1,
 };

 var map = L.map('mapindonesia').setView([-3.721933, 114.793895], 5);

 L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
   attribution: '&copy; <a href="http://osm.org/copyright"></a> contributors'
 }).addTo(map);

 $.ajax({
 dataType: "json",
 //memanggil data.php yang tadi dibuat
 url: "<?php if ($g1==1){ echo BASE_URL("main/petaina");} elseif ($g2==1){ echo BASE_URL("main/petaina22");} elseif ($g3==1){ echo BASE_URL("main/petaina3");} elseif ($g4==1){ echo BASE_URL("main/petaina4");}  ?>",
 success: function(datasql) {
   indonesialayer(datasql);
   }
 });

 function indonesialayer(datasql)
 {
   $.ajax({
   dataType: "json",
   //memanggil provinsi.json yang tadi di download
   url: "<?php echo BASE_URL('/application/views/html/provinsi.json');?>",
   success: function(data) {
       //provinsi.json disimpan dalam variable data
       //tampilkan provinsi.json ke dalam mapindonesia
       //bindPopup digunakan untuk menampilkan info nama provinsi ketika layer provinsi di klik
       L.geoJSON(data,{
         onEachFeature:onEachFeature,
         style:style
       }).bindPopup(function (layer) {
           return layer.feature.properties.PROVINSI;
       }).addTo(map);

       function onEachFeature(feature, layer) {

         for(var i=0;i<datasql.provinsi.length;i++)
         {
           if(datasql.provinsi[i].ID==feature.properties.ID2013)
           {
             jumlah=parseInt(datasql.provinsi[i].JUMLAHPENDUDUK);
             if(jumlah<1000)
             {
               layer.setStyle({fillColor:"#CCCCCC"});
             }
             if(jumlah>1000&&jumlah<=3000)
             {
               layer.setStyle({fillColor:"#FFCCCC"});
             }
             else if(jumlah>3000&&jumlah<=6000)
             {
               layer.setStyle({fillColor:"#FF9999"});
             }
             else if(jumlah>6000&&jumlah<=9000)
             {
               layer.setStyle({fillColor:"#FF6666"});
             }
             else if(jumlah>9000&&jumlah<=12000)
             {
               layer.setStyle({fillColor:"#FF3333"});
             }
             else if(jumlah>12000)
             {
               layer.setStyle({fillColor:"#FF0000"});
             }

           }
         }
       }
     }
   });
 }
