var UIDatepaginator = function () {

    return {

        //main function to initiate the module
        init: function () {

            //sample #1
            $('#datepaginator_sample_1').datepaginator();

            //sample #2
            $('#datepaginator_sample_2').datepaginator({
                size: "large"
            });

            //sample #3
            $('#datepaginator_sample_3').datepaginator({
                size: "small"
            });

            //sample #3
             
			 
            $('#datepaginator_sample_4').datepaginator({
            
            selectedDate:  document.getElementById("ftanggal").value , 
            //selectedDate: '2013-01-01',
			  selectedDateFormat:  'YYYY-MM-DD', 
                onSelectedDateChanged: function(event, date) { 
    				document.getElementById("ftanggal").value=moment(date).format("YYYY-MM-DD");
    				 document.getElementById("myForm").submit();
                   //alert("tg: " + moment(date).format("Do, MMM YYYY")); 
                }
            });
            
        } // end init

    };

}();