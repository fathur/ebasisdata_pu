/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {

    // private functions & variables

    var myFunc = function(text) {
        alert(text);
    }

    // public functions
    return {

        //main function
        init: function () {
            //initialize here something.
        },

        //some helper function
        doSomeStuff: function () {
            myFunc();
        }

    };

    var handleInputMasks = function () {
        $.extend($.inputmask.defaults, {
            'autounmask': true
        });

        $("#k221").inputmask('999.999.999.999,99', {
            numericInput: true
        }); //123456  =>  ___.__1.234,56

        $("#k231").inputmask('999.999.999.999,99', {
            numericInput: true
        }); //123456  =>  ___.__1.234,56
    }

    return {
        //main function to initiate the module
        init: function () {
            handleInputMasks();
        }
    };

}();

/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();
