<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<script> 
    $("#provinsi_filter").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_filter').load(url);
        return false;
    });
    $("#provinsi_filter2").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_profile_filter').load(url);
        return false;
    });
 

</script>
<?php require_once "header2.php";?>
<?php foreach($dinas_view as $r): ?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Formulir IB <?php if (isset($nama_kabkot_from)){echo $nama_kabkot_from;}?><small class="page-title-tag">Rekapitulasi</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?=base_url();?>">Home</a><i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Rekapitulasi Formulir IB 
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
                  <!-- BEGIN TOMBOL TAMBAH  -->
									<div class="col-md-6">
										<div class="btn-group">
										    <?php if ($privilage=="3"){ ?>
                      <form action="Form1_insert2" id="Form1_insert" class="form-horizontal" method="post">
                      <?php foreach($dinas_view as $r): ?>
											<select name='tahun' id='tahun'  class="form-control" style="width: 140px">
												<!-- <option value="">Pilih Tahun</option> -->
												<?php
													$tahun_berjalan = date('Y');
													$tahun = $tahun_berjalan;
														for($t=$tahun;$t>$tahun-10;$t--){
															$this->db->select('*');
                              $this->db->where('tahun',$t);
                              $this->db->where('iddinas',$r->iddinas);
                              $this->db->from('form_1');
                              $query = $this->db->get();
                          		  if ($query->num_rows()<1){
																	echo "<option value=".$t.">".$t."</option>";
																}
																else{
																	echo "<option value=".$t." disabled>".$t."</option>";
																}
														}
												?>
												</select>
												<input type='hidden' id='idprovinsi' name='idprovinsi' value='<?=$r->idprovinsi;?>'></input>
												<input type='hidden' id='iddinas' name='iddinas' value='<?=$r->iddinas;?>'></input>
												<input type='hidden' id='privilage' name='privilage' value='<?=$r->idprivilage;?>'></input>
                      <?php endforeach; ?>
												<input type='submit' id='sample_editable_1_new' value='Tambah Form IB' class='btn left-zeroradius add-form-1 green'></input>
											</form>
											<?php } ?>
										</div>
									</div>
                  <!-- END TOMBOL TAMBAH  -->
                  <!-- BEGIN TOMBOL PERALATAN -->
                  <div class="col-xs-6">
                    <div class="btn-group pull-right">
                      <button type="button" class="btn default" data-toggle="dropdown" aria-expanded="false">
                        Peralatan <i class="fa fa-angle-down"></i>
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li>
                          <a href="javascript:window.print();"><i class="fa fa-print"></i> Cetak Halaman </a>
                        </li>
                        <li>
                          <a href="<?=BASE_URL('main/cetak_form_1a_view');?>"><i class="fa fa-file-pdf-o"></i> Ekspor ke PDF </a>
                        </li>
                        <li>
                          <a href="javascript:;"><i class="fa fa-file-excel-o"></i> Ekspor ke Excel </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- END TOMBOL PERALATAN -->
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th width="9%" class="table-checkbox hide"> No. </th>
									<th width="20%"> Tahun </th>
									<th width="25%"> Tanggal Dibuat </th>
									<th width="18%"> Status </th>
									<th width="10%"> Lihat </th>
									
										    <?php if ($privilage=="3") { ?>
									<th width="18%"> Fungsi </th>
									<?php } else{?>
									<th width="18%"> Waktu Perubahan </th>
									<?php }?>
								</tr>
								</thead>
								<tbody>
	              <?php $i=0; foreach($form_1_view as $r): $i++;?>
								<tr class="odd gradeX">
									<td class="hide">
										<?php echo $i;?>
									</td>
									<td>
										 <?php echo $r->tahun;?>
									</td>
									<td class="center">
										 <?php echo $r->tanggal_buat;?>
									</td>
									<td>
	                  <?php if($r->status=="Draft"){?>
										<span class="label label-sm label-warning">	Draft </span>
	                  <?php } elseif ($r->status=="Disetujui"){?>
										<span class="label label-sm label-success">	Disetujui </span>
	                  <?php }?>
									</td>
									<td class="center">
										<?php echo form_open('main/form1b_cetak'); ?>
	                  <?php echo form_input(array('id' => 'idform1', 'name' => 'idform1', 'type' => 'hidden', 'value' => $r->idform_1)); ?>
	                  <?php echo form_submit(array('id' => 'sample_editable_1_new', 'style' => 'width: 60px', 'value' => 'Lihat', 'class' => 'btn btn-xs grey-cascade')); ?>
										<?php echo form_close(); ?>
									</td>
									<td class="center">
									   
										    <?php if ($privilage=="3") { ?>
	                  <table border="0">
	                    <tr>
	                      <td>
													<?php echo form_open('main/form_1b'); ?>
	                        <?php echo form_input(array('id' => 'idform1', 'name' => 'idform1', 'type' => 'hidden', 'value' => $r->idform_1)); ?>
	                        <?php echo form_submit(array('id' => 'sample_editable_1_new', 'style' => 'width: 60px', 'value' => 'Ubah', 'class' => 'btn btn-xs grey-cascade')); ?>
												  <?php echo form_close(); ?>
												</td>
	                      <td>
													<a href="javascript:confirmDelete(<?=$r->idform_1;?>)" class="btn btn-xs red-sunglo" style="width: 60px"> Hapus </a>
												</td>
	                    </tr>
	                  </table>
	                  <?php } else  { ?>
	                  <?php echo $r->waktu_buat;?>
	                  <?php } ?>
									</td>
								</tr>
	              <?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<?php require_once('footer2.php'); ?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
   Layout.init(); // init current layout
   Demo.init(); // init demo features
   TableManaged.init();
});

function confirmDelete(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_12');?>/"+delUrl;
  }
}
</script>
 
<script> 
    $("#provinsi_filter").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_filter').load(url);
        return false;
    });
    $("#provinsi_filter2").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_profile_filter').load(url);
        return false;
    });
 

</script>
<?php endforeach; ?>
</body>
<!-- END BODY -->
</html>
