-- MySQL dump 10.15  Distrib 10.0.36-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: basisdataperumahan
-- ------------------------------------------------------
-- Server version	10.0.36-MariaDB-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `idmenu` int(11) NOT NULL AUTO_INCREMENT,
  `idprivilage` int(11) DEFAULT NULL,
  `idparent` int(11) DEFAULT '0',
  `urut` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `Category` varchar(25) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `kind` varchar(200) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `icon` varchar(200) DEFAULT NULL,
  `csubmenu_caret` varchar(10) NOT NULL,
  `notif` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `atribut` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`idmenu`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,1,0,1,'Dashboard','Parent','main/index',NULL,'2016-02-11 01:21:51','icon-briefcase','0',0,0,NULL),(2,1,0,2,'Profil','Parent','#profile_modal',NULL,'2017-02-11 01:21:51','','0',0,0,'data-toggle=\"modal\" '),(3,1,0,4,'Formulir','Parent','','classic-menu-dropdown','2017-02-11 01:21:51','icon-briefcase','0',0,1,NULL),(5,1,4,NULL,'Pengguna','Child','main/pengguna',NULL,'2017-03-28 17:55:15','icon-user','no-caret',0,0,NULL),(7,1,4,NULL,'Data Master','Child','',NULL,'2017-03-28 17:58:24','icon-folder','0',0,0,NULL),(8,1,7,NULL,'Provinsi','Grandchild','',NULL,'2017-03-28 17:58:24','icon-pointer','0',0,0,NULL),(9,1,7,NULL,'Kabupaten/kota','Grandchild','',NULL,'2017-03-28 17:59:20','icon-pointer','0',0,0,NULL),(10,1,7,NULL,'Dinas','Grandchild','main/dinas',NULL,'2017-03-28 17:59:20','icon-briefcase','0',0,0,NULL),(12,1,0,5,'Laporan','Parent','','classic-menu-dropdown','2017-02-11 01:21:51','icon-briefcase','0',0,0,NULL),(13,1,3,NULL,'Provinsi','Child','',NULL,'2016-12-11 01:21:51','icon-notebook','0',0,0,NULL),(14,1,3,NULL,'Kabupaten/Kota','Child','',NULL,'2017-02-11 01:21:51','icon-notebook','0',0,0,NULL),(21,1,13,NULL,'Form 1A','Grandchild','form-1a',NULL,'2017-02-11 01:21:51','icon-notebook','0',0,0,'data-toggle=\"modal\" '),(27,1,14,NULL,'Form 1B','Grandchild','form-1b','','2017-03-15 21:33:07','icon-notebook','0',0,0,'data-toggle=\"modal\" '),(29,1,31,NULL,'RTLH','Grandchild','main/rtlh','','2017-03-19 13:40:06','icon-list','0',0,0,NULL),(30,1,32,NULL,'RTLH','Grandchild','main/rtlh','','2017-03-19 13:40:06','icon-list','0',0,0,NULL),(31,1,12,NULL,'Provinsi','Child','main/provinsi',NULL,'2017-03-19 13:41:09','icon-list','0',0,0,NULL),(32,1,12,NULL,'Kabupaten/Kota','Child',NULL,NULL,'2017-03-19 13:41:09','icon-list','0',0,0,NULL),(33,2,0,1,'Dashboard','Parent','main/index',NULL,'2016-02-11 01:21:51','icon-briefcase','0',0,0,NULL),(34,2,0,2,'Profil','Parent','main/profil',NULL,'2017-02-11 01:21:51','icon-briefcase','0',0,0,NULL),(35,2,0,4,'Formulir','Parent','','classic-menu-dropdown','2017-02-11 01:21:51','icon-briefcase','0',0,0,NULL),(37,2,35,NULL,'Form 1A','','form-1a',NULL,'2016-12-11 01:21:51','icon-notebook','no-caret',0,0,''),(42,2,35,NULL,'Form 1B','','form-1b',NULL,'2017-03-19 13:40:06','icon-notebook','no-caret',0,0,'data-toggle=\"modal\" '),(46,3,0,1,'Dashboard','Parent','main/index',NULL,'2016-02-11 01:21:51','icon-briefcase','0',0,0,NULL),(47,3,0,2,'Profil','Parent','main/profil',NULL,'2017-02-11 01:21:51','icon-briefcase','0',0,0,NULL),(48,3,0,4,'Formulir','Parent','','classic-menu-dropdown','2017-02-11 01:21:51','icon-briefcase','0',0,0,NULL),(51,3,48,NULL,'Form 1B','','main/form_1b_view',NULL,'2017-02-11 01:21:51','icon-notebook','no-caret',0,0,NULL),(67,1,31,NULL,'Kepemilikan Rumah','Grandchild','main/status_kepemilikan_rumah',NULL,'2017-03-26 14:05:19','icon-list','0',0,0,NULL),(68,1,32,NULL,'Kepemilikan Rumah','Grandchild','main/status_kepemilikan_rumah_kabupaten_kota',NULL,'2017-03-26 14:05:19','icon-list','0',0,0,NULL),(69,1,0,7,'Informasi Aplikasi','Parent','','classic-menu-dropdown','2017-03-29 01:31:33',NULL,'0',0,0,NULL),(71,3,0,6,'Informasi Aplikasi','Parent','','classic-menu-dropdown','2017-03-29 01:31:33',NULL,'0',0,0,NULL),(74,2,0,4,'Laporan','Parent',NULL,'classic-menu-dropdown','2017-03-30 13:39:10',NULL,'no-caret',0,0,NULL),(75,2,74,NULL,'RTLH','','main/rtlh',NULL,'2017-03-30 13:40:18','icon-list','no-caret',0,0,NULL),(76,2,74,NULL,'Kepemilikan Rumah','','main/status_kepemilikan_rumah',NULL,'2017-03-30 13:41:37','icon-list','no-caret',0,0,NULL),(78,NULL,0,NULL,'RTLH','Parent',NULL,NULL,'2017-03-30 13:43:11',NULL,'0',0,0,NULL),(80,2,0,5,'Informasi Aplikasi','Parent','','classic-menu-dropdown','2017-03-30 13:44:27',NULL,'0',0,0,NULL),(83,2,82,NULL,'Pengguna','','main/pengguna_dinas',NULL,'2017-03-30 22:10:12','icon-user','no-caret',0,0,NULL),(84,2,82,NULL,'Dinas PKP','','main/dinas_view',NULL,'2017-03-30 22:10:12','icon-briefcase','no-caret',0,0,NULL),(86,3,85,NULL,'Pengguna','','main/pengguna_dinas',NULL,'2017-03-30 23:12:03','icon-user','no-caret',0,0,NULL),(87,3,85,NULL,'Dinas PKP','','main/dinas_view',NULL,'2017-03-30 23:14:25','icon-briefcase','no-caret',0,0,NULL),(88,3,85,NULL,'Kecamatan','','main/kecamatan',NULL,'2017-03-30 23:14:25','icon-pointer','no-caret',0,0,NULL),(89,3,0,5,'Laporan','Parent',NULL,'classic-menu-dropdown','2017-03-30 23:14:25','','0',0,0,NULL),(90,3,89,NULL,'RTLH','','main/rtlh_kecamatan',NULL,'2017-03-30 23:14:25','icon-list','no-caret',0,0,NULL),(107,2,82,NULL,'Kabupaten/Kota','','main/kabupaten_kota',NULL,'2017-03-30 23:30:32','icon-pointer\r\n','no-caret',0,0,NULL),(108,2,112,NULL,'Form Pengembang','','main/form_pengembang_view',NULL,'2017-03-31 06:35:41','icon-notebook','0',0,0,NULL),(109,3,113,NULL,'Form Suplai IMB','','main/form_imb_view',NULL,'2017-03-31 06:36:31','icon-notebook','0',0,0,NULL),(110,2,112,NULL,'Form Perbankan','','main/form_perbankan_view',NULL,'2017-04-10 05:56:09','icon-notebook','0',0,0,NULL),(111,3,113,NULL,'Form Suplai Non-IMB','','main/form_non_imb_view',NULL,'2017-04-10 05:56:09','icon-notebook','0',0,0,NULL),(112,2,35,NULL,'Suplai Perumahan','','',NULL,'2017-04-10 05:57:56','icon-notebook','0',0,0,NULL),(113,3,48,NULL,'Suplai Perumahan','','',NULL,'2017-04-10 05:57:56','icon-notebook','0',0,0,NULL),(114,3,89,NULL,'Kepemilikan Rumah','','main/status_kepemilikan_rumah_kabupaten_kota',NULL,'2017-05-02 00:39:02','icon-list','no-caret',0,0,NULL),(115,1,13,NULL,'Formulir Perbankan','Grandchild','main/form_perbankan_view',NULL,'2017-05-04 15:29:58','icon-notebook','',0,0,NULL),(116,1,13,NULL,'Formulir Pengembang','Grandchild','main/form_pengembang_view',NULL,'2017-05-04 15:31:24','icon-notebook','',0,0,NULL),(117,1,14,NULL,'Formulir IMB','Grandchild','main/form_imb_view',NULL,'2017-05-04 15:36:45',NULL,'',0,0,NULL),(118,1,14,NULL,'Formulir Non-IMB','Grandchild','main/form_non_imb_view',NULL,'2017-05-04 15:36:46',NULL,'',0,0,NULL),(119,1,31,NULL,'Suplai Perumahan','Grandchild','main/sejuta_rumah_kabupaten_kota',NULL,'2017-05-04 15:39:30',NULL,'',0,0,NULL),(120,1,32,NULL,'Suplai Perumahan','Grandchild','main/sejuta_rumah_kabupaten_kota',NULL,'2017-05-04 15:39:31',NULL,'',0,0,NULL),(121,3,89,NULL,'Suplai Perumahan','Grandchild','main/sejuta_rumah_kabupaten_kota',NULL,'2017-05-04 15:46:54','icon-list','no-caret',0,0,NULL),(122,2,74,NULL,'Suplai Perumahan','Grandchild','main/sejuta_rumah_kabupaten_kota',NULL,'2017-05-04 15:55:04','icon-list','no-caret',0,0,NULL),(123,1,69,NULL,'Tentang e-Basisdata','Child','main/tentang',NULL,'2017-05-04 17:44:22','icon-info','no-caret',0,0,NULL),(124,3,71,NULL,'Tentang e-Basisdata','Child','main/tentang',NULL,'2017-05-04 17:45:14','icon-info','no-caret',0,0,NULL),(125,2,80,NULL,'Tentang e-Basisdata','Child','main/tentang',NULL,'2017-05-04 17:45:14','icon-info','no-caret',0,0,NULL),(126,1,69,NULL,'FAQ','Child','main/faq',NULL,'2017-05-04 17:47:56','icon-question','no-caret',0,0,NULL),(127,2,80,NULL,'FAQ','Child','main/faq',NULL,'2017-05-04 17:47:56','icon-question','no-caret',0,0,NULL),(128,3,71,NULL,'FAQ','Child','main/faq',NULL,'2017-05-04 17:47:56','icon-question','no-caret',0,0,NULL),(129,1,69,NULL,'Pesan','Child','main/kontak_admin',NULL,'2017-05-04 17:53:48','icon-envelope','no-caret',0,0,NULL),(130,2,80,NULL,'Kontak Kami','Child','main/kontak',NULL,'2017-05-04 17:53:48','icon-envelope','no-caret',0,0,NULL),(131,3,71,NULL,'Kontak Kami','Child','main/kontak',NULL,'2017-05-04 17:53:48','icon-envelope','no-caret',0,0,NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-01  3:38:47
