-- MySQL dump 10.15  Distrib 10.0.36-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: basisdataperumahan
-- ------------------------------------------------------
-- Server version	10.0.36-MariaDB-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backlog`
--

DROP TABLE IF EXISTS `backlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backlog` (
  `idbacklog` int(11) NOT NULL AUTO_INCREMENT,
  `iddinas` int(11) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  `jumlah_kk` float DEFAULT NULL,
  `milik_sendiri` float DEFAULT NULL,
  `sewa_kontrak` float DEFAULT NULL,
  `menumpang` float DEFAULT NULL,
  `lainnya` float DEFAULT NULL,
  `kepemilikan` float DEFAULT NULL,
  `penghunian` float DEFAULT NULL,
  `sumber_data` varchar(120) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  PRIMARY KEY (`idbacklog`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `backlog_kepemilikan_dan_penghunian`
--

DROP TABLE IF EXISTS `backlog_kepemilikan_dan_penghunian`;
/*!50001 DROP VIEW IF EXISTS `backlog_kepemilikan_dan_penghunian`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `backlog_kepemilikan_dan_penghunian` (
  `tanggal` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `kode_provinsi` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `rtlh` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `non_mbr_4` tinyint NOT NULL,
  `non_mbr_6` tinyint NOT NULL,
  `mbr_5` tinyint NOT NULL,
  `mbr_7` tinyint NOT NULL,
  `kepemilikan` tinyint NOT NULL,
  `penghunian` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `backlog_kepemilikan_dan_penghunian_kab`
--

DROP TABLE IF EXISTS `backlog_kepemilikan_dan_penghunian_kab`;
/*!50001 DROP VIEW IF EXISTS `backlog_kepemilikan_dan_penghunian_kab`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `backlog_kepemilikan_dan_penghunian_kab` (
  `tanggal` tinyint NOT NULL,
  `rtlh` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `kepemilikan` tinyint NOT NULL,
  `penghunian` tinyint NOT NULL,
  `non_mbr_4` tinyint NOT NULL,
  `non_mbr_6` tinyint NOT NULL,
  `mbr_5` tinyint NOT NULL,
  `mbr_7` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `backlog_kepemilikan_dan_penghunian_kab_view`
--

DROP TABLE IF EXISTS `backlog_kepemilikan_dan_penghunian_kab_view`;
/*!50001 DROP VIEW IF EXISTS `backlog_kepemilikan_dan_penghunian_kab_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `backlog_kepemilikan_dan_penghunian_kab_view` (
  `tanggal` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `rtlh` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `kepemilikan` tinyint NOT NULL,
  `penghunian` tinyint NOT NULL,
  `mak1` tinyint NOT NULL,
  `mak2` tinyint NOT NULL,
  `non_mbr_4` tinyint NOT NULL,
  `mbr_5` tinyint NOT NULL,
  `non_mbr_6` tinyint NOT NULL,
  `mbr_7` tinyint NOT NULL,
  `mak4` tinyint NOT NULL,
  `mak5` tinyint NOT NULL,
  `mak6` tinyint NOT NULL,
  `mak7` tinyint NOT NULL,
  `totalrtlh` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `backlog_kepemilikan_dan_penghunian_view`
--

DROP TABLE IF EXISTS `backlog_kepemilikan_dan_penghunian_view`;
/*!50001 DROP VIEW IF EXISTS `backlog_kepemilikan_dan_penghunian_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `backlog_kepemilikan_dan_penghunian_view` (
  `tanggal` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `kode_provinsi` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `kepemilikan` tinyint NOT NULL,
  `penghunian` tinyint NOT NULL,
  `non_mbr_4` tinyint NOT NULL,
  `mbr_5` tinyint NOT NULL,
  `non_mbr_6` tinyint NOT NULL,
  `rtlh` tinyint NOT NULL,
  `mbr_7` tinyint NOT NULL,
  `mak1` tinyint NOT NULL,
  `mak2` tinyint NOT NULL,
  `mak4` tinyint NOT NULL,
  `mak5` tinyint NOT NULL,
  `mak6` tinyint NOT NULL,
  `mak7` tinyint NOT NULL,
  `totalrtlh` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `dinas`
--

DROP TABLE IF EXISTS `dinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dinas` (
  `iddinas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dinas` varchar(120) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `id_privilage` int(11) DEFAULT NULL,
  `alamat` text,
  `avatar` text,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `idkelurahan` int(11) DEFAULT NULL,
  `firstName` varchar(120) DEFAULT NULL,
  `lastName` varchar(120) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `telepon` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `URL` varchar(50) DEFAULT NULL,
  `nomor_id` varchar(30) DEFAULT NULL,
  `x` float DEFAULT '0',
  `y` float DEFAULT '0',
  `alamat_user` text,
  `telepon_user` varchar(20) DEFAULT NULL,
  `email_user` varchar(120) DEFAULT NULL,
  `login_count` int(11) DEFAULT '0',
  `isLoggedIn` enum('true','false') DEFAULT 'false',
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iddinas`)
) ENGINE=InnoDB AUTO_INCREMENT=565 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `dinas_update` BEFORE UPDATE ON `dinas` FOR EACH ROW SET new.update_date = NOW() */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `form_1`
--

DROP TABLE IF EXISTS `form_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1` (
  `idform_1` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_buat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `idprovinsi` int(11) DEFAULT NULL,
  `iddinas` int(11) DEFAULT NULL,
  `tanggal_pengesahan` timestamp NULL DEFAULT NULL,
  `tahun` int(11) DEFAULT '0',
  `status` enum('Disetujui','Butuh Revisi','Belum Disetujui','Draft') DEFAULT 'Draft',
  `catatan_revisi` text,
  `penjelasan_k1` text,
  `penjelasan_k2_a` text,
  `penjelasan_k2_b` text,
  `penjelasan_k3` text,
  `penjelasan_k4_1` text,
  `penjelasan_k4_2` text,
  `penjelasan_k4_3` text,
  `penjelasan_k4_4` text,
  `penjelasan_k4_5` text,
  `penjelasan_k5` text,
  `penjelasan_k6` text,
  `formulir` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`idform_1`)
) ENGINE=InnoDB AUTO_INCREMENT=2714 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `insert_form1` AFTER INSERT ON `form_1` FOR EACH ROW BEGIN 
INSERT INTO form_1_k2_a ( idform_1, uraian1) VALUES ( NEW.idform_1,'Total APBD Provinsi' );
INSERT INTO form_1_k2_a ( idform_1, uraian1) VALUES ( NEW.idform_1,'Anggaran Dinas PKP Provinsi' );

INSERT INTO form_1_k4_1 ( idform_1, status1, status2, status3, status4, status5) VALUES ( NEW.idform_1,'Milik sendiri','Kontrak / sewa','Bebas sewa','Dinas','lainnya' ); 


INSERT INTO form_1_k4_2 ( idform_1, jenis1, jenis2) VALUES ( NEW.idform_1,'Rumah Tapak','Rumah Susun/Apartemen' ); 

INSERT INTO form_1_k4_3 ( idform_1, fungsi1, fungsi2) VALUES ( NEW.idform_1,'Rumah Dengan 1 (satu) KK','Rumah dengan lebih dari 1 (satu) KK' ); 

insert into log_pekerjaan set jenis_pekerjaan='Tambah Form 1',idpekerjaan=NEW.idform_1,iddinas=NEW.iddinas,waktu=now(),status=0; 

insert into kelengkapan set idform_1=new.idform_1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form1_update` AFTER UPDATE ON `form_1` FOR EACH ROW BEGIN
          DECLARE jenis,idkab,jmlprov,i,idprov,kabisi integer;  
SELECT id_privilage INTO jenis FROM dinas WHERE iddinas = NEW.iddinas;  
SELECT kk.idkabupaten_kota INTO idkab FROM dinas d join kabupaten_kota kk on kk.iddinas=d.iddinas WHERE d.iddinas = NEW.iddinas; 
SELECT count(*) INTO jmlprov FROM provinsi; 
SELECT count(*) INTO kabisi FROM statusform_kab WHERE idkabupaten_kota = idkab and tahun=NEW.tahun; 
if NEW.status='Disetujui' then 
SET i=0;
WHILE i<jmlprov DO 
SELECT idprovinsi INTO idprov FROM provinsi LIMIT i,1; 
INSERT INTO statusform(idprovinsi, tahun) VALUES(idprov, NEW.tahun);
SET i = i + 1;
END WHILE; 
if kabisi=0 and jenis=3 then
INSERT INTO statusform_kab(idkabupaten_kota, tahun) VALUES(idkab, NEW.tahun);
end if;
if jenis=2 then
  update statusform set form1a=1 where idprovinsi=NEW.idprovinsi and tahun=NEW.tahun; 
elseif jenis=3 then 
  update statusform set form1b=form1b+1 where idprovinsi=NEW.idprovinsi and tahun=NEW.tahun; 
  update statusform_kab set form1b=1 where idkabupaten_kota=idkab and tahun=NEW.tahun;   
  insert into statusform_detail set idprovinsi=NEW.idprovinsi,tahun=NEW.tahun,idkabupaten_kota=idkab; 
end if; 
end if; 
      END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `form_1_before_delete` BEFORE DELETE ON `form_1` FOR EACH ROW BEGIN 
   delete from kelengkapan where `idform_1`=old.idform_1;
   delete from form_1_k1 where `idform_1`=old.idform_1;
delete from form_1_k2_a where idform_1=old.idform_1;
delete from form_1_k2_b where `idform_1`=old.`idform_1`;
delete from form_1_k3 where `idform_1`=old.`idform_1`;
delete from form_1_k4_1 where `idform_1`=old.`idform_1`;
 delete from form_1_k4_2 where `idform_1`=old.`idform_1`;
 delete from form_1_k4_3 where `idform_1`=old.`idform_1`;
 delete from form_1_k4_4 where `idform_1`=old.`idform_1`;
 delete from form_1_k4_5 where `idform_1`=old.`idform_1`;
 delete from form_1_k5 where `idform_1`=old.`idform_1`;
 delete from form_1_k6 where `idform_1`=old.`idform_1`;
 delete from status_kepemilikan_rumah_backlog where `idform_1`=old.`idform_1`; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `form_1_k1`
--

DROP TABLE IF EXISTS `form_1_k1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k1` (
  `idform_1_k1` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `struktur_dinas` text,
  PRIMARY KEY (`idform_1_k1`),
  KEY `idform_1` (`idform_1`)
) ENGINE=InnoDB AUTO_INCREMENT=575 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `form_1_k2_a`
--

DROP TABLE IF EXISTS `form_1_k2_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k2_a` (
  `idform_1_k2_a` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `uraian1` varchar(300) DEFAULT NULL,
  `k221` decimal(25,2) DEFAULT '0.00',
  `k231` decimal(25,2) DEFAULT '0.00',
  PRIMARY KEY (`idform_1_k2_a`),
  KEY `idform_1` (`idform_1`)
) ENGINE=InnoDB AUTO_INCREMENT=3260 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k2_a` AFTER UPDATE ON `form_1_k2_a` FOR EACH ROW BEGIN  
      if (NEW.k221>-1)or(NEW.k231>-1) then
        update kelengkapan set k2_a=1 where idform_1=NEW.idform_1; 
      else 
        update kelengkapan set k2_a=0 where idform_1=NEW.idform_1; 
      end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `form_1_k2_b`
--

DROP TABLE IF EXISTS `form_1_k2_b`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k2_b` (
  `idform_1_k2_b` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `jenis_kegiatan_urusan_pkp_2` text,
  `ta_a_vol_unit_3` int(11) DEFAULT '0',
  `ta_a_biaya_4` decimal(25,2) DEFAULT '0.00',
  `ta_a_vol_unit_5` int(11) DEFAULT '0',
  `ta_a_biaya_6` decimal(25,2) DEFAULT '0.00',
  PRIMARY KEY (`idform_1_k2_b`)
) ENGINE=InnoDB AUTO_INCREMENT=1312 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k2_b_insert` AFTER INSERT ON `form_1_k2_b` FOR EACH ROW BEGIN  
     if (NEW.ta_a_biaya_4>-1)or(NEW.ta_a_biaya_6>-1) then
       update kelengkapan set k2_b=1 where idform_1=NEW.idform_1;   
     else
       update kelengkapan set k2_b=0 where idform_1=NEW.idform_1;   
     end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k2_b` AFTER UPDATE ON `form_1_k2_b` FOR EACH ROW BEGIN  
      if (NEW.ta_a_biaya_4>-1)or(NEW.ta_a_biaya_6>-1) then
        update kelengkapan set k2_b=1 where idform_1=NEW.idform_1;   
      else
        update kelengkapan set k2_b=0 where idform_1=NEW.idform_1;   
      end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k2_b_delete` AFTER DELETE ON `form_1_k2_b` FOR EACH ROW BEGIN  
DECLARE ada integer;
select count(*) into ada from form_1_k2_b where idform_1_k2_b=OLD.idform_1_k2_b;
   if (ada>0) then
     update kelengkapan set k2_b=1 where idform_1=OLD.idform_1;   
   else
     update kelengkapan set k2_b=0 where idform_1=OLD.idform_1;   
   end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `form_1_k3`
--

DROP TABLE IF EXISTS `form_1_k3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k3` (
  `idform_1_k3` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `isi_1a` enum('Y','T') DEFAULT 'T',
  `isi_1b` enum('Y','T') DEFAULT 'T',
  `isi_1c` enum('Y','T') DEFAULT 'T',
  `isi_1d` enum('Y','T') DEFAULT 'T',
  `isi_1e` enum('Y','T') DEFAULT 'T',
  `isi_1f` enum('Y','T') DEFAULT 'T',
  `isi_1f_keterangan` text,
  `isi_2a` enum('Y','T') DEFAULT 'T',
  `isi_2b` enum('Y','T') DEFAULT 'T',
  `isi_2c` enum('Y','T') DEFAULT 'T',
  `isi_2d` enum('Y','T') DEFAULT 'T',
  `isi_2e` enum('Y','T') DEFAULT 'T',
  `isi_2c_keterangan` text,
  `isi_2d_keterangan` text,
  `isi_2e_keterangan` text,
  PRIMARY KEY (`idform_1_k3`)
) ENGINE=InnoDB AUTO_INCREMENT=1168 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `form_1_k4_1`
--

DROP TABLE IF EXISTS `form_1_k4_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k4_1` (
  `idform_1_k4_1` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `k4141` decimal(25,0) DEFAULT '0',
  `k4142` decimal(25,0) DEFAULT '0',
  `k4143` decimal(25,0) DEFAULT '0',
  `k4144` decimal(25,0) DEFAULT '0',
  `k4145` decimal(25,0) DEFAULT '0',
  `k4151` varchar(120) DEFAULT NULL,
  `k4152` varchar(120) DEFAULT NULL,
  `k4153` varchar(120) DEFAULT NULL,
  `k4154` varchar(120) DEFAULT NULL,
  `k4155` varchar(120) DEFAULT NULL,
  `status1` varchar(200) DEFAULT NULL,
  `status2` varchar(200) DEFAULT NULL,
  `status3` varchar(200) DEFAULT NULL,
  `status4` varchar(200) DEFAULT NULL,
  `status5` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idform_1_k4_1`)
) ENGINE=InnoDB AUTO_INCREMENT=1633 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `backlog_insert` AFTER INSERT ON `form_1_k4_1` FOR EACH ROW BEGIN 
 
   	insert into status_kepemilikan_rumah_backlog set idform_1=NEW.idform_1,idform_1_k4_1=NEW.idform_1_k4_1,iddinas=(select iddinas from form_1 where idform_1=NEW.idform_1),idprovinsi=(select idprovinsi from form_1 where idform_1=NEW.idform_1),milik_sendiri=NEW.k4141,kontrak_sewa=NEW.k4142,bebas_sewa=NEW.k4143,dinas=NEW.k4144,lainnya=NEW.k4145,kepemilikan=(NEW.k4141+NEW.k4142+NEW.k4143+NEW.k4144+NEW.k4145),penghunian=(NEW.k4143),tahun=(select tahun from form_1 where idform_1=NEW.idform_1); 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `backlog_update` AFTER UPDATE ON `form_1_k4_1` FOR EACH ROW BEGIN  
 update status_kepemilikan_rumah_backlog set idform_1_k4_1=NEW.idform_1_k4_1,iddinas=(select iddinas from form_1 where idform_1=NEW.idform_1),idprovinsi=(select idprovinsi from form_1 where idform_1=NEW.idform_1),milik_sendiri=NEW.k4141,kontrak_sewa=NEW.k4142,bebas_sewa=NEW.k4143,dinas=NEW.k4144,lainnya=NEW.k4145,kepemilikan=(NEW.k4141+NEW.k4142+NEW.k4143+NEW.k4144+NEW.k4145),penghunian=(NEW.k4143) where idform_1=NEW.idform_1 and tahun=(select tahun from form_1 where idform_1=NEW.idform_1); 

if (NEW.k4141>0)or(NEW.k4142>0)or(NEW.k4143>0)or(NEW.k4144>0)or(NEW.k4145>0) then
       update kelengkapan set k4_1=1 where idform_1=NEW.idform_1; 
     end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `form_1_k4_2`
--

DROP TABLE IF EXISTS `form_1_k4_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k4_2` (
  `idform_1_k4_2` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `k4231` decimal(25,2) DEFAULT '0.00',
  `k4232` decimal(25,2) DEFAULT '0.00',
  `k4241` varchar(200) DEFAULT NULL,
  `k4242` varchar(200) DEFAULT NULL,
  `jenis1` varchar(200) DEFAULT 'Rumah Tapak',
  `jenis2` varchar(200) DEFAULT 'Rumah Susun/Apartemen',
  PRIMARY KEY (`idform_1_k4_2`)
) ENGINE=InnoDB AUTO_INCREMENT=1612 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k4_2` AFTER UPDATE ON `form_1_k4_2` FOR EACH ROW BEGIN  
      if (NEW.k4231>-1)or(NEW.k4232>-1) then
        update kelengkapan set k4_2=1 where idform_1=NEW.idform_1; 
      else
        update kelengkapan set k4_2=0 where idform_1=NEW.idform_1; 
      end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `form_1_k4_3`
--

DROP TABLE IF EXISTS `form_1_k4_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k4_3` (
  `idform_1_k4_3` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `k4331` decimal(25,2) DEFAULT '0.00',
  `k4332` decimal(25,2) DEFAULT '0.00',
  `k4341` varchar(200) DEFAULT NULL,
  `k4342` varchar(200) DEFAULT NULL,
  `fungsi1` varchar(300) DEFAULT NULL,
  `fungsi2` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idform_1_k4_3`)
) ENGINE=InnoDB AUTO_INCREMENT=1614 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `backlog_update2` AFTER UPDATE ON `form_1_k4_3` FOR EACH ROW BEGIN 
 
   	update status_kepemilikan_rumah_backlog set idform_1_k4_3=NEW.idform_1_k4_3,jumlah_kk=NEW.k4331+NEW.k4332 where idform_1=NEW.idform_1; 
if (NEW.k4331>-1)or(NEW.k4332>-1) then
        update kelengkapan set k4_3=1 where idform_1=NEW.idform_1; 
else 
        update kelengkapan set k4_3=0 where idform_1=NEW.idform_1; 
      end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `form_1_k4_4`
--

DROP TABLE IF EXISTS `form_1_k4_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k4_4` (
  `idform_1_k4_4` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `idkabupaten_kota` varchar(120) DEFAULT NULL,
  `jumlah_rumah_3` decimal(25,2) DEFAULT NULL,
  `jumlah_rumah_4` decimal(25,2) DEFAULT NULL,
  `sumber_data_5` varchar(120) DEFAULT NULL,
  `idkelurahan` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  PRIMARY KEY (`idform_1_k4_4`)
) ENGINE=InnoDB AUTO_INCREMENT=13835 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k4_4_add` AFTER INSERT ON `form_1_k4_4` FOR EACH ROW BEGIN  
      if (NEW.jumlah_rumah_3>-1)or(NEW.jumlah_rumah_4>-1) then
        update kelengkapan set k4_4=1 where idform_1=NEW.idform_1; 
      end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k4_4` AFTER UPDATE ON `form_1_k4_4` FOR EACH ROW BEGIN  
      if (NEW.jumlah_rumah_3>-1)or(NEW.jumlah_rumah_4>-1) then
        update kelengkapan set k4_4=1 where idform_1=NEW.idform_1; 
      end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k4_4_delete` AFTER DELETE ON `form_1_k4_4` FOR EACH ROW BEGIN  
DECLARE ada integer;
select count(*) into ada from form_1_k4_4 where idform_1_k4_4=OLD.idform_1_k4_4;
  if (ada>4) then
    update kelengkapan set k4_4=1 where idform_1=OLD.idform_1;   
  else
    update kelengkapan set k4_4=0 where idform_1=OLD.idform_1;   
  end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `form_1_k4_4_view`
--

DROP TABLE IF EXISTS `form_1_k4_4_view`;
/*!50001 DROP VIEW IF EXISTS `form_1_k4_4_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_1_k4_4_view` (
  `idform_1_k4_4` tinyint NOT NULL,
  `idform_1` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `jumlah_rumah_3` tinyint NOT NULL,
  `jumlah_rumah_4` tinyint NOT NULL,
  `sumber_data_5` tinyint NOT NULL,
  `idkelurahan` tinyint NOT NULL,
  `kelurahan` tinyint NOT NULL,
  `idkecamatan` tinyint NOT NULL,
  `kecamatan` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `form_1_k4_5`
--

DROP TABLE IF EXISTS `form_1_k4_5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k4_5` (
  `idform_1_k4_5` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `non_mbr_2` decimal(25,2) DEFAULT NULL,
  `mbr_3` decimal(25,2) DEFAULT NULL,
  `non_mbr_4` decimal(25,2) DEFAULT NULL,
  `mbr_5` decimal(25,2) DEFAULT NULL,
  `non_mbr_6` decimal(25,2) DEFAULT NULL,
  `mbr_7` decimal(25,2) DEFAULT NULL,
  `sumber_data_8` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`idform_1_k4_5`)
) ENGINE=InnoDB AUTO_INCREMENT=1068 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k4_5` AFTER INSERT ON `form_1_k4_5` FOR EACH ROW BEGIN  
      if (NEW.non_mbr_2>-1)or(NEW.mbr_3>-1)or(NEW.non_mbr_4>-1)or(NEW.mbr_5>-1)or(NEW.non_mbr_6>-1)or(NEW.mbr_7>-1) then
        update kelengkapan set k4_5=1 where idform_1=NEW.idform_1; 
      end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k4_5_delete` AFTER DELETE ON `form_1_k4_5` FOR EACH ROW BEGIN  
DECLARE ada integer;
select count(*) into ada from form_1_k4_5 where idform_1_k4_5=OLD.idform_1_k4_5;
if (ada>0) then  
    update kelengkapan set k4_5=1 where idform_1=OLD.idform_1; 
  end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `form_1_k5`
--

DROP TABLE IF EXISTS `form_1_k5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k5` (
  `idform_1_k5` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `jumlah_kk_rt_3` int(11) DEFAULT '0',
  `jumlah_rtlh_versi_bdt_4` int(11) DEFAULT '0',
  `jumlah_rtlh_verifikasi_pemda_5` int(11) DEFAULT '0',
  `sumber_data_6` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`idform_1_k5`)
) ENGINE=InnoDB AUTO_INCREMENT=14873 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `rtlh_insert` AFTER INSERT ON `form_1_k5` FOR EACH ROW BEGIN 
   INSERT INTO rtlh_all set
  idform_1_k5=NEW.idform_1_k5,iddinas=(select iddinas from form_1 where idform_1=NEW.idform_1),idprovinsi=(select idprovinsi from form_1 where idform_1=NEW.idform_1),idkabupaten_kota=NEW.idkabupaten_kota,idkecamatan=NEW.idkecamatan,rtlh=NEW.jumlah_rtlh_versi_bdt_4,rtlh2=NEW.jumlah_rtlh_verifikasi_pemda_5,sumber_data=NEW.sumber_data_6,tahun=(select tahun from form_1 where idform_1=NEW.idform_1);

if (NEW.jumlah_kk_rt_3>-1)or(NEW.jumlah_rtlh_versi_bdt_4>-1)or(NEW.jumlah_rtlh_verifikasi_pemda_5>-1) then
        update kelengkapan set k5=1 where idform_1=NEW.idform_1; 
      end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `rtlh_update` AFTER UPDATE ON `form_1_k5` FOR EACH ROW BEGIN 
   update rtlh_all set
   iddinas=(select iddinas from form_1 where idform_1=NEW.idform_1),idprovinsi=(select idprovinsi from form_1 where idform_1=NEW.idform_1),idkabupaten_kota=NEW.idkabupaten_kota,idkecamatan=NEW.idkecamatan,rtlh=NEW.jumlah_rtlh_versi_bdt_4,rtlh2=NEW.jumlah_rtlh_verifikasi_pemda_5,sumber_data=NEW.sumber_data_6,tahun=(select tahun from form_1 where idform_1=NEW.idform_1) where idform_1_k5=NEW.idform_1_k5;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `rtlh_delete` BEFORE DELETE ON `form_1_k5` FOR EACH ROW BEGIN 
   delete from rtlh_all where idform_1_k5=OLD.idform_1_k5;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `form_1_k5_view`
--

DROP TABLE IF EXISTS `form_1_k5_view`;
/*!50001 DROP VIEW IF EXISTS `form_1_k5_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_1_k5_view` (
  `provinsi` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `idform_1_k5` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `idform_1` tinyint NOT NULL,
  `idkecamatan` tinyint NOT NULL,
  `jumlah_kk_rt_3` tinyint NOT NULL,
  `jumlah_rtlh_versi_bdt_4` tinyint NOT NULL,
  `jumlah_rtlh_verifikasi_pemda_5` tinyint NOT NULL,
  `sumber_data_6` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `kecamatan` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `form_1_k6`
--

DROP TABLE IF EXISTS `form_1_k6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k6` (
  `idform_1_k6` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT '0',
  `idkelurahan` int(11) DEFAULT '0',
  `idkabupaten_kota` int(11) DEFAULT '0',
  `luas_wilayah_kumuh_4` decimal(10,2) DEFAULT '0.00',
  `jumlah_rtlh_dalam_wilayah_kumuh_5` int(11) DEFAULT '0',
  `sumber_data_6` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`idform_1_k6`)
) ENGINE=InnoDB AUTO_INCREMENT=14135 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `kelengkapan_form_1_k6` AFTER INSERT ON `form_1_k6` FOR EACH ROW BEGIN  
      if (NEW.luas_wilayah_kumuh_4>-1)or(NEW.jumlah_rtlh_dalam_wilayah_kumuh_5>-1) then
        update kelengkapan set k6=1 where idform_1=NEW.idform_1; 
      end if;  
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `form_1_k6_kecamatan`
--

DROP TABLE IF EXISTS `form_1_k6_kecamatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_1_k6_kecamatan` (
  `idform_1_k6_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1_k6` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  PRIMARY KEY (`idform_1_k6_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `form_1_k6_kecamatan_view`
--

DROP TABLE IF EXISTS `form_1_k6_kecamatan_view`;
/*!50001 DROP VIEW IF EXISTS `form_1_k6_kecamatan_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_1_k6_kecamatan_view` (
  `idform_1_k6_kecamatan` tinyint NOT NULL,
  `idform_1_k6` tinyint NOT NULL,
  `idkecamatan` tinyint NOT NULL,
  `kecamatan` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_1_k6_view`
--

DROP TABLE IF EXISTS `form_1_k6_view`;
/*!50001 DROP VIEW IF EXISTS `form_1_k6_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_1_k6_view` (
  `idform_1_k6` tinyint NOT NULL,
  `idform_1` tinyint NOT NULL,
  `idkecamatan` tinyint NOT NULL,
  `idkelurahan` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `luas_wilayah_kumuh_4` tinyint NOT NULL,
  `jumlah_rtlh_dalam_wilayah_kumuh_5` tinyint NOT NULL,
  `sumber_data_6` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `kecamatan` tinyint NOT NULL,
  `kelurahan` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_1_k6_view_b`
--

DROP TABLE IF EXISTS `form_1_k6_view_b`;
/*!50001 DROP VIEW IF EXISTS `form_1_k6_view_b`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_1_k6_view_b` (
  `idform_1_k6` tinyint NOT NULL,
  `idform_1` tinyint NOT NULL,
  `idkecamatan` tinyint NOT NULL,
  `idkelurahan` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `luas_wilayah_kumuh_4` tinyint NOT NULL,
  `jumlah_rtlh_dalam_wilayah_kumuh_5` tinyint NOT NULL,
  `sumber_data_6` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `kecamatan` tinyint NOT NULL,
  `kelurahan` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_1_progres`
--

DROP TABLE IF EXISTS `form_1_progres`;
/*!50001 DROP VIEW IF EXISTS `form_1_progres`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_1_progres` (
  `idform_1` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `persen` tinyint NOT NULL,
  `status` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `form_satu_juta_rumah`
--

DROP TABLE IF EXISTS `form_satu_juta_rumah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_satu_juta_rumah` (
  `idform_satu_juta_rumah` int(11) NOT NULL AUTO_INCREMENT,
  `iddinas` int(11) DEFAULT NULL,
  `bulan` varchar(30) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT NULL,
  `nama_kontak` varchar(30) DEFAULT NULL,
  `hp_kontak` varchar(20) DEFAULT NULL,
  `jenis` enum('IMB','Non IMB') DEFAULT 'IMB',
  `status` enum('Draft','Selesai') DEFAULT 'Draft',
  `file_approval` text,
  PRIMARY KEY (`idform_satu_juta_rumah`)
) ENGINE=InnoDB AUTO_INCREMENT=569 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `form_satu_juta_rumah_imb`
--

DROP TABLE IF EXISTS `form_satu_juta_rumah_imb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_satu_juta_rumah_imb` (
  `idform_satu_juta_rumah_imb` int(11) NOT NULL AUTO_INCREMENT,
  `idform_satu_juta_rumah` int(11) DEFAULT NULL,
  `perumahan` varchar(60) DEFAULT NULL,
  `nama_pengembang` varchar(60) DEFAULT NULL,
  `alamat` text,
  `bentuk_rumah` enum('Tapak','Susun','Tapak/Susun') DEFAULT NULL,
  `luas` float DEFAULT '0',
  `tipe` varchar(30) DEFAULT NULL,
  `jumlah_mbr` float DEFAULT '0',
  `jumlah_non_mbr` float DEFAULT '0',
  PRIMARY KEY (`idform_satu_juta_rumah_imb`)
) ENGINE=InnoDB AUTO_INCREMENT=588 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `form_satu_juta_rumah_imb_nonmbr`
--

DROP TABLE IF EXISTS `form_satu_juta_rumah_imb_nonmbr`;
/*!50001 DROP VIEW IF EXISTS `form_satu_juta_rumah_imb_nonmbr`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_satu_juta_rumah_imb_nonmbr` (
  `status` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `idform_satu_juta_rumah` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `nama_dinas` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `total` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_satu_juta_rumah_imb_view_lap`
--

DROP TABLE IF EXISTS `form_satu_juta_rumah_imb_view_lap`;
/*!50001 DROP VIEW IF EXISTS `form_satu_juta_rumah_imb_view_lap`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_satu_juta_rumah_imb_view_lap` (
  `idform_satu_juta_rumah` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `bulan` tinyint NOT NULL,
  `tanggal` tinyint NOT NULL,
  `nama_kontak` tinyint NOT NULL,
  `hp_kontak` tinyint NOT NULL,
  `jenis` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `alamat` tinyint NOT NULL,
  `bentuk_rumah` tinyint NOT NULL,
  `jumlah_mbr` tinyint NOT NULL,
  `jumlah_non_mbr` tinyint NOT NULL,
  `luas` tinyint NOT NULL,
  `nama_pengembang` tinyint NOT NULL,
  `perumahan` tinyint NOT NULL,
  `tipe` tinyint NOT NULL,
  `idkecamatan` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `jumlah_mbr_2` tinyint NOT NULL,
  `jumalh_non_mbr_2` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `form_satu_juta_rumah_non_imb`
--

DROP TABLE IF EXISTS `form_satu_juta_rumah_non_imb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_satu_juta_rumah_non_imb` (
  `idform_satu_juta_rumah_non_imb` int(11) NOT NULL AUTO_INCREMENT,
  `idform_satu_juta_rumah` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `jumlah_mbr` float DEFAULT NULL,
  `jumlah_non_mbr` float DEFAULT NULL,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  PRIMARY KEY (`idform_satu_juta_rumah_non_imb`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `form_satu_juta_rumah_non_imb_view_lap`
--

DROP TABLE IF EXISTS `form_satu_juta_rumah_non_imb_view_lap`;
/*!50001 DROP VIEW IF EXISTS `form_satu_juta_rumah_non_imb_view_lap`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_satu_juta_rumah_non_imb_view_lap` (
  `idform_satu_juta_rumah` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `bulan` tinyint NOT NULL,
  `tanggal` tinyint NOT NULL,
  `nama_kontak` tinyint NOT NULL,
  `hp_kontak` tinyint NOT NULL,
  `jenis` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `jumlah_mbr` tinyint NOT NULL,
  `jumlah_non_mbr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `form_satu_juta_rumah_unit`
--

DROP TABLE IF EXISTS `form_satu_juta_rumah_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_satu_juta_rumah_unit` (
  `idform_satu_juta_rumah_unit` int(11) NOT NULL AUTO_INCREMENT,
  `idform_satu_juta_rumah` int(11) NOT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `jumlah_mbr` float NOT NULL DEFAULT '0',
  `jumlah_non_mbr` float NOT NULL DEFAULT '0',
  `idkabupaten_kota` int(11) DEFAULT NULL,
  PRIMARY KEY (`idform_satu_juta_rumah_unit`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `form_satu_juta_rumah_unit_view`
--

DROP TABLE IF EXISTS `form_satu_juta_rumah_unit_view`;
/*!50001 DROP VIEW IF EXISTS `form_satu_juta_rumah_unit_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_satu_juta_rumah_unit_view` (
  `idform_satu_juta_rumah_unit` tinyint NOT NULL,
  `idform_satu_juta_rumah` tinyint NOT NULL,
  `idkecamatan` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `jumlah_mbr` tinyint NOT NULL,
  `jumlah_non_mbr` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `kecamatan` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `inbox`
--

DROP TABLE IF EXISTS `inbox`;
/*!50001 DROP VIEW IF EXISTS `inbox`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `inbox` (
  `link` tinyint NOT NULL,
  `id` tinyint NOT NULL,
  `Name` tinyint NOT NULL,
  `avatar` tinyint NOT NULL,
  `privilage` tinyint NOT NULL,
  `message` tinyint NOT NULL,
  `waktu` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `kabupaten_kota`
--

DROP TABLE IF EXISTS `kabupaten_kota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kabupaten_kota` (
  `idkabupaten_kota` int(11) NOT NULL AUTO_INCREMENT,
  `idprovinsi` int(11) DEFAULT NULL,
  `iddinas` int(11) DEFAULT NULL,
  `logo_kabkot` varchar(254) NOT NULL,
  `kabupaten_kota` varchar(120) DEFAULT NULL,
  `kode` varchar(5) DEFAULT NULL,
  `kepala_daerah` varchar(80) NOT NULL,
  `foto_kepala_daerah` varchar(254) NOT NULL,
  `wakil_kepala_daerah` varchar(80) NOT NULL,
  `foto_wakil_kepala_daerah` varchar(254) NOT NULL,
  `luas_wilayah` float NOT NULL,
  `luas_wilayah_daratan` float NOT NULL,
  `luas_wilayah_lautan` float NOT NULL,
  `letak_geografis` text NOT NULL,
  `jumlah_penduduk` int(11) NOT NULL,
  `pertumbuhan_penduduk` float NOT NULL,
  `tingkat_kepadatan_penduduk` float NOT NULL,
  `jumlah_penduduk_miskin_kota` int(11) NOT NULL,
  `jumlah_penduduk_miskin_desa` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  PRIMARY KEY (`idkabupaten_kota`)
) ENGINE=InnoDB AUTO_INCREMENT=533 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kecamatan`
--

DROP TABLE IF EXISTS `kecamatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kecamatan` (
  `idkecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `kecamatan` varchar(120) DEFAULT NULL,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  `kode` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idkecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=7178 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kelengkapan`
--

DROP TABLE IF EXISTS `kelengkapan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kelengkapan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT '0',
  `struktur_dinas` int(11) DEFAULT '0',
  `k2_a` int(11) DEFAULT '0',
  `k2_b` int(11) DEFAULT '0',
  `k3` int(11) DEFAULT '0',
  `k4_1` int(11) DEFAULT '0',
  `k4_2` int(11) DEFAULT '0',
  `k4_3` int(11) DEFAULT '0',
  `k4_4` int(11) DEFAULT '0',
  `k4_5` int(11) DEFAULT '0',
  `k5` int(11) NOT NULL DEFAULT '0',
  `k6` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1513 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kelurahan`
--

DROP TABLE IF EXISTS `kelurahan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kelurahan` (
  `idkelurahan` int(11) NOT NULL AUTO_INCREMENT,
  `kelurahan` varchar(120) DEFAULT NULL,
  `kode` varchar(5) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  PRIMARY KEY (`idkelurahan`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_pekerjaan`
--

DROP TABLE IF EXISTS `log_pekerjaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pekerjaan` varchar(120) DEFAULT NULL,
  `iddinas` int(11) DEFAULT NULL,
  `waktu` timestamp NULL DEFAULT NULL,
  `status` float DEFAULT NULL,
  `idpekerjaan` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1566 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `log_pekerjaan_view`
--

DROP TABLE IF EXISTS `log_pekerjaan_view`;
/*!50001 DROP VIEW IF EXISTS `log_pekerjaan_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `log_pekerjaan_view` (
  `id` tinyint NOT NULL,
  `jenis_pekerjaan` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `waktu` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `idpekerjaan` tinyint NOT NULL,
  `nama_dinas` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `idmenu` int(11) NOT NULL AUTO_INCREMENT,
  `idprivilage` int(11) DEFAULT NULL,
  `idparent` int(11) DEFAULT '0',
  `urut` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `Category` varchar(25) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `kind` varchar(200) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `icon` varchar(200) DEFAULT NULL,
  `csubmenu_caret` varchar(10) NOT NULL,
  `notif` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `atribut` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`idmenu`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text,
  `id_user` int(11) DEFAULT NULL,
  `id_user_from` int(11) DEFAULT NULL,
  `status` varchar(60) DEFAULT 'unread',
  `sendtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `link` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pengembang`
--

DROP TABLE IF EXISTS `pengembang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengembang` (
  `idpengembang` int(11) NOT NULL AUTO_INCREMENT,
  `pengembang` varchar(120) DEFAULT NULL,
  `alamat` text,
  `tanggal` timestamp NULL DEFAULT NULL,
  `asosiasi` text,
  `iddinas` int(11) DEFAULT NULL,
  `status` enum('Draft','Disetujui') DEFAULT NULL,
  `bulan` varchar(60) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpengembang`)
) ENGINE=MyISAM AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pengembang_mbr`
--

DROP TABLE IF EXISTS `pengembang_mbr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengembang_mbr` (
  `idpengembang_mbr` int(11) NOT NULL AUTO_INCREMENT,
  `idpengembang` int(11) DEFAULT NULL,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  `nama_perusahaan` varchar(120) DEFAULT NULL,
  `alamat` text,
  `nama_pengembang` varchar(120) DEFAULT NULL,
  `bentuk_rumah` enum('Susun','Tapak') DEFAULT NULL,
  `luas` float DEFAULT NULL,
  `tipe` varchar(20) DEFAULT NULL,
  `rencana` float DEFAULT NULL,
  `realisasi` float DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `jenis` enum('MBR','Non_MBR') DEFAULT NULL,
  PRIMARY KEY (`idpengembang_mbr`)
) ENGINE=MyISAM AUTO_INCREMENT=575 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `pengembang_mbr_view`
--

DROP TABLE IF EXISTS `pengembang_mbr_view`;
/*!50001 DROP VIEW IF EXISTS `pengembang_mbr_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pengembang_mbr_view` (
  `asosiasi` tinyint NOT NULL,
  `pengembang` tinyint NOT NULL,
  `alamat1` tinyint NOT NULL,
  `bulan` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `idpengembang_mbr` tinyint NOT NULL,
  `idpengembang` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `nama_perusahaan` tinyint NOT NULL,
  `alamat` tinyint NOT NULL,
  `nama_pengembang` tinyint NOT NULL,
  `bentuk_rumah` tinyint NOT NULL,
  `luas` tinyint NOT NULL,
  `tipe` tinyint NOT NULL,
  `rencana` tinyint NOT NULL,
  `realisasi` tinyint NOT NULL,
  `harga` tinyint NOT NULL,
  `jenis` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pengembang_view`
--

DROP TABLE IF EXISTS `pengembang_view`;
/*!50001 DROP VIEW IF EXISTS `pengembang_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pengembang_view` (
  `bulan` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `idpengembang` tinyint NOT NULL,
  `pengembang` tinyint NOT NULL,
  `alamat` tinyint NOT NULL,
  `tanggal` tinyint NOT NULL,
  `asosiasi` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `nama_dinas` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `perbankan`
--

DROP TABLE IF EXISTS `perbankan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perbankan` (
  `idperbankan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(120) DEFAULT NULL,
  `alamat` text,
  `tanggal` timestamp NULL DEFAULT NULL,
  `no_kontrak` varchar(120) DEFAULT NULL,
  `iddinas` int(11) DEFAULT NULL,
  `status` enum('Draft','Disetujui') DEFAULT 'Draft',
  `bulan` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`idperbankan`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `perbankan_detail`
--

DROP TABLE IF EXISTS `perbankan_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perbankan_detail` (
  `idperbankan_detail` int(11) NOT NULL AUTO_INCREMENT,
  `idperbankan` int(11) DEFAULT NULL,
  `lokasi` text,
  `pengembang_perorangan` varchar(120) DEFAULT NULL,
  `kpr` varchar(120) DEFAULT NULL,
  `nama_perumahan` varchar(120) DEFAULT NULL,
  `bentuk_rumah` enum('Susun','Tapak') DEFAULT NULL,
  `jenis_rumah` enum('Umum','Komersial') DEFAULT NULL,
  `tipe` enum('LB','LT') DEFAULT NULL,
  `rencana` float DEFAULT NULL,
  `realisasi` float DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `mbr` float DEFAULT NULL,
  `non_mbr` float DEFAULT NULL,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  PRIMARY KEY (`idperbankan_detail`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `perbankan_detail_view`
--

DROP TABLE IF EXISTS `perbankan_detail_view`;
/*!50001 DROP VIEW IF EXISTS `perbankan_detail_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `perbankan_detail_view` (
  `no_kontrak` tinyint NOT NULL,
  `bulan` tinyint NOT NULL,
  `idperbankan_detail` tinyint NOT NULL,
  `idperbankan` tinyint NOT NULL,
  `lokasi` tinyint NOT NULL,
  `pengembang_perorangan` tinyint NOT NULL,
  `kpr` tinyint NOT NULL,
  `nama_perumahan` tinyint NOT NULL,
  `bentuk_rumah` tinyint NOT NULL,
  `jenis_rumah` tinyint NOT NULL,
  `tipe` tinyint NOT NULL,
  `rencana` tinyint NOT NULL,
  `realisasi` tinyint NOT NULL,
  `harga` tinyint NOT NULL,
  `mbr` tinyint NOT NULL,
  `non_mbr` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pesan`
--

DROP TABLE IF EXISTS `pesan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesan` (
  `idpesan` int(11) NOT NULL AUTO_INCREMENT,
  `idjudul` int(11) DEFAULT NULL,
  `pesan` text,
  `waktu` timestamp NULL DEFAULT NULL,
  `status` enum('Dibaca','Dibalas','Baru') DEFAULT 'Baru',
  `status_pesan` enum('in','out') DEFAULT NULL,
  PRIMARY KEY (`idpesan`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pesan_judul`
--

DROP TABLE IF EXISTS `pesan_judul`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesan_judul` (
  `idjudul` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT NULL,
  `iddinas` int(11) DEFAULT NULL,
  PRIMARY KEY (`idjudul`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `peta_kepemilikan_penghunian`
--

DROP TABLE IF EXISTS `peta_kepemilikan_penghunian`;
/*!50001 DROP VIEW IF EXISTS `peta_kepemilikan_penghunian`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `peta_kepemilikan_penghunian` (
  `tahun` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `kepemilikan` tinyint NOT NULL,
  `penghunian` tinyint NOT NULL,
  `x` tinyint NOT NULL,
  `y` tinyint NOT NULL,
  `non_mbr_4` tinyint NOT NULL,
  `mbr_5` tinyint NOT NULL,
  `non_mbr_6` tinyint NOT NULL,
  `mbr_7` tinyint NOT NULL,
  `rtlh` tinyint NOT NULL,
  `totalnon_mbr_4` tinyint NOT NULL,
  `totalmbr_5` tinyint NOT NULL,
  `totalnon_mbr_6` tinyint NOT NULL,
  `totalmbr_7` tinyint NOT NULL,
  `totalrtlh` tinyint NOT NULL,
  `total` tinyint NOT NULL,
  `totalp` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `provinsi`
--

DROP TABLE IF EXISTS `provinsi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinsi` (
  `idprovinsi` int(11) NOT NULL AUTO_INCREMENT,
  `provinsi` varchar(200) DEFAULT NULL,
  `kode_provinsi` int(11) DEFAULT NULL,
  `logo_prov` varchar(254) NOT NULL,
  `gubernur` varchar(120) DEFAULT NULL,
  `foto_gubernur` text,
  `wakil_gubernur` varchar(120) DEFAULT NULL,
  `foto_wakil_gubernur` text,
  `letak_geografis` text,
  `luas_wilayah_daratan` float DEFAULT NULL,
  `luas_wilayah_lautan` float DEFAULT NULL,
  `luas_wilayah` float DEFAULT NULL,
  `jumlah_penduduk` int(11) DEFAULT NULL,
  `pertumbuhan_penduduk` float DEFAULT NULL,
  `tingkat_kepadatan_penduduk` float DEFAULT NULL,
  `jumlah_penduduk_miskin_kota` int(11) DEFAULT NULL,
  `jumlah_penduduk_miskin_desa` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `iddinas` int(11) DEFAULT NULL,
  `x` float DEFAULT '1',
  `y` float DEFAULT '1',
  `z` float DEFAULT '1',
  PRIMARY KEY (`idprovinsi`),
  UNIQUE KEY `idprovinsi` (`idprovinsi`),
  UNIQUE KEY `kode_provinsi` (`kode_provinsi`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `provinsi_eprofil`
--

DROP TABLE IF EXISTS `provinsi_eprofil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinsi_eprofil` (
  `detil_id` int(11) NOT NULL AUTO_INCREMENT,
  `idprovinsi` int(11) NOT NULL,
  `kode_provinsi` int(11) DEFAULT NULL,
  `nama_singkat` varchar(50) NOT NULL,
  `id_eprofil` int(11) NOT NULL,
  PRIMARY KEY (`detil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `provinsi_singkat`
--

DROP TABLE IF EXISTS `provinsi_singkat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinsi_singkat` (
  `detil_id` int(11) NOT NULL AUTO_INCREMENT,
  `idprovinsi` int(11) NOT NULL DEFAULT '0',
  `kode_provinsi` int(11) DEFAULT NULL,
  `provinsi` varchar(200) DEFAULT NULL,
  `nama_singkat` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`detil_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `rtlh`
--

DROP TABLE IF EXISTS `rtlh`;
/*!50001 DROP VIEW IF EXISTS `rtlh`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `rtlh` (
  `kode_provinsi` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `rtlh` tinyint NOT NULL,
  `sumber_data_6` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `rtlh_all`
--

DROP TABLE IF EXISTS `rtlh_all`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rtlh_all` (
  `id_trlh_provinsi` int(11) NOT NULL AUTO_INCREMENT,
  `idprovinsi` int(11) DEFAULT NULL,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  `rtlh` float DEFAULT NULL,
  `sumber_data` varchar(120) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `idkecamatan` int(11) DEFAULT NULL,
  `iddinas` int(11) DEFAULT NULL,
  `idform_1_k5` int(11) DEFAULT NULL,
  `rtlh2` float DEFAULT NULL,
  PRIMARY KEY (`id_trlh_provinsi`)
) ENGINE=MyISAM AUTO_INCREMENT=14626 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `rtlh_kabupaten_kota`
--

DROP TABLE IF EXISTS `rtlh_kabupaten_kota`;
/*!50001 DROP VIEW IF EXISTS `rtlh_kabupaten_kota`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `rtlh_kabupaten_kota` (
  `kode` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `rtlh` tinyint NOT NULL,
  `sumber_data_6` tinyint NOT NULL,
  `tahun` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `rtlh_kecamatan`
--

DROP TABLE IF EXISTS `rtlh_kecamatan`;
/*!50001 DROP VIEW IF EXISTS `rtlh_kecamatan`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `rtlh_kecamatan` (
  `tahun` tinyint NOT NULL,
  `idform_1` tinyint NOT NULL,
  `idform_1_k5` tinyint NOT NULL,
  `idkecamatan` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kecamatan` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `rtlh_bdt` tinyint NOT NULL,
  `rtlh_pemda` tinyint NOT NULL,
  `sumber_data_6` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `satu_juta_rumah_imb`
--

DROP TABLE IF EXISTS `satu_juta_rumah_imb`;
/*!50001 DROP VIEW IF EXISTS `satu_juta_rumah_imb`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `satu_juta_rumah_imb` (
  `status` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `idform_satu_juta_rumah` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `nama_dinas` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `total` tinyint NOT NULL,
  `total2` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `satu_juta_rumah_imb_nonmbr`
--

DROP TABLE IF EXISTS `satu_juta_rumah_imb_nonmbr`;
/*!50001 DROP VIEW IF EXISTS `satu_juta_rumah_imb_nonmbr`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `satu_juta_rumah_imb_nonmbr` (
  `status` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `idform_satu_juta_rumah` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `nama_dinas` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `total` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `sejuta_rumah_imb`
--

DROP TABLE IF EXISTS `sejuta_rumah_imb`;
/*!50001 DROP VIEW IF EXISTS `sejuta_rumah_imb`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `sejuta_rumah_imb` (
  `idprovinsi` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `mbr` tinyint NOT NULL,
  `non_mbr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `sejuta_rumah_imb_view`
--

DROP TABLE IF EXISTS `sejuta_rumah_imb_view`;
/*!50001 DROP VIEW IF EXISTS `sejuta_rumah_imb_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `sejuta_rumah_imb_view` (
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `mbr` tinyint NOT NULL,
  `non_mbr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `status_kepemilikan_rumah`
--

DROP TABLE IF EXISTS `status_kepemilikan_rumah`;
/*!50001 DROP VIEW IF EXISTS `status_kepemilikan_rumah`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `status_kepemilikan_rumah` (
  `kode_provinsi` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `jumlah_kepala_keluarga` tinyint NOT NULL,
  `milik_sendiri` tinyint NOT NULL,
  `kontrak_sewa` tinyint NOT NULL,
  `menumpang` tinyint NOT NULL,
  `dinas` tinyint NOT NULL,
  `lainnya` tinyint NOT NULL,
  `kepemilikan` tinyint NOT NULL,
  `penghunian` tinyint NOT NULL,
  `sumber_data` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `status_kepemilikan_rumah_backlog`
--

DROP TABLE IF EXISTS `status_kepemilikan_rumah_backlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_kepemilikan_rumah_backlog` (
  `idstatus_kepemilikan_rumah` int(11) NOT NULL AUTO_INCREMENT,
  `idform_1` int(11) DEFAULT NULL,
  `idform_1_k4_1` int(11) DEFAULT NULL,
  `idform_1_k4_3` int(11) DEFAULT NULL,
  `jumlah_kk` float DEFAULT NULL,
  `milik_sendiri` float DEFAULT NULL,
  `kontrak_sewa` float DEFAULT NULL,
  `bebas_sewa` float DEFAULT NULL,
  `dinas` float DEFAULT NULL,
  `lainnya` float DEFAULT NULL,
  `iddinas` int(11) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  `kepemilikan` float DEFAULT '0',
  `penghunian` float DEFAULT '0',
  `tahun` int(11) DEFAULT NULL,
  PRIMARY KEY (`idstatus_kepemilikan_rumah`)
) ENGINE=MyISAM AUTO_INCREMENT=1552 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `status_kepemilikan_rumah_kabupaten_kota`
--

DROP TABLE IF EXISTS `status_kepemilikan_rumah_kabupaten_kota`;
/*!50001 DROP VIEW IF EXISTS `status_kepemilikan_rumah_kabupaten_kota`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `status_kepemilikan_rumah_kabupaten_kota` (
  `kabupaten_kota` tinyint NOT NULL,
  `x` tinyint NOT NULL,
  `y` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `idform_1` tinyint NOT NULL,
  `tahun` tinyint NOT NULL,
  `jumlah_kepala_keluarga` tinyint NOT NULL,
  `milik_sendiri` tinyint NOT NULL,
  `kontrak_sewa` tinyint NOT NULL,
  `menumpang` tinyint NOT NULL,
  `dinas` tinyint NOT NULL,
  `lainnya` tinyint NOT NULL,
  `kepemilikan` tinyint NOT NULL,
  `penghunian` tinyint NOT NULL,
  `sumber_data` tinyint NOT NULL,
  `non_mbr_4` tinyint NOT NULL,
  `mbr_5` tinyint NOT NULL,
  `rtlh` tinyint NOT NULL,
  `non_mbr_6` tinyint NOT NULL,
  `mbr_7` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `statusform`
--

DROP TABLE IF EXISTS `statusform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statusform` (
  `idstatusform` int(11) NOT NULL AUTO_INCREMENT,
  `idprovinsi` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `form1a` int(11) DEFAULT '0',
  `form1b` int(11) DEFAULT '0',
  PRIMARY KEY (`idstatusform`)
) ENGINE=MyISAM AUTO_INCREMENT=8333 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statusform_detail`
--

DROP TABLE IF EXISTS `statusform_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statusform_detail` (
  `idstatusform_detail` int(11) NOT NULL AUTO_INCREMENT,
  `idprovinsi` int(11) DEFAULT NULL,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  PRIMARY KEY (`idstatusform_detail`)
) ENGINE=MyISAM AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statusform_kab`
--

DROP TABLE IF EXISTS `statusform_kab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statusform_kab` (
  `idstatusform_kab` int(11) NOT NULL AUTO_INCREMENT,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `form1b` int(11) DEFAULT '0',
  PRIMARY KEY (`idstatusform_kab`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_task_kind` mediumint(9) DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  `date_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `date_expired` datetime DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task_kind`
--

DROP TABLE IF EXISTS `task_kind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_kind` (
  `id_task_kind` int(11) NOT NULL AUTO_INCREMENT,
  `kind` varchar(120) DEFAULT NULL,
  `link` text,
  `icon` varchar(120) DEFAULT NULL,
  `class` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id_task_kind`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `task_notification`
--

DROP TABLE IF EXISTS `task_notification`;
/*!50001 DROP VIEW IF EXISTS `task_notification`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `task_notification` (
  `kind` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `date_create` tinyint NOT NULL,
  `date_expired` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `icon` tinyint NOT NULL,
  `class` tinyint NOT NULL,
  `link` tinyint NOT NULL,
  `waktu` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) DEFAULT NULL,
  `avatar` text,
  `id_privilage` int(11) DEFAULT NULL,
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `iddinas` int(11) DEFAULT NULL,
  `idprivilage` int(11) DEFAULT NULL,
  `nama_dinas` varchar(120) DEFAULT 'Pusat',
  `provinsi` varchar(120) DEFAULT NULL,
  `idprovinsi` int(11) DEFAULT NULL,
  `alamat` varchar(120) DEFAULT NULL,
  `kabupaten_kota` varchar(120) DEFAULT NULL,
  `idkabupaten_kota` int(11) DEFAULT NULL,
  `logo_pusat` text,
  `isLoggedIn` enum('true','false') DEFAULT 'false',
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `user_admin`
--

DROP TABLE IF EXISTS `user_admin`;
/*!50001 DROP VIEW IF EXISTS `user_admin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_admin` (
  `isLoggedIn` tinyint NOT NULL,
  `id` tinyint NOT NULL,
  `Name` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `logo_pusat` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `user_kabupaten_kota`
--

DROP TABLE IF EXISTS `user_kabupaten_kota`;
/*!50001 DROP VIEW IF EXISTS `user_kabupaten_kota`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_kabupaten_kota` (
  `isLoggedIn` tinyint NOT NULL,
  `login_count` tinyint NOT NULL,
  `x` tinyint NOT NULL,
  `y` tinyint NOT NULL,
  `z` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `nama_dinas` tinyint NOT NULL,
  `alamat` tinyint NOT NULL,
  `password` tinyint NOT NULL,
  `telepon` tinyint NOT NULL,
  `fax` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `URL` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `kode` tinyint NOT NULL,
  `logo_kabkot` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `kepala_daerah` tinyint NOT NULL,
  `wakil_kepala_daerah` tinyint NOT NULL,
  `letak_geografis` tinyint NOT NULL,
  `luas_wilayah` tinyint NOT NULL,
  `luas_wilayah_daratan` tinyint NOT NULL,
  `luas_wilayah_lautan` tinyint NOT NULL,
  `jumlah_penduduk` tinyint NOT NULL,
  `pertumbuhan_penduduk` tinyint NOT NULL,
  `tingkat_kepadatan_penduduk` tinyint NOT NULL,
  `jumlah_penduduk_miskin_kota` tinyint NOT NULL,
  `jumlah_penduduk_miskin_desa` tinyint NOT NULL,
  `kecamatan` tinyint NOT NULL,
  `kelurahan` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `idprivilage` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_privilage`
--

DROP TABLE IF EXISTS `user_privilage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_privilage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `privilage` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `user_provinsi`
--

DROP TABLE IF EXISTS `user_provinsi`;
/*!50001 DROP VIEW IF EXISTS `user_provinsi`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_provinsi` (
  `isLoggedIn` tinyint NOT NULL,
  `login_count` tinyint NOT NULL,
  `x` tinyint NOT NULL,
  `y` tinyint NOT NULL,
  `z` tinyint NOT NULL,
  `idkabupaten_kota` tinyint NOT NULL,
  `alamat` tinyint NOT NULL,
  `iddinas` tinyint NOT NULL,
  `nama_dinas` tinyint NOT NULL,
  `password` tinyint NOT NULL,
  `telepon` tinyint NOT NULL,
  `fax` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `URL` tinyint NOT NULL,
  `kabupaten_kota` tinyint NOT NULL,
  `kecamatan` tinyint NOT NULL,
  `kelurahan` tinyint NOT NULL,
  `idprivilage` tinyint NOT NULL,
  `idprovinsi` tinyint NOT NULL,
  `provinsi` tinyint NOT NULL,
  `kode_provinsi` tinyint NOT NULL,
  `logo_prov` tinyint NOT NULL,
  `gubernur` tinyint NOT NULL,
  `foto_gubernur` tinyint NOT NULL,
  `wakil_gubernur` tinyint NOT NULL,
  `foto_wakil_gubernur` tinyint NOT NULL,
  `letak_geografis` tinyint NOT NULL,
  `luas_wilayah_daratan` tinyint NOT NULL,
  `luas_wilayah_lautan` tinyint NOT NULL,
  `luas_wilayah` tinyint NOT NULL,
  `jumlah_penduduk` tinyint NOT NULL,
  `pertumbuhan_penduduk` tinyint NOT NULL,
  `tingkat_kepadatan_penduduk` tinyint NOT NULL,
  `jumlah_penduduk_miskin_kota` tinyint NOT NULL,
  `jumlah_penduduk_miskin_desa` tinyint NOT NULL,
  `iduser` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `backlog_kepemilikan_dan_penghunian`
--

/*!50001 DROP TABLE IF EXISTS `backlog_kepemilikan_dan_penghunian`*/;
/*!50001 DROP VIEW IF EXISTS `backlog_kepemilikan_dan_penghunian`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `backlog_kepemilikan_dan_penghunian` AS select (select `f1`.`tanggal_buat` from `form_1` `f1` where ((`f1`.`idprovinsi` = `p`.`idprovinsi`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `tanggal`,`p`.`idprovinsi` AS `idprovinsi`,`p`.`kode_provinsi` AS `kode_provinsi`,`p`.`provinsi` AS `provinsi`,(select `fk_5`.`jumlah_rtlh_verifikasi_pemda_5` from (`form_1_k5` `fk_5` join `form_1` `f1` on((`f1`.`idform_1` = `fk_5`.`idform_1`))) where ((`f1`.`idprovinsi` = `p`.`idprovinsi`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `rtlh`,(select `f1`.`tahun` from (`form_1_k5` `fk_5` join `form_1` `f1` on((`f1`.`idform_1` = `fk_5`.`idform_1`))) where ((`f1`.`idprovinsi` = `p`.`idprovinsi`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `tahun`,(select `fk5`.`non_mbr_4` from (`form_1_k4_5` `fk5` join `form_1` `f1` on((`f1`.`idform_1` = `fk5`.`idform_1`))) where ((`f1`.`idprovinsi` = `p`.`idprovinsi`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `non_mbr_4`,(select `fk5`.`non_mbr_6` from (`form_1_k4_5` `fk5` join `form_1` `f1` on((`f1`.`idform_1` = `fk5`.`idform_1`))) where ((`f1`.`idprovinsi` = `p`.`idprovinsi`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `non_mbr_6`,(select `fk5`.`mbr_5` from (`form_1_k4_5` `fk5` join `form_1` `f1` on((`f1`.`idform_1` = `fk5`.`idform_1`))) where ((`f1`.`idprovinsi` = `p`.`idprovinsi`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `mbr_5`,(select `fk5`.`mbr_7` from (`form_1_k4_5` `fk5` join `form_1` `f1` on((`f1`.`idform_1` = `fk5`.`idform_1`))) where ((`f1`.`idprovinsi` = `p`.`idprovinsi`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `mbr_7`,(select (((`fk4`.`k4142` + `fk4`.`k4143`) + `fk4`.`k4144`) + `fk4`.`k4145`) from (`form_1_k4_1` `fk4` join `form_1` `f1` on((`f1`.`idform_1` = `fk4`.`idform_1`))) where ((`f1`.`idprovinsi` = `p`.`idprovinsi`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `kepemilikan`,(select `fk4`.`k4143` from (`form_1_k4_1` `fk4` join `form_1` `f1` on((`f1`.`idform_1` = `fk4`.`idform_1`))) where ((`f1`.`idprovinsi` = `p`.`idprovinsi`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `penghunian` from `provinsi` `p` order by `p`.`kode_provinsi` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `backlog_kepemilikan_dan_penghunian_kab`
--

/*!50001 DROP TABLE IF EXISTS `backlog_kepemilikan_dan_penghunian_kab`*/;
/*!50001 DROP VIEW IF EXISTS `backlog_kepemilikan_dan_penghunian_kab`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `backlog_kepemilikan_dan_penghunian_kab` AS select (select `f1`.`tanggal_buat` from `form_1` `f1` where ((`f1`.`iddinas` = `d`.`iddinas`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `tanggal`,(select `fk_5`.`jumlah_rtlh_verifikasi_pemda_5` from (`form_1_k5` `fk_5` join `form_1` `f1` on((`f1`.`idform_1` = `fk_5`.`idform_1`))) where ((`fk_5`.`idkabupaten_kota` = `d`.`idkabupaten_kota`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `rtlh`,(select `f1`.`tahun` from (`form_1_k5` `fk_5` join `form_1` `f1` on((`f1`.`idform_1` = `fk_5`.`idform_1`))) where ((`fk_5`.`idkabupaten_kota` = `d`.`idkabupaten_kota`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `tahun`,`d`.`idprovinsi` AS `idprovinsi`,`d`.`idkabupaten_kota` AS `idkabupaten_kota`,`d`.`kabupaten_kota` AS `kabupaten_kota`,(select (((`fk4`.`k4142` + `fk4`.`k4143`) + `fk4`.`k4144`) + `fk4`.`k4145`) from (`form_1_k4_1` `fk4` left join `form_1` `f1` on((`f1`.`idform_1` = `fk4`.`idform_1`))) where ((`f1`.`iddinas` = `d`.`iddinas`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `kepemilikan`,(select `fk4`.`k4143` from (`form_1_k4_1` `fk4` left join `form_1` `f1` on((`f1`.`idform_1` = `fk4`.`idform_1`))) where ((`f1`.`iddinas` = `d`.`iddinas`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `penghunian`,(select `fk5`.`non_mbr_4` from (`form_1_k4_5` `fk5` left join `form_1` `f1` on((`f1`.`idform_1` = `fk5`.`idform_1`))) where ((`f1`.`iddinas` = `d`.`iddinas`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `non_mbr_4`,(select `fk5`.`non_mbr_6` from (`form_1_k4_5` `fk5` left join `form_1` `f1` on((`f1`.`idform_1` = `fk5`.`idform_1`))) where ((`f1`.`iddinas` = `d`.`iddinas`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `non_mbr_6`,(select `fk5`.`mbr_5` from (`form_1_k4_5` `fk5` left join `form_1` `f1` on((`f1`.`idform_1` = `fk5`.`idform_1`))) where ((`f1`.`iddinas` = `d`.`iddinas`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `mbr_5`,(select `fk5`.`mbr_7` from (`form_1_k4_5` `fk5` left join `form_1` `f1` on((`f1`.`idform_1` = `fk5`.`idform_1`))) where ((`f1`.`iddinas` = `d`.`iddinas`) and (`f1`.`status` = 'Disetujui')) limit 1) AS `mbr_7` from `user_kabupaten_kota` `d` order by `d`.`iddinas` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `backlog_kepemilikan_dan_penghunian_kab_view`
--

/*!50001 DROP TABLE IF EXISTS `backlog_kepemilikan_dan_penghunian_kab_view`*/;
/*!50001 DROP VIEW IF EXISTS `backlog_kepemilikan_dan_penghunian_kab_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `backlog_kepemilikan_dan_penghunian_kab_view` AS select `backlog_kepemilikan_dan_penghunian_kab`.`tanggal` AS `tanggal`,`backlog_kepemilikan_dan_penghunian_kab`.`tahun` AS `tahun`,`backlog_kepemilikan_dan_penghunian_kab`.`rtlh` AS `rtlh`,`backlog_kepemilikan_dan_penghunian_kab`.`idprovinsi` AS `idprovinsi`,`backlog_kepemilikan_dan_penghunian_kab`.`idkabupaten_kota` AS `idkabupaten_kota`,`backlog_kepemilikan_dan_penghunian_kab`.`kabupaten_kota` AS `kabupaten_kota`,ifnull(`backlog_kepemilikan_dan_penghunian_kab`.`kepemilikan`,0) AS `kepemilikan`,ifnull(`backlog_kepemilikan_dan_penghunian_kab`.`penghunian`,0) AS `penghunian`,ifnull((select max(`b`.`kepemilikan`) AS `mak` from `backlog_kepemilikan_dan_penghunian_kab` `b` where (`b`.`idprovinsi` = `backlog_kepemilikan_dan_penghunian_kab`.`idprovinsi`)),0) AS `mak1`,ifnull((select max(`d`.`penghunian`) AS `mak` from `backlog_kepemilikan_dan_penghunian_kab` `d` where (`d`.`idprovinsi` = `backlog_kepemilikan_dan_penghunian_kab`.`idprovinsi`)),0) AS `mak2`,ifnull(`backlog_kepemilikan_dan_penghunian_kab`.`non_mbr_4`,0) AS `non_mbr_4`,ifnull(`backlog_kepemilikan_dan_penghunian_kab`.`mbr_5`,0) AS `mbr_5`,ifnull(`backlog_kepemilikan_dan_penghunian_kab`.`non_mbr_6`,0) AS `non_mbr_6`,ifnull(`backlog_kepemilikan_dan_penghunian_kab`.`mbr_7`,0) AS `mbr_7`,ifnull((select max(`e`.`non_mbr_4`) from `backlog_kepemilikan_dan_penghunian_kab` `e` where (`e`.`idprovinsi` = `backlog_kepemilikan_dan_penghunian_kab`.`idprovinsi`)),0) AS `mak4`,ifnull((select max(`f`.`mbr_5`) from `backlog_kepemilikan_dan_penghunian_kab` `f` where (`f`.`idprovinsi` = `backlog_kepemilikan_dan_penghunian_kab`.`idprovinsi`)),0) AS `mak5`,ifnull((select max(`g`.`non_mbr_6`) from `backlog_kepemilikan_dan_penghunian_kab` `g` where (`g`.`idprovinsi` = `backlog_kepemilikan_dan_penghunian_kab`.`idprovinsi`)),0) AS `mak6`,ifnull((select max(`h`.`mbr_7`) from `backlog_kepemilikan_dan_penghunian_kab` `h` where (`h`.`idprovinsi` = `backlog_kepemilikan_dan_penghunian_kab`.`idprovinsi`)),0) AS `mak7`,ifnull((select max(`h`.`rtlh`) from `backlog_kepemilikan_dan_penghunian_kab` `h` where (`h`.`idprovinsi` = `backlog_kepemilikan_dan_penghunian_kab`.`idprovinsi`)),0) AS `totalrtlh` from `backlog_kepemilikan_dan_penghunian_kab` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `backlog_kepemilikan_dan_penghunian_view`
--

/*!50001 DROP TABLE IF EXISTS `backlog_kepemilikan_dan_penghunian_view`*/;
/*!50001 DROP VIEW IF EXISTS `backlog_kepemilikan_dan_penghunian_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `backlog_kepemilikan_dan_penghunian_view` AS select `backlog_kepemilikan_dan_penghunian`.`tanggal` AS `tanggal`,`backlog_kepemilikan_dan_penghunian`.`tahun` AS `tahun`,`backlog_kepemilikan_dan_penghunian`.`idprovinsi` AS `idprovinsi`,`backlog_kepemilikan_dan_penghunian`.`kode_provinsi` AS `kode_provinsi`,`backlog_kepemilikan_dan_penghunian`.`provinsi` AS `provinsi`,ifnull(`backlog_kepemilikan_dan_penghunian`.`kepemilikan`,0) AS `kepemilikan`,ifnull(`backlog_kepemilikan_dan_penghunian`.`penghunian`,0) AS `penghunian`,ifnull(`backlog_kepemilikan_dan_penghunian`.`non_mbr_4`,0) AS `non_mbr_4`,ifnull(`backlog_kepemilikan_dan_penghunian`.`mbr_5`,0) AS `mbr_5`,ifnull(`backlog_kepemilikan_dan_penghunian`.`non_mbr_6`,0) AS `non_mbr_6`,ifnull(`backlog_kepemilikan_dan_penghunian`.`rtlh`,0) AS `rtlh`,ifnull(`backlog_kepemilikan_dan_penghunian`.`mbr_7`,0) AS `mbr_7`,ifnull((select max(`b`.`kepemilikan`) AS `mak` from `backlog_kepemilikan_dan_penghunian` `b`),0) AS `mak1`,ifnull((select max(`c`.`penghunian`) AS `mak` from `backlog_kepemilikan_dan_penghunian` `c`),0) AS `mak2`,ifnull((select max(`c`.`non_mbr_4`) AS `mak` from `backlog_kepemilikan_dan_penghunian` `c`),0) AS `mak4`,ifnull((select max(`c`.`mbr_5`) AS `mak` from `backlog_kepemilikan_dan_penghunian` `c`),0) AS `mak5`,ifnull((select max(`c`.`non_mbr_6`) AS `mak` from `backlog_kepemilikan_dan_penghunian` `c`),0) AS `mak6`,ifnull((select max(`c`.`mbr_7`) AS `mak` from `backlog_kepemilikan_dan_penghunian` `c`),0) AS `mak7`,ifnull((select max(`c`.`rtlh`) AS `mak` from `backlog_kepemilikan_dan_penghunian` `c`),0) AS `totalrtlh` from `backlog_kepemilikan_dan_penghunian` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_1_k4_4_view`
--

/*!50001 DROP TABLE IF EXISTS `form_1_k4_4_view`*/;
/*!50001 DROP VIEW IF EXISTS `form_1_k4_4_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_1_k4_4_view` AS select `f1k44`.`idform_1_k4_4` AS `idform_1_k4_4`,`f1k44`.`idform_1` AS `idform_1`,`f1k44`.`idkabupaten_kota` AS `idkabupaten_kota`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,`f1k44`.`jumlah_rumah_3` AS `jumlah_rumah_3`,`f1k44`.`jumlah_rumah_4` AS `jumlah_rumah_4`,`f1k44`.`sumber_data_5` AS `sumber_data_5`,`f1k44`.`idkelurahan` AS `idkelurahan`,`kl`.`kelurahan` AS `kelurahan`,`f1k44`.`idkecamatan` AS `idkecamatan`,`kc`.`kecamatan` AS `kecamatan` from (((`form_1_k4_4` `f1k44` left join `kelurahan` `kl` on((`kl`.`idkelurahan` = `f1k44`.`idkelurahan`))) left join `kabupaten_kota` `kk` on((`kk`.`idkabupaten_kota` = `f1k44`.`idkabupaten_kota`))) left join `kecamatan` `kc` on((`kc`.`idkecamatan` = `f1k44`.`idkecamatan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_1_k5_view`
--

/*!50001 DROP TABLE IF EXISTS `form_1_k5_view`*/;
/*!50001 DROP VIEW IF EXISTS `form_1_k5_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_1_k5_view` AS select `p`.`provinsi` AS `provinsi`,`f1`.`tahun` AS `tahun`,`f1`.`idprovinsi` AS `idprovinsi`,`f`.`idform_1_k5` AS `idform_1_k5`,`f`.`idkabupaten_kota` AS `idkabupaten_kota`,`f`.`idform_1` AS `idform_1`,`f`.`idkecamatan` AS `idkecamatan`,`f`.`jumlah_kk_rt_3` AS `jumlah_kk_rt_3`,`f`.`jumlah_rtlh_versi_bdt_4` AS `jumlah_rtlh_versi_bdt_4`,`f`.`jumlah_rtlh_verifikasi_pemda_5` AS `jumlah_rtlh_verifikasi_pemda_5`,`f`.`sumber_data_6` AS `sumber_data_6`,`k`.`kabupaten_kota` AS `kabupaten_kota`,`kc`.`kecamatan` AS `kecamatan` from ((((`form_1_k5` `f` left join `kabupaten_kota` `k` on((`k`.`idkabupaten_kota` = `f`.`idkabupaten_kota`))) left join `kecamatan` `kc` on((`kc`.`idkecamatan` = `f`.`idkecamatan`))) join `form_1` `f1` on((`f1`.`idform_1` = `f`.`idform_1`))) left join `provinsi` `p` on((`p`.`idprovinsi` = `f1`.`idprovinsi`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_1_k6_kecamatan_view`
--

/*!50001 DROP TABLE IF EXISTS `form_1_k6_kecamatan_view`*/;
/*!50001 DROP VIEW IF EXISTS `form_1_k6_kecamatan_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_1_k6_kecamatan_view` AS select `f6`.`idform_1_k6_kecamatan` AS `idform_1_k6_kecamatan`,`f6`.`idform_1_k6` AS `idform_1_k6`,`f6`.`idkecamatan` AS `idkecamatan`,`k`.`kecamatan` AS `kecamatan` from (`form_1_k6_kecamatan` `f6` join `kecamatan` `k` on((`k`.`idkecamatan` = `f6`.`idkecamatan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_1_k6_view`
--

/*!50001 DROP TABLE IF EXISTS `form_1_k6_view`*/;
/*!50001 DROP VIEW IF EXISTS `form_1_k6_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_1_k6_view` AS select `f`.`idform_1_k6` AS `idform_1_k6`,`f`.`idform_1` AS `idform_1`,`f`.`idkecamatan` AS `idkecamatan`,`f`.`idkelurahan` AS `idkelurahan`,`f`.`idkabupaten_kota` AS `idkabupaten_kota`,`f`.`luas_wilayah_kumuh_4` AS `luas_wilayah_kumuh_4`,`f`.`jumlah_rtlh_dalam_wilayah_kumuh_5` AS `jumlah_rtlh_dalam_wilayah_kumuh_5`,`f`.`sumber_data_6` AS `sumber_data_6`,`k`.`kabupaten_kota` AS `kabupaten_kota`,`kc`.`kecamatan` AS `kecamatan`,`kl`.`kelurahan` AS `kelurahan` from (((`form_1_k6` `f` join `kabupaten_kota` `k` on((`k`.`idkabupaten_kota` = `f`.`idkabupaten_kota`))) left join `kecamatan` `kc` on((`kc`.`idkecamatan` = `f`.`idkecamatan`))) left join `kelurahan` `kl` on((`kl`.`idkelurahan` = `f`.`idkelurahan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_1_k6_view_b`
--

/*!50001 DROP TABLE IF EXISTS `form_1_k6_view_b`*/;
/*!50001 DROP VIEW IF EXISTS `form_1_k6_view_b`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_1_k6_view_b` AS select `f`.`idform_1_k6` AS `idform_1_k6`,`f`.`idform_1` AS `idform_1`,`f`.`idkecamatan` AS `idkecamatan`,`f`.`idkelurahan` AS `idkelurahan`,`f`.`idkabupaten_kota` AS `idkabupaten_kota`,`f`.`luas_wilayah_kumuh_4` AS `luas_wilayah_kumuh_4`,`f`.`jumlah_rtlh_dalam_wilayah_kumuh_5` AS `jumlah_rtlh_dalam_wilayah_kumuh_5`,`f`.`sumber_data_6` AS `sumber_data_6`,`k`.`kabupaten_kota` AS `kabupaten_kota`,`kc`.`kecamatan` AS `kecamatan`,`kl`.`kelurahan` AS `kelurahan` from (((`form_1_k6` `f` left join `kabupaten_kota` `k` on((`k`.`idkabupaten_kota` = `f`.`idkabupaten_kota`))) left join `kecamatan` `kc` on((`kc`.`idkecamatan` = `f`.`idkecamatan`))) left join `kelurahan` `kl` on((`kl`.`idkelurahan` = `f`.`idkelurahan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_1_progres`
--

/*!50001 DROP TABLE IF EXISTS `form_1_progres`*/;
/*!50001 DROP VIEW IF EXISTS `form_1_progres`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_1_progres` AS select `form_1`.`idform_1` AS `idform_1`,date_format(`form_1`.`tanggal_buat`,'%Y') AS `tahun`,`form_1`.`iddinas` AS `iddinas`,(case `form_1`.`status` when 'Draft' then '25%' when 'Belum Disetujui' then '50%' when 'Butuh Revisi' then '75%' else '100%' end) AS `persen`,`form_1`.`status` AS `status` from `form_1` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_satu_juta_rumah_imb_nonmbr`
--

/*!50001 DROP TABLE IF EXISTS `form_satu_juta_rumah_imb_nonmbr`*/;
/*!50001 DROP VIEW IF EXISTS `form_satu_juta_rumah_imb_nonmbr`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_satu_juta_rumah_imb_nonmbr` AS select `a`.`status` AS `status`,year(`a`.`tanggal`) AS `tahun`,`a`.`idform_satu_juta_rumah` AS `idform_satu_juta_rumah`,`a`.`iddinas` AS `iddinas`,`d`.`nama_dinas` AS `nama_dinas`,`p`.`provinsi` AS `provinsi`,`p`.`idprovinsi` AS `idprovinsi`,`kk`.`idkabupaten_kota` AS `idkabupaten_kota`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,((sum(ifnull(`b`.`jumlah_non_mbr`,0)) + sum(ifnull(`c`.`jumlah_non_mbr`,0))) + sum(ifnull(`f`.`jumlah_non_mbr`,0))) AS `total` from ((((((`form_satu_juta_rumah` `a` left join `form_satu_juta_rumah_imb` `b` on((`b`.`idform_satu_juta_rumah` = `a`.`idform_satu_juta_rumah`))) left join `form_satu_juta_rumah_unit` `c` on((`c`.`idform_satu_juta_rumah` = `a`.`idform_satu_juta_rumah`))) left join `form_satu_juta_rumah_non_imb` `f` on((`f`.`idform_satu_juta_rumah` = `a`.`idform_satu_juta_rumah`))) join `dinas` `d` on((`d`.`iddinas` = `a`.`iddinas`))) join `kabupaten_kota` `kk` on((`kk`.`iddinas` = `d`.`iddinas`))) join `provinsi` `p` on((`p`.`idprovinsi` = `kk`.`idprovinsi`))) group by `d`.`iddinas` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_satu_juta_rumah_imb_view_lap`
--

/*!50001 DROP TABLE IF EXISTS `form_satu_juta_rumah_imb_view_lap`*/;
/*!50001 DROP VIEW IF EXISTS `form_satu_juta_rumah_imb_view_lap`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_satu_juta_rumah_imb_view_lap` AS select `f1`.`idform_satu_juta_rumah` AS `idform_satu_juta_rumah`,`f1`.`iddinas` AS `iddinas`,`f1`.`bulan` AS `bulan`,`f1`.`tanggal` AS `tanggal`,`f1`.`nama_kontak` AS `nama_kontak`,`f1`.`hp_kontak` AS `hp_kontak`,`f1`.`jenis` AS `jenis`,`f1`.`status` AS `status`,`f2`.`alamat` AS `alamat`,`f2`.`bentuk_rumah` AS `bentuk_rumah`,`f2`.`jumlah_mbr` AS `jumlah_mbr`,`f2`.`jumlah_non_mbr` AS `jumlah_non_mbr`,`f2`.`luas` AS `luas`,`f2`.`nama_pengembang` AS `nama_pengembang`,`f2`.`perumahan` AS `perumahan`,`f2`.`tipe` AS `tipe`,`k`.`idkecamatan` AS `idkecamatan`,`f3`.`idkabupaten_kota` AS `idkabupaten_kota`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,`f3`.`jumlah_mbr` AS `jumlah_mbr_2`,`f3`.`jumlah_non_mbr` AS `jumalh_non_mbr_2` from ((((`form_satu_juta_rumah` `f1` join `form_satu_juta_rumah_imb` `f2` on((`f2`.`idform_satu_juta_rumah` = `f1`.`idform_satu_juta_rumah`))) left join `form_satu_juta_rumah_unit` `f3` on((`f3`.`idform_satu_juta_rumah` = `f1`.`idform_satu_juta_rumah`))) left join `kecamatan` `k` on((`k`.`idkecamatan` = `f3`.`idkecamatan`))) left join `kabupaten_kota` `kk` on((`kk`.`idkabupaten_kota` = `f3`.`idkabupaten_kota`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_satu_juta_rumah_non_imb_view_lap`
--

/*!50001 DROP TABLE IF EXISTS `form_satu_juta_rumah_non_imb_view_lap`*/;
/*!50001 DROP VIEW IF EXISTS `form_satu_juta_rumah_non_imb_view_lap`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_satu_juta_rumah_non_imb_view_lap` AS select `f1`.`idform_satu_juta_rumah` AS `idform_satu_juta_rumah`,`f1`.`iddinas` AS `iddinas`,`f1`.`bulan` AS `bulan`,`f1`.`tanggal` AS `tanggal`,`f1`.`nama_kontak` AS `nama_kontak`,`f1`.`hp_kontak` AS `hp_kontak`,`f1`.`jenis` AS `jenis`,`f1`.`status` AS `status`,`f2`.`jumlah_mbr` AS `jumlah_mbr`,`f2`.`jumlah_non_mbr` AS `jumlah_non_mbr` from (((`form_satu_juta_rumah` `f1` join `form_satu_juta_rumah_non_imb` `f2` on((`f2`.`idform_satu_juta_rumah` = `f1`.`idform_satu_juta_rumah`))) left join `kecamatan` `k` on((`k`.`idkecamatan` = `f2`.`idkecamatan`))) left join `kabupaten_kota` `kk` on((`kk`.`idkabupaten_kota` = `f2`.`idkabupaten_kota`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_satu_juta_rumah_unit_view`
--

/*!50001 DROP TABLE IF EXISTS `form_satu_juta_rumah_unit_view`*/;
/*!50001 DROP VIEW IF EXISTS `form_satu_juta_rumah_unit_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_satu_juta_rumah_unit_view` AS select `fs`.`idform_satu_juta_rumah_unit` AS `idform_satu_juta_rumah_unit`,`fs`.`idform_satu_juta_rumah` AS `idform_satu_juta_rumah`,`fs`.`idkecamatan` AS `idkecamatan`,`fs`.`idkabupaten_kota` AS `idkabupaten_kota`,`fs`.`jumlah_mbr` AS `jumlah_mbr`,`fs`.`jumlah_non_mbr` AS `jumlah_non_mbr`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,`k`.`kecamatan` AS `kecamatan` from ((`form_satu_juta_rumah_unit` `fs` left join `kabupaten_kota` `kk` on((`kk`.`idkabupaten_kota` = `fs`.`idkabupaten_kota`))) left join `kecamatan` `k` on((`k`.`idkecamatan` = `fs`.`idkecamatan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `inbox`
--

/*!50001 DROP TABLE IF EXISTS `inbox`*/;
/*!50001 DROP VIEW IF EXISTS `inbox`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `inbox` AS select `a`.`link` AS `link`,`a`.`id` AS `id`,`b`.`Name` AS `Name`,`b`.`avatar` AS `avatar`,`c`.`privilage` AS `privilage`,`a`.`message` AS `message`,(case when (timestampdiff(YEAR,`a`.`sendtime`,now()) > 0) then concat(timestampdiff(YEAR,`a`.`sendtime`,now()),' Tahun') when ((timestampdiff(MONTH,`a`.`sendtime`,now()) > 0) and (timestampdiff(MONTH,`a`.`sendtime`,now()) < 13)) then concat(timestampdiff(MONTH,`a`.`sendtime`,now()),' Bulan') when ((timestampdiff(DAY,`a`.`sendtime`,now()) > 0) and (timestampdiff(DAY,`a`.`sendtime`,now()) < 31)) then concat(timestampdiff(DAY,`a`.`sendtime`,now()),' Hari') when ((timestampdiff(HOUR,`a`.`sendtime`,now()) > 0) and (timestampdiff(HOUR,`a`.`sendtime`,now()) < 25)) then concat(timestampdiff(HOUR,`a`.`sendtime`,now()),' Jam') when ((timestampdiff(MINUTE,`a`.`sendtime`,now()) > 0) and (timestampdiff(MINUTE,`a`.`sendtime`,now()) < 61)) then concat(timestampdiff(MINUTE,`a`.`sendtime`,now()),' Menit') when ((timestampdiff(SECOND,`a`.`sendtime`,now()) > 0) and (timestampdiff(SECOND,`a`.`sendtime`,now()) < 61)) then ' Just Now' end) AS `waktu` from ((`message` `a` join `user` `b` on((`a`.`id_user_from` = `b`.`id`))) join `user_privilage` `c` on((`b`.`id_privilage` = `c`.`id`))) order by `a`.`sendtime` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `log_pekerjaan_view`
--

/*!50001 DROP TABLE IF EXISTS `log_pekerjaan_view`*/;
/*!50001 DROP VIEW IF EXISTS `log_pekerjaan_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `log_pekerjaan_view` AS select `l`.`id` AS `id`,`l`.`jenis_pekerjaan` AS `jenis_pekerjaan`,`l`.`iddinas` AS `iddinas`,`l`.`waktu` AS `waktu`,`l`.`status` AS `status`,`l`.`idpekerjaan` AS `idpekerjaan`,`d`.`nama_dinas` AS `nama_dinas` from (`log_pekerjaan` `l` join `dinas` `d` on((`d`.`iddinas` = `l`.`iddinas`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pengembang_mbr_view`
--

/*!50001 DROP TABLE IF EXISTS `pengembang_mbr_view`*/;
/*!50001 DROP VIEW IF EXISTS `pengembang_mbr_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pengembang_mbr_view` AS select `pp`.`asosiasi` AS `asosiasi`,`pp`.`pengembang` AS `pengembang`,`pp`.`alamat` AS `alamat1`,`pp`.`bulan` AS `bulan`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,`p`.`idpengembang_mbr` AS `idpengembang_mbr`,`p`.`idpengembang` AS `idpengembang`,`p`.`idkabupaten_kota` AS `idkabupaten_kota`,`p`.`nama_perusahaan` AS `nama_perusahaan`,`p`.`alamat` AS `alamat`,`p`.`nama_pengembang` AS `nama_pengembang`,`p`.`bentuk_rumah` AS `bentuk_rumah`,`p`.`luas` AS `luas`,`p`.`tipe` AS `tipe`,`p`.`rencana` AS `rencana`,`p`.`realisasi` AS `realisasi`,`p`.`harga` AS `harga`,`p`.`jenis` AS `jenis` from ((`pengembang_mbr` `p` join `kabupaten_kota` `kk` on((`kk`.`idkabupaten_kota` = `p`.`idkabupaten_kota`))) join `pengembang` `pp` on((`p`.`idpengembang` = `pp`.`idpengembang`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pengembang_view`
--

/*!50001 DROP TABLE IF EXISTS `pengembang_view`*/;
/*!50001 DROP VIEW IF EXISTS `pengembang_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pengembang_view` AS select `p`.`bulan` AS `bulan`,`p`.`status` AS `status`,`p`.`idpengembang` AS `idpengembang`,`p`.`pengembang` AS `pengembang`,`p`.`alamat` AS `alamat`,`p`.`tanggal` AS `tanggal`,`p`.`asosiasi` AS `asosiasi`,`p`.`iddinas` AS `iddinas`,`d`.`nama_dinas` AS `nama_dinas`,`v`.`idprovinsi` AS `idprovinsi`,`v`.`provinsi` AS `provinsi` from ((`pengembang` `p` join `dinas` `d` on((`d`.`iddinas` = `p`.`iddinas`))) join `provinsi` `v` on((`v`.`iddinas` = `d`.`iddinas`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `perbankan_detail_view`
--

/*!50001 DROP TABLE IF EXISTS `perbankan_detail_view`*/;
/*!50001 DROP VIEW IF EXISTS `perbankan_detail_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `perbankan_detail_view` AS select `p`.`no_kontrak` AS `no_kontrak`,`p`.`bulan` AS `bulan`,`pd`.`idperbankan_detail` AS `idperbankan_detail`,`pd`.`idperbankan` AS `idperbankan`,`pd`.`lokasi` AS `lokasi`,`pd`.`pengembang_perorangan` AS `pengembang_perorangan`,`pd`.`kpr` AS `kpr`,`pd`.`nama_perumahan` AS `nama_perumahan`,`pd`.`bentuk_rumah` AS `bentuk_rumah`,`pd`.`jenis_rumah` AS `jenis_rumah`,`pd`.`tipe` AS `tipe`,`pd`.`rencana` AS `rencana`,`pd`.`realisasi` AS `realisasi`,`pd`.`harga` AS `harga`,`pd`.`mbr` AS `mbr`,`pd`.`non_mbr` AS `non_mbr`,`pd`.`idkabupaten_kota` AS `idkabupaten_kota`,`kk`.`kabupaten_kota` AS `kabupaten_kota` from ((`perbankan_detail` `pd` join `kabupaten_kota` `kk` on((`kk`.`idkabupaten_kota` = `pd`.`idkabupaten_kota`))) join `perbankan` `p` on((`p`.`idperbankan` = `pd`.`idperbankan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `peta_kepemilikan_penghunian`
--

/*!50001 DROP TABLE IF EXISTS `peta_kepemilikan_penghunian`*/;
/*!50001 DROP VIEW IF EXISTS `peta_kepemilikan_penghunian`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `peta_kepemilikan_penghunian` AS select `s`.`tahun` AS `tahun`,`s`.`idprovinsi` AS `idprovinsi`,`s`.`iddinas` AS `iddinas`,`s`.`kabupaten_kota` AS `kabupaten_kota`,`s`.`kepemilikan` AS `kepemilikan`,`s`.`penghunian` AS `penghunian`,`s`.`x` AS `x`,`s`.`y` AS `y`,`s`.`non_mbr_4` AS `non_mbr_4`,`s`.`mbr_5` AS `mbr_5`,`s`.`non_mbr_6` AS `non_mbr_6`,`s`.`mbr_7` AS `mbr_7`,`s`.`rtlh` AS `rtlh`,(select max(`status_kepemilikan_rumah_kabupaten_kota`.`non_mbr_4`) from `status_kepemilikan_rumah_kabupaten_kota` where ((`status_kepemilikan_rumah_kabupaten_kota`.`idprovinsi` = `s`.`idprovinsi`) and (`status_kepemilikan_rumah_kabupaten_kota`.`tahun` = `s`.`tahun`))) AS `totalnon_mbr_4`,(select max(`status_kepemilikan_rumah_kabupaten_kota`.`mbr_5`) from `status_kepemilikan_rumah_kabupaten_kota` where ((`status_kepemilikan_rumah_kabupaten_kota`.`idprovinsi` = `s`.`idprovinsi`) and (`status_kepemilikan_rumah_kabupaten_kota`.`tahun` = `s`.`tahun`))) AS `totalmbr_5`,(select max(`status_kepemilikan_rumah_kabupaten_kota`.`non_mbr_6`) from `status_kepemilikan_rumah_kabupaten_kota` where ((`status_kepemilikan_rumah_kabupaten_kota`.`idprovinsi` = `s`.`idprovinsi`) and (`status_kepemilikan_rumah_kabupaten_kota`.`tahun` = `s`.`tahun`))) AS `totalnon_mbr_6`,(select max(`status_kepemilikan_rumah_kabupaten_kota`.`mbr_7`) from `status_kepemilikan_rumah_kabupaten_kota` where ((`status_kepemilikan_rumah_kabupaten_kota`.`idprovinsi` = `s`.`idprovinsi`) and (`status_kepemilikan_rumah_kabupaten_kota`.`tahun` = `s`.`tahun`))) AS `totalmbr_7`,(select max(`status_kepemilikan_rumah_kabupaten_kota`.`rtlh`) from `status_kepemilikan_rumah_kabupaten_kota` where ((`status_kepemilikan_rumah_kabupaten_kota`.`idprovinsi` = `s`.`idprovinsi`) and (`status_kepemilikan_rumah_kabupaten_kota`.`tahun` = `s`.`tahun`))) AS `totalrtlh`,(select max(`status_kepemilikan_rumah_kabupaten_kota`.`kepemilikan`) from `status_kepemilikan_rumah_kabupaten_kota` where ((`status_kepemilikan_rumah_kabupaten_kota`.`idprovinsi` = `s`.`idprovinsi`) and (`status_kepemilikan_rumah_kabupaten_kota`.`tahun` = `s`.`tahun`))) AS `total`,(select max(`status_kepemilikan_rumah_kabupaten_kota`.`penghunian`) from `status_kepemilikan_rumah_kabupaten_kota` where ((`status_kepemilikan_rumah_kabupaten_kota`.`idprovinsi` = `s`.`idprovinsi`) and (`status_kepemilikan_rumah_kabupaten_kota`.`tahun` = `s`.`tahun`))) AS `totalp` from `status_kepemilikan_rumah_kabupaten_kota` `s` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `rtlh`
--

/*!50001 DROP TABLE IF EXISTS `rtlh`*/;
/*!50001 DROP VIEW IF EXISTS `rtlh`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `rtlh` AS select `p`.`kode_provinsi` AS `kode_provinsi`,`f1`.`tahun` AS `tahun`,`p`.`idprovinsi` AS `idprovinsi`,`p`.`provinsi` AS `provinsi`,sum(`k5`.`jumlah_rtlh_versi_bdt_4`) AS `rtlh`,`k5`.`sumber_data_6` AS `sumber_data_6` from ((`form_1_k5` `k5` join `form_1` `f1` on((`f1`.`idform_1` = `k5`.`idform_1`))) join `provinsi` `p` on((`p`.`idprovinsi` = `f1`.`idprovinsi`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `rtlh_kabupaten_kota`
--

/*!50001 DROP TABLE IF EXISTS `rtlh_kabupaten_kota`*/;
/*!50001 DROP VIEW IF EXISTS `rtlh_kabupaten_kota`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `rtlh_kabupaten_kota` AS select `p`.`kode_provinsi` AS `kode`,`d`.`iddinas` AS `iddinas`,`p`.`idprovinsi` AS `idprovinsi`,`p`.`provinsi` AS `provinsi`,`kk`.`idkabupaten_kota` AS `idkabupaten_kota`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,sum(`k5`.`jumlah_rtlh_versi_bdt_4`) AS `rtlh`,`k5`.`sumber_data_6` AS `sumber_data_6`,`f1`.`tahun` AS `tahun` from ((((`dinas` `d` join `form_1` `f1` on((`f1`.`iddinas` = `d`.`iddinas`))) join `form_1_k5` `k5` on((`k5`.`idform_1` = `f1`.`idform_1`))) join `kabupaten_kota` `kk` on((`kk`.`idkabupaten_kota` = `d`.`idkabupaten_kota`))) join `provinsi` `p` on((`p`.`iddinas` = `d`.`iddinas`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `rtlh_kecamatan`
--

/*!50001 DROP TABLE IF EXISTS `rtlh_kecamatan`*/;
/*!50001 DROP VIEW IF EXISTS `rtlh_kecamatan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `rtlh_kecamatan` AS select `f1`.`tahun` AS `tahun`,`f1`.`idform_1` AS `idform_1`,`f5`.`idform_1_k5` AS `idform_1_k5`,`k`.`idkecamatan` AS `idkecamatan`,`k`.`idkabupaten_kota` AS `idkabupaten_kota`,`k`.`kecamatan` AS `kecamatan`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,`f5`.`jumlah_rtlh_versi_bdt_4` AS `rtlh_bdt`,`f5`.`jumlah_rtlh_verifikasi_pemda_5` AS `rtlh_pemda`,`f5`.`sumber_data_6` AS `sumber_data_6` from (((`form_1_k5` `f5` join `form_1` `f1` on((`f1`.`idform_1` = `f5`.`idform_1`))) join `kecamatan` `k` on((`k`.`idkecamatan` = `f5`.`idkecamatan`))) join `kabupaten_kota` `kk` on((`kk`.`idkabupaten_kota` = `k`.`idkabupaten_kota`))) where (`f1`.`tahun` > 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `satu_juta_rumah_imb`
--

/*!50001 DROP TABLE IF EXISTS `satu_juta_rumah_imb`*/;
/*!50001 DROP VIEW IF EXISTS `satu_juta_rumah_imb`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `satu_juta_rumah_imb` AS select `a`.`status` AS `status`,year(`a`.`tanggal`) AS `tahun`,`a`.`idform_satu_juta_rumah` AS `idform_satu_juta_rumah`,`a`.`iddinas` AS `iddinas`,`d`.`nama_dinas` AS `nama_dinas`,`p`.`provinsi` AS `provinsi`,`p`.`idprovinsi` AS `idprovinsi`,`kk`.`idkabupaten_kota` AS `idkabupaten_kota`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,((sum(ifnull(`b`.`jumlah_mbr`,0)) + sum(ifnull(`c`.`jumlah_mbr`,0))) + sum(ifnull(`f`.`jumlah_mbr`,0))) AS `total`,((sum(ifnull(`b`.`jumlah_non_mbr`,0)) + sum(ifnull(`c`.`jumlah_non_mbr`,0))) + sum(ifnull(`f`.`jumlah_non_mbr`,0))) AS `total2` from ((((((`form_satu_juta_rumah` `a` left join `form_satu_juta_rumah_imb` `b` on((`b`.`idform_satu_juta_rumah` = `a`.`idform_satu_juta_rumah`))) left join `form_satu_juta_rumah_unit` `c` on((`c`.`idform_satu_juta_rumah` = `a`.`idform_satu_juta_rumah`))) left join `form_satu_juta_rumah_non_imb` `f` on((`f`.`idform_satu_juta_rumah` = `a`.`idform_satu_juta_rumah`))) join `dinas` `d` on((`d`.`iddinas` = `a`.`iddinas`))) join `kabupaten_kota` `kk` on((`kk`.`iddinas` = `d`.`iddinas`))) join `provinsi` `p` on((`p`.`idprovinsi` = `kk`.`idprovinsi`))) group by `d`.`iddinas` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `satu_juta_rumah_imb_nonmbr`
--

/*!50001 DROP TABLE IF EXISTS `satu_juta_rumah_imb_nonmbr`*/;
/*!50001 DROP VIEW IF EXISTS `satu_juta_rumah_imb_nonmbr`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `satu_juta_rumah_imb_nonmbr` AS select `a`.`status` AS `status`,year(`a`.`tanggal`) AS `tahun`,`a`.`idform_satu_juta_rumah` AS `idform_satu_juta_rumah`,`a`.`iddinas` AS `iddinas`,`d`.`nama_dinas` AS `nama_dinas`,`p`.`provinsi` AS `provinsi`,`p`.`idprovinsi` AS `idprovinsi`,`kk`.`idkabupaten_kota` AS `idkabupaten_kota`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,((sum(ifnull(`b`.`jumlah_non_mbr`,0)) + sum(ifnull(`c`.`jumlah_non_mbr`,0))) + sum(ifnull(`f`.`jumlah_non_mbr`,0))) AS `total` from ((((((`form_satu_juta_rumah` `a` left join `form_satu_juta_rumah_imb` `b` on((`b`.`idform_satu_juta_rumah` = `a`.`idform_satu_juta_rumah`))) left join `form_satu_juta_rumah_unit` `c` on((`c`.`idform_satu_juta_rumah` = `a`.`idform_satu_juta_rumah`))) left join `form_satu_juta_rumah_non_imb` `f` on((`f`.`idform_satu_juta_rumah` = `a`.`idform_satu_juta_rumah`))) join `dinas` `d` on((`d`.`iddinas` = `a`.`iddinas`))) join `kabupaten_kota` `kk` on((`kk`.`iddinas` = `d`.`iddinas`))) join `provinsi` `p` on((`p`.`idprovinsi` = `kk`.`idprovinsi`))) group by `d`.`iddinas` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `sejuta_rumah_imb`
--

/*!50001 DROP TABLE IF EXISTS `sejuta_rumah_imb`*/;
/*!50001 DROP VIEW IF EXISTS `sejuta_rumah_imb`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `sejuta_rumah_imb` AS select `d`.`idprovinsi` AS `idprovinsi`,`d`.`idkabupaten_kota` AS `idkabupaten_kota`,`d`.`kabupaten_kota` AS `kabupaten_kota`,(select (`fk4`.`non_mbr_4` + `fk4`.`mbr_5`) from (`form_1_k4_5` `fk4` join `form_1` `f1` on((`f1`.`idform_1` = `fk4`.`idform_1`))) where ((`f1`.`iddinas` = `d`.`iddinas`) and (`f1`.`tahun` = year(curdate())) and (`f1`.`status` = 'Disetujui')) limit 1) AS `mbr`,(select (`fk4`.`non_mbr_6` + `fk4`.`mbr_7`) from (`form_1_k4_5` `fk4` join `form_1` `f1` on((`f1`.`idform_1` = `fk4`.`idform_1`))) where ((`f1`.`iddinas` = `d`.`iddinas`) and (`f1`.`tahun` = year(curdate())) and (`f1`.`status` = 'Disetujui')) limit 1) AS `non_mbr` from `user_kabupaten_kota` `d` order by `d`.`iddinas` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `sejuta_rumah_imb_view`
--

/*!50001 DROP TABLE IF EXISTS `sejuta_rumah_imb_view`*/;
/*!50001 DROP VIEW IF EXISTS `sejuta_rumah_imb_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `sejuta_rumah_imb_view` AS select `sejuta_rumah_imb`.`idkabupaten_kota` AS `idkabupaten_kota`,`sejuta_rumah_imb`.`kabupaten_kota` AS `kabupaten_kota`,ifnull(`sejuta_rumah_imb`.`mbr`,0) AS `mbr`,ifnull(`sejuta_rumah_imb`.`non_mbr`,0) AS `non_mbr` from `sejuta_rumah_imb` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `status_kepemilikan_rumah`
--

/*!50001 DROP TABLE IF EXISTS `status_kepemilikan_rumah`*/;
/*!50001 DROP VIEW IF EXISTS `status_kepemilikan_rumah`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `status_kepemilikan_rumah` AS select `p`.`kode_provinsi` AS `kode_provinsi`,`f1`.`tahun` AS `tahun`,`p`.`idprovinsi` AS `idprovinsi`,`p`.`provinsi` AS `provinsi`,sum((`k4_3`.`k4331` + `k4_3`.`k4332`)) AS `jumlah_kepala_keluarga`,sum(`k4_1`.`k4141`) AS `milik_sendiri`,sum(`k4_1`.`k4142`) AS `kontrak_sewa`,sum(`k4_1`.`k4143`) AS `menumpang`,sum(`k4_1`.`k4144`) AS `dinas`,sum(`k4_1`.`k4145`) AS `lainnya`,sum((((`k4_1`.`k4142` + `k4_1`.`k4143`) + `k4_1`.`k4144`) + `k4_1`.`k4145`)) AS `kepemilikan`,sum(((`k4_1`.`k4141` + `k4_1`.`k4142`) + `k4_1`.`k4144`)) AS `penghunian`,`k4_1`.`k4151` AS `sumber_data` from (((`form_1` `f1` join `provinsi` `p` on((`p`.`idprovinsi` = `f1`.`idprovinsi`))) join `form_1_k4_1` `k4_1` on((`k4_1`.`idform_1` = `f1`.`idform_1`))) join `form_1_k4_3` `k4_3` on((`k4_3`.`idform_1` = `f1`.`idform_1`))) group by `p`.`idprovinsi` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `status_kepemilikan_rumah_kabupaten_kota`
--

/*!50001 DROP TABLE IF EXISTS `status_kepemilikan_rumah_kabupaten_kota`*/;
/*!50001 DROP VIEW IF EXISTS `status_kepemilikan_rumah_kabupaten_kota`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `status_kepemilikan_rumah_kabupaten_kota` AS select `kk`.`kabupaten_kota` AS `kabupaten_kota`,`d`.`x` AS `x`,`d`.`y` AS `y`,`f1`.`idprovinsi` AS `idprovinsi`,`f1`.`iddinas` AS `iddinas`,`f1`.`idform_1` AS `idform_1`,`f1`.`tahun` AS `tahun`,(`k4_3`.`k4331` + `k4_3`.`k4332`) AS `jumlah_kepala_keluarga`,`k4_1`.`k4141` AS `milik_sendiri`,`k4_1`.`k4142` AS `kontrak_sewa`,`k4_1`.`k4143` AS `menumpang`,`k4_1`.`k4144` AS `dinas`,`k4_1`.`k4145` AS `lainnya`,(((`k4_1`.`k4142` + `k4_1`.`k4143`) + `k4_1`.`k4144`) + `k4_1`.`k4145`) AS `kepemilikan`,`k4_1`.`k4143` AS `penghunian`,`k4_1`.`k4151` AS `sumber_data`,`k4_5`.`non_mbr_4` AS `non_mbr_4`,`k4_5`.`mbr_5` AS `mbr_5`,ifnull(`k5`.`jumlah_rtlh_verifikasi_pemda_5`,0) AS `rtlh`,`k4_5`.`non_mbr_6` AS `non_mbr_6`,`k4_5`.`mbr_7` AS `mbr_7` from ((((((`form_1` `f1` left join `form_1_k5` `k5` on((`k5`.`idform_1` = `f1`.`idform_1`))) join `form_1_k4_1` `k4_1` on((`k4_1`.`idform_1` = `f1`.`idform_1`))) join `form_1_k4_3` `k4_3` on((`k4_3`.`idform_1` = `f1`.`idform_1`))) join `form_1_k4_5` `k4_5` on((`k4_5`.`idform_1` = `f1`.`idform_1`))) join `dinas` `d` on((`d`.`iddinas` = `f1`.`iddinas`))) join `kabupaten_kota` `kk` on((`kk`.`iddinas` = `d`.`iddinas`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `task_notification`
--

/*!50001 DROP TABLE IF EXISTS `task_notification`*/;
/*!50001 DROP VIEW IF EXISTS `task_notification`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `task_notification` AS select `b`.`kind` AS `kind`,`a`.`status` AS `status`,`a`.`date_create` AS `date_create`,`a`.`date_expired` AS `date_expired`,`a`.`description` AS `description`,`b`.`icon` AS `icon`,`b`.`class` AS `class`,`b`.`link` AS `link`,(case when (timestampdiff(YEAR,`a`.`date_create`,now()) > 0) then concat(timestampdiff(YEAR,`a`.`date_create`,now()),' Tahun') when ((timestampdiff(MONTH,`a`.`date_create`,now()) > 0) and (timestampdiff(MONTH,`a`.`date_create`,now()) < 13)) then concat(timestampdiff(MONTH,`a`.`date_create`,now()),' Bulan') when ((timestampdiff(DAY,`a`.`date_create`,now()) > 0) and (timestampdiff(DAY,`a`.`date_create`,now()) < 31)) then concat(timestampdiff(DAY,`a`.`date_create`,now()),' Hari') when ((timestampdiff(HOUR,`a`.`date_create`,now()) > 0) and (timestampdiff(HOUR,`a`.`date_create`,now()) < 25)) then concat(timestampdiff(HOUR,`a`.`date_create`,now()),' Jam') when ((timestampdiff(MINUTE,`a`.`date_create`,now()) > 0) and (timestampdiff(MINUTE,`a`.`date_create`,now()) < 61)) then concat(timestampdiff(MINUTE,`a`.`date_create`,now()),' Menit') when ((timestampdiff(SECOND,`a`.`date_create`,now()) > 0) and (timestampdiff(SECOND,`a`.`date_create`,now()) < 61)) then ' Just Now' end) AS `waktu` from (`task` `a` join `task_kind` `b` on((`a`.`id_task_kind` = `b`.`id_task_kind`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_admin`
--

/*!50001 DROP TABLE IF EXISTS `user_admin`*/;
/*!50001 DROP VIEW IF EXISTS `user_admin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `user_admin` AS select `u`.`isLoggedIn` AS `isLoggedIn`,`u`.`id` AS `id`,`u`.`Name` AS `Name`,`u`.`idprovinsi` AS `idprovinsi`,`p`.`provinsi` AS `provinsi`,`u`.`logo_pusat` AS `logo_pusat` from (`user` `u` join `provinsi` `p` on((`p`.`idprovinsi` = `u`.`idprovinsi`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_kabupaten_kota`
--

/*!50001 DROP TABLE IF EXISTS `user_kabupaten_kota`*/;
/*!50001 DROP VIEW IF EXISTS `user_kabupaten_kota`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `user_kabupaten_kota` AS select `d`.`isLoggedIn` AS `isLoggedIn`,`d`.`login_count` AS `login_count`,`p`.`x` AS `x`,`p`.`y` AS `y`,`p`.`z` AS `z`,`d`.`iddinas` AS `iddinas`,`d`.`nama_dinas` AS `nama_dinas`,`d`.`alamat` AS `alamat`,`d`.`password` AS `password`,`d`.`telepon` AS `telepon`,`d`.`fax` AS `fax`,`d`.`email` AS `email`,`d`.`URL` AS `URL`,`kk`.`idkabupaten_kota` AS `idkabupaten_kota`,`kk`.`kabupaten_kota` AS `kabupaten_kota`,`kk`.`kode` AS `kode`,`kk`.`logo_kabkot` AS `logo_kabkot`,`kk`.`idprovinsi` AS `idprovinsi`,`kk`.`kepala_daerah` AS `kepala_daerah`,`kk`.`wakil_kepala_daerah` AS `wakil_kepala_daerah`,`kk`.`letak_geografis` AS `letak_geografis`,`kk`.`luas_wilayah` AS `luas_wilayah`,`kk`.`luas_wilayah_daratan` AS `luas_wilayah_daratan`,`kk`.`luas_wilayah_lautan` AS `luas_wilayah_lautan`,`kk`.`jumlah_penduduk` AS `jumlah_penduduk`,`kk`.`pertumbuhan_penduduk` AS `pertumbuhan_penduduk`,`kk`.`tingkat_kepadatan_penduduk` AS `tingkat_kepadatan_penduduk`,`kk`.`jumlah_penduduk_miskin_kota` AS `jumlah_penduduk_miskin_kota`,`kk`.`jumlah_penduduk_miskin_desa` AS `jumlah_penduduk_miskin_desa`,`kc`.`kecamatan` AS `kecamatan`,`kl`.`kelurahan` AS `kelurahan`,`p`.`provinsi` AS `provinsi`,`pr`.`id` AS `idprivilage` from (((((`dinas` `d` join `kabupaten_kota` `kk` on((`kk`.`iddinas` = `d`.`iddinas`))) join `user_privilage` `pr` on((`pr`.`id` = `d`.`id_privilage`))) left join `kecamatan` `kc` on((`kc`.`idkecamatan` = `d`.`idkecamatan`))) left join `kelurahan` `kl` on((`kl`.`idkelurahan` = `d`.`idkelurahan`))) left join `provinsi` `p` on((`p`.`idprovinsi` = `kk`.`idprovinsi`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_provinsi`
--

/*!50001 DROP TABLE IF EXISTS `user_provinsi`*/;
/*!50001 DROP VIEW IF EXISTS `user_provinsi`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `user_provinsi` AS select `d`.`isLoggedIn` AS `isLoggedIn`,`d`.`login_count` AS `login_count`,`p`.`x` AS `x`,`p`.`y` AS `y`,`p`.`z` AS `z`,0 AS `idkabupaten_kota`,`d`.`alamat` AS `alamat`,`d`.`iddinas` AS `iddinas`,`d`.`nama_dinas` AS `nama_dinas`,`d`.`password` AS `password`,`d`.`telepon` AS `telepon`,`d`.`fax` AS `fax`,`d`.`email` AS `email`,`d`.`URL` AS `URL`,`kb`.`kabupaten_kota` AS `kabupaten_kota`,`kc`.`kecamatan` AS `kecamatan`,`kl`.`kelurahan` AS `kelurahan`,`pr`.`id` AS `idprivilage`,`p`.`idprovinsi` AS `idprovinsi`,`p`.`provinsi` AS `provinsi`,`p`.`kode_provinsi` AS `kode_provinsi`,`p`.`logo_prov` AS `logo_prov`,`p`.`gubernur` AS `gubernur`,`p`.`foto_gubernur` AS `foto_gubernur`,`p`.`wakil_gubernur` AS `wakil_gubernur`,`p`.`foto_wakil_gubernur` AS `foto_wakil_gubernur`,`p`.`letak_geografis` AS `letak_geografis`,`p`.`luas_wilayah_daratan` AS `luas_wilayah_daratan`,`p`.`luas_wilayah_lautan` AS `luas_wilayah_lautan`,`p`.`luas_wilayah` AS `luas_wilayah`,`p`.`jumlah_penduduk` AS `jumlah_penduduk`,`p`.`pertumbuhan_penduduk` AS `pertumbuhan_penduduk`,`p`.`tingkat_kepadatan_penduduk` AS `tingkat_kepadatan_penduduk`,`p`.`jumlah_penduduk_miskin_kota` AS `jumlah_penduduk_miskin_kota`,`p`.`jumlah_penduduk_miskin_desa` AS `jumlah_penduduk_miskin_desa`,`p`.`iduser` AS `iduser` from (((((`dinas` `d` join `provinsi` `p` on((`p`.`iddinas` = `d`.`iddinas`))) join `user_privilage` `pr` on((`pr`.`id` = `d`.`id_privilage`))) left join `kabupaten_kota` `kb` on((`kb`.`idkabupaten_kota` = `d`.`idkabupaten_kota`))) left join `kecamatan` `kc` on((`kc`.`idkecamatan` = `d`.`idkecamatan`))) left join `kelurahan` `kl` on((`kl`.`idkelurahan` = `d`.`idkelurahan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-21  1:11:46
