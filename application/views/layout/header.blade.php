<div class="page-header">
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <div class="pull-left">
                    <a href="{{base_url('/')}}">
                        <img src="{{base_url('assets/global/img/empty.png')}}" alt="Logo PUPR" class="logo-default" />
                    </a>
                </div>
                <div>
                    <div class="site-title">e-BASISDATA PERUMAHAN</div>
                    <div class="text-header-kementerian">Sistem Informasi Basisdata Perumahan Indonesia</div>
                </div>
            </div>
            <!-- END LOGO -->

            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->

            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-mobile">
                                @if(isset($webHeader['nama']))
                                    {{$webHeader['nama']}} <br/> {{$webHeader['wilayah']}}
                                @endif
                            </span>

                            <img alt="Logo @if(isset($webHeader['nama'])) {{$webHeader['wilayah']}} @endif" class="avatar" src="{{$webHeader['avatar']}}">
                        </a>

                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{base_url("main/pengguna_dinas")}}"> <i class="icon-user"></i>
                                    Profil Pengguna
                                </a>
                            </li>

                            <li>
                                <a href="{{base_url("users/provinsi")}}"> <i class="icon-user"></i>
                                    Daftar Pengguna
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="{{base_url("login/logout_user/1/23")}}">
                                    <i class="icon-key"></i> Log Out
                                </a>
                            </li>
                        </ul>
                    </li>
                <!-- END USER LOGIN DROPDOWN-->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>

    <div class="page-header-menu">
        <div class="container">

            <!-- BEGIN MEGA MENU -->
            <div class="hor-menu">
                <ul class="nav navbar-nav">

                    @foreach($menu as $item)

                        @if($item->children->count() > 0)
                            @if($item->kind == 'classic-menu-dropdown')
                                <li class="menu-dropdown {{$item->kind}}">
                                    <a data-hover="megamenu-dropdown a" data-close-others="true" data-toggle="dropdown" href="javascript:;">
                                        {{$item->name}} <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-left">

                                        @foreach($item->children as $firstChild)
                                            <li class="dropdown-submenu {{$firstChild->csubmenu_caret}}">
                                                <a href="{{base_url($firstChild->link)}}" {{$firstChild->atribut}}>
                                                    <i class="{{$firstChild->icon}}"></i> {{$firstChild->name}}
                                                </a>

                                                @if($firstChild->children->count() > 0)
                                                    <ul class="dropdown-menu">

                                                        @foreach($firstChild->children as $secondChild)
                                                            <li>
                                                                <a href="{{base_url($secondChild->link)}}" {{$secondChild->atribut}}>{{$secondChild->name}}</a>
                                                            </li>
                                                        @endforeach

                                                    </ul>
                                                @endif


                                            </li>
                                        @endforeach


                                    </ul>

                                </li>
                            @else
                                <li class="menu-dropdown {{$item->kind}}">
                                    <a data-hover="megamenu-dropdown b" data-close-others="true" data-toggle="dropdown"
                                       href="javascript:;" class="dropdown-toggle">
                                        {{$item->name}} <i class="fa fa-angle-down"></i>
                                    </a>

                                    <ul class="dropdown-menu" style="min-width: 710px">
                                        <li>
                                            <div class="mega-menu-content">
                                                <div class="row">
                                                    @foreach($item->children as $firstChild)
                                                        <div class="col-md-4">
                                                            <ul class="mega-menu-submenu">
                                                                @if($firstChild->children->count() > 0)
                                                                    @foreach($firstChild->children as $secondChild)
                                                                        <li>
                                                                            <a href="{{base_url($secondChild->link)}}" class="iconify">
                                                                                <i class="{{$secondChild->icon}}"></i>  {{$secondChild->name}}

                                                                                @if($secondChild->date >= \Carbon\Carbon::now()->subDays(30))
                                                                                    <span class="badge badge-round badge-danger">baru</span>
                                                                                @endif
                                                                            </a>
                                                                        </li>
                                                                    @endforeach
                                                                @else
                                                                    <li>
                                                                        <a href="{{base_url($firstChild->link)}}" class="iconify">
                                                                            <i class="{{$firstChild->icon}}"></i> {{$firstChild->name}}
                                                                            @if($firstChild->date >= \Carbon\Carbon::now()->subDays(30))
                                                                                <span class="badge badge-round badge-danger">baru</span>
                                                                            @endif
                                                                        </a>
                                                                    </li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        @else
                            <li class="{{$item->active}}">
                                <a href="{{base_url($item->link)}}" {{$item->atribut}}>{{$item->name}}</a>
                            </li>
                        @endif
                    @endforeach

                </ul>
            </div>
        <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>

