<html>
<head>

    <meta charset="utf-8"/>
    <title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <link rel="shortcut icon" type="image/png" href="{{base_url('assets/global/img/favicon.png')}}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css">
    <link href="{{base_url('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{base_url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{base_url('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css">

    <link href="{{base_url('assets/global/css/components.css')}}" id="style_components" rel="stylesheet"
          type="text/css">
    <link href="{{base_url('assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css">
    <link href="{{base_url('assets/admin/layout3/css/layout.css')}}" rel="stylesheet" type="text/css">
    <link href="{{base_url('assets/admin/layout3/css/themes/default.css')}}" rel="stylesheet" type="text/css"
          id="style_color">
    <link href="{{base_url('assets/admin/layout3/css/custom.css')}}" rel="stylesheet" type="text/css">

    @stack('styles')

    @stack('style')
</head>
<body>

    @include('layout.header')

    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>{{$title}} <small class="page-title-tag">{{$webHeader['wilayah']}}</small></h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="container">

            @include('layout.breadcrumb')

            <div class="margin-top-10">
                @yield('content')

            </div>
        </div>
    </div>

    @include('layout.footer')

    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{base_url('assets/global/plugins/respond.min.js')}}"></script>
<script src="{{base_url('assets/global/plugins/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{base_url('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<!-- <script type="text/javascript" src="</?php echo base_url('assets/js/jquery-1.7.2.min.js'); ?>"></script> -->
<script src="{{base_url('assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{base_url('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{base_url('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{base_url('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}"
        type="text/javascript"></script>
<script src="{{base_url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"
        type="text/javascript"></script>
<script src="{{base_url('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{base_url('assets/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
<script src="{{base_url('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<script src="{{base_url('assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{base_url('assets/admin/layout3/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{base_url('assets/admin/layout3/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{base_url('assets/admin/pages/scripts/form-validation.js')}}"></script>
<script src="{{base_url('assets/admin/pages/scripts/ui-general.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL STYLES -->
<script src="{{base_url('assets/global/plugins/holder/holder.min.js')}}" type="text/javascript"></script>

@stack('scripts')

<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        // Demo.init(); // init demo features
        FormValidation.init();
        UIGeneral.init();
        Index.init(); // init index page
        Tasks.initDashboardWidget(); // init tash dashboard widget

    });
</script>

@stack('script')

</body>
</html>