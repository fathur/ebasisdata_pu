<!-- BEGIN PAGE BREADCRUMB -->
@php $breadcrumbsCount = count($breadcrumbs) @endphp

@if($breadcrumbsCount > 0)
<ul class="page-breadcrumb breadcrumb">


    @for($i = 0; $i < $breadcrumbsCount; $i++)

        @if($i == $breadcrumbsCount - 1)
            <li class="active">{{$breadcrumbs[$i]['text']}}</li>
        @else
            <li>
                <a href="{{$breadcrumbs[$i]['url']}}">{{$breadcrumbs[$i]['text']}}</a><i class="fa fa-angle-right"></i>
            </li>
        @endif

    @endfor

</ul>
@endif