<form action="{{base_url('users/'.$id.'/update-password')}}" method="post">

    <input type="hidden" value="{{$type}}" name="type">

    <div class="alert alert-danger hidden" id="password-alert">
    </div>

    <div class="form-group">
        <label for="password1">Password</label>
        <input type="password" class="form-control" id="password1" name="password1">
    </div>

    <div class="form-group">
        <label for="password2">Confirm password</label>
        <input type="password" class="form-control" id="password2" name="password2">
    </div>

    <button type="button" class="btn btn-primary" onclick="updatePassword(this)">Simpan</button>
</form>