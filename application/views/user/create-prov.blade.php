@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body">
                    <form action="{{base_url('/users/provinsi/store')}}" method="post">
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" name="name" class="form-control" id="name" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="nama_dinas">Dinas</label>
                            <input type="text" name="nama_dinas" class="form-control" id="nama_dinas" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" class="form-control" id="email" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="telepon">Telephone</label>
                            <input type="text" name="telepon" class="form-control" id="telepon" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="fax">Fax</label>
                            <input type="text" name="fax" class="form-control" id="fax" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea name="alamat" class="form-control" id="alamat" autocomplete="off"></textarea>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="password1">Password</label>
                            <input type="password" name="password1" class="form-control" id="password1" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="password2">Confirm password</label>
                            <input type="password" name="password2" class="form-control" id="password2" autocomplete="off">
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection