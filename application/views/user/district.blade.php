@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="table-toolbar">

                        <a href="{{ base_url('/users/provinsi/'.$province->id.'/kabupaten/create') }}"
                           class="btn btn-primary pull-right">Tambah user kabupaten/kota</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">

                        <table class="table table-hover table-striped table-bordered" id="list-user-kab">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Kota/Kabupaten</th>
                                <th>Dinas</th>
                                <th>Alamat</th>
                                <th>Email</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php $i = 1; @endphp
                            @foreach($districts as $district)
                                <tr class="@if(is_null($district->dinas)) danger @endif">
                                    <td>{{ $i }}</td>
                                    <td>{{$district->kabupaten_kota}}</td>
                                    <td>{{optional($district->dinas)->nama_dinas}}</td>

                                    <td>{{optional($district->dinas)->alamat}}</td>
                                    <td>
                                        <a href="mailto:{{optional($district->dinas)->email}}">{{optional($district->dinas)->email}}</a>
                                    </td>
                                    <td>
                                        @if(is_null($district->dinas))
                                            <a href="{{base_url('/users/provinsi/'.$province->id.'/kabupaten/'.$district->id.'/edit?password=true')}}"
                                               class="btn btn-danger btn-xs">Buat akses</a>
                                        @else
                                            <button href="#" class="btn btn-xs blue-hoki"
                                                    data-id="{{$district->id}}"
                                                    data-type="district"
                                                    onclick="showPasswordEdit(this)">
                                                <i class="fa fa-lock"></i> Password
                                            </button>
                                            <a href="{{base_url('/users/provinsi/'.$province->id.'/kabupaten/'.$district->id.'/edit')}}"
                                               class="btn btn-xs blue-hoki">
                                                <i class="fa fa-pencil"></i> Edit
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                @php $i++ @endphp

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('form.modal')

@endsection

@push('styles')
    <link rel="stylesheet" type="text/css"
          href="{{base_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
@endpush

@push('scripts')
    <script type="text/javascript"
            src="{{base_url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{base_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
    <script src="{{base_url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>

    <script src="{{base_url('assets/js/helperDataTable.js')}}"></script>
@endpush


@push('script')
    <script>

        let baseUrl = '{{base_url()}}';

        $('#region').change(function () {
            let val = $(this).val();
            window.location.href = baseUrl + 'users/' + val;
        });


        $('#list-user-kab').dataTable({
            "bStateSave": true
        });

        function showPasswordEdit(obj) {
            let $obj = $(obj);
            let formPasswordUrl = baseUrl + 'users/' + $obj.data('id') + '/password?type=' + $obj.data('type');

            $kModal = $('#k-modal');
            $kModal.modal('toggle');
            $kModal.find('.modal-body').html('Loading...');
            $.get(formPasswordUrl, function (response) {
                $kModal.find('.modal-body').html(response.body);
                $kModal.find('.modal-title').html(response.title);
            });
        }

        function updatePassword(object) {
            let $this = $(object);
            let $form = $this.parent();
            let url = $form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: $form.serialize(), // serializes the form's elements.
                success: function (data) {
                    // close modal
                    $('#k-modal').modal('toggle');
                },
                error: function (res) {

                    $('#password-alert').text('')
                        .removeClass('hidden')
                        .text(res.responseJSON['data']);
                }
            });
        }
    </script>
@endpush