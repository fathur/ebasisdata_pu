@extends('layout.main')

@section('content')
    @if($user->privilege->id == 1)
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-body">
                        <form action="#">
                            <div class="form-group">
                                <label for="provinsi">Provinsi</label>
                                <select name="provinsi" id="provinsi" class="form-control">
                                    <option value="0" selected>- Pilih satu -</option>
                                    @foreach($provinces as $province)
                                        <option value="{{$province->id}}">{{$province->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group hidden" id="wrapper-kabupaten">
                                <label for="kabupaten">Kota/Kabupaten</label>
                                <select name="kabupaten" id="kabupaten" class="form-control" onchange="loadDataTable1B()">
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @elseif($user->privilege->id == 2)
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-body">
                        <form action="#">
                            <div class="form-group" id="wrapper-kabupaten">
                                <label for="kabupaten">Kota/Kabupaten</label>
                                <select name="kabupaten" id="kabupaten" class="form-control" onchange="loadDataTable1B()">
                                    <option value="0" selected>- Pilih satu -</option>

                                    @foreach($kabupaten as $kab)
                                        <option value="{{$kab->id}}">{{$kab->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <form action="{{base_url('form-1b/store')}}" class="form-horizontal" method="post">
                                    <div class="input-group">
                                        @if($user->privilege->id == 3)

                                            <select name="tahun" id="tahun" class="form-control" title="Tahun">
                                                @foreach($years as $year => $status)
                                                    <option value="{{$year}}" {{$status ? 'disabled' : ''}}>{{$year}}</option>
                                                @endforeach
                                            </select>

                                            <span class="input-group-btn">
                                                <button type="submit" id="" class="btn left-zeroradius green">Tambah Form IB</button>
                                            </span>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-hover table-striped table-bordered" id="list-form-1b">
                        <thead>
                            <tr>
                                {{--<th width="9%">No.</th>--}}
                                <th width="20%">Tahun</th>
                                <th width="25%">Tanggal</th>
                                <th width="18%">Status</th>
                                <th width="10%">Lihat</th>
                                <th width="18%">Fungsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{base_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{base_url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{base_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
    <script src="{{base_url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>

    <script src="{{base_url('assets/js/helperDataTable.js')}}"></script>
@endpush


@push('script')
    <script>
        $('#list-form-1b').dataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            ajax: {
                url: '{{base_url('form-1b/data')}}',
                type: 'POST',

                @if($user->privilege->id == 1)
                data: function (d) {
                    d.provinsi = $('#provinsi').val();
                    d.kabupaten = $('#kabupaten').val();
                }
                @elseif($user->privilege->id == 2)
                data: function (d) {
                    d.provinsi = {{$dinas->provinsi->id}};
                    d.kabupaten = $('#kabupaten').val();
                }
                @endif
            },
            columns: [
                // {data: 'idform_1', name: 'idform_1', orderable: false},
                {data: 'tahun', name: 'tahun'},
                {data: 'tanggal', name: 'tanggal_buat'},
                {data: 'status', name: 'status'},
                {data: 'lihat', name: 'lihat', searchable: false, orderable: false},
                {data: 'fungsi', name: 'fungsi', searchable: false, orderable: false},
            ]
        });


        $('#provinsi').change(function () {

            let $this = $(this);
            let $valProv = $('#provinsi').val();

            if ($valProv !== 0) {
                $.get('{{base_url("kabupaten/lists")}}/' + $valProv, function (response) {
                    $('#wrapper-kabupaten').removeClass('hidden');

                    let htmlOptions = '<option value="0" selected>- Pilih satu -</option>';
                    let i;
                    for (i = 0; i < response.length; i++) {
                        let html = '<option value="'+response[i].id+'">'+response[i].name+'</option>';
                        htmlOptions = htmlOptions + html;
                    }

                    $('#kabupaten').html(htmlOptions);
                });
            } else {
                $('#wrapper-kabupaten').addClass('hidden');

            }

        });

        function loadDataTable1B() {
            $('#list-form-1b').DataTable().ajax.reload(null, false);
        }
    </script>
@endpush