<h4>K.1 KELEMBAGAAN PERUMAHAN DAN PERMUKIMAN</h4>

<p>Struktur organisasi dinas yang menangani perumahan dan permukiman di
    @if($mode == '1a')
        Provinsi {{$dinas->provinsi->name}}
    @elseif($mode == '1b')
        {{$dinas->kabupatenKota->name}}
    @endif
</p>

<div class="image-upload-wrapper margin-bottom-20">

    <div class="image text-center margin-bottom-20">
        @if($form->k1)
            <img src="{{base_url($form->k1->struktur_dinas)}}" alt="No image available" class="img-responsive center-block">
        @else
        <img src="{{base_url('assets/uploads/no-image.png')}}" alt="No image available" class="img-responsive center-block">
        @endif
    </div>

    @if($editable)
        <div class="col-progress">
            <div class="wrapper"></div>
        </div>

        <input type="file" class="simple-upload center-block " id="k1-upload">
    @endif
</div>

@if($editable)
<div class="note note-info">
    <p>Penjelasan:</p>
    <ul>
        <li>Format gambar yang diperbolehkan adalah JPG, JPEG dan PNG.</li>
        <li>Ukuran resolusi gambar yang dianjurkan adalah 700x400 pixel</li>
    </ul>
</div>
@endif


@push('scripts')
@endpush

@push('script')
    <script>
        $('#k1-upload').change(function () {
            let $this = $(this);
            let $thisProgressStatus = $this.parent().find('.col-progress');
            let $thisImage = $this.parent().find('.image');

            console.log($thisProgressStatus);

            $(this).simpleUpload('{{base_url("form-{$mode}/{$form->id}/k1/upload")}}', {
                name: 'image',
                data: {
                    idform_1: '{{$form->id}}',
                    jenis: '1A',

                },
                progress: function (progress) {
                    let htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                        '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="'+Math.round(progress)+'" aria-valuemin="0" aria-valuemax="100" style="width: '+Math.round(progress)+'%"> ' +
                        '<span class="">'+Math.round(progress)+'%</span> ' +
                        '</div> ' +
                        '</div>';
                    $thisProgressStatus.find('.wrapper').html(htmlProgress);
                },
                success: function (data) {

                    // Logic put image here
                    $thisImage.html("<img src='"+data.image+"' class='img-responsive center-block'/>");

                    $thisProgressStatus.find('.progress').fadeOut();
                },
                error: function (error) {

                    let htmlError = `<div class="note note-danger">`+error.message+`</div>`;
                    $thisProgressStatus.find('.progress').fadeOut();
                    $thisProgressStatus.find('.wrapper')
                        .html(htmlError)
                        .css({display:'none'});

                    $thisProgressStatus.find('.wrapper').fadeIn();
                }
            })
        });
    </script>
@endpush