<h4>K.3 PEMBANGUNAN PERUMAHAN</h4>
@if($editable)

    <div class="margin-bottom-20">
        @if($form->k3)
            <a class="btn btn-sm blue-hoki"
               onclick="kModalEdit('{{base_url("form-{$mode}/{$form->id}/k3/{$form->k3->id}/edit")}}')">Edit</a>
        @else
            <a class="btn btn-sm btn-success"
               onclick="k3Create(this)">Tambah</a>
        @endif
    </div>
@endif

<div id="list-k3">
    @include('form.k3.view')
</div>

<div class="note note-info">
    <strong>Penjelasan:</strong>
    <p>Kelengkapan dokumen kelengkapan/pendukung program/kegiatan Perumahan dan Kawasan Permukiman yang telah ada dasar
        leglisasinya seperti Perda atau Pergub, dan juga sumber dana yang pernah digunakan untuk
        pembangunan/rehabilitasi rumah di Kab/Kota</p>
</div>

@push('script')
    <script>
        function k3Create(obj) {

            let $this = $(obj);

            $.get("{{base_url("form-{$mode}/{$form->id}/k3/create")}}", function (response) {
                $('#list-k3').html(response.body);

                $this.html('Edit');
                $this.attr('onclick', "kModalEdit('{{base_url("form-{$mode}/{$form->id}/k3")}}/" + response.k3.id + "/edit')");
            });
        }
    </script>
@endpush