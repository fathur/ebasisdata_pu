<h4>K.4 BACKLOG PERUMAHAN</h4>

<div class="row">
    <div class="col-md-12">
        @include('form.k4.a.view')
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-md-12">
        @include('form.k4.b.view')
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-md-12">
        @include('form.k4.c.view')
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-md-12">
        @include('form.k4.d.view')
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-md-12">
        @include('form.k4.e.view')
    </div>
</div>

