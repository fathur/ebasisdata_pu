<ol>
    <li>
        <div>Apakah Pemerintah Kabupaten/Kota mempunyai Dokumen Perencanaan Urusan Perumahan?</div>

        @if($form->k3)
        <ol class="list-unstyled">

            @foreach (range('a', 'f') as $char)

                @php
                    $isi1 = "isi_1{$char}"
                @endphp

                <li>
                    <label>
                        <span class="checker">
                            @if($form->k3->{$isi1} == 'Y')
                                <i class="fa fa-check" style="color: green;"></i>
                            @elseif($form->k3->{$isi1} == 'T')
                                <i class="fa fa-times" style="color: gray;"></i>
                            @else
                                <i class="fa fa-times" style="color: gray;"></i>
                            @endif
                        </span>
                        {{ \Model\Eloquent\Form\K3::DOCUMENTS[$isi1] }}

                        @if(in_array($isi1, ['isi_1f']))
                            @php $ket1 = "{$isi1}_keterangan"; @endphp

                            @if($form->k3->{$isi1} == 'Y')
                            : {{ $form->k3->{$ket1} }}
                            @endif
                        @endif
                    </label>

                </li>
            @endforeach
        </ol>
        @endif

    </li>
    <li>
        <div>Dalam Pelaksanaan Urusan Perumahan, Pemerintah Kabupaten/Kota Memanfaatkan Sumber Dana yang Berasal
            dari:
        </div>

        @if($form->k3)
        <ol class="list-unstyled">
            @foreach (range('a', 'e') as $char)

                @php
                    $isi2 = "isi_2{$char}"
                @endphp

                <li>
                    <label>
                        <span class="checker">
                            @if($form->k3->{$isi2} == 'Y')
                                <i class="fa fa-check" style="color: green;"></i>
                            @elseif($form->k3->{$isi2} == 'T')
                                <i class="fa fa-times" style="color: gray;"></i>
                            @else
                                <i class="fa fa-times" style="color: gray;"></i>
                            @endif
                        </span>
                        {{ \Model\Eloquent\Form\K3::BUDGETS[$isi2] }}

                        @if(in_array($isi2, ['isi_2c','isi_2d','isi_2e']))
                            @php $ket2 = "{$isi2}_keterangan"; @endphp

                            @if($form->k3->{$isi2} == 'Y')
                            : {{ $form->k3->{$ket2} }}
                            @endif
                        @endif
                    </label>

                </li>
            @endforeach
        </ol>
        @endif
    </li>
</ol>
