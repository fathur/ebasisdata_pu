<form action="{{base_url("form-{$mode}/{$form->id}/k3/{$form->k3->id}/update")}}" method="post" class="form-inline">
    <ol>
        <li>
            <div>Apakah Pemerintah Kabupaten/Kota mempunyai Dokumen Perencanaan Urusan Perumahan?</div>

            <ol type="a">

                @foreach (range('a', 'f') as $char)

                    @php
                        $isi1 = "isi_1{$char}"
                    @endphp

                    <li>
                        <label>
                        <span class="checker">
                            @if($form->k3->{$isi1} == 'Y')
                                <input type="checkbox" value="Y" name="{{$isi1}}" checked>
                            @elseif($form->k3->{$isi1} == 'T')
                                <input type="checkbox" value="Y" name="{{$isi1}}">
                            @else
                                <input type="checkbox" value="Y" name="{{$isi1}}">
                            @endif
                        </span>
                            {{ \Model\Eloquent\Form\K3::DOCUMENTS[$isi1] }}
                        </label>

                        @if(in_array($isi1, ['isi_1f']))
                            @php $ket1 = "{$isi1}_keterangan"; @endphp

                        : <input type="text" class="form-control input-sm" value="{{ $form->k3->{$ket1} }}" title="Keterangan 1F" name="{{$ket1}}" placeholder="uraikan">
                        @endif
                    </li>
                @endforeach
            </ol>
        </li>
        <li>
            <div>Dalam Pelaksanaan Urusan Perumahan, Pemerintah Kabupaten/Kota Memanfaatkan Sumber Dana yang Berasal
                dari:
            </div>

            <ol type="a">
                @foreach (range('a', 'e') as $char)

                    @php
                        $isi2 = "isi_2{$char}"
                    @endphp

                    <li>
                        <label>
                        <span class="checker">
                            @if($form->k3->{$isi2} == 'Y')
                                <input type="checkbox" value="Y" name="{{$isi2}}" checked>

                            @elseif($form->k3->{$isi2} == 'T')
                                <input type="checkbox" value="Y" name="{{$isi2}}">
                            @else
                                <input type="checkbox" value="Y" name="{{$isi2}}">
                            @endif
                        </span>
                            {{ \Model\Eloquent\Form\K3::BUDGETS[$isi2] }}
                        </label>

                        @if(in_array($isi2, ['isi_2c','isi_2d','isi_2e']))

                            @php $ket2 = "{$isi2}_keterangan"; @endphp

                            : <input type="text" class="form-control input-sm" value="{{ $form->k3->{$ket2} }}" title="Keterangan {{$isi2}}" name="{{$isi2}}_keterangan" placeholder="uraikan">
                        @endif
                    </li>
                @endforeach
            </ol>
        </li>
    </ol>

    <button type="button" class="btn btn-success" onclick="k3Submit(this)">Simpan</button>
</form>