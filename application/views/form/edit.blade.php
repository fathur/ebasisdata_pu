@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body">

                    <table id="user" class="table table-bordered table-striped" style="margin-bottom: 5px;">
                        <tbody>
                        <tr>
                            @if($mode == '1a')
                                <td style="width: 15%">Provinsi</td>
                                <td style="width: 50%">{{$dinas->provinsi->name}}</td>
                            @elseif($mode == '1b')
                                <td style="width: 15%">Kabupaten/Kota</td>
                                <td style="width: 50%">{{$dinas->kabupatenKota->name}}</td>
                            @endif

                            <td style="width:35%; vertical-align:middle; text-align:center" rowspan="3">
                                <h3>{{$title}}</h3>
                            </td>
                        </tr>

                        <tr>
                            <td>Nama Dinas</td>
                            <td>{{$dinas->nama_dinas}}</td>
                        </tr>

                        <tr>
                            <td>Alamat</td>
                            <td>{{$dinas->alamat}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body">
                    <h3 class="uppercase bold text-center">

                        DAFTAR ISIAN PENDATAAN PKP

                        @if($mode == '1a')
                            PROVINSI {{$dinas->provinsi->name}}
                        @elseif($mode == '1b')
                            {{$dinas->kabupatenKota->name}}
                        @endif

                        TAHUN {{$form->tahun}}

                    </h3>

                    <div class="note note-info">
                        <p>Kegiatan pendataan perumahan ini dilakukan untuk memenuhi kebutuhan data dan
                            informasi perumahan. Data ini merupakan bagian dari Pembangunan Basis Data PKP
                            Kabupaten/Kota sesuai UU No. 1 Tahun 2011 tentang Perumahan dan Kawasan Permukiman (PKP)
                            pasal 17 dan Peraturan Pemerintah No. 88 Tahun 2014 tentang Pembinaan Penyelenggaraan
                            PKP pasal 18 ayat 1.</p>
                        <p>Tabel ini diisi oleh SNVT bidang Perumahan Kabupaten/Kota, untuk kelengkapan datanya
                            berkoordinasi dengan Pokja PKP Kabupaten/Kota, BPS Kabupaten/Kota, Bappeda Kabupaten/Kota,
                            Dinas
                            Sosial Kabupaten/Kota, BKKBN Kabupaten/Kota, serta rekapitulasi data dari Form 1B oleh
                            Kabupaten/Kota.
                            Untuk pertanyaan lebih lanjut dapat menghubungi Subdit Data dan Informasi, Direktorat
                            Perencanaan Penyediaan Perumahan: <a href="mailto:datinperumahan@gmail.com">datinperumahan@gmail.com</a>
                            atau Telp./Fax. 021-7211883.</p>
                        <p>Daftar isian ini merupakan data sekunder hasil dari berbagai kegiatan pendataan
                            perumahan, terdiri dari:</p>
                        <ul>
                            <li>K.1 Kelembagaan Perumahan dan Permukiman</li>
                            <li>K.2 Alokasi APBD untuk Urusan Perumahan dan Kawasan Permukiman (PKP)</li>
                            <li>K.3 Pembangunan Perumahan</li>
                            <li>K.4 Backlog Perumahan</li>
                            <li>K.5 Rumah Tidak Layak Huni</li>
                            <li>K.6 Kawasan Kumuh</li>
                        </ul>


                    </div>


                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="#tab_k_1" data-toggle="tab"> <i class="fa fa-bookmark-o"></i> K.1 </a>
                        </li>
                        <li>
                            <a href="#tab_k_2" data-toggle="tab"> <i class="fa fa-bookmark-o"></i> K.2 </a>
                        </li>
                        <li>
                            <a href="#tab_k_3" data-toggle="tab"> <i class="fa fa-bookmark-o"></i> K.3 </a>
                        </li>
                        <li>
                            <a href="#tab_k_4" data-toggle="tab"> <i class="fa fa-bookmark-o"></i> K.4 </a>
                        </li>
                        <li>
                            <a href="#tab_k_5" data-toggle="tab"> <i class="fa fa-bookmark-o"></i> K.5 </a>
                        </li>
                        <li>
                            <a href="#tab_k_6" data-toggle="tab"> <i class="fa fa-bookmark-o"></i> K.6 </a>
                        </li>
                    </ul>

                    <div class="tab-content form-1">
                        <div class="tab-pane fade active in" id="tab_k_1">
                            @include('form.k1')
                        </div>

                        <div class="tab-pane fade" id="tab_k_2">
                            @include('form.k2')
                        </div>

                        <div class="tab-pane fade" id="tab_k_3">
                            @include('form.k3')
                        </div>

                        <div class="tab-pane fade" id="tab_k_4">
                            @include('form.k4')
                        </div>

                        <div class="tab-pane fade" id="tab_k_5">
                            @include('form.k5.view')
                        </div>

                        <div class="tab-pane fade" id="tab_k_6">
                            @include('form.k6.view')
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body">

                    <div class="well-lg" style="background-color: #eee; border: 1px solid #ddd;">
                        <p>Demikian hasil pelaksanaan pendataan perumahan dan kawasan permukiman,
                            dilakukan untuk dipergunakan sebagaimana mestinya.</p>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-6 col-sm-12">
                                @if($mode == '1a')
                                    <p>{{$dinas->provinsi->name}}, {{date('d-M-Y')}}</p>
                                @elseif($mode == '1b')
                                    <p>{{$dinas->kabupatenKota->name}}, {{date('d-M-Y')}}</p>
                                @endif

                                <p>Tanda Bukti Penyetujuan,</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>Kepala {{$user->nama_dinas}}</p>
                            </div>
                        </div>

                        <div id="wrapper-pengesahan">
                            @include('form.pengesahan')
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('form.modal')

@endsection

@push('styles')
    <link rel="stylesheet" type="text/css"
          href="{{base_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
@endpush

@push('style')
    <style>
        tfoot {background-color: #dddddd;}
    </style>
@endpush

@push('scripts')
    <script type="text/javascript"
            src="{{base_url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{base_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
    <script src="{{base_url('assets/global/plugins/bootbox/bootbox.min.js') }}"></script>
    <script src="{{base_url('assets/js/simpleUpload.min.js')}}"></script>
    <script src="{{base_url('assets/js/helperDataTable.js')}}"></script>

@endpush

@push('script')
    <script>

        function finishForm(obj) {
            bootbox.confirm({
                message: "Anda yakin ingin menyelesaikan formulir ini?",
                buttons: {
                    confirm: {
                        label: 'Ya',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Tidak',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {

                    // Jika ya
                    if (result) {
                        let $this = $('#pengesahan-upload');
                        let $thisProgressStatus = $this.parent().find('.col-progress');

                        $this.simpleUpload('{{base_url("form-1b/{$form->id}/pengesahan/upload")}}', {
                            name: 'image',
                            data: {
                                {{--idform_1: '{{$form->id}}',--}}
                                jenis: '1A',

                            },
                            progress: function (progress) {
                                let htmlProgress = '<div class="progress" style="margin-bottom: 0;"> ' +
                                    '<div class="progress-bar progress-bar-striped active " role="progressbar" aria-valuenow="' + Math.round(progress) + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + Math.round(progress) + '%"> ' +
                                    '<span class="">' + Math.round(progress) + '%</span> ' +
                                    '</div> ' +
                                    '</div>';
                                $thisProgressStatus.find('.wrapper').html(htmlProgress);
                            },
                            success: function (data) {

                                // Logic put image here
                                // $thisImage.html("<img src='"+data.image+"' class='img-responsive center-block'/>");

                                $thisProgressStatus.find('.progress').fadeOut();
                                $('#uploader').fadeOut();

                                let $filePengesahan = $('#file-pengesahan');
                                $filePengesahan.html('<a href="'+data.image+'" target="__blank" class="btn btn-primary">Unduh bukti pengesahan</a>');
                                $filePengesahan.fadeIn();
                            },
                            error: function (error) {

                                let htmlError = `<div class="note note-danger">` + error.message + `</div>`;
                                $thisProgressStatus.find('.progress').fadeOut();
                                $thisProgressStatus.find('.wrapper')
                                    .html(htmlError)
                                    .css({display: 'none'});

                                $thisProgressStatus.find('.wrapper').fadeIn();
                            }
                        })

                    }
                }
            });

        }

        function kModalEdit(formLocation) {

            $kModal = $('#k-modal');
            $kModal.modal('toggle');
            $kModal.find('.modal-body').html('Loading...');

            $.get(formLocation, function (response) {
                $kModal.find('.modal-body').html(response.body);
                $kModal.find('.modal-title').html(response.title);

            });

        }

        function kModalSubmit(object) {

            let $this = $(object);
            let $form = $this.parent();
            let url = $form.attr('action');
            let tableName = $form.data('table');

            $.ajax({
                type: "POST",
                url: url,
                data: $form.serialize(), // serializes the form's elements.
                success: function (data) {
                    // close modal
                    $('#k-modal').modal('toggle');

                    // reload datatables\
                    $('#' + tableName).DataTable().ajax.reload(null, false);

                    // Check pengesahan
                    $.get('{{base_url("form-1b/{$form->id}/pengesahan/check")}}', function (resp) {
                        $('#wrapper-pengesahan').html(resp.body);
                    })
                },
            });
        }

        function k3Submit(object) {
            let $this = $(object);
            let $form = $this.parent();
            let url = $form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: $form.serialize(), // serializes the form's elements.
                success: function (data) {
                    // close modal
                    $('#k-modal').modal('toggle');

                    $.get('{{base_url()}}form-1b/' + data.id + '/k3/' + data.k3id + '/view', function (response) {
                        // console.log(response.body);
                        $('#list-k3').html(response.body);
                    });
                },
            });

        }
    </script>
@endpush