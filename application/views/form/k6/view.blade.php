<h4>K.6. KAWASAN KUMUH</h4>

@if($editable)
    @if(count($form->k6) == 0)
        <a class="btn btn-sm btn-success"
           onclick="k6Create(this)">Tambah</a>
    @endif
@endif

<table class="table table-striped table-bordered table-hover table-advance" id="table-k6">
    <thead>
    <tr>
        <th class="text-center">No.</th>
        @if($mode == '1a')
            <th class="text-center">Kabupaten/Kota</th>
        @elseif($mode == '1b')
            <th class="text-center">Kecamatan</th>
        @endif
        <th class="text-center">Luas wilayah kumuh <br/>(Ha)</th>
        <th class="text-center">Jumlah RTLH <br/>dalam wilayah kumuh <br/>(unit)</th>
        <th class="text-center">Sumber Data</th>
        @if($editable)

        <th class="text-center">Fungsi</th>
        @endif
    </tr>
    <tr>
        <th class="text-center small">(1)</th>
        <th class="text-center small">(2)</th>
        <th class="text-center small">(3)</th>
        <th class="text-center small">(4)</th>
        <th class="text-center small">(5)</th>
        @if($editable)

        <th class="text-center small">(6)</th>
        @endif
    </tr>
    </thead>

    <tfoot>
    <tr>
        <th></th>
        <th>Total</th>
        <th id="cell-k6-wilayah" class="text-right"></th>
        <th id="cell-k6-rtlh" class="text-right"></th>
        <th></th>
        @if($editable)

        <th></th>
        @endif
    </tr>
    </tfoot>
</table>

<div class="note note-info">
    <strong>Penjelasan:</strong>
    <p>Jumlah RTLH dalam kawasan kumuh dapat diperoleh dari Satker Kotaku, data ini untuk integrasi penanganan kumuh, Ditjen Penyediaan Perumahan dengan program BSPS atau Rusunawa, Ditjen Cipta Karya dengan penanganan infrastruktur di dalam satu kawasan. Uraian per Kabupaten/Kota</p></span>
</div>


@push('script')
    <script>
        $('#table-k6').DataTable({
            processing: true,
            serverSide: true,
            // stateSave: true,
            order: [],
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: '{{base_url("form-{$mode}/{$form->id}/k6/data")}}',
                type: 'POST',
            },
            columns: [
                {data: 'no', name: 'no', searchable: false, orderable: false, className: 'text-center'},
                {data: 'region', name: 'region', searchable: false, orderable: false, className: 'text-left'},
                {data: 'luas_wilayah_kumuh_4', name: 'luas_wilayah_kumuh_4', searchable: false, orderable: false, className: 'text-right'},
                {data: 'jumlah_rtlh_dalam_wilayah_kumuh_5', name: 'jumlah_rtlh_dalam_wilayah_kumuh_5', searchable: false, orderable: false, className: 'text-right'},
                {data: 'sumber_data_6', name: 'sumber_data_6', searchable: false, orderable: false},
                @if($editable)

                {data: 'fungsi', name: 'fungsi', searchable: false, orderable: false, className: 'text-center'},
                @endif
            ],
            drawCallback: function (settings) {
                let foot = settings.json.footer;
                $("#cell-k6-wilayah").html(foot.total_wilayah);
                $("#cell-k6-rtlh").html(foot.total_rtlh);
                // $("#cell-k5-rtlh-pemda").html(foot.total_rtlh_pemda);
            }
        });

        function k6Create(obj) {
            let $this = $(obj);

            $.get("{{base_url("form-{$mode}/{$form->id}/k6/create")}}", function (response) {
                $('#table-k6').DataTable().ajax.reload();

                $this.addClass('hidden');
            });
        }
    </script>
@endpush