<form action="{{base_url("form-{$mode}/{$form->id}/k6/{$k->id}/update")}}" method="post" data-table="table-k6">
    <div class="form-group">
        <label for="luas_wilayah_kumuh_4">Luas wilayah kumuh (Ha)</label>
        <input type="number" id="luas_wilayah_kumuh_4" name="luas_wilayah_kumuh_4" class="form-control" value="{{$k->luas_wilayah_kumuh_4}}">
    </div>

    <div class="form-group">
        <label for="jumlah_rtlh_dalam_wilayah_kumuh_5">Jumlah RTLH dalam wilayah kumuh (unit)</label>
        <input type="number" id="jumlah_rtlh_dalam_wilayah_kumuh_5" name="jumlah_rtlh_dalam_wilayah_kumuh_5" class="form-control" value="{{ (int)$k->jumlah_rtlh_dalam_wilayah_kumuh_5}}">
    </div>

    <div class="form-group">
        <label for="sumber_data_6">Sumber Data</label>
        <input type="text" id="sumber_data_6" name="sumber_data_6" class="form-control" value="{{$k->sumber_data_6}}">
    </div>

    <button type="button" class="btn btn-success" onclick="kModalSubmit(this)">Simpan</button>
</form>