<form action="{{base_url("form-{$mode}/{$form->id}/k2a/{$k->id}/update")}}" method="post" data-table="table-k2a">
    <div class="form-group">
        <label for="k231">
            @if($mode == '1b')
                @if(strtolower(trim($k->uraian1)) == 'total apbd provinsi')
                    Total APBD Kota/Kabupaten
                @elseif(strtolower(trim($k->uraian1)) == 'anggaran dinas pkp provinsi')
                    Anggaran Dinas PKP Kota/Kabupaten
                @else
                    {{$k->uraian1}}
                @endif
            @else
            {{$k->uraian1}}
            @endif
        </label>
        <input type="number" id="k231" name="k231" class="form-control" value="{{$k->k231}}">
    </div>

    <button type="button" class="btn btn-success" onclick="kModalSubmit(this)">Simpan</button>
</form>