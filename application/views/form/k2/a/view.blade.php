<p>1. Rasio Anggaran PKP Terhadap APBD</p>

<table class="table table-striped table-bordered table-hover table-advance" id="table-k2a">
    <thead>
    <tr>
        <th class="text-center">Uraian</th>
        <th class="text-center">Angka (Rp)</th>

        @if($editable)
        <th class="text-center">Fungsi</th>
        @endif
    </tr>
    <tr>
        <th class="text-center small">(1)</th>
        <th class="text-center small">(2)</th>
        @if($editable)

        <th class="text-center small">(3)</th>
        @endif

    </tr>
    </thead>
</table>

<div id="k2a-pkp-bigger-apbd" class="note note-danger hidden">
    <p><strong>Perhatian!</strong></p>
    <p>Alokasi anggaran dinas PKP provinsi lebih besar dari total APBD provinsi. Silakan perbaiki input Anda.</p>
</div>

<div class='note note-info'>
    <strong>Penjelasan:</strong>

    <p>Anggaran total APBD
        @if($mode == '1a')
            {{$dinas->provinsi->name}}
        @elseif($mode == '1b')
            {{$dinas->kabupatenKota->name}}
        @endif
        terhadap anggaran SKPD/Dinas PKP Provinsi dalam tahun yang sama,
        sehingga didapat persentase perbandingan anggaran urusan PKP, yaitu: {{$percentageK2A}} %</p>

</div>

@push('script')
    <script>
        $('#table-k2a').DataTable({
            processing: true,
            serverSide: true,
            // stateSave: true,
            searching: false,
            paging: false,
            info: false,
            order: [],
            ajax: {
                url: '{{base_url("form-{$mode}/{$form->id}/k2/data-a")}}',
                type: 'POST'
            },
            columns: [
                {data: 'uraian1', name: 'uraian1', orderable: false},
                {data: 'k231', name: 'k231', className: 'text-right', orderable: false},

                    @if($editable)

                {data: 'fungsi', name: 'fungsi', searchable: false, orderable: false, className: 'text-center'},
                @endif
            ],
            drawCallback: function (setting) {
                console.log(setting);
                let data = setting.json.data;
                let apbd = data[0];
                let pkp = data[1];

                if (pkp.k231_float > apbd.k231_float) {
                    $("#k2a-pkp-bigger-apbd").removeClass('hidden');
                } else {
                    $("#k2a-pkp-bigger-apbd").addClass('hidden');

                }


            }
        });
    </script>
@endpush