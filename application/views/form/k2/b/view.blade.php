<p>2. Uraian Program PKP</p>
@if($editable)

<a onclick="kModalEdit('{{base_url("form-{$mode}/{$form->id}/k2b/create")}}')" class="btn btn-success">Tambah Data</a>
@endif

<table class="table table-striped table-bordered table-hover table-advance" id="table-k2b">
    <thead>
    <tr>
        {{--<th class="text-center">No.</th>--}}
        <th class="text-center">Jenis Kegiatan Urusan PKP</th>
        <th class="text-center">Volume/Unit</th>
        <th class="text-center">Biaya (Rp)</th>

        @if($editable)

        <th class="text-center">Fungsi</th>
        @endif
    </tr>
    <tr>
        {{--<th  class="text-center small">(1)</th>--}}
        <th class="text-center small">(1)</th>
        <th class="text-center small">(2)</th>
        <th class="text-center small">(3)</th>

        @if($editable)

        <th class="text-center small">(4)</th>
        @endif
    </tr>
    </thead>

    <tfoot>
    <tr>
        <th>Total</th>
        <th id="cell-k2b-total-vol" class="text-right"></th>
        <th id="cell-k2b-total-budget" class="text-right"></th>

        @if($editable)

        <th></th>
        @endif

    </tr>
    </tfoot>
</table>

<div class="note note-info">
    <strong>Penjelasan:</strong>
    <p>Kegiatan fisik, biasanya rehabilitasi rumah berupa peningkatan kualitas, yang bersumber dari APBD
        Kab/Kota/anggaran Dinas PKP Kab/Kota, volumenya berupa unit, didata sebagai data pembangunan unit baru lingkup
        Program Suplai Perumahan</p>
</div>

@push('script')
    <script>


        $('#table-k2b').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: '{{base_url("form-{$mode}/{$form->id}/k2/data-b")}}',
                type: 'POST'
            },
            columns: [
                {data: 'jenis_kegiatan_urusan_pkp_2', name: 'jenis_kegiatan_urusan_pkp_2'},
                {data: 'ta_a_vol_unit_5', name: 'ta_a_vol_unit_5', className: 'text-right'},
                {data: 'ta_a_biaya_6', name: 'ta_a_biaya_6', className: 'text-right'},
                // {data: 'lihat', name: 'lihat', searchable: false, orderable: false},

                    @if($editable)

                {
                    data: 'fungsi', name: 'fungsi', searchable: false, orderable: false, className: 'text-center'
                },
                @endif
            ],
            drawCallback: function (settings) {
                let foot = settings.json.footer;
                $("#cell-k2b-total-vol").html(foot.total_volume);
                $("#cell-k2b-total-budget").html(foot.total_biaya);
            }
        });
    </script>
@endpush