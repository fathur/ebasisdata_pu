<form action="{{base_url("form-{$mode}/{$form->id}/k2b/store")}}" method="post" data-table="table-k2b">
    <div class="form-group">
        <label for="jenis_kegiatan_urusan_pkp_2">Jenis Kegiatan Urusan PKP</label>
        <input type="text" id="jenis_kegiatan_urusan_pkp_2" name="jenis_kegiatan_urusan_pkp_2" class="form-control">
    </div>

    <div class="form-group">
        <label for="ta_a_vol_unit_5">Volume/Unit</label>
        <input type="number" id="ta_a_vol_unit_5" name="ta_a_vol_unit_5" class="form-control">
    </div>

    <div class="form-group">
        <label for="ta_a_biaya_6">Biaya (Rp)</label>
        <input type="number" id="ta_a_biaya_6" name="ta_a_biaya_6" class="form-control">
    </div>

    <button type="button" class="btn btn-success" onclick="kModalSubmit(this)">Simpan</button>
</form>