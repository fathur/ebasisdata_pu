{{-- if belum di upload--}}

@if(is_null($form->formulir))
    @if($form->isComplete())

        <div id="uploader">

            <div class="note note-info">
                <p>Bukti pengesahan adalah lembar terakhir dari Form 1 yang telah dicetak dan
                    ditandatangani
                    oleh kepala dinas. Hasil cetak tersebut kemudian dipindai (<i>scan</i>) dan
                    disimpan dalam
                    format gambar (JPG, JPEG atau PNG) lalu diunggah pada bagian ini.</p>
            </div>

            <div class="note note-danger">
                <strong>PERHATIAN!</strong>
                <p>Proses pengunggahan bukti pengesahan hanya dapat dilakukan sekali dan tak dapat
                    diubah.
                    <br>Pastikan file yang akan Anda unggah sudah benar!</p>
            </div>

            <div class="col-progress">
                <div class="wrapper"></div>
            </div>

            <input type="file" class="simple-upload center-block " id="pengesahan-upload">

            <button type="button" class="btn btn-danger center-block margin-top-15" onclick="finishForm(this)">Selesai</button>
        </div>

        <div id="file-pengesahan"></div>

        {{--else attach dokuemnnya--}}
    @endif

@else
    <div id="file-pengesahan">

        <a href="{{base_url($form->formulir)}}" target="__blank" class="btn btn-primary">Unduh bukti pengesahan</a>
    </div>
@endif

{{--end--}}
