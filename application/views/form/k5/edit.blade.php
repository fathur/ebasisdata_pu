<form action="{{base_url("form-{$mode}/{$form->id}/k5/{$k->id}/update")}}" method="post" data-table="table-k5">
    <div class="form-group">
        <label for="jumlah_kk_rt_3">Jumlah KK/RT</label>
        <input type="number" id="jumlah_kk_rt_3" name="jumlah_kk_rt_3" class="form-control" value="{{$k->jumlah_kk_rt_3}}">
    </div>

    <div class="form-group">
        <label for="jumlah_rtlh_versi_bdt_4">Jumlah RTLH Versi BDT (unit)</label>
        <input type="number" id="jumlah_rtlh_versi_bdt_4" name="jumlah_rtlh_versi_bdt_4" class="form-control" value="{{$k->jumlah_rtlh_versi_bdt_4}}">
    </div>

    <div class="form-group">
        <label for="jumlah_rtlh_verifikasi_pemda_5">Jumlah RTLH Verifikasi Pemda (unit)</label>
        <input type="number" id="jumlah_rtlh_verifikasi_pemda_5" name="jumlah_rtlh_verifikasi_pemda_5" class="form-control" value="{{$k->jumlah_rtlh_verifikasi_pemda_5}}">
    </div>

    <div class="form-group">
        <label for="sumber_data_6">Sumber Data</label>
        <input type="text" id="sumber_data_6" name="sumber_data_6" class="form-control" value="{{$k->sumber_data_6}}">
    </div>

    <button type="button" class="btn btn-success" onclick="kModalSubmit(this)">Simpan</button>
</form>