<h4>K.5 RUMAH TIDAK LAYAK HUNI</h4>

@if($editable)
    @if(count($form->k5) == 0)
        <a class="btn btn-sm btn-success"
           onclick="k5Create(this)">Tambah</a>
    @endif
@endif

<table class="table table-striped table-bordered table-hover table-advance" id="table-k5">
    <thead>
    <tr>
        <th class="text-center">No.</th>
        @if($mode == '1a')
        <th class="text-center">Kabupaten/Kota</th>
        @elseif($mode == '1b')
            <th class="text-center">Kecamatan</th>
        @endif
        <th class="text-center">Jumlah KK/RT</th>
        <th class="text-center">Jumlah RTLH <br>Versi BDT <br>(unit)</th>
        <th class="text-center">Jumlah RTLH <br>Verifikasi Pemda <br>(unit)</th>
        <th class="text-center">Sumber Data</th>
        @if($editable)

        <th class="text-center">Fungsi</th>
        @endif
    </tr>
    <tr>
        <th class="text-center small">(1)</th>
        <th class="text-center small">(2)</th>
        <th class="text-center small">(3)</th>
        <th class="text-center small">(4)</th>
        <th class="text-center small">(5)</th>
        <th class="text-center small">(6)</th>
        @if($editable)

        <th  class="text-center small">(7)</th>
        @endif
    </tr>
    </thead>

    <tfoot>
    <tr>
        <th></th>
        <th>Total</th>
        <th id="cell-k5-kk"></th>
        <th id="cell-k5-rtlh-bdt"></th>
        <th id="cell-k5-rtlh-pemda"></th>
        <th></th>
        @if($editable)

        <th></th>
        @endif
    </tr>
    </tfoot>
</table>

<div class="note note-info">
    <strong>Penjelasan:</strong>
    <p>Data RTLH merupakan data yang sama dengan Sistem Informasi Rumah Swadaya,
        yang bersumber dari data olahan BDT 2015</p>
</div>


@push('script')
    <script>
       $('#table-k5').DataTable({
            processing: true,
            serverSide: true,
            // stateSave: true,
            order: [],
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: '{{base_url("form-{$mode}/{$form->id}/k5/data")}}',
                type: 'POST',
            },
            columns: [
                {data: 'no', name: 'no', searchable: false, orderable: false, className: 'text-center'},
                // {data: 'kabupaten_kota', name: 'kabupaten_kota', searchable: false, orderable: false},
                {data: 'region', name: 'region', searchable: false, orderable: false},
                {data: 'jumlah_kk_rt_3', name: 'jumlah_kk_rt_3', searchable: false, orderable: false, className: 'text-right'},
                {data: 'jumlah_rtlh_versi_bdt_4', name: 'jumlah_rtlh_versi_bdt_4', searchable: false, orderable: false, className: 'text-right'},
                {data: 'jumlah_rtlh_verifikasi_pemda_5', name: 'jumlah_rtlh_verifikasi_pemda_5', searchable: false, orderable: false, className: 'text-right'},
                {data: 'sumber_data_6', name: 'sumber_data_6', searchable: false, orderable: false},
                    @if($editable)

                {data: 'fungsi', name: 'fungsi', searchable: false, orderable: false, className: 'text-center'},
                @endif

            ],
            drawCallback: function (settings) {
                let foot = settings.json.footer;
                $("#cell-k5-kk").html(foot.total_kk);
                $("#cell-k5-rtlh-bdt").html(foot.total_rtlh_bdt);
                $("#cell-k5-rtlh-pemda").html(foot.total_rtlh_pemda);
            }
        });

       function k5Create(obj) {
           let $this = $(obj);

           $.get("{{base_url("form-{$mode}/{$form->id}/k5/create")}}", function (response) {
               $('#table-k5').DataTable().ajax.reload();

               $this.addClass('hidden');
           });
       }
    </script>
@endpush