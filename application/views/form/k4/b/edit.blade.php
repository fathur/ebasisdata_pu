@php
    $rumahI = "jenis{$i}";
    $jumlahI = "k423{$i}";
    $sumberI = "k424{$i}";
@endphp

<h4>{{ $k->{$rumahI} }}</h4>

<form action="{{base_url("form-{$mode}/{$form->id}/k4b/{$i}/update")}}" method="post" data-table="table-k4b">



    <input type="hidden" value="{{ $k->{$rumahI} }}" name="jenis">

    <div class="form-group">
        <label for="jumlah">Jumlah (unit)</label>
        <input type="number" id="jumlah" name="jumlah" class="form-control" value="{{ (int)$k->{$jumlahI} }}">
    </div>

    <div class="form-group">
        <label for="sumber">Sumber Data</label>
        <input type="text" id="sumber" name="sumber" class="form-control" value="{{ $k->{$sumberI} }}">
    </div>

    <button type="button" class="btn btn-success" onclick="kModalSubmit(this)">Simpan</button>
</form>