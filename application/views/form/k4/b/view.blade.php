
<p>2. Jenis Fisik Bangunan Rumah</p>

<table class="table table-striped table-bordered table-hover table-advance" id="table-k4b">

    <thead>
    <tr>
        <th class="text-center">No.</th>
        <th class="text-center">Jenis Fisik Bangunan</th>
        <th class="text-center">Jumlah (Unit)</th>
        <th class="text-center">Sumber Data</th>

        @if($editable)

        <th class="text-center">Fungsi</th>
        @endif
    </tr>
    <tr>
        <th  class="text-center small">(1)</th>
        <th  class="text-center small">(2)</th>
        <th  class="text-center small">(3)</th>
        <th  class="text-center small">(4)</th>
        @if($editable)

        <th  class="text-center small">(5)</th>
        @endif
    </tr>
    </thead>

    <tfoot>
        <tr>
            <th colspan="2">Total</th>
            <th id="cell-k4b-total" class="text-right"></th>
            <th></th>
            @if($editable)

            <th></th>
            @endif
        </tr>
    </tfoot>
</table>

<div class="note note-info">
    <strong>Penjelasan:</strong>
    <p>Jumlah fisik bangunan rumah, bukan data Rumah Tangga atau data Kepala Keluarga, tapi merupakan unit bangunan rumah baik terhuni maupun tidak. </p></span>
</div>

@push('script')
    <script>

        $('#table-k4b').DataTable({
            processing: true,
            serverSide: true,
            // stateSave: true,
            order: [],
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: '{{base_url("form-{$mode}/{$form->id}/k4/data-b")}}',
                type: 'POST'
            },
            columns: [
                {data: 'no', name: 'no', searchable: false, orderable: false, className: 'text-center'},
                {data: 'rumah', name: 'rumah', searchable: false, orderable: false},
                {data: 'jumlah', name: 'jumlah', searchable: false, orderable: false, className: 'text-right'},
                {data: 'sumber', name: 'sumber', searchable: false, orderable: false},

                @if($editable)

                {data: 'fungsi', name: 'fungsi', searchable: false, orderable: false, className: 'text-center'},
                @endif

            ],
            drawCallback: function (settings) {
                let foot = settings.json.footer;
                $("#cell-k4b-total").html(foot.total);
            }
        });

    </script>
@endpush