
<p>3. Jumlah Kepala Keluarga (KK) dalam Satu Rumah</p>

<table class="table table-striped table-bordered table-hover table-advance" id="table-k4c">

    <thead>
    <tr>
        <th class="text-center">No.</th>
        <th class="text-center">Fungsi Rumah</th>
        <th class="text-center">Jumlah (Unit)</th>
        <th class="text-center">Sumber Data</th>
        @if($editable)

        <th class="text-center">Fungsi</th>
        @endif
    </tr>
    <tr>
        <th  class="text-center small">(1)</th>
        <th  class="text-center small">(2)</th>
        <th  class="text-center small">(3)</th>
        <th  class="text-center small">(4)</th>
        @if($editable)

        <th  class="text-center small">(5)</th>
        @endif
    </tr>
    </thead>

    <tfoot>

    </tfoot>
</table>

<div class="note note-info">
    <strong>Penjelasan:</strong>
    <p>Jumlah KK lebih dari 1 (satu) dalam satu rumah merupakan bagian dari data backlog
        penghunian.</p>
</div>

@push('script')
    <script>

        $('#table-k4c').DataTable({
            processing: true,
            serverSide: true,
            // stateSave: true,
            order: [],
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: '{{base_url("form-{$mode}/{$form->id}/k4/data-c")}}',
                type: 'POST'
            },
            columns: [
                {data: 'no', name: 'no', searchable: false, orderable: false, className: 'text-center'},
                {data: 'fungsi', name: 'fungsi', searchable: false, orderable: false},
                {data: 'jumlah', name: 'jumlah', searchable: false, orderable: false, className: 'text-right'},
                {data: 'sumber', name: 'sumber', searchable: false, orderable: false},
                    @if($editable)

                {data: 'action', name: 'action', searchable: false, orderable: false, className: 'text-center'},
                @endif

            ]
        });

    </script>
@endpush