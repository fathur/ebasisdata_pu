@php
    $fungsiI = "fungsi{$i}";
    $jumlahI = "k433{$i}";
    $sumberI = "k434{$i}";
@endphp

<h4>{{ $k->{$fungsiI} }}</h4>

<form action="{{base_url("form-{$mode}/{$form->id}/k4c/{$i}/update")}}" method="post" data-table="table-k4c">



    <input type="hidden" value="{{ $k->{$fungsiI} }}" name="jenis">

    <div class="form-group">
        <label for="jumlah">Jumlah (unit)</label>
        <input type="number" id="jumlah" name="jumlah" class="form-control" value="{{ (int)$k->{$jumlahI} }}">
    </div>

    <div class="form-group">
        <label for="sumber">Sumber Data</label>
        <input type="text" id="sumber" name="sumber" class="form-control" value="{{ $k->{$sumberI} }}">
    </div>

    <button type="button" class="btn btn-success" onclick="kModalSubmit(this)">Simpan</button>
</form>