<p>1. Jumlah Rumah Berdasarkan Status Kepemilikan Tempat Tinggal</p>

<table class="table table-striped table-bordered table-hover table-advance" id="table-k4a">

    <thead>
    <tr>
        <th class="text-center">No.</th>
        <th class="text-center">Status Kepemilikan Tempat Tinggal</th>
        <th class="text-center">Jumlah (unit)</th>
        <th class="text-center">Sumber Data</th>

        @if($editable)
        <th class="text-center">Fungsi</th>
        @endif

    </tr>
    <tr>
        <th  class="text-center small">(1)</th>
        <th  class="text-center small">(2)</th>
        <th  class="text-center small">(3)</th>
        <th  class="text-center small">(4)</th>

        @if($editable)

        <th  class="text-center small">(5)</th>
        @endif

    </tr>
    </thead>

    <tfoot>
    <tr>

        <th colspan="2" class="text-right">Total A (2+3+4+5)</th>
        <th id="cell-k4a-total-a" class="text-right"></th>
        <th></th>
        @if($editable)

        <th></th>
        @endif

    </tr>

    <tr>

        <th colspan="2" class="text-right">Total B (1+2+4)</th>
        <th id="cell-k4a-total-b" class="text-right"></th>
        <th></th>
        @if($editable)

        <th></th>
        @endif
    </tr>

    <tr>

        <th colspan="2" class="text-right">Total Seluruhnya</th>
        <th id="cell-k4a-total" class="text-right"></th>
        <th></th>
        @if($editable)

        <th></th>
        @endif

    </tr>
    </tfoot>
</table>

<p>Backlog kepemilikan = (Total A) = <span id="span-k4a-total-a"></span> unit</p>
<p>Backlog penghunian = (Total B) = <span id="span-k4a-total-b"></span> unit</p>

<div class="note note-info">
    <strong>Penjelasan:</strong>
    <p>Data backlog kepemilikan dan backlog penghunian kabupaten ditentukan oleh Dinas PKP,
        data dari BPS dan BKKBN hanya sebagai benchmark data yang perlu divalidasi oleh Dinas PKP.</p>
</div>

@push('script')
    <script>
        let k4a = $('#table-k4a').DataTable({
            processing: true,
            serverSide: true,
            // stateSave: true,
            order: [],
            searching: false,
            paging: false,
            info: false,
            ajax: {
                url: '{{base_url("form-{$mode}/{$form->id}/k4/data-a")}}',
                type: 'POST',
            },
            columns: [
                {data: 'no', name: 'no', searchable: false, orderable: false, className: 'text-center'},
                {data: 'status', name: 'status', searchable: false, orderable: false, className: 'text-left'},
                {data: 'jumlah', name: 'jumlah', searchable: false, orderable: false, className: 'text-right'},
                {data: 'sumber', name: 'sumber', searchable: false, orderable: false, className: 'text-left'},

                @if($editable)
                {data: 'fungsi', name: 'fungsi', searchable: false, orderable: false, className: 'text-center'},
                @endif

            ],
            drawCallback: function (settings) {
                console.log(settings.json.footer)
                let foot = settings.json.footer;
                $("#cell-k4a-total-a").html(foot.total_a);
                $("#cell-k4a-total-b").html(foot.total_b);
                $("#cell-k4a-total").html(foot.total);

                $('#span-k4a-total-a').html(foot.total_a);
                $('#span-k4a-total-b').html(foot.total_b);
            }
        });

        // k4a.on( 'order.dt search.dt', function () {
        //     t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //         cell.innerHTML = i+1;
        //     } );
        // } ).draw();
    </script>
@endpush