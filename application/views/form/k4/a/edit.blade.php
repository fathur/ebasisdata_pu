@php
    $statusI = "status{$i}";
    $jumlahI = "k414{$i}";
    $sumberI = "k415{$i}";
@endphp

<p>{{ $k->{$statusI} }}</p>

<form action="{{base_url("form-{$mode}/{$form->id}/k4a/{$i}/update")}}" method="post" data-table="table-k4a">



    <input type="hidden" value="{{ $k->{$statusI} }}" name="status">

    <div class="form-group">
        <label for="jumlah">Jumlah (unit)</label>
        <input type="number" id="jumlah" name="jumlah" class="form-control" value="{{ $k->{$jumlahI} }}">
    </div>

    <div class="form-group">
        <label for="sumber">Sumber Data</label>
        <input type="text" id="sumber" name="sumber" class="form-control" value="{{ $k->{$sumberI} }}">
    </div>

    <button type="button" class="btn btn-success" onclick="kModalSubmit(this)">Simpan</button>
</form>