<form action="{{base_url("form-{$mode}/{$form->id}/k4e/{$k->id}/update")}}" method="post" data-table="table-k4e">

    <h4>Jumlah Pembangunan Rumah Berdasarkan IMB (unit)</h4>
    <div class="form-group">
        <label for="non_mbr_2">Non-MBR</label>
        <input type="number" id="non_mbr_2" name="non_mbr_2" class="form-control" value="{{ (int)$k->non_mbr_2 }}">
    </div>

    <div class="form-group">
        <label for="mbr_3">MBR</label>
        <input type="text" id="mbr_3" name="mbr_3" class="form-control" value="{{ (int)$k->mbr_3 }}">
    </div>

    <h4>Jumlah Pembangunan Rumah Berdasarkan Non-IMB (unit)</h4>

    <div class="form-group">
        <label for="non_mbr_4">Non-MBR</label>
        <input type="text" id="non_mbr_4" name="non_mbr_4" class="form-control" value="{{ (int)$k->non_mbr_4 }}">
    </div>

    <div class="form-group">
        <label for="mbr_5">MBR</label>
        <input type="text" id="mbr_5" name="mbr_5" class="form-control" value="{{ (int)$k->mbr_5 }}">
    </div>

    <div class="form-group">
        <label for="sumber_data_8">Sumber Data</label>
        <input type="text" id="sumber_data_8" name="sumber_data_8" class="form-control" value="{{ $k->sumber_data_8 }}">
    </div>

    <button type="button" class="btn btn-success" onclick="kModalSubmit(this)">Simpan</button>
</form>