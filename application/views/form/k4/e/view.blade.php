<p>5. Jumlah Pembangunan Rumah dalam 1 Tahun</p>

@if($editable)
    @if(!$form->k4e)
        <a class="btn btn-sm btn-success"
           onclick="k4eCreate(this)">Tambah</a>
    @endif
@endif

<table class="table table-striped table-bordered table-hover table-advance" id="table-k4e">

    <thead>
    <tr>
        <th class="text-center" colspan="2">Jumlah Pembangunan Rumah <br> Berdasarkan IMB <br>(unit)</th>
        <th class="text-center" colspan="2">Jumlah Pembangunan Rumah <br>Berdasarkan Non-IMB <br>(unit)</th>
        <th class="text-center" rowspan="2">Sumber Data</th>
        @if($editable)
            <th class="text-center">Fungsi</th>
        @endif
    </tr>
    <tr>
        <th  class="text-center">Non-MBR</th>
        <th  class="text-center">MBR</th>
        <th  class="text-center">Non-MBR</th>
        <th  class="text-center">MBR</th>
        @if($editable)
        <th></th>
        @endif
    </tr>
    </thead>

    <tfoot>

    </tfoot>
</table>

<div class="note note-info">
    <strong>Penjelasan:</strong>
    <p>Data pembangunan unit rumah per tahun oleh seluruh stakeholder perumahan
        terdata dalam IMB melalui Dinas PTSP, dan jumlah yang tidak terdata dalam
        IMB dianalisis/diasumsikan oleh Dinas PKP, sehingga didapat perkiraan
        pembangunan unit rumah satu kabupaten dalam satu tahun. Ini disebut data
        supply perumahan. Data total dari seluruh Kabupaten/Kota</p>
</div>


@push('script')
    <script>


        $('#table-k4e').DataTable({
            processing: true,
            serverSide: true,
            // stateSave: true,
            searching: false,
            paging: false,
            info: false,
            order: [],
            ajax: {
                url: '{{base_url("form-{$mode}/{$form->id}/k4/data-e")}}',
                type: 'POST'
            },
            columns: [
                {data: 'non_mbr_2', name: 'non_mbr_2', className: 'text-right', orderable: false, searchable: false},
                {data: 'mbr_3', name: 'mbr_3', className: 'text-right', orderable: false, searchable: false},
                {data: 'non_mbr_4', name: 'non_mbr_4', className: 'text-right', orderable: false, searchable: false},
                {data: 'mbr_5', name: 'mbr_5', className: 'text-right', orderable: false, searchable: false},
                {data: 'sumber_data_8', name: 'sumber_data_8', className: 'text-left', orderable: false, searchable: false},

                @if($editable)
                {data: 'fungsi', name: 'fungsi', className: 'text-left', orderable: false, searchable: false},
                @endif

            ]
        });

        function k4eCreate(obj) {
            let $this = $(obj);

            $.get("{{base_url("form-{$mode}/{$form->id}/k4e/create")}}", function (response) {
                $('#table-k4e').DataTable().ajax.reload();

                $this.addClass('hidden');
            });
        }
    </script>
@endpush