<p>4. Jumlah Sambungan Listrik Rumah (PLN)</p>

@if($editable)
    @if(count($form->k4d) == 0)
        <a class="btn btn-sm btn-success"
           onclick="k4dCreate(this)">Tambah</a>
    @endif
@endif

<table class="table table-striped table-bordered table-hover table-advance" id="table-k4d">

    <thead>
    <tr>
        <th class="text-center">No.</th>

        @if($mode == '1a')
            <th class="text-center">Kabupaten/Kota</th>
        @elseif($mode == '1b')
            <th class="text-center">Kecamatan</th>
        @endif

        <th class="text-center">Jumlah (Unit)</th>
        <th class="text-center">Sumber Data</th>
        @if($editable)
            <th class="text-center">Fungsi</th>
        @endif
    </tr>
    <tr>
        <th  class="text-center small">(1)</th>
        <th  class="text-center small">(2)</th>
        <th  class="text-center small">(3)</th>
        <th  class="text-center small">(4)</th>
        @if($editable)

        <th  class="text-center small">(5)</th>
        @endif
    </tr>
    </thead>

    <tfoot>
    <tr>
        <th></th>
        <th class="text-right">Total</th>
        <th id="cell-k4d-total" class="text-right"></th>
        <th></th>
        @if($editable)

        <th></th>
        @endif
    </tr>

    </tfoot>
</table>

<div class="note note-info">
    <strong>Penjelasan:</strong>
    <p>Data sambungan listrik berasal dari PLN, per-kabupaten/kota.</p>
</div>


@push('script')
    <script>

        $('#table-k4d').DataTable({
            processing: true,
            serverSide: true,
            // stateSave: true,
            searching: false,
            paging: false,
            info: false,
            order: [],
            ajax: {
                url: '{{base_url("form-{$mode}/{$form->id}/k4/data-d")}}',
                type: 'POST'
            },
            columns: [
                {data: 'id', name: 'idform_1_k4_4', searchable: false, orderable: false, className: 'text-center'},
                {data: 'region', name: 'region', searchable: false, orderable: false},
                {data: 'jumlah_rumah_4', name: 'jumlah_rumah_4', className: 'text-right', orderable: false},
                {data: 'sumber_data_5', name: 'sumber_data_5', className: 'text-right', orderable: false},
                @if($editable)

                {data: 'fungsi', name: 'fungsi', className: 'text-center', orderable: false},
                @endif

            ],
            drawCallback: function (settings) {
                let foot = settings.json.footer;
                $("#cell-k4d-total").html(foot.total);
            }
        });

        function k4dCreate(obj) {
            let $this = $(obj);

            $.get("{{base_url("form-{$mode}/{$form->id}/k4d/create")}}", function (response) {
                $('#table-k4d').DataTable().ajax.reload();

                $this.addClass('hidden');
            });
        }
    </script>
@endpush