<form action="{{base_url("form-{$mode}/{$form->id}/k4d/{$k->id}/update")}}" method="post" data-table="table-k4d">
    <div class="form-group">
        <label for="jumlah_rumah_4">Jumlah (unit)</label>
        <input type="number" id="jumlah_rumah_4" name="jumlah_rumah_4" class="form-control" value="{{$k->jumlah_rumah_4}}">
    </div>

    <div class="form-group">
        <label for="sumber_data_5">Sumber Data</label>
        <input type="text" id="sumber_data_5" name="sumber_data_5" class="form-control" value="{{$k->sumber_data_5}}">
    </div>

    <button type="button" class="btn btn-success" onclick="kModalSubmit(this)">Simpan</button>
</form>