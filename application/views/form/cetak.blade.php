@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="actions">
                        <div class="clearfix"></div>

                        <a href="javascript:window.print()" class="btn btn-circle btn-default btn-icon-only" title="Cetak">
                            <i class="fa fa-print"></i>
                        </a>
                    </div>
                </div>

                <div class="portlet-body">
                    {{--{{dd($dinas->kabupatenKota)}}--}}

                    <table id="user" class="table table-bordered table-striped" style="margin-bottom: 5px;">
                        <tbody>
                        <tr>
                            @if($mode == '1a')
                                <td style="width: 15%">Provinsi</td>
                                <td style="width: 50%">{{$dinas->provinsi->name}}</td>
                            @elseif($mode == '1b')
                                <td style="width: 15%">Kabupaten/Kota</td>
                                <td style="width: 50%">{{$dinas->kabupatenKota->name}}</td>
                            @endif

                            <td style="width:35%; vertical-align:middle; text-align:center" rowspan="3">
                                <h3>{{$title}}</h3>
                            </td>
                        </tr>

                        <tr>
                            <td>Nama Dinas</td>
                            <td>{{$dinas->nama_dinas}}</td>
                        </tr>

                        <tr>
                            <td>Alamat</td>
                            <td>{{$dinas->alamat}}</td>
                        </tr>
                        </tbody>
                    </table>

                    <h3 class="uppercase bold text-center">

                        DAFTAR ISIAN PENDATAAN PKP

                        @if($mode == '1a')
                            PROVINSI {{$dinas->provinsi->name}}
                        @elseif($mode == '1b')
                            {{$dinas->kabupatenKota->name}}
                        @endif

                        TAHUN {{$form->tahun}}
                    </h3>

                    <div class="note note-info">
                        <p>Kegiatan pendataan perumahan ini dilakukan untuk memenuhi kebutuhan data dan
                            informasi perumahan. Data ini merupakan bagian dari Pembangunan Basis Data PKP
                            Kabupaten/Kota sesuai UU No. 1 Tahun 2011 tentang Perumahan dan Kawasan Permukiman (PKP)
                            pasal 17 dan Peraturan Pemerintah No. 88 Tahun 2014 tentang Pembinaan Penyelenggaraan
                            PKP pasal 18 ayat 1.</p>
                        <p>Tabel ini diisi oleh SNVT bidang Perumahan Kabupaten/Kota, untuk kelengkapan datanya
                            berkoordinasi dengan Pokja PKP Kabupaten/Kota, BPS Kabupaten/Kota, Bappeda Kabupaten/Kota,
                            Dinas
                            Sosial Kabupaten/Kota, BKKBN Kabupaten/Kota, serta rekapitulasi data dari Form 1B oleh
                            Kabupaten/Kota.
                            Untuk pertanyaan lebih lanjut dapat menghubungi Subdit Data dan Informasi, Direktorat
                            Perencanaan Penyediaan Perumahan: <a href="mailto:datinperumahan@gmail.com">datinperumahan@gmail.com</a>
                            atau Telp./Fax. 021-7211883.</p>
                        <p>Daftar isian ini merupakan data sekunder hasil dari berbagai kegiatan pendataan
                            perumahan, terdiri dari:</p>
                        <ul>
                            <li>K.1 Kelembagaan Perumahan dan Permukiman</li>
                            <li>K.2 Alokasi APBD untuk Urusan Perumahan dan Kawasan Permukiman (PKP)</li>
                            <li>K.3 Pembangunan Perumahan</li>
                            <li>K.4 Backlog Perumahan</li>
                            <li>K.5 Rumah Tidak Layak Huni</li>
                            <li>K.6 Kawasan Kumuh</li>
                        </ul>


                    </div>

                    <div class="form-1">
                        <div class="row page-break">
                            <div class="col-md-12">
                                @include('form.k1')
                            </div>
                        </div>


                        <div class="row page-break">
                            <div class="col-md-12">
                                @include('form.k2')
                            </div>
                        </div>


                        <div class="row page-break">
                            <div class="col-md-12">
                                @include('form.k3')
                            </div>
                        </div>


                        <div class="row page-break">
                            <div class="col-md-12">
                                @include('form.k4')
                            </div>
                        </div>

                        <div class="row page-break">
                            <div class="col-md-12">
                                @include('form.k5.view')
                            </div>
                        </div>


                        <div class="row page-break">
                            <div class="col-md-12">
                                @include('form.k6.view')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css"
          href="{{base_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
@endpush

@push('scripts')
    <script type="text/javascript"
            src="{{base_url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{base_url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>

    {{--<script src="{{base_url('assets/js/helperDataTable.js')}}"></script>--}}
@endpush

@push('style')
    <style>
        tfoot {background-color: #dddddd;}
        h4 {font-weight: bolder;}

        @media print {
            .page-break {
                page-break-before: always;
            }
        }

    </style>
@endpush

@push('script')
    <script>

    </script>
@endpush