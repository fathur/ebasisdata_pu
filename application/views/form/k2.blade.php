<h4>K.2 ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN PERMUKIMAN (PKP)</h4>

<div class="row">
    <div class="col-md-12">
        @include('form.k2.a.view')
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        @include('form.k2.b.view')
    </div>
</div>
