<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 foreach($form_pengembang_view_detail as $r):
     $idform_1=$r->idpengembang;
     $bulan=$r->bulan;
 endforeach;
?>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?php echo $site_title;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
<script type="text/javascript">
function tambah_imb_modal()
{
	  document.getElementById('perumahan').value="";
	  document.getElementById('nama_pengembang').value="";
	  document.getElementById('alamat').value="";
	  document.getElementById('bentuk_rumah').value="";
	  document.getElementById('luas').value="";
	  document.getElementById('tipe').value="";
	  document.getElementById('jumlah_mbr').value="";
	  document.getElementById('jumlah_non_mbr').value="";
	  document.getElementById('idform_satu_juta_rumah_imb').value="";
    document.getElementById('imb_f').action="<?php echo base_url('main/fimb_insert');?>";
}
function ubah_imb_modal(a,b,c,d,e,f,g,h,i)
{
	  document.getElementById('perumahan').value=a;
	  document.getElementById('nama_pengembang').value=b;
	  document.getElementById('alamat').value=c;
	  document.getElementById('bentuk_rumah').value=d;
	  document.getElementById('luas').value=e;
	  document.getElementById('tipe').value=f;
	  document.getElementById('jumlah_mbr').value=g;
	  document.getElementById('jumlah_non_mbr').value=h;
	  document.getElementById('idform_satu_juta_rumah_imb').value=i;
    document.getElementById('imb_f').action="<?php echo base_url('main/fimb_update');?>";
}
function confirmDelete1(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_fimb');?>/"+delUrl;
  }
}
function tambah_imb_unit_modal()
{
	  document.getElementById('idkabupaten_kota').value="";
	  document.getElementById('jumlah_mbr2').value="";
	  document.getElementById('jumlah_non_mbr2').value="";
	  document.getElementById('idform_satu_juta_rumah_unit').value="";
    document.getElementById('imb_unit_f').action="<?php echo base_url('main/fimb_unit_insert');?>";
}
function ubah_imb_unit_modal(a,b,c,d)
{
	  document.getElementById('idkabupaten_kota').value=a;
	  document.getElementById('jumlah_mbr2').value=b;
	  document.getElementById('jumlah_non_mbr2').value=c;
	  document.getElementById('idform_satu_juta_rumah_unit').value=d;
    document.getElementById('imb_unit_f').action="<?php echo base_url('main/fimb_unit_update');?>";
}
function confirmDelete2(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_fimb_unit');?>/"+delUrl;
  }
}
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php include "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
        <div id="fimb_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
            <form action="" class="form-horizontal" method="post" id="imb_f"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title"><strong>TABEL DATA PEMBANGUNAN UNIT RUMAH BERDASARKAN IMB PERUMAHAN<strong></h5>
										</div>
										<div class="modal-body">
											<p>
										<table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td><strong>Perumahan</strong></td>
											<td align="center">
                        <input name="idform_satu_juta_rumah" id="idform_satu_juta_rumah" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                        <input name="idform_satu_juta_rumah_imb" id="idform_satu_juta_rumah_imb" type="hidden" class="form-control" />
                        <input name="perumahan"  id="perumahan" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_perumahan"></p>
                      </td>
										</tr>
                    <tr>
											<td><strong>Pengembang</strong></td>
											<td align="center"><input name="nama_pengembang" id="nama_pengembang" type="text" class="form-control"  style="background-color:#E8F3FF" /><p id="val_nama_pengembang"></p></td>
										</tr>
                    <tr>
											<td><strong>Alamat Lokasi</strong></td>
											<td align="center" ><input name="alamat" id="alamat" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_alamat"></p></td>
										</tr>
                    <tr>
											<td><strong>Bentuk Rumah</strong></td>
											<td align="left"> 
                         
  <select name="bentuk_rumah" id="bentuk_rumah" class="form-control" style="background-color:#E8F3FF">
                          <option value="Tapak"  style="background-color:#E8F3FF" class="form-control">Tapak</option>
                          <option value="Susun"  style="background-color:#E8F3FF" class="form-control">Susun</option>
                        </select>
  <p id="val_bentuk_rumah"></p></td>
										</tr>
                    <tr>
											<td><strong>Luas Lahan</strong></td>
											<td align="center"><input name="luas" id="luas" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_luas"></p></td>
										</tr>
                    <tr>
											<td><strong>Type</strong></td>
											<td align="center"><input name="tipe" id="tipe" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_tipe"></p></td>
										</tr>
                    <tr>
											<td><strong>Unit MBR</strong></td>
											<td align="center"><input name="jumlah_mbr" id="jumlah_mbr" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_jumlah_mbr"></p></td>
										</tr>
                    <tr>
											<td><strong>Unit Non MBR</strong></td>
											<td align="center"><input name="jumlah_non_mbr" id="jumlah_non_mbr" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_jumlah_non_mbr"></p></td>
										</tr>
							  		</tbody>
									</table>
											</p>
										</div>
										<div class="modal-footer">
                      <button type="button" class="btn green" onClick="val_imb1()">Simpan</button>
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>
									</div>
								</div>
								</form>
							</div>
              <div id="fimb_unit_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                <form action="" class="form-horizontal" method="post" id="imb_unit_f"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title"><strong>TABEL DATA PEMBANGUNAN UNIT RUMAH BERDASARKAN IMB PERORANGAN<strong></h5>
										</div>
										<div class="modal-body">
											<p>
										<table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td><strong>Kabupaten/Kota</strong></td>
											<td align="center">
                        <input name="idform_satu_juta_rumah" id="idform_satu_juta_rumah" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                        <input name="idform_satu_juta_rumah_unit" id="idform_satu_juta_rumah_unit" type="hidden" class="form-control" />
                        <select name="idkabupaten_kota" id="idkabupaten_kota" class="form-control" style="background-color:#E8F3FF"> 
                        <option value="0">Pilih Provinsi</option>
                        <?php foreach($kabupaten_kota as $kk): ?>
                        <option value="<?php echo $kk->idkabupaten_kota;?>"><?php echo  $kk->kabupaten_kota;?></option>
  											<?php endforeach;?>
                        </select>
                      <p id="val_idkabupaten_kota"></p></td>
										</tr>
                    <tr>
											<td><strong>UNIT MBR</strong></td>
											<td align="center"><input name="jumlah_mbr2" id="jumlah_mbr2" type="text" class="form-control"  style="background-color:#E8F3FF" /><p id="val_jumlah_mbr2"></p></td>
										</tr>
                    <tr>
											<td><strong>UNIT NON-MBR</strong></td>
											<td align="center" ><input name="jumlah_non_mbr2" id="jumlah_non_mbr2" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_jumlah_non_mbr2"></p></td>
										</tr>
							  		</tbody>
									</table>
											</p>
										</div>
										<div class="modal-footer">
                      <button type="button" class="btn green" onClick="val_imb2()">Simpan</button>
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>
									</div>
								</div>
								</form>
							</div>
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?php echo base_url('main/index'); ?>">Home</a><i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Formulir Suplai Perumahan<i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Formulir IMB
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->

			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-list font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase">Laporan Bulanan Program Suplai Perumahan Tahun 2017 Berdasarkan IMB</span>
							</div>
							<div class="tools">
								<a href="" class="collapse" data-original-title="" title=""></a>
								<a href="" class="reload" data-original-title="" title=""></a>
								<a href="" class="fullscreen" data-original-title="" title=""></a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="btn-group">
										</div>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="btn-group pull-right">
											<button type="button" class="btn default"  onclick="window.print()"> Cetak <i class="fa fa-angle-down"></i></button>
												 
										</div>
									</div>
								</div>
							</div>
              <!-- TABEL KEPALA -->
              <div class="col-md-6 col-sm-12" style="padding-left:0">
                <table class="table table-bordered">
                  <tr>
                    <td width="30%">Provinsi</td>
                    <td width="70%"><?php echo $provinsi;?></td>
                  </tr>
                  <tr>
                    <td>Nama Instansi</td>
                    <td><?php echo $nama_dinas;?></td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td><?php echo $alamat;?></td>
                  </tr>
                  <tr>
                    <td>Bulan</td>
                    <td>
                       
                          <?php echo ($bulan);?>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="note note-success">
                  Data berdasarkan pengajuan IMB oleh pengembang perumahan maupun oleh perorangan/masyarakat,
                  diasumsikan sebagai jumlah unit rumah terbangun di kota/kabupaten, terutama jumlah unit rumah
                  yang dibangun oleh masyarakat secara swadaya. Diasumsikan tipe bangunan max 36 m2 adalah MBR.
                </div>
              </div>
              <!-- END OF TABEL KEPALA -->
							<!-- TABEL IMB PERUMAHAN DIMULAI -->
              <div class="col-md-12" style="padding-left:0"><h3>Tabel Data Pembangunan Unit Rumah Berdasarkan IMB Perumahan</h3></div> 
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="text-align:center" width="3%">No</th>
                    <th style="text-align:center" width="15%">Perumahan</th>
                    <th style="text-align:center" width="15%">Pengembang</th>
                    <th style="text-align:center" width="25%">Alamat Lokasi</th>
                    <th style="text-align:center" width="9%">Bentuk Rumah</th>
                    <th style="text-align:center" width="7%">Luas Lahan</th>
                    <th style="text-align:center" width="6%">Tipe (LB/LT)</th>
                    <th style="text-align:center" width="10%">Unit MBR</th>
                    <th style="text-align:center" width="10%">Unit Non-MBR</th> 
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($form_pengembang_view_detail as $f): ?>
                  <tr>
                    <td><?php $i++; echo $i;?>.</td>
                    <td><?php echo $f->perumahan;?></td>
                    <td><?php echo $f->nama_pengembang;?></td>
                    <td><?php echo $f->alamat;?></td>
                    <td><?php echo $f->bentuk_rumah;?></td>
                    <td><?php echo number_format($f->luas, 0 , ',' , '.' );?></td>
                    <td><?php echo $f->tipe;?></td>
                    <td><?php echo number_format($f->jumlah_mbr, 0 , ',' , '.' );?></td>
                    <td><?php echo number_format($f->jumlah_non_mbr, 0 , ',' , '.' );?></td> 
                  </tr>
                  <?php endforeach;?>
                </tbody>
              </table>
							<!-- TABEL IMB PERUMAHAN SELESAI -->
              <!-- TABEL IMB PERORANGAN DIMULAI -->
              <div class="col-md-12" style="padding-left:0"><h3>Tabel Data Pembangunan Unit Rumah Berdasarkan IMB Perorangan</h3></div> 
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="text-align:center" width="3%">No</th>
                    <th style="text-align:center" width="15%">Kabupaten/Kota</th>
                    <th style="text-align:center" width="10%">Unit MBR</th>
                    <th style="text-align:center" width="10%">Unit Non-MBR</th> 
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($form_satu_juta_rumah_unit_view_detail as $f): ?>
                  <tr>
                    <td><?php $i++; echo $i;?>.</td>
                    <td><?php echo $f->kabupaten_kota;?></td>
                    <td><?php echo number_format($f->jumlah_mbr, 0 , ',' , '.' );?></td>
                    <td><?php echo number_format($f->jumlah_non_mbr, 0 , ',' , '.' );?></td>
                     
                  </tr>
                <?php endforeach;?>
                </tbody>
              </table>
							<!-- TABEL IMB PERORANGAN SELESAI -->
              <div class="note note-warning">
                Data ini merupakan bagian dari Pembangunan Basis Data PKP Pemkab/Pemkot
                sesuai UU No.1 Tahun 2011 tentang Perumahan dan Kawasan Permukiman (PKP)
                pasal 18 dan Peraturan Pemerintah No.88 Tahun 2014 tentang Pembinaan
                Penyelenggaraan PKP pasal 18 ayat 1.
                Tabel ini diisi oleh Badan Pelayanan IMB Kab/Kota dan dikembalikan
                ke SNVT bidang Perumahan Provinsi melalui Dinas PKP Kab/Kota,
                untuk pertanyaan lebih lanjut dapat menghubungi Subdit Data dan Informasi,
                Direktorat Perencanaan Penyediaan Perumahan: datinperumahan@gmail.com (021-7211883)
              </div>
              <div> 
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END PAGE CONTAINER -->
<?php include "footer2.php";?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-contextmenu/bootstrap-contextmenu.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
	 Layout.init(); // init current layout
	 Demo.init(); // init demo features
   TableManaged.init();
});

function val_imb1() {
    var perumahan,nama_pengembang,alamat,bentuk_rumah,luas,tipe,jumlah_mbr,jumlah_non_mbr;
    perumahan = document.getElementById("perumahan").value;
    nama_pengembang = document.getElementById("nama_pengembang").value;
    alamat = document.getElementById("alamat").value;
    bentuk_rumah = document.getElementById("bentuk_rumah").selectedindex;
    luas = document.getElementById("luas").value;
    tipe = document.getElementById("tipe").value;
    jumlah_mbr = document.getElementById("jumlah_mbr").value; 
    jumlah_non_mbr = document.getElementById("jumlah_non_mbr").value;
    //if (isNaN(x) || x < 1 || x > 10) {
    if ((perumahan=="")) {
        document.getElementById("val_perumahan").innerHTML = "Masukan Data Perumahan";
    } else if (nama_pengembang=="") {
        document.getElementById("val_nama_pengembang").innerHTML = "Masukan Nama Pengembang";
    } else if (alamat=="") {
        document.getElementById("val_alamat").innerHTML = "Masukan Alamat";
	} else if ((bentuk_rumah)==0) {
        document.getElementById("val_bentuk_rumah").innerHTML = "Pilih Salah Satu";
    } else if (isNaN(luas) || luas < 0) {
        document.getElementById("val_luas").innerHTML = "Input Salah"; 
    } else if (tipe=="") {
        document.getElementById("val_tipe").innerHTML = "Masukan Tipe"; 
    } else if (isNaN(jumlah_mbr) || jumlah_mbr < 0) {
        document.getElementById("val_jumlah_mbr").innerHTML = "Input Salah"; 
    } else if (isNaN(jumlah_non_mbr) || jumlah_non_mbr < 0) {
        document.getElementById("val_jumlah_non_mbr").innerHTML = "Input Salah";  
	} else {
        document.getElementById("imb_f").submit();
		document.getElementById("val_perumahan").innerHTML = "";
		document.getElementById("val_nama_pengembang").innerHTML = "";
		document.getElementById("val_alamat").innerHTML = "";
		document.getElementById("val_bentuk_rumah").innerHTML = "";
		document.getElementById("val_luas").innerHTML = "";
		document.getElementById("val_tipe").innerHTML = "";
		document.getElementById("val_jumlah_mbr").innerHTML = "";
		document.getElementById("val_jumlah_non_mbr").innerHTML = "";
    }
}

function val_imb2() {
    var idkabupaten_kota,jumlah_mbr2,jumlah_non_mbr2;
    idkabupaten_kota = document.getElementById("idkabupaten_kota").value;
    jumlah_mbr2 = document.getElementById("jumlah_mbr2").value;
    jumlah_non_mbr2 = document.getElementById("jumlah_non_mbr2").value;
    //if (isNaN(x) || x < 1 || x > 10) {
    if (isNaN(idkabupaten_kota) || idkabupaten_kota=="0") {
        document.getElementById("val_idkabupaten_kota").innerHTML = "Pilih Kabupaten";
    } else if (jumlah_mbr2=="" || isNaN(jumlah_mbr2) || jumlah_mbr2 < 1 ) {
        document.getElementById("val_jumlah_mbr2").innerHTML = "Masukan Jumlah MBR";
    } else if (jumlah_non_mbr2=="" || isNaN(jumlah_non_mbr2) || jumlah_non_mbr2 < 1 ) {
        document.getElementById("val_jumlah_non_mbr2").innerHTML = "Masukan Jumlah Non MBR";
	} else {
        document.getElementById("imb_unit_f").submit();
		document.getElementById("val_idkabupaten_kota").innerHTML = "";
		document.getElementById("val_jumlah_mbr2").innerHTML = "";
		document.getElementById("val_jumlah_non_mbr2").innerHTML = "";
    }
}
</script>
</body>
<!-- END BODY -->
</html>
