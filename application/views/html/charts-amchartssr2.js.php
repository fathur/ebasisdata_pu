var ChartsAmcharts = function() {


    var initChartSample5 = function() {
        var chart = AmCharts.makeChart("chart_5", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',

            "color":    '#0',

            "dataProvider": [
            <?php $i=0; 
                if ($iddinas>=40){
                $query = $this->db->query('SELECT total,tahun,(select max(total) from satu_juta_rumah_imb_nonmbr where iddinas='.$iddinas.') as mak FROM satu_juta_rumah_imb_nonmbr where iddinas='.$iddinas); 
}{$query = $this->db->query('SELECT year(p.tanggal) as tahun,sum(pd.non_mbr)+(SELECT sum(pm.realisasi) FROM `pengembang` p join pengembang_mbr pm on pm.idpengembang=p.idpengembang where  pm.jenis<>"MBR" and iddinas='.$iddinas.' group by year(p.tanggal)) as total,sum(pd.non_mbr)+(SELECT sum(pm.realisasi) FROM `pengembang` p join pengembang_mbr pm on pm.idpengembang=p.idpengembang where  pm.jenis<>"MBR" and iddinas='.$iddinas.' group by p.iddinas) as mak FROM `perbankan` p join perbankan_detail pd on pd.idperbankan=p.idperbankan where iddinas='.$iddinas.' group by p.iddinas'); 
                } 
		foreach ($query->result_array() as $row):  $i++?>
                {"country": "<?php echo $row['tahun'];?>","visits": <?php echo $row['total'];?>,"color": "<?php echo "hsl(".(50-$row['total']/($row['mak']/50)).",100%,70%)";?>"}<?php if ($i<=$query->num_rows()){echo ",";};?>
             <?php endforeach; ?>  ],
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "title":"Unit"
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]] unit</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 0,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "labelRotation":90,
                "minHorizontalGap":1

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);

        jQuery('.chart_5_chart_input').off().on('input change', function() {
            var property = jQuery(this).data('property');
            var target = chart;
            chart.startDuration = 0;

            if (property == 'topRadius') {
                target = chart.graphs[0];
            }

            target[property] = this.value;
            chart.validateNow();
        });

        $('#chart_5').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }

    return {
        //main function to initiate the module

        init: function() {
            initChartSample5();
        }

    };

}();
