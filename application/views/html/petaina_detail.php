 
<div class="portlet-title">
  <div class="caption" style="line-height: 21px">
		<i class="icon-map font-grey-gallery"></i>
		<span class="caption-subject bold font-grey-gallery uppercase">
      <?php if ($g2=="1"){echo 'PETA SEBARAN BACKLOG';}?>
      <?php if ($g3=="1"){echo 'RTLH';}?>
      <?php if ($g4=="1"){echo 'PETA SEBARAN Suplai Perumahan MBR';}?>
      <?php if ($g5=="1"){echo 'PETA SEBARAN Suplai Perumahan Non MBR';}?>
    </span>
	</div>
	<div class="actions">
		<?php if ($g3!="1"){?>
		<div class="btn-group btn-group-devided" data-toggle="buttons"> 
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php if ($kategori_1=='1') { echo 'active';}?>" onclick="kategori1()">
			<input type="radio" name="options" class="toggle" id="kepemilikan"/><?php if ($g2=="1"){ echo "Kepemilikan";}else{ echo "Non MBR";}?></label>
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php if ($kategori_1!='1') { echo 'active';}?>" onclick="kategori2()">
			<input type="radio" name="options" class="toggle" id="penghunian"/><?php if ($g2=="1"){ echo "Penghunian";}else{ echo "MBR";}?></label>
		</div>
		<?php }?>
	</div>
</div>
<div class="portlet-body">
  <div id="mapindonesia">
    <style type="text/css">
    #mapindonesia { height: 500px; width: 100%; }
    </style>
  </div>
  <div style="padding-top: 5px">
    <table>
        <tbody> 
          <tr>
            <td colspan="6">LEGENDA</td>
          </tr>
          <tr>
            <?php $query = $this->db->query("SELECT * from peta_kepemilikan_penghunian WHERE tahun=".$tahun." and idprovinsi=".$idprovinsi." limit 1");
            foreach ($query->result_array() as $row):  if ($kategori_1==1){$st=$row['total'];}else{$st=$row['totalp'];}?>
            <td width="20"><div class="btn" style="background:#CCCCCC"></div></td><td>&nbsp;</td><td align="left"> &nbsp; No Data </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#FFFF00"></div></td><td>&nbsp;</td><td align="left"> &nbsp; 0 - <?=$st/5;?>  </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#FEBA1B"></div></td><td>&nbsp;</td><td align="left"> &nbsp; <?=$st/5;?> - <?=($st/5)*2;?> </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#FF5828"></div></td><td>&nbsp;</td><td align="left"> &nbsp; <?=($st/5)*2;?> - <?=($st/5)*3;?> </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#FF0000"></div></td><td>&nbsp;</td><td align="left"> &nbsp; <?=($st/5)*3;?>- <?=($st/5)*4;?> </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#821414"></div></td><td>&nbsp;</td><td align="left"> &nbsp; > <?=($st/5)*4;?> </td><td>&emsp;&emsp;</td>
            <?php endforeach; ?>
          </tr>
      </tbody>
    </table>
  </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo BASE_URL('application/views/html/leaflet/leaflet.js');?>" type="text/javascript"></script>
<script>
/* BEGIN SCRIPT PETA INDONESIA */
var style = {
    "fillColor": "#CCCCCC",
    "fillOpacity": 1,
    "stroke": true,
    "color": "#666",
    "weight": 0.5,
 };

 var map = L.map('mapindonesia').setView([<?php echo $x;?>,<?php echo $y;?>], <?php echo $z;?>)

 L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="http://osm.org/copyright"></a> contributors'
 }).addTo(map);

 $.ajax({
 dataType: "json",
 //memanggil data.php yang tadi dibuat
 url: "<?php if ($g1==1){ echo BASE_URL("main/petaina");} elseif ($g2==1){ echo BASE_URL("main/petaina22");} elseif ($g3==1){ echo BASE_URL("main/petaina3");} elseif ($g4==1){ echo BASE_URL("main/petaina4");}elseif ($g5==1){ echo BASE_URL("main/petaina4");}  ?>",
 success: function(datasql) {
  indonesialayer(datasql);
  }
 });

 function indonesialayer(datasql)
 {
  $.ajax({
  dataType: "json",
  //memanggil provinsi.json yang tadi di download
  <?php
  if ($g2==1)
  { 
      if ($kategori_1!='1') 
      { 
          $pa=BASE_URL('/main/provinsi3/'.$idprovinsi);
      } 
      else 
      { 
          $pa=BASE_URL('/main/provinsi2/'.$idprovinsi);
          
      }
      
  }
  elseif ($g3==1)
  { 
      $pa= BASE_URL('/main/provinsi4/'.$idprovinsi);
  }
  elseif ($g4==1)
  { 
      if ($kategori_1=='1') 
      { 
          $pa=BASE_URL('/main/provinsi5/'.$idprovinsi);
      } 
      else 
      { 
          $pa=BASE_URL('/main/provinsi6/'.$idprovinsi);
          
      }
  }
  elseif ($g5==1)
  { 
      if ($kategori_1=='1') 
      { 
          $pa=BASE_URL('/main/provinsi7/'.$idprovinsi);
      } 
      else 
      { 
          $pa=BASE_URL('/main/provinsi8/'.$idprovinsi);
          
      }
  }
  ?>
  url: "<?php echo $pa;?>", 
  success: function(data) {
      //provinsi.json disimpan dalam variable data
      //tampilkan provinsi.json ke dalam mapindonesia
      //bindPopup digunakan untuk menampilkan info nama provinsi ketika layer provinsi di klik
      L.geoJSON(data,{
        onEachFeature:onEachFeature,
        style:style
      }).bindPopup(function (layer) {
          return layer.feature.properties.PROVINSI;
      }).addTo(map);

      function onEachFeature(feature, layer) {

          if(datasql.provinsi[0].ID==feature.properties.ID2013)
          {
              layer.setStyle({fillColor:"#B1D1AF",fillOpacity: 0.5});
          }
          if(datasql.provinsi[0].ID==feature.properties.ID2014)
          {
              layer.setStyle({fillColor:"#821414"});
          }
          if(datasql.provinsi[0].ID==feature.properties.ID2015)
          {
              layer.setStyle({fillColor:"#FF0000"});
          }
          if(datasql.provinsi[0].ID==feature.properties.ID2016)
          {
              layer.setStyle({fillColor:"#FF5828"});
          }
          if(datasql.provinsi[0].ID==feature.properties.ID2017)
          {
              layer.setStyle({fillColor:"#FEBA1B"});
          }
          if(datasql.provinsi[0].ID==feature.properties.ID2018)
          {
              layer.setStyle({fillColor:"#FFFF00"});
          }
      }
    }
  });
 }
 /* END SCRIPT PETA INDONESIA */ 
 </script>
