<div class="hor-menu">
  <ul class="nav navbar-nav">
    <li class="active">
      <a href="index.html">Dashboard</a>
    </li>
    <li class="menu-dropdown classic-menu-dropdown ">
      <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
        Lihat Data<i class="fa fa-angle-down"></i> </a>
      <ul class="dropdown-menu pull-left">
        <li class=" dropdown-submenu">
          <a href=":;"> <i class="icon-docs"></i> Kebutuhan Perumahan </a>
          <ul class="dropdown-menu">
            <li class=" ">
              <a href="data_backlog.html"> Backlog Perumahan </a>
            </li>
            <li class=" ">
              <a href="data_rtlh.html"> Rumah Tak Layak Huni </a>
            </li>
          </ul>
        </li>
        <li class=" dropdown-submenu">
          <a href=":;"> <i class="icon-docs"></i> Penyediaan Perumahan </a>
          <ul class="dropdown-menu">
            <li class=" ">
              <a href="data_imb.html"> Data IMB </a>
            </li>
            <li class=" ">
              <a href="data_non_imb.html"> Data Non-IMB </a>
            </li>
            <li class=" ">
              <a href="data_program_apbn.html"> Program Suplai Perumahan <span class="badge badge-roundless badge-danger">baru</span> </a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="menu-dropdown classic-menu-dropdown ">
      <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
        Input Data<i class="fa fa-angle-down"></i> </a>
      <ul class="dropdown-menu pull-left">
        <li class=" dropdown-submenu">
          <a href=":;"> <i class="icon-note"></i> Formulir I </a>
          <ul class="dropdown-menu">
            <li class=" ">
              <a href="form_1a.html"> Formulir IA (Provinsi)</a>
            </li>
            <li class=" ">
              <a href="form_1b.html"> Formulir IB (Kabupaten/Kota)</a>
            </li>
          </ul>
        </li>
        <li class=" dropdown-submenu">
          <a href=":;"> <i class="icon-note"></i> Formulir Suplai Perumahan </a>
          <ul class="dropdown-menu">
            <li class=" ">
              <a href="form_imb.html"> Formulir IMB </a>
            </li>
            <li class=" ">
              <a href="form_non_imb.html"> Formulir Non-IMB </a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="menu-dropdown classic-menu-dropdown ">
      <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
        Laporan<i class="fa fa-angle-down"></i> </a>
      <ul class="dropdown-menu pull-left">
        <li class="mega-menu-content">
          <a href="lap_rekapitulasi.html"> <i class="icon-grid"></i> Rekapitulasi </a>
        </li>
        <li class="mega-menu-content">
          <a href="lap_infografik.html"> <i class="icon-bar-chart"></i> Infografik </a>
        </li>
      </ul>
    </li>
  </ul>
</div>
