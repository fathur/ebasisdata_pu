
<div class="portlet-title">
  <div class="caption" style="line-height: 21px">
		<i class="icon-map font-grey-gallery"></i>
		<span class="caption-subject bold font-grey-gallery uppercase">	<?php if ($g2=="1"){echo 'PETA SEBARAN BACKLOG';}?><?php if ($g3=="1"){echo 'RTLH';}?><?php if ($g4=="1"){echo 'PETA SEBARAN Suplai Perumahan MBR';}?><?php if ($g5=="1"){echo 'PETA SEBARAN Suplai Perumahan Non MBR';}?></span>
	</div>
	<div class="actions">
		<?php if ($g3!="1"){?>
		<div class="btn-group btn-group-devided" data-toggle="buttons">
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php if ($kategori_1=='1') { echo 'active';}?>" onclick="kategori1()">
			<input type="radio" name="options" class="toggle" id="kepemilikan"/><?php if ($g2=="1") { echo "Kepemilikan";}else{ echo "Non MBR";}?></label>
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php if ($kategori_1!='1') { echo 'active';}?>" onclick="kategori2()">
			<input type="radio" name="options" class="toggle" id="penghunian"/><?php if ($g2=="1") { echo "Penghunian";}else{ echo "MBR";}?></label>
		</div>
		<?php }?>
	</div>
</div>
<div class="portlet-body">
  <div id="mapindonesia">
    <style type="text/css">
    #mapindonesia { height: 450px; width: 100%; }
    </style>
  </div>
  <div style="padding-top: 5px">
    <table>
        <tbody>
          <tr>
            <td colspan="6">LEGENDA</td>
          </tr>
          <tr>
            <?php  
            $query = $this->db->query("SELECT * from backlog_kepemilikan_dan_penghunian_view where tahun=".$tahun." limit 1");
            foreach ($query->result_array() as $row):  
            if ($kategori_1==1 && $g2==1)
            {
                $st=$row['mak1'];
            }
            elseif ($kategori_2==1 && $g2==1)
            {
                $st=$row['mak2'];
            }
            elseif ($g3==1)
            {
                $st=$row['totalrtlh'];
            }
            elseif ($kategori_1==1 && $g4==1)
            {
                $st=$row['mak4'];
            }
            elseif ($kategori_2==1 && $g4==1)
            {
                $st=$row['mak5'];
            }
            elseif ($kategori_1==1 && $g5==1)
            {
                $st=$row['mak6'];
            }
            elseif ($kategori_2==1 && $g5==1)
            {
                $st=$row['mak7'];
            } 
            ?>
            <td width="20"><div class="btn" style="background:#CCCCCC"></div></td><td>&nbsp;</td><td align="left"> &nbsp; No Data </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#FFFF00"></div></td><td>&nbsp;</td><td align="left"> &nbsp; 0 - <?=$st/5;?>  </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#FEBA1B"></div></td><td>&nbsp;</td><td align="left"> &nbsp; <?=$st/5;?> - <?=($st/5)*2;?> </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#FF5828"></div></td><td>&nbsp;</td><td align="left"> &nbsp; <?=($st/5)*2;?> - <?=($st/5)*3;?> </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#FF0000"></div></td><td>&nbsp;</td><td align="left"> &nbsp; <?=($st/5)*3;?>- <?=($st/5)*4;?> </td><td>&emsp;&emsp;</td>
            <td width="20"><div class="btn" style="background:#821414"></div></td><td>&nbsp;</td><td align="left"> &nbsp; > <?=($st/5)*4;?> </td><td>&emsp;&emsp;</td>
            <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo BASE_URL('application/views/html/leaflet/leaflet.js');?>" type="text/javascript"></script>
<script>
/* BEGIN SCRIPT PETA INDONESIA */
var style = {
    "fillColor": "#CCCCCC",
    "fillOpacity": 0.5,
    "stroke": true,
    "color": "#666",
    "weight": 0.5,
 };

 var map = L.map('mapindonesia').setView([-2, 117.5], 5); // koordinat posisi peta dan zoom

 L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="http://osm.org/copyright"></a> contributors'
 }).addTo(map);

 $.ajax({
 dataType: "json",
 //memanggil data.php yang tadi dibuat
 <?php if ($kategori_1==1)
 {
     $petapro="petaina22/".$tahun;
     $petapro2="petaina4/".$tahun;
     $petapro3="petaina5/".$tahun;

 }
 else
 {
     $petapro="petaina22x/".$tahun;
     $petapro2="petaina4x/".$tahun;
     $petapro3="petaina5x/".$tahun;

 };?>
 url: "<?php if ($g1==1){ echo BASE_URL("main/petaina/".$tahun);} elseif ($g2==1){ echo BASE_URL("main/".$petapro);} elseif ($g3==1){ echo BASE_URL("main/petaina3_prov/".$tahun);} elseif ($g4==1){ echo BASE_URL("main/".$petapro2);}  elseif ($g5==1){ echo BASE_URL("main/".$petapro3);}  ?>",
 success: function(datasql) {
  indonesialayer(datasql);
  }
 });

 function indonesialayer(datasql)
 {
  $.ajax({
  dataType: "json",
  //memanggil provinsi.json yang tadi di download
  url: "<?php echo BASE_URL('/application/views/html/provinsi.json');?>",
  success: function(data) {
      //provinsi.json disimpan dalam variable data
      //tampilkan provinsi.json ke dalam mapindonesia
      //bindPopup digunakan untuk menampilkan info nama provinsi ketika layer provinsi di klik
      L.geoJSON(data,{
        onEachFeature:onEachFeature,
        style:style
      }).bindPopup(function (layer) {
          return layer.feature.properties.PROVINSI;
      }).addTo(map);

      function onEachFeature(feature, layer) {

        for(var i=0;i<datasql.provinsi.length;i++)
        {
          if(datasql.provinsi[i].ID==feature.properties.ID2013)
          {
            jumlah=parseInt(datasql.provinsi[i].JUMLAHPENDUDUK);
            if(jumlah==null)
            {
              layer.setStyle({fillColor:"#CCCCCC"}); 
            }
            if(jumlah>0&&jumlah<=<?=($st/5)*2;?>)
            {
              layer.setStyle({fillColor:"#FFFF00"}); 
            }
            if(jumlah><?=$st/5;?>&&jumlah<=<?=($st/5)*2;?>)
            {
              layer.setStyle({fillColor:"#FEBA1B"});
            }
            else if(jumlah><?=($st/5)*2;?>&&jumlah<=<?=($st/5)*3;?>)
            {
              layer.setStyle({fillColor:"#FF5828"});
            }
            else if(jumlah><?=($st/5)*3;?>&&jumlah<=<?=($st/5)*4;?>)
            {
              layer.setStyle({fillColor:"#FF0000"});
            }
            else if(jumlah><?=($st/5)*4;?>)
            {
              layer.setStyle({fillColor:"#821414"});
            } 
          }
        }
      }
    }
  });
 }
 /* END SCRIPT PETA INDONESIA */
 </script> 
