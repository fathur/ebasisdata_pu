b<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 foreach($form_perbankan_view as $r):
     $idform_1=$r->idperbankan;  
     $perbankan=$r->nama; 
     $alamat=$r->alamat; 
     $no_kontrak=$r->no_kontrak; 
     $bulan=$r->bulan;
 endforeach;
?><!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function tambah_imb_modal()
{
	  document.getElementById('pengembang_perorangan').value="";
	  document.getElementById('nama_perumahan').value="";
	  document.getElementById('lokasi').value=""; 
	  document.getElementById('jenis_rumah').value="";
	  document.getElementById('bentuk_rumah').value="";
	  document.getElementById('rencana').value="";
	  document.getElementById('realisasi').value="";
	  document.getElementById('harga').value="";
	  document.getElementById('mbr').value="";
	  document.getElementById('non_mbr').value="";
	  document.getElementById('kpr').value=""; 
    document.getElementById('imb_f').action="<?php echo base_url('main/fperbankan_mbr_insert');?>";
}
function val_imb1() { 
    var lokasi,pengembang_perorangan,kpr,nama_perumahan,bentuk_rumah,jenis_rumah,rencana,realisasi,harga,jenis,non_mbr,mbr;
    lokasi = document.getElementById("lokasi").value;
    pengembang_perorangan = document.getElementById("pengembang_perorangan").value;
    kpr = document.getElementById("kpr").value;
    nama_perumahan = document.getElementById("nama_perumahan").value;
    bentuk_rumah = document.getElementById("bentuk_rumah").value;
    jenis_rumah = document.getElementById("jenis_rumah").value; 
    rencana = document.getElementById("rencana").value;
    realisasi = document.getElementById("realisasi").value;
    harga = document.getElementById("harga").value; 
   // window.alert("sometext"); 
    kpr = document.getElementById("kpr").value;  
    mbr = document.getElementById("mbr").value;  
    non_mbr = document.getElementById("non_mbr").value; 
    if ((pengembang_perorangan=="")) {
        document.getElementById("val_pengembang_perorangan").innerHTML = "Masukan Data Pengembang Perorangan"; 
    } else if (nama_perumahan=="") {
        document.getElementById("val_nama_perumahan").innerHTML = "Masukan Nama Perumahan"; 
    } else if (lokasi=="") {
        document.getElementById("val_lokasi").innerHTML = "Masukan Lokasi"; 
    } else if (jenis_rumah=="") {
        document.getElementById("val_jenis_rumah").innerHTML = "Masukan jenis_rumah"; 
    } else if (bentuk_rumah=="") {
        document.getElementById("val_bentuk_rumah").innerHTML = "Masukan Bentuk Rumah"; 
    } else if (rencana=="") {
        document.getElementById("val_rencana").innerHTML = "Input Salah"; // || realisasi < 0
    } else if (realisasi=="") {
        document.getElementById("val_realisasi").innerHTML = "Input Salah";    
    } else if (harga=="") {
        document.getElementById("val_harga").innerHTML = "Input Salah";     
    } else if (mbr=="") {
        document.getElementById("val_mbr").innerHTML = "Input Salah";     
    } else if (non_mbr=="") {
        document.getElementById("val_non_mbr").innerHTML = "Input Salah";  
    } else if (kpr=="") {
        document.getElementById("val_kpr").innerHTML = "Masukan Fasilitas KPR"; 
	} else {
        document.getElementById("imb_f").submit();
		document.getElementById("val_pengembang_perorangan").innerHTML = "";
		document.getElementById("val_nama_perumahan").innerHTML = "";
		document.getElementById("val_lokasi").innerHTML = ""; 
		document.getElementById("val_jenis_rumah").innerHTML = "";
		document.getElementById("val_bentuk_rumah").innerHTML = "";
		document.getElementById("val_rencana").innerHTML = "";
		document.getElementById("val_realisasi").innerHTML = "";
		document.getElementById("val_harga").innerHTML = "";
		document.getElementById("val_mbr").innerHTML = "";
		document.getElementById("val_non_mbr").innerHTML = "";
    }
}

</script>
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head>

<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->

<?php include "header2.php";?>
<?php foreach($dinas_view as $r): ?>

<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Formulir Unit KPR Perbankan <small class="page-title-tag">Suplai Perumahan</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->

	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?php echo base_url('main/index'); ?>">Home</a><i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Formulir Suplai Perumahan<i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Formulir Unit KPR Perbankan
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->

			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN DETAIL modal -->
					<!-- /.modal -->
					 
					<div id="fimb_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form action="" class="form-horizontal" method="post" id="imb_f"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title"><strong>Tabel Data Perumahan untuk MBR/Rumah Umum/Subsidi<strong></h5>
										</div>
										<div class="modal-body">
											<p>
										<table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td><strong>Kabupaten / Kota</strong></td>
											<td align="center">
                                                <input name="idperbankan" id="idperbankan" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                                                <input name="idperbankan_mbr" id="idperbankan_mbr" type="hidden" class="form-control" />
                                                <select name="idkabupaten_kota" id="idkabupaten_kota" class="form-control" style="background-color:#E8F3FF"> 
                        <option value="0">Pilih Kabupaten / Kota</option>
                        <?php foreach($kabupaten_kota as $kk): ?>
                        <option value="<?php echo $kk->idkabupaten_kota;?>"><?php echo  $kk->kabupaten_kota;?></option>
  											<?php endforeach;?>
                        </select>
                                             </td>
										</tr>
                                        <tr>
											<td><strong>Pengembang/Perorangan</strong></td>
											<td align="center"><input name="pengembang_perorangan" id="pengembang_perorangan" type="text" class="form-control"  style="background-color:#E8F3FF" /><p id="val_pengembang_perorangan"></p></td>
										</tr>
                                        <tr>
											<td><strong>Nama Perumahan</strong></td>
											<td align="center" ><input name="nama_perumahan" id="nama_perumahan" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_nama_perumahan"></p></td>
										</tr>
                                        <tr>
											<td><strong>Alamat/Lokasi</strong></td>
											<td align="center" ><input name="lokasi" id="lokasi" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_lokasi"></p></td>
										</tr> 
                                        <tr>
											<td><strong>Jenis Rumah</strong></td>
											<td align="left">  
                                            <select name="jenis_rumah" id="jenis_rumah" class="form-control" style="background-color:#E8F3FF">
                                                <option value="Umum"  style="background-color:#E8F3FF" class="form-control">Umum</option>
                                                <option value="Komersial"  style="background-color:#E8F3FF" class="form-control">Komersial</option>
                                            </select>
                                            <p id="val_jenis_rumah"></p></td>
										</tr>
                                        <tr>
											<td><strong>Bentuk Rumah</strong></td>
											<td align="left">  
                                            <select name="bentuk_rumah" id="bentuk_rumah" class="form-control" style="background-color:#E8F3FF">
                                                <option value="Tapak"  style="background-color:#E8F3FF" class="form-control">Tapak</option>
                                                <option value="Susun"  style="background-color:#E8F3FF" class="form-control">Susun</option>
                                            </select>
                                            <p id="val_bentuk_rumah"></p></td>
										</tr>
                                        <tr>
											<td><strong>Jumlah Unit(Rencana)</strong></td>
											<td align="center"><input name="rencana" id="rencana" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_rencana"></p></td>
										</tr>
                                        <tr>
											<td><strong>Jumlah Unit(Realisasi)</strong></td>
											<td align="center"><input name="realisasi" id="realisasi" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_realisasi"></p></td>
										</tr>
                                        <tr>
											<td><strong>Harga</strong></td>
											<td align="center"><input name="harga" id="harga" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_harga"></p></td>
										</tr>
                                        <tr>
											<td><strong>MBR</strong></td>
											<td align="center"><input name="mbr" id="mbr" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_mbr"></p></td>
										</tr>
                                        <tr>
											<td><strong>Non MBR</strong></td>
											<td align="center"><input name="non_mbr" id="non_mbr" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_non_mbr"></p></td>
										</tr>
                                        <tr>
											<td><strong>Fasilitas KPR Yang Dipakai</strong></td>
											<td align="center"><input name="kpr" id="kpr" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_kpr"></p></td>
										</tr>
							  		</tbody>
									</table>
											</p>
										</div>
										<div class="modal-footer">
                      <button type="button" class="btn green" onclick="val_imb1()">Simpan</button>
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>
									</div>
								</div>
								</form>
					</div>
					<!-- END DETAIL modal -->
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-list font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase">
									Laporan Bulanan Data Pengajuan KPR Kepemilikan Unit Rumah, Data Suplai Perumahan Tahun 2017
								</span>
							</div>
							<div class="tools">
								<a href="" class="collapse" data-original-title="" title=""></a>
								<a href="" class="reload" data-original-title="" title=""></a>
								<a href="" class="fullscreen" data-original-title="" title=""></a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="btn-group">
										</div>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="btn-group pull-right">
											<button type="button" class="btn default" data-toggle="dropdown" aria-expanded="false"> Peralatan <i class="fa fa-angle-down"></i></button>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;" class="printthis"><i class="fa fa-print font-grey-mint"></i> Cetak</a>
													</li>
													<li>
														<a href="javascript:;" class="savetopdf"><i class="fa fa-file-pdf-o font-grey-mint"></i> Ekspor ke PDF</a>
													</li>
													<li>
														<a href="javascript:;" class="savetoxls"><i class="fa fa-file-excel-o font-grey-mint"></i> Ekspor ke Excel</a>
													</li>
												</ul>
										</div>
									</div>
								</div>
							</div>
              <!-- TABEL KEPALA -->
              <div class="col-md-6 col-sm-12" style="padding-left:0">
                  <form action="<?php echo base_url('/main/perbankan_update_bulan');?>" method="post">
                <table class="table table-bordered">
                  <tr>
                    <td width="30%">Provinsi</td>
                    <td width="70%"><?php echo $provinsi;?></td>
                  </tr>
                  <tr>
                    <td>Bulan Data</td>
                    <td> 
                    <input name="idperbankan" type="hidden" value="<?php echo $idform_1;?>"/>
                        <input name="tanggal" type="hidden" value="<?php echo date('Y-m-d');?>"/>
      									  <select name="bulan"  id="bulan" class="form-control"  onchange='this.form.submit()'>
                          <?php if (isset($bulan)){ echo "<option value=".$bulan.">".$bulan."</option>"; } else { ?>
                          <option value="0">Pilih Bulan</option>
                          <?php  }
                           for($i=1;$i<=12;$i++){
                           $monthNum = $i;
                           $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
                           echo '<option value="'.$monthName.'">'.$monthName.'</option>';  }
                          ?>
                          </select>
      					 </td>
      					</td>
                  </tr>
                  <tr>
                    <td>Nama Bank</td>
                    <td><input name="idform_satu_juta_rumah" type="hidden" value="<?php echo $idform_1;?>"/>
                        <input name="tanggal" type="hidden" value="<?php echo date('Y-m-d');?>"/>
      					
                        <input name="nama_perbankan" type="text" value="<?php echo $perbankan;?>"/></td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td><input name="alamat" type="text" value="<?php echo $alamat;?>"/></td>
                  </tr>
                  <tr>
                    <td>No. Kontak</td>
                    <td><input name="no_kontrak" type="text" value="<?php echo $no_kontrak;?>"/></td>
                  </tr>
                  
                  <tr>
                    <td></td>
                    <td>
                        <input type="submit" value="Simpan"/></td>
                  </tr>
                </table>
                  </form>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="note note-success">
                  Untuk Unit KPR Perbankan
                </div>
              </div>
              <!-- END OF TABEL KEPALA -->
							<!-- TABEL UTAMA PERBANKAN DIMULAI -->
              <div class="col-md-12" style="padding-left:0">
								<h3>Tabel Data Perumahan untuk Pengajuan KPR Kepemilikan Unit Rumah</h3>
              <p> <a class=" btn default" data-toggle="modal" href="#fimb_modal" onClick="tambah_imb_modal()">Tambah</a>   </p> 
							</div>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="text-align:center; vertical-align:middle">No</th>
                    <th style="text-align:center; vertical-align:middle">Nama Pemohon</th>
                    <th style="text-align:center; vertical-align:middle">Nama Perumahan</th>
                    <th style="text-align:center; vertical-align:middle">Alamat Lokasi</th>
                    <th style="text-align:center; vertical-align:middle">Jenis Rumah<br />(Umum/Komersial)</th>
										<th style="text-align:center; vertical-align:middle">Detail</th>
										<th style="text-align:center; vertical-align:middle">Ubah</th>
										<th style="text-align:center; vertical-align:middle">Hapus</th>
                    <!--
										<th style="text-align:center">Kategori Pemohon (Tapak/Susun)</th>
										<th style="text-align:center">Bentuk Rumah (Tapak/Susun)</th>
                    <th style="text-align:center">Jumlah Unit Rencana</th>
                    <th style="text-align:center">Jumlah Unit Realisasi</th>
                    <th style="text-align:center">Harga</th>
                    <th style="text-align:center">MBR</th>
                    <th style="text-align:center">Non-MBR</th>
                    <th style="text-align:center">Fasilitas KPR</th>-->
                  </tr>
                </thead>
                <tbody>
                    <?php $i=0; foreach($perbankan_view_detail as $f): ?>
                  <tr>
                    <td><?php $i++; echo $i;?>.</td>
                    <td><?php echo $f->kabupaten_kota;?></td>
                    <td><?php echo $f->nama_perumahan;?></td>
                    <td><?php echo $f->lokasi;?></td>
                    <td><?php echo $f->jenis_rumah;?></td>
                    <td style="text-align:center"><a href="#detail_perbankan" data-toggle="modal">Detail</a></td>
                    <td style="text-align:center"><a href="#edit_perbankan" data-toggle="modal">Ubah</a></td>
                    <td style="text-align:center"><a href="#hapus">Hapus</a></td>
                    <!--
										<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>-->
                  </tr>
                  <?php endforeach;?> 
                </tbody>
              </table>
							<!-- TABEL UTAMA PERBANKAN SELESAI -->
              <div class="note note-warning">
								Dalam rangka pendataan pembangunan unit rumah baru bagian dari Program Suplai Rumah,
								diperlukan data dari berbagai sumber antara lain dari Perbankan, dengan cara menghitung
								jumlah pengajuan kepemilikan unit rumah baru dengan KPR yang diasumsikan sebagai unit fisik
								rumah terbangun.
								<br><br>
                Tabel ini diisi oleh Perbankan dan dikembalikan ke SNVT bidang Perumahan Provinsi melalui
								TAPP Provinsi, untuk pertanyaan lebih lanjut dapat menghubungi Subdit Data dan Informasi,
								Direktorat Perencanaan Penyediaan Perumahan: datinperumahan@gmail.com (021-7211883)
              </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
					<!-- BEGIN EDIT modal -->
					<!-- /.modal -->
					<div id="edit_perbankan" class="modal fade bs-modal-lg" tabindex="-1" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Ubah Data <!-- NAMA PEMOHON --></h4>
								</div>
								<div class="modal-body">
									<div class="scroller" style="height:390px" data-always-visible="1" data-rail-visible1="1">
										<div class="row">
											<div class="col-md-12">
												<form action="javascript:;" class="form-horizontal">
												<table class="table table-light">
													<tbody>
														<tr>
															<td class="fit">A.</td>
															<td width="15%">Nama Perumahan</td>
															<td class="fit">:</td>
															<td width="83%"><input type="text" class="form-control" placeholder="Nama Perumahan"></td>
														</tr>
														<tr>
															<td>B.</td>
															<td>Lokasi</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Alamat</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Alamat"></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Kabupaten/Kota</td>
															<td>:</td>
															<td>
																<select class="form-control" style="width:auto">
																	<option>Pilih Kabupaten</option>
																	<option>Kabupaten A</option>
																	<option>Kabupaten B</option>
																</select>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Kecamatan</td>
															<td>:</td>
															<td>
																<select class="form-control" style="width:auto">
																	<option>Pilih Kecamatan</option>
																	<option>Kecamatan A</option>
																	<option>Kecamatan B</option>
																</select>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Kelurahan</td>
															<td>:</td>
															<td>
																<select class="form-control" style="width:auto">
																	<option>Pilih Kelurahan</option>
																	<option>Kelurahan A</option>
																	<option>Kelurahan B</option>
																</select>
															</td>
														</tr>
														<tr>
															<td>C.</td>
															<td>Jenis Rumah</td>
															<td>:</td>
															<td>
																<select class="form-control" style="width:auto">
																	<option>Komersial</option>
																	<option>Umum</option>
																</select>
															</td>
														</tr>
														<tr>
															<td>D.</td>
															<td>Bentuk Rumah</td>
															<td>:</td>
															<td>
																<select class="form-control" style="width:auto">
																	<option>Tapak</option>
																	<option>Susun</option>
																</select>
															</td>
														</tr>
														<tr>
															<td>E.</td>
															<td>Jumlah Unit</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">- Rencana</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Unit Rencana" style="width:auto"></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">- Realisasi</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Unit Realisasi" style="width:auto"></td>
														</tr>
														<tr>
															<td>F.</td>
															<td>Peruntukan</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">- MBR</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Unit MBR" style="width:auto"></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">- Non-MBR</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Unit Non-MBR" style="width:auto"></td>
														</tr>
														<tr>
															<td>G.</td>
															<td>Harga</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Rp 999.999,-" style="width:auto"></td>
														</tr>
														<tr>
															<td>H.</td>
															<td>Fasilitas KPR</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Jenis Fasilitas KPR" style="width:auto"></td>
														</tr>
													</tbody>
												</table>
											</form>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer form-actions" style="text-align:center">
									<button type="button" data-dismiss="modal" class="btn default">Batal</button>
									<button type="submit" class="btn green">Simpan</button>
								</div>
							</div>
						</div>
					</div>
					<!-- END EDIT modal -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<?php include "footer2.php";?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-contextmenu/bootstrap-contextmenu.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
	 Layout.init(); // init current layout
	 Demo.init(); // init demo features
   TableManaged.init();
});
</script>
<?php endforeach; ?>
</body>
<!-- END BODY -->
</html>
