<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
<script type="text/javascript">
function confirmDelete(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_fsejuta_rumah');?>/"+delUrl;
  }
}


function reset_pengguna(a)
{ 
		document.getElementById("iddinas_reset").value=a; 
}
function ubah_password_daftar_pengguna(a)
{ 
		document.getElementById("iddinasx").value=a; 
}

function ubah_pengguna(a,b,c,d,e)
{ 
		document.getElementById("val_alamat_user").innerHTML = "";
		document.getElementById("val_telepon_user").innerHTML = "";
		document.getElementById("val_email_pengguna").innerHTML = "";
	  document.getElementById('iddinas_pengguna').value=a; 
	  document.getElementById('nama_dinas').value=b;
	  document.getElementById('alamat_user').value=c;
	  document.getElementById('telepon_user').value=d; 
        document.getElementById('email_pengguna').value=e;
        document.getElementById('ubah_pengguna_form').action="<?php  echo base_url('main/update_daftar_user');?>";
}

function level1()
{
        document.getElementById('provinsi_select').value="1";
        document.getElementById('form_user_provinsi').submit();
}
function level2()
{
        document.getElementById('provinsi_select').value="0";
        document.getElementById('form_user_provinsi').submit();
}

function val_pengguna() {
    var alamat_user,telepon_user,email_pengguna; 
    alamat_user = document.getElementById("alamat_user").value;
    telepon_user = document.getElementById("telepon_user").value;
    email_pengguna = document.getElementById("email_pengguna").value;
    //if (isNaN(x) || x < 1 || x > 10) {
    if ((alamat_user=="")) {
        document.getElementById("val_alamat_user").innerHTML = "Harap mengisi alamat pengguna";
        document.getElementById("val_telepon_user").innerHTML = "";
        document.getElementById("val_email_pengguna").innerHTML = ""; 
    } else if (isNaN(telepon_user) || telepon_user == "") {
        document.getElementById("val_telepon_user").innerHTML = "Masukan nomor telepon dengan benar";
        document.getElementById("val_email_pengguna").innerHTML = ""; 
        document.getElementById("val_alamat_user").innerHTML = "";
    } else if (email_pengguna=="") {
        document.getElementById("val_email_pengguna").innerHTML = "Masukan email pengguna"; 
        document.getElementById("val_alamat_user").innerHTML = "";
        document.getElementById("val_telepon_user").innerHTML = "";
	} else {
        document.getElementById("ubah_pengguna_form").submit(); 
		document.getElementById("val_alamat_user").innerHTML = "";
		document.getElementById("val_telepon_user").innerHTML = "";
		document.getElementById("val_email_pengguna").innerHTML = "";
    }
}

function val_password_baru() {
    var pass; 
    pass = document.getElementById("password_baru_user").value; 
    //if (isNaN(x) || x < 1 || x > 10) {
    if ((pass=="")) {
        document.getElementById("val_pass_user").innerHTML = "Harap isi password baru"; 
    } else if ((pass.length<5)) {
        document.getElementById("val_pass_user").innerHTML = "Password minimal 5 karakter";  
	}  else {
        document.getElementById("form_password_daftar_pengguna").submit(); 
		document.getElementById("val_pass_user").innerHTML = "";
    }
}
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php include "header2.php";?>
<!-- END HEADER -->
<form id="form_user_provinsi" method="post" action="<?=BASE_URL('main/daftar_pengguna');?>">
      <input type="hidden" id="provinsi_select" name="provinsi_select" value="<?php echo $provinsi_select;?>">  
    </form>
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Pengguna <small class="page-title-tag">Form Daftar Pengguna</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?= base_url(); ?>">Home</a><i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Daftar Pengguna
				</li>
			</ul> 
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
                    
					<div class="portlet light">
						<div class="portlet-body"> 
                        <div class="actions">
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php if ($provinsi_select=='1') { echo 'active';}?>" onclick="level1()">
                                <input type="radio" name="options" class="toggle" id="level"/>Provinsi</label>
                                <label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php if ($provinsi_select!='1') { echo 'active';}?>" onclick="level2()">
                                <input type="radio" name="options" class="toggle" id="level"/>Kabupaten/Kota</label>
                            </div>
                        </div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th class="table-checkbox">
									#
								</th>
								<th width="50">
									 Nama Dinas
								</th>
								<th>
									 Alamat
								</th>
								<th>
									 Telepon
								</th>
								<th width="30">
									 Email
								</th>
<!--								<th width="30">-->
<!--									 Status-->
<!--								</th>-->
								<th width="30" colspan="2">
									 fungsi
								</th> 
							</tr>
							</thead>
							<tbody>
                            <?php $i=0; foreach($daftar_pengguna as $r): $i++;?>
							<tr class="odd gradeX">
								<td>
									<?php echo $i;?>
								</td>
								<td>
									 <?php if ($provinsi_select=="1") {echo $r->nama_dinas." ".$r->provinsi;} else {echo $r->nama_dinas." ".$r->kabupaten_kota;} ?>
								</td>
								<td class="center">
									 <?php echo $r->alamat_user;?>
								</td>
								<td class="center">
									 <?php echo $r->telepon_user;?>
								</td>
								<td>
									 <?php echo $r->email_user;?>
								</td>
<!--                                <td>-->
<!--                                    --><?php //if ($r->isLoggedIn == true) {
//                                        echo "Aktif";
//                                    } else {
//                                        echo "Tidak Aktif";
//                                    } ?>
<!--                                </td>-->
								<td class="center">

									 <a class="btn btn-sm blue-hoki" data-toggle="modal" href="#ubah_password_daftar_pengguna" onClick="ubah_password_daftar_pengguna('<?php echo $r->iddinas;?>')">Password</a>
								</td> 
								<td class="center">

									
									 <a class="btn btn-sm blue-hoki" data-toggle="modal" href="#daftar_pengguna" onClick="ubah_pengguna('<?php echo $r->iddinas;?>','<?php echo $r->nama_dinas;?>','<?php echo $r->alamat_user;?>','<?php echo $r->telepon_user;?>','<?php echo $r->email_user;?>')">Ubah</a>
								</td> 
								<td class="center">

									 
									 <a class="btn btn-sm blue-hoki" data-toggle="modal" href="#reset_login" onClick="reset_pengguna('<?php echo $r->iddinas;?>')" >Reset Login</a> 
								</td> 
							</tr>
                            <?php endforeach; ?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "footer2.php"; ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
   TableManaged.init();
});
</script>
</body>
<!-- END BODY -->
</html>
