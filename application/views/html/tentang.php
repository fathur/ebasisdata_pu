<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?=$title; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../../assets/admin/pages/css/about-us.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php require "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Tentang Aplikasi e-Basisdata Perumahan <small class="page-title-tag">selayang pandang</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('main/index'); ?>">Home</a>
          <i class="fa fa-2x fa-angle-right"></i>
        </li>
        <li class="active">
          Tentang
        </li>
      </ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="portlet light margin-top-10">
				<div class="portlet-body">
					<div class="row margin-bottom-30">
						<div class="col-md-12">
              <h4 class="bold uppercase">Latar Belakang</h4>
              <p>Aplikasi e-Basisdata Perumahan adalah sebuah platform sistem informasi basisdata (<i>database</i>) berbasiskan
              jaringan internet yang dikembangkan oleh Subdit Data dan Informasi, Direktorat Perencanaan, Dirjen Penyediaan
              Perumahan untuk terciptanya sistem pengelolaan data perumahan di seluruh wilayah Indonesia yang sinkron dan terintegrasi.</p>
              <p>Mengingat luasnya cakupan wilayah serta banyaknya pihak yang berkepentingan dalam urusan perumahan, maka urgensi
              untuk membuat suatu sistem informasi manajemen basisdata perumahan yang ditujukan untuk memenuhi kebutuhan informasi
              menjadi sangat strategis. Sebab, dengan ketersediaan basisdata yang akurat, faktual dan handal, maka proses
              penyusunan program perencanaan perumahan akan lebih terarah, tepat guna dan tepat sasaran.</p>
              <h4 class="bold uppercase">Landasan Hukum</h4>
              <p>Dalam Undang-Undang Nomor 1 Tahun 2011 tentang Perumahan dan Kawasan Permukiman, bagian ketiga dijelaskan
              mengenai wewenang sebagai berikut:
              <blockquote style="font-size:15px">
              <ol class="list-unstyled margin-top-10 margin-bottom-10">
                 <li><i class="fa fa-check-circle-o"></i> Pasal 16, Pemerintah pusat menyusun dan menyediakan basis data PKP</li>
                 <li><i class="fa fa-check-circle-o"></i> Pasal 17, Pemerintah provinsi menyusun dan menyediakan basis data PKP pada tingkat provinsi</li>
                 <li><i class="fa fa-check-circle-o"></i> Pasal 18, Pemerintah Kabupaten/Kota menyusun dan menyediakan basis data PKP pada tingkat Kabupaten/Kota</li>
              </ol></blockquote>
              </p>
              <p>Dalam Peraturan Pemerintah Nomor 88 Tahun 2014 tentang Pembinaan Penyelenggaraan Perumahan dan Kawasan Permukiman,
              pasal 18 (1) dikatakan, bahwa:
              <blockquote style="font-size:15px">“Pengembangan sistem informasi dan komunikasi dilakukan melalui menyusun dan menyediakan
              basis data, pemuktahiran data, jaringan, perangkat keras, dan perangkat lunak.”</blockquote></p>
						</div>
					</div>
					<!--/row-->

					<!-- //End Meer Our Team -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<?php include "footer2.php";?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
});
</script>
<script>
    function val_profile_form() {
    var kada,wakada;
    kada = document.getElementById("kada").value;
    wakada = document.getElementById("wakada").value;
     //if (isNaN(x) || x < 1 || x > 10) {
    if ((kada=="")) {
        document.getElementById("val_kada").innerHTML = "Masukan Nama Kepala Daerah";
    } else if ((wakada=="")) {
        document.getElementById("val_wakada").innerHTML = "Masukan Nama Wakil Kepala Daerah";
    } else {
        document.getElementById("frofile_form").submit();
		    document.getElementById("val_kada").innerHTML = "";
		    document.getElementById("val_wakada").innerHTML = "";
        document.getElementById("val_letak_geografi").innerHTML = "";  
    }
}
    $("#provinsi_filter").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_filter').load(url);
        return false;
    }); 
function val_filter_kab() {
    var idkabupaten_kota_select;
    idkabupaten_kota_select = document.getElementById("kabupaten_filter").selectedIndex;  
    if ((idkabupaten_kota_select==0)) {
        document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
    } else {
        document.getElementById("form_filter_kab").submit(); 
    }
} 
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
