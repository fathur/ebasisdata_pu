<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
		$divi = $dinas_view; 
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<script type="text/javascript">
function val_password(){
    if (document.getElementById("current_password").innerHTML.length<=5){
        if (document.getElementById("pw_baru").innerHTML.length<=5){
            if (document.getElementById("pw_baru").value==document.getElementById("cpw_baru").value){
                document.getElementById("form_ubah_password").submit();
            }
            else
            {
                document.getElementById("val_cpw_baru").innerHTML="Konfirmasikan password baru dengan benar";
            }   
        }
        else
        {
            document.getElementById("val_pw_baru").innerHTML="Minimal jumlah karakter password baru 5 karakter";
        }    
    }
    else
    {
        document.getElementById("val_current_password").innerHTML="Password Lama Salah";
    }
}
</script>
<!-- BEGIN HEADER -->
<?php require_once "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Dinas PKP <small class="page-title-tag">informasi detail</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
          <a href="<?php echo base_url('main/index'); ?>">Home</a>
          <i class="fa fa-2x fa-angle-right"></i>
				</li>
				<li class="active">
					Data
					<i class="fa fa-2x fa-angle-right"></i>
				</li>
				<li class="active">
					 Dinas PKP
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light">
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="<?php echo BASE_URL("main/dinas_view_update")?>" id="form_sample_2" class="form-horizontal" method="post">
								<div class="form-body">
                  <?php
										foreach($divi as $r):
									?>
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										Terdapat kesalahan! Silakan periksa kembali.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Data Berhasil Tersimpan!
									</div>
									<?php if($r->login_count <= 1){
										$kelip = "id='pulsate-regular'";
										?>
										<div class="alert alert-success">
											<button class="close" data-close="alert"></button>
											<b>SELAMAT DATANG!</b>
											<br>Ini adalah kali pertama Anda login ke dalam aplikasi ini.
											<br>Silakan ubah informasi <a href="#">Dinas PKP</a> dan <a href="<?=base_url('main/pengguna_dinas');?>">Pengguna</a> terlebih dahulu sebelum Anda memulai menggunakan aplikasi ini.
											<br>Jangan lupa untuk mengubah password dengan mengklik tombol <b>ubah password</b> di bawah.
											<br><br>Terima kasih,<br><br>Admin
										</div>
									<?php }
										else {
										$kelip = ""; 
										}
								 	?>
								 	<?php if($ubah_pass == 0){?>
								 	<div class="alert alert-success">
											<button class="close" data-close="alert"></button>
											<b>Password Lama Salah!</b>
											<br>Pengguna akan logout otomatis ketika perubahan password berhasil. 
											<br><br>Terima kasih,<br><br>Admin
										</div>
										
								 	<?php }?>
									<div class="form-group">
										<label class="control-label col-md-3">Nama Dinas PKP<span class="required"> * </span></label>
										<div class="col-md-4">
										  <div class="input-icon right">
												<i class="fa"></i>
												<input type="text" class="form-control" name="nama_dinas" value="<?php echo $r->nama_dinas;?>"/>
												<span class="help-block">Nama daerah tak perlu disebutkan.</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Alamat Kantor<span class="required">	* </span></label>
										<div class="col-md-7">
										  <div class="input-icon right">
												<i class="fa"></i>
												<input type="text" class="form-control" name="alamat" value="<?php echo $r->alamat;?>"/>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">No. Telepon </label>
										<div class="col-md-4">
										  <div class="input-icon right">
												<i class="fa"></i>
												<input type="tel" class="form-control" name="telepon" value="<?php echo $r->telepon;?>"/>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">No. Fax</label>
										<div class="col-md-4">
										  <div class="input-icon right">
												<i class="fa"></i>
												<input type="tel" class="form-control" name="fax" value="<?php echo $r->fax;?>"/>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Alamat E-mail</label>
										<div class="col-md-4">
										  <div class="input-icon right">
												<i class="fa"></i>
												<input type="email" class="form-control" name="email" value="<?php echo $r->email;?>"/>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Situs Web</label>
										<div class="col-md-4">
										  <div class="input-icon right">
												<i class="fa"></i>
												<input type="url" class="form-control" name="URL" value="<?php echo $r->URL;?>"/>
												<span class="help-block">contoh: <strong>http://www.namasitusweb.com</strong> atau <strong>http://namasitusweb.com</strong> </span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green pulse-button"> Perbarui Data </button>
											<a href="#change-pass" data-toggle="modal" class="btn btn-md blue-hoki"><div <?=$kelip;?>/>Ubah Password</div></a>
										</div>
									</div>
								</div>
								<?php endforeach; ?>
							</form>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END VALIDATION STATES-->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
			<!-- BEGIN MODAL UBAH PASSWORD -->
			<div class="modal fade" id="change-pass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title uppercase bold">Ubah Password</h4>
						</div>
							<form  action="<?= base_url('main/ganti_password'); ?>" method="POST"class="form-horizontal" id="form_ubah_password">
						<div class="modal-body">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-5 control-label">Password Lama</label>
										<div class="col-md-7">
											<div class="input-group">
												<input name="current_password" id="current_password" type="password" class="form-control" placeholder="Password Lama" autofocus /> 
												<p id="val_current_password"></p>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-5 control-label">Password Baru</label>
										<div class="col-md-7">
											<div class="input-group">
												<input name="pw_baru" id="pw_baru" type="password" class="form-control" placeholder="Password Baru">
												<p id="val_pw_baru"></p>
												<input name="iddinas" type="hidden" class="form-control" value="<?=$iddinas;?>"> 
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-5 control-label">Ulangi Password Baru</label>
										<div class="col-md-7">
											<div class="input-group">
												<input name="cpw_baru" id="cpw_baru" type="password" class="form-control" placeholder="Ulangi Password Baru">
												<p id="val_cpw_baru"></p> 
											</div>
										</div>
									</div>
								</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue" onclick="val_password()">Simpan</button> 
							<button type="reset" class="btn default" data-dismiss="modal">Tutup</button>
						</div>
							</form>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- END MODAL UBAH PASSWORD -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER --> 
<?php require_once "footer2.php";?>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/jquery.pulsate.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/form-validation2.js"></script>
<script src="../../../assets/admin/pages/scripts/ui-general.js" type="text/javascript"></script>
<!-- END PAGE LEVEL STYLES -->
<script>

jQuery(document).ready(function() {
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
	 Layout.init(); // init current layout
	 Demo.init(); // init demo features
   FormValidation.init();
	 UIGeneral.init();
});
</script>

<script>
    function val_profile_form() {
    var kada,wakada;
    kada = document.getElementById("kada").value;
    wakada = document.getElementById("wakada").value;
     //if (isNaN(x) || x < 1 || x > 10) {
    if ((kada=="")) {
        document.getElementById("val_kada").innerHTML = "Masukan Nama Kepala Daerah";
    } else if ((wakada=="")) {
        document.getElementById("val_wakada").innerHTML = "Masukan Nama Wakil Kepala Daerah";
    } else {
        document.getElementById("frofile_form").submit();
		    document.getElementById("val_kada").innerHTML = "";
		    document.getElementById("val_wakada").innerHTML = "";
        document.getElementById("val_letak_geografi").innerHTML = "";  
    }
}
    
    $("#provinsi_filter").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_filter').load(url);
        return false;
    });
    $("#provinsi_filter2").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_profile_filter').load(url);
        return false;
    });

    function val_filter_kab() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab").submit();
        }
    }
    
    function val_filter_kab_prov() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_kota_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab2").submit();
        }
    }
     
    function sel_prov(a) {
        document.getElementById("pilih_prov_text").value=a;
        document.getElementById("pilih_prov").submit();
    }
    
    function val_filter_kab2() {
    var idkabupaten_kota_select;
    idkabupaten_kota_select = document.getElementById("provinsi_filter2").selectedIndex;
    if ((idkabupaten_kota_select==0)) {
        document.getElementById("val_idkabupaten_kota_select2").innerHTML = "Pilih Provinsi";
    } else {
        document.getElementById("form_filter_profile").submit();
    }
}

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
