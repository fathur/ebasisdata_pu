 <!-- BEGIN PRE-FOOTER -->
<div class="page-prefooter">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
        <h2>Situs Terkait</h2>
        <ul class="tautan-prefooter">
          <li><a target="_blank" class="btn grey-mint btn-sm" href="http://pu.go.id">PU-Net</a></li>  
					<li><a target="_blank" class="btn grey-mint btn-sm" href="http://datartlh.perumahan.pu.go.id">Pendataan RTLH</a></li>
					<li><a target="_blank" class="btn grey-mint btn-sm" href="http://pu.go.id/saran/penjelasan">Layanan Pengaduan Masyarakat</a></li>
					<li><a target="_blank" class="btn grey-mint btn-sm" href="http://ehousing.perumahan.pu.go.id">E-Housing</a></li> 
        </ul>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
        <h2>Media Sosial</h2>
        <ul class="social-icons">
          <li>
            <a href="https://www.facebook.com/dirjenpenyediaanperumahan" data-original-title="facebook" class="facebook" target="_blank"></a>
          </li>
          <li>
            <a href="https://twitter.com/perumahan_pupr" data-original-title="twitter" class="twitter" target="_blank"></a>
          </li>
          <li>
            <a href="https://www.instagram.com/perumahan_pupr/" data-original-title="instagram" class="instagram" target="_blank"></a>
          </li>
          <li>
            <a href="https://www.youtube.com/channel/UCd0XnEUHrIGDEpcVmNMIwUQ" target="_blank" data-original-title="youtube" class="youtube"></a>
          </li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
        <h2>Kontak Kami</h2>
        <address style="font-size: 11.5px">
          Direktorat Jenderal Penyediaan Perumahan<br>
          Gedung G Lantai 8<br>
          Jl. Pattimura No. 20 Kebayoran Baru<br>
          Jakarta Selatan 12110<br>
          <span class="fa fa-phone"></span>&nbsp; (021) 7228497<br>
          <span class="fa fa-fax"></span>&nbsp; (021) 7228497<br>
          <span class="fa fa-envelope"></span>&nbsp; <a href="mailto:datinperumahan@gmail.com" target="_blank">datinperumahan@gmail.com</a>
        </address>
      </div>
      <div class="col-md-1 col-sm-6 col-xs-12 footer-block">
        <div>
          <img class="logo-footer logo-footer-mobile" src="<?php echo BASE_URL('application/views/html/aset/logo_footer.png');?>" alt="Kementerian Pekerjaan Umum dan Perumahan Rakyat">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END PRE-FOOTER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
  <div class="container">
    2017 © Direktorat Jenderal Penyediaan Perumahan.
    <a href="http://www.pu.go.id/" title="Situs Web PUPR" target="_blank">Kementerian Pekerjaan Umum dan Perumahan Rakyat</a>
  </div>
</div>
<div class="scroll-to-top" style="display: none;">
  <i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
