var ChartsAmcharts = function() {


    var initChartSample5 = function() {
        var chart = AmCharts.makeChart("chart_5", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,

            "fontFamily": 'Open Sans',

            "color":    '#0',

            "dataProvider": [
            <?php $i=0;  
                 $query = $this->db->query('SELECT kabupaten_kota,rtlh,(SELECT max(rtlh) FROM backlog_kepemilikan_dan_penghunian_kab_view where idprovinsi='.$idprovinsi.') as mak FROM backlog_kepemilikan_dan_penghunian_kab_view where idprovinsi='.$idprovinsi.' and tahun='.$tahun); 
 
		foreach ($query->result_array() as $row):  $i++; if ($row['rtlh']==null){$hsl=1;}else{$hsl=$row['rtlh'];}?>
                {"country": "<?php echo $row['kabupaten_kota'];?>","visits": <?php echo $hsl;?>,"color": "<?php echo "hsl(".$hsl.",100%,70%)";?>"}<?php if ($i<$query->num_rows()){echo ",";};?>
             <?php endforeach; ?>  ],
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "title":"Unit"
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]] unit</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 0,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "labelRotation":90,
                "minHorizontalGap":1

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);

        jQuery('.chart_5_chart_input').off().on('input change', function() {
            var property = jQuery(this).data('property');
            var target = chart;
            chart.startDuration = 0;

            if (property == 'topRadius') {
                target = chart.graphs[0];
            }

            target[property] = this.value;
            chart.validateNow();
        });

        $('#chart_5').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }

    return {
        //main function to initiate the module

        init: function() {
            initChartSample5();
        }

    };

}();
