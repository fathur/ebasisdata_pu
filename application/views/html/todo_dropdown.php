<li class="dropdown dropdown-extended dropdown-dark dropdown-tasks" id="header_task_bar">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										<?php foreach($form_1_progres_nr as $f): $c=$f->c;?> 
										<?php endforeach;?> 
						<i class="icon-calendar"></i>
						<span class="badge badge-default"><?php echo $c;?></span>
						</a>
						<ul class="dropdown-menu extended tasks"> 
							<li>
								<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                        <?php if ($c>0) {?>
										<?php $i=0; foreach($form_1_progres as $r): $i++;?> 
									<li>
										<a href="javascript:;">
										<span class="task">
										<span class="desc"><?php if (($privilage=="2") or ($privilage=="3")){ echo "Form 1A, ";}else{ echo "Form 1B, ";} echo $r->tahun;?></span>
										<span class="percent"><?php echo $r->persen;?>(<?php echo $r->status;?>)</span>
										</span>
										<span class="progress">
										<span style="width: <?php echo $r->persen;?>;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"><span class="sr-only"><?php echo $r->status;?></span></span>
										</span>
										</a>
									</li> 
						<?php endforeach; ?>
                        <?php }else { ?>
                        	<li>
										<a href="javascript:;">
										<span class="task">
										<span class="desc"> </span>
										<span class="percent"> </span>
										</span>
										<span class="progress">
										<span style="width: 0;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"><span class="sr-only"> </span></span>
										</span>
										</a>
									</li> 
                        
                        <?php } ?>
								</ul>
							</li>
						</ul>
					</li>