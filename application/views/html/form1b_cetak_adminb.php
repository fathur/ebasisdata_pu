<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 foreach($form_1_view_detail as $r):
     $idform_1=$r->idform_1;
     $tahun=$r->tahun;
 //$kabupaten_kota=$r->kabupaten_kota;
//	 $kecamatan=$r->kecamatan;
//	 $kelurahan=$r->kelurahan;
 endforeach;
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../../assets/global/img/favicon.png">
<style>p.indent{ padding-left: 1em }</style>
<style>p.indent2{ padding-left: 9.7em }</style>
<style>p.indent3{ padding-left: 3em }</style>
<style>
#kecamatan_combo{
 width:300px;
}
#tahun_form {
 width:100px;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php include "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Formulir IB <small class="page-title-tag"><?php echo $kabupaten_kota;?></small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT <form action="http://localhost/datadekonpera/main/upload" id="form_sample_1" class="form-horizontal" method="post">-->
	<div class="page-content">
		<div class="container">
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('main/index'); ?>">Home</a><i class="fa fa-angle-right"></i>
        </li>
        <li>
           <a href="form_1b_view">Rekapitulasi Formulir IB</a><i class="fa fa-angle-right"></i>
        </li>
        <li class="active">
           Formulir IB
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="basic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Unggah Gambar Struktur Organisasi</h4>
						</div>
						<div class="modal-body">

						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue">Simpan Perubahan</button>
            </div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
      <div id="k2b_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
        <form action=""  class="form-horizontal" method="post" id="k2b_form"/>
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h5 class="modal-title">Jenis Kegiatan Urusan PKP</h5>
							</div>
							<div class="modal-body">
								<p>
							<table id="user" class="table table-bordered table-striped">
							<tbody>
							<tr>
								<td>Jenis Kegiatan Urusan PKP</td>
								<td align="center">
                  <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                  <input name="idform_1_k2_b" id="idform_1_k2_b" type="hidden" class="form-control" />
                  <input name="jenis_kegiatan_urusan_pkp_2"  id="jenis_kegiatan_urusan_pkp_2" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
							</tr>
							<tr>
								<td>Vol/unit (TA. <?=$tahun-1;?>)</td>
								<td align="center"><input name="ta_a_vol_unit_3" id="ta_a_vol_unit_3" type="text" class="form-control"  style="background-color:#E8F3FF" /></td>
							</tr>
							<tr>
								<td>Biaya (TA. <?=$tahun-1;?>)</td>
								<td align="center" ><input name="ta_a_biaya_4" id="ta_a_biaya_4" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
							</tr>
							<tr>
								<td>Vol/unit (TA. <?=$tahun;?>)</td>
								<td align="center"><input name="ta_a_vol_unit_5" id="ta_a_vol_unit_5" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
							</tr>
							<tr>
								<td>Biaya (TA. <?=$tahun;?>)</td>
								<td align="center"><input name="ta_a_biaya_6" id="ta_a_biaya_6" type="text" class="form-control"  style="background-color:#E8F3FF"/> </td>
							</tr>
				  		</tbody>
						</table>
								</p>
							</div>
							<div class="modal-footer">
                <input type="submit" class="btn green" >
								<button type="button" data-dismiss="modal" class="btn default">Batal</button>
							</div>
						</div>
					</div>
					</form>
				</div>
          <div id="k3_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
              <form action=""  class="form-horizontal" method="post" id="k3_form"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title">K.3. PEMBANGUNAN PERUMAHAN </h5>
										</div>
										<div class="modal-body">
                                            <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                                            <input name="idform_1_k3" id="idform_1_k3" type="hidden" class="form-control" />
											<p>1. Apakah Pemerintah Kab/Kota mempunyai Dokumen Perencanaan Urusan Perumahan? </p>
                                            <p class="indent">
                                            <label>
												<input type="checkbox" name="isi_1a" id="isi_1a" value="Y"/> a. RTRW (sudah perda) </label></p>
												<p class="indent"><label>
												<input type="checkbox" name="isi_1b" id="isi_1b" value="Y"/> b. RDTR (sudah perda) </label></p>
												<p class="indent"><label>
												<input type="checkbox" name="isi_1c" id="isi_1c" value="Y"/> c. RP3KP/RP4D </label></p>
												<p class="indent"><label>
												<input type="checkbox" name="isi_1d" id="isi_1d" value="Y"/> d. RPIJM </label></p>
												<p class="indent"><label>
												<input type="checkbox" name="isi_1e" id="isi_1e" value="Y"/> e. Renstra Dinas PKP </label></p>
												<p class="indent">
												<input type="checkbox" name="isi_1f" id="isi_1f" value="Y"/>f. Lainnya (tuliskan) :
												<label><input name="isi_1f_keterangan" id="isi_1f_keterangan" type="text" class="form-control" />
												</label>
												</p>  </br>
                                                <p>2. Dalam pelaksanaan Urusan Perumahan, Pemerintah Kab/Kota memanfaatkan sumber dana yang berasal dari:</p>
												<p class="indent">
												<label><input type="checkbox" name="isi_2a" id="isi_2a" value="Y"/> a. APBD </label></p>
												<p class="indent">
												<label><input type="checkbox" name="isi_2b"  id="isi_2b" value="Y"/> b. Loan (pinjaman) dari badan/bank luar negeri </label></p>
												<p class="indent">
												<input type="checkbox"  name="isi_2c"  id="isi_2c" value="Y"/> c. CSR (uraikan) :
												<label><input name="isi_2c_keterangan" id="isi_2c_keterangan" type="text" class="form-control" /></label>
												</p>
												<p class="indent">
												<input type="checkbox"  name="isi_2d"  id="isi_2d" value="Y"/> d. Swasta (uraikan) :
												<label><input name="isi_2d_keterangan" id="isi_2d_keterangan" type="text" class="form-control" /></label></p>
												<p class="indent">
												<input type="checkbox"  name="isi_2e" id="isi_2e" value="Y"/> e. Lainnya (tuliskan) : APBN
												</p>
										</div>
										<div class="modal-footer">
                                            <input type="submit" class="btn green" >
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>

									</div>
								</div>
										</form>
							</div>

                            <div id="k2_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                                            <form action=""  class="form-horizontal" method="post" id="k2a_form"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title">K.2. ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN PERMUKIMAN (PKP)</h5>
										</div>
										<div class="modal-body">
											<p>
										<table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:40%" align="center">Uraian</td>
											<td style="width:30%" align="center">Tahun <?=$tahun-1;?> (Rp)</td>
											<td style="width:30%" align="center">Tahun <?=$tahun;?> (Rp)</td>
										</tr>
										<tr>
											<td align="center" >
                                            <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                                            <input name="idform_1_k2_a" type="hidden" class="form-control" id="idform_1_k2_a" />
                                            <input name="uraian1" type="text" class="form-control" style="background-color:#E8F3FF" id="uraian1"/>
                                            </td>
											<td align="center">
											<input name="k221" type="text" class="form-control"  style="background-color:#E8F3FF" id="k221" /> </td>
											<td align="center"><input name="k231" type="text" class="form-control" style="background-color:#E8F3FF"  id="k231" /></td>
										</tr>
							  		</tbody>
									</table>
											</p>
										</div>
										<div class="modal-footer">
                                            <input type="submit" class="btn green" >
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>

									</div>
								</div>

										</form>
							</div>
                            <div id="k4_1_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                                            <form action=""  class="form-horizontal" method="post" id="k4_1_form"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title">1. Jumlah Rumah Berdasarkan Status Kepemilikan Tempat Tinggal</h5>
										</div>
										<div class="modal-body">
											<p>
										<table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center">No.</td>
											<td colspan="2" align="center" style="width:45%">Status Kepemilikan Tempat Tinggal</td>
											<td style="width:25%" align="center">Jumlah (unit)</td>
											<td style="width:25%" align="center">Sumber Data</td>
										</tr>
										<tr>
											<td align="center" >1.</td>
											<td colspan="2" align="center">
                        <input name="idform_1" id="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                        <input name="idform_1_k4_1" id="idform_1_k4_1" type="hidden" class="form-control"/>
                        <input name="status1" id="status1" type="text" class="form-control"  style="background-color:#E8F3FF" /></font></td>
											<td align="center" ><input name="k4141" id="k4141" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
											<td align="center"><input name="k4151" id="k4151" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
										</tr>
										<tr>
											<td align="center" >2.</td>
											<td colspan="2" align="center"><input name="status2" id="status2" type="text" class="form-control"  style="background-color:#E8F3FF" /></td>
											<td align="center" ><input name="k4142" id="k4142" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
											<td align="center"><input name="k4152" id="k4152" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
										</tr>
										<tr>
											<td align="center" >3.</td>
											<td colspan="2" align="center"><input name="status3" id="status3" type="text" class="form-control"  style="background-color:#E8F3FF" /></td>
											<td align="center" ><input name="k4143" id="k4143" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
											<td align="center"><input name="k4153" id="k4153" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
										</tr>
										<tr>
											<td align="center" >4.</td>
											<td colspan="2" align="center"><input name="status4" id="status4" type="text" class="form-control"  style="background-color:#E8F3FF" /></td>
											<td align="center" ><input name="k4144" id="k4144" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
											<td align="center"><input name="k4154" id="k4154" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
										</tr>
										<tr>
											<td align="center" >5.</td>
											<td colspan="2" align="center"><input name="status5" id="status5" type="text" class="form-control"  style="background-color:#E8F3FF" /></td>
											<td align="center" ><input name="k4145" id="k4145" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
											<td align="center"><input name="k4155" id="k4155" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
										</tr>

							  		</tbody>
								</table>
											</p>
										</div>
										<div class="modal-footer">
                                            <input type="submit" class="btn green" >
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>

									</div>
								</div>
										</form>
							</div>
                            <div id="k4_2_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                                            <form action=""  class="form-horizontal" method="post"  id="k4_2_form"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title">2. Jenis Fisik Bangunan Rumah</h5>
										</div>
										<div class="modal-body">
											<p>
										  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"  >No.</td>
											<td style="width:45%" align="center" >
                                            Jenis Fisik Bangunan Rumah</td>
										  <td style="width:25%" align="center">Jumlah (unit)</td>
										  <td style="width:25%" align="center" >Sumber Data</td>
										</tr>
										<tr>
											<td align="center" >1.</td>
											<td align="left">
                                            <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                                            <input name="idform_1_k4_2" id="idform_1_k4_2" type="hidden" class="form-control"/>
                                            <input name="jenis1" id="jenis1" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
											<td align="center" ><input name="k4231" id="k4231" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
											<td align="center"><input name="k4241" id="k4241" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
										</tr>
										<tr>
											<td align="center" >2.</td>
											<td align="left"><input name="jenis2" id="jenis2" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
											<td align="center" ><input name="k4232" id="k4232" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
											<td align="center"><input name="k4242" id="k4242" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
										</tr>
							  		</tbody>
							    </table>
											</p>
										</div>
										<div class="modal-footer">
                                            <input type="submit" class="btn green" >
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>

									</div>
								</div>
										</form>
							</div>
                            <div id="k4_3_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                                            <form action=""  class="form-horizontal" method="post" id="k4_3_form"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title">3. Jumlah Kepala Keluarga (KK) dalam satu rumah</h5>
										</div>
										<div class="modal-body">
											<p>
										  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"  >No.</td>
											<td style="width:45%" align="center"  >Fungsi Rumah</td>
										  <td style="width:25%" align="center">Jumlah (unit)</td>
										  <td style="width:25%" align="center" >Sumber Data</td>
										</tr>
										<tr>
											<td align="center" >1.</td>
											<td align="left">
                                            <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                                            <input name="idform_1_k4_3" id="idform_1_k4_3" type="hidden" class="form-control" />
                                            <input name="fungsi1" id="fungsi1" type="text" class="form-control" value="Rumah Dengan 1 (satu) KK"  style="background-color:#E8F3FF"/></td>
											<td align="center" ><input name="k4331" id="k4331" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
											<td align="center"><input name="k4341" id="k4341" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
										</tr>
										<tr>
											<td align="center" >2.</td>
											<td align="left"><input name="fungsi2" id="fungsi2" type="text" class="form-control" value="Rumah dengan lebih dari 1 (satu) KK" style="background-color:#E8F3FF"/></td>
											<td align="center" ><input name="k4332" id="k4332" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
											<td align="center"><input name="k4342" id="k4342" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
										</tr>

							  		</tbody>
								  </table>
											</p>
										</div>
										<div class="modal-footer">
                                            <input type="submit" class="btn green" >
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>

									</div>
								</div>
										</form>
							</div>
                            <div id="k4_4_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                                            <form action=""  class="form-horizontal" method="post" id="k44_form"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title">4. Jumlah sambungan listrik rumah (PLN)</h5>
										</div>
										<div class="modal-body">
											<p>
										  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td>Kab/Kota
                                            <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"  style="background-color:#E8F3FF"/>
                                            <input name="idform_1_k4_4" id="idform_1_k4_4" type="hidden" class="form-control"/>
                                            </td>
                                            <td><select name="idkabupaten_kota" id="idkabupaten_kota" class="form-control">
                                        <?php foreach($kabupaten_kota_combo as $r): ?>
                                        <option value="<?php echo $r->idkabupaten_kota;?>"  style="background-color:#E8F3FF"><?php echo $r->kabupaten_kota;?> </option>
										<?php endforeach; ?>
                                     </select> </td>
  										<tr>
                                          </tr>
										  <td>Jumlah rumah (unit) <?=$tahun-1;?></td>
                                          <td><input name="jumlah_rumah_3" id="jumlah_rumah_3" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
  										<tr>
                                          </tr>
										  <td>Jumlah rumah (unit) <?=$tahun;?></td>
                                          <td align="center"><input name="jumlah_rumah_4" id="jumlah_rumah_4" type="text" class="form-control"  style="background-color:#E8F3FF"/>
  										<tr>
                                          </tr>
										  <td >Sumber Data</td>
                                          <td align="center"><input name="sumber_data_5" id="sumber_data_5" type="text" class="form-control"  style="background-color:#E8F3FF"/>
										</tr>
							  		</tbody>
								  </table>
											</p>
										</div>
										<div class="modal-footer">
                                            <input type="submit" class="btn green" >
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>

									</div>
								</div>
										</form>
							</div>
                            <div id="k4_5_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                                            <form action=""  class="form-horizontal" method="post" id="k45_form"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title">5. Jumlah Pembangunan Rumah dalam 1 tahun</h5>
										</div>
										<div class="modal-body">
											<p>
										     <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:27%" align="center"  colspan="2">Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun-1;?> (unit)</td>

										</tr>
										<tr>
											<td align="center"  >Non MBR</td>
											<td align="center"  >MBR</td>
										</tr>
										<tr>
											<td align="center">
                                            <input name="idform_1_k4_5" id="idform_1_k4_5" type="hidden"/>
                                            <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                                            <input name="non_mbr_2" id="non_mbr_2" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
											<td align="center" ><input name="mbr_3" id="mbr_3" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
										</tr>
										<tr>
										  <td style="width:27%" align="center" colspan="2">Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun;?> (unit)  (unit).....Tahun</font></td>
										</tr>
										<tr>
											<td align="center"  >Non MBR</td>
											<td align="center"  >MBR</td>
										</tr>
										<tr>
											<td align="center" ><input name="non_mbr_4" id="non_mbr_4" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
											<td align="center" ><input name="mbr_5" id="mbr_5" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
										</tr>
										<tr>
                                          <td style="width:27%" align="center" colspan="2"><font color="#FF0000">Jumlah Pembangunan Rumah Berdasarkan Non IMB (unit).....Tahun</font></td>
										</tr>
										<tr>
											<td align="center"  >Non MBR</td>
											<td align="center"  >MBR</td>
										</tr>
										<tr>
											<td align="center"><input name="non_mbr_6" id="non_mbr_6" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
											<td align="center" ><input name="mbr_7" id="mbr_7" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
										</tr>
										<tr>
										   <td align="center" colspan="2">Sumber Data</td>
										</tr>
										<tr>
											<td align="center" colspan="2"><input name="sumber_data_8" id="sumber_data_8" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
										</tr>

							  		</tbody>
								  </table>
											</p>
										</div>
										<div class="modal-footer">
                                            <input type="submit" class="btn green" >
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>

									</div>
								</div>
										</form>
							</div>
                            <div id="k5_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                                            <form action=""  class="form-horizontal" method="post" id="k5_form"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title">K.5. RUMAH TIDAK LAYAK HUNI</h5>
										</div>
										<div class="modal-body">
											<p>
										     <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:25%" align="center">Kab/Kota</td><td align="left">
                                            <select name="idkabupaten_kota" id="idkabupaten_kota"  class="form-control" >
                                        <?php foreach($kabupaten_kota_combo as $r): ?>
                                        <option value="<?php echo $r->idkabupaten_kota;?> "  style="background-color:#E8F3FF"><?php echo $r->kabupaten_kota;?> </option>
										<?php endforeach; ?>
                                     </select>
                                     </td>
										<tr>
										</tr>
											<td style="width:15%" align="center">Jumlah  KK/RT</td><td align="center" >

                                            <input name="idform_1_k5" id="idform_1_k5" type="hidden"/>
                                            <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                                            <input name="jumlah_kk_rt_3" id="jumlah_kk_rt_3" type="text" class="form-control" /></td>
										<tr>
										</tr>
											<td style="width:15%" align="center">Jumlah  RTLH Versi BDT (unit)</td><td align="center">
<input name="jumlah_rtlh_versi_bdt_4" id="jumlah_rtlh_versi_bdt_4" type="text" class="form-control" /></td>
										<tr>
										</tr>
											<td style="width:15%" align="center">Jumlah RTLH  Verifikasi Pemda (unit)</td><td align="center" >
<input name="jumlah_rtlh_verifikasi_pemda_5" id="jumlah_rtlh_verifikasi_pemda_5" type="text" class="form-control" /></td>
										<tr>
										</tr>
                                            <td align="center">Sumber Data</td><td align="center">
<input name="sumber_data_6" id="sumber_data_6" type="text" class="form-control" /></td>
										</tr>
							  		</tbody>
								</table>
											</p>
										</div>
										<div class="modal-footer">
                                            <input type="submit" class="btn green" >
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>

									</div>
								</div>
										</form>
							</div>
                            <div id="k6_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                                            <form action=""  class="form-horizontal" method="post" id="k6_form"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title">K.6. KAWASAN KUMUH </h5>
										</div>
										<div class="modal-body">
											<p>
										      <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>

											<td style="width:40%" >Kab/Kota</td><td align="left"><input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/><input name="idform_1_k6" id="idform_1_k6" type="hidden"/>
										<select name="kabupaten_kota_select" id="kabupaten_kota_select" class="form-control">
                                        <option value=""  style="background-color:#E8F3FF">Pilih Kabupaten / Kota</option>
                                        <?php foreach($kabupaten_kota_combo as $r): ?>
                                        <option value="<?php echo $r->idkabupaten_kota;?> "  style="background-color:#E8F3FF"><?php echo $r->kabupaten_kota;?> </option>
										<?php endforeach; ?>
                                     </select></td>
										<tr>
										</tr>
											<td>Kecamatan</td><td >
<select name="kecamatan_combo[]" id="kecamatan_combo"  multiple="multiple" class="form-control">
<option value=""  style="background-color:#E8F3FF">Kecamatan</option>
<?php foreach($kecamatan as $r): ?>
                                        <option value="<?php echo $r->idkecamatan;?> "  style="background-color:#E8F3FF"><?php echo $r->kecamatan;?> </option>
										<?php endforeach; ?>
                                     </select><p>Tekan Control(ctrl) pada papan keyboard untuk memilih lebih dari satu kecamatan</p></td>
										<tr>
										</tr>

											<td style="width:15%" >Luas wilayah kumuh (Ha)</td>
<td align="center"><input name="luas_wilayah_kumuh_4" id="luas_wilayah_kumuh_4" type="text" class="form-control" /></td>
										<tr>
										</tr>
											<td style="width:15%" >Jumlah RTLH dalam wilayah kumuh (unit)</td>
<td align="center" ><input name="jumlah_rtlh_dalam_wilayah_kumuh_5" id="jumlah_rtlh_dalam_wilayah_kumuh_5" type="text" class="form-control" /></td>
										<tr>
										</tr>
                                            <td >Sumber Data</td>
<td align="center"><input name="sumber_data_6" id="s6" type="text" class="form-control"/></td>
										<tr>
										</tr>
										</tr>
							  		</tbody>
								</table>
											</p>
										</div>
										<div class="modal-footer">
                                            <input type="submit" class="btn green" >
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>

									</div>
								</div>
										</form>
							</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="portlet light margin-top-10">
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
              <!-- BEGIN FORM-->
              <div class="form-actions">
								<div class="row">
								<div class="form-body">
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										Terdapat Input Yang Salah. Periksa Kembali.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Form Anda Telah Tersimpan!
									</div>
							<table id="user" class="table table-bordered table-striped">
							<tbody>
							<tr>
								<td style="width:15%">
									 Kabupaten/Kota
								</td>
								<td style="width:50%">
									<?php echo $kabupaten_kota;?>
                  <input name="idkabupaten_kota" type="hidden" class="form-control" value="<?php echo $idkabupaten_kota;?>"/>
									<input name="kabupaten_kota" type="hidden" class="form-control" value="<?php echo $kabupaten_kota;?>"/>
								</td>
								<td style="width:35%" align="center">
									<span class="text-muted">
									FORMULIR IB </span>
								</td>
							</tr>
							<tr>
								<td>
									 Nama Dinas
                   <input name="iddinas" type="hidden" class="form-control" value="<?php echo $iddinas;?>"/>
                   <input name="privilage" type="hidden" class="form-control" value="<?php echo $privilage;?>"/>
                   <input name="idform_1" type="hidden" class="form-control" value="<?php echo $idform_1;?>"/>
								</td>
								<td>
									  <?php echo $nama_dinas;?>
								</td>
								<td rowspan="2" align="center" valign="middle">
                	<form action="<?php echo base_url('/main/Form1_update_tahun2');?>" method="post">
                    <input name="idform_1" type="hidden" value="<?php echo $idform_1;?>"/>
                    <input name="tahun" type="hidden" value="<?php echo $tahun;?>"/>
                  	 <select name="tahun_form"  id="tahun_form" class="form-control"  onchange='this.form.submit()'>
                     <?php if (isset($tahun)){ echo "<option value=".$tahun.">".$tahun."</option>"; } else { ?>
                     <option value="0">Tahun</option>
                     <?php } for($t=date('Y');$t>=date('Y')-10;$t--){?>
                     <option value="<?php echo $t;?>"><?php echo $t;?></option>
                     <?php }?>
                     </select>
                		<noscript><input type="submit" value="Submit"></noscript>
									</form>
								</td>
							</tr>
							<tr>
								<td>
									 Alamat
								</td>
								<td>
									 <?php echo $alamat;?>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center">
									<h3 class="uppercase"><strong>DAFTAR ISIAN PENDATAAN PKP <?=$kabupaten_kota;?> TAHUN <?=$tahun;?></strong></h3>
								</td>
							</tr>
							<tr>
								<td colspan="3" class="note note-info">
                  <p>Kegiatan pendataan perumahan ini dilakukan untuk memenuhi kebutuhan data dan
                  informasi perumahan. Data ini merupakan bagian dari Pembangunan Basis Data PKP
                  Kab/Kota sesuai UU No.1 Tahun 2011 tentang Perumahan dan Kawasan Permukiman (PKP)
                  pasal 17 dan Peraturan Pemerintah No.88 Tahun 2014 tentang Pembinaan Penyelenggaraan
                  PKP pasal 18 ayat 1.</p>
                  <p>Tabel ini diisi oleh SNVT bidang Perumahan Kab/Kota, untuk kelengkapan datanya
                  berkoordinasi dengan Pokja PKP Kab/Kota, BPS Kab/Kota, Bappeda Kab/Kota, Dinas
                  Sosial Kab/Kota, BKKBN Kab/Kota, serta rekapitulasi data dari Form 1B oleh Kab/Kota.
                  Untuk pertanyaan lebih lanjut dapat menghubungi Subdit Data dan Informasi, Direktorat
                  Perencanaan Penyediaan Perumahan :  datinperumahan@gmail.com (021.7211883)
                  Daftar isian ini merupakan data sekunder hasil dari berbagai kegiatan pendataan
                  perumahan, terdiri dari:</p>
                  K.1. Kelembagaan Perumahan dan Permukiman</br>
                  K.2. Alokasi APBD untuk Urusan Perumahan dan Kawasan Permukiman (PKP)</br>
                  K.3. Pembangunan Perumahan</br>
                  K.4. Backlog Perumahan</br>
                  K.5. Rumah Tidak Layak Huni</br>
                  K.6. Kawasan Kumuh
								</td>
							</tr>
							<tr>
								<td colspan="3">
									 <span class="text-muted">K.1. KELEMBAGAAN PERUMAHAN DAN PERMUKIMAN
<p>Struktur Dinas yang menangani perumahan dan permukiman di Kab/Kota </p></span>
<center>
<?php foreach($form_1_k1_count as $f1k1count):
                                  if ($f1k1count->f1k1count<1) {?>
                              		<img src="../../../assets/uploads/sample.jpg" height="400" width="700"></img><p></p>
                              	<?php } else {?>
                              <img src="<?php foreach($form_1_k1 as $f1k1): ?><?php echo ".".$f1k1->struktur_dinas;?><?php endforeach;?>" height="400" width="700"></img><p></p>
                              <?php } endforeach;?>
                               
                            </center>
          								</td>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">Penjelasan :
<p>Anggaran total APBD Kab/Kota terhadap anggaran SKPD/Dinas PKP Kab/Kota dalam tahun yang sama, sehingga didapat persentase perbandingan anggaran urusan PKP, yaitu : 0,061 atau 6 %</p></span>
								</td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">K.2. ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN PERMUKIMAN (PKP)</span> </br></br>
                                      <div id="k2_a_div"> 
                                     <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:50%" align="center">Uraian</td>
											<td style="width:15%" align="center">Tahun <?=$tahun-1;?> (Rp)<label class="control-label"><span class="required">*</span></label></td>
											<td style="width:15%" align="center">Tahun <?=$tahun;?> (Rp)</td>
											<td style="width:20%" align="center">Fungsi</td>
										</tr>
										<tr>
											<td  align="center"><font size="1">(1)</font></td>
											<td  align="center"><font size="1">(2)</font></td>
											<td  align="center"><font size="1">(3)</font></td>
											<td  align="center"><font size="1">(4)</font></td>
										</tr>
                                        <?php foreach($form_1_k2_a as $f1k2av): ?>
										<tr>
											<td align="center"><?php echo $f1k2av->uraian1;?></td>
											<td align="center">

											<?php echo "Rp. ".number_format( $f1k2av->k221 , 2 , ',' , '.' );?></td>
											<td align="center"><?php echo "Rp. ".number_format( $f1k2av->k231 , 2 , ',' , '.' ) ;?></td>
											<td align="center"> 
                                        </td>
										</tr>
                                        <?php endforeach; ?>
							  		</tbody>
									</table>

								</td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">Penjelasan :<div id="k2_b_div">
<p>Anggaran total APBD Kabupaten terhadap anggaran SKPD/Dinas PKP Kabupaten dalam tahun yang sama, sehingga didapat persentase perbandingan anggaran urusan PKP, yaitu : 0,061 atau 6,1 %</p></span>
								</td>
							</tr>
							<tr>
								<td colspan="3"> 
									<table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center" rowspan="2">No.</td>
											<td style="width:30%" align="center" rowspan="2">Jenis Kegiatan Urusan PKP
</td>
											<td style="width:25%" align="center" colspan="2">TA. <?=$tahun-1;?></td>
											<td style="width:25%" align="center" colspan="2">TA. <?=$tahun;?></td>
											<td style="width:15%" align="center" rowspan="2">Fungsi</td>
										</tr>
										<tr>
											<td align="center">Vol/unit</td>
											<td align="center" >Biaya (Rp.)</td>
											<td align="center">Vol/unit</td>
											<td align="center">Biaya (Rp.)</td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
											<td align="center" ><font size="1">(4)</font></td>
											<td align="center"><font size="1">(5)</font></td>
											<td align="center"><font size="1">(6)</font></td>
											<td align="center"><font size="1">(7)</font></td>
										</tr>
                                            <?php $i=0; foreach($form_1_k2_b as $f1k2b3): ?>
										<tr>
											<td align="center" ><?php $i++; echo $i;?>.</td>
											<td align="center"><?php echo $f1k2b3->jenis_kegiatan_urusan_pkp_2;?></td>
											<td align="center"><?php echo $f1k2b3->ta_a_vol_unit_3;?></td>
											<td align="center" ><?php echo "Rp. ".number_format( $f1k2b3->ta_a_biaya_4 , 2 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k2b3->ta_a_vol_unit_5;?></td>
											<td align="center"><?php echo "Rp. ".number_format( $f1k2b3->ta_a_biaya_6 , 2 , ',' , '.' ) ;?> </td>
                                            <td align="center">
                                      
                                        </td>
										</tr>
                                        <?php endforeach; ?>
										<tr>
											<td align="center" ></td>
											<td align="center"></font></td>
											<td align="center"></font></td>
											<td align="center" ></font></td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
										</tr>
										<tr>
                                            <?php $i=0; foreach($form_1_k2_b_sum as $f1k2b3sum): ?>
											<td align="center" ></td>
											<td align="right">Total unit rumah</font></td>
											<td align="center"><?php echo $f1k2b3sum->total_unit_rumah_1;?></font></td>
											<td align="right" >Total unit rumah</font></td>
											<td align="center"><?php echo $f1k2b3sum->total_unit_rumah_2;?></td>
											<td align="center"></td>
											<td align="center"></td>
                                        <?php endforeach; ?>
										</tr>

							  		</tbody>
									</table>
                                    <p>*) Total unit rumah dikumpulkan pada acara Forum Sinkronisasi Pendataan Perumahan sebagai data Program Suplai Perumahan</p>
								</td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 Penjelasan :
<p>Kegiatan fisik, biasanya rehabilitasi rumah berupa peningkatan kualitas, yang bersumber dari APBD Kab/Kota/anggaran Dinas PKP Kab/Kota, volumenya berupa unit, didata sebagai data pembangunan unit baru lingkup Program Suplai Perumahan</p>
								</td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 K.3.  PEMBANGUNAN PERUMAHAN </br></br><div id="k3_div">
<p>1. Apakah Pemerintah Kab/Kota mempunyai Dokumen Perencanaan Urusan Perumahan? </p>
<p> 
                       <?php $i=0; foreach($form_1_k3 as $f1k3): ?>
												<p class="indent"><label>
												<input type="checkbox" name="isi_1a" <?php if($f1k3->isi_1a=='Y') { echo "checked"; } ?> value="Y" disabled/> a. RTRW (sudah perda) </label></p>
												<p class="indent"><label>
												<input type="checkbox" name="isi_1b" <?php if($f1k3->isi_1b=='Y') { echo "checked"; } ?> value="Y" disabled/> b. RDTR (sudah perda) </label></p>
												<p class="indent"><label>
												<input type="checkbox" name="isi_1c" <?php if($f1k3->isi_1c=='Y') { echo "checked"; } ?> value="Y" disabled/> c. RP3KP/RP4D </label></p>
												<p class="indent"><label>
												<input type="checkbox" name="isi_1d" <?php if($f1k3->isi_1d=='Y') { echo "checked"; } ?> value="Y" disabled/> d. RPIJM </label></p>
												<p class="indent"><label>
												<input type="checkbox"name="isi_1e" <?php if($f1k3->isi_1e=='Y') { echo "checked"; } ?> value="Y" disabled/> e. Renstra Dinas PKP </label></p>
												<p class="indent">
												<input type="checkbox" name="isi_1f" <?php if($f1k3->isi_1f=='Y') { echo "checked"; } ?> value="Y" disabled/>
												f. Lainnya (tuliskan) :
												<label><?php echo $f1k3->isi_1f_keterangan;?>
												</label>
												</p>
											<p class="indent"><label>
											  </label></p></br>
<p>2. Dalam pelaksanaan Urusan Perumahan, Pemerintah Kab/Kota memanfaatkan sumber dana yang berasal dari:</p>
<div class="checkbox-list" data-error-container="#form_2_services_error">
												<p class="indent">
												<label><input type="checkbox" name="isi_2a" <?php if($f1k3->isi_2a=='Y') { echo "checked"; } ?> value="Y" disabled/> a. APBD </label></p>
												<p class="indent">
												<label><input type="checkbox" name="isi_2b" <?php if($f1k3->isi_2b=='Y') { echo "checked"; } ?> value="Y" disabled/> b. Loan (pinjaman) dari badan/bank luar negeri </label></p>
												<p class="indent">
												<input type="checkbox"  name="isi_2c" <?php if($f1k3->isi_2c=='Y') { echo "checked"; } ?> value="Y" disabled/> c. CSR (uraikan) :
												<label><?php echo $f1k3->isi_2c_keterangan;?></label>
												</p>
												<p class="indent">
												<input type="checkbox"  name="isi_2d" <?php if($f1k3->isi_2d=='Y') { echo "checked"; } ?> value="Y" disabled/> d. Swasta (uraikan) :
												<label><?php echo $f1k3->isi_2d_keterangan;?></label>

												</p>
												<p class="indent"><label>
												<input type="checkbox"  name="isi_2e" <?php if($f1k3->isi_2e=='Y') { echo "checked"; } ?> value="Y" disabled/> e. Lainnya (tuliskan) : APBN </label></p>

											<p class="indent"><label><span class="help-block">
											  </span>
											</label></p>
                                            
<?php endforeach;?>
</span>
								</td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">Penjelasan :
<p>Kelengkapan dokumen kelengkapan/pendukung program/kegiatan Perumahan dan Kawasan Permukiman yang telah ada dasar leglisasinya seperti Perda atau Pergub, dan juga sumber dana yang pernah digunakan untuk pembangunan/rehabilitasi rumah di Kab/Kota</p></span>
								</td>
							</tr>
							<tr>
							  <td colspan="3"class = "note note-info">
									 <span class="text-muted">K.4. BACKLOG PERUMAHAN<div id="k4_1_div">
									 <p>1. Jumlah Rumah Berdasarkan Status Kepemilikan Tempat Tinggal </p></span> 
                                     <?php $i=0; foreach($form_1_k4_1 as $f1k41): ?>
                                      
                          <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center">No.</td>
											<td colspan="2" align="center" style="width:45%">Status Kepemilikan Tempat Tinggal</td>
											<td style="width:25%" align="center">Jumlah (unit)</td>
											<td style="width:25%" align="center">Sumber Data</td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td colspan="2" align="center"><font size="1">(2)</font></td>
											<td align="center" ><font size="1">(3)</font></td>
											<td align="center"><font size="1">(4)</font></td>
										</tr>
										<tr>
											<td align="center" >1.</td>
											<td colspan="2" ><?php echo $f1k41->status1;?></font></td>
											<td align="center" ><?php echo number_format( $f1k41->k4141 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4151;?></td>
										</tr>
										<tr>
											<td align="center" >2.</td>
											<td colspan="2" ><?php echo $f1k41->status2;?></td>
											<td align="center" ><?php echo number_format( $f1k41->k4142 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4152;?></td>
										</tr>
										<tr>
											<td align="center" >3.</td>
											<td colspan="2" ><?php echo $f1k41->status3;?></td>
											<td align="center" ><?php echo number_format( $f1k41->k4143 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4153;?></td>
										</tr>
										<tr>
											<td align="center" >4.</td>
											<td colspan="2" ><?php echo $f1k41->status4;?></td>
											<td align="center" ><?php echo number_format( $f1k41->k4144 , 0, ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4154;?></td>
										</tr>
										<tr>
											<td align="center" >5.</td>
											<td colspan="2" ><?php echo $f1k41->status5;?></td>
											<td align="center" ><?php echo number_format( $f1k41->k4145 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4155;?></td>

										</tr>
										<tr>
											<td align="center" ></td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center" ></td>
											<td align="center"></td>
										</tr>
										<tr>
											<td align="center" ></td>
											<td colspan="2" align="right">Total A (2+3+4+5)</td>
											<td align="center" ><?php echo number_format( $f1k41->total_a , 0 , ',' , '.' ) ;?></td>
											<td align="center">&nbsp;</td>
										</tr>
										<tr>
											<td align="center" ></td>
											<td colspan="2" align="right">Total B (3)</td>
											<td align="center" ><?php echo number_format( $f1k41->total_b , 0 , ',' , '.' ) ;?></td>
											<td align="center">&nbsp;</td>
										</tr>

							  		</tbody>
								</table>
                                <p> Backlog kepemilikan = (Total A) = <?php echo number_format( $f1k41->total_a , 0 , ',' , '.' ) ;?> unit</p>
                              <p>Backlog penghunian = (Total B  = <?php echo number_format( $f1k41->total_b , 0 , ',' , '.' ) ;?> unit</p></td>
                                <?php endforeach;?>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">Penjelasan :
<p>Data backlog kepemilikan dan backlog penghunian kabupaten ditentukan oleh Dinas PKP, data dari BPS dan BKKBN hanya sebagai benchmark data yang perlu divalidasi oleh Dinas PKP.</p><p>
Catatan : data BKKBN bersumber dari sensus Pendataan Keluarga 2015 oleh BKKBN, yang telah menandatangani Kesepakatan Bersama dengan Kementerian PUPR Nomor:1/KSM/G2/2017 Nomor:01/PKS/M/2017 tentang Peningkatan Program Kependudukan, Keluarga Berencana dan Pembangunan Keluarga dalam Pembangunan Infrastruktur Pekerjaan Umum dan Perumahan Rakyat, tanggal 11 Januari 2017
</p></span>
								</td>
							</tr>
							<tr>
							  <td colspan="3">
									 <p>2. Jenis Fisik Bangunan Rumah</p></span>  <div id="k4_2_div"> 
                                <?php $i=0; foreach($form_1_k4_2 as $f1k42): ?>
                                 
                          <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"  >No.</td>
											<td style="width:45%" align="center"  >Jenis Fisik Bangunan Rumah</td>
										  <td style="width:25%" align="center">Jumlah (unit)</td>
										  <td style="width:25%" align="center" >Sumber Data</td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td><td align="center"><font size="1">(4)</font></td>


										</tr>
										<tr>
											<td align="center" >1.</td>
											<td align="left"><?php echo $f1k42->jenis1;?></td>
											<td align="center" ><?php echo number_format( $f1k42->k4231 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k42->k4241;?></td>
										</tr>
										<tr>
											<td align="center" >2.</td>
											<td align="left"><?php echo $f1k42->jenis2;?></td>
											<td align="center" ><?php echo number_format( $f1k42->k4232 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k42->k4242;?></td>
										</tr>
										<tr>
											<td align="center" ></td>
											<td align="right">Total</td>
											<td align="center"><?php echo number_format( $f1k42->total , 0 , ',' , '.' ) ;?></td>
											<td align="center" >&nbsp;</td>
										</tr>

							  		</tbody>
							    </table>
                                <?php endforeach;?>
                                <p>&nbsp;</p></td>
							</tr>
							<tr>
								<td colspan="3">
									 <span class="text-muted">Penjelasan :
<p>Jumlah fisik bangunan rumah, bukan data Rumah Tangga atau data Kepala Keluarga, tapi merupakan unit bangunan rumah baik terhuni maupun tidak. </p></span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									 <p>3. Jumlah Kepala Keluarga (KK) dalam satu rumah</p> <div id="k4_3_div"> 
									 </span>
                                     <?php $i=0; foreach($form_1_k4_3 as $f1k43): ?>
                                      
                          <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"  >No.</td>
											<td style="width:45%" align="center"  >Fungsi Rumah</td>
										  <td style="width:25%" align="center">Jumlah (unit)</td>
										  <td style="width:25%" align="center" >Sumber Data</td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td><td align="center"><font size="1">(4)</font></td>


										</tr>
										<tr>
											<td align="center" >1.</td>
											<td align="left"><?php echo $f1k43->fungsi1;?></td>
											<td align="center" ><?php echo number_format( $f1k43->k4331 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k43->k4341;?></td>
										</tr>
										<tr>
											<td align="center" >2.</td>
											<td align="left"><?php echo $f1k43->fungsi2;?></td>
											<td align="center" ><?php echo number_format( $f1k43->k4332 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k43->k4342;?></td>
										</tr>

							  		</tbody>
								  </table>
                                <?php endforeach;?>
								</td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">
								  <p>&nbsp;</p></span>
								</td>
							</tr>
                            <tr>
								<td colspan="3">
								  <p>4. Jumlah sambungan listrik rumah (PLN)</p>
 
                          <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"  rowspan="2">No.</td>
											<td style="width:30%" align="center" rowspan="2">Kab/Kota</td>
										  <td style="width:25%" align="center" colspan="2">Jumlah rumah (unit)</td>
										  <td style="width:25%" align="center" rowspan="2">Sumber Data</td>
										  <td style="width:30%" align="center" rowspan="2">Fungsi</td>
										</tr>
										<tr>
											<td align="center" ><?=$tahun-1;?></td>
											<td align="center"><?=$tahun;?></td>

										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
                                            <td align="center"><font size="1">(4)</font></td>
                                            <td align="center"><font size="1">(5)</font></td>
                                            <td align="center"><font size="1">(6)</font></td>


										</tr>
                                        <?php $i=0; foreach($form_1_k4_4 as $f1k44): ?>
										<tr>
											<td align="center" ><?php $i++; echo $i;?>.</td>
											<td align="left"><?php echo $f1k44->kabupaten_kota;?>
											<td align="center" > <?php echo  number_format( $f1k44->jumlah_rumah_3 , 0 , ',' , '.' ) ;?></td>
											<td align="center">  <?php echo  number_format( $f1k44->jumlah_rumah_4 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k44->sumber_data_5;?></td>
                                            <td align="center">
                                       
                                        </td>
										</tr>
                                		<?php endforeach;?>
										<tr>
                                        <?php $i=0; foreach($form_1_k4_4_sum as $f1k44sum): ?>
											<td colspan="2" align="right" >Total</td>
											<td align="center" > <?php echo  number_format( $f1k44sum->total_1 , 0 , ',' , '.' ) ;?></td>
											<td align="center"> <?php echo  number_format( $f1k44sum->total_2 , 0 , ',' , '.' ) ;?></td>
                                		<?php endforeach;?>
										</tr>
							  		</tbody>
								  </table>
								</td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">Penjelasan :
<p>Data Sambungan Listrik berasal dari PLN, per kabupaten/kota.  </p></span>
								</td>
							</tr>
                            <tr>
								<td colspan="3">
									 <p>5. Jumlah Pembangunan Rumah dalam 1 tahun</p><div id="k4_5_div"></div> 
                          <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"  rowspan="2">No.</td>
											<td style="width:20%" align="center"  colspan="2">Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun-1;?> (unit)</td>
										  <td style="width:20%" align="center" colspan="2">Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun;?> (unit)</td>
                                          <td style="width:20%" align="center" colspan="2"><font color="#FF0000">Jumlah Pembangunan Rumah Berdasarkan Non IMB (unit).....Tahun</font></td>
										  <td align="center" rowspan="2">Sumber Data</td>
										  <td align="center" rowspan="2">Fungsi</td>
										</tr>
										<tr>
											<td align="center"  >Non MBR</td>
											<td align="center"  >MBR</td>
											<td align="center"  >Non MBR</td>
											<td align="center"  >MBR</td>
											<td align="center"  >Non MBR</td>
											<td align="center"  >MBR</td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
                                            <td align="center"><font size="1">(4)</font></td>
											<td align="center"><font size="1">(5)</font></td>
                                            <td align="center"><font size="1">(6)</font></td>
											<td align="center"><font size="1">(7)</font></td>
                                            <td align="center"><font size="1">(8)</font></td>
                                            <td align="center"><font size="1">(9)</font></td>


										</tr>
                                        <?php $i=0; foreach($form_1_k4_5 as $f1k45): ?>
										<tr>
											<td align="center" ><?php $i++; echo $i;?>.</td>
											<td align="center"><?php echo number_format( $f1k45->non_mbr_2 , 0 , ',' , '.' ) ;?></td>
											<td align="center" ><?php echo number_format( $f1k45->mbr_3 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo number_format( $f1k45->non_mbr_4 , 0 , ',' , '.' ) ;?></td>
											<td align="center" ><?php echo number_format( $f1k45->mbr_5 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo number_format( $f1k45->non_mbr_6 , 0 , ',' , '.' ) ;?></td>
											<td align="center" ><?php echo number_format( $f1k45->mbr_7 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k45->sumber_data_8;?></td>
                                            <td align="center">
                                              
                                        </td>
										</tr>
                                        <?php endforeach;?>

							  		</tbody>
								  </table>
                                    <p>*) Jumlah Pembangunan Rumah Berdasarkan IMB 2016 (unit) dikumpulkan pada acara Forum Sinkronisasi Pendataan Perumahan sebagai data Program Suplai Perumahan (identifikasi awal perkiraan pembangunan rumah)</p>
								</td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">Penjelasan :
<p>Data pembangunan unit rumah per tahun oleh seluruh stakeholder perumahan terdata dalam IMB melalui Dinas PTSP, dan jumlah yang tidak terdata dalam IMB dianalisis/diasumsikan oleh Dinas PKP, sehingga didapat perkiraan pembangunan unit rumah satu kabupaten dalam satu tahun. Ini disebut data supply perumahan. Data total dari seluruh Kabupaten/Kota</p></span>
								</td>
							</tr>

							<tr>
							  <td colspan="3"class = "note note-info">
                              <span class="text-muted">K.5. RUMAH TIDAK LAYAK HUNI </span></br></br><div id="k5_div"></div>
 
                          <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center">No.</td>

											<td style="width:25%" align="center">Kab/Kota</td>
											<td style="width:15%" align="center">Jumlah  KK/RT</td>
											<td style="width:15%" align="center">Jumlah  RTLH Versi BDT (unit)</td>
											<td style="width:15%" align="center">Jumlah RTLH  Verifikasi Pemda (unit)</td>
                                            <td align="center">Sumber Data</td>
                                            <td align="center">Fungsi</td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
											<td align="center" ><font size="1">(4)</font></td>
											<td align="center"><font size="1">(5)</font></td>
											<td align="center"><font size="1">(6)</font></td>
											<td align="center"><font size="1">(7)</font></td>
										</tr>
                                        <?php $i=0; foreach($form_1_k5 as $f1k5): ?>
										<tr>
<td align="center" ><?php $i++; echo $i;?>.</td>
<td align="left"><?php echo $f1k5->kabupaten_kota;?></td>
<td align="center" >  <?php echo number_format( $f1k5->jumlah_kk_rt_3 , 0 , ',' , '.' ) ;?></td>
<td align="center"> <?php echo number_format( $f1k5->jumlah_rtlh_versi_bdt_4 , 0 , ',' , '.' ) ;?></td>
<td align="center" > <?php echo number_format( $f1k5->jumlah_rtlh_verifikasi_pemda_5 , 0 , ',' , '.' ) ;?></td>
<td align="center">
<?php echo $f1k5->sumber_data_6;?></td>
<td align="center">  
                                        </td>
										</tr>

                                        <?php endforeach;?>
										<tr>
                                        <?php $i=0; foreach($form_1_k5_sum as $f1k5sum): ?>
											<td align="right" colspan="2">Total</td>
											<td align="center"> <?php echo number_format( $f1k5sum->total_1 , 0 , ',' , '.' ) ;?></td>
											<td align="center"> <?php echo number_format( $f1k5sum->total_2 , 0 , ',' , '.' ) ;?></td>
											<td align="center" > <?php echo number_format( $f1k5sum->total_3 , 0 , ',' , '.' ) ;?></td>
                                        <?php endforeach;?>
										</tr>

							  		</tbody>
								</table>

							  </td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">Penjelasan :
<p>Data RTLH merupakan data yang sama dengan Sistem Informasi Rumah Swadaya, yang bersumber dari data olahan BDT 2015</p></span>
								</td>
							</tr>
                            <tr>
							  <td colspan="3"class = "note note-info">
                              <span class="text-muted">K.6. KAWASAN KUMUH </span></br></br><div id="k6_div"></div>
 
                              <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center">No.</td>

											<td style="width:20%" align="center">Kab/Kota</td>
											<td style="width:20%" align="center">Kecamatan</td>
											<td style="width:15%" align="center">Luas wilayah kumuh (Ha)</td>
											<td style="width:15%" align="center">Jumlah RTLH dalam wilayah kumuh (unit)</td>
                                            <td align="center">Sumber Data</td>
                                            <td align="center">Fungsi</td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
											<td align="center" ><font size="1">(4)</font></td>
											<td align="center"><font size="1">(5)</font></td>
											<td align="center"><font size="1">(6)</font></td>
											<td align="center"><font size="1">(7)</font></td>
										</tr>
                                        <?php $i=0; foreach($form_1_k6 as $f1k6): ?>
										<tr>
<td align="center" ><?php $i++; echo $i;?>.</td>
<td align="left"><input name="idform_1_k6_<?php echo $i;?>" type="hidden" class="form-control" value="<?php echo $f1k6->idform_1_k6;?>"/><?php echo $f1k6->kabupaten_kota;?></td>
<td >
<?php
		$this->db->select('idkecamatan,kecamatan');
        $this->db->where('idform_1_k6',$f1k6->idform_1_k6);
        $this->db->from('form_1_k6_kecamatan_view');
        $query = $this->db->get();
		$x=0;
      foreach ($query->result() as $value) {
		  $x++;
		  if ($x<$query->num_rows()){
          echo $value->kecamatan.", ";
		  }
		  else
		  {
          echo $value->kecamatan;
		  }
      }
?></td>
<td align="center"><?php echo $f1k6->luas_wilayah_kumuh_4;?></td>
<td align="center" ><?php echo $f1k6->jumlah_rtlh_dalam_wilayah_kumuh_5;?></td>
<td align="center"><?php echo $f1k6->sumber_data_6;?></td>
<td align="center"> 
                                        </td>
										</tr>
                                        <?php endforeach;?>
										<tr>
                                        <?php $i=0; foreach($form_1_k6_sum as $f1k6sum): ?>
											<td align="right" colspan="3">Total</td>
											<td align="center"><?php echo number_format( $f1k6sum->total_luas_wilayah_kumuh , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo number_format( $f1k6sum->total_jumlah_rtlh_dalam_wilayah_kumuh , 0 , ',' , '.' ) ;?></td>
											<td align="center" ></td>
                                        <?php endforeach;?>
										</tr>

							  		</tbody>
								</table>

							  </td>
							</tr>
							<tr>
								<td colspan="3"class = "note note-info">
									 <span class="text-muted">Penjelasan :
<p>Jumlah RTLH dalam kawasan kumuh dapat diperoleh dari Satker Kotaku, data ini untuk integrasi penanganan kumuh, Ditjen Penyediaan Perumahan dengan program BSPS atau Rusunawa, Ditjen Cipta Karya dengan penanganan infrastruktur di dalam satu kawasan. Uraian per Kabupaten/Kota</p></span>
								</td>
							</tr>
							</tbody>
							</table>
                            <p>Demikian hasil pelaksanaan pendataan perumahan dan kawasan permukiman, dilakukan untuk dipergunakan sebagaimana mestinya.</p>
                            <p><?php echo $kabupaten_kota;?>, ___________________<?php echo $tahun;?></p>
                                    <p>Ttd.</p></br>
<p><?php echo isset($r->kepala_dinas);?></p>
<p>Kepala Dinas PKP Kab/Kota</p> 

		</div>
	</div>
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
</div>
</div>
</div>
</div>


<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<?php include "footer.php";?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/form-validation.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.7.2.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.form.js'); ?>"></script>
<script src="<?php echo BASE_URL("/application/views/html/css/"); ?>/jquery.min.js"></script>
<script>
        $(document).ready(function(){
            $("#kabupaten_kota_select").change(function (){
                var url = "<?php echo site_url('main/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan_combo').load(url);
                return false;
            })
			// Handler for .ready() called.
			$('html, body').animate({
				scrollTop: $('#<?php echo $_GET["dv"];?>').offset().top
			}, 'slow');
		});
</script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.7.2.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.form.js'); ?>"></script>
<script type="text/javascript">
        jQuery(document).ready(function() {
			jQuery('#form-upload').on('submit', function(e) {
				e.preventDefault();
				jQuery('#submit-button').attr('disabled', '');
				jQuery("#output").html('<div style="padding:10px"><img src="<?php echo base_url('assets/images/loading.gif'); ?>" alt="Please Wait"/> <span>Mengunggah...</span></div>');
				jQuery(this).ajaxSubmit({
					target: '#output',
					success:  sukses
				});
			});
        });

		function sukses()  {
			jQuery('#form-upload').resetForm();
			jQuery('#submit-button').removeAttr('disabled');

		}
    </script>
<script type="text/javascript">
function tambah_k3_modal()
{
	  //document.getElementById('idform_1_k3')="";
//	  document.getElementById('isi_1a').checked=false;
//	  document.getElementById('isi_1b').checked=false;
//	  document.getElementById('isi_1c').checked=false;
//	  document.getElementById('isi_1d').checked=false;
//	  document.getElementById('isi_1e').checked=false;
//	  document.getElementById('isi_1f').checked=false;
//	  document.getElementById('isi_1f_keterangan').value="";
//	  document.getElementById('isi_2a').checked=false;
//	  document.getElementById('isi_2b').checked=false;
//	  document.getElementById('isi_2c').checked=false;
//	  document.getElementById('isi_2d').checked=false;
//	  document.getElementById('isi_2e').checked=false;
//	  document.getElementById('isi_2c_keterangan').value="";
//	  document.getElementById('isi_2d_keterangan').value="";
        document.getElementById('k3_form').action="<?php echo base_url('main/Form1k3_insert2');?>";
}
function ubah_k3_modal(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o)
{
	  document.getElementById('idform_1_k3').value=o;
	  if (a==1) {
	  document.getElementById('isi_1a').click();
	  }
	  if (b==1) {
	  document.getElementById('isi_1b').click();
	  }
	  if (c==1) {
	  document.getElementById('isi_1c').click();
	  }
	  if (d==1) {
	  document.getElementById('isi_1d').click();
	  }
	  if (e==1) {
	  document.getElementById('isi_1e').click();
	  }
	  if (f==1) {
	  document.getElementById('isi_1f').click();
	  }
	  document.getElementById('isi_1f_keterangan').value=g;
	  if (h==1) {
	  document.getElementById('isi_2a').click();
	  }
	  if (i==1) {
	  document.getElementById('isi_2b').click();
	  }
	  if (j==1) {
	  document.getElementById('isi_2c').click();
	  }
	  if (k==1) {
	  document.getElementById('isi_2d').click();
	  }
	  if (l==1) {
	  document.getElementById('isi_2e').click();
	  }
	  document.getElementById('isi_2c_keterangan').value=m;
	  document.getElementById('isi_2d_keterangan').value=n;
        document.getElementById('k3_form').action="<?php echo base_url('main/Form1k3_update2');?>";
}
</script>
<script type="text/javascript">

function k5_form_submit()
{
	  document.getElementById('k5_form').submit();
}
function tambah_k5_modal()
{
	  document.getElementById('idkabupaten_kota').value="";
	  document.getElementById('jumlah_kk_rt_3').value="";
	  document.getElementById('jumlah_rtlh_versi_bdt_4').value="";
	  document.getElementById('jumlah_rtlh_verifikasi_pemda_5').value="";
	  document.getElementById('sumber_data_6').value="";
	  document.getElementById('idform_1_k5').value="";
        document.getElementById('k5_form').action="<?php echo base_url('main/Form1k5_insert2');?>";
}
function ubah_k5_modal(a,b,c,d,e,f)
{
	  document.getElementById('idkabupaten_kota').value=a;
	  document.getElementById('jumlah_kk_rt_3').value=b;
	  document.getElementById('jumlah_rtlh_versi_bdt_4').value=c;
	  document.getElementById('jumlah_rtlh_verifikasi_pemda_5').value=d;
	  document.getElementById('sumber_data_6').value=e;
	  document.getElementById('idform_1_k5').value=f;
        document.getElementById('k5_form').action="<?php echo base_url('main/Form1k5_update2');?>";
}
function confirmDelete7(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k52');?>/"+delUrl;
  }
}
//document.getElementById('idkabupaten_kota').value=a;
//document.getElementById('idkecamatan').value=b;
function tambah_k6_modal()
{
	  document.getElementById('luas_wilayah_kumuh_4').value="";
	  document.getElementById('jumlah_rtlh_dalam_wilayah_kumuh_5').value="";
	  document.getElementById('s6').value="";
	  document.getElementById('idform_1_k6').value="";
        document.getElementById('k6_form').action="<?php echo base_url('main/Form1k6_insert2');?>";
}
function ubah_k6_modal(a,b,c,d)
{
	  document.getElementById('luas_wilayah_kumuh_4').value=a;
	  document.getElementById('jumlah_rtlh_dalam_wilayah_kumuh_5').value=b;
	  document.getElementById('s6').value=c;
	  document.getElementById('idform_1_k6').value=d;
        document.getElementById('k6_form').action="<?php echo base_url('main/Form1k6_update2');?>";
}
function confirmDelete8(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k62');?>/"+delUrl;
  }
}

function k2a_form_submit()
{
	  document.getElementById('k2a_form').submit();
}
function tambah_k2_modal()
{
	  document.getElementById('uraian1').value="";
	  document.getElementById('k221').value="";
	  document.getElementById('k231').value="";
	  document.getElementById('idform_1_k2_a').value="";
        document.getElementById('k2a_form').action="<?php echo base_url('main/Form1k2_a_insert2');?>";
}
function ubah_k2_modal(a,b,c,d)
{
	  document.getElementById('uraian1').value=a;
	  document.getElementById('k221').value=b;
	  document.getElementById('k231').value=c;
	  document.getElementById('idform_1_k2_a').value=d;
        document.getElementById('k2a_form').action="<?php echo base_url('main/Form1k2_a_update2');?>";
}
function confirmDelete(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k2_a2');?>/"+delUrl;
  }
}
//
function tambah_k4_4_modal()
{
	  document.getElementById('idkabupaten_kota').value="";
	  document.getElementById('jumlah_rumah_3').value="";
	  document.getElementById('jumlah_rumah_4').value="";
	  document.getElementById('sumber_data_5').value="";
	  document.getElementById('idform_1_k4_4').value="";
        document.getElementById('k44_form').action="<?php echo base_url('main/Form1k4_4_insert2');?>";
}
function ubah_k4_4_modal(a,b,c,d,e)
{
	  document.getElementById('idkabupaten_kota').value=a;
	  document.getElementById('jumlah_rumah_3').value=b;
	  document.getElementById('jumlah_rumah_4').value=c;
	  document.getElementById('sumber_data_5').value=d;
	  document.getElementById('idform_1_k4_4').value=e;
        document.getElementById('k44_form').action="<?php echo base_url('main/Form1k4_4_update2');?>";
}
function confirmDelete(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k2_a2');?>/"+delUrl;
  }
}
//
function tambah_k4_5_modal()
{
	  document.getElementById('non_mbr_2').value="";
	  document.getElementById('mbr_3').value="";
	  document.getElementById('non_mbr_4').value="";
	  document.getElementById('mbr_5').value="";
	  document.getElementById('non_mbr_6').value="";
	  document.getElementById('mbr_7').value="";
	  document.getElementById('sumber_data_8').value="";
	  document.getElementById('idform_1_k4_5').value="";
        document.getElementById('k45_form').action="<?php echo base_url('main/Form1k4_5_insert2');?>";
}
function ubah_k4_5_modal(a,b,c,d,e,f,g,h)
{
	  document.getElementById('non_mbr_2').value=a;
	  document.getElementById('mbr_3').value=b;
	  document.getElementById('non_mbr_4').value=c;
	  document.getElementById('mbr_5').value=d;
	  document.getElementById('non_mbr_6').value=e;
	  document.getElementById('mbr_7').value=f;
	  document.getElementById('sumber_data_8').value=g;
	  document.getElementById('idform_1_k4_5').value=h;
        document.getElementById('k45_form').action="<?php echo base_url('main/Form1k4_5_update2');?>";
}
function confirmDelete6(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k4_52');?>/"+delUrl;
  }
}
//
function tambah_k2_b_modal()
{
	  document.getElementById('jenis_kegiatan_urusan_pkp_2').value="";
	  document.getElementById('ta_a_vol_unit_3').value="";
	  document.getElementById('ta_a_biaya_4').value="";
	  document.getElementById('ta_a_vol_unit_5').value="";
	  document.getElementById('ta_a_biaya_6').value="";
	  document.getElementById('idform_1_k2_b').value="";
        document.getElementById('k2b_form').action="<?php echo base_url('main/Form1k2_b_insert2');?>";
}
function ubah_k2_b_modal(a,b,c,d,e,f)
{
	  document.getElementById('jenis_kegiatan_urusan_pkp_2').value=a;
	  document.getElementById('ta_a_vol_unit_3').value=b;
	  document.getElementById('ta_a_biaya_4').value=c;
	  document.getElementById('ta_a_vol_unit_5').value=d;
	  document.getElementById('ta_a_biaya_6').value=e;
	  document.getElementById('idform_1_k2_b').value=f;
        document.getElementById('k2b_form').action="<?php echo base_url('main/Form1k2_b_update2');?>";
}
function confirmDelete2(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k2_b2');?>/"+delUrl;
  }
}
//
function tambah_k4_2_modal()
{
	  document.getElementById('k4231').value="";
	  document.getElementById('k4232').value="";
	  document.getElementById('k4241').value="";
	  document.getElementById('k4242').value="";
	  document.getElementById('jenis1').value="";
	  document.getElementById('jenis2').value="";
	  document.getElementById('idform_1_k4_2').value="";
        document.getElementById('k4_2_form').action="<?php echo base_url('main/Form1k4_2_insert2');?>";
}
function ubah_k4_2_modal(a,b,c,d,e,f,g)
{
	  document.getElementById('k4231').value=a;
	  document.getElementById('k4232').value=b;
	  document.getElementById('k4241').value=c;
	  document.getElementById('k4242').value=d;
	  document.getElementById('jenis1').value=e;
	  document.getElementById('jenis2').value=f;
	  document.getElementById('idform_1_k4_2').value=g;
        document.getElementById('k4_2_form').action="<?php echo base_url('main/Form1k4_2_update2');?>";
}
//
function tambah_k4_3_modal()
{
	  document.getElementById('k4331').value="";
	  document.getElementById('k4332').value="";
	  document.getElementById('k4341').value="";
	  document.getElementById('k4342').value="";
	  document.getElementById('fungsi1').value="";
	  document.getElementById('fungsi2').value="";
	  document.getElementById('idform_1_k4_3').value="";
        document.getElementById('k4_3_form').action="<?php echo base_url('main/Form1k4_3_insert2');?>";
}
function ubah_k4_3_modal(a,b,c,d,e,f,g)
{
	  document.getElementById('k4331').value=a;
	  document.getElementById('k4332').value=b;
	  document.getElementById('k4341').value=c;
	  document.getElementById('k4342').value=d;
	  document.getElementById('fungsi1').value=e;
	  document.getElementById('fungsi2').value=f;
	  document.getElementById('idform_1_k4_3').value=g;
        document.getElementById('k4_3_form').action="<?php echo base_url('main/Form1k4_3_update2');?>";
}
//
function tambah_k4_1_modal()
{
	  document.getElementById('idform_1_k4_1').value="";
	  document.getElementById('k4141').value="";
	  document.getElementById('k4142').value="";
	  document.getElementById('k4143').value="";
	  document.getElementById('k4144').value="";
	  document.getElementById('k4145').value="";
	  document.getElementById('k4151').value="";
	  document.getElementById('k4152').value="";
	  document.getElementById('k4153').value="";
	  document.getElementById('k4154').value="";
	  document.getElementById('k4155').value="";
	  document.getElementById('status1').value="";
	  document.getElementById('status2').value="";
	  document.getElementById('status3').value="";
	  document.getElementById('status4').value="";
	  document.getElementById('status5').value="";
        document.getElementById('k4_1_form').action="<?php echo base_url('main/Form1k4_1_insert2');?>";
}

function ubah_k4_1_modal(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p)
{
	  document.getElementById('idform_1_k4_1').value=p;

	  document.getElementById('k4141').value=a;
	  document.getElementById('k4142').value=b;
	  document.getElementById('k4143').value=c;
	  document.getElementById('k4144').value=d;
	  document.getElementById('k4145').value=e;
	  document.getElementById('k4151').value=f;
	  document.getElementById('k4152').value=g;
	  document.getElementById('k4153').value=h;
	  document.getElementById('k4154').value=i;
	  document.getElementById('k4155').value=j;
	  document.getElementById('status1').value=k;
	  document.getElementById('status2').value=l;
	  document.getElementById('status3').value=m;
	  document.getElementById('status4').value=n;
	  document.getElementById('status5').value=o;
        document.getElementById('k4_1_form').action="<?php echo base_url('main/Form1k4_1_update2');?>";
}

function confirmDelete4(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k4_12');?>/"+delUrl;
  }
}
function confirmDelete5(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k4_42');?>/"+delUrl;
  }
}
</script>
<script>
jQuery(document).ready(function() {
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
   FormValidation.init();
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>
<!-- END BODY -->
</html>
