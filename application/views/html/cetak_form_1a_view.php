<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=Form1A.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
 
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head> 
<body> 
 
							<table class="table table-striped table-bordered table-hover" id="sample_1">
  							<thead>
                  <tr>
                    <th width="9%" class="table-checkbox"> No. </th>
                    <th width="20%"> Tahun </th>
                    <th width="25%"> Tanggal </th>
                    <th width="18%"> Status </th> 
                  </tr>
  							</thead>
  							<tbody>
                <?php $i=0; foreach($form_1_view as $r): $i++;?>
	  							<tr class="odd gradeX">
	  								<td><?php echo $i;?></td>
	  								<td><?php echo $r->tahun;?></td>
	  								<td class="center"><?php echo $r->tanggal_buat;?></td>
	  								<td>
	                    <?php if($r->status=="Draft"){?>
	  									<span class="label label-md label-warning"> Draft </span>
	                    <?php } elseif ($r->status=="Disetujui"){?>
	  									<span class="label label-md label-success">	Disetujui </span>
	                    <?php }?>
	  								</td> 
										<td align="center">
		                  <table border="0"> 
											</table>
										</td>
	  							</tr>
                <?php endforeach; ?>
  							</tbody>
							</table>
					 
</body>
<!-- END BODY -->
</html>
