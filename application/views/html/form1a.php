<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css">
    <link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css">
    <link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css"
          href="../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
    <link rel="stylesheet" type="text/css"
          href="../../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
    <link rel="stylesheet" type="text/css"
          href="../../../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link rel="stylesheet" type="text/css"
          href="../../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
    <link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css"
          id="style_color">
    <link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
    <style>
        p.indent {
            padding-left: 1em
        }

        p.indent2 {
            padding-left: 9.7em
        }

        p.indent3 {
            padding-left: 3em
        }

        #kecamatan_combo {
            width: 300px;
        }

        #tahun_form {
            width: 100px;
        }
    </style>
</head>

<body>
<div class="page-container">
    <!-- BEGIN PAGE HEAD -->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Formulir IA
                    <small class="page-title-tag"><?php echo $provinsi; ?></small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>

    <div class="page-content">
        <div class="container">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="#">Home</a><i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="form_1a_view">Rekapitulasi Formulir IA</a><i class="fa fa-angle-right"></i>
                </li>
                <li class="active">
                    Formulir IA (#)
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="row margin-top-10">
                <div class="col-md-12">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- BEGIN FORM-->
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12 form-body">
                                        <!-- BEGIN FORM ALERT -->
                                        <div class="alert alert-danger display-hide">
                                            <button class="close" data-close="alert"></button>
                                            Terdapat Input yang Salah. Silakan Periksa Kembali.
                                        </div>
                                        <div class="alert alert-success display-hide">
                                            <button class="close" data-close="alert"></button>
                                            Form Anda Telah Tersimpan!
                                        </div>
                                        <!-- END FORM ALERT -->
                                        <table id="user" class="table table-bordered table-striped">
                                            <tbody>
                                            <tr>
                                                <td style="width:15%">
                                                    Provinsi
                                                </td>
                                                <td style="width:50%">
                                                    sdsds
                                                    <input name="idkabupaten_kota" type="hidden"
                                                           class="form-control" value="2323"/>
                                                    <input name="provinsi" type="hidden" class="form-control"
                                                           value="23232"/>
                                                </td>
                                                <td style="width:35%; vertical-align:middle; text-align:center"
                                                    rowspan="3">
                                                    <h3> FORMULIR IA </h3>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    Nama Dinas
                                                    <input name="iddinas" type="hidden" class="form-control"
                                                           value="223>"/>
                                                    <input name="privilage" type="hidden" class="form-control"
                                                           value="2323"/>
                                                    <input name="idform_1" type="hidden" class="form-control"
                                                           value="2323"/>
                                                </td>
                                                <td>
                                                    sfsdfdsf
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Alamat
                                                </td>
                                                <td>
                                                    sdfdsfs
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="form-1">
                                            <tr>
                                                <td colspan="3" align="center">
                                                    <h3 class="uppercase"><strong>DAFTAR ISIAN PENDATAAN
                                                            PKP </strong>
                                                    </h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="note note-info">
                                                    Kegiatan pendataan perumahan ini dilakukan untuk memenuhi
                                                    kebutuhan data dan
                                                    informasi perumahan. Data ini merupakan bagian dari Pembangunan
                                                    Basis Data PKP
                                                    Kabupaten/Kota sesuai UU No. 1 Tahun 2011 tentang Perumahan dan
                                                    Kawasan Permukiman (PKP)
                                                    pasal 17 dan Peraturan Pemerintah No. 88 Tahun 2014 tentang
                                                    Pembinaan Penyelenggaraan
                                                    PKP pasal 18 ayat 1.<br/><br/>
                                                    Tabel ini diisi oleh SNVT bidang Perumahan Kabupaten/Kota, untuk
                                                    kelengkapan datanya
                                                    berkoordinasi dengan Pokja PKP Kabupaten/Kota, BPS
                                                    Kabupaten/Kota, Bappeda Kabupaten/Kota, Dinas
                                                    Sosial Kabupaten/Kota, BKKBN Kabupaten/Kota, serta rekapitulasi
                                                    data dari Form 1A oleh Provinsi.
                                                    Untuk pertanyaan lebih lanjut dapat menghubungi Subdit Data dan
                                                    Informasi, Direktorat
                                                    Perencanaan Penyediaan Perumahan: <a
                                                            href="mailto:datinperumahan@gmail.com">datinperumahan@gmail.com</a>
                                                    atau Telp./Fax. 021-7211883.<br/><br/>
                                                    Daftar isian ini merupakan data sekunder hasil dari berbagai
                                                    kegiatan pendataan
                                                    perumahan, terdiri dari:
                                                    <ul style="list-style-type: none; padding: 0 0 0 10px">
                                                        <li>K.1 Kelembagaan Perumahan dan Permukiman</li>
                                                        <li>K.2 Alokasi APBD untuk Urusan Perumahan dan Kawasan
                                                            Permukiman (PKP)
                                                        </li>
                                                        <li>K.3 Pembangunan Perumahan</li>
                                                        <li>K.4 Backlog Perumahan</li>
                                                        <li>K.5 Rumah Tidak Layak Huni</li>
                                                        <li>K.6 Kawasan Kumuh</li>
                                                    </ul>
                                                    <p>
                                                        <a href="#mulai" class="btn btn-sm blue-chambray"> MULAI
                                                            ISI </a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>

                                        <div id="mulai"></div>

                                        <div id="k1_div"></div>

                                        <div class="portlet light">
                                            <div class="portlet-body">
                                                <ul class="nav nav-tabs nav-justified">
                                                    <li class="active">
                                                        <a href="#tab_k_1" data-toggle="tab">
                                                            <i class="fa fa-bookmark-o"></i> K.1
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_k_2" data-toggle="tab"> <i
                                                                    class="fa fa-bookmark-o"></i> K.2 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_k_3" data-toggle="tab"> <i
                                                                    class="fa fa-bookmark-o"></i> K.3 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_k_4" data-toggle="tab"> <i
                                                                    class="fa fa-bookmark-o"></i> K.4 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_k_5" data-toggle="tab"> <i
                                                                    class="fa fa-bookmark-o"></i> K.5 </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_k_6" data-toggle="tab"> <i
                                                                    class="fa fa-bookmark-o"></i> K.6 </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content form-1" style="padding: 0 10px 0 10px">
                                                    <div class="tab-pane fade active in" id="tab_k_1">

                                                        <h4 style="font-weight: bold">K.1 KELEMBAGAAN PERUMAHAN DAN PERMUKIMAN</h4>

                                                        <p>Struktur organisasi dinas yang menangani perumahan dan
                                                            permukiman di dsfdsfds</p>

                                                        <div style="text-align: center">

                                                            <img src="../../../assets/uploads/sample.jpg" height="400" width="700"/>

                                                            <form action="#"
                                                                  class="form-horizontal"
                                                                  enctype="multipart/form-data" method="post">

                                                                <table width="300">
                                                                    <tr>
                                                                        <td>
                                                                            <input type="hidden" name="idform_1" value="2" class="form-control"/>
                                                                            <input type="hidden" name="jenis" value="1A" class="form-control"/>
                                                                            <input type="hidden" name="iddinas" value="2" class="form-control"/>
                                                                            <input type="file" name="image" class="form-control" id="gambar"/>
                                                                        </td>
                                                                        <td>
                                                                            <input type="submit" class="btn left-zeroradius blue" value="Unggah..">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </form>
                                                        </div>

                                                        <div class="note note-info">
                                                            <strong>Penjelasan:</strong>
                                                            <ul>
                                                                <li>Format gambar yang diperbolehkan adalah JPG, JPEG
                                                                    dan PNG.
                                                                </li>
                                                                <li>Ukuran resolusi gambar yang dianjurkan adalah
                                                                    700x400 pixel
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane fade" id="tab_k_2">

                                                        <div id="k2_a_div"></div>

                                                        <h4 style="font-weight: bold">K.2 ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN
                                                                KAWASAN PERMUKIMAN (PKP)</h4>
                                                        <p>1. Rasio Anggaran PKP Terhadap APBD</p>

                                                        <table id="user"
                                                               class="table table-advance table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width:40%" align="center">Uraian</th>
                                                                    <th style="width:50%" align="center">Angka (Rp)</th>
                                                                    <th style="width:10%" align="center">Fungsi</th>
                                                                </tr>
                                                                <tr>
                                                                    <th align="center"><font size="1">(1)</font></th>
                                                                    <th align="center"><font size="1">(2)</font></th>
                                                                    <th align="center"><font size="1">(3)</font></th>
                                                                </tr>
                                                                </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center">dfdsfsd</td>
                                                                    <td align="right">343.43</td>
                                                                    <td align="center">
                                                                        <a class="btn default btn-sm blue-hoki"
                                                                           data-toggle="modal" href="#k2_modal"
                                                                           onClick="ubah_k2_modal('3','2','1')">
                                                                            Ubah </a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>



                                                        <div id='pulsate-regular' class='note note-danger'><strong>PERHATIAN!</strong> <br> Alokasi anggaran Dinas PKP lebih besar dari APBD. <br> Silakan perbaiki input Anda.</div>


                                                        <div class="note note-info">

                                                            <strong>Penjelasan:</strong>
                                                            <p>Anggaran total APBD jhbj terhadap
                                                                anggaran SKPD/Dinas PKP njkhj
                                                                dalam tahun
                                                                yang sama, sehingga didapat persentase perbandingan
                                                                anggaran urusan PKP, yaitu:</p>
                                                        </div>

                                                        <hr/>

                                                        <div id="k2_b_div"></div>

                                                        <p>2. Uraian Program PKP</p>

                                                        <p>
                                                            <a class="btn btn-sm green" data-toggle="modal"
                                                              href="#k2b_modal"
                                                              onClick="tambah_k2_b_modal()">Tambah</a>
                                                        </p>

                                                        <table id="user"
                                                               class="table table-advance table-bordered table-hover table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width:5%" align="center">No.</th>
                                                                    <th style="width:35%" align="center">Jenis Kegiatan
                                                                        Urusan PKP
                                                                    </th>
                                                                    <th style="width:15%">Volume/Unit</th>
                                                                    <th style="width:30%">Biaya (Rp)</th>
                                                                    <th style="width:15%" align="center">Fungsi</th>
                                                                </tr>
                                                                <tr>
                                                                    <th align="center"><font size="1">(1)</font></th>
                                                                    <th align="center"><font size="1">(2)</font></th>
                                                                    <th align="center"><font size="1">(3)</font></th>
                                                                    <th align="center"><font size="1">(4)</font></th>
                                                                    <th align="center"><font size="1">(5)</font></th>
                                                                </tr>
                                                            </thead>

                                                            <tbody>

                                                                <tr>
                                                                    <td align="center"><?php $i++;
                                                                        echo $i; ?></td>
                                                                    <td align="left"><?php echo $f1k2b3->jenis_kegiatan_urusan_pkp_2; ?></td>
                                                                    <td align="center"><?php echo $f1k2b3->ta_a_vol_unit_5; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k2b3->ta_a_biaya_6, 2, ',', '.'); ?> </td>
                                                                    <td align="center">
                                                                        <a class="btn btn-sm blue-hoki"
                                                                           data-toggle="modal" href="#k2b_modal"
                                                                           onClick="ubah_k2_b_modal('<?php echo $f1k2b3->jenis_kegiatan_urusan_pkp_2; ?>','<?php echo $f1k2b3->ta_a_vol_unit_5; ?>','<?php echo $f1k2b3->ta_a_biaya_6; ?>','<?php echo $f1k2b3->idform_1_k2_b; ?>')">Ubah</a>
                                                                        <a class="btn btn-sm red-sunglo"
                                                                           href="javascript:confirmDelete2(<?php $f1k2b3->idform_1_k2_b; ?>)">Hapus</a>
                                                                    </td>
                                                                </tr>
                                                            <tr>
                                                                <?php $i = 0;
                                                                foreach ($form_1_k2_b_sum as $f1k2b3sum): ?>
                                                                    <td align="center">&nbsp;</td>
                                                                    <td align="right"><strong>Total</strong></td>
                                                                    <td align="center">
                                                                        <strong><?php echo number_format($f1k2b3sum->total_unit_rumah_2, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="right">
                                                                        <strong><?php echo number_format($f1k2b3sum->total_biaya_2, 2, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="center">&nbsp;</td>
                                                                <?php endforeach; ?>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <? if ($f1k2b3sum->total_biaya_2 > $pkp->k231) {
                                                            echo "<div id='pulsate-regular' class='note note-danger'><strong>PERHATIAN!</strong> <br> Jumlah total biaya lebih besar dari alokasi anggaran Dinas PKP. <br> Silakan periksa kembali input Anda.</div>";
                                                        } else {
                                                            echo "";
                                                        }
                                                        ?>
                                                        <div class="note note-info">
                                                            <strong>Penjelasan:</strong>
                                                            <p>Kegiatan fisik, biasanya rehabilitasi rumah berupa
                                                                peningkatan kualitas, yang bersumber dari
                                                                APBD Kabupaten/Kota/Anggaran Dinas PKP
                                                                Kabupaten/Kota, volumenya berupa unit, dihitung
                                                                sebagai data
                                                                pembangunan unit baru lingkup Program Satu Juta
                                                                Rumah</p>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_k_3">
                                                        <div id="k3_div"></div>
                                                        <h4><strong>K.3 PEMBANGUNAN PERUMAHAN</h4></strong>
                                                        <p><?php $i = 0;
                                                            foreach ($form_1_k3_count as $f1k3c): if ($f1k3c->c == 0){ ?>
                                                            <a class="btn btn-sm green" data-toggle="modal"
                                                               href="#k3_modal"
                                                               onClick="tambah_k3_modal()">Tambah</a></p>
                                                        <?php } endforeach; ?>
                                                        <?php $i = 0;
                                                        foreach ($form_1_k3 as $f1k3): ?>
                                                            <p>1. Apakah Pemerintah Provinsi mempunyai Dokumen
                                                                Perencanaan Urusan Perumahan?</p>
                                                            <p class="indent"><label>
                                                                    <input type="checkbox"
                                                                           name="isi_1a" <?php if ($f1k3->isi_1a == 'Y') {
                                                                        echo "checked";
                                                                    } ?> value="Y" disabled/> a. RTRW (sudah perda)
                                                                </label></p>
                                                            <p class="indent"><label>
                                                                    <input type="checkbox"
                                                                           name="isi_1b" <?php if ($f1k3->isi_1b == 'Y') {
                                                                        echo "checked";
                                                                    } ?> value="Y" disabled/> b. RDTR (sudah perda)
                                                                </label></p>
                                                            <p class="indent"><label>
                                                                    <input type="checkbox"
                                                                           name="isi_1c" <?php if ($f1k3->isi_1c == 'Y') {
                                                                        echo "checked";
                                                                    } ?> value="Y" disabled/> c. RP3KP/RP4D </label>
                                                            </p>
                                                            <p class="indent"><label>
                                                                    <input type="checkbox"
                                                                           name="isi_1d" <?php if ($f1k3->isi_1d == 'Y') {
                                                                        echo "checked";
                                                                    } ?> value="Y" disabled/> d. RPIJM </label></p>
                                                            <p class="indent"><label>
                                                                    <input type="checkbox"
                                                                           name="isi_1e" <?php if ($f1k3->isi_1e == 'Y') {
                                                                        echo "checked";
                                                                    } ?> value="Y" disabled/> e. Renstra Dinas PKP
                                                                </label></p>
                                                            <p class="indent"><label>
                                                                    <input type="checkbox"
                                                                           name="isi_1f" <?php if ($f1k3->isi_1f == 'Y') {
                                                                        echo "checked";
                                                                    } ?> value="Y" disabled/>
                                                                    f. <?php echo $f1k3->isi_1f_keterangan; ?>
                                                                </label></p>
                                                            <p class="indent"></p></br>
                                                            <p>2. Dalam Pelaksanaan Urusan Perumahan, Pemerintah
                                                                Provinsi Memanfaatkan Sumber Dana yang Berasal
                                                                dari:</p>
                                                            <div class="checkbox-list"
                                                                 data-error-container="#form_2_services_error">
                                                                <p class="indent">
                                                                    <label><input type="checkbox"
                                                                                  name="isi_2a" <?php if ($f1k3->isi_2a == 'Y') {
                                                                            echo "checked";
                                                                        } ?> value="Y" disabled/> a. APBD </label>
                                                                </p>
                                                                <p class="indent">
                                                                    <label><input type="checkbox"
                                                                                  name="isi_2b" <?php if ($f1k3->isi_2b == 'Y') {
                                                                            echo "checked";
                                                                        } ?> value="Y" disabled/> b. Loan (pinjaman)
                                                                        dari badan/bank luar negeri </label></p>
                                                                <p class="indent">
                                                                    <input type="checkbox"
                                                                           name="isi_2c" <?php if ($f1k3->isi_2c == 'Y') {
                                                                        echo "checked";
                                                                    } ?> value="Y" disabled/> c. CSR:
                                                                    <label><?php echo $f1k3->isi_2c_keterangan; ?></label>
                                                                </p>
                                                                <p class="indent">
                                                                    <input type="checkbox"
                                                                           name="isi_2d" <?php if ($f1k3->isi_2d == 'Y') {
                                                                        echo "checked";
                                                                    } ?> value="Y" disabled/> d. Swasta:
                                                                    <label><?php echo $f1k3->isi_2d_keterangan; ?></label>
                                                                </p>
                                                                <p class="indent"><label>
                                                                        <input type="checkbox"
                                                                               name="isi_2e" <?php if ($f1k3->isi_2e == 'Y') {
                                                                            echo "checked";
                                                                        } ?> value="Y" disabled/> e. Lainnya:
                                                                    </label></p>
                                                                <a class="btn btn-sm blue-hoki" data-toggle="modal"
                                                                   href="#k3_modal"
                                                                   onClick="ubah_k3_modal('<?php if ($f1k3->isi_1a == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php if ($f1k3->isi_1b == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php if ($f1k3->isi_1c == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php if ($f1k3->isi_1d == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php if ($f1k3->isi_1e == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php if ($f1k3->isi_1f == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php $f1k3->isi_1f_keterangan; ?>','<?php if ($f1k3->isi_2a == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php if ($f1k3->isi_2b == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php if ($f1k3->isi_2c == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php if ($f1k3->isi_2d == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php if ($f1k3->isi_2e == "Y") {
                                                                       echo 1;
                                                                   } else {
                                                                       echo 0;
                                                                   } ?>','<?php echo $f1k3->isi_2c_keterangan; ?>','<?php echo $f1k3->isi_2d_keterangan; ?>','<?php echo $f1k3->idform_1_k3; ?>','<?php echo $f1k3->isi_2e_keterangan; ?>')">
                                                                    Ubah </a>
                                                            </div>
                                                        <?php endforeach; ?>
                                                        <div class="note note-info">
                                                            <strong>Penjelasan:</strong>
                                                            <p>Kelengkapan dokumen kelengkapan/pendukung
                                                                program/kegiatan Perumahan dan Kawasan Permukiman
                                                                yang telah ada dasar leglisasinya seperti Perda atau
                                                                Pergub, dan juga sumber dana yang pernah digunakan
                                                                untuk pembangunan/rehabilitasi rumah di Kab/Kota</p>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_k_4">
                                                        <div id="k4_1_div"></div>
                                                        <h4><strong>K.4 BACKLOG PERUMAHAN</h4></strong>
                                                        <p>1. Jumlah Rumah Berdasarkan Status Kepemilikan Tempat
                                                            Tinggal </p>
                                                        <?php $i = 0;
                                                        foreach ($form_1_k4_1_count as $f1k41c): if ($f1k41c->f1k41count == 0) { ?>
                                                            <p><a class="btn btn-sm green" data-toggle="modal"
                                                                  href="#k4_1_modal" onClick="tambah_k4_1_modal()">
                                                                    Tambah </a></p><?php } endforeach; ?>
                                                        <?php $i = 0;
                                                        foreach ($form_1_k4_1 as $f1k41): ?>
                                                            <p><a class="btn btn-sm blue-hoki" data-toggle="modal"
                                                                  href="#k4_1_modal"
                                                                  onClick="ubah_k4_1_modal('<?php echo $f1k41->k4141; ?>','<?php echo $f1k41->k4142; ?>','<?php echo $f1k41->k4143; ?>','<?php echo $f1k41->k4144; ?>','<?php echo $f1k41->k4145; ?>','<?php echo $f1k41->k4151; ?>','<?php echo $f1k41->k4152; ?>','<?php echo $f1k41->k4153; ?>','<?php echo $f1k41->k4154; ?>','<?php echo $f1k41->k4155; ?>','<?php echo $f1k41->status1; ?>','<?php echo $f1k41->status2; ?>','<?php echo $f1k41->status3; ?>','<?php echo $f1k41->status4; ?>','<?php echo $f1k41->status5; ?>','<?php echo $f1k41->idform_1_k4_1; ?>')">
                                                                    Ubah </a></p>
                                                            <table id="user"
                                                                   class="table table-advance table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th style="width:5%" align="center">No.</th>
                                                                    <th style="width:45%" align="center">Status
                                                                        Kepemilikan Tempat Tinggal
                                                                    </th>
                                                                    <th style="width:25%" align="center">Jumlah
                                                                        (unit)
                                                                    </th>
                                                                    <th style="width:25%" align="center">Sumber
                                                                        Data
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th align="center"><font size="1">(1)</font>
                                                                    </th>
                                                                    <th align="center"><font size="1">(2)</font>
                                                                    </th>
                                                                    <th align="center"><font size="1">(3)</font>
                                                                    </th>
                                                                    <th align="center"><font size="1">(4)</font>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center">1.</td>
                                                                    <td align="left"><?php echo $f1k41->status1; ?></font></td>
                                                                    <td align="right"><?php echo number_format($f1k41->k4141, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k41->k4151; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">2.</td>
                                                                    <td align="left"><?php echo $f1k41->status2; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k41->k4142, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k41->k4152; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">3.</td>
                                                                    <td align="left"><?php echo $f1k41->status3; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k41->k4143, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k41->k4153; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">4.</td>
                                                                    <td align="left"><?php echo $f1k41->status4; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k41->k4144, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k41->k4154; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">5.</td>
                                                                    <td align="left"><?php echo $f1k41->status5; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k41->k4145, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k41->k4155; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" align="right"><strong>Total A
                                                                            (2+3+4+5)</strong></td>
                                                                    <td align="right">
                                                                        <strong><?php echo number_format($f1k41->total_a, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="center">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" align="right"><strong>Total B
                                                                            (3)</strong></td>
                                                                    <td align="right">
                                                                        <strong><?php echo number_format($f1k41->total_b, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="center">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" align="right"><strong>TOTAL
                                                                            SELURUHNYA</strong></td>
                                                                    <td align="right">
                                                                        <strong><?php echo number_format($f1k41->k4141 + $f1k41->k4142 + $f1k41->k4143 + $f1k41->k4144 + $f1k41->k4145, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="center">&nbsp;</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <p>Backlog kepemilikan = (Total A)
                                                                = <?php echo number_format($f1k41->total_a, 0, ',', '.'); ?>
                                                                unit</p>
                                                            <p>Backlog penghunian = (Total B)
                                                                = <?php echo number_format($f1k41->total_b, 0, ',', '.'); ?>
                                                                unit</p>
                                                        <?php endforeach; ?>
                                                        <div class="note note-info">
                                                            <strong>Penjelasan:</strong>
                                                            <p>Data backlog kepemilikan dan backlog penghunian
                                                                kabupaten ditentukan oleh Dinas PKP,
                                                                data dari BPS dan BKKBN hanya sebagai benchmark data
                                                                yang perlu divalidasi oleh Dinas PKP.</p>
                                                            <p>Catatan: data BKKBN bersumber dari sensus Pendataan
                                                                Keluarga 2015 oleh BKKBN, yang telah
                                                                menandatangani Kesepakatan Bersama dengan
                                                                Kementerian PUPR Nomor:1/KSM/G2/2017
                                                                Nomor:01/PKS/M/2017 tentang Peningkatan Program
                                                                Kependudukan, Keluarga Berencana dan
                                                                Pembangunan Keluarga dalam Pembangunan Infrastruktur
                                                                Pekerjaan Umum dan Perumahan Rakyat,
                                                                tanggal 11 Januari 2017</p>
                                                        </div>
                                                        <hr/>
                                                        <div id="k4_2_div"></div>
                                                        <p>2. Jenis Fisik Bangunan Rumah</p></span>
                                                        <?php $i = 0;
                                                        foreach ($form_1_k4_2_count as $f1k42c): if ($f1k42c->f1k42count == 0) { ?>
                                                            <p><a class="btn btn-sm green" data-toggle="modal"
                                                                  href="#k4_2_modal" onClick="tambah_k4_2_modal()">Tambah</a>
                                                            </p>
                                                        <?php } endforeach; ?>
                                                        <?php $i = 0;
                                                        foreach ($form_1_k4_2 as $f1k42): ?>
                                                            <p><a class="btn btn-sm blue-hoki" data-toggle="modal"
                                                                  href="#k4_2_modal"
                                                                  onClick="ubah_k4_2_modal('<?php echo $f1k42->k4231; ?>','<?php echo $f1k42->k4232; ?>','<?php echo $f1k42->k4241; ?>','<?php echo $f1k42->k4242; ?>','<?php echo $f1k42->jenis1; ?>','<?php echo $f1k42->jenis2; ?>','<?php echo $f1k42->idform_1_k4_2; ?>')">Ubah</a>
                                                            </p>
                                                            <table id="user"
                                                                   class="table table-advance table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th style="width:5%" align="center">No.</th>
                                                                    <th style="width:45%" align="center">Jenis Fisik
                                                                        Bangunan Rumah
                                                                    </th>
                                                                    <th style="width:25%" align="center">Jumlah
                                                                        (unit)
                                                                    </th>
                                                                    <th style="width:25%" align="center">Sumber
                                                                        Data
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th align="center"><font size="1">(1)</font>
                                                                    </th>
                                                                    <th align="center"><font size="1">(2)</font>
                                                                    </th>
                                                                    <th align="center"><font size="1">(3)</font>
                                                                    </th>
                                                                    <th align="center"><font size="1">(4)</font>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center">1.</td>
                                                                    <td align="left"><?php echo $f1k42->jenis1; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k42->k4231, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k42->k4241; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">2.</td>
                                                                    <td align="left"><?php echo $f1k42->jenis2; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k42->k4232, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k42->k4242; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" colspan="2">
                                                                        <strong>Total</strong></td>
                                                                    <td align="right">
                                                                        <strong><?php echo number_format($f1k42->total, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="center">&nbsp;</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        <?php endforeach; ?>
                                                        <div class="note note-info">
                                                            <strong>Penjelasan:</strong>
                                                            <p>Jumlah fisik bangunan rumah, bukan data Rumah Tangga
                                                                atau data Kepala Keluarga, tapi merupakan unit
                                                                bangunan rumah baik terhuni maupun
                                                                tidak. </p></span>
                                                        </div>
                                                        <hr/>
                                                        <div id="k4_3_div"></div>
                                                        <p>3. Jumlah Kepala Keluarga (KK) dalam Satu Rumah</p>
                                                        <?php $i = 0;
                                                        foreach ($form_1_k4_3_count as $f1k43c): if ($f1k43c->f1k43count == 0) { ?>
                                                            <p><a class="btn btn-sm green" data-toggle="modal"
                                                                  href="#k4_3_modal" onClick="tambah_k4_3_modal()">Tambah</a>
                                                            </p>
                                                        <?php } endforeach; ?>
                                                        <?php $i = 0;
                                                        foreach ($form_1_k4_3 as $f1k43): ?>
                                                            <p><a class="btn btn-sm blue-hoki" data-toggle="modal"
                                                                  href="#k4_3_modal"
                                                                  onClick="ubah_k4_3_modal('<?php echo $f1k43->k4331; ?>','<?php echo $f1k43->k4332; ?>','<?php echo $f1k43->k4341; ?>','<?php echo $f1k43->k4342; ?>','<?php echo $f1k43->fungsi1; ?>','<?php echo $f1k43->fungsi2; ?>','<?php echo $f1k43->idform_1_k4_3; ?>')">Ubah</a>
                                                            </p>
                                                            <table id="user"
                                                                   class="table table-advance table-bordered table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th style="width:5%" align="center">No.</th>
                                                                    <th style="width:45%" align="center">Fungsi
                                                                        Rumah
                                                                    </th>
                                                                    <th style="width:25%" align="center">Jumlah
                                                                        (unit)
                                                                    </th>
                                                                    <th style="width:25%" align="center">Sumber
                                                                        Data
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th align="center"><font size="1">(1)</font>
                                                                    </th>
                                                                    <th align="center"><font size="1">(2)</font>
                                                                    </th>
                                                                    <th align="center"><font size="1">(3)</font>
                                                                    </th>
                                                                    <th align="center"><font size="1">(4)</font>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center">1.</td>
                                                                    <td align="left"><?php echo $f1k43->fungsi1; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k43->k4331, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k43->k4341; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">2.</td>
                                                                    <td align="left"><?php echo $f1k43->fungsi2; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k43->k4332, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k43->k4342; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" colspan="2">
                                                                        <strong>Total</strong></td>
                                                                    <td align="right">
                                                                        <strong><?php echo number_format($f1k43->k4331 + $f1k43->k4332, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="center">&nbsp;</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        <?php endforeach; ?>
                                                        <div class="note note-info">
                                                            <strong>Penjelasan:</strong>
                                                            <p>Jumlah KK lebih dari 1 (satu) dalam satu rumah
                                                                merupakan bagian dari data backlog
                                                                penghunian.</p>
                                                        </div>
                                                        <hr/>
                                                        <div id="k4_4_div"></div>
                                                        <p>4. Jumlah Sambungan Listrik Rumah (PLN)</p>
                                                        <p><a class="btn btn-sm green" data-toggle="modal"
                                                              href="#k4_4_modal" onclick="tambah_k4_4_modal()">Tambah
                                                                Data</a></p>
                                                        <table id="user"
                                                               class="table table-advance table-bordered table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th style="width:5%" align="center">No.</th>
                                                                <th style="width:30%" align="center">
                                                                    Kabupaten/Kota
                                                                </th>
                                                                <th style="width:25%" align="center">Jumlah rumah
                                                                    (unit)
                                                                </th>
                                                                <th style="width:25%" align="center">Sumber Data
                                                                </th>
                                                                <th style="width:30%" align="center">Fungsi</th>
                                                            </tr>
                                                            <tr>
                                                                <th align="center"><font size="1">(1)</font></th>
                                                                <th align="center"><font size="1">(2)</font></th>
                                                                <th align="center"><font size="1">(3)</font></th>
                                                                <th align="center"><font size="1">(4)</font></th>
                                                                <th align="center"><font size="1">(5)</font></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $i = 0;
                                                            foreach ($form_1_k4_4 as $f1k44): ?>
                                                                <tr>
                                                                    <td align="center"><?php $i++;
                                                                        echo $i; ?>.
                                                                    </td>
                                                                    <td align="left"><?php echo $f1k44->kabupaten_kota; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k44->jumlah_rumah_4, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k44->sumber_data_5; ?></td>
                                                                    <td align="center">
                                                                        <a class="btn btn-sm blue-hoki"
                                                                           data-toggle="modal" href="#k4_4_modal"
                                                                           onClick="ubah_k4_4_modal('<?php echo $f1k44->idkabupaten_kota; ?>','<?php echo $f1k44->jumlah_rumah_4; ?>','<?php echo $f1k44->sumber_data_5; ?>','<?php echo $f1k44->idform_1_k4_4; ?>','<?php echo $f1k44->kabupaten_kota; ?>')">Ubah</a>
                                                                        <a href="javascript:confirmDelete5(<?php echo $f1k44->idform_1_k4_4; ?>)"
                                                                           class="btn btn-sm red-sunglo">Hapus</a>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            <tr>
                                                                <?php $i = 0;
                                                                foreach ($form_1_k4_4_sum as $f1k44sum): ?>
                                                                    <td colspan="2" align="right">
                                                                        <strong>Total</strong></td>
                                                                    <td align="right">
                                                                        <strong><?php echo number_format($f1k44sum->total_2, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td colspan="2">&nbsp;</td>
                                                                <?php endforeach; ?>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="note note-info">
                                                            <strong>Penjelasan:</strong>
                                                            <p>Data sambungan listrik berasal dari PLN,
                                                                per-kabupaten/kota.</p>
                                                        </div>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <div id="k4_5_div"></div>
                                                                <p>5. Jumlah Pembangunan Rumah dalam 1 Tahun</p>
                                                                <p>
                                                                    <?php $i = 0;
                                                                    foreach ($form_1_k4_5 as $f1k45): $i++;
                                                                        if ($i <= 0) { ?>
                                                                        <?php } endforeach; ?>
                                                                    <?php if ($i == 0) { ?>
                                                                        <a class="btn btn-sm green"
                                                                           data-toggle="modal" href="#k4_5_modal"
                                                                           onClick="tambah_k4_5_modal()">Tambah
                                                                            Data</a>
                                                                    <?php } ?>
                                                                </p>
                                                                <table id="user"
                                                                       class="table table-advance table-bordered table-striped table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="width:30%" align="center"
                                                                            colspan="2">Jumlah Pembangunan Rumah<br>Berdasarkan
                                                                            IMB (unit)
                                                                        </th>
                                                                        <th style="width:30%" align="center"
                                                                            colspan="2">Jumlah Pembangunan Rumah<br>Berdasarkan
                                                                            Non-IMB (unit)
                                                                        </th>
                                                                        <th style="width:25%" align="center"
                                                                            rowspan="2">Sumber Data
                                                                        </th>
                                                                        <th style="width:10%" align="center"
                                                                            rowspan="2">Fungsi
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th align="center">Non MBR</th>
                                                                        <th align="center">MBR</th>
                                                                        <th align="center">Non MBR</th>
                                                                        <th align="center">MBR</th>
                                                                        </strong>
                                                                    </tr>
                                                                    <tr>
                                                                        <th align="center"><font size="1">(1)</font>
                                                                        </th>
                                                                        <th align="center"><font size="1">(2)</font>
                                                                        </th>
                                                                        <th align="center"><font size="1">(3)</font>
                                                                        </th>
                                                                        <th align="center"><font size="1">(4)</font>
                                                                        </th>
                                                                        <th align="center"><font size="1">(5)</font>
                                                                        </th>
                                                                        <th align="center"><font size="1">(6)</font>
                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php $i = 0;
                                                                    foreach ($form_1_k4_5 as $f1k45): ?>
                                                                        <tr>
                                                                            <td align="right"><?php echo number_format($f1k45->non_mbr_2, 0, ',', '.'); ?></td>
                                                                            <td align="right"><?php echo number_format($f1k45->mbr_3, 0, ',', '.'); ?></td>
                                                                            <td align="right"><?php echo number_format($f1k45->non_mbr_4, 0, ',', '.'); ?></td>
                                                                            <td align="right"><?php echo number_format($f1k45->mbr_5, 0, ',', '.'); ?></td>
                                                                            <td align="center"><?php echo $f1k45->sumber_data_8; ?></td>
                                                                            <td align="center">
                                                                                <a class="btn btn-sm blue-hoki"
                                                                                   data-toggle="modal"
                                                                                   href="#k4_5_modal"
                                                                                   onClick="ubah_k4_5_modal('<?php echo $f1k45->non_mbr_2; ?>','<?php echo $f1k45->mbr_3; ?>','<?php echo $f1k45->non_mbr_4; ?>','<?php echo $f1k45->mbr_5; ?>','<?php echo $f1k45->sumber_data_8; ?>','<?php echo $f1k45->idform_1_k4_5; ?>')">Ubah</a>
                                                                            </td>
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                    </tbody>
                                                                </table>
                                                                <div class="note note-info">
                                                                    <strong>Penjelasan:</strong>
                                                                    <p>Data pembangunan unit rumah per tahun oleh
                                                                        seluruh stakeholder perumahan
                                                                        terdata dalam IMB melalui Dinas PTSP, dan
                                                                        jumlah yang tidak terdata dalam
                                                                        IMB dianalisis/diasumsikan oleh Dinas PKP,
                                                                        sehingga didapat perkiraan
                                                                        pembangunan unit rumah satu kabupaten dalam
                                                                        satu tahun. Ini disebut data
                                                                        supply perumahan. Data total dari seluruh
                                                                        Kabupaten/Kota</p>
                                                                </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_k_5">
                                                        <div id="k5_div"></div>
                                                        <h4><strong>K.5 RUMAH TIDAK LAYAK HUNI</strong></h4>
                                                        <p><a class="btn btn-sm green" data-toggle="modal"
                                                              href="#k5_modal" onClick="tambah_k5_modal()">Tambah
                                                                Data</a></p>
                                                        <table id="user"
                                                               class="table table-advance table-bordered table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th style="width:5%" align="center">No.</th>
                                                                <th style="width:20%" align="center">
                                                                    Kabupaten/Kota
                                                                </th>
                                                                <th style="width:10%" align="center">Jumlah KK/RT
                                                                </th>
                                                                <th style="width:10%" align="center">Jumlah RTLH
                                                                    Versi BDT (unit)
                                                                </th>
                                                                <th style="width:10%" align="center">Jumlah RTLH
                                                                    Verifikasi Pemda (unit)
                                                                </th>
                                                                <th style="width:20%" align="center">Sumber Data
                                                                </th>
                                                                <th style="width:15%" align="center">Fungsi</th>
                                                            </tr>
                                                            <tr>
                                                                <th align="center"><font size="1">(1)</font></th>
                                                                <th align="center"><font size="1">(2)</font></th>
                                                                <th align="center"><font size="1">(3)</font></th>
                                                                <th align="center"><font size="1">(4)</font></th>
                                                                <th align="center"><font size="1">(5)</font></th>
                                                                <th align="center"><font size="1">(6)</font></th>
                                                                <th align="center"><font size="1">(7)</font></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $i = 0;
                                                            foreach ($form_1_k5 as $f1k5): ?>
                                                                <tr>
                                                                    <td align="center"><?php $i++;
                                                                        echo $i; ?>.
                                                                    </td>
                                                                    <td align="left"><?php echo $f1k5->kabupaten_kota; ?></td>
                                                                    <td align="right"><?php echo number_format($f1k5->jumlah_kk_rt_3, 0, ',', '.'); ?></td>
                                                                    <td align="right"><?php echo number_format($f1k5->jumlah_rtlh_versi_bdt_4, 0, ',', '.'); ?></td>
                                                                    <td align="right"><?php echo number_format($f1k5->jumlah_rtlh_verifikasi_pemda_5, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k5->sumber_data_6; ?></td>
                                                                    <td align="center">
                                                                        <a class="btn btn-sm blue-hoki"
                                                                           data-toggle="modal" href="#k5_modal"
                                                                           onClick="ubah_k5_modal('<?php echo $f1k5->idkabupaten_kota; ?>','<?php echo $f1k5->jumlah_kk_rt_3; ?>','<?php echo $f1k5->jumlah_rtlh_versi_bdt_4; ?>','<?php echo $f1k5->jumlah_rtlh_verifikasi_pemda_5; ?>','<?php echo $f1k5->sumber_data_6; ?>','<?php echo $f1k5->idform_1_k5; ?>','<?php echo $f1k5->kabupaten_kota; ?>')">Ubah</a>
                                                                        <a class="btn btn-sm red-sunglo"
                                                                           href="javascript:confirmDelete7(<?php echo $f1k5->idform_1_k5; ?>)">
                                                                            Hapus </a>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            <tr>
                                                                <?php $i = 0;
                                                                foreach ($form_1_k5_sum as $f1k5sum): ?>
                                                                    <td align="right" colspan="2">
                                                                        <strong>Total</strong></td>
                                                                    <td align="right">
                                                                        <strong> <?php echo number_format($f1k5sum->total_1, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="right">
                                                                        <strong> <?php echo number_format($f1k5sum->total_2, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="right">
                                                                        <strong> <?php echo number_format($f1k5sum->total_3, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td colspan="2">&nbsp;</td>
                                                                <?php endforeach; ?>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="note note-info">
                                                            <strong>Penjelasan:</strong>
                                                            <p>Data RTLH merupakan data yang sama dengan Sistem
                                                                Informasi Rumah Swadaya,
                                                                yang bersumber dari data olahan BDT 2015</p>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_k_6">
                                                        <div id="k6_div"></div>
                                                        <h4><strong>K.6 KAWASAN KUMUH</strong></h4>
                                                        <p><a class="btn btn-sm green" data-toggle="modal"
                                                              href="#k6_modal" onClick="tambah_k6_modal()">Tambah
                                                                Data</a></p>
                                                        <table id="user"
                                                               class="table table-advance table-bordered table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th style="width:5%" align="center">No.</th>
                                                                <th style="width:25%" align="center">
                                                                    Kabupaten/Kota
                                                                </th>
                                                                <!-- <th style="width:20%" align="center">Kecamatan</th> -->
                                                                <th style="width:15%" align="center">Luas Wilayah
                                                                    Kumuh (Ha)
                                                                </th>
                                                                <th style="width:17%" align="center">Jumlah RTLH
                                                                    dalam Wilayah Kumuh (unit)
                                                                </th>
                                                                <th style="width:25%" align="center">Sumber Data
                                                                </th>
                                                                <th style="width:13%" align="center">Fungsi</th>
                                                            </tr>
                                                            <tr>
                                                                <th align="center"><font size="1">(1)</font></th>
                                                                <th align="center"><font size="1">(2)</font></th>
                                                                <th align="center"><font size="1">(3)</font></th>
                                                                <th align="center"><font size="1">(4)</font></th>
                                                                <th align="center"><font size="1">(5)</font></th>
                                                                <th align="center"><font size="1">(6)</font></th>
                                                                <!-- <th align="center"><font size="1">(7)</font></th> -->
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $i = 0;
                                                            foreach ($form_1_k6 as $f1k6): ?>
                                                                <tr>
                                                                    <td align="center"><?php $i++;
                                                                        echo $i; ?>.
                                                                    </td>
                                                                    <td align="left"><input
                                                                                name="idform_1_k6_<?php echo $i; ?>"
                                                                                type="hidden" class="form-control"
                                                                                value="<?php echo $f1k6->idform_1_k6; ?>"/><?php echo $f1k6->kabupaten_kota; ?>
                                                                    </td>
                                                                    <!-- <td >
                                  <?php
                                                                    $this->db->select('idkecamatan,kecamatan');
                                                                    $this->db->where('idform_1_k6', $f1k6->idform_1_k6);
                                                                    $this->db->from('form_1_k6_kecamatan_view');
                                                                    $query = $this->db->get();
                                                                    $x = 0;
                                                                    foreach ($query->result() as $value) {
                                                                        $x++;
                                                                        if ($x < $query->num_rows()) {
                                                                            echo $value->kecamatan . ", ";
                                                                        } else {
                                                                            echo $value->kecamatan;
                                                                        }
                                                                    }
                                                                    ?>
                                  </td> -->
                                                                    <td align="right"><?php echo number_format($f1k6->luas_wilayah_kumuh_4, 2, ',', '.'); ?></td>
                                                                    <td align="right"><?php echo number_format($f1k6->jumlah_rtlh_dalam_wilayah_kumuh_5, 0, ',', '.'); ?></td>
                                                                    <td align="center"><?php echo $f1k6->sumber_data_6; ?></td>
                                                                    <td align="center">
                                                                        <a class="btn btn-sm blue-hoki"
                                                                           data-toggle="modal" href="#k6_modal"
                                                                           onClick="ubah_k6_modal('<?php echo $f1k6->idkabupaten_kota; ?>','<?php echo $f1k6->luas_wilayah_kumuh_4; ?>','<?php echo $f1k6->jumlah_rtlh_dalam_wilayah_kumuh_5; ?>','<?php echo $f1k6->sumber_data_6; ?>','<?php echo $f1k6->idform_1_k6; ?>','<?php echo $f1k6->kabupaten_kota; ?>')">Ubah</a>
                                                                        <a class="btn btn-sm red-sunglo"
                                                                           href="javascript:confirmDelete8(<?php echo $f1k6->idform_1_k6; ?>)">
                                                                            Hapus </a>
                                                                    </td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            <tr>
                                                                <?php $i = 0;
                                                                foreach ($form_1_k6_sum as $f1k6sum): ?>
                                                                    <td align="right" colspan="2">
                                                                        <strong>Total</strong></td>
                                                                    <td align="right">
                                                                        <strong><?php echo number_format($f1k6sum->total_luas_wilayah_kumuh, 2, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="right">
                                                                        <strong><?php echo number_format($f1k6sum->total_jumlah_rtlh_dalam_wilayah_kumuh, 0, ',', '.'); ?></strong>
                                                                    </td>
                                                                    <td align="center" colspan="2">&nbsp;</td>
                                                                <?php endforeach; ?>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="note note-info">
                                                            <strong>Penjelasan:</strong>
                                                            <p>Jumlah RTLH dalam kawasan kumuh dapat diperoleh dari
                                                                Satker Kotaku, data ini untuk integrasi penanganan
                                                                kumuh, Ditjen Penyediaan Perumahan dengan program
                                                                BSPS atau Rusunawa, Ditjen Cipta Karya dengan
                                                                penanganan infrastruktur di dalam satu kawasan.
                                                                Uraian per Kabupaten/Kota</p></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="well-lg" style="background: #eee; border: 1px solid #ddd">
                                                <p>Demikian hasil pelaksanaan pendataan perumahan dan kawasan
                                                    permukiman, dilakukan untuk dipergunakan sebagaimana
                                                    mestinya.</p>
                                                <p><?php echo $provinsi; ?>, <?php echo date('d-m-Y'); ?></p>
                                                <p>Tanda Bukti Penyetujuan,</p></br>
                                                <p><?php echo isset($r->kepala_dinas); ?></p>
                                                <p>Kepala <?php echo $nama_dinas; ?></p>
                                                <?php if ($pengesahan == "") { ?>
                                                    <?php foreach ($kelengkapan as $kl): ?>
                                                        <form action="<?php echo site_url('main/upload3'); ?>"
                                                              class="form-horizontal" enctype="multipart/form-data"
                                                              id="form-upload2" method="post"/>
                                                        <table width="300">
                                                            <tr>
                                                                <td>
                                                                    <input type="hidden" name="idform"
                                                                           value="<?php echo $idform_1; ?>"
                                                                           class="form-control"/>
                                                                    <input type="file" name="image2"
                                                                           class="form-control" id="gambar2"/>
                                                                </td>
                                                                <td>
                                                                    <input type="submit"
                                                                           class="btn left-zeroradius red"
                                                                           value="Selesai" id="submit-button2">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        </form>
                                                        <p></p>
                                                        <div class="note note-info">
                                                            Bukti pengesahan adalah lembar terakhir dari Form 1 yang
                                                            telah dicetak dan ditandatangani
                                                            oleh kepala dinas. Hasil cetak tersebut kemudian
                                                            dipindai (<i>scan</i>) dan disimpan dalam
                                                            format gambar (JPG, JPEG atau PNG) lalu diunggah pada
                                                            bagian ini.
                                                        </div>
                                                        <div class="note note-danger">
                                                            <strong>PERHATIAN!</strong>
                                                            <br>Proses pengunggahan bukti pengesahan hanya dapat
                                                            dilakukan sekali dan tak dapat diubah.
                                                            <br>Pastikan file yang akan Anda unggah sudah benar!
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php } else { ?>
                                                    <a href="<?php echo '../' . $pengesahan; ?>" target="_blank">Bukti
                                                        Pengesahan</a>
                                                <? } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PAGE CONTENT -->
                            </div>
                        </div>
                    </div>
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <div class="modal fade" id="basic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Unggah Gambar Struktur Organisasi</h4>
                                </div>
                                <div class="modal-body">
                                    <!-- KOSONG -->
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn blue">Simpan Perubahan</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <div id="k2_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k2a_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title">K.2. ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN
                                        PERMUKIMAN (PKP)</h5>
                                </div>
                                <div class="modal-body">
                                    <table id="user" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td style="width:40%" align="center">Uraian</td>
                                            <td style="width:60%" align="center">Angka (Rp)</td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <input name="idform_1" type="hidden" class="form-control"
                                                       value="<?php echo $idform_1; ?>"/>
                                                <input name="iddinas" type="hidden" class="form-control"
                                                       value="<?php echo $iddinas; ?>"/>
                                                <input name="jenisk2" type="hidden" class="form-control"
                                                       value="1A"/>
                                                <input name="idform_1_k2_a" type="hidden" class="form-control"
                                                       id="idform_1_k2_a"/>
                                                <input name="uraian1" type="text" class="form-control"
                                                       style="background-color:#E8F3FF" id="uraian1"/>
                                                <p id="val_uraian1"></p>
                                            </td>
                                            <td align="center">
                                                <input name="k231" type="text" class="form-control"
                                                       style="background-color:#E8F3FF" id="k231"/>
                                                <p id="val_k231"></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn green" onclick="val_k2()">Simpan</button>
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>

                    <div id="k2b_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k2b_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title">URAIAN PROGRAM PKP</h5>
                                </div>
                                <div class="modal-body">
                                    <table id="user" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td>Jenis Kegiatan Urusan PKP</td>
                                            <td align="center">
                                                <input name="idform_1" type="hidden" class="form-control"
                                                       value="<?php echo $idform_1; ?>"/>
                                                <input name="iddinas" type="hidden" class="form-control"
                                                       value="<?php echo $iddinas; ?>"/>
                                                <input name="jenisk2b" type="hidden" class="form-control"
                                                       value="1A"/>
                                                <input name="idform_1_k2_b" id="idform_1_k2_b" type="hidden"
                                                       class="form-control"/>
                                                <input name="jenis_kegiatan_urusan_pkp_2"
                                                       id="jenis_kegiatan_urusan_pkp_2" type="text"
                                                       class="form-control" style="background-color:#E8F3FF"/>
                                                <p id="val_jenis_kegiatan_urusan_pkp_2"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Volume/Unit</td>
                                            <td align="center">
                                                <input name="ta_a_vol_unit_5" id="ta_a_vol_unit_5" type="text"
                                                       class="form-control" style="background-color:#E8F3FF"/>
                                                <p id="val_ta_a_vol_unit_5"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Biaya</td>
                                            <td align="center">
                                                <input name="ta_a_biaya_6" id="ta_a_biaya_6" type="text"
                                                       class="form-control" style="background-color:#E8F3FF"/>
                                                <p id="val_ta_a_biaya_6"></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn green" onclick="val_k2b()">Simpan</button>
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div id="k3_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k3_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title">K.3. PEMBANGUNAN PERUMAHAN</h5>
                                </div>
                                <div class="modal-body">
                                    <input name="idform_1" type="hidden" class="form-control"
                                           value="<?php echo $idform_1; ?>"/>
                                    <input name="iddinas" type="hidden" class="form-control"
                                           value="<?php echo $iddinas; ?>"/>
                                    <input name="jenisk3" type="hidden" class="form-control" value="1A"/>
                                    <input name="idform_1_k3" id="idform_1_k3" type="hidden" class="form-control"/>
                                    <p>1. Apakah Pemerintah Kabupaten/Kota mempunyai Dokumen Perencanaan Urusan
                                        Perumahan? </p>
                                    <p class="indent"><label>
                                            <input type="checkbox" name="isi_1a" id="isi_1a" value="Y"/> a. RTRW
                                            (sudah perda) </label></p>
                                    <p class="indent"><label>
                                            <input type="checkbox" name="isi_1b" id="isi_1b" value="Y"/> b. RDTR
                                            (sudah perda) </label></p>
                                    <p class="indent"><label>
                                            <input type="checkbox" name="isi_1c" id="isi_1c" value="Y"/> c.
                                            RP3KP/RP4D </label></p>
                                    <p class="indent"><label>
                                            <input type="checkbox" name="isi_1d" id="isi_1d" value="Y"/> d. RPIJM
                                        </label></p>
                                    <p class="indent"><label>
                                            <input type="checkbox" name="isi_1e" id="isi_1e" value="Y"/> e. Renstra
                                            Dinas PKP </label></p>
                                    <p class="indent">
                                        <input type="checkbox" name="isi_1f" id="isi_1f" value="Y"
                                               onclick="dis_isi_1f_keterangan()"/> f. Lainnya:
                                        <label><input name="isi_1f_keterangan" id="isi_1f_keterangan"
                                                      placeholder="uraikan" type="text" class="form-control"
                                                      style="background-color:#E8F3FF" disabled/> </label></p></br>
                                    <p>2. Dalam pelaksanaan Urusan Perumahan, Pemerintah Kab/Kota memanfaatkan
                                        sumber dana yang berasal dari:</p>
                                    <p class="indent">
                                        <label><input type="checkbox" name="isi_2a" id="isi_2a" value="Y"/> a. APBD
                                        </label></p>
                                    <p class="indent">
                                        <label><input type="checkbox" name="isi_2b" id="isi_2b" value="Y"/> b. Loan
                                            (pinjaman) dari badan/bank luar negeri </label></p>
                                    <p class="indent">
                                        <input type="checkbox" name="isi_2c" id="isi_2c" value="Y"
                                               onclick="dis_csr()"/> c. CSR: <label><input name="isi_2c_keterangan"
                                                                                           id="isi_2c_keterangan"
                                                                                           type="text"
                                                                                           placeholder="uraikan"
                                                                                           class="form-control"
                                                                                           style="background-color:#E8F3FF"
                                                                                           disabled/></label></p>
                                    <p class="indent">
                                        <input type="checkbox" name="isi_2d" id="isi_2d" value="Y"
                                               onclick="dis_swasta()"/> d. Swasta: <label><input
                                                    name="isi_2d_keterangan" id="isi_2d_keterangan" type="text"
                                                    placeholder="uraikan" class="form-control"
                                                    style="background-color:#E8F3FF" disabled/></label></p>
                                    <p class="indent">
                                        <input type="checkbox" name="isi_2e" id="isi_2e" value="Y"
                                               onclick="dis_lainnya()"/> e. Lainnya: <label><input
                                                    name="isi_2e_keterangan" id="isi_2e_keterangan" type="text"
                                                    placeholder="uraikan" class="form-control"
                                                    style="background-color:#E8F3FF" disabled/></label></p>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" value="Simpan" class="btn green">
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div id="k4_1_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k4_1_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title">1. Jumlah Rumah Berdasarkan Status Kepemilikan Tempat
                                        Tinggal</h5>
                                </div>
                                <div class="modal-body">
                                    <table id="user" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td style="width:5%" align="center">No.</td>
                                            <td colspan="2" align="center" style="width:45%">Status Kepemilikan
                                                Tempat Tinggal
                                            </td>
                                            <td style="width:25%" align="center">Jumlah (unit)</td>
                                            <td style="width:25%" align="center">Sumber Data</td>
                                        </tr>
                                        <tr>
                                            <td align="center">1.</td>
                                            <td colspan="2" align="center">
                                                <input name="idform_1" id="idform_1" type="hidden"
                                                       class="form-control" value="<?php echo $idform_1; ?>"/>
                                                <input name="iddinas" type="hidden" class="form-control"
                                                       value="<?php echo $iddinas; ?>"/>
                                                <input name="jenisk41" id="idform_1" type="hidden"
                                                       class="form-control" value="1A"/>
                                                <input name="idform_1_k4_1" id="idform_1_k4_1" type="hidden"
                                                       class="form-control"/>
                                                <input name="status1" id="status1" type="text" class="form-control"
                                                       style="background-color:#E8F3FF"/></font><p
                                                        id="val_status1"></p></td>
                                            <td align="center"><input name="k4141" id="k4141" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4141"></p></td>
                                            <td align="center"><input name="k4151" id="k4151" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4151"></p></td>
                                        </tr>
                                        <tr>
                                            <td align="center">2.</td>
                                            <td colspan="2" align="center"><input name="status2" id="status2"
                                                                                  type="text" class="form-control"
                                                                                  style="background-color:#E8F3FF"/>
                                                <p id="val_status2"></p></td>
                                            <td align="center"><input name="k4142" id="k4142" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4142"></p></td>
                                            <td align="center"><input name="k4152" id="k4152" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4152"></p></td>
                                        </tr>
                                        <tr>
                                            <td align="center">3.</td>
                                            <td colspan="2" align="center"><input name="status3" id="status3"
                                                                                  type="text" class="form-control"
                                                                                  style="background-color:#E8F3FF"/>
                                                <p id="val_status3"></p></td>
                                            <td align="center"><input name="k4143" id="k4143" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4143"></p></td>
                                            <td align="center"><input name="k4153" id="k4153" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4153"></p></td>
                                        </tr>
                                        <tr>
                                            <td align="center">4.</td>
                                            <td colspan="2" align="center"><input name="status4" id="status4"
                                                                                  type="text" class="form-control"
                                                                                  style="background-color:#E8F3FF"/>
                                                <p id="val_status4"></p></td>
                                            <td align="center"><input name="k4144" id="k4144" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4144"></p></td>
                                            <td align="center"><input name="k4154" id="k4154" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4154"></p></td>
                                        </tr>
                                        <tr>
                                            <td align="center">5.</td>
                                            <td colspan="2" align="center"><input name="status5" id="status5"
                                                                                  type="text" class="form-control"
                                                                                  style="background-color:#E8F3FF"/>
                                                <p id="val_status5"></p></td>
                                            <td align="center"><input name="k4145" id="k4145" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4145"></p></td>
                                            <td align="center"><input name="k4155" id="k4155" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4155"></p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn green" onclick="val_k4_1()">Simpan</button>
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div id="k4_2_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k4_2_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title">2. Jenis Fisik Bangunan Rumah</h5>
                                </div>
                                <div class="modal-body">
                                    <table id="user" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td style="width:5%" align="center">No.</td>
                                            <td style="width:45%" align="center">Jenis Fisik Bangunan Rumah</td>
                                            <td style="width:25%" align="center">Jumlah (unit)</td>
                                            <td style="width:25%" align="center">Sumber Data</td>
                                        </tr>
                                        <tr>
                                            <td align="center">1.</td>
                                            <td align="left">
                                                <input name="idform_1" type="hidden" class="form-control"
                                                       value="<?php echo $idform_1; ?>"/>
                                                <input name="iddinas" type="hidden" class="form-control"
                                                       value="<?php echo $iddinas; ?>"/>
                                                <input name="jenisk42" type="hidden" class="form-control"
                                                       value="1A"/>
                                                <input name="idform_1_k4_2" id="idform_1_k4_2" type="hidden"
                                                       class="form-control"/>
                                                <input name="jenis1" id="jenis1" type="text" class="form-control"
                                                       style="background-color:#E8F3FF"/>
                                                <p id="val_jenis1"></p></td>
                                            <td align="center"><input name="k4231" id="k4231" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4231"></p></td>
                                            <td align="center"><input name="k4241" id="k4241" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4241"></p></td>
                                        </tr>
                                        <tr>
                                            <td align="center">2.</td>
                                            <td align="left"><input name="jenis2" id="jenis2" type="text"
                                                                    class="form-control"
                                                                    style="background-color:#E8F3FF"/>
                                                <p id="val_jenis2"></p></td>
                                            <td align="center"><input name="k4232" id="k4232" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4232"></p></td>
                                            <td align="center"><input name="k4242" id="k4242" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4242"></p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn green" onclick="val_k4_2()">Simpan</button>
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div id="k4_3_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k4_3_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title">3. Jumlah Kepala Keluarga (KK) dalam satu rumah</h5>
                                </div>
                                <div class="modal-body">
                                    <table id="user" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td style="width:5%" align="center">No.</td>
                                            <td style="width:45%" align="center">Fungsi Rumah</td>
                                            <td style="width:25%" align="center">Jumlah (unit)</td>
                                            <td style="width:25%" align="center">Sumber Data</td>
                                        </tr>
                                        <tr>
                                            <td align="center">1.</td>
                                            <td align="left">
                                                <input name="idform_1" type="hidden" class="form-control"
                                                       value="<?php echo $idform_1; ?>"/>
                                                <input name="iddinas" type="hidden" class="form-control"
                                                       value="<?php echo $iddinas; ?>"/>
                                                <input name="jenisk43" type="hidden" class="form-control"
                                                       value="1A"/>
                                                <input name="idform_1_k4_3" id="idform_1_k4_3" type="hidden"
                                                       class="form-control"/>
                                                <input name="fungsi1" id="fungsi1" type="text" class="form-control"
                                                       value="Rumah Dengan 1 (satu) KK"
                                                       style="background-color:#E8F3FF"/>
                                                <p id="val_fungsi1"></p></td>
                                            <td align="center"><input name="k4331" id="k4331" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4331"></p></td>
                                            <td align="center"><input name="k4341" id="k4341" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4341"></p></td>
                                        </tr>
                                        <tr>
                                            <td align="center">2.</td>
                                            <td align="left"><input name="fungsi2" id="fungsi2" type="text"
                                                                    class="form-control"
                                                                    value="Rumah dengan lebih dari 1 (satu) KK"
                                                                    style="background-color:#E8F3FF"/>
                                                <p id="val_fungsi2"></p></td>
                                            <td align="center"><input name="k4332" id="k4332" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4332"></p></td>
                                            <td align="center"><input name="k4342" id="k4342" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_k4342"></p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn green" onclick="val_k4_3()">Simpan</button>
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div id="k4_4_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k44_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title uppercase"><strong>K.4.4 Jumlah Sambungan Listrik Rumah
                                            (PLN)</strong></h5>
                                </div>
                                <div class="modal-body">
                                    <table id="user" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td><strong>Kabupaten/Kota</strong>
                                                <input name="idform_1" type="hidden" class="form-control"
                                                       value="<?php echo $idform_1; ?>" style="background-color:#E8F3FF"/>
                                                <input name="jenisk44" type="hidden" class="form-control" value="1A"
                                                       style="background-color:#E8F3FF"/>
                                                <input name="iddinas" type="hidden" class="form-control"
                                                       value="<?php echo $iddinas; ?>"/>
                                                <input name="idform_1_k4_4" id="idform_1_k4_4" type="hidden"
                                                       class="form-control"/>
                                            </td>
                                            <td>
                                                <div id="ada_k44">
                                                    <input type"text" id="adat_k44" class="form-control" disabled>
                                                </div>

                                                <div id="hilang_k44">
                                                    <select name="idkabupaten_kota" id="idkabupaten_kota"
                                                            class="form-control">
                                                        <option value="">Pilih Kabupaten/Kota</option>
                                                        <?php foreach ($kabupaten_kota_combo as $r):
                                                            $this->db->select('*');
                                                            $this->db->where('idform_1', $idform_1);
                                                            $this->db->where('idkabupaten_kota', $r->idkabupaten_kota);
                                                            $this->db->from('form_1_k4_4');
                                                            $query = $this->db->get();
                                                            if ($query->num_rows() < 1) {
                                                                $mati = "";
                                                            } else {
                                                                $mati = "disabled";
                                                            }
                                                            ?>
                                                            <option value="<?php echo $r->idkabupaten_kota; ?>"
                                                                    style="background-color:#E8F3FF" <?php echo $mati; ?>>
                                                                <?php echo $r->kabupaten_kota; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Jumlah Rumah (unit)</strong></td>
                                            <td align="center"><input name="jumlah_rumah_4" id="jumlah_rumah_4"
                                                                      type="text" class="form-control"
                                                                      style="background-color:#E8F3FF"/></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Sumber Data</strong></td>
                                            <td align="center"><input name="sumber_data_5" id="sumber_data_5"
                                                                      type="text" class="form-control"
                                                                      style="background-color:#E8F3FF"/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn green" value="Simpan">
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div id="k4_5_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k45_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title">5. Jumlah Pembangunan Rumah dalam 1 Tahun</h5>
                                </div>
                                <div class="modal-body">
                                    <table id="user" class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td style="width:27%" align="center" colspan="2">Jumlah Pembangunan
                                                Rumah Berdasarkan<br>IMB (unit)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">Non-MBR</td>
                                            <td align="center">MBR</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><input name="idform_1" type="hidden"
                                                                      class="form-control" value="<?php echo $idform_1; ?>"
                                                                      style="background-color:#E8F3FF"/>
                                                <input name="idform_1_k4_5" id="idform_1_k4_5" type="hidden"
                                                       class="form-control" style="background-color:#E8F3FF"/>
                                                <input name="iddinas" type="hidden" class="form-control"
                                                       value="<?php echo $iddinas; ?>"/>
                                                <input name="jenisk45" id="idform_1" type="hidden"
                                                       class="form-control" value="1A"/>
                                                <input name="non_mbr_2" id="non_mbr_2" type="text"
                                                       class="form-control" style="background-color:#E8F3FF"/>
                                                <p id="val_non_mbr_2"></p></td>
                                            <td align="center"><input name="mbr_3" id="mbr_3" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_mbr_3"></p></td>
                                        </tr>
                                        <tr>
                                            <td style="width:27%" align="center" colspan="2">Jumlah Pembangunan
                                                Rumah Berdasarkan<br>Non-IMB (unit)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">Non-MBR</td>
                                            <td align="center">MBR</td>
                                        </tr>
                                        <tr>
                                            <td align="center"><input name="non_mbr_4" id="non_mbr_4" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_non_mbr_4"></td>
                                            <td align="center"><input name="mbr_5" id="mbr_5" type="text"
                                                                      class="form-control"
                                                                      style="background-color:#E8F3FF"/>
                                                <p id="val_mbr_5"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">Sumber Data</td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2"><input name="sumber_data_8"
                                                                                  id="sumber_data_8" type="text"
                                                                                  class="form-control"
                                                                                  style="background-color:#E8F3FF"/>
                                                <p id="val_sumber_data_8"></p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn green" onclick="val_k4_5()">Simpan</button>
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div id="k5_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k5_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title"><strong>K.5 RUMAH TIDAK LAYAK HUNI</strong></h5>
                                </div>
                                <div class="modal-body">
                                    <table id="user" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td style="width:25%" align="center"><strong>Kabupaten/Kota</strong>
                                            </td>
                                            <td align="left">
                                                <div id="ada_k5">
                                                    <input type"text" id="adat_k5" class="form-control" disabled>
                                                </div>

                                                <div id="hilang_k5">
                                                    <select name="idkabupaten_kota" id="idkabupaten_kota"
                                                            class="form-control">
                                                        <option value="">Pilih Kabupaten/Kota</option>
                                                        <?php foreach ($kabupaten_kota_combo as $r):
                                                            $this->db->select('*');
                                                            $this->db->where('idform_1', $idform_1);
                                                            $this->db->where('idkabupaten_kota', $r->idkabupaten_kota);
                                                            $this->db->from('form_1_k5');
                                                            $query = $this->db->get();
                                                            if ($query->num_rows() < 1) {
                                                                $mati = "";
                                                            } else {
                                                                $mati = "disabled";
                                                            }
                                                            ?>
                                                            <option value="<?php echo $r->idkabupaten_kota; ?>"
                                                                    style="background-color:#E8F3FF" <?php echo $mati; ?> >
                                                                <?php echo $r->kabupaten_kota; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%" align="center"><strong>Jumlah KK/RT</strong></td>
                                            <td align="center">
                                                <input name="idform_1_k5" id="idform_1_k5" type="hidden"/>
                                                <input name="iddinas" type="hidden" class="form-control"
                                                       value="<?php echo $iddinas; ?>"/>
                                                <input name="jenisk5" value="1A" type="hidden"/>
                                                <input name="idform_1" type="hidden" class="form-control"
                                                       value="<?php echo $idform_1; ?>"/>
                                                <input name="jumlah_kk_rt_3" id="jumlah_kk_rt_3" type="text"
                                                       class="form-control"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%" align="center"><strong>Jumlah RTLH Versi BDT
                                                    (unit)</strong></td>
                                            <td align="center">
                                                <input name="jumlah_rtlh_versi_bdt_4" id="jumlah_rtlh_versi_bdt_4"
                                                       type="text" class="form-control"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%" align="center"><strong>Jumlah RTLH Verifikasi
                                                    Pemda (unit)</strong></td>
                                            <td align="center">
                                                <input name="jumlah_rtlh_verifikasi_pemda_5"
                                                       id="jumlah_rtlh_verifikasi_pemda_5" type="text"
                                                       class="form-control"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center"><strong>Sumber Data</strong></td>
                                            <td align="center">
                                                <input name="sumber_data_6" id="sumber_data_6" type="text"
                                                       class="form-control"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn green" value="Simpan">
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div id="k6_modal" class="modal fade" tabindex="-1" data-backdrop="static"
                         data-keyboard="false">
                        <form action="" class="form-horizontal" method="post" id="k6_form"/>
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h5 class="modal-title"><strong>K.6. KAWASAN KUMUH </strong></h5>
                                </div>
                                <div class="modal-body">
                                    <table id="user" class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td style="width:40%"><strong>Kabupaten/Kota</strong></td>
                                            <td align="left">
                                                <input name="idform_1" type="hidden" class="form-control"
                                                       value="<?php echo $idform_1; ?>"/>
                                                <input name="iddinas" type="hidden" class="form-control"
                                                       value="<?php echo $iddinas; ?>"/>
                                                <input name="jenisk6_1" value="1A" type="hidden"
                                                       class="form-control"/>
                                                <input name="idform_1_k6" id="idform_1_k6" type="hidden"
                                                       class="form-control"/>
                                                <div id="ada_k6"><input type"text" id="adat_k6" class="form-control"
                                                    disabled>
                                                </div>
                                                <div id="hilang_k6">
                                                    <select name="idkabupaten_kota" id="idkabupaten_kota"
                                                            class="form-control">
                                                        <option value="">Pilih Kabupaten/Kota</option>
                                                        <?php foreach ($kabupaten_kota_combo as $r):
                                                            $this->db->select('*');
                                                            $this->db->where('idform_1', $idform_1);
                                                            $this->db->where('idkabupaten_kota', $r->idkabupaten_kota);
                                                            $this->db->from('form_1_k6');
                                                            $query = $this->db->get();
                                                            if ($query->num_rows() < 1) {
                                                                $mati = "";
                                                            } else {
                                                                $mati = "disabled";
                                                            }
                                                            ?>
                                                            <option value="<?php echo $r->idkabupaten_kota; ?>"
                                                                    style="background-color:#E8F3FF" <?php echo $mati; ?>>
                                                                <?php echo $r->kabupaten_kota; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                </div>
                                                <p id="val_idkabupaten_kota_select"></p>
                                            </td>
                                        </tr>
                                        <!-- <tr>
              <td><strong>Kecamatan</strong></td>
              <td>
                <select name="kecamatan_combo[]" id="kecamatan_combo"  multiple="multiple" class="form-control">
                  <option value=""  style="background-color:#E8F3FF">Kecamatan</option>
                  </?php foreach($kecamatan as $r): ?>
                  <option value="</?php echo $r->idkecamatan;?>" style="background-color:#E8F3FF"></?php echo $r->kecamatan;?> </option>
                  </?php endforeach; ?>
                </select>
                <p>Tekan Control (ctrl) pada keyboard untuk memilih lebih dari satu kecamatan</p>
              </td>
            </tr> -->
                                        <tr>
                                            <td style="width:15%"><strong>Luas wilayah kumuh (Ha)</strong></td>
                                            <td align="center">
                                                <input name="luas_wilayah_kumuh_4" id="luas_wilayah_kumuh_4"
                                                       type="text" class="form-control"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%"><strong>Jumlah RTLH dalam wilayah kumuh
                                                    (unit)</strong></td>
                                            <td align="center">
                                                <input name="jumlah_rtlh_dalam_wilayah_kumuh_5"
                                                       id="jumlah_rtlh_dalam_wilayah_kumuh_5" type="text"
                                                       class="form-control"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Sumber Data</strong></td>
                                            <td align="center">
                                                <input name="sumber_data_6" id="s6" type="text"
                                                       class="form-control"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn green" value="Simpan">
                                    <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTAINER -->
        </div>
    </div>
</body>
</html>