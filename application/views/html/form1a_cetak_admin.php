<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 foreach($form_1_view_detail as $r):
     $idform_1  = $r->idform_1;
     $tahun     = $r->tahun;
 endforeach;
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../../assets/global/img/favicon.png">
<style>p.indent{ padding-left: 1em }</style>
<style>p.indent{ padding-left: 1em }</style>
<style>p.indent2{ padding-left: 9.7em }</style>
<style>p.indent3{ padding-left: 3em }</style>
<style>
#kecamatan_combo{
 width:300px;
}
#tahun_form {
 width:100px;
}
</style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php include "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Formulir IA <small class="page-title-tag"><?php echo $provinsi;?></small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT <form action="http://localhost/datadekonpera/main/upload" id="form_sample_1" class="form-horizontal" method="post">-->
	<div class="page-content">
		<div class="container">
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('main/index'); ?>">Home</a><i class="fa fa-angle-right"></i>
        </li>
        <li>
           <a href="form_1a_view">Rekapitulasi Formulir IA</a><i class="fa fa-angle-right"></i>
        </li>
        <li class="active">
           Formulir IA Cetak
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
  			<div class="portlet light">
  				<div class="portlet-body">
            <!-- BEGIN FORM-->
            <div class="form-actions">
							<div class="row">
								<div class="form-body">

							<table id="user" class="table table-bordered table-striped">
							<tbody>
							<tr>
								<td style="width:15%">
									 Provinsi
								</td>
								<td style="width:50%">
									 <?php echo $provinsi;?>
                </td>
								<td style="width:35%" align="center">
									<span class="text-muted">
									<strong>FORMULIR IA</strong>
                  </span>
								</td>
							</tr>
							<tr>
								<td>
									Nama Dinas
                </td>
								<td>
								  <?php echo $nama_dinas;?>
								</td>
								<td align="center" rowspan="2" style="vertical-align:middle">
                  <?php echo $tahun;?>
								</td>
							</tr>
							<tr>
								<td>
									 Alamat
								</td>
								<td>
									 <?php echo $alamat;?>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" valign="middle">
									<span class="uppercase">
									<h3><strong>DAFTAR ISIAN PENDATAAN PKP PROVINSI <?php echo $provinsi;?> TAHUN <?=$tahun;?></strong></h3>
                  </span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<span class="text-muted">
									<p>Kegiatan pendataan perumahan ini dilakukan untuk memenuhi kebutuhan data
                  dan informasi perumahan. Data ini merupakan bagian dari Pembangunan Basis
                  Data PKP Provinsi sesuai UU No.1 Tahun 2011 tentang Perumahan dan Kawasan
                  Permukiman (PKP) pasal 17 dan Peraturan Pemerintah No.88 Tahun 2014 tentang
                  Pembinaan Penyelenggaraan PKP pasal 18 ayat 1.</p>
                  <p>Tabel ini diisi oleh SNVT bidang Perumahan Provinsi, untuk kelengkapan
                  datanya berkoordinasi dengan Pokja PKP Provinsi, BPS Provinsi, Bappeda
                  Provinsi, Dinas Sosial Provinsi, BKKBN Provinsi, serta rekapitulasi data
                  dari Form 1B oleh Kab/Kota. Untuk pertanyaan lebih lanjut dapat menghubungi
                  Subdit Data dan Informasi, Direktorat Jenderal Penyediaan Perumahan:
                  datinperumahan@gmail.com (021.7211883)</p>
                  <p>Daftar isian ini merupakan data sekunder hasil dari berbagai kegiatan
                  pendataan perumahan, terdiri dari:</p>
                  K.1. Kelembagaan Perumahan dan Permukiman</br>
                  K.2. Alokasi APBD untuk Urusan Perumahan dan Kawasan Permukiman (PKP)</br>
                  K.3. Pembangunan Perumahan</br>
                  K.4. Backlog Perumahan</br>
                  K.5. Rumah Tidak Layak Huni</br>
                  K.6. Kawasan Kumuh</span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
								  <h4><strong>K.1 KELEMBAGAAN PERUMAHAN DAN PERMUKIMAN</strong></h4>
                  <p>Struktur organisasi dinas yang menangani perumahan dan permukiman di Provinsi <?php echo $provinsi;?></p>
                  <center>
                  <?php foreach($form_1_k1_count as $f1k1count):
                      if ($f1k1count->f1k1count<1) {?>
                  		<img src="../../../../assets/uploads/sample.jpg" height="400" width="700"></img><p></p>
                  	<?php } else {?>
                  <img src="<?php foreach($form_1_k1 as $f1k1): ?><?php echo ".".$f1k1->struktur_dinas;?><?php endforeach;?>" height="400" width="700"></img><p></p>
                  <?php } endforeach;?>
								  </center>
                </td>
							</tr>
							<tr>
								<td colspan="3">
									<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Tidak ada</p></span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
                  <div id="k2_a_div">
									 <h4><strong>K.2 ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN PERMUKIMAN (PKP)</strong></h4>
                    <table id="user" class="table table-bordered table-striped">
  										<tbody>
    										<tr>
    											<td style="width:50%" align="center"><strong>Uraian</strong></td>
    											<td style="width:15%" align="center"><strong>Tahun <?=$tahun-1;?> (Rp)<label class="control-label"><span class="required">*</span></label></strong></td>
    											<td style="width:15%" align="center"><strong>Tahun <?=$tahun;?> (Rp)</strong></td>
    										</tr>
    										<tr>
    											<td align="center"><font size="1">(1)</font></td>
    											<td align="center"><font size="1">(2)</font></td>
    											<td align="center"><font size="1">(3)</font></td>
    										</tr>
                        <?php foreach($form_1_k2_a as $f1k2av): ?>
    										<tr>
    											<td align="center"><?php echo $f1k2av->uraian1;?></td>
    											<td align="center">
    											<?php echo "Rp ".number_format( $f1k2av->k221 , 2 , ',' , '.' );?></td>
    											<td align="center"><?php echo "Rp. ".number_format( $f1k2av->k231 , 2 , ',' , '.' ) ;?></td>
    										</tr>
                        <?php endforeach; ?>
  							  		</tbody>
								    </table>
                  </div>
								 </td>
							</tr>
							<tr>
								<td colspan="3">
									<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Anggaran total APBD Kabupaten terhadap anggaran SKPD/Dinas PKP Kabupaten dalam tahun yang sama, sehingga didapat persentase perbandingan anggaran urusan PKP, yaitu : 0,061 atau 6,1 %</p></span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
                <div id="k2_b_div">
									<table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%"  align="center" rowspan="2"><strong>No.</strong></td>
											<td style="width:30%" align="center" rowspan="2"><strong>Jenis Kegiatan Urusan PKP</strong></td>
											<td style="width:25%" align="center" colspan="2"><strong>TA. <?=$tahun-1;?></strong></td>
											<td style="width:25%" align="center" colspan="2"><strong>TA. <?=$tahun;?></strong></td>
										</tr>
										<tr>
											<td align="center">Volume/unit</td>
											<td align="center">Biaya (Rp.)</td>
											<td align="center">Volume/unit</td>
											<td align="center">Biaya (Rp.)</td>
										</tr>
										<tr>
											<td align="center"><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
											<td align="center"><font size="1">(4)</font></td>
											<td align="center"><font size="1">(5)</font></td>
											<td align="center"><font size="1">(6)</font></td>
										</tr>
                    <?php $i=0; foreach($form_1_k2_b as $f1k2b3): ?>
										<tr>
											<td align="center" ><?php $i++; echo $i;?>.</td>
											<td align="center"><?php echo $f1k2b3->jenis_kegiatan_urusan_pkp_2;?></td>
											<td align="center"><?php echo $f1k2b3->ta_a_vol_unit_3;?></td>
											<td align="center" ><?php echo "Rp. ".number_format( $f1k2b3->ta_a_biaya_4 , 2 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k2b3->ta_a_vol_unit_5;?></td>
											<td align="center"><?php echo "Rp. ".number_format( $f1k2b3->ta_a_biaya_6 , 2 , ',' , '.' ) ;?> </td>
										</tr>
                    <?php endforeach; ?>
										<tr>
											<td align="center"></td>
											<td align="center"></font></td>
											<td align="center"></font></td>
											<td align="center"></font></td>
											<td align="center"></td>
											<td align="center"></td>
										</tr>
										<tr>
                      <?php $i=0; foreach($form_1_k2_b_sum as $f1k2b3sum): ?>
											<td align="center"></td>
											<td align="right" ><strong>Total unit rumah</strong></font></td>
											<td align="center"><?php echo $f1k2b3sum->total_unit_rumah_1;?></font></td>
											<td align="right" ><strong>Total unit rumah</strong></font></td>
											<td align="center"><?php echo $f1k2b3sum->total_unit_rumah_2;?></td>
											<td align="center"></td>
                      <?php endforeach; ?>
										</tr>
							  		</tbody>
									</table>
                  <p>*) <strong>Total unit rumah</strong> dikumpulkan pada acara Forum Sinkronisasi Pendataan Perumahan sebagai data Program Suplai Perumahan</p>
                </div>
                </td>
							</tr>
							<tr>
								<td colspan="3">
  								<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Kegiatan fisik, biasanya rehabilitasi rumah berupa peningkatan kualitas, yang bersumber dari APBD Provinsi/anggaran Dinas PKP Provinsi, volumenya berupa unit, didata sebagai data pembangunan unit baru lingkup Program Suplai Perumahan</p></span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
                <div id="k3_div">
								<h4><strong>K.3  PEMBANGUNAN PERUMAHAN </strong></h4>
                <p>1. Apakah Pemerintah Provinsi mempunyai Dokumen Perencanaan Urusan Perumahan?</p>
                  <?php $i=0; foreach($form_1_k3 as $f1k3): ?>
									<p class="indent"><label>
									<input type="checkbox" name="isi_1a" <?php if($f1k3->isi_1a=='Y') { echo "checked"; } ?> value="Y" disabled/> a. RTRW (sudah perda) </label></p>
									<p class="indent"><label>
									<input type="checkbox" name="isi_1b" <?php if($f1k3->isi_1b=='Y') { echo "checked"; } ?> value="Y" disabled/> b. RDTR (sudah perda) </label></p>
									<p class="indent"><label>
									<input type="checkbox" name="isi_1c" <?php if($f1k3->isi_1c=='Y') { echo "checked"; } ?> value="Y" disabled/> c. RP3KP/RP4D </label></p>
									<p class="indent"><label>
									<input type="checkbox" name="isi_1d" <?php if($f1k3->isi_1d=='Y') { echo "checked"; } ?> value="Y" disabled/> d. RPIJM </label></p>
									<p class="indent"><label>
									<input type="checkbox" name="isi_1e" <?php if($f1k3->isi_1e=='Y') { echo "checked"; } ?> value="Y" disabled/> e. Renstra Dinas PKP </label></p>
									<p class="indent"><label>
									<input type="checkbox" name="isi_1f" <?php if($f1k3->isi_1f=='Y') { echo "checked"; } ?> value="Y" disabled/>	f. Lainnya (tuliskan):
									<?php echo $f1k3->isi_1f_keterangan;?></label></p>
									</br>
                <p>2. Dalam pelaksanaan Urusan Perumahan, Pemerintah Provinsi memanfaatkan sumber dana yang berasal dari:</p>
                <div class="checkbox-list" data-error-container="#form_2_services_error">
									<p class="indent">
									<label><input type="checkbox" name="isi_2a" <?php if($f1k3->isi_2a=='Y') { echo "checked"; } ?> value="Y" disabled/> a. APBD </label></p>
									<p class="indent">
									<label><input type="checkbox" name="isi_2b" <?php if($f1k3->isi_2b=='Y') { echo "checked"; } ?> value="Y" disabled/> b. Loan (pinjaman) dari badan/bank luar negeri </label></p>
									<p class="indent">
									<input type="checkbox"  name="isi_2c" <?php if($f1k3->isi_2c=='Y') { echo "checked"; } ?> value="Y" disabled/> c. CSR (uraikan):
									<label><?php echo $f1k3->isi_2c_keterangan;?></label></p>
									<p class="indent">
									<input type="checkbox"  name="isi_2d" <?php if($f1k3->isi_2d=='Y') { echo "checked"; } ?> value="Y" disabled/> d. Swasta (uraikan):
									<label><?php echo $f1k3->isi_2d_keterangan;?></label></p>
									<p class="indent"><label>
									<input type="checkbox"  name="isi_2e" <?php if($f1k3->isi_2e=='Y') { echo "checked"; } ?> value="Y" disabled/> e. Lainnya (tuliskan): APBN </label></p>
                  <?php endforeach;?>
                </div>
                </div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Kelengkapan dokumen kelengkapan/pendukung program/kegiatan Perumahan dan Kawasan Permukiman yang telah ada dasar leglisasinya seperti Perda atau Pergub, dan juga sumber dana yang pernah digunakan untuk pembangunan/rehabilitasi rumah di provinsi</p></span>
								</td>
							</tr>
							<tr>
							  <td colspan="3">
                <div id="k4_1_div">
							  <h4><strong>K.4 BACKLOG PERUMAHAN</strong></h4>
								<p>1. Jumlah Rumah Berdasarkan Status Kepemilikan Tempat Tinggal</p>
                <?php $i=0; foreach($form_1_k4_1 as $f1k41): ?>
                  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"><strong>No.</strong></td>
											<td colspan="2" align="center" style="width:45%"><strong>Status Kepemilikan Tempat Tinggal</strong></td>
											<td style="width:25%" align="center"><strong>Jumlah (unit)</strong></td>
											<td style="width:25%" align="center"><strong>Sumber Data</strong></td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td colspan="2" align="center"><font size="1">(2)</font></td>
											<td align="center" ><font size="1">(3)</font></td>
											<td align="center"><font size="1">(4)</font></td>
										</tr>
										<tr>
											<td align="center" >1.</td>
											<td colspan="2" ><?php echo $f1k41->status1;?></font></td>
											<td align="center" ><?php echo number_format( $f1k41->k4141 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4151;?></td>
										</tr>
										<tr>
											<td align="center" >2.</td>
											<td colspan="2" ><?php echo $f1k41->status2;?></td>
											<td align="center" ><?php echo number_format( $f1k41->k4142 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4152;?></td>
										</tr>
										<tr>
											<td align="center" >3.</td>
											<td colspan="2" ><?php echo $f1k41->status3;?></td>
											<td align="center" ><?php echo number_format( $f1k41->k4143 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4153;?></td>
										</tr>
										<tr>
											<td align="center" >4.</td>
											<td colspan="2" ><?php echo $f1k41->status4;?></td>
											<td align="center" ><?php echo number_format( $f1k41->k4144 , 0, ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4154;?></td>
										</tr>
										<tr>
											<td align="center">5.</td>
											<td colspan="2" ><?php echo $f1k41->status5;?></td>
											<td align="center"><?php echo number_format( $f1k41->k4145 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k41->k4155;?></td>
										</tr>
										<tr>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
										</tr>
										<tr>
											<td align="center"></td>
											<td colspan="2" align="right"><strong>Total A (2+3+4+5)</strong></td>
											<td align="center"><?php echo number_format( $f1k41->total_a , 0 , ',' , '.' ) ;?></td>
											<td align="center">&nbsp;</td>
										</tr>
										<tr>
											<td align="center"></td>
											<td colspan="2" align="right"><strong>Total B (3)</strong></td>
											<td align="center"><?php echo number_format( $f1k41->total_b , 0 , ',' , '.' ) ;?></td>
											<td align="center">&nbsp;</td>
										</tr>
							  		</tbody>
								</table>
                <p>Backlog Kepemilikan = <strong>(Total A) = <?php echo number_format( $f1k41->total_a , 0 , ',' , '.' ) ;?> unit</strong></p>
                <p>Backlog Penghunian = <strong>(Total B  = <?php echo number_format( $f1k41->total_b , 0 , ',' , '.' ) ;?><strong> unit</strong></p>
              </div>
              </td>
              <?php endforeach;?>
							</tr>
							<tr>
								<td colspan="3">
  								<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Data backlog kepemilikan dan backlog penghunian kabupaten ditentukan oleh Dinas PKP, data dari BPS dan BKKBN hanya sebagai benchmark data yang perlu divalidasi oleh Dinas PKP.</p><p>
                  <strong>Catatan:</strong> data BKKBN bersumber dari sensus Pendataan Keluarga 2015 oleh BKKBN, yang telah menandatangani Kesepakatan Bersama dengan Kementerian PUPR Nomor:1/KSM/G2/2017 Nomor:01/PKS/M/2017 tentang Peningkatan Program Kependudukan, Keluarga Berencana dan Pembangunan Keluarga dalam Pembangunan Infrastruktur Pekerjaan Umum dan Perumahan Rakyat, tanggal 11 Januari 2017</p></span>
								</td>
							</tr>
							<tr>
							  <td colspan="3">
                <div id="k4_2_div">
								<p>2. Jenis Fisik Bangunan Rumah</p>
                <?php $i=0; foreach($form_1_k4_2 as $f1k42): ?>
                  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%"  align="center"><strong>No.</strong></td>
											<td style="width:45%" align="center"><strong>Jenis Fisik Bangunan Rumah</strong></td>
										  <td style="width:25%" align="center"><strong>Jumlah (unit)</strong></td>
										  <td style="width:25%" align="center"><strong>Sumber Data</strong></td>
										</tr>
										<tr>
											<td align="center"><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td><td align="center"><font size="1">(4)</font></td>
										</tr>
										<tr>
											<td align="center">1.</td>
											<td align="left"  ><?php echo $f1k42->jenis1;?></td>
											<td align="center"><?php echo number_format( $f1k42->k4231 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k42->k4241;?></td>
										</tr>
										<tr>
											<td align="center">2.</td>
											<td align="left"  ><?php echo $f1k42->jenis2;?></td>
											<td align="center"><?php echo number_format( $f1k42->k4232 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k42->k4242;?></td>
										</tr>
										<tr>
											<td align="center"></td>
											<td align="right" ><strong>Total</strong></td>
											<td align="center"><strong><?php echo number_format( $f1k42->total , 0 , ',' , '.' ) ;?></strong></td>
											<td align="center">&nbsp;</td>
										</tr>
							  		</tbody>
							    </table>
                  <?php endforeach;?>
                  <p>&nbsp;</p>
                </div>
                </td>
							</tr>
							<tr>
								<td colspan="3">
  								<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Jumlah fisik bangunan rumah, bukan data Rumah Tangga atau data Kepala Keluarga, tapi merupakan unit bangunan rumah baik terhuni maupun tidak.</p></span>
								</td>
							</tr>
							<tr>
								<td colspan="3">
                <div id="k4_3_div">
								<p>3. Jumlah Kepala Keluarga (KK) dalam satu rumah</p>
                <?php $i=0; foreach($form_1_k4_3 as $f1k43): ?>
                  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"  ><strong>No.</strong></td>
											<td style="width:45%" align="center"  ><strong>Fungsi Rumah</strong></td>
										  <td style="width:25%" align="center"><strong>Jumlah (unit)</strong></td>
										  <td style="width:25%" align="center" ><strong>Sumber Data</strong></td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td><td align="center"><font size="1">(4)</font></td>
										</tr>
										<tr>
											<td align="center" >1.</td>
											<td align="left"><?php echo $f1k43->fungsi1;?></td>
											<td align="center" ><?php echo number_format( $f1k43->k4331 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k43->k4341;?></td>
										</tr>
										<tr>
											<td align="center" >2.</td>
											<td align="left"><?php echo $f1k43->fungsi2;?></td>
											<td align="center" ><?php echo number_format( $f1k43->k4332 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k43->k4342;?></td>
										</tr>
							  		</tbody>
								  </table>
                  <?php endforeach;?>
                </div>
								</td>
							</tr>
							<tr>
								<td colspan="3" height="50">
									<span class="text-muted">
								  <p>&nbsp;</p></span>
								</td>
							</tr>
              <tr>
								<td colspan="3">
                <div id="k4_4_div">
								<p>4. Jumlah sambungan listrik rumah (PLN)</p>
                  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"  rowspan="2"><strong>No.</strong></td>
											<td style="width:30%" align="center" rowspan="2"><strong>Kab/Kota</strong></td>
										  <td style="width:25%" align="center" colspan="2"><strong>Jumlah rumah (unit)</strong></td>
										  <td style="width:25%" align="center" rowspan="2"><strong>Sumber Data</strong></td>
										</tr>
										<tr>
											<td align="center"><strong><?=$tahun-1;?></strong></td>
											<td align="center"><strong><?=$tahun;?></strong></td>
										</tr>
										<tr>
											<td align="center"><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
                      <td align="center"><font size="1">(4)</font></td>
                      <td align="center"><font size="1">(5)</font></td>
										</tr>
                    <?php $i=0; foreach($form_1_k4_4 as $f1k44): ?>
										<tr>
											<td align="center"><?php $i++; echo $i;?>.</td>
											<td align="left"  ><?php echo $f1k44->kabupaten_kota;?></td>
											<td align="center"> <?php echo  number_format( $f1k44->jumlah_rumah_3 , 0 , ',' , '.' ) ;?></td>
											<td align="center">  <?php echo  number_format( $f1k44->jumlah_rumah_4 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k44->sumber_data_5;?></td>
										</tr>
                    <?php endforeach;?>
										<tr>
                      <?php $i=0; foreach($form_1_k4_4_sum as $f1k44sum): ?>
											<td colspan="2" align="right" ><strong>Total</strong></td>
											<td align="center" ><strong> <?php echo  number_format( $f1k44sum->total_1 , 0 , ',' , '.' ) ;?></strong></td>
											<td align="center"><strong> <?php echo  number_format( $f1k44sum->total_2 , 0 , ',' , '.' ) ;?></strong></td>
                      <?php endforeach;?>
										</tr>
							  		</tbody>
								  </table>
                </div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
  								<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Data Sambungan Listrik berasal dari PLN, per kabupaten/kota.</p></span>
								</td>
							</tr>
              <tr>
								<td colspan="3">
								<p>5. Jumlah Pembangunan Rumah dalam 1 tahun</p><div id="k4_5_div"></div>
                  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%" align="center"  rowspan="2"><strong>No.</strong></td>
											<td style="width:25%" align="center" colspan="2"><strong>Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun-1;?> (unit)</strong></td>
										  <td style="width:25%" align="center" colspan="2"><strong>Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun;?> (unit)</strong></td>
                      <td style="width:25%" align="center" colspan="2"><strong>Jumlah Pembangunan Rumah Berdasarkan Non IMB (unit).....Tahun</strong></td>
										  <td align="center" rowspan="2"><strong>Sumber Data</strong></td>
										</tr>
										<tr>
											<td align="center">Non MBR</td>
											<td align="center">MBR</td>
											<td align="center">Non MBR</td>
											<td align="center">MBR</td>
											<td align="center">Non MBR</td>
											<td align="center">MBR</td>
										</tr>
										<tr>
											<td align="center" ><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
                      <td align="center"><font size="1">(4)</font></td>
											<td align="center"><font size="1">(5)</font></td>
                      <td align="center"><font size="1">(6)</font></td>
											<td align="center"><font size="1">(7)</font></td>
                      <td align="center"><font size="1">(8)</font></td>
										</tr>
                    <?php $i=0; foreach($form_1_k4_5 as $f1k45): ?>
										<tr>
											<td align="center" ><?php $i++; echo $i;?>.</td>
											<td align="center"><?php echo number_format( $f1k45->non_mbr_2 , 0 , ',' , '.' ) ;?></td>
											<td align="center" ><?php echo number_format( $f1k45->mbr_3 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo number_format( $f1k45->non_mbr_4 , 0 , ',' , '.' ) ;?></td>
											<td align="center" ><?php echo number_format( $f1k45->mbr_5 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo number_format( $f1k45->non_mbr_6 , 0 , ',' , '.' ) ;?></td>
											<td align="center" ><?php echo number_format( $f1k45->mbr_7 , 0 , ',' , '.' ) ;?></td>
											<td align="center"><?php echo $f1k45->sumber_data_8;?></td>
										</tr>
                    <?php endforeach;?>
							  		</tbody>
								  </table>
                  <p>*) <strong>Jumlah Pembangunan Rumah Berdasarkan IMB 2016 (unit)</strong> dikumpulkan pada acara Forum Sinkronisasi Pendataan Perumahan sebagai data Program Suplai Perumahan (identifikasi awal perkiraan pembangunan rumah)</p>
								</td>
							</tr>
							<tr>
								<td colspan="3" height="50">
									<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Data pembangunan unit rumah per tahun oleh seluruh stakeholder perumahan terdata dalam IMB melalui Dinas PTSP, dan jumlah yang tidak terdata dalam IMB dianalisis/diasumsikan oleh Dinas PKP, sehingga didapat perkiraan pembangunan unit rumah satu kabupaten dalam satu tahun. Ini disebut data supply perumahan. Data total dari seluruh Kabupaten/Kota</p></span>
								</td>
							</tr>
							<tr>
							  <td colspan="3" height="50">
                <div id="k5_div">
                  <h4><strong>K.5 RUMAH TIDAK LAYAK HUNI</strong></h4>
                  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%"  align="center"><strong>No.</strong></td>
											<td style="width:25%" align="center"><strong>Kab/Kota</strong></td>
											<td style="width:15%" align="center"><strong>Jumlah KK/RT</strong></td>
											<td style="width:15%" align="center"><strong>Jumlah RTLH Versi BDT (unit)</strong></td>
											<td style="width:15%" align="center"><strong>Jumlah RTLH  Verifikasi Pemda (unit)</strong></td>
                      <td align="center"><strong>Sumber Data</strong></td>
                      <td align="center"><strong>Fungsi</strong></td>
										</tr>
										<tr>
											<td align="center"><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
											<td align="center"><font size="1">(4)</font></td>
											<td align="center"><font size="1">(5)</font></td>
											<td align="center"><font size="1">(6)</font></td>
											<td align="center"><font size="1">(7)</font></td>
										</tr>
                    <?php $i=0; foreach($form_1_k5 as $f1k5): ?>
										<tr>
                      <td align="center"><?php $i++; echo $i;?>.</td>
                      <td align="left"  ><?php echo $f1k5->kabupaten_kota;?></td>
                      <td align="center"><?php echo number_format( $f1k5->jumlah_kk_rt_3 , 0 , ',' , '.' ) ;?></td>
                      <td align="center"><?php echo number_format( $f1k5->jumlah_rtlh_versi_bdt_4 , 0 , ',' , '.' ) ;?></td>
                      <td align="center"><?php echo number_format( $f1k5->jumlah_rtlh_verifikasi_pemda_5 , 0 , ',' , '.' ) ;?></td>
                      <td align="center"><?php echo $f1k5->sumber_data_6;?></td>
										</tr>
                    <?php endforeach;?>
										<tr>
                    <?php $i=0; foreach($form_1_k5_sum as $f1k5sum): ?>
											<td align="right" colspan="2"><strong>Total</strong></td>
											<td align="center"><strong> <?php echo number_format( $f1k5sum->total_1 , 0 , ',' , '.' ) ;?></strong></td>
											<td align="center"><strong> <?php echo number_format( $f1k5sum->total_2 , 0 , ',' , '.' ) ;?></strong></td>
											<td align="center"><strong> <?php echo number_format( $f1k5sum->total_3 , 0 , ',' , '.' ) ;?></strong></td>
                    <?php endforeach;?>
										</tr>
							  		</tbody>
								  </table>
                </div>
							  </td>
							</tr>
							<tr>
								<td colspan="3">
									<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Data RTLH merupakan data yang sama dengan Sistem Informasi Rumah Swadaya, yang bersumber dari data olahan BDT 2015</p></span>
								</td>
							</tr>
              <tr>
							  <td colspan="3">
                <div id="k6_div">
                <h4><strong>K.6 KAWASAN KUMUH</strong></h4>
                  <table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td style="width:5%"  align="center"><strong>No.</strong></td>
											<td style="width:20%" align="center"><strong>Kab/Kota</strong></td>
											<td style="width:20%" align="center"><strong>Kecamatan</strong></td>
											<td style="width:15%" align="center"><strong>Luas wilayah kumuh (Ha)</strong></td>
											<td style="width:15%" align="center"><strong>Jumlah RTLH dalam wilayah kumuh (unit)</strong></td>
                      <td align="center"><strong>Sumber Data</strong></td>
										</tr>
										<tr>
											<td align="center"><font size="1">(1)</font></td>
											<td align="center"><font size="1">(2)</font></td>
											<td align="center"><font size="1">(3)</font></td>
											<td align="center"><font size="1">(4)</font></td>
											<td align="center"><font size="1">(5)</font></td>
											<td align="center"><font size="1">(6)</font></td>
										</tr>
                    <?php $i=0; foreach($form_1_k6 as $f1k6): ?>
										<tr>
                      <td align="center" ><?php $i++; echo $i;?>.</td>
                      <td align="left"><input name="idform_1_k6_<?php echo $i;?>" type="hidden" class="form-control" value="<?php echo $f1k6->idform_1_k6;?>"/><?php echo $f1k6->kabupaten_kota;?></td>
                      <td >
                      <?php
                      		$this->db->select('idkecamatan,kecamatan');
                              $this->db->where('idform_1_k6',$f1k6->idform_1_k6);
                              $this->db->from('form_1_k6_kecamatan_view');
                              $query = $this->db->get();
                      		$x=0;
                            foreach ($query->result() as $value) {
                      		  $x++;
                        		  if ($x<$query->num_rows()){
                                  echo $value->kecamatan.", ";
                        		  }
                        		  else
                        		  {
                                  echo $value->kecamatan;
                        		  }
                            }
                      ?>
                      </td>
                      <td align="center"><?php echo $f1k6->luas_wilayah_kumuh_4;?></td>
                      <td align="center"><?php echo $f1k6->jumlah_rtlh_dalam_wilayah_kumuh_5;?></td>
                      <td align="center"><?php echo $f1k6->sumber_data_6;?></td>
										</tr>
                    <?php endforeach;?>
										<tr>
                    <?php $i=0; foreach($form_1_k6_sum as $f1k6sum): ?>
											<td align="right" colspan="3"><strong>Total</strong></td>
											<td align="center"><strong><?php echo number_format( $f1k6sum->total_luas_wilayah_kumuh , 0 , ',' , '.' ) ;?></strong></td>
											<td align="center"><strong><?php echo number_format( $f1k6sum->total_jumlah_rtlh_dalam_wilayah_kumuh , 0 , ',' , '.' ) ;?></strong></td>
											<td align="center">&nbsp;</td>
                    <?php endforeach;?>
										</tr>
							  		</tbody>
								</table>
                </div>
							  </td>
							</tr>
							<tr>
								<td colspan="3">
									<span class="text-muted"><strong>Penjelasan:</strong>
                  <p>Jumlah RTLH dalam kawasan kumuh dapat diperoleh dari Satker Kotaku, data ini untuk integrasi penanganan kumuh, Ditjen Penyediaan Perumahan dengan program BSPS atau Rusunawa, Ditjen Cipta Karya dengan penanganan infrastruktur di dalam satu kawasan. Uraian per Kabupaten/Kota</p></span>
								</td>
							</tr>
							</tbody>
							</table>
              <p>Demikian hasil pelaksanaan pendataan perumahan dan kawasan permukiman, dilakukan untuk dipergunakan sebagaimana mestinya.</p>
              <p><?php echo $kabupaten_kota;?>, <strong>___________________<?php echo $tahun;?></strong></p>
              <p>Ttd.</p></br>
              <p><strong><?php echo isset($r->kepala_dinas);?></strong></p>
              <p>Kepala Dinas PKP Provinsi</p>
								<div class="col-md-offset-0 col-md-12" align="right">
								<button class="btn green" onclick="javascript:window.print();">Cetak</button>
								</div>
						</div>
					</div>
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
</div>
</div>
</div>
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<?php include "footer2.php";?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/form-validation.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
jQuery(document).ready(function() {
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
   FormValidation.init();
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>
<!-- END BODY -->
</html>
