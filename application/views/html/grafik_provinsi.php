<div class="portlet-title">
  <div class="caption" style="line-height: 21px">
		<i class="icon-bar-chart font-grey-gallery"></i>
		<span class="caption-subject bold font-grey-gallery uppercase">
      Diagram
      <?php
      if ($privilage == "1"){
        echo "";
      }
      else{
        echo "Provinsi ".$provinsi;
      }
      ?>
    </span>
	</div>
		<div class="tools" style="position: absolute; top: 5px; right: 50px; display: <?php if ($privilage=='1') { echo 'block'; } else { echo 'none'; }?>">
		    <form action="<?=BASE_URL('main/index')?>" class="form-horizontal" method="post" />
  		    <label class="control-label visible-ie8 visible-ie9">Provinsi</label>
  				<table border="0">
            <tr>
              <td>
                <select class="form-control form-control-solid placeholder-no-fix" style="border: solid #ccc 1px" autocomplete="off" id="provinsi_select" placeholder="provinsi_select" name="provinsi_select"/>
        				  <option value="0">Nasional</option>
        					<?php foreach($provinsi_diagram as $r): ?>
        					<option value="<?php echo $r->idprovinsi;?>"><?php echo $r->provinsi;?></option>
        					<?php endforeach; ?>
        				</select>
              </td>
              <td>
                <input type="submit" class="btn left-zeroradius grey-cascade" value="Lihat">
              </td>
            </tr>
          </table>
		    </form>
		</div>
</div>
<div class="portlet-body">
  <div class="tabbable-custom ">
    <form id="formgrafik" method="post" action="<?=BASE_URL('main/index');?>">
      <input type="hidden" id="g1" name="g1">
      <input type="hidden" id="g2" name="g2">
      <input type="hidden" id="g3" name="g3">
      <input type="hidden" id="g4" name="g4">
      <input type="hidden" id="g5" name="g5">
      <input type="hidden" id="kategori_1" name="kategori_1">
      <input type="hidden" id="kategori_2" name="kategori_2">
    </form>
    <ul class="nav nav-tabs">
      <li <?php if ($g2=="1"){echo 'class="active"';}?>>
        <a href="#tab12" data-toggle="tab" onclick="g2()"><i class="icon-bar-chart theme-font hide"></i>Backlog</a>
      </li>
      <li <?php if ($g3=="1"){echo 'class="active"';}?>>
        <a href="#tab11" data-toggle="tab" onclick="g3()"><i class="icon-bar-chart theme-font hide"></i>RTLH</a>
      </li>
      <li <?php if ($g4=="1"){echo 'class="active"';}?>>
        <a href="#tab11" data-toggle="tab" onclick="g4()"><i class="icon-bar-chart theme-font hide"></i>Suplai Rumah IMB</a>
      </li>
      <li <?php if ($g5=="1"){echo 'class="active"';}?>>
        <a href="#tab11" data-toggle="tab" onclick="g5()"><i class="icon-bar-chart theme-font hide"></i>Suplai Rumah Non IMB</a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab11" style="display: <?php if ($g3==1) { echo 'block'; } else { echo 'none'; }?>">
        <div id="chart_5" class="chart" style="height: 400px;"></div>
      </div>
      <div class="tab-pane" id="tab12"  style="display: <?php if ($g2==1) { echo 'block'; } else { echo 'none'; }?>">
        <div id="<?php if ($privilage==3){echo "echarts_bar";}else{echo "echarts_bar";}  ?>" style="height:400px"></div>
      </div>
      <div class="tab-pane" id="tab12"  style="display: <?php if ($g4==1) { echo 'block'; } else { echo 'none'; }?>">
        <div id="<?php if ($privilage==3){echo "echarts_bar_imb";}else{echo "echarts_bar_imb";}  ?>" style="height:400px"></div>
      </div>
      <div class="tab-pane" id="tab12"  style="display: <?php if ($g5==1) { echo 'block'; } else { echo 'none'; }?>">
        <div id="<?php if ($privilage==3){echo "echarts_bar_non_imb";}else{echo "echarts_bar_non_imb";}  ?>" style="height:400px"></div>
      </div>
    </div>
  </div>
</div>
<!-- CORE PLUGINS -->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../../assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="<?php echo BASE_URL("main/grafikechart_prov/");?>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php if ($g1==1){ echo BASE_URL("main/grafikdpenghunian_1a_prov/".$iddinas."/".$tahun);} elseif ($g2==1){ echo BASE_URL("main/grafikdpenghunian_1a_prov/".$iddinas."/".$tahun);} elseif ($g3==1){ echo BASE_URL("main/grafikd_prov/".$iddinas."/".$tahun."/".$idprovinsi);} elseif ($g4==1){ echo BASE_URL("main/grafikdsejutarumah/".$iddinas);} elseif ($g5==1){ echo BASE_URL("main/grafikdsejutarumah2/".$iddinas);} ?>"></script>
<script>
	jQuery(document).ready(function() {
  ChartsAmcharts.init(); // init grafik
});
/* BEGIN SCRIPT GRAFIK */
function g1()
{
        document.getElementById('g1').value="1";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function g2()
{
        document.getElementById('g2').value="1";
        document.getElementById('g1').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function g3()
{
        document.getElementById('g2').value="0";
        document.getElementById('g1').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g3').value="1";
        document.getElementById('g5').value="0";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function g4()
{
        document.getElementById('g2').value="0";
        document.getElementById('g1').value="0";
        document.getElementById('g4').value="1";
        document.getElementById('g3').value="0";
        document.getElementById('g5').value="0";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function g5()
{
        document.getElementById('g2').value="0";
        document.getElementById('g1').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g5').value="1";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function kategori1()
{
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        <?php if ($g2=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="1";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        <?php }?>
        <?php if ($g3=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="1";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        <?php }?>
        <?php if ($g4=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="1";
        document.getElementById('g5').value="0";
        <?php }?>
        <?php if ($g5=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="1";
        <?php }?>
        document.getElementById('formgrafik').submit();
}
function kategori2()
{
        document.getElementById('kategori_1').value="0";
        document.getElementById('kategori_2').value="1";
        <?php if ($g2=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="1";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        <?php }?>
        <?php if ($g3=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="1";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        <?php }?>
        <?php if ($g4=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="1";
        document.getElementById('g5').value="0";
        <?php }?>
        <?php if ($g5=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="1";
        <?php }?>
        document.getElementById('formgrafik').submit();
}
try {
var AG_onLoad=function(func)
{
  if(document.readyState==="complete"||document.readyState==="interactive")
    func();
  else if(document.addEventListener)document.addEventListener("DOMContentLoaded",func);
  else if(document.attachEvent)document.attachEvent("DOMContentLoaded",func)};
  var AG_removeElementById = function(id)
  {
    var element = document.getElementById(id);
    if (element && element.parentNode)
    {
      element.parentNode.removeChild(element);
    }
  };
  var AG_removeElementBySelector = function(selector)
  {
    if (!document.querySelectorAll)
    {
      return;
    }
    var nodes = document.querySelectorAll(selector);
    if (nodes)
    {
      for (var i = 0; i < nodes.length; i++)
      {
        if (nodes[i] && nodes[i].parentNode)
          {
            nodes[i].parentNode.removeChild(nodes[i]);
          }
      }
    }
  };
  var AG_each = function(selector, fn)
  {
  if (!document.querySelectorAll)
  {
    return;
  }
  var elements = document.querySelectorAll(selector);
  for (var i = 0; i < elements.length; i++)
  {
    fn(elements[i]);
  };
};
var AG_removeParent = function(el, fn)
{
  while (el && el.parentNode)
  {
    if (fn(el))
    {
      el.parentNode.removeChild(el);
      return;
    }
    el = el.parentNode;
  }
};
var AdFox_getCodeScript = function() {};
AG_onLoad(function()
{
   AG_each('iframe[id^="AdFox_iframe_"]', function(el)
   {
    if (el && el.parentNode)
    {
      el.parentNode.removeChild(el);
    }
});
});
try {
  Object.defineProperty(window, 'noAdsAtAll', { get: function() { return true; } });
}
catch (ex) {}
window.wcs_add = {};
window.wcs_do = function() { };
}
catch (ex) { console.error('Error executing AG js: ' + ex); }
/* END SCRIPT GRAFIK */
</script>
