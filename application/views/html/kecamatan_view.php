<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 foreach($kecamatan_view as $s):
    $idkab=$s->idkabupaten_kota;
 endforeach;
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php include "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Kecamatan <small class="page-title-tag">Daftar Kecamatan</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?= base_url(); ?>">Home</a><i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Daftar Kecamatan
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="k2b_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action=""  class="form-horizontal" method="post" id="k2b_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title uppercase"><strong>Kecamatan</strong></h5>
            </div>
            <div class="modal-body">
              <table id="user" class="table table-bordered table-striped form-1">
                <tbody>
                  <tr>
                    <td><strong>Kode</strong></td>
                    <td align="center">
                      <input name="idkabupaten_kota" id="idkabupaten_kota" type="hidden" class="form-control" value="<?=$idkab;?>"/>
                      <input name="idkecamatan" id="idkecamatan" type="hidden" class="form-control"/>
                      <input name="kode" id="kode" type="text" class="form-control" style="background-color:#E8F3FF"/>
                      <p id="val_kode"></p>
                    </td>
                  </tr>
                  <tr>
                    <td><strong>Kecamatan</strong></td>
                    <td align="center"><input name="Kecamatan_1" id="Kecamatan_1" type="text" class="form-control" style="background-color:#E8F3FF"/> </td>
                    <p id="val_Kecamatan_1"></p>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn green" onclick="val_k2()">Simpan</button>
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
                                            <a class="btn btn-sm green" data-toggle="modal" href="#k2b_modal" onClick="tambah_k2_b_modal()"> Tambah Data </a>
										</div>
									</div>
									<div class="col-md-6">
										<div class="btn-group pull-right">
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
											</button>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													Print </a>
												</li>
												<li>
													<a href="javascript:;">
													Save as PDF </a>
												</li>
												<li>
													<a href="javascript:;">
													Export to Excel </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th class="table-checkbox">
									#
								</th>
								<th width="50">
									 Kode
								</th>
								<th>
									 Kecamatan
								</th>
								<th >Ubah</th>
								<th >Hapus</th>
								<th style="display:none;"></th>
							</tr>
							</thead>
							<tbody>
                            <?php $i=0; foreach($kecamatan_view as $r): $i++;?>
							<tr class="odd gradeX">
								<td>
									<?php echo $i;?>
								</td>
								<td>
									 <?php echo $r->kode;?>
								</td>
								<td>
									 <?php echo $r->kecamatan;?>
								</td>
                <td align="center">
                   <a class="btn default btn-sm blue-hoki" data-toggle="modal" href="#k2b_modal" onClick="<?php echo "ubah_k2_b_modal('".$r->kode."','".$r->kecamatan."','".$r->idkecamatan."')";?>")>Ubah</a>
                </td>
								<td><a class="btn default btn-sm red-sunglo" href="javascript:confirmDelete(<?=$r->idkecamatan;?>)"> Hapus </a></td>
								<td style="display:none;"></td>
							</tr>
              <?php endforeach; ?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "footer2.php"; ?>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
   TableManaged.init();
});
</script>
<script type="text/javascript">
function confirmDelete(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_kecamatan');?>/"+delUrl;
  }
}
function tambah_k2_b_modal()
{
	  document.getElementById('kode').value="";
	  document.getElementById('Kecamatan_1').value="";
	  document.getElementById('idkecamatan').value="";
        document.getElementById('k2b_form').action="<?php echo base_url('main/kecamatan_insert');?>";
}
function ubah_k2_b_modal(a,b,c)
{
	  document.getElementById('kode').value=a;
	  document.getElementById('Kecamatan_1').value=b;
	  document.getElementById('idkecamatan').value=c;
        document.getElementById('k2b_form').action="<?php echo base_url('main/kecamatan_update');?>";
}
function val_k2() {
    var kode,Kecamatan_1, text;

    kode = document.getElementById("kode").value;
    Kecamatan_1 = document.getElementById("Kecamatan_1").value;

    // If x is Not a Number or less than one or greater than 10
    //if (isNaN(x) || x < 1 || x > 10) {
    if ((kode=="")) {
        text = "Masukan kode";
        document.getElementById("val_kode").innerHTML = text;
    } else if ((Kecamatan_1=="")) {
        text = "Masukan Kecamatan";
        document.getElementById("val_Kecamatan_1").innerHTML = text;
    }else {
        document.getElementById("k2b_form").submit();
		document.getElementById("val_kode").innerHTML = "";
		document.getElementById("val_Kecamatan_1").innerHTML = "";
    }
}
</script>
</body>
<!-- END BODY -->
</html>
