<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" class="img-circle" src="../../assets/admin/layout3/img/avatar9.jpg">
						<span class="username username-hide-mobile"><?php echo $name; ?></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">      
							<li>
								<a href="<?php echo base_url("/login/logout_user");?>"> 
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>