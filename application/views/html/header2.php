<script>
    function val_profile_form() {
        var kada, wakada;
        kada = document.getElementById("kada").value;
        wakada = document.getElementById("wakada").value;
        //if (isNaN(x) || x < 1 || x > 10) {
        if ((kada == "")) {
            document.getElementById("val_kada").innerHTML = "Masukan Nama Kepala Daerah";
        } else if ((wakada == "")) {
            document.getElementById("val_wakada").innerHTML = "Masukan Nama Wakil Kepala Daerah";
        } else {
            document.getElementById("frofile_form").submit();
            document.getElementById("val_kada").innerHTML = "";
            document.getElementById("val_wakada").innerHTML = "";
            document.getElementById("val_letak_geografi").innerHTML = "";
        }
    }

    $("#provinsi_filter").change(function () {
        var url = "<?php echo site_url('main/add_ajax_kab');?>/" + $(this).val();
        $('#kabupaten_filter').load(url);
        return false;
    });
    $("#provinsi_filter2").change(function () {
        var url = "<?php echo site_url('main/add_ajax_kab');?>/" + $(this).val();
        $('#kabupaten_profile_filter').load(url);
        return false;
    });

    function val_filter_kab() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_filter").selectedIndex;
        if ((idkabupaten_kota_select == 0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab").submit();
        }
    }

    function val_fkabprov() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_kota_filter").selectedIndex;
        if ((idkabupaten_kota_select == 0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab2").submit();
        }
    }

    function sel_prov(a) {
        document.getElementById("pilih_prov_text").value = a;
        document.getElementById("pilih_prov").submit();
    }

    function val_filter_kab2() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("provinsi_filter2").selectedIndex;
        if ((idkabupaten_kota_select == 0)) {
            document.getElementById("val_idkabupaten_kota_select2").innerHTML = "Pilih Provinsi";
        } else {
            document.getElementById("form_filter_profile").submit();
        }
    }

</script>
<?php
if ($privilage == "1") {
    foreach ($dinas_view_all as $dv):
        $logo = $dv->logo_pusat;
        foreach ($dinas_view as $dvx):
            if (isset($dvx->logo_prov)) {
                $logop = ($dvx->logo_prov);
            } else {
                $logop = ($dvx->logo_kabkot);
            }
            $email_pesan = ($dvx->email);
        endforeach;
        if ($privilage == "3") {
            foreach ($dinas_view2 as $dvx):
                $logop = $dvx->logo_kabkot;
                $email_pesan = ($dvx->email);
            endforeach;
        }
        $nama = $dv->Name;
        $wilayah = "";
    endforeach;
    $wilayah = 'Pusat';
    $menu_url = 'Pusat';
    $menu_ttl = 'Data Pusat';
} else
    if ($privilage == 2) {
        foreach ($dinas_view as $dv):
            $nama = $dv->nama_dinas;
            $wilayah = $dv->provinsi;
            $email_pesan = ($dv->email);
        endforeach;
        foreach ($dinas_view2 as $dv2):
            $logo = $dv2->logo_kabkot;
        endforeach;
        if (strlen($this->input->post('kabupaten_kota_filter')) != 0) {
            foreach ($dinas_viewx as $dv):
                $logop = $dv->logo_prov;
                $logo = $dv->logo_prov;
            endforeach;
        }
        $menu_url = 'kabupaten_kota';
        $menu_ttl = 'Data Kabupaten/Kota';

    } elseif ($privilage == 3) {
        foreach ($dinas_view2 as $dv2):
            $logo = $dv2->logo_kabkot;
            $nama = $dv2->nama_dinas;
            $wilayah = $dv2->kabupaten_kota;
            $email_pesan = ($dv2->email);
        endforeach;
        $menu_url = 'kecamatan';
        $menu_ttl = 'Data Kecamatan';
    }
/* root file avatar di server */
if (isset($logo)) {
    $avatar_url = site_url("assets/global/img/logo/" . $logo);
    $avatar = site_url("assets/global/img/logo/" . $logo);
} else {
    $holdersize = '35x35';
    $avatar = "holder.js/" . $holdersize . "?text=logo"; /* tampilkan holder */
}
/* cek file avatar di server */
?>

<div id="reset_login" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <form action="<?php echo base_url('main/reset_pengguna'); ?>" class="form-horizontal" method="post"
          id="reset_pengguna_form"/>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <input name="iddinas_reset" id="iddinas_reset" type="hidden" class="form-control"/>
                <input type="hidden" id="provinsi_select1" name="provinsi_select1"
                       value="<?php echo $provinsi_select; ?>">
                <h5 class="modal-title">Reset Sesi Login</h5>
            </div>
            <div class="modal-body">
                Reset Sesi Login?
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn green">OK</button>
                <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
        </div>
    </div>
    </form>
</div>
<div id="daftar_pengguna" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <form action="" class="form-horizontal" method="post" id="ubah_pengguna_form"/>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">Daftar Pengguna Semua Dinas</h5>
            </div>
            <div class="modal-body">
                <table id="user" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td style="width:40%" align="center">Nama Dinas</td>
                        <td align="center">
                            <input name="iddinas_pengguna" id="iddinas_pengguna" type="hidden" class="form-control"/>
                            <input type="hidden" id="provinsi_select2" name="provinsi_select2"
                                   value="<?php echo $provinsi_select; ?>">
                            <input id="nama_dinas" name="nama_dinas" type="text" class="form-control" disabled/>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:40%" align="center">Alamat User</td>
                        <td align="center">
                            <input id="alamat_user" name="alamat_user" type="text" class="form-control"/>
                            <p id="val_alamat_user"></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:40%" align="center">Telepon User</td>
                        <td align="center">
                            <input id="telepon_user" name="telepon_user" type="text" class="form-control"/>
                            <p id="val_telepon_user"></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:40%" align="center">Email User</td>
                        <td align="center">
                            <input id="email_pengguna" name="email_user" type="text" class="form-control"/>
                            <p id="val_email_pengguna"></p>
                        </td>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" onclick="val_pengguna()">OK</button>
                <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
        </div>
    </div>
    </form>
</div>
<div id="ubah_password_daftar_pengguna" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <form action="<?= BASE_URL('main/ubah_password_daftar_pengguna'); ?>" class="form-horizontal" method="post"
          id="form_password_daftar_pengguna"/>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">Ubah Pasword Daftar Pengguna</h5>
            </div>
            <div class="modal-body">
                <table id="user" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td style="width:40%" align="center">Password Baru</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input id="password_baru_user" name="password_baru_user" type="text" class="form-control"/>
                            <input type="hidden" id="provinsi_select3" name="provinsi_select3"
                                   value="<?php echo $provinsi_select; ?>">
                            <input type="hidden" id="iddinasx" name="iddinasx">
                            <p id="val_pass_user"></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" onclick="val_password_baru()">OK</button>
                <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
        </div>
    </div>
    </form>
</div>
<div id="profile_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <form action="<?= BASE_URL('main/profil'); ?>" class="form-horizontal" method="post"/>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">Daftar Profil Provinsi</h5>
            </div>
            <div class="modal-body">
                <table id="user" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td style="width:40%" align="center">Provinsi</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <select class="form-control form-control-solid placeholder-no-fix" autocomplete="off"
                                    id="provinsi_profil" name="provinsi_profil"/>
                            <?php foreach ($provinsi_form_1a as $rx): ?>
                                <option value="<?php echo $rx->idprovinsi; ?>"><?php echo $rx->provinsi; ?></option>
                            <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn green">OK</button>
                <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
        </div>
    </div>
    </form>
</div>
<div id="f1a_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <form action="<?= BASE_URL('main/form_1a_view'); ?>" class="form-horizontal" method="post" id="form_filter_kab_a"/>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">Form 1 A</h5>
            </div>
            <div class="modal-body">
                <table id="user" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td align="center">
                            <label class="control-label visible-ie8 visible-ie9">Provinsi</label>
                            <select class="form-control form-control-solid placeholder-no-fix" autocomplete="off"
                                    id="provinsi_form_1a" placeholder="provinsi_filter" name="provinsi_form_1a"/>
                            <?php foreach ($provinsi_diagram as $r): ?>
                                <option value="<?php echo $r->idprovinsi; ?>"><?php echo $r->provinsi; ?></option>
                            <?php endforeach; ?>
                            </select>

                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn green">OK</button>
                <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
        </div>
    </div>
    </form>
</div>
<div id="f1b_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <form action="<?= BASE_URL('main/form_1b_view'); ?>" class="form-horizontal" method="post" id="form_filter_kab"/>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">Form 1 B provinsi</h5>
            </div>
            <div class="modal-body">
                <table id="user" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td align="center">
                            <label class="control-label visible-ie8 visible-ie9">Provinsi</label>
                            <select class="form-control form-control-solid placeholder-no-fix" autocomplete="off"
                                    id="provinsi_filter" placeholder="provinsi_filter" name="provinsi_filter"/>
                            <option value="">Provinsi</option>
                            <?php foreach ($provinsi_diagram as $r): ?>
                                <option value="<?php echo $r->idprovinsi; ?>"><?php echo $r->provinsi; ?></option>
                            <?php endforeach; ?>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <label class="control-label visible-ie8 visible-ie9">Kabupaten/Kota</label>
                            <select class="form-control form-control-solid placeholder-no-fix" autocomplete="off"
                                    id="kabupaten_filter" placeholder="kabupaten_kota" name="kabupaten_filter"/>
                            <option value="">Kabupaten/Kota</option>
                            <?php foreach ($kabupaten_kota_p as $r): ?>
                                <option value="<?php echo $r->idkabupaten_kota; ?>"><?php echo $r->kabupaten_kota; ?></option>
                            <?php endforeach; ?>
                            </select>
                            <div id="val_idkabupaten_kota_select"></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" onclick="val_filter_kab()">OK</button>
                <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
        </div>
    </div>
    </form>
</div>

<div id="f1b2_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <form action="<?= BASE_URL('main/form_1b_view'); ?>" class="form-horizontal" method="post" id="form_filter_kab2"/>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">Form 1 B Provinsi</h5>
            </div>
            <div class="modal-body">
                <table id="user" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td align="center">
                            <label class="control-label visible-ie8 visible-ie9">Kabupaten / Kota</label>
                            <select class="form-control form-control-solid placeholder-no-fix" autocomplete="off"
                                    id="kabupaten_kota_filter" placeholder="kabupaten_kota_filter"
                                    name="kabupaten_kota_filter"/>
                            <option value="">Kabupaten Kota</option>
                            <?php foreach ($kab_matrix as $r): ?>
                                <option value="<?php echo $r->idkabupaten_kota; ?>"><?php echo $r->kabupaten_kota; ?></option>
                            <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" onClick="val_fkabprov()" class="btn green">OK</button>
                <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
        </div>
    </div>
    </form>
</div>
<div id="profile_daerah_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <form action="<?= BASE_URL('main/dinas_view'); ?>" class="form-horizontal" method="post" id="form_filter_profile"/>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h5 class="modal-title">Profil Daerah</h5>
            </div>
            <div class="modal-body">
                <table id="user" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td align="center">
                            <label class="control-label visible-ie8 visible-ie9">Provinsi</label>
                            <select class="form-control form-control-solid placeholder-no-fix" autocomplete="off"
                                    id="provinsi_filter2" placeholder="provinsi_filter2" name="provinsi_filter2"/>
                            <option value="">Provinsi</option>
                            <?php foreach ($provinsi_diagram as $r): ?>
                                <option value="<?php echo $r->idprovinsi; ?>"><?php echo $r->provinsi; ?></option>
                            <?php endforeach; ?>
                            </select>
                            <div id="val_idkabupaten_kota_select2"></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <label class="control-label visible-ie8 visible-ie9">Kabupaten/Kota</label>
                            <select class="form-control form-control-solid placeholder-no-fix" autocomplete="off"
                                    id="kabupaten_profile_filter" placeholder="kabupaten_kota"
                                    name="kabupaten_profile_filter"/>
                            <option value="">Kabupaten/Kota</option>
                            <?php foreach ($kabupaten_kota as $r): ?>
                                <option value="<?php echo $r->idkabupaten_kota; ?>"><?php echo $r->kabupaten_kota; ?></option>
                            <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" onclick="val_filter_kab2()">OK</button>
                <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
        </div>
    </div>
    </form>
</div>
<div class="page-header">
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
        <span class="pull-left"><a href="<?php echo BASE_URL(); ?>">
            <img src="../../../assets/global/img/empty.png" alt="Logo PUPR" class="logo-default">
          </a></span>
                <span style="pull-right">
          <section class="site-title">e-BASISDATA PERUMAHAN</section>
          <section class="text-header-kementerian">Sistem Informasi Basisdata Perumahan Indonesia</section>
        </span>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
               <span class="username username-hide-mobile">
                  <?php if (isset($nama)) {
                      echo $nama . '<br>' . $wilayah;
                  } ?>
                </span>
                            <img alt="Logo <?php if (isset($nama)) {
                                echo $wilayah;
                            } ?>" class="avatar" src="<?= $avatar; ?>">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo base_url("main/pengguna_dinas"); ?>"> <i class="icon-user"></i>
                                    Profil Pengguna </a>
                            </li>
                            <li>
                                <?php if ($privilage == "1") { ?>
                                    <a href="#profile_daerah_modal" data-toggle="modal"> <i class="icon-pointer"></i>
                                        Data Dinas </a>
                                <?php } else { ?>
                                    <a href="<?php echo base_url("main/dinas_view"); ?>"> <i class="icon-briefcase"></i>
                                        Data Dinas </a>
                                <?php } ?>
                            </li>
                            <li>
                                <?php if ($privilage == "1") { ?>
                                    <a href="<?php echo base_url("users/provinsi"); ?>" data-toggle="modal"> <i
                                                class="icon-pointer"></i> Daftar Pengguna </a>
                                <?php } ?>
                            </li>

                            <?php if ($privilage != "1"): ?>
                            <li>
                                <a href="<?php echo base_url("main/" . $menu_url . ""); ?>"> <i
                                            class="icon-pointer"></i> <?= $menu_ttl; ?> </a>
                            </li>
                            <?php endif; ?>
                            
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo base_url("/login/logout_user/" . $privilage . "/" . $user_id); ?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN
          <li class="droddown dropdown-separator">
            <span class="separator"></span>
          </li>
            BEGIN NOTIFICATION DROPDOWN 
          <li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="icon-bell"></i> <span class="badge badge-default"></span></a>
            <ul class="dropdown-menu">
              <li class="external">
                <h3>Terdapat <strong>12 progres </strong>pekerjaan</h3>
                <a href="javascript:;">Lihat Semua</a>
              </li>
              <li>
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="dropdown-menu-list scroller" style="height: 250px; overflow: hidden; width: auto;" data-handle-color="#637283" data-initialized="1">
                  <li>
                    <a href="javascript:;"><span class="time">Baru saja</span><span class="details"><span class="label label-sm label-icon label-success"> <i class="fa fa-plus"></i> </span> Form 1A, <?php if (isset($nama_dinas)) {
                        echo $nama_dinas;
                    } ?></span></a>
                  </li>
                </ul><div class="slimScrollBar" style="background: rgb(99, 114, 131); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
              </li>
            </ul>
          </li>-->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <div class="page-header-menu">
        <div class="container">

            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN MEGA MENU -->
            <?php require_once "menu.php"; ?>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
<script src="../../../assets/global/plugins/holder/holder.min.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        Demo.init(); // init demo(theme settings page)
        Index.init(); // init index page
        Tasks.initDashboardWidget(); // init tash dashboard widget
    });
</script>
 