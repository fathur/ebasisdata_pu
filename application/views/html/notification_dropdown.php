<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="icon-bell"></i>
						<span class="badge badge-default"><?php $query = $this->db->query('SELECT * FROM task_notification');
 					echo $query->num_rows();  ?></span>
						</a>
						<ul class="dropdown-menu">
							<li class="external">
								<h3>Status Form <strong><?php $query = $this->db->query('SELECT * FROM task_notification');
 					echo $query->num_rows();  ?> pending</strong> tasks</h3>
								<a href="menu/notification">view all</a>
							</li>
							<li>
								<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    <?php foreach($notification as $r): ?>
									<li>
										<a href="<?php echo $r->link; ?>">
										<span class="time"><?php echo $r->waktu; ?></span>
										<span class="details">
										<span class="label label-sm label-icon label-success">
										<i class="fa fa-plus"></i>
										</span>
										<?php echo $r->kind; ?> </span>
										</a>
									</li>
                                    <?php endforeach; ?>
								</ul> 
							</li>
						</ul>
					</li>