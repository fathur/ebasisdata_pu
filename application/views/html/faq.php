<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php require_once "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>FAQ <small class="page-title-tag">hal yang sering ditanyakan</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?php echo BASE_URL(); ?>">Dashboard</a> <i class="fa fa-2x fa-angle-right"></i>
				</li>
				<li class="active">
					 FAQ
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="container">
					<div class="portlet light ">
						<div class="row margin-top-10">
							<div class="col-md-3">
								<ul class="ver-inline-menu tabbable margin-bottom-10">
									<li class="active">
										<a data-toggle="tab" href="#tab_1">
										<i class="fa fa-question-circle"></i> Pertanyaan Umum </a>
										<span class="after"></span>
									</li>
									<li>
										<a data-toggle="tab" href="#tab_2">
										<i class="fa fa-users"></i>	Pengguna </a>
									</li>
									<li>
										<a data-toggle="tab" href="#tab_3">
										<i class="fa fa-briefcase"></i> Profil </a>
									</li>
									<li>
										<a data-toggle="tab" href="#tab_4">
										<i class="fa fa-database"></i> Formulir </a>
									</li>
									<li>
										<a data-toggle="tab" href="#tab_5">
										<i class="fa fa-book"></i> Laporan </a>
									</li>
									<li>
										<a data-toggle="tab" href="#tab_6">
										<i class="fa fa-file-text"></i> Syarat & Ketentuan </a>
									</li>
								</ul>
							</div>
							<div class="col-md-9">
								<div class="tab-content">
									<div id="tab_1" class="tab-pane active">
										<div id="accordion1" class="panel-group">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1">
													1. Apa itu e-Basisdata Perumahan? </a>
													</h4>
												</div>
												<div id="accordion1_1" class="panel-collapse collapse in">
													<div class="panel-body">
													Sistem Informasi eBasisdata-Perumahan adalah suatu aplikasi berbasiskan web (online)
													yang dikembangkan oleh Pusdatin Direktorat Jenderal Penyediaan Perumahan Kementerian Pekerjaan
													Umum dan Perumahan Rakyat. Keterangan lebih rinci dapat dilihat pada tautan
													<a href="http://www.basisdata-perumahan.com/main/tentang" target="_blank">berikut ini</a>.</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2">
													2. Seperti apa spesifikasi perangkat yang dibutuhkan untuk menjalankan aplikasi ini? </a>
													</h4>
												</div>
												<div id="accordion1_2" class="panel-collapse collapse">
													<div class="panel-body">
														 <div class="portlet gren col-md-6">
															 	<div class="portlet-title"><div class="caption"><h5 class="bold">Perangkat Keras</h5></div></div>
																<div class="portlet-body">
																	<ul>
																		<li>Komputer Personal</li>
																		<li>Notebook/Netbook</li>
																		<li>Tablet/Smartphone</li>
																	</ul>
																</div>
														 </div>
 														 <div class="portlet gren col-md-6">
 															 	<div class="portlet-title"><div class="caption"><h5 class="bold">Perangkat Lunak</h5></div></div>
 																<div class="portlet-body">
																	<ul>
																		<li>Operasi Sistem: MS Windows 7/8/10, MacOS, Android</li>
																		<li>Peramban (<i>browser</i>): Google Chrome (direkomendasikan), Mozilla Firefox, Internet Explorer 10+, Opera, Safari</li>
																	</ul>
																</div>
 														 </div>
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3">
													3. Apakah aplikasi ini dapat dioperasikan secara offline (tanpa koneksi internet)? </a>
													</h4>
												</div>
												<div id="accordion1_3" class="panel-collapse collapse">
													<div class="panel-body">
														 Tidak. Mengingat aplikasi ini berbasiskan web (online), maka untuk dapat menggunakannya mutlak
														 diperlukan koneksi internet.
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="tab_2" class="tab-pane">
										<div id="accordion2" class="panel-group">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1">
													1. Siapakah saja pengguna aplikasi e-Basisdata Perumahan? </a>
													</h4>
												</div>
												<div id="accordion2_1" class="panel-collapse collapse in">
													<div class="panel-body">
														Admin pusat di Dirjen Penyediaan Perumahan KemenPUPR dan personil yang ditunjuk pada tiap-tiap
														satuan kerja PKP (perumahan dan kawasan permukiman) di tingkat provinsi dan kabupaten/kota.
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_2">
													2. Siapa yang dapat memanfaatkan informasi yang ada di dalam e-Basisdata Perumahan? </a>
													</h4>
												</div>
												<div id="accordion2_2" class="panel-collapse collapse">
													<div class="panel-body">
														Informasi di dalam aplikasi ini akan menjadi dasar bagi Direktorat Jenderal Penyediaan Perumahan
														Kementerian PUPR untuk merencanakan dan menetapkan program terkait penyediaan perumahan.
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_3">
													3. Siapa yang mem-validasi atau menyetujui informasi yang telah diinput dalam formulir pendataan? </a>
													</h4>
												</div>
												<div id="accordion2_3" class="panel-collapse collapse">
													<div class="panel-body">
														Kepala Dinas atau Kepala Satker.
													</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_4">
													4. Bagaimana prosedur validasi atau penyetujuan formulir pendataan? </a>
													</h4>
												</div>
												<div id="accordion2_4" class="panel-collapse collapse">
													<div class="panel-body">
														Langkah-langkahnya adalah, sebagai berikut:
														<ol>
															<li>Cetak (<i>print</i>) terlebih dahulu form dalam kertas A4,</li>
															<li>Serahkan hasil cetakannya pada kepada personal yang memiliki otoritas, untuk diperiksa dan ditandatangani.</li>
															<li>Selanjutnya halaman penyetujuan (halaman yang terdapat tandatangan) dipindai (<i>scan</i>) ke dalam format gambar (jpg, jpeg & png) atau pdf.</li>
														  <li>Terakhir, file hasil pindai tersebut diunggah via menu yang telah ditentukan (muncul pada saat seluruh bagian formulir terisi).</li>
															<li>Jika langkah sebelumnya berhasil, maka status input akan berubah dari 'draft' menjadi 'disetujui'.
														</ol>
														<div class="note note-warning">
														Perhatian: Proses unggah pada langkah nomor 4 hanya dapat dilakukan sekali untuk setiap form input, oleh karenanya harus dipastikan
														dengan cermat bahwa file yang diunggah sudah benar.</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="tab_3" class="tab-pane">
										<div id="accordion3" class="panel-group">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_3">
													Apakah yang tertera dalam profil? </a>
													</h4>
												</div>
												<div id="accordion3_3" class="panel-collapse collapse">
													<div class="panel-body">
														 Profil daerah pada halaman muka formulir 1A dan 1B adalah gambaran umum sektor perumahan di suatu daerah.
													</div>
												</div>
											</div> 
										</div>
									</div>
									<div id="tab_4" class="tab-pane">
										<div id="accordion4" class="panel-group">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#accordion4_1">
													1. Apakah formulir 1A itu? </a>
													</h4>
												</div>
												<div id="accordion4_1" class="panel-collapse collapse">
													<div class="panel-body">
														 Formulir isian basis data perumahan untuk Provinsi (UU 1/2011 pasal 17).
													</div>
												</div> 
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion5" href="#accordion5_1">
													2. Apakah formulir 1B itu? </a>
													</h4>
												</div>
												<div id="accordion5_1" class="panel-collapse collapse">
													<div class="panel-body">
														 Formulir isian basis data perumahan untuk Kab/Kota (UU 1/2011 pasal 18).
													</div>
												</div>  
												<div id="accordion7_1" class="panel-collapse collapse">
													<div class="panel-body">
														 Formulir isian basis data perumahan untuk Kab/Kota (UU 1/2011 pasal 18).
													</div>
												</div>  
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion8" href="#accordion8_1">
													5. Apakah formulir IMB itu? </a>
													</h4>
												</div>
												<div id="accordion8_1" class="panel-collapse collapse">
													<div class="panel-body">
														 Formulir isian data dukungan untuk basis data perumahan dari sisi suplai unit rumah yang didata oleh Dinas Perijinan di Kab/Kota.
													</div>
												</div>  
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion9" href="#accordion9_1">
													6. Apakah formulir non IMB itu? </a>
													</h4>
												</div>
												<div id="accordion9_1" class="panel-collapse collapse">
													<div class="panel-body">
														 Formulir isian data dukungan untuk basis data perumahan dari sisi suplai unit rumah yang dianalisis oleh Dinas PKP Kab/Kota.
													</div>
												</div>  
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion10" href="#accordion10_1">
													7. Apakah formulir unit KPR perbankan? </a>
													</h4>
												</div>
												<div id="accordion10_1" class="panel-collapse collapse">
													<div class="panel-body">
														 Formulir isian data dukungan untuk basis data perumahan dari sisi suplai unit rumah yang melalui pengajuan KPR di Bank, diisi oleh perwakilan Bank di provinsi, kemudian data ini di sinkronkan dengan data IMB juga data non IMB supaya tidak terjadi double-counting.
													</div>
												</div> 
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion11" href="#accordion11_1">
													8. Apakah formulir unit pembangunan pengembang? </a>
													</h4>
												</div>
												<div id="accordion11_1" class="panel-collapse collapse">
													<div class="panel-body">
														 Formulir isian data dukungan untuk basis data perumahan dari sisi suplai unit rumah yang melalui pembangunan oleh pengembang, diisi oleh perwakilan asosiasi pengembang di provinsi, kemudian data ini di sinkronkan dengan data IMB juga data non IMB supaya tidak terjadi double-counting.
													</div>
												</div> 
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion12" href="#accordion12_1">
													9. Bagaimana laporan pengisian formulir? </a>
													</h4>
												</div>
												<div id="accordion12_1" class="panel-collapse collapse">
													<div class="panel-body">
														 Laporan formulir 1A dan 1B diisi oleh staf/pejabat di dinas PKP yang memperoleh datanya dari stakeholder sumber data yang ada di wilayahnya termasuk Pokja PKP dan setelah formulir 1A dan 1B terisi ditandatangani oleh Kepala Dinas PKP, dan kemudian di upload di aplikasi.
													</div>
												</div> 
											</div> 
										</div>
									</div>
									<div id="tab_5" class="tab-pane">
										<div id="accordion5" class="panel-group">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion13" href="#accordion13_1">
													Apakah Isi dari laporan? </a>
													</h4>
												</div>
												<div id="accordion13_1" class="panel-collapse collapse">
													<div class="panel-body">
														 LAporan merupakan output dari semua form ayng dapat diakses melalui setiap formulir.
													</div>
												</div>     
											</div> 
										</div>
									</div>
									
									<div id="tab_6" class="tab-pane">
										<div id="accordion3" class="panel-group">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_3">
													Apa syarat dan ketentuan sebagai admin kab/kota? </a>
													</h4> 
													<div class="panel-body">
														 Syarat staf/pejabat yang melakukan input data dan upload formulir adalah yang ditunjuk oleh pejabat Dinas PKP Kab/Kota yang berwenang.
													</div>
													
													<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_3">
													Apa syarat dan ketentuan sebagai admin provinsi? </a>
													</h4> 
													<div class="panel-body">
														 Syarat staf/pejabat yang melakukan input data dan upload formulir adalah yang ditunjuk oleh pejabat Dinas PKP Provinsi yang berwenang.
													</div>
												</div>
											</div> 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<?php include "footer2.php"; ?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
});
</script>
<script>
    function val_profile_form() {
    var kada,wakada;
    kada = document.getElementById("kada").value;
    wakada = document.getElementById("wakada").value;
     //if (isNaN(x) || x < 1 || x > 10) {
    if ((kada=="")) {
        document.getElementById("val_kada").innerHTML = "Masukan Nama Kepala Daerah";
    } else if ((wakada=="")) {
        document.getElementById("val_wakada").innerHTML = "Masukan Nama Wakil Kepala Daerah";
    } else {
        document.getElementById("frofile_form").submit();
		    document.getElementById("val_kada").innerHTML = "";
		    document.getElementById("val_wakada").innerHTML = "";
        document.getElementById("val_letak_geografi").innerHTML = "";  
    }
}
    
    $("#provinsi_filter").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_filter').load(url);
        return false;
    });
    $("#provinsi_filter2").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_profile_filter').load(url);
        return false;
    });

    function val_filter_kab() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab").submit();
        }
    }
    
    function val_filter_kab_prov() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_kota_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab2").submit();
        }
    }
     
    function sel_prov(a) {
        document.getElementById("pilih_prov_text").value=a;
        document.getElementById("pilih_prov").submit();
    }
    
    function val_filter_kab2() {
    var idkabupaten_kota_select;
    idkabupaten_kota_select = document.getElementById("provinsi_filter2").selectedIndex;
    if ((idkabupaten_kota_select==0)) {
        document.getElementById("val_idkabupaten_kota_select2").innerHTML = "Pilih Provinsi";
    } else {
        document.getElementById("form_filter_profile").submit();
    }
}

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
