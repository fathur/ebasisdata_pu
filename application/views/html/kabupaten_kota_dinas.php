<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
    
<script type="text/javascript">
function validasi_kab_kot() {
    var kab_kot, kode_kab_kot;

    kab_kot = document.getElementById("kab_kot").value;
    kode_kab_kot = document.getElementById("kode_kab_kot").value;

    if ((kab_kot=="")) { 
        document.getElementById("val_kab_kot").innerHTML = "Masukan Kabupaten/Kota";
    } else if (isNaN(kode_kab_kot) || kode_kab_kot < 1 || kode_kab_kot =="") { 
        document.getElementById("val_kode_kab_kot").innerHTML = "Input salah";
    } else {
		    document.getElementById("val_kab_kot").innerHTML = "";
        document.getElementById("val_kode_kab_kot").innerHTML = "";
        document.getElementById("kab_kot_form").submit();
    }
}

function confirmDelete(delUrl) {
      if (confirm("Yakin akan menghapus?")) {
        document.location = "<?=BASE_URL('main/hapus_kabupaten_kota');?>/"+delUrl;
      }
    }
    
function tambah_kab_kot() { 
    document.getElementById("kab_kot").value="";
    document.getElementById("kode_kab_kot").value=""; 
    document.getElementById('kab_kot_form').action="<?php echo base_url('main/kabupaten_kota_insert');?>";
}
function ubah_kab_kot(a,b,c) {
    document.getElementById("idkabupaten_kota").value=a;
    document.getElementById("kab_kot").value=b;
    document.getElementById("kode_kab_kot").value=c; 
    document.getElementById('kab_kot_form').action="<?php echo base_url('main/kabupaten_kota_update');?>";
}
</script>
<!-- BEGIN HEADER -->
<?php require_once "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Wilayah Administratif <small class="page-title-tag">Daftar Master</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
		    <!-- modal -->
		    <div id="kabkot_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
              <form action=""  class="form-horizontal" method="post" id="kab_kot_form"/>
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h5 class="modal-title">Kabupaten / Kota</h5>
                    </div>
                    <div class="modal-body">
                      <table id="user" class="table table-bordered table-striped">
                        <tbody>
                          <tr>
                            <td style="width:40%" align="center">Kabupaten/kota</td>
                            <td style="width:60%" align="center">Kode</td>
                          </tr>
                          <tr>
                            <td align="center" >
                              <input name="idprovinsi" type="hidden" class="form-control" value="<?=$idprovinsi;?>"/> 
                              <input name="idkabupaten_kota" id="idkabupaten_kota" type="hidden" class="form-control" /> 
                              <input name="kab_kot" type="text" class="form-control" style="background-color:#E8F3FF"  id="kab_kot" />
                              <p id="val_kab_kot"></p>
                            </td>
                            <td align="center">
                              <input name="kode_kab_kot" type="text" class="form-control" style="background-color:#E8F3FF"  id="kode_kab_kot" />
                              <p id="val_kode_kab_kot"></p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn green" onclick="validasi_kab_kot()">Simpan</button>
                      <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
		    <!-- modal -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?php echo base_url('main/index'); ?>">Home</a>
					<i class="fa fa-2x fa-angle-right"></i>
				</li>
				<li class="active">
					Data Master
					<i class="fa fa-2x fa-angle-right"></i>
				</li>
				<li class="active">
					 Wilayah Administratif
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group"> 
											<a class="btn default btn-sm blue-hoki" data-toggle="modal" href="#kabkot_modal" onclick="tambah_kab_kot()"> Tambah </a> 
										</div>
									</div>
								</div>
							</div> 
							<!-- johns-->
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
                                             <?php $attributes = array('class' => 'email', 'id' => 'my_form');?>
                                            <?php echo form_open('main/rtlh_kabupaten_kota', $attributes); ?>
                                            <?php echo form_input(array('id' => 'tahun', 'name' => 'tahun', 'type' => 'hidden', 'value' => date('Y'))); ?>
											<?php echo form_close(); ?>
                                                <?php $attributes = array('class' => 'email', 'id' => 'my_form2');?>
                                            <?php echo form_open('main/rtlh_kabupaten_kota', $attributes); ?>
                                            <?php echo form_input(array('id' => 'tahun', 'name' => 'tahun', 'type' => 'hidden', 'value' => date('Y')-1)); ?>
											<?php echo form_close(); ?>
                                                <?php $attributes = array('class' => 'email', 'id' => 'my_form3');?>
                                            <?php echo form_open('main/rtlh_kabupaten_kota', $attributes); ?>
                                            <?php echo form_input(array('id' => 'tahun', 'name' => 'tahun', 'type' => 'hidden', 'value' => date('Y')-2)); ?>
											<?php echo form_close(); ?>
										</div>
									</div> 
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th>Provinsi</th>
								<th>Kode</th>
								<th>Kabupaten/Kota</th>
								<th></th>
								<th></th>
							</tr>
							</thead>
							<tbody>
                            <?php $i=0; foreach($kabupaten_kota_dinas as $r): $i++; ?>
							<tr class="odd gradeX"> 
								<td>
									 <?php echo $r->provinsi;?>
								</td>
								<td>
									<?php echo $r->kode;?>
								</td>
								<td>
									 <?php echo $r->kabupaten_kota;?>
								</td>
								<td align="center"> 
											<a class="btn grey-cascade btn-sm edit" data-toggle="modal" href="#kabkot_modal" onClick="ubah_kab_kot('<?php echo $r->idkabupaten_kota;?>','<?php echo $r->kabupaten_kota;?>','<?php echo $r->kode;?>')"> 
									Edit <i class="fa fa-edit"></i></a>
								</td>
								<td align="center"> 
									<a href="javascript:confirmDelete(<?=$r->idkabupaten_kota;?>)" class="btn red-sunglo btn-sm delete"> Hapus <i class="fa fa-trash"></i></a>
								</td>
							</tr>
                            <?php endforeach; ?>
							</tbody>
							</table> 
							<!-- johns-->
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<?php require_once "footer2.php";?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL("main/kabupaten_kota_script");?>"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
	 Layout.init(); // init current layout
	 Demo.init(); // init demo features
   TableEditable.init();
});
</script>
</body>
<!-- END BODY -->
</html>
