<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <div id="printableArea">
  
                <?php foreach($form_1_view as $r): ;?>
                    <?php $provinsi=$r->provinsi;?>
                <?php endforeach; ?>
                
					 Rekapitulasi Formulir IA <?=$provinsi;?>
				</li>
			</ul>
	 
							<table class="table table-striped table-bordered table-hover" id="sample_1">
  							<thead>
                  <tr>
                    <th width="9%" class="table-checkbox"> No. </th>
                    <th width="20%"> Tahun </th>
                    <th width="25%"> Tanggal </th>
                    <th width="18%"> Status </th> 
                  </tr>
  							</thead>
  							<tbody>
                <?php $i=0; foreach($form_1_view as $r): $i++; $r->provinsi;?>
	  							<tr class="odd gradeX">
	  								<td><?php echo $i;?></td>
	  								<td><?php echo $r->tahun;?></td>
	  								<td class="center"><?php echo $r->tanggal_buat;?></td>
	  								<td>
	                    <?php if($r->status=="Draft"){?>
	  									<span class="label label-md label-warning"> Draft </span>
	                    <?php } elseif ($r->status=="Disetujui"){?>
	  									<span class="label label-md label-success">	Disetujui </span>
	                    <?php }?>
	  								</td> 
	  							</tr>
                <?php endforeach; ?>
  							</tbody>
							</table> 
</div>
 
</body>
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<!-- END BODY -->
</html>
