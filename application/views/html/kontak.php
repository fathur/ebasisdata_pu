<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?=$title;?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php include "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Kontak Kami <small class="page-title-tag">halaman informasi kontak</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?php echo base_url('main/index'); ?>">Home</a> <i class="fa fa-2x fa-angle-right"></i>
				</li>
				<li class="active">
					 Kontak Kami
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
      <div class="portlet light margin-top-10">
				<div class="portlet-body">
					<div class="row">
						  <div class="col-md-<?php if ($privilage==1) {echo "12";}else{echo "6";}?>">
							<!-- Google Map -->
								<div id="locmap" class="gmaps margin-bottom-40" style="height:500px; border:1px solid #ccc"></div>
              </div>
              <?php if ($privilage!=1) {?>
              <div class="col-md-6">
								<!-- BEGIN FORM--> 
								<form action="<?php echo site_url('main/kirim_pesan'); ?>" name="form_pesan" id="form_pesan" class="form-horizontal" enctype="multipart/form-data" method="post" /> 
									<h3 class="form-section uppercase">Umpan Balik</h3>
									<p class="note note-info">Anda dapat mengirimkan pertanyaan, saran dan kritik, khususnya yang
                    berkaitan dengan aplikasi <b>ebasisdata-perumahan</b> melalui formulir di
                    bawah ini.
										Tanggapan/jawaban dari admin (jika ada/perlu) akan dikirim melalui e-mail.
									</p>
									<div class="form-group">
										<div class="input-icon">
											<i class="fa fa-check"></i>
											<input type="text" name="judul_pesan" id="judul_pesan" class="form-control" placeholder="Judul">
									        <div id="val_judul_pesan"></div>
										</div>
									</div>
									<div class="form-group">
										<div class="input-icon">
											<i class="fa fa-user"></i>
											<input type="text" name="namalengkap" id="namalengkap" class="form-control" placeholder="Nama" value="<?php echo $nama." ".$wilayah;?>" disabled>
											<input type="hidden" name="iddinas" id="iddinas" value="<?php echo $iddinas;?>" class="form-control" >
											<input type="hidden" name="status_pesan" id="status_pesan" value="in" class="form-control" >
										</div>
									</div>
									<div class="form-group">
										<div class="input-icon">
											<i class="fa fa-envelope"></i>
											<input type="email" name="email" class="form-control" placeholder="Email" value="<?php if (isset($email_pesan)) {echo $email_pesan;}?>" disabled>
										</div>
									</div>
									<div class="form-group">
										<textarea class="form-control" name="isipesan" id="isipesan" rows="3=8" placeholder="Isi Pesan" class="form-control" ></textarea>
									        <div id="val_isipesan"></div>
									</div>
									<button type="button" class="btn green pull-right" onclick="val_pesan()">Kirim</button> 
								</form> 
								<!-- END FORM-->
							</div>
						</div>
					</div>
						<hr>
					<div class="portlet light ">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-bubble font-red-sunglo"></i>
								<span class="caption-subject font-red-sunglo bold uppercase">Pesan</span>
							</div> 
							<div class="actions">
								<div class="portlet-input input-inline">
										<form action="<?php echo site_url('main/kontak'); ?>" name="form_cari_pesan" id="form_cari_pesan" class="form-horizontal" enctype="multipart/form-data" method="post" />
									<div class="input-icon right">
										<i class="icon-magnifier" onclick="cari_pesan()"></i>  
									</div>
									<div class="input-icon right"> 
										<input type="text" class="form-control input-circle" placeholder="Cari Judul" name="text_cari_pesan">
									</div>
										</form>
								</div>
							</div>
						</div>
						<?php if (isset($logo)){ $logo=$logo;}else{$logo="";}?>
						<div class="portlet-body" id="chats">
							<div class="scroller" style="height: 341px;" data-always-visible="1" data-rail-visible1="1">
								<ul class="chats">
								    <?php $x=0; foreach($pesan as $d):  $x++;?> 
								    <?php if ($d->status_pesan=="in"){ ?>
									<li class="in">
									    <?php if (file_exists($logo)) {
                                          $avatar = site_url("assets/global/img/logo/".$logo); /* tampilkan logo */
                                        }
                                        else {
                                          $holdersize = '35x35';
                                          $avatar = "holder.js/".$holdersize."?text=logo"; /* tampilkan holder */
                                        }?>
										<img class="avatar" alt="" src="<?=$avatar;?>"/>
										<div class="message">
											<span class="arrow">
											</span>
											<a href="javascript:;" class="name">
											<?=$nama." ".$wilayah;?> </a>
											<span class="datetime">
											<?=$d->waktu;?> [<?=$d->judul;?>]</span>
											<span class="body">
											<?=$d->pesan;?>. </span>
										</div>
									</li> 
								    <?php } else { ?>
									<li class="out">
										<img class="avatar" alt="" src="<?=site_url("assets/global/img/logo/NKRI.png");?>"/>
										<div class="message">
											<span class="arrow">
											</span>
											<a href="javascript:;" class="name">
											Pusat </a>
											<span class="datetime">
											<?=$d->waktu;?> [<?=$d->judul;?>]</span>
											<span class="body">
											<?=$d->pesan;?>. </span>
										</div>
									</li> 
									
								    <?php }   ?>
									<?php endforeach;?>
									
								</ul>
							</div>
							<?php if ($pesan_personal==1){?>
							<div class="chat-form">
								<div class="input-cont">
									<input class="form-control" type="text" placeholder="Type a message here..."/>
								</div>
								<div class="btn-cont">
									<span class="arrow">
									</span>
									<a href="" class="btn blue icn-only">
									<i class="fa fa-check icon-white"></i>
									</a>
								</div>
							</div>
							<?php }?>
						</div>
					</div> 
				</div>
                
              <?php }?>
			<!-- END PAGE CONTENT INNER -->
			<!-- BEGIN PORTLET-->
					
<!-- END CONTAINER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
	
</div>
<!-- END PAGE CONTAINER -->
<?php include "footer2.php"; ?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCjIcwb-b_wnI6MGDEj5p3oUO-hYD_PLW4" type="text/javascript"></script>
<script src="../../../assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/contact-us.js"></script>

<script type="text/javascript">
function val_pesan(){  
    if (document.getElementById("judul_pesan").value==""){
        document.getElementById("val_judul_pesan").innerHTML = "Isi Judul";
    }
    else if (document.getElementById("isipesan").value==""){
        document.getElementById("val_isipesan").innerHTML = "Isi Pesan";
    }
    else{
        document.getElementById("val_judul_pesan").innerHTML = "";
        document.getElementById("val_isipesan").innerHTML = "";
        document.getElementById("form_pesan").submit();
    }
}
function cari_pesan(){  
     
        document.getElementById("form_cari_pesan").submit(); 
}
</script>
<script>
jQuery(document).ready(function() {
    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    Demo.init(); // init demo features
    ContactUs.init();

    var map = new GMaps({
      div: '#locmap',
      lat: -6.236857,
      lng: 106.800954,
      zoom: 15  // default zoom is 15
    });
    var marker = map.addMarker({
     lat: -6.236857,
     lng: 106.800954,
     title: 'Lokasi',
       infoWindow: {
         content: '<b>Subdit Data dan Informasi (Datin)</b><br>Direktorat Perencanaan Penyediaan Perumahan<br>Kementerian PUPR'
       }
     });

     marker.infoWindow.open(map, marker);

});

</script>
<script>
    function val_profile_form() {
    var kada,wakada;
    kada = document.getElementById("kada").value;
    wakada = document.getElementById("wakada").value;
     //if (isNaN(x) || x < 1 || x > 10) {
    if ((kada=="")) {
        document.getElementById("val_kada").innerHTML = "Masukan Nama Kepala Daerah";
    } else if ((wakada=="")) {
        document.getElementById("val_wakada").innerHTML = "Masukan Nama Wakil Kepala Daerah";
    } else {
        document.getElementById("frofile_form").submit();
		    document.getElementById("val_kada").innerHTML = "";
		    document.getElementById("val_wakada").innerHTML = "";
        document.getElementById("val_letak_geografi").innerHTML = "";  
    }
}
    
    $("#provinsi_filter").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_filter').load(url);
        return false;
    });
    $("#provinsi_filter2").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_profile_filter').load(url);
        return false;
    });

    function val_filter_kab() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab").submit();
        }
    }
    
    function val_filter_kab_prov() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_kota_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab2").submit();
        }
    }
     
    function sel_prov(a) {
        document.getElementById("pilih_prov_text").value=a;
        document.getElementById("pilih_prov").submit();
    }
    
    function val_filter_kab2() {
    var idkabupaten_kota_select;
    idkabupaten_kota_select = document.getElementById("provinsi_filter2").selectedIndex;
    if ((idkabupaten_kota_select==0)) {
        document.getElementById("val_idkabupaten_kota_select2").innerHTML = "Pilih Provinsi";
    } else {
        document.getElementById("form_filter_profile").submit();
    }
}

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
