<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="../../../assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="../../../assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL('/application/views/html/leaflet/leaflet.css');?>" rel="stylesheet" />
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
<style type="text/css">
.rotate {
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    width: 1.5em;
}
.rotate div {
    -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
    -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
    -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
    filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
    -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
    margin-left: -6em;
    margin-right: -6em;
}
#table-scroll {
  overflow-x: none;
  overflow-y: auto;
  margin-top: 10px;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
 
  <!-- BEGIN HEADER -->
  <?php require "header2.php";?>
  <!-- END HEADER -->
  <!-- BEGIN PAGE CONTAINER -->
  <div class="page-container">
    <!-- BEGIN PAGE HEAD -->
    <div class="page-head">
      <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
          <h1>Dashboard <small class="page-title-tag">rangkuman data perumahan</small></h1>
        </div>
        <!-- END PAGE TITLE -->
      </div>
    </div>
    <!-- END PAGE HEAD -->
    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
      <div class="container">
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
          <li>
            <a href="<?php echo base_url('main/index'); ?>">Home</a>
            <i class="fa fa-2x fa-angle-right"></i>
          </li>
          <li class="active">
            Dashboard
          </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->

        <!-- BEGIN PAGE CONTENT INNER -->
        <!-- DATA COUNTER -->
        <div class="row margin-top-10">
          <div class="col-md-12 col-sm-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
              <!-- BEGIN DATA PORTLET-->
              
              <div class="portlet-title">
                <div class="caption" style="line-height: 21px">
              		<i class="icon-grid font-grey-gallery"></i>
              		<span class="caption-subject bold font-grey-gallery uppercase">	Status Kelengkapan Data <?php  if ($privilage==3){ echo "Provinsi ".$provinsi; }else{ echo "Nasional"; }?></span>
              	</div>
              	<div class="tools"> 
                 <form action="<?=BASE_URL('main/index');?>" id="form_tahun" class="form-horizontal" method="post">
              		 <select name='tahun' id='tahun' class="form-control" style="width: 140px" onchange="this.form.submit()">
                     <option value="<?php echo $tahun;?>"><?php echo $tahun;?></option> 
						<?php
                            $tahun_berjalan = date('Y');
                            $ta = $tahun_berjalan;
                                for($t=$ta;$t>$ta-10;$t--){ 
                                            echo "<option value=".$t.">".$t."</option>"; 
                                }
                        ?>
                     </select>
                    </form>
              	</div>
              </div>
              <div class="portlet-body">
                <div id="table-scroll">
                <?php  if ($privilage!=3){ ?>
                <table class="stat table-bordered" width="100%">
                  <tr>
                    <td width="8%" style="background: #ccc"><b>Form 1A</b></td>
                    <?php $i=0; foreach ($prov_matrix as $nmprov): $i++;?>
                    
                    <td<?php if ($nmprov->form1a==0) { echo ' class="bg-red-thunderbird" align="center">X';}else{echo ' class="bg-yellow-crusta" align="center">V';} ?></td>
                    <?php endforeach; 
					if ($i==0){ ?>
                    <?php foreach ($prov_matrix_new as $nmprov_new): ?>
                    
                    <td <?php echo ' class="bg-red-thunderbird" align="center">';?>X</td>
                    <?php endforeach; ?>
					 <?php }?>
                  </tr>
                  <tr>
                    <td style="background: #ccc"><b>Form 1B</b></td>
                    <?php $i=0; foreach ($prov_matrix as $nmprov): $i++; ?>
                    <td <?php if ($nmprov->form1b==0) { echo ' class="bg-red-thunderbird" align="center">';}elseif($nmprov->form1b<=$nmprov->jumlah_kk){echo ' class="bg-blue" align="center">'; }else{echo ' class="bg-yellow-crusta" align="center">'; }echo $nmprov->form1b."<br>(".$nmprov->jumlah_kk.")"; ?></td>
                    <?php endforeach;  
					if ($i==0){ ?>
                    <?php foreach ($prov_matrix_new as $nmprov_new): ?>
                    
                    <td <?php echo ' class="bg-red-thunderbird" align="center">';?>X</td>
                    <?php endforeach; ?>
					 <?php }?>
                  </tr>
                  <tr height="200px">
                    <td style="background: #ccc"><b>Provinsi</b></td>
                    <?php $i=0; foreach ($prov_matrix as $nmprov):$i++; ?>
                    <td class="rotate nmprov"><div <?php if ($privilage==1) {?>onclick="sel_prov('<?php echo $nmprov->idprovinsi; ?>')"<?php }?> <?php if ($pilih_prov_text==$nmprov->idprovinsi){?>style="background-color: yellow;"<?php }?>><?php echo $nmprov->provinsi; ?></div></td>
                    <?php endforeach;   
					if ($i==0){ ?>
                    <?php foreach ($prov_matrix_new as $nmprov_new):  ?>
                    
                    <td class="rotate nmprov"><div><?php echo $nmprov_new->provinsi; ?></div></td>
                    <?php endforeach; ?>
					 <?php }?>
                  </tr>
                </table>
                    <form action="<?=BASE_URL('main/index');?>" id="pilih_prov" class="form-horizontal" method="post">
                        <input type="hidden" name="pilih_prov_text" id="pilih_prov_text">
                    </form>
                <?php } echo "</br>";?>
                <table class="stat table-bordered" width="100%">
                  <tr>
                    <td style="background: #ccc"><b>Form 1B</b></td>
                    <?php foreach ($kab_matrix as $nmkab): ?>
                    <td <?php if ($nmkab->form1b==0)
                            {
                                echo ' class="bg-red-thunderbird" align="center">X';
                            }
                                else
                                {
                                    echo ' class="bg-yellow-crusta" align="center">V'; //'.$nmkab->form1b;
                                    } ?></td>
                    <?php endforeach; ?>
                  </tr>
                  <tr height="200px">
                    <td style="background: #ccc" class="rotate nmprov"><div><b>Kabupaten / Kota</b></div></td>
                    <?php foreach ($kab_matrix as $nmkab): ?>
                    <td class="rotate nmprov"><div><?php echo $nmkab->kabupaten_kota; ?></div></td>
                    <?php endforeach; ?>
                  </tr>
                </table>
                </div>
                <br>
                <div class="">
                  <table>
                    <tr>
                      <td colspan="4">STATUS:</td>
                    </tr>
                    <tr>
                      <td><div class="btn yellow-crusta"></div></td>
                      <td>&emsp;Lengkap</td>
                    </tr>
                    <?php  if ($privilage!=3){ ?>
                    <tr>
                      <td><div class="btn blue"></div></td>
                      <td>&emsp;Sebagian Lengkap</td>
                    </tr>
                    <?php }?>
                    <tr>
                      <td><div class="btn red-thunderbird"></div></td>
                      <td>&emsp;Tidak Lengkap</td>
                    </tr>
                  </table>
                </div>
              </div>
              <!-- END DATA PORTLET-->
            </div>
            <!-- END PORTLET-->
          </div>
        </div>
        <!-- END DATA COUNTER -->
        <!-- CHART BEGIN -->
        <div class="row" id="db_chart">
          <div class="col-md-12 col-sm-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
              <!-- BEGIN CHART PORTLET-->
              <?php if ($privilage==3)
                { 
                    include "grafik.php"; 
                }
                else 
                {
                    include "grafik_detail_prov.php";
                }  ?>
              <!-- END CHART PORTLET-->
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                     
                        <span class="caption-subject bold font-grey-gallery uppercase">	Provinsi</span>
						<div class="portlet-body">  
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th class="table-checkbox">
									#
								</th>
								<th width="50">
									 Provinsi
								</th>
								<th>
									 Kepemilikan
								</th>
								<th>
									 Penghunian
								</th>
								<th width="30">
									 Suplai Rumah IMB MBR
								</th>
								<th width="30">
									 Suplai Rumah IMB Non MBR
								</th>
								<th width="30">
									 Suplai Rumah Non IMB MBR
								</th>
								<th width="30">
									 Suplai Rumah Non IMB Non MBR
								</th>
								<th width="30" colspan="2">
									 RTLH
								</th> 
							</tr>
							</thead>
							<tbody>
                            <?php  
							$sum_kepemilikan=0;
							$sum_penghunian=0;
							$sum_mbr_5=0;
							$sum_non_mbr_4=0;
							$sum_mbr_7=0;
							$sum_non_mbr_6=0;
							$sum_rtlh=0; 
							$i=0; foreach($backlog_kepemilikan_dan_penghunian_view as $r): $i++;?>
							<tr class="odd gradeX">
								<td>
									<?php echo $i;?>
								</td> 
								<td>
									 <?php echo ($r->provinsi); ?>
								</td>
								<td>
									 <?php echo number_format($r->kepemilikan);$sum_kepemilikan=$sum_kepemilikan+$r->kepemilikan;?>
								</td>
								<td>
									 <?php echo number_format($r->penghunian);$sum_penghunian=$sum_penghunian+$r->penghunian;?>
								</td>
								<td>
									 <?php echo number_format($r->mbr_5);$sum_mbr_5=$sum_mbr_5+$r->mbr_5;?>
								</td>
								<td>
									 <?php echo number_format($r->non_mbr_4);$sum_non_mbr_4=$sum_non_mbr_4+$r->non_mbr_4;?>
								</td>
								<td>
									 <?php echo number_format($r->mbr_7);$sum_mbr_7=$sum_mbr_7+$r->mbr_7;?>
								</td>
								<td>
									 <?php echo number_format($r->non_mbr_6);$sum_non_mbr_6=$sum_non_mbr_6+$r->non_mbr_6;?>
								</td>
								<td class="center"> 
									 <?php echo number_format($r->rtlh);$sum_rtlh=$sum_rtlh+$r->kepemilikan;?>
								</td>  
							</tr>
                            <?php endforeach; ?>
                            <tr class="odd gradeX">
								<td>
									 
								</td> 
								<td>
									 Total
								</td>
								<td>
									 <?php echo number_format($sum_kepemilikan);?>
								</td>
								<td>
									 <?php echo number_format($sum_penghunian);?>
								</td>
								<td>
									 <?php echo number_format($sum_mbr_5);?>
								</td>
								<td>
									 <?php echo number_format($sum_non_mbr_4);?>
								</td>
								<td>
									 <?php echo number_format($sum_mbr_7);?>
								</td>
								<td>
									 <?php echo number_format($sum_non_mbr_6);?>
								</td>
								<td class="center"> 
									 <?php echo number_format($sum_rtlh);?>
								</td>  
							</tr>
							</tbody>
							</table>
						</div> 
                        <span class="caption-subject bold font-grey-gallery uppercase">	Kabupaten / Kota</span>
                        <div class="portlet-body">  
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th class="table-checkbox">
									#
								</th>
								<th width="50">
									 Kabupaten / Kota
								</th>
								<th>
									 Kepemilikan
								</th>
								<th>
									 Penghunian
								</th>
								<th width="30">
									 Suplai Rumah IMB MBR
								</th>
								<th width="30">
									 Suplai Rumah IMB Non MBR
								</th>
								<th width="30">
									 Suplai Rumah Non IMB MBR
								</th>
								<th width="30">
									 Suplai Rumah Non IMB Non MBR
								</th>
								<th width="30" colspan="2">
									 RTLH
								</th> 
							</tr>
							</thead>
							<tbody>
                            <?php 
							$sum_kepemilikan=0;
							$sum_penghunian=0;
							$sum_mbr_5=0;
							$sum_non_mbr_4=0;
							$sum_mbr_7=0;
							$sum_non_mbr_6=0;
							$sum_rtlh=0; 
							$i=0; foreach($backlog_kepemilikan_dan_penghunian_kab_view as $r): $i++;?>
							<tr class="odd gradeX">
								<td>
									<?php echo $i;?>
								</td> 
								<td>
									 <?php echo ($r->kabupaten_kota); ?>
								</td>
								<td>
									 <?php echo number_format($r->kepemilikan);$sum_kepemilikan=$sum_kepemilikan+$r->kepemilikan;?>
								</td>
								<td>
									 <?php echo number_format($r->penghunian);$sum_penghunian=$sum_penghunian+$r->penghunian;?>
								</td>
								<td>
									 <?php echo number_format($r->mbr_5);$sum_mbr_5=$sum_mbr_5+$r->mbr_5;?>
								</td>
								<td>
									 <?php echo number_format($r->non_mbr_4);$sum_non_mbr_4=$sum_non_mbr_4+$r->non_mbr_4;?>
								</td>
								<td>
									 <?php echo number_format($r->mbr_7);$sum_mbr_7=$sum_mbr_7+$r->mbr_7;?>
								</td>
								<td>
									 <?php echo number_format($r->non_mbr_6);$sum_non_mbr_6=$sum_non_mbr_6+$r->non_mbr_6;?>
								</td>
								<td class="center"> 
									 <?php echo number_format($r->rtlh);$sum_rtlh=$sum_rtlh+$r->kepemilikan;?>
								</td>  
							</tr>
                            <?php endforeach; ?>
                            <tr class="odd gradeX">
								<td>
									 
								</td> 
								<td>
									 Total
								</td>
								<td>
									 <?php echo number_format($sum_kepemilikan);?>
								</td>
								<td>
									 <?php echo number_format($sum_penghunian);?>
								</td>
								<td>
									 <?php echo number_format($sum_mbr_5);?>
								</td>
								<td>
									 <?php echo number_format($sum_non_mbr_4);?>
								</td>
								<td>
									 <?php echo number_format($sum_mbr_7);?>
								</td>
								<td>
									 <?php echo number_format($sum_non_mbr_6);?>
								</td>
								<td class="center"> 
									 <?php echo number_format($sum_rtlh);?>
								</td>  
							</tr>
							</tbody>
							</table>
						</div> 
					<!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <!-- END PORTLET-->
          </div>
        </div>
        <!-- CHART END -->
        <!-- MAP BEGIN -->
        <div class="row">
          <div class="col-md-12 col-sm-12">
              <!-- BEGIN PORTLET--> 
                    <?php if ($privilage!=3){ ?>
                <div class="portlet light "> 
                    <?php if ($provinsi_select==0){include "petaina.php";}else{include "petaina_detail.php";}?>
                </div> 
                    <?php } ?>
                <!-- BEGIN MAP PORTLET -->
                <!-- END MAP PORTLET -->
              <!-- END PORTLET-->
          </div>
        </div>
        <!-- MAP END -->
        <!-- END PAGE CONTENT INNER -->
      </div>
    </div>
    <!-- END PAGE CONTENT -->
  </div>
  <!-- END PAGE CONTAINER -->
  <?php require_once "footer2.php";?>
  <!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
  <!-- BEGIN CORE PLUGINS -->
  <script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
  <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
  <script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
  <script src="../../../assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
  <script src="../../../assets/global/plugins/bootstrap-contextmenu/bootstrap-contextmenu.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
  <script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
  <script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
  <script src="../../../assets/admin/pages/scripts/index3.js" type="text/javascript"></script>
  <script src="../../../assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
        <script src="../../../assets/global/plugins/echarts/echarts.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL SCRIPTS -->
  
  <!-- END JAVASCRIPTS -->
<!-- END BODY -->

</body>
</html>
