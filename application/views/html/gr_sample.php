<!-- BEGIN PAGE BAR -->
    <div id="morris_chart_1" style="display: none;"></div>
    <div id="morris_chart_2" style="display: none;"></div>
    <div class="portlet light portlet-fit bordered">
      <div class="portlet-body">
        <div id="morris_chart_3" style="height:500px;"></div>
                <table width="100%">
        <tr height="200px">
                    <td width="38px"></td>
                    <?php foreach ($prov_matrix as $nmprov): ?>
                    <td class="rotate nmprov" align="right"><div><?php echo $nmprov->provinsi; ?></div></td>
                    <?php endforeach; ?>
                  </tr>
                </table>
      </div>
    </div>
<!-- END : MORRIS CHARTS -->
<script src="../../../jsgrafik/jquery.min.js" type="text/javascript"></script>
<script src="../../../jsgrafik/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../jsgrafik/morris/morris.min.js" type="text/javascript"></script>
<script src="../../../jsgrafik/morris/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo BASE_URL("main/grafikmoris/");?>" type="text/javascript"></script>
