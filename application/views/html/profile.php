<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 if($privilage==2 || $privilage==1){ 
	foreach($dinas_view as $dv):
		$nawil = $dv->provinsi;
		$nadin = $dv->nama_dinas;
		$alamt = $dv->alamat;
		$telpn = $dv->telepon;
		$faxim = $dv->fax;
		$email = $dv->email;
		$wsurl = $dv->URL;
		$kada = $dv->gubernur;
		$wakada = $dv->wakil_gubernur;
	 	$logo = $dv->logo_prov;
		$label_kada = "Gubernur";
		$label_wakada = "Wakil Gubernur";
		$parent = "Kabupaten/Kota";
		$child = "Kecamatan";
		$parentcount = $kab_kot_count;
		$childcount = $kec_kot_count;
	endforeach;
}
else {
	foreach($dinas_view2 as $dv):
		$nawil = $dv->kabupaten_kota;
		$nadin = $dv->nama_dinas;
		$alamt = $dv->alamat;
		$telpn = $dv->telepon;
		$faxim = $dv->fax;
		$email = $dv->email;
		$wsurl = $dv->URL;
		$kada = $dv->kepala_daerah;
		$wakada = $dv->wakil_kepala_daerah;
	 	//$logo = $dv->logo_kabkot;
		$label_kada = "Kepala Daerah";
		$label_wakada = "Wakil Kepala Daerah";
		$parent = "Kecamatan";
		$child = "Kelurahan";
	endforeach;

	foreach($kecamatan_count as $dv):
			$parentcount = $dv->countkecamatan;
	endforeach;
	foreach($kelurahan_count as $dv):
			$childcount = $dv->countkelurahan;
	endforeach;
}
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link href="../../../assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="../../../assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php require "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div id="profile_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
              <form action="<?=BASE_URL('main/profil');?>"  class="form-horizontal" method="post"/>
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h5 class="modal-title">Daftar Profil Provinsi</h5>
                    </div>
                    <div class="modal-body">
                      <table id="user" class="table table-bordered table-striped">
                        <tbody>
                          <tr>
                            <td style="width:40%" align="center">Provinsi</td> 
                          </tr>
                          <tr>
                            <td align="center" >   
                                <select class="form-control form-control-solid placeholder-no-fix" autocomplete="off" id="provinsi_profil" name="provinsi_profil"/> 
  						        <?php foreach($provinsi_form_1a as $rx): ?>
  						        <option value="<?php echo $rx->idprovinsi;?>"><?php echo $rx->provinsi;?></option>
                                <?php endforeach; ?>
					            </select>
                            </td> 
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn green">OK</button>
                      <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div id="f1a_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
              <form action="<?=BASE_URL('main/form_1a_view');?>"  class="form-horizontal" method="post"/>
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h5 class="modal-title">Tambah Form 1 A</h5>
                    </div>
                    <div class="modal-body">
                      <table id="user" class="table table-bordered table-striped">
                        <tbody>
                          <tr>
                            <td style="width:40%" align="center">Provinsi</td> 
                          </tr>
                          <tr>
                            <td align="center" >   
                                <select class="form-control form-control-solid placeholder-no-fix" autocomplete="off" id="provinsi_form_1a" name="provinsi_form_1a"/> 
  						        <?php foreach($provinsi_form_1a as $rx): ?>
  						        <option value="<?php echo $rx->idprovinsi;?>"><?php echo $rx->provinsi;?></option>
                                <?php endforeach; ?>
					            </select>
                            </td> 
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn green">OK</button>
                      <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div id="f1b_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
              <form action="<?=BASE_URL('main/form_1b_view');?>"  class="form-horizontal" method="post" id="form_filter_kab"/>
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h5 class="modal-title">Form 1 B</h5>
                    </div>
                    <div class="modal-body">
                      <table id="user" class="table table-bordered table-striped">
                        <tbody> 
                          <tr>
                            <td align="center" >    
						<label class="control-label visible-ie8 visible-ie9">Provinsi</label>
						<select class="form-control form-control-solid placeholder-no-fix" autocomplete="off" id="provinsi_filter" placeholder="provinsi_filter" name="provinsi_filter"/>
  						<option value="">Provinsi</option>
  						<?php foreach($provinsi_diagram as $r): ?>
  						<option value="<?php echo $r->idprovinsi;?>"><?php echo $r->provinsi;?></option>
  						<?php endforeach; ?>
						</select> 
						 
                            </td> 
                          </tr> 
                          <tr>
                            <td align="center" >     
						<label class="control-label visible-ie8 visible-ie9">Kabupaten/Kota</label>
						<select class="form-control form-control-solid placeholder-no-fix" autocomplete="off" id="kabupaten_filter" placeholder="kabupaten_kota" name="kabkot_form_1b"/>
  						<option value="">Kabupaten/Kota</option>
  						<?php foreach($kabupaten_kota as $r): ?>
  						<option value="<?php echo $r->idkabupaten_kota;?>"><?php echo $r->kabupaten_kota;?></option>
              <?php endforeach; ?>
					  </select> <div id="val_idkabupaten_kota_select"></div>
                            </td> 
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn green" onclick="val_filter_kab()">OK</button>
                      <button type="button" data-dismiss="modal" class="btn default">Batal</button>
                    </div>
                  </div>
                </div>
              </form>
            </div> 
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1><?php if (isset($nawil)){ echo "";}else{echo $nawil;}?> <small class="page-title-tag">Profil Wilayah</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN modal -->
	<!-- /.modal -->
		<div id="responsive" class="modal fade bs-modal-lg" tabindex="-1" aria-hidden="true">
		    
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
			    
			    
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Perbarui Data Profil <?= $nawil;?></h4>
				</div>
				<form action="<?php echo site_url('main/update_profile'); ?>" class="form-horizontal" method="post" id="frofile_form"/>
				<div class="modal-body">
					<div class="scroller" style="height:360px" data-always-visible="1" data-rail-visible1="1">
						<div class="row">
							<div class="col-md-7">
						    <?php
						    foreach($dinas_view as $dv):?>
									<div class="form-body">
										<h3 style="border-bottom:1px solid #666">UMUM</h3>
										<div class="form-group">
											<label class="col-md-4 control-label">Nama <?php echo $label_kada ?></label>
											<div class="col-md-8">
												<input type="text" class="form-control" placeholder="Masukkan Teks" id="kada" name="nama_gubernur" value="<?=$kada;?>">
												<input type="hidden" id="idprovinsi" name="idprovinsi" value="<?=$dv->idprovinsi;?>">
												<input type="hidden" id="idkabupaten_kota" name="idkabupaten_kota" value="<?=$dv->idkabupaten_kota;?>">
												<input type="hidden" id="priv" name="priv" value="<?=$privilage;?>">
												<p id="val_kada">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">Nama <?php echo $label_wakada ?></label>
											<div class="col-md-8">
												<input type="text" class="form-control" placeholder="Masukkan Teks" id="wakada" name="nama_wakil_gubernur" value="<?=$wakada;?>">
												<p id="val_wakada">
											</div>
										</div>
										<h4 style="border-bottom:1px solid #666; margin-top:20px">LETAK GEOGRAFIS</h4> 
										<div class="form-group">
											<label class="col-md-4 control-label">LETAK GEOGRAFIS</label>
											<div class="col-md-8">
												<input type="text" class="form-control" placeholder="letak_geografis" id="letak_geografis" name="letak_geografis" value="<?=$dv->letak_geografis;?>">
											</div>
											<p id="val_letak_geografis"> 
										</div>  
										<h4 style="border-bottom:1px solid #666; margin-top:20px">LUAS WILAYAH</h4>
										<div class="form-group">
											<label class="col-md-4 control-label">Luas Wilayah Daratan</label>
											<div class="col-md-8">
												<input type="text" class="form-control" placeholder="Masukkan Angka" id="luas_wilayah_darat" name="luas_wilayah_darat" value="<?=$dv->luas_wilayah_daratan;?>">
											</div>
											<p id="val_luas_wilayah_darat">
										</div>
										<div class="form-group">
											<label class="col-md-4 control-label">Luas Wilayah Lautan</label>
											<div class="col-md-8">
												<input type="text" class="form-control" placeholder="Masukkan Angka" id="luas_wilayah_laut" name="luas_wilayah_laut" value="<?=$dv->luas_wilayah_lautan;?>">
											</div>
											<p id="val_luas_wilayah_laut">
										</div>
										<h4 style="border-bottom:1px solid #666; margin-top:20px">DEMOGRAFI</h4>
										<div class="form-group">
											<label class="col-md-5 control-label">Jumlah Penduduk</label>
											<div class="col-md-7">
												<input type="text" class="form-control col-md-11" placeholder="Masukkan Angka" id="jumlah_penduduk" name="jumlah_penduduk"  value="<?=$dv->jumlah_penduduk;?>">
											</div>
											<p id="val_demografi">
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Pertumbuhan Penduduk</label>
											<div class="col-md-7">
												<input type="text" class="form-control" placeholder="Masukkan Angka" id="pertumbuhan_penduduk" name="pertumbuhan_penduduk" value="<?=$dv->pertumbuhan_penduduk;?>">
											</div>
											<p id="val_pertumbuhan_penduduk">
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Kepadatan Penduduk</label>
											<div class="col-md-7">
												<input type="text" class="form-control" placeholder="Masukkan Angka" id="kepadatan_penduduk" name="kepadatan_penduduk" value="<?=$dv->tingkat_kepadatan_penduduk;?>">
											</div>
											<p id="val_kepadatan_penduduk">
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Jumlah Penduduk Miskin Kota</label>
											<div class="col-md-7">
												<input type="text" class="form-control" placeholder="Masukkan Angka" id="jumlah_penduduk_miskin_kota" name="jumlah_penduduk_miskin_kota" value="<?=$dv->jumlah_penduduk_miskin_kota;?>">
											</div>
											<p id="val_jumlah_penduduk_miskin_kota">
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Jumlah Penduduk Miskin Desa</label>
											<div class="col-md-7">
												<input type="text" class="form-control" placeholder="Masukkan Angka" id="jumlah_penduduk_miskin_desa" name="jumlah_penduduk_miskin_desa" value="<?=$dv->jumlah_penduduk_miskin_desa;?>">
											</div>
											<p id="val_jumlah_penduduk_miskin_desa">
										</div>
									</div>
							</div>
							<div class="col-md-5"  style="text-align:center">
								<h3 style="border-bottom:1px; text-align:center">LOGO DAERAH</h3>
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new thumbnail" style="width: 200px; height: 150px; text-align:center">
										<img src="<?=$avatar;?>" alt="Logo <?=$nawil;?>"/>
									</div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width:200px; max-height:150px; text-align:center">
									</div>
									<div style="text-align:center">
										<span class="btn default btn-file">
										<span class="fileinput-new">
										Pilih gambar </span>
										<span class="fileinput-exists">
										Ganti </span>
										<input type="file" name="...">
										</span>
										<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
										Hapus </a>
									</div>
								</div>
								<div class="clearfix margin-top-10">
									<span class="label label-danger">PERHATIAN!</span>
									Ukuran file tidak boleh lebih dari 500Kb.
								</div>
							</div>
							<?php endforeach;?>
						</div>
					</div>
				</div>
				<div class="modal-footer form-actions">
					<button type="button" data-dismiss="modal" class="btn default">Tutup</button>
          <button type="button" class="btn green" onclick="val_profile_form()">Simpan Perubahan</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- END modal -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('main/index'); ?>">Home</a>
          <i class="fa fa-2x fa-angle-right"></i>
        </li>
        <li class="active">
          Profil Wilayah
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar" style="width: 300px;">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div>
								<center>
								    <?php if($privilage==1){$lgo=$logop;} else {$lgo=$logo;} ?><img src="<?= site_url("assets/global/img/logo/".$lgo);?>" height="125px" alt="Logo <?= $nama;?>"></center>
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle" style="min-height: 100%">
								<div class="profile-usertitle-name">
								<?= $nawil;?></div>
  							<!-- STAT -->
  							<div class="row list-separated profile-stat">
  								<div class="col-md-6 col-sm-12">
  									<div class="uppercase profile-stat-title">
             					<?php echo $parentcount; ?>
  									</div>
  									<div class="uppercase profile-stat-text">
  										 <?php echo "$parent"; ?>
  									</div>
  								</div>
  								<div class="col-md-6 col-sm-12">
  									<div class="uppercase profile-stat-title">
  										 <?php echo $childcount; ?>
  									</div>
  									<div class="uppercase profile-stat-text">
  										 <?php echo "$child"; ?>
  									</div>
  								</div>
                </div>
  							<div class="row list-separated profile-stat">
  								<div class="profile-usertitle-job">
  									 <?= $nadin;?>
  								</div>
  								<div class="profile-stat-text-left">
  									<i class="fa fa-building-o icon-wrap"></i>
  									<span><?=$alamt;?><br></span>
  								</div><!--
  								<div class="profile-stat-text-left">
  									<i class="fa fa-phone-square icon-wrap"></i>
  									<span><?=$telpn;?><br></span>
  								</div>
  								<div class="profile-stat-text-left">
  									<i class="fa fa-fax icon-wrap"></i>
  									<span><?=$faxim;?><br></span>
  								</div>
  								<div class="profile-stat-text-left">
  									<i class="fa fa-envelope-square icon-wrap"></i>
  									<span><?=$email;?><br></span>
  								</div>
  								<div class="profile-stat-text-left">
  									<i class="fa fa-globe icon-wrap"></i>
  									<span><a href="<?=$wsurl;?>" target="_blank"><?=$wsurl;?></a><br></span>
  								</div>-->
                </div>
  							<div class="row list-separated profile-stat">
    							<div class="col-md-12">
    								<a data-toggle="modal" href="#responsive">
    								<i class="icon-note"></i>	Ubah Data Profil </a>
    							</div>
  							</div>
                <!-- END STAT -->
							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<!-- BEGIN PORTLET -->
								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption caption-md">
											<i class="icon-bar-chart theme-font hide"></i>
											<span class="caption-subject font-blue-madison bold uppercase">Informasi Wilayah</span>
										</div>
									</div>
									<div class="portlet-body">
										<div class="table-scrollable table-scrollable-borderless">
											<table class="table table-hover table-light table-striped">
											<tr>
												<td class="fit">1.</td>
												<td width="32%"><?php echo $label_kada ?></td>
											  <td width="3%">:</td>
												<td width="65%"><strong><?= $kada;?></strong></td>
											</tr>
											<tr>
												<td class="fit">2.</td>
												<td><?php echo $label_wakada ?></td>
												<td>:</td>
												<td><?= $wakada;?></td>
											</tr>
											<tr>
												<td class="fit">3.</td>
												<td>Letak Geografis</td>
												<td>:</td>
												<td><?= $dv->letak_geografis;?></td>
											</tr>
											<tr>
												<td class="fit">4.</td>
												<td>Total Luas Wilayah</td>
												<td>:</td>
												<td><?= number_format($dv->luas_wilayah_daratan+$dv->luas_wilayah_lautan, 2, ',', '.');?> Km<sup>2</sup></td>
											</tr>
											<tr>
											  <td class="fit"></td>
											  <td>a. Luas Daratan</td>
											  <td>:</td>
												<td><?= number_format($dv->luas_wilayah_daratan, 2, ',', '.');?> Km<sup>2</sup></td>
											</tr>
											<tr>
												<td class="fit"></td>
												<td>b. Luas Lautan</td>
												<td>:</td>
												<td><?= number_format($dv->luas_wilayah_lautan, 2, ',', '.');?> Km<sup>2</sup></td>
											</tr>
                      <tr>
												<td class="fit">5.</td>
												<td>Informasi Umum PKP</td>
											  <td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td class="fit">&nbsp;</td>
												<td>a. Jumlah Penduduk</td>
											  <td>:</td>
												<td><?= number_format($dv->jumlah_penduduk, 0, ' ', '.');?> Jiwa</td>
											</tr>
											<tr>
												<td class="fit">&nbsp;</td>
												<td>b. Pertumbuhan Penduduk</td>
												<td>:</td>
												<td><?= number_format($dv->pertumbuhan_penduduk, 0, ' ', '.');?>% per-Tahun</td>
											</tr>
											<tr>
												<td class="fit">&nbsp;</td>
												<td>c. Tingkat Kepadatan Penduduk</td>
												<td>:</td>
												<td><?= number_format($dv->tingkat_kepadatan_penduduk, 0, ' ', '.');?> Jiwa/Km<sup>2</sup></td>
											</tr>
											<tr>
												<td class="fit">&nbsp;</td>
												<td>d. Jumlah Penduduk Miskin Kota</td>
												<td>:</td>
												<td><?= number_format($dv->jumlah_penduduk_miskin_kota, 0, ' ', '.');?> Jiwa</td>
											</tr>
											<tr>
												<td class="fit">&nbsp;</td>
												<td>e. Jumlah Penduduk Miskin Desa</td>
												<td>:</td>
												<td><?= number_format($dv->jumlah_penduduk_miskin_desa, 0, ' ', '.');?> Jiwa</td>
											</tr>
											</table>
										</div>
									</div>
								</div>
								<!-- END PORTLET -->
							</div>
						</div>
					</div>
					</?php endforeach; ?>
					<!-- BEGIN profil bagian bawah -->
          <div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<!-- BEGIN PORTLET -->
								<div class="portlet light ">
									<div class="portlet-title">
										<div class="caption caption-md">
											<i class="icon-bar-chart theme-font hide"></i>
											<span class="caption-subject font-blue-madison bold uppercase">Basis Data Perumahan (Backlog berdasarkan data BKKBN, RTLH berdasarkan data TNP2K)</span>
											<span class="caption-helper hide"></span>
										</div>
									</div>
									<div class="portlet-body">
										<div class="table-scrollable table-scrollable-borderless">
											<table class="table table-hover table-light">
											<thead>
												<tr class="uppercase">
													<th colspan="2">
														 Kabupaten/Kota
													</th>
													<th style="text-align: right">
														 Backlog<br>Kepemilikan
													</th>
													<th style="text-align: right">
														 Backlog<br>Penghunian
													</th>
													<th style="text-align: right">
														 RTLH
													</th>
													<th style="text-align: right">
														 Pembangunan
													</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($backlog_kepemilikan_dan_penghunian_kab_view as $kkd): ?>
												<tr>
													<td class="fit">&nbsp;</td>
													<td width="30%">
														<a href="javascript:;" class="primary-link"><?php echo $kkd->kabupaten_kota;?></a>
												  </td>
													<td width="17%" align="right"><?php echo number_format($kkd->kepemilikan, 0, ' ', '.');?></td>
													<td width="17%" align="right"><?php echo number_format($kkd->penghunian, 0, ' ', '.');?></td>
													<td width="17%" align="right"><?php echo number_format($kkd->totalrtlh, 0, ' ', '.');?></td>
													<td width="17%" align="right"><?php echo number_format($kkd->non_mbr_4+$kkd->mbr_5+$kkd->non_mbr_6+$kkd->mbr_7, 0, ' ', '.');?></td>
												</tr>
												<?php endforeach; ?>
											</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- END PORTLET -->
							</div>
						</div>
					</div>
					<!-- END profil bagian bawah -->
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<?php include "footer2.php";?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/profile.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
  // initiate layout and plugins
  Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features\
	Profile.init(); // init page demo
});
</script>
<script>
    function val_profile_form() {
    var kada,wakada;
    kada = document.getElementById("kada").value;
    wakada = document.getElementById("wakada").value;
     //if (isNaN(x) || x < 1 || x > 10) {
    if ((kada=="")) {
        document.getElementById("val_kada").innerHTML = "Masukan Nama Kepala Daerah";
    } else if ((wakada=="")) {
        document.getElementById("val_wakada").innerHTML = "Masukan Nama Wakil Kepala Daerah";
    } else {
        document.getElementById("frofile_form").submit();
		    document.getElementById("val_kada").innerHTML = "";
		    document.getElementById("val_wakada").innerHTML = "";
        document.getElementById("val_letak_geografi").innerHTML = "";  
    }
}
    
    $("#provinsi_filter").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_filter').load(url);
        return false;
    });
    $("#provinsi_filter2").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_profile_filter').load(url);
        return false;
    });

    function val_filter_kab() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab").submit();
        }
    }
    
    function val_filter_kab_prov() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_kota_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab2").submit();
        }
    }
     
    function sel_prov(a) {
        document.getElementById("pilih_prov_text").value=a;
        document.getElementById("pilih_prov").submit();
    }
    
    function val_filter_kab2() {
    var idkabupaten_kota_select;
    idkabupaten_kota_select = document.getElementById("provinsi_filter2").selectedIndex;
    if ((idkabupaten_kota_select==0)) {
        document.getElementById("val_idkabupaten_kota_select2").innerHTML = "Pilih Provinsi";
    } else {
        document.getElementById("form_filter_profile").submit();
    }
}

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
