<div class="portlet-title" id="diagram_level">
  <div class="caption" style="line-height: 21px">
		<i class="icon-bar-chart font-grey-gallery"></i>
		<span class="caption-subject bold font-grey-gallery uppercase">
      Diagram
      <?php
      if ($nama_dinas == "Pusat"){
        echo "Nasional";
      }
      else{
        echo "Provinsi ".$provinsi;
      } 
      ?> 
    </span>
		<?php if ($privilage=="1"){?>
		<div class="actions select-diagram-pusat">
		  <form action="<?=BASE_URL('main/index')?>" class="form-horizontal" method="post" /> 
		  		<?php $query = $this->db->query('SELECT provinsi FROM provinsi where idprovinsi="'.$provinsi_select.'"');
		  		foreach ($query->result_array() as $row):
					$pr=$row['provinsi'];
		  		endforeach; ?>
				<select class="form-control form-control-solid placeholder-no-fix" autocomplete="off" id="provinsi_select" placeholder="provinsi_select" name="provinsi_select" onchange="this.form.submit()"/>
				  	<option value="0"><?php if ($provinsi_select==0){echo "Nasional";}else{ echo $pr;}?></option> 
				  	<option value="0">Nasional</option> 
					<?php foreach($provinsi_diagram as $r): ?>
					<option value="<?php echo $r->idprovinsi;?>"><?php echo $r->provinsi;?></option>
					<?php endforeach; ?>
				</select> 
			</form>
		</div>
		<?php }?>
	</div>
		<?php if ($privilage=="2"){?>
  	<div class="actions">
  		<div class="btn-group btn-group-devided" data-toggle="buttons">
  			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php if ($provinsi_select=='0') { echo 'active';}?>" onclick="level1()">
  			<input type="radio" name="options" class="toggle" id="level"/><?php echo "Nasional";?></label>
  			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php if ($provinsi_select!='0') { echo 'active';}?>" onclick="level2()">
  			<input type="radio" name="options" class="toggle" id="level"/><?php echo $provinsi;?></label>
  		</div>
  	</div>
		<?php }?>
</div>
<div class="portlet-body">
  <div class="tabbable-custom ">
    <form id="formgrafik" method="post" action="<?=BASE_URL('main/index');?>">
      <input type="hidden" id="g1" name="g1" value="<?php echo $g1;?>">
      <input type="hidden" id="g2" name="g2" value="<?php echo $g2;?>">
      <input type="hidden" id="g3" name="g3" value="<?php echo $g3;?>">
      <input type="hidden" id="g4" name="g4" value="<?php echo $g4;?>">
      <input type="hidden" id="g5" name="g5" value="<?php echo $g5;?>">
      <input type="hidden" id="kategori_1" name="kategori_1">
      <input type="hidden" id="kategori_2" name="kategori_2">
      <input type="hidden" id="tahun" name="tahun" value="<?php echo $tahun;?>" />
      <input type="hidden" id="provinsi_select" name="provinsi_select" value="<?php echo $provinsi_select;?>">
    </form>
    <ul class="nav nav-tabs">
      <li <?php if ($g2=="1"){echo 'class="active"';}?>>
        <a href="#tab12" data-toggle="tab" onclick="g2()"><i class="icon-bar-chart theme-font hide"></i>Backlog</a>
      </li>
      <li <?php if ($g3=="1"){echo 'class="active"';}?>>
        <a href="#tab11" data-toggle="tab" onclick="g3()"><i class="icon-bar-chart theme-font hide"></i>RTLH</a>
      </li>
      <li <?php if ($g4=="1"){echo 'class="active"';}?>>
        <a href="#tab11" data-toggle="tab" onclick="g4()"><i class="icon-bar-chart theme-font hide"></i>Suplai Rumah IMB</a>
      </li>
      <li <?php if ($g5=="1"){echo 'class="active"';}?>>
        <a href="#tab11" data-toggle="tab" onclick="g5()"><i class="icon-bar-chart theme-font hide"></i>Suplai Rumah Non IMB</a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab11" style="display: <?php if ($g3==1) { echo 'block'; } else { echo 'none'; }?>">
        <div id="chart_5" class="chart" style="height: 400px;"></div>
      </div>
      <div class="tab-pane" id="tab12"  style="display: <?php if ($g2==1 || $g4==1 || $g5==1) { echo 'block'; } else { echo 'none'; }?>">
        <div id="<?php  echo "echarts_bar"; ?>" style="height:400px"></div>
      </div> 
    </div>
  </div>
</div>
<!-- CORE PLUGINS -->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../../assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<?php 
if ($g2==1) { $g="2"; } 
elseif ($g3==1) { $g="1"; } 
elseif ($g4==1) { $g="4"; } 
elseif ($g5==1) { $g="5"; }?>  
        <script src="<?php echo BASE_URL("main/grafikechart_prov_det/".$provinsi_select."/".$g."/".$tahun);?>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->  
<script src="<?php if ($g3==1){ if ($provinsi_select>0){ echo BASE_URL("main/grafikd/".$provinsi_select."/".$tahun);}else{ echo BASE_URL("main/grafikd_prov/".$iddinas."/".$tahun."/".$provinsi_select);}} ?>"></script>
<script>
	jQuery(document).ready(function() {
  ChartsAmcharts.init(); // init grafik
});
/* BEGIN SCRIPT GRAFIK */
function g1()
{
        document.getElementById('g1').value="1";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function g2()
{
        document.getElementById('g2').value="1";
        document.getElementById('g1').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function g3()
{
        document.getElementById('g2').value="0";
        document.getElementById('g1').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g3').value="1";
        document.getElementById('g5').value="0";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function g4()
{
        document.getElementById('g2').value="0";
        document.getElementById('g1').value="0";
        document.getElementById('g4').value="1";
        document.getElementById('g3').value="0";
        document.getElementById('g5').value="0";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function g5()
{
        document.getElementById('g2').value="0";
        document.getElementById('g1').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g5').value="1";
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        document.getElementById('formgrafik').submit();
}
function kategori1()
{
        document.getElementById('kategori_1').value="1";
        document.getElementById('kategori_2').value="0";
        <?php if ($g2=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="1";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        <?php }?>
        <?php if ($g3=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="1";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        <?php }?>
        <?php if ($g4=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="1";
        document.getElementById('g5').value="0";
        <?php }?>
        <?php if ($g5=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="1";
        <?php }?>
        document.getElementById('formgrafik').submit();
}
function level1()
{
        document.getElementById('provinsi_select').value="0";
        document.getElementById('formgrafik').submit();
}
function level2()
{
        document.getElementById('provinsi_select').value=<?php echo $idprovinsi;?>;
        document.getElementById('formgrafik').submit();
}
function kategori2()
{
        document.getElementById('kategori_1').value="0";
        document.getElementById('kategori_2').value="1";
        <?php if ($g2=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="1";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        <?php } if ($g3=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="1";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="0";
        <?php } if ($g4=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="1";
        document.getElementById('g5').value="0";
        <?php } if ($g5=="1") { ?>
        document.getElementById('g1').value="0";
        document.getElementById('g2').value="0";
        document.getElementById('g3').value="0";
        document.getElementById('g4').value="0";
        document.getElementById('g5').value="1";
        <?php }?>
        document.getElementById('formgrafik').submit();
}
 
try {
var AG_onLoad=function(func)
{
  if(document.readyState==="complete"||document.readyState==="interactive")
    func();
  else if(document.addEventListener)document.addEventListener("DOMContentLoaded",func);
  else if(document.attachEvent)document.attachEvent("DOMContentLoaded",func)};
  var AG_removeElementById = function(id)
  {
    var element = document.getElementById(id);
    if (element && element.parentNode)
    {
      element.parentNode.removeChild(element);
    }
  };
  var AG_removeElementBySelector = function(selector)
  {
    if (!document.querySelectorAll)
    {
      return;
    }
    var nodes = document.querySelectorAll(selector);
    if (nodes)
    {
      for (var i = 0; i < nodes.length; i++)
      {
        if (nodes[i] && nodes[i].parentNode)
          {
            nodes[i].parentNode.removeChild(nodes[i]);
          }
      }
    }
  };
  var AG_each = function(selector, fn)
  {
  if (!document.querySelectorAll)
  {
    return;
  }
  var elements = document.querySelectorAll(selector);
  for (var i = 0; i < elements.length; i++)
  {
    fn(elements[i]);
  };
};
var AG_removeParent = function(el, fn)
{
  while (el && el.parentNode)
  {
    if (fn(el))
    {
      el.parentNode.removeChild(el);
      return;
    }
    el = el.parentNode;
  }
};
var AdFox_getCodeScript = function() {};
AG_onLoad(function()
{
   AG_each('iframe[id^="AdFox_iframe_"]', function(el)
   {
    if (el && el.parentNode)
    {
      el.parentNode.removeChild(el);
    }
});
});
try {
  Object.defineProperty(window, 'noAdsAtAll', { get: function() { return true; } });
}
catch (ex) {}
window.wcs_add = {};
window.wcs_do = function() { };
}
catch (ex) { console.error('Error executing AG js: ' + ex); }
/* END SCRIPT GRAFIK */
</script>
