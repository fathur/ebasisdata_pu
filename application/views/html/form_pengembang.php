<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 foreach($form_pengembang_view as $r):
     $idform_1=$r->idpengembang;  
     $pengembang=$r->pengembang; 
     $alamat=$r->alamat; 
     $asosiasi=$r->asosiasi; 
     $bulan=$r->bulan;
     $tahun=$r->tahun;
 endforeach;
?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">

<script type="text/javascript">
function tambah_imb_modal()
{
	  document.getElementById('nama_perusahaan').value="";
	  document.getElementById('alamat').value="";
	  document.getElementById('nama_pengembang').value=""; 
	  document.getElementById('luas').value="";
	  document.getElementById('tipe').value="";
	  document.getElementById('rencana').value="";
	  document.getElementById('realisasi').value="";
	  document.getElementById('harga').value="";
	  document.getElementById('idpengembang_mbr').value="";
    document.getElementById('imb_f').action="<?php echo base_url('main/fpengembang_mbr_insert');?>";
} 
function ubah_imb_modal(a,b,c,d,e,f,g,h,i,j,k) 
    {
	  document.getElementById('idkabupaten_kota').value=b;
	  document.getElementById('nama_perusahaan').value=c;
	  document.getElementById('alamat').value=d; 
	  document.getElementById('nama_pengembang').value=e;
	  document.getElementById('bentuk_rumah').value=f;
	  document.getElementById('luas').value=g;
	  document.getElementById('tipe').value=h;
	  document.getElementById('rencana').value=i;
	  document.getElementById('realisasi').value=j;
	  document.getElementById('harga').value=k;  
	  document.getElementById('idpengembang_mbr').value=a;
    document.getElementById('imb_f').action="<?php echo base_url('main/fpengembang_mbr_update');?>";
}
function tambah_imb_modal2()
{
	  document.getElementById('nama_perusahaan2').value="";
	  document.getElementById('alamat2').value="";
	  document.getElementById('pengembang2').value=""; 
	  document.getElementById('luas2').value="";
	  document.getElementById('tipe2').value="";
	  document.getElementById('rencana2').value="";
	  document.getElementById('realisasi2').value="";
	  document.getElementById('harga2').value="";
	  document.getElementById('idpengembang_mbr2').value="";
    document.getElementById('imb_f2').action="<?php echo base_url('main/fpengembang_mbr_insert2');?>";
}

function val_imb1() {
    var nama_perusahaan,alamat,nama_pengembang,luas,tipe,rencana,realisasi,harga;
    nama_perusahaan = document.getElementById("nama_perusahaan").value;
    alamat = document.getElementById("alamat").value;
    nama_pengembang = document.getElementById("nama_pengembang").value; 
    luas = document.getElementById("luas").value; 
    rencana = document.getElementById("rencana").value; 
    realisasi = document.getElementById("realisasi").value;
    harga = document.getElementById("harga").value; 
    //if (isNaN(x) || x < 1 || x > 10) {
    if ((nama_perusahaan=="")) {
        document.getElementById("val_nama_perusahaan").innerHTML = "Masukan Data nama perusahaan"; 
    } else if (alamat=="") {
        document.getElementById("val_alamat").innerHTML = "Masukan Alamat"; 
    } else if (nama_pengembang=="") {
        document.getElementById("val_nama_pengembang").innerHTML = "Masukan pengembang"; 
    } else if (isNaN(luas) || luas < 0) {
        document.getElementById("val_luas").innerHTML = "Input Salah"; 
    } else if (tipe=="") {
        document.getElementById("val_tipe").innerHTML = "Masukan Tipe"; 
    } else if (isNaN(rencana) || rencana < 0) {
        document.getElementById("val_rencana").innerHTML = "Input Salah"; 
    } else if (isNaN(realisasi) || realisasi < 0) {
        document.getElementById("val_realisasi").innerHTML = "Input Salah";  
    } else if (isNaN(harga) || harga < 0) {
        document.getElementById("val_harga").innerHTML = "Input Salah";  
	} else {
        document.getElementById("imb_f").submit();
		document.getElementById("val_nama_perusahaan").innerHTML = "";
		document.getElementById("val_alamat").innerHTML = "";
		document.getElementById("val_pengembang").innerHTML = ""; 
		document.getElementById("val_luas").innerHTML = "";
		document.getElementById("val_tipe").innerHTML = "";
		document.getElementById("val_rencana").innerHTML = "";
		document.getElementById("val_realisasi").innerHTML = "";
		document.getElementById("val_harga").innerHTML = "";
    }
}
 
function confirmDelete(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_pengembang_1');?>/"+delUrl;
  }
}
function confirmDelete2(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_pengembang_1');?>/"+delUrl;
  }
}
</script>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
 
<?php include "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Formulir Pengembang <small class="page-title-tag">Suplai Perumah</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->

	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Formulir Suplai Perumahan<i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Formulir Pengembang
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->

			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN DETAIL modal -->
					<!-- /.modal -->
					<div id="fimb_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form action="" class="form-horizontal" method="post" id="imb_f"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title"><strong>Tabel Data Perumahan untuk MBR/Rumah Umum/Subsidi<strong></h5>
										</div>
										<div class="modal-body">
											<p>
										<table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td><strong>Kabupaten / Kota</strong></td>
											<td align="center">
                                                <input name="idpengembang" id="idpengembang" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                                                <input name="idpengembang_mbr" id="idpengembang_mbr" type="hidden" class="form-control" />
                                                <select name="idkabupaten_kota" id="idkabupaten_kota" class="form-control" style="background-color:#E8F3FF"> 
                        <option value="0">Pilih Kabupaten / Kota</option>
                        <?php foreach($kabupaten_kota as $kk): ?>
                        <option value="<?php echo $kk->idkabupaten_kota;?>"><?php echo  $kk->kabupaten_kota;?></option>
  											<?php endforeach;?>
                        </select>
                                             </td>
										</tr>
                                        <tr>
											<td><strong>Nama Perusahaan</strong></td>
											<td align="center"><input name="nama_perusahaan" id="nama_perusahaan" type="text" class="form-control"  style="background-color:#E8F3FF" /><p id="val_nama_perusahaan"></p></td>
										</tr>
                                        <tr>
											<td><strong>Alamat</strong></td>
											<td align="center" ><input name="alamat" id="alamat" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_alamat"></p></td>
										</tr>
                                        <tr>
											<td><strong>Pembangunan Pengembang</strong></td>
											<td align="center" ><input name="nama_pengembang" id="nama_pengembang" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_nama_pengembang"></p></td>
										</tr> 
                                        <tr>
											<td><strong>Bentuk Rumah</strong></td>
											<td align="left">  
                                            <select name="bentuk_rumah" id="bentuk_rumah" class="form-control" style="background-color:#E8F3FF">
                                                <option value="Tapak"  style="background-color:#E8F3FF" class="form-control">Tapak</option>
                                                <option value="Susun"  style="background-color:#E8F3FF" class="form-control">Susun</option>
                                            </select>
                                            <p id="val_bentuk_rumah"></p></td>
										</tr>
                                        <tr>
											<td><strong>Luas Lahan</strong></td>
											<td align="center"><input name="luas" id="luas" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_luas"></p></td>
										</tr>
                                        <tr>
											<td><strong>Type</strong></td>
											<td align="center"><input name="tipe" id="tipe" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_tipe"></p></td>
										</tr>
                                        <tr>
											<td><strong>Rencana</strong></td>
											<td align="center"><input name="rencana" id="rencana" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_rencana"></p></td>
										</tr>
                                        <tr>
											<td><strong>Realisasi</strong></td>
											<td align="center"><input name="realisasi" id="realisasi" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_realisasi"></p></td>
										</tr>
                                        <tr>
											<td><strong>Harga</strong></td>
											<td align="center"><input name="harga" id="harga" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_harga"></p></td>
										</tr>
							  		</tbody>
									</table>
											</p>
										</div>
										<div class="modal-footer">
                      <button type="button" class="btn green" onclick="val_imb1()">Simpan</button>
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>
									</div>
								</div>
								</form>
					</div>
					<div id="fimb_modal2" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <form action="<?php echo base_url('main/fpengembang_mbr_insert2');?>" class="form-horizontal" method="post" id="imb_f2"/>
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h5 class="modal-title"><strong>Tabel Data Perumahan untuk Non-MBR/Rumah Komersial<strong></h5>
										</div>
										<div class="modal-body">
											<p>
										<table id="user" class="table table-bordered table-striped">
										<tbody>
										<tr>
											<td><strong>Kabupaten / Kota</strong></td>
											<td align="center">
                                                <input name="idpengembang2" id="idpengembang2" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                                                <input name="idpengembang_mbr2" id="idpengembang_mbr2" type="hidden" class="form-control" />
                                                <select name="idkabupaten_kota2" id="idkabupaten_kota2" class="form-control" style="background-color:#E8F3FF"> 
                        <option value="0">Pilih Kabupaten / Kota</option>
                        <?php foreach($kabupaten_kota as $kk): ?>
                        <option value="<?php echo $kk->idkabupaten_kota;?>"><?php echo  $kk->kabupaten_kota;?></option>
  											<?php endforeach;?>
                        </select>
                                             </td>
										</tr>
                                        <tr>
											<td><strong>Nama Perusahaan</strong></td>
											<td align="center"><input name="nama_perusahaan2" id="nama_perusahaan2" type="text" class="form-control"  style="background-color:#E8F3FF" /><p id="val_nama_perusahaan"></p></td>
										</tr>
                                        <tr>
											<td><strong>Alamat</strong></td>
											<td align="center" ><input name="alamat2" id="alamat2" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_alamat"></p></td>
										</tr>
                                        <tr>
											<td><strong>Pembangunan Pengembang</strong></td>
											<td align="center" ><input name="nama_pengembang2" id="nama_pengembang2" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_nama_pengembang"></p></td>
										</tr> 
                                        <tr>
											<td><strong>Bentuk Rumah</strong></td>
											<td align="left">  
                                            <select name="bentuk_rumah2" id="bentuk_rumah2" class="form-control" style="background-color:#E8F3FF">
                                                <option value="Tapak"  style="background-color:#E8F3FF" class="form-control">Tapak</option>
                                                <option value="Susun"  style="background-color:#E8F3FF" class="form-control">Susun</option>
                                            </select>
                                            <p id="val_bentuk_rumah2"></p></td>
										</tr>
                                        <tr>
											<td><strong>Luas Lahan</strong></td>
											<td align="center"><input name="luas2" id="luas2" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_luas2"></p></td>
										</tr>
                                        <tr>
											<td><strong>Type</strong></td>
											<td align="center"><input name="tipe2" id="tipe2" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_tipe2"></p></td>
										</tr>
                                        <tr>
											<td><strong>Rencana</strong></td>
											<td align="center"><input name="rencana2" id="rencana2" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_rencana2"></p></td>
										</tr>
                                        <tr>
											<td><strong>Realisasi</strong></td>
											<td align="center"><input name="realisasi2" id="realisasi2" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_realisasi2"></p></td>
										</tr>
                                        <tr>
											<td><strong>Harga</strong></td>
											<td align="center"><input name="harga2" id="harga2" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_harga2"></p></td>
										</tr>
							  		</tbody>
									</table>
											</p>
										</div>
										<div class="modal-footer">
                      <button type="submit" class="btn green" >Simpan</button>
											<button type="button" data-dismiss="modal" class="btn default">Batal</button>
										</div>
									</div>
								</div>
								</form>
					</div>
					<div id="detail_pengembang" class="modal fade bs-modal-lg" tabindex="-1" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Informasi Rinci Perumahan <!-- KATEGORI --></h4> 
								</div>
								<div class="modal-body">
									<div class=" " style="height:410px" data-always-visible="1" data-rail-visible1="1">
										<div class="row">
											<div class="col-md-12">
												<table class="table table-light detail-info">
													<tbody>
													    <?php foreach($pengembang_view_detail as $f): ?>  
														<tr>
															<td class="fit">A.</td>
															<td width="15%">Nama Perumahan</td>
															<td class="fit">:</td>
															<td><?php echo $f->nama_perusahaan;?></td>
														</tr>
														<tr>
															<td>B.</td>
															<td>Alamat</td>
															<td>:</td>
															<td><?php echo $f->alamat;?></td>
														</tr>
														<tr>
															<td>C.</td>
															<td>Kabupaten/Kota</td>
															<td>:</td>
															<td><?php echo $f->kabupaten_kota;?></td>
														</tr>
														<tr>
															<td class="fit">D.</td>
															<td width="15%">Nama Pengembang</td>
															<td class="fit">:</td>
															<td><?php echo $f->nama_pengembang;?></td>
														</tr>
														<tr>
															<td>E.</td>
															<td>Luas Perumahan</td>
															<td>:</td>
															<td><?php echo $f->luas;?> m<sup>2</sup></td>
														</tr>
														<tr>
															<td>F.</td>
															<td>Bentuk Rumah</td>
															<td>:</td>
															<td><?php echo $f->bentuk_rumah;?></td>
														</tr>
														<tr>
															<td>G.</td>
															<td>Tipe</td>
															<td>:</td>
															<td><?php echo $f->tipe;?></td>
														</tr>
														<tr>
															<td>H.</td>
															<td>Jumlah Unit</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">- Rencana</td>
															<td>:</td>
															<td><?php echo $f->rencana;?></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">- Realisasi</td>
															<td>:</td>
															<td><?php echo $f->realisasi;?></td>
														</tr>
														<tr>
															<td>I.</td>
															<td>Harga</td>
															<td>:</td>
															<td>Rp <?php echo $f->harga;?>,-</td>
														</tr>
                                                    <?php endforeach;?> 
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer form-actions" style="text-align:center">
									<button type="button" data-dismiss="modal" class="btn default">Tutup</button>
								</div>
							</div>
						</div>
					</div>
					
					<div id="detail_pengembang2" class="modal fade bs-modal-lg" tabindex="-1" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Informasi Rinci Perumahan <!-- KATEGORI --></h4> 
								</div>
								<div class="modal-body">
									<div class=" " style="height:410px" data-always-visible="1" data-rail-visible1="1">
										<div class="row">
											<div class="col-md-12">
												<table class="table table-light detail-info">
													<tbody>
													    <?php foreach($pengembang_view_detail2 as $f): ?>  
														<tr>
															<td class="fit">A.</td>
															<td width="15%">Nama Perumahan</td>
															<td class="fit">:</td>
															<td><?php echo $f->nama_perusahaan;?></td>
														</tr>
														<tr>
															<td>B.</td>
															<td>Alamat</td>
															<td>:</td>
															<td><?php echo $f->alamat;?></td>
														</tr>
														<tr>
															<td>C.</td>
															<td>Kabupaten/Kota</td>
															<td>:</td>
															<td><?php echo $f->kabupaten_kota;?></td>
														</tr>
														<tr>
															<td class="fit">D.</td>
															<td width="15%">Nama Pengembang</td>
															<td class="fit">:</td>
															<td><?php echo $f->nama_pengembang;?></td>
														</tr>
														<tr>
															<td>E.</td>
															<td>Luas Perumahan</td>
															<td>:</td>
															<td><?php echo $f->luas;?> m<sup>2</sup></td>
														</tr>
														<tr>
															<td>F.</td>
															<td>Bentuk Rumah</td>
															<td>:</td>
															<td><?php echo $f->bentuk_rumah;?></td>
														</tr>
														<tr>
															<td>G.</td>
															<td>Tipe</td>
															<td>:</td>
															<td><?php echo $f->tipe;?></td>
														</tr>
														<tr>
															<td>H.</td>
															<td>Jumlah Unit</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">- Rencana</td>
															<td>:</td>
															<td><?php echo $f->rencana;?></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">- Realisasi</td>
															<td>:</td>
															<td><?php echo $f->realisasi;?></td>
														</tr>
														<tr>
															<td>I.</td>
															<td>Harga</td>
															<td>:</td>
															<td>Rp <?php echo $f->harga;?>,-</td>
														</tr>
                                                    <?php endforeach;?> 
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer form-actions" style="text-align:center">
									<button type="button" data-dismiss="modal" class="btn default">Tutup</button>
								</div>
							</div>
						</div>
					</div>
					<!-- END DETAIL modal -->
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption"> 
								<span class="caption-subject font-green-sharp bold uppercase">
									Laporan Bulanan Data Pembangunan Unit Baru, Data Suplai Perumahan Tahun <?=date('Y');?>
								</span>
							</div> 
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="btn-group">
										</div>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="btn-group pull-right">
											
														<a href="javascript:;" class="printthis" onClick="window.print()"><i class="fa fa-print font-grey-mint"></i> Cetak</a>
										</div>
									</div>
								</div>
							</div>
              <!-- TABEL KEPALA -->
              <div class="col-md-6 col-sm-12" style="padding-left:0">
                  <form action="<?php echo base_url('/main/pengembang_update_bulan');?>" method="post">
                <table class="table table-bordered">
                  <tr>
                    <td width="30%">Provinsi</td>
                    <td width="70%"><?php echo $provinsi;?></td>
                  </tr>
                  <tr>
                    <td>Tahun</td>
                    <td>  
      									  <select name="tahun"  id="tahun" class="form-control"  onchange='this.form.submit()'>
                          <?php if (isset($tahun)){ echo "<option value=".$tahun.">".$tahun."</option>"; } else { ?>
                          <option value="0">Pilih Tahun</option>
                          <?php  }
                           for($i=date('Y')+10;$i>=date('Y')-10;$i--){
                           $yearNum = $i; 
                           echo '<option value="'.$yearNum.'">'.$yearNum.'</option>';  }
                          ?>
                          </select>
      					 </td>
                  </tr>
                  <tr>
                    <td>Bulan Data</td>
                    <td>
                        <input name="idpengembang" type="hidden" value="<?php echo $idform_1;?>"/>
                        <input name="tanggal" type="hidden" value="<?php echo date('Y-m-d');?>"/>
      									  <select name="bulan"  id="bulan" class="form-control"  onchange='this.form.submit()'>
                          <?php if (isset($bulan)){ echo "<option value=".$bulan.">".$bulan."</option>"; } else { ?>
                          <option value="0">Pilih Bulan</option>
                          <?php  }
                           for($i=1;$i<=12;$i++){
                           $monthNum = $i;
                           $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
                           echo '<option value="'.$monthName.'">'.$monthName.'</option>';  }
                          ?>
                          </select>
      					 </td>
                  </tr>
                  <tr> 
                    <td>Nama Pengembang</td>
                    <td>
                        <input name="idform_satu_juta_rumah" type="hidden" value="<?php echo $idform_1;?>"/>
                        <input name="tanggal" type="hidden" value="<?php echo date('Y-m-d');?>"/>
      					
                        <input name="nama_pengembang" type="text" value="<?php echo $pengembang;?>"/>
  					</td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>
                        <input name="alamat" type="text" value="<?php echo $alamat;?>"/></td>
                  </tr>
                  <tr>
                    <td>Asosiasi</td>
                    <td>
                        <input name="asosiasi" type="text" value="<?php echo $asosiasi;?>"/></td>	 
                  </tr>
                  <tr>
                    <td></td>
                    <td>
                        <input type="submit" value="Simpan"/></td>
                  </tr>
                </table>
                  </form>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="note note-success">
                  Untuk Pengembang
                </div>
              </div>
              <!-- END OF TABEL KEPALA -->
							<!-- TABEL UTAMA PERBANKAN DIMULAI -->
              <div class="col-md-12" style="padding-left:0">
								<h3>Tabel Data Perumahan untuk MBR/Rumah Umum/Subsidi</h3>
							</div>
							<div class="col-md-12" style="padding-left:0">
              <p> <a class=" btn default" data-toggle="modal" href="#fimb_modal" onClick="tambah_imb_modal()">Tambah</a>   </p> </div>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="text-align:center; vertical-align:middle">No</th>
                    <th style="text-align:center; vertical-align:middle">Kabupaten/Kota</th>
                    <th style="text-align:center; vertical-align:middle">Nama Perumahan</th>
                    <th style="text-align:center; vertical-align:middle">Alamat Perumahan</th>
                    <th style="text-align:center; vertical-align:middle">Bentuk Rumah</th>
										<th style="text-align:center; vertical-align:middle">Detail</th>
										<th style="text-align:center; vertical-align:middle">Ubah</th>
										<th style="text-align:center; vertical-align:middle">Hapus</th> 
                  </tr>
                </thead>
                <tbody> 
                  <?php $i=0; foreach($pengembang_view_detail as $f): ?>
                  <tr>
                    <td><?php $i++; echo $i;?>.</td>
                    <td><?php echo $f->kabupaten_kota;?></td>
                    <td><?php echo $f->nama_pengembang;?></td>
                    <td><?php echo $f->alamat;?></td>
                    <td><?php echo $f->bentuk_rumah;?></td>  
                    <td style="text-align:center"><a href="#detail_pengembang" data-toggle="modal">Detail</a></td>  
                    <td align="center">
                      <a class="btn default btn-xs green-stripe" data-toggle="modal" href="#fimb_modal" onClick="ubah_imb_modal('<?php echo $f->idpengembang_mbr;?>','<?php echo $f->idkabupaten_kota;?>','<?php echo $f->nama_perusahaan;?>','<?php echo $f->alamat;?>','<?php echo $f->nama_pengembang;?>','<?php echo $f->bentuk_rumah;?>','<?php echo $f->luas;?>','<?php echo $f->tipe;?>','<?php echo $f->rencana;?>','<?php echo $f->realisasi;?>','<?php echo $f->harga;?>')">Ubah</a>
                    </td>
                    <td align="center">
  									 <a href="javascript:confirmDelete(<?=$f->idpengembang_mbr;?>)" class="btn default btn-xs blue-stripe"> Hapus </a>
                    </td>
                  </tr>
                  <?php endforeach;?> 
                </tbody>
              </table><div class="col-md-12" style="padding-left:0">
								<h3>Tabel Data Perumahan untuk Non-MBR/Rumah Komersial</h3>
							</div>
							<div class="col-md-12" style="padding-left:0">
              <p> <a class=" btn default" data-toggle="modal" href="#fimb_modal2" onClick="tambah_imb_modal2()">Tambah</a>   </p> </div>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="text-align:center; vertical-align:middle">No</th>
                    <th style="text-align:center; vertical-align:middle">Kabupaten/Kota</th>
                    <th style="text-align:center; vertical-align:middle">Nama Perumahan</th>
                    <th style="text-align:center; vertical-align:middle">Alamat Perumahan</th>
                    <th style="text-align:center; vertical-align:middle">Bentuk Rumah</th>
										<th style="text-align:center; vertical-align:middle">Detail</th>
										<th style="text-align:center; vertical-align:middle">Ubah</th>
										<th style="text-align:center; vertical-align:middle">Hapus</th> 
                  </tr>
                </thead>
                <tbody>
                  <?php $i=0; foreach($pengembang_view_detail2 as $f): ?>
                  <tr>
                    <td><?php $i++; echo $i;?>.</td>
                    <td><?php echo $f->kabupaten_kota;?></td>
                    <td><?php echo $f->nama_pengembang;?></td>
                    <td><?php echo $f->alamat;?></td>
                    <td><?php echo $f->bentuk_rumah;?></td>  
                    <td style="text-align:center"><a href="#detail_pengembang2" data-toggle="modal">Detail</a></td>  
                    <td align="center">
                      <a class="btn default btn-xs green-stripe" data-toggle="modal" href="#fimb_modal" onClick="ubah_imb_modal('<?php echo $f->idpengembang_mbr;?>','<?php echo $f->idkabupaten_kota;?>','<?php echo $f->nama_perusahaan;?>','<?php echo $f->alamat;?>','<?php echo $f->nama_pengembang;?>','<?php echo $f->bentuk_rumah;?>','<?php echo $f->luas;?>','<?php echo $f->tipe;?>','<?php echo $f->rencana;?>','<?php echo $f->realisasi;?>','<?php echo $f->harga;?>')">Ubah</a>
                    </td>
                    <td align="center">
  									 <a href="javascript:confirmDelete2(<?=$f->idpengembang_mbr;?>)" class="btn default btn-xs blue-stripe"> Hapus </a>
                    </td>
                  </tr>
                  <?php endforeach;?> 
                </tbody>
              </table>
							<!-- TABEL UTAMA PERBANKAN SELESAI -->
              <div class="note note-warning">
                Informasi dalam tabel ini bersumber dari asosiasi/pengembang dan dihimpun oleh SNVT bidang Perumahan Provinsi
								melalui TAPP Provinsi, untuk pertanyaan lebih lanjut dapat menghubungi Subdit Data dan Informasi,
								Direktorat Perencanaan Penyediaan Perumahan: datinperumahan@gmail.com (021-7211883)
              </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
					<!-- BEGIN EDIT modal -->
					<!-- /.modal -->
					<div id="edit_pengembang" class="modal fade bs-modal-lg" tabindex="-1" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Ubah Data Perumahan <!-- KATEGORI --></h4>
									<!--
									isian untuk KATEGORI hanya muncul di form penambahan data baru, terdiri dari:
									1. MBR/Rumah Umum/Subsidi
									2. Non-MBR/Rumah Komersial
									-->
								</div>
								<div class="modal-body">
									<div class="scroller" style="height:390px" data-always-visible="1" data-rail-visible1="1">
										<div class="row">
											<div class="col-md-12">
												<form action="javascript:;" class="form-horizontal">
												<table class="table table-light">
													<tbody>
														<tr>
															<td class="fit">A.</td>
															<td width="15%">Nama Perumahan</td>
															<td class="fit">:</td>
															<td width="83%"><input type="text" class="form-control" placeholder="Nama Perumahan"></td>
														</tr>
														<tr>
															<td>B.</td>
															<td>Nama Pengembang</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Nama Pengembang"></td>
														</tr>
														<tr>
															<td>C.</td>
															<td>Lokasi</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Alamat</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Alamat"></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Kabupaten/Kota</td>
															<td>:</td>
															<td>
																<select class="form-control" style="width:auto">
																	<option>Pilih Kabupaten</option>
																	<option>Kabupaten A</option>
																	<option>Kabupaten B</option>
																</select>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Kecamatan</td>
															<td>:</td>
															<td>
																<select class="form-control" style="width:auto">
																	<option>Pilih Kecamatan</option>
																	<option>Kecamatan A</option>
																	<option>Kecamatan B</option>
																</select>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Kelurahan</td>
															<td>:</td>
															<td>
																<select class="form-control" style="width:auto">
																	<option>Pilih Kelurahan</option>
																	<option>Kelurahan A</option>
																	<option>Kelurahan B</option>
																</select>
															</td>
														</tr>
														<tr>
															<td>D.</td>
															<td>Luas Perumahan</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="000.000,00" style="width:auto"></td>
														</tr>
														<tr>
															<td>E.</td>
															<td>Bentuk Rumah</td>
															<td>:</td>
															<td>
																<select class="form-control" style="width:auto">
																	<option>Tapak</option>
																	<option>Susun</option>
																</select>
															</td>
														</tr>
														<tr>
															<td>F.</td>
															<td>Tipe Rumah</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Tipe (LB/LT)" style="width:auto"></td>
														</tr>
														<tr>
															<td>G.</td>
															<td>Jumlah Unit</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Rencana</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Unit Rencana" style="width:auto"></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td style="padding-left:2em">Realisasi</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Unit Realisasi" style="width:auto"></td>
														</tr>
														<tr>
															<td>H.</td>
															<td>Harga</td>
															<td>:</td>
															<td><input type="text" class="form-control" placeholder="Rp 999.999,-" style="width:auto"></td>
														</tr>
													</tbody>
												</table>
											</form>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer form-actions" style="text-align:center">
									<button type="button" data-dismiss="modal" class="btn default">Batal</button>
									<button type="submit" class="btn green">Simpan</button>
								</div>
							</div>
						</div>
					</div>
					<!-- END EDIT modal -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<?php include "footer2.php";?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-contextmenu/bootstrap-contextmenu.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
	 Layout.init(); // init current layout
	 Demo.init(); // init demo features
   TableManaged.init();
});
</script> 
</body>
<!-- END BODY -->
</html>
