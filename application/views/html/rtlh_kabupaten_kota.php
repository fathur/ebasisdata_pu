<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<style>
.rotate {
/* Safari */
-webkit-transform: rotate(-90deg);

/* Firefox */
-moz-transform: rotate(-90deg);

/* IE */
-ms-transform: rotate(-90deg);

/* Opera */
-o-transform: rotate(-90deg);

/* Internet Explorer */
filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

}
</style>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php include "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>RTLH <small>Provinsi</small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="filter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
                    <form action="<?php echo BASE_URL("main/rtlh");?>" method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Filter</h4>
						</div>
						<div class="modal-body">
							<?php  $query = $this->db->query('SELECT idprovinsi,provinsi FROM provinsi where idprovinsi<>'.$idprovinsi);?>
							Provinsi <select name="idprovinsi" class="form-control">
								<option value="<?php echo $idprovinsi;?>"><?php echo $provinsi;?></option>
                            <?php foreach ($query->result_array() as $row):?>
								<option value="<?php echo $row['idprovinsi'];?>"><?php echo $row['provinsi'];?></option>
                            <?php endforeach;
                            ?> </select>
							<?php $query = $this->db->query('SELECT (tahun) as tahun FROM rtlh_kabupaten_kota group by tahun order by tahun desc');?>
							TAHUN<select name="tahun" class="form-control">
                            <?php foreach ($query->result_array() as $row):?>
								 <option value="<?php echo $row['tahun'];?>"><?php echo $row['tahun'];?></option>
                            <?php endforeach;
                            ?> </select>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn blue" >Ok</button>
							<button type="button" class="btn default" data-dismiss="modal">Batal</button>
						</div>
                        </form>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase"></span>
							</div>
							<div class="tools"> 
                 <form action="<?=BASE_URL('main/rtlh');?>" id="form_tahun" class="form-horizontal" method="post">
              		 <select name='tahun' id='tahun' class="form-control" style="width: 140px" onchange="this.form.submit()">
                     <option value="<?php echo $tahun;?>"><?php echo $tahun;?></option> 
						<?php
                            $tahun_berjalan = date('Y');
                            $ta = $tahun_berjalan;
                                for($t=$ta;$t>$ta-10;$t--){ 
                                            echo "<option value=".$t.">".$t."</option>"; 
                                }
                        ?>
                     </select>
                    </form>
              	</div>
						</div>
                        <div class="portlet-body">
							 <?php  include "grafik_kabupaten_kota.php"; ?>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
                                             <?php $attributes = array('class' => 'email', 'id' => 'my_form');?>
                                            <?php echo form_open('main/rtlh_kabupaten_kota', $attributes); ?>
                                            <?php echo form_input(array('id' => 'tahun', 'name' => 'tahun', 'type' => 'hidden', 'value' => date('Y'))); ?>
											<?php echo form_close(); ?>
                                                <?php $attributes = array('class' => 'email', 'id' => 'my_form2');?>
                                            <?php echo form_open('main/rtlh_kabupaten_kota', $attributes); ?>
                                            <?php echo form_input(array('id' => 'tahun', 'name' => 'tahun', 'type' => 'hidden', 'value' => date('Y')-1)); ?>
											<?php echo form_close(); ?>
                                                <?php $attributes = array('class' => 'email', 'id' => 'my_form3');?>
                                            <?php echo form_open('main/rtlh_kabupaten_kota', $attributes); ?>
                                            <?php echo form_input(array('id' => 'tahun', 'name' => 'tahun', 'type' => 'hidden', 'value' => date('Y')-2)); ?>
											<?php echo form_close(); ?>
										</div>
									</div>

									<div class="col-md-6">
										 
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th >
									No
								</th>
								<th>
									 Provinsi
								</th>
								<th>
									 Kabupaten/Kota
								</th>
								<th>
									 Jumlah RTLH
								</th>
								<th>
									 Sumber Data
								</th>
								<th>
									 Tahun (<?=$tahun;?>)
								</th>
							</tr>
							</thead>
							<tbody>
                            <?php $i=0; foreach($rtlh_kabupaten_kota as $r): $i++; ?>
							<tr class="odd gradeX">
								<td>
									 <?php echo $i;?>
								</td>
								<td>
									<?php echo $r->provinsi;?>
								</td>
								<td>
									<?php echo $r->kabupaten_kota;?>
								</td>
								<td>
									<?php echo $r->jumlah_rtlh_verifikasi_pemda_5;?>
								</td>
								<td>
									 <?php echo $r->sumber_data_6;?>
								</td>
								<td class="center">
									 <?php echo $r->tahun;?>
								</td>
							</tr>
                <?php endforeach; ?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<?php include "footer2.php"; ?>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
   Layout.init(); // init current layout
   Demo.init(); // init demo features
   TableManaged.init();
});
</script>
<script>
    function val_profile_form() {
    var kada,wakada;
    kada = document.getElementById("kada").value;
    wakada = document.getElementById("wakada").value;
     //if (isNaN(x) || x < 1 || x > 10) {
    if ((kada=="")) {
        document.getElementById("val_kada").innerHTML = "Masukan Nama Kepala Daerah";
    } else if ((wakada=="")) {
        document.getElementById("val_wakada").innerHTML = "Masukan Nama Wakil Kepala Daerah";
    } else {
        document.getElementById("frofile_form").submit();
		    document.getElementById("val_kada").innerHTML = "";
		    document.getElementById("val_wakada").innerHTML = "";
        document.getElementById("val_letak_geografi").innerHTML = "";  
    }
}
    
    $("#provinsi_filter").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_filter').load(url);
        return false;
    });
    $("#provinsi_filter2").change(function (){
        var url = "<?php echo site_url('main/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten_profile_filter').load(url);
        return false;
    });

    function val_filter_kab() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab").submit();
        }
    }
    
    function val_filter_kab_prov() {
        var idkabupaten_kota_select;
        idkabupaten_kota_select = document.getElementById("kabupaten_kota_filter").selectedIndex;
        if ((idkabupaten_kota_select==0)) {
            document.getElementById("val_idkabupaten_kota_select").innerHTML = "Pilih Kabupaten/Kota";
        } else {
            document.getElementById("form_filter_kab2").submit();
        }
    }
     
    function sel_prov(a) {
        document.getElementById("pilih_prov_text").value=a;
        document.getElementById("pilih_prov").submit();
    }
    
    function val_filter_kab2() {
    var idkabupaten_kota_select;
    idkabupaten_kota_select = document.getElementById("provinsi_filter2").selectedIndex;
    if ((idkabupaten_kota_select==0)) {
        document.getElementById("val_idkabupaten_kota_select2").innerHTML = "Pilih Provinsi";
    } else {
        document.getElementById("form_filter_profile").submit();
    }
}

</script>
</body>
<!-- END BODY -->
</html>
