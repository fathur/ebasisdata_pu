var Login = function () {

	// VALIDASI FORM LOGIN PUSAT
	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                tabulasi: {
	                    required: true
	                },
	                password2: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	                tabulasi: {
	                    required: "Username harus diisi dengan benar."
	                },
	                password2: {
	                    required: "Password harus diisi dengan benar."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit
	                $('.alert-danger', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    $('.login-form').submit();
	                }
	                return false;
	            }
	        });

			// VALIDASI FORM LOGIN PROVINSI
			$('.login-form-provinsi').validate({
		            errorElement: 'span', //default input error message container
		            errorClass: 'help-block', // default input error message class
		            focusInvalid: false, // do not focus the last invalid input
		            rules: {
		                username: {
		                    required: true
		                },
		                password: {
		                    required: true
		                },
		                remember: {
		                    required: false
		                }
		            },

		            messages: {
		                username: {
		                    required: "Provinsi harus diisi."
		                },
		                password: {
		                    required: "Password harus diisi."
		                }
		            },

		            invalidHandler: function (event, validator) { //display error alert on form submit
		                $('.alert-danger', $('.login-form-provinsi')).show();
		            },

		            highlight: function (element) { // hightlight error inputs
		                $(element)
		                    .closest('.form-group').addClass('has-error'); // set error class to the control group
		            },

		            success: function (label) {
		                label.closest('.form-group').removeClass('has-error');
		                label.remove();
		            },

		            errorPlacement: function (error, element) {
		                error.insertAfter(element.closest('.input-icon'));
		            },

		            submitHandler: function (form) {
		                form.submit();
		            }
		        });

		        $('.login-form-provinsi input').keypress(function (e) {
		            if (e.which == 13) {
		                if ($('.login-form-provinsi').validate().form()) {
		                    $('.login-form-provinsi').submit();
		                }
		                return false;
		            }
		        });

			// VALIDASI FORM LOGIN KABKOT
			$('.login-form-kabkot').validate({
		            errorElement: 'span', //default input error message container
		            errorClass: 'help-block', // default input error message class
		            focusInvalid: false, // do not focus the last invalid input
		            rules: {
		                username: {
		                    required: true
		                },
		                password: {
		                    required: true
		                },
		                remember: {
		                    required: false
		                }
		            },

		            messages: {
		                username: {
		                    required: "Kabupaten/Kota harus diisi."
		                },
		                password: {
		                    required: "Password harus diisi."
		                }
		            },

		            invalidHandler: function (event, validator) { //display error alert on form submit
		                $('.alert-danger', $('.login-form-kabkot')).show();
		            },

		            highlight: function (element) { // hightlight error inputs
		                $(element)
		                    .closest('.form-group').addClass('has-error'); // set error class to the control group
		            },

		            success: function (label) {
		                label.closest('.form-group').removeClass('has-error');
		                label.remove();
		            },

		            errorPlacement: function (error, element) {
		                error.insertAfter(element.closest('.input-icon'));
		            },

		            submitHandler: function (form) {
		                form.submit();
		            }
		        });

		        $('.login-form-kabkot input').keypress(function (e) {
		            if (e.which == 13) {
		                if ($('.login-form-kabkot').validate().form()) {
		                    $('.login-form-kabkot').submit();
		                }
		                return false;
		            }
		        });

					function format(province,kabupaten) {

									if (!province.id) return province.text; // optgroup
									return "<img class='flag' src='../../../assets/global/img/flags/" + province.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + province.text;

									if (!kabupaten.id) return kabupaten.text
									return "<img class='flag' src='../../../assets/global/img/flags/" + kabupaten.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + kabupaten.text;
					}

					$("#select2_sample4").select2({
							placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Pilih Provinsi',
									allowClear: true,
									formatResult: format,
									formatSelection: format,
									escapeMarkup: function (m) {
											return m;
									}
							});

					$("#select2_sample5").select2({
							placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Pilih Provinsi',
									allowClear: true,
									formatResult: format,
									formatSelection: format,
									escapeMarkup: function (m) {
											return m;
									}
							});

					$("#select2_sample6").select2({
							placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Pilih Kabupaten',
									allowClear: true,
									formatResult: format,
									formatSelection: format,
									escapeMarkup: function (m) {
											return m;
									}
							});

	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Masukkan alamat e-mail."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}


    return {
        //main function to initiate the module
        init: function () {

            handleLogin();
            handleForgetPassword();
        }

    };

}();
