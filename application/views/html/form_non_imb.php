<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 foreach($form_non_imb_view_detail as $r):
     $idform_1=$r->idform_satu_juta_rumah;
     $bulan=$r->bulan;
 endforeach;
?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<script type="text/javascript">
function tambah_imb_modal()
{
	  document.getElementById('perumahan').value="";
	  document.getElementById('nama_pengembang').value="";
	  document.getElementById('alamat').value="";
	  document.getElementById('bentuk_rumah').value="";
	  document.getElementById('luas').value="";
	  document.getElementById('tipe').value="";
	  document.getElementById('jumlah_mbr').value="";
	  document.getElementById('jumlah_non_mbr').value="";
	  document.getElementById('idform_satu_juta_rumah_imb').value="";
        document.getElementById('imb_f').action="<?php echo base_url('main/fnonimb_insert');?>";
}
function ubah_imb_modal(a,b,c,d,e,f,g,h,i)
{
	  document.getElementById('perumahan').value=a;
	  document.getElementById('nama_pengembang').value=b;
	  document.getElementById('alamat').value=c;
	  document.getElementById('bentuk_rumah').value=d;
	  document.getElementById('luas').value=e;
	  document.getElementById('tipe').value=f;
	  document.getElementById('jumlah_mbr').value=g;
	  document.getElementById('jumlah_non_mbr').value=h;
	  document.getElementById('idform_satu_juta_rumah_imb').value=i;
        document.getElementById('imb_f').action="<?php echo base_url('main/fnonimb_update');?>";
}
function confirmDelete1(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_fnonimb');?>/"+delUrl;
  }
}
function tambah_imb_unit_modal()
{
	  document.getElementById('idkecamatan').SelectedIndex=0;
	  document.getElementById('jumlah_mbr2').value="";
	  document.getElementById('jumlah_non_mbr2').value="";
	  document.getElementById('idform_satu_juta_rumah_unit').value="";
    document.getElementById('imb_unit_f').action="<?php echo base_url('main/fnon_imb_unit_insert');?>";
}
function ubah_imb_unit_modal(a,b,c,d,e)
{
	  document.getElementById('idkecamatan').value=a;
	  //document.getElementById('idkabupaten_kota').value=b;
	  document.getElementById('jumlah_mbr2').value=c;
	  document.getElementById('jumlah_non_mbr2').value=d;
	  document.getElementById('idform_satu_juta_rumah_unit').value=e;
    document.getElementById('imb_unit_f').action="<?php echo base_url('main/fnon_imb_unit_update');?>";
}
function confirmDelete2(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_fnonimb_unit');?>/"+delUrl;
  }
}

function val_imb2() {
    var idkecamatan,jumlah_mbr2,jumlah_non_mbr2;
    idkecamatan = document.getElementById("idkecamatan").value;
    jumlah_mbr2 = document.getElementById("jumlah_mbr2").value;
    jumlah_non_mbr2 = document.getElementById("jumlah_non_mbr2").value;
    //if (isNaN(x) || x < 1 || x > 10) {
    if (isNaN(idkecamatan) || idkecamatan=="0") {
        document.getElementById("val_idkabupaten_kota").innerHTML = "Pilih Kecamatan";
    } else if (jumlah_mbr2=="" || isNaN(jumlah_mbr2) || jumlah_mbr2 < 1 ) {
        document.getElementById("val_jumlah_mbr2").innerHTML = "Masukan Jumlah MBR";
    } else if (jumlah_non_mbr2=="" || isNaN(jumlah_non_mbr2) || jumlah_non_mbr2 < 1 ) {
        document.getElementById("val_jumlah_non_mbr2").innerHTML = "Masukan Jumlah Non MBR";
	} else {
        document.getElementById("imb_unit_f").submit();
		document.getElementById("val_idkabupaten_kota").innerHTML = "";
		document.getElementById("val_jumlah_mbr2").innerHTML = "";
		document.getElementById("val_jumlah_non_mbr2").innerHTML = "";
    }
}
</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body>
<?php include "header2.php";?>
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>Formulir Non-IMB <small class="page-title-tag"></?php echo $kabupaten_kota;?></small></h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="<?=base_url();?>">Home</a><i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Formulir Suplai Rumah<i class="fa fa-angle-right"></i>
				</li>
				<li class="active">
					 Formulir Non-IMB
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->

			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-list font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase">Laporan Bulanan Program Suplai Rumah Tahun 2017 Berdasarkan Data Non-IMB</span>
							</div>
							<div class="tools">
								<a href="" class="collapse" data-original-title="" title=""></a>
								<a href="" class="reload" data-original-title="" title=""></a>
								<a href="" class="fullscreen" data-original-title="" title=""></a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="btn-group">
										</div>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="btn-group pull-right">
											<button type="button" class="btn default" data-toggle="dropdown" aria-expanded="false"> Peralatan <i class="fa fa-angle-down"></i></button>
												<ul class="dropdown-menu pull-right">
													<li>
														<a href="javascript:;" class="printthis"><i class="fa fa-print font-grey-mint"></i> Cetak</a>
													</li>
													<li>
														<a href="javascript:;" class="savetopdf"><i class="fa fa-file-pdf-o font-grey-mint"></i> Ekspor ke PDF</a>
													</li>
													<li>
														<a href="javascript:;" class="savetoxls"><i class="fa fa-file-excel-o font-grey-mint"></i> Ekspor ke Excel</a>
													</li>
												</ul>
										</div>
									</div>
								</div>
							</div>
              <!-- TABEL KEPALA -->
              <div class="col-md-6 col-sm-12" style="padding-left:0">
                <table class="table table-bordered">
                  <tr>
                    <td width="30%">Provinsi</td>
                    <td width="70%"><?php echo $provinsi;?></td>
                  </tr>
                  <tr>
                    <td>Nama Instansi</td>
                    <td><?php echo $nama_dinas;?></td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td><?php echo $alamat;?></td>
                  </tr>
                  <tr>
                    <td>Bulan Data</td>
                    <td><form action="<?php echo base_url('/main/form_satu_juta_rumah_update_bulan_non_imb');?>" method="post">
                                    <input name="idform_satu_juta_rumah" type="hidden" value="<?php echo $idform_1;?>"/>
                                    <input name="tanggal" type="hidden" value="<?php echo date('Y-m-d');?>"/>
									 <select name="bulan"  id="bulan" class="form-control"  onchange='this.form.submit()'>
                                     <?php if (isset($bulan)){ echo "<option value=".$bulan.">".$bulan."</option>"; } else { ?>
                                     <option value="0">Pilih Bulan</option>
                                    <?php  }
 for($i=1;$i<=12;$i++){
 $monthNum = $i;
 $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
 echo '<option value="'.$monthName.'">'.$monthName.'</option>';  }
?>
                                     </select>
    								<noscript><input type="submit" value="Submit"></noscript>
										</form></td>
                  </tr>
                </table>
              </div>
              <div class="col-md-6 col-sm-12">
                  <!--KOSONG -->
              </div>
              <!-- END OF TABEL KEPALA -->
              <!-- TABEL NON-IMB DIMULAI -->
              <div class="col-md-12" style="padding-left:0"><h3>Tabel Data Pembangunan Unit Rumah yang Tidak Terdaftar IMB</h3></div>
              <div class="col-md-12" style="padding-left:0">
              <p> <a class=" btn default" data-toggle="modal" href="#fimb_unit_modal" onClick="tambah_imb_unit_modal()">Tambah</a>   </p> </div>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="text-align:center" width="3%">No</th>
                    <th style="text-align:center" width="15%">Kecamatan</th>
                    <th style="text-align:center" width="10%">Unit MBR</th>
                    <th style="text-align:center" width="10%">Unit Non-MBR</th>
                    <th style="text-align:center" width="10%" colspan="2">Fungsi</th>
                  </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach($form_satu_juta_rumah_non_imb_view_detail as $f): ?>
                  <tr>
                    <td><?php $i++; echo $i;?>.</td>
                    <td><?php echo $f->kecamatan;?></td>
                    <td><?php echo $f->jumlah_mbr;?></td>
                    <td><?php echo $f->jumlah_non_mbr;?></td>
                    <td align="center">
                      <a class="btn default btn-xs green-stripe" data-toggle="modal" href="#fimb_unit_modal" onClick="ubah_imb_unit_modal('<?php echo $f->idkecamatan;?>','<?php echo $f->idkabupaten_kota;?>','<?php echo $f->jumlah_mbr;?>','<?php echo $f->jumlah_non_mbr;?>','<?php echo $f->idform_satu_juta_rumah_unit;?>')">Ubah</a>
                    </td>
                    <td align="center">
  									 <a href="javascript:confirmDelete2(<?=$f->idform_satu_juta_rumah_unit;?>)" class="btn default btn-xs blue-stripe">
  										Hapus </a>
                    </td>
                  </tr>
                    <?php endforeach;?>
                </tbody>
              </table>
							<!-- TABEL NON-IMB SELESAI -->
              <div class="note note-danger">
                Data ini merupakan bagian dari Pembangunan Basis Data PKP Pemkab/Pemkot sesuai UU No.1
                Tahun 2011 tentang Perumahan dan Kawasan Permukiman (PKP) pasal 18 dan Peraturan
                Pemerintah No.88 Tahun 2014 tentang Pembinaan Penyelenggaraan PKP pasal 18 ayat 1.
                Data ini merupakan analisis jumlah pembangunan rumah oleh masyarakat yang tidak terdaftar
                di data IMB.
                <br><br>
                Tabel ini diisi oleh Badan Pelayanan IMB Kab/Kota dan dikembalikan
                ke SNVT bidang Perumahan Provinsi melalui Dinas PKP Kab/Kota,
                untuk pertanyaan lebih lanjut dapat menghubungi Subdit Data dan Informasi,
                Direktorat Perencanaan Penyediaan Perumahan: datinperumahan@gmail.com (021-7211883)
              </div>
              <div>
                  Pengesahan Kepala Dinas Perumahan dan Kawasan<br>
                  <form action="<?php echo site_url('main/form_satu_juta_rumah_update_approval_non'); ?>" class="form-horizontal" enctype="multipart/form-data" id="form-upload2" method="post" />
                        <table width="300"><tr>
                          <td align="center">
                           ttd
                          </td>
                          </tr>
                        <tr>
                        <td>
                        <input name="idform_satu_juta_rumah" type="hidden" value="<?php echo $idform_1;?>"/>
                        <input type="file" name="image2" class="form-control" id="gambar2"/>
                        </td>
                        <td>
                         <input type="submit" class="btn blue"  value="Selesai" id="submit-button2" >
                        </td>
                        </tr>
                        </table></form></div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

   <?php include "footer2.php";?>

   <div id="fimb_unit_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
           <form action="" class="form-horizontal" method="post" id="imb_unit_f"/>
           <div class="modal-dialog">
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                 <h5 class="modal-title"><strong>TABEL DATA PEMBANGUNAN UNIT RUMAH BERDASARKAN IMB PERORANGAN<strong></h5>
               </div>
               <div class="modal-body">
                 <p>
               <table id="user" class="table table-bordered table-striped">
               <tbody>
               <tr>
                 <td><strong>Kecamatan</strong></td>
                 <td align="center">
                   <input name="idform_satu_juta_rumah" id="idform_satu_juta_rumah" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                   <input name="idkabupaten_kota" id="idkabupaten_kota" type="hidden" class="form-control"  value="<?=$idkabupaten_kota;?>"/>
                   <input name="idform_satu_juta_rumah_unit" id="idform_satu_juta_rumah_unit" type="hidden" class="form-control" />
                   <select name="idkecamatan" id="idkecamatan" class="form-control" style="background-color:#E8F3FF">
                   <option value="0">Pilih Kecamatan</option>
                   <?php foreach($kecamatan as $kk): ?>
                   <option value="<?php echo $kk->idkecamatan;?>"><?php echo  $kk->kecamatan;?></option>
                   <?php endforeach;?>
                   </select>
                 <p id="val_idkabupaten_kota"></p></td>
               </tr>
               <tr>
                 <td><strong>UNIT MBR</strong></td>
                 <td align="center"><input name="jumlah_mbr2" id="jumlah_mbr2" type="text" class="form-control"  style="background-color:#E8F3FF" /><p id="val_jumlah_mbr2"></p></td>
               </tr>
               <tr>
                 <td><strong>UNIT NON-MBR</strong></td>
                 <td align="center" ><input name="jumlah_non_mbr2" id="jumlah_non_mbr2" type="text" class="form-control"  style="background-color:#E8F3FF"/><p id="val_jumlah_non_mbr2"></p></td>
               </tr>
               </tbody>
             </table>
                 </p>
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn green" onclick="val_imb2()">Simpan</button>
                 <button type="button" data-dismiss="modal" class="btn default">Batal</button>
               </div>
             </div>
           </div>
           </form>
         </div>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-contextmenu/bootstrap-contextmenu.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/table-managed.js"></script>
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core components
	 Layout.init(); // init current layout
	 Demo.init(); // init demo features
   TableManaged.init();
});
</script>
</body>
<!-- END BODY -->
</html>
