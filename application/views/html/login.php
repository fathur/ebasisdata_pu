<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Halaman Login - eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../../assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css" />
<link href="../../../assets/admin/pages/css/login-soft.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css" />
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css" />
<link href="../../../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css" />
<link href="../../../assets/admin/layout/css/themes/default.css" id="style_color" rel="stylesheet" type="text/css" />
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css" />
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
  <body class="login">
  <!-- BEGIN LOGO -->
  <div class="logo">
    <a href="<?=base_url();?>">
      <img src="../../../assets/global/img/empty.png" alt="Logo PUPR" class="logo-login">
    </a>
  </div>
  <div class="logo text-header-kementerian" style="line-height: 1.5 !important">
    Direktorat Jenderal Penyediaan Perumahan
    <br>Kementerian Pekerjaan Umum dan Perumahan Rakyat
  </div>
  <div class="logo site-title">e-BASISDATA PERUMAHAN</div><p>&nbsp;</p>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <div class="login-form portlet light">
      <div class="portlet-title tabbable-line">
        <div class="caption">
          <span class="caption-subject bold font-yellow-crusta uppercase" style="text-align:center"> Panel Login </span>
          &nbsp;
          <i class="icon-login font-yellow-crusta pull-right"></i>
        </div>
      <ul class="nav nav-tabs nav-justified">
        <li class="active">
          <a href="#tab_2_1" data-toggle="tab" style="color: #eee" onClick="javascript:Check1();"> PUSAT </a>
        </li>
        <li>
          <a href="#tab_2_2" data-toggle="tab" style="color: #eee" onclick="javascript:Check2();"> PROVINSI </a>
        </li>
        <li>
          <a href="#tab_2_3" data-toggle="tab" style="color: #eee" onClick="javascript:Check3();"> KABUPATEN/KOTA</a>
        </li>
      </ul>
      </div>
		<div class="portlet-body">
			<div class="tab-content">
				<div class="tab-pane fade active in" id="tab_2_2">
				<!-- BEGIN LOGIN FORM -->  
  					<form class="login-form" action="<?php echo BASE_URL("/login/login_user");?>" method="post"> 
          <?php if (isset($error) && $error){ ?>
  					<div class="alert alert-danger" onload="close()">
  					  <button class="close" data-close="alert"></button>
  					  <span>Username atau Password salah! </span>
  					</div>
          <?php } ?>
					<!-- BEGIN LOGIN FORM PUSAT -->
					<div class="form-group" id="admindiv">
					  <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
					  <label class="control-label visible-ie8 visible-ie9">Pusat</label>
					  <input id="tabulasi" name="tabulasi" type="hidden" value="3">
					  <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" id="username" placeholder="Username" name="username"/>
				 </div>
					<!-- END LOGIN FORM PUSAT -->
					<!-- BEGIN LOGIN FORM PROVINSI -->
					<div class="form-group" id="provinsidiv" style="display: none">
						<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
						<label class="control-label visible-ie8 visible-ie9">Provinsi</label>
						<select class="form-control form-control-solid placeholder-no-fix" autocomplete="off" id="provinsi_select" placeholder="provinsi_select" name="provinsi_select"/>
  						<option value="">Provinsi</option>
  						<?php foreach($provinsi as $r): ?>
  						<option value="<?php echo $r->idprovinsi;?>"><?php echo $r->provinsi;?></option>
  						<?php endforeach; ?>
						</select>
					</div>
					<!-- END LOGIN FORM PROVINSI -->
					<!-- BEGIN LOGIN FORM KABUPATEN/KOTA -->
					<div class="form-group" id="kabupaten_kotadiv" style="display: none">
						<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
						<label class="control-label visible-ie8 visible-ie9">Kabupaten/Kota</label>
						<select class="form-control form-control-solid placeholder-no-fix" autocomplete="off" id="kabupaten" placeholder="kabupaten_kota" name="kab"/>
  						<option value="">Kabupaten/Kota</option>
  						<?php foreach($kabupaten_kota as $r): ?>
  						<option value="<?php echo $r->idkabupaten_kota;?>"><?php echo $r->kabupaten_kota;?></option>
              <?php endforeach; ?>
					  </select>
					</div>
					<!-- END LOGIN FORM KABUPATEN/KOTA -->
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Password</label>
						<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" id="password2" placeholder="Password" name="password2"/>
					</div>
					<div class="form-actions">
					  <label class="checkbox">
  						<input type="checkbox" name="remember" value="1" /> Ingat saya
					  </label>
					  <button type="submit" class="btn blue pull-right" style="width: 100px">
						Login
						<i class="fa fa-arrow-right fa-white"></i>
					  </button>
					</div>
					</form> 
				</div>
		  		<!-- END LOGIN FORM -->
			  	<div class="forget-password" style="text-align: left; padding-left: 5px">
					<h5><a href="javascript:;" id="forget-password" style="color: #eee">Lupa Password?</a></h5>
			  	</div>
			</div>
		</div>
    </div>
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <?php
    if (isset($emailInvalid) && $emailInvalid == 'true'){
      $mailfail = '';
      $mailsuccess = 'hide';

    }
    elseif (isset($emailInvalid) && $emailInvalid == 'false') {
      $mailfail ='hide';
      $mailsuccess = '';
    }
    else {
      $mailfail ='hide';
      $mailsuccess = 'hide';
    }
    ?>
	<form class="forget-form" action="<?php echo base_url('login/lupa_password'); ?>" method="post"/>
	<h3>Lupa Password?</h3>
	<p>Masukkan e-mail Dinas untuk mengatur ulang password.</p>
  <div class="alert alert-danger <?=$mailfail;?>">
    <button class="close" data-close="alert"></button>
    <span>E-mail yang Anda masukan tidak terdaftar.</span>
  </div>
  <div class="success alert-success <?=$mailsuccess;?>">
    <button class="close" data-close="alert"></button>
    <span>Password baru sudah dikirim ke alamat e-mail Anda.</span>
  </div>
	<div class="form-group">
	  <div class="input-icon">
		<i class="fa fa-envelope"></i>
		<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
	  </div>
	</div>
	<div class="form-actions">
	  <button type="button" id="back-btn" class="btn">
		<i class="fa fa-arrow-left"></i> Kembali
	  </button>
	  <button type="submit" class="btn blue pull-right">
		Kirim
		<i class="fa fa-arrow-right fa-white"></i>
	  </button>
	</div>
	</form>
    <!-- END FORGOT PASSWORD FORM -->
  </div>
  <!-- BEGIN COPYRIGHT -->
  <div class="copyright">
    2017 &copy; Direktorat Jenderal Penyediaan Perumahan.
    <br>
    <a href="http://www.pu.go.id" title="Situs Web PUPR" target="_blank">Kementerian Pekerjaan Umum dan Perumahan Rakyat</a>
  </div>
  <!-- END COPYRIGHT -->
  <!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/select2/select2.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
  jQuery(document).ready(function() {
  Metronic.init(); // init metronic core components
  Layout.init(); // init current layout
  Login.init();
  Demo.init();
   // init background slide images
   $.backstretch([
    "../../../assets/admin/pages/media/bg/1.jpg",
    "../../../assets/admin/pages/media/bg/2.jpg",
    "../../../assets/admin/pages/media/bg/3.jpg",
    "../../../assets/admin/pages/media/bg/4.jpg",
    "../../../assets/admin/pages/media/bg/5.jpg",
    "../../../assets/admin/pages/media/bg/6.jpg",
    "../../../assets/admin/pages/media/bg/7.jpg",
    "../../../assets/admin/pages/media/bg/8.jpg"
    ], {
      fade: 1000,
      duration: 8000
    }
    );
  });

  // DROPDOWN WILAYAH (provinsi dan/atau kabupaten/kota)
  $(document).ready(function(){
      <?php if ((isset($emailInvalid) && ($emailInvalid = 'true' || $emailInvalid = 'false'))){ ?>
      $("#forget-password").trigger('click');
      <?php }?>
    $("#provinsi_select").change(function (){
        var url = "<?php echo site_url('login/add_ajax_kab');?>/"+$(this).val();
        $('#kabupaten').load(url);
        return false;
    })
  });
  // Mengecek Tab
  function Check1() {
    document.getElementById('tabulasi').value=3;
    document.getElementById('provinsidiv').style.display = 'none';
    document.getElementById('kabupaten_kotadiv').style.display = 'none';
    document.getElementById('admindiv').style.display = 'block';
  }
  function Check2() {
    document.getElementById('tabulasi').value=1;
    document.getElementById('kabupaten_kotadiv').style.display = 'none';
    document.getElementById('provinsidiv').style.display = 'block';
    document.getElementById('admindiv').style.display = 'none';
  }
  function Check3() {
    document.getElementById('tabulasi').value=2;
    document.getElementById('kabupaten_kotadiv').style.display = 'block';
    document.getElementById('provinsidiv').style.display = 'block';
    document.getElementById('admindiv').style.display = 'none';
}
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
