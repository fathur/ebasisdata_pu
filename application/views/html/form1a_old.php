<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 foreach($form_1_view_detail as $r):
     $idform_1 = $r->idform_1;
     $tahun    = $r->tahun;
     //$kabupaten_kota=$r->kabupaten_kota;
     //$kecamatan=$r->kecamatan;
     //$kelurahan=$r->kelurahan;
 endforeach;
?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="../../../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" type="image/png" href="../../../assets/global/img/favicon.png">
<style>
p.indent {
  padding-left: 1em
}
p.indent2 {
  padding-left: 9.7em
}
p.indent3 {
  padding-left: 3em
}
#kecamatan_combo {
  width:300px;
}
#tahun_form {
  width:100px;
}
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN HEADER -->
<?php include "header2.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Formulir IA <small class="page-title-tag">Provinsi <?php echo $provinsi;?></small></h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT <form action="http://localhost/datadekonpera/main/upload" id="form_sample_1" class="form-horizontal" method="post">-->
	<div class="page-content">
		<div class="container">
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <a href="<?php echo base_url('main/index'); ?>">Home</a><i class="fa fa-angle-right"></i>
        </li>
        <li>
           <a href="form_1a_view">Rekapitulasi Formulir IA</a><i class="fa fa-angle-right"></i>
        </li>
        <li class="active">
           Formulir IA
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
      <!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
         <div class="col-md-12">
    			<div class="portlet light">
    				<div class="portlet-body">
              <!-- BEGIN FORM-->
              <div class="form-actions">
  							<div class="row">
  								<div class="col-md-12 form-body">
                    <!-- BEGIN FORM ALERT -->
  									<div class="alert alert-danger display-hide">
  										<button class="close" data-close="alert"></button>
  										Terdapat Input yang Salah. Silakan Periksa Kembali.
  									</div>
  									<div class="alert alert-success display-hide">
  										<button class="close" data-close="alert"></button>
  										Form Anda Telah Tersimpan!
  									</div>
                    <!-- END FORM ALERT -->
        							<table id="user" class="table table-bordered table-striped">
          							<tbody>
            							<tr>
            								<td style="width:15%">
            									 PROVINSI
            								</td>
            								<td style="width:50%">
            									<?php echo $provinsi;?>
                              <input name="idprovinsi" type="hidden" class="form-control" value="<?php echo $idprovinsi;?>"/>
            									<input name="provinsi" type="hidden" class="form-control" value="<?php echo $provinsi;?>"/>
            								</td>
            								<td style="width:35%" align="center">
            									<span class="text-muted">
            									   <strong>FORMULIR IA</strong>
                              </span>
            								</td>
            							</tr>
            							<tr>
            								<td>
            									 NAMA DINAS
                               <input name="iddinas" type="hidden" class="form-control" value="<?php echo $iddinas;?>"/>
                               <input name="privilage" type="hidden" class="form-control" value="<?php echo $privilage;?>"/>
                               <input name="idform_1" type="hidden" class="form-control" value="<?php echo $idform_1;?>"/>
            								</td>
            								<td>
            									 <?php echo $nama_dinas;?>
            								</td>
            								<td rowspan="2" align="center" style="vertical-align:middle">
                              <label class="col-md-5" style="height:34px; padding:8px 8px 0 0; text-align:right">TAHUN</label>
                            	<span class="col-md-7" style="padding: 0; text-align:left"><form action="<?php echo base_url('/main/Form1_update_tahun');?>" method="post">
                                <input name="idform_1" type="hidden" value="<?php echo $idform_1;?>"/>
                                <input name="tahun" type="hidden" value="<?php echo $tahun;?>"/>
              									  <select name="tahun_form" id="tahun_form" class="form-control" onchange='this.form.submit()'>
                                    <?php
                                      $tahun_berjalan = date('Y');
                                      if (isset($tahun)){
                                          if ($tahun < $tahun_berjalan && $tahun != 0) {
                                              $setelah = $tahun_berjalan - $tahun;
                                              $sebelum = $tahun - 11;
                                              // TAHUN SETELAH
                                              for($ta=$tahun+$setelah;$ta>$tahun;$ta--){
                                                echo "<option value=".$ta.">".$ta."</option>";
                                              }
                                              // TAHUN TERPILIH
                                                echo "<option value=".$tahun." selected="."selected".">".$tahun."</option>";
                                              // TAHUN SEBELUM
                                              for($tb=$tahun-1;$tb>$sebelum+$setelah;$tb--){
                                                echo "<option value=".$tb.">".$tb."</option>";
                                              }
                                          }
                                          else
                                          {
                                            $tahun = $tahun_berjalan;
                                              echo "<option value=".$tahun." selected="."selected".">".$tahun."</option>";
                                              for($t=$tahun-1;$t>$tahun-11;$t--){
                                                echo "<option value=".$t.">".$t."</option>";
                                              }
                                          }
                                      }
                                    ?>
                                  </select>
                								<noscript><input type="submit" value="Submit"></noscript>
                            	</form></span>
            								</td>
          							  </tr>
            							<tr>
            								<td>
            									 ALAMAT
            								</td>
            								<td>
            									 <?php echo $alamat;?>
            								</td>
            							</tr>
                        </tbody>
                      </table>
                      <table class="form-1">
                        <tbody>
                        <tr>
          								<td colspan="3" style="text-align:center; vertical-align:middle">
          									<h3><strong style="text-transform:uppercase">DAFTAR ISIAN PENDATAAN PKP PROVINSI <?php echo $provinsi;?> TAHUN <?=$tahun;?></strong></h3>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3" class="note note-info">
        									  Kegiatan pendataan perumahan ini dilakukan untuk memenuhi kebutuhan data dan informasi perumahan.
                            Data ini merupakan bagian dari Pembangunan Basis Data PKP Provinsi sesuai UU No. 1 Tahun 2011 tentang
                            Perumahan dan Kawasan Permukiman (PKP) pasal 17 dan Peraturan Pemerintah No. 88 Tahun 2014 tentang Pembinaan
                            Penyelenggaraan PKP pasal 18 ayat 1.<br /><br />
                            Tabel ini diisi oleh SNVT bidang Perumahan Provinsi, untuk kelengkapan datanya berkoordinasi dengan Pokja PKP
                            Provinsi, BPS Provinsi, Bappeda Provinsi, Dinas Sosial Provinsi, BKKBN Provinsi, serta rekapitulasi data dari
                            Formulir IB oleh Kabupaten/Kota. Untuk pertanyaan lebih lanjut dapat menghubungi Subdit Data dan Informasi,
                            Direktorat Perencanaan Penyediaan Perumahan melalui alamat surel
                            <a href="mailto:datinperumahan@gmail.com">datinperumahan@gmail.com</a> atau Telp./Fax. 021-7211883</br></br>
                            Daftar isian ini merupakan data sekunder hasil dari berbagai kegiatan pendataan perumahan, terdiri dari:<br />
                            <ul style="list-style-type: none; padding: 0 0 0 10px">
                              <li><a href="#k1_div">K.1. Kelembagaan Perumahan dan Permukiman</a></li>
                              <li><a href="#k2_a_div">K.2. Alokasi APBD untuk Urusan Perumahan dan Kawasan Permukiman (PKP)</a></li>
                              <li><a href="#k3_div">K.3. Pembangunan Perumahan</a></li>
                              <li><a href="#k4_1_div">K.4. Backlog Perumahan</a></li>
                              <li><a href="#k5_div">K.5. Rumah Tidak Layak Huni</a></li>
                              <li><a href="#k6_div">K.6. Kawasan Kumuh</a></li>
                            </ul>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3" height="50">
                            <div id="k1_div"></div>
          									<h4><strong>K.1 KELEMBAGAAN PERUMAHAN DAN PERMUKIMAN</strong></h4>
                            <p>Struktur organisasi dinas yang menangani perumahan dan permukiman di Provinsi <?php echo $provinsi;?></p>
                              <center>
                              <?php foreach($form_1_k1_count as $f1k1count):
                                  if ($f1k1count->f1k1count<1) {?>
                              		<img src="../../../assets/uploads/sample.jpg" height="400" width="700"></img><p></p>
                              	<?php } else {?>
                              <img src="<?php foreach($form_1_k1 as $f1k1): ?><?php echo ".".$f1k1->struktur_dinas;?><?php endforeach;?>" height="400" width="700"></img><p></p>
                              <?php } endforeach;?>
                              <form action="<?php echo site_url('main/upload'); ?>" class="form-horizontal" enctype="multipart/form-data" id="form-upload" method="post" />
                                <table width="300">
                                  <tr>
                                    <td>
                                      <input type="hidden" name="idform_1" value="<?=$idform_1;?>" class="form-control" />
                                      <input type="file" name="image" class="form-control" id="gambar"/>
                                    </td>
                                    <td>
                                      <input type="submit" value="Unggah" class="btn blue" style="border-radius:0 8px 8px 0 !important"/>
                                    </td>
                                  </tr>
                                </table>
                              </form>
                            </center>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3" height="50" class="note note-info">
                           <strong>Penjelasan:</strong>
                           <p>Tidak ada</p>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3" height="50">
                            <div id="k2_a_div">
          									  <h4><strong>K.2 ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN PERMUKIMAN (PKP)</strong></h4>
                              <p>1. Rasio Anggaran PKP Terhadap APBD</p>
                                <table id="user" class="table table-advance table-bordered table-hover">
              										<thead>
              										<tr>
              											<th style="width:50%; text-align:center"><strong>Uraian</strong></th>
              											<th style="width:20%; text-align:center"><strong>Tahun <?=$tahun-1;?> (Rp)</strong></th>
              											<th style="width:20%; text-align:center"><strong>Tahun <?=$tahun;?> (Rp)</strong></th>
              											<th style="width:10%; text-align:center"><strong>Fungsi</strong></th>
              										</tr>
              										<tr>
              											<th style="text-align:center"><font size="1">(1)</font></th>
              											<th style="text-align:center"><font size="1">(2)</font></th>
              											<th style="text-align:center"><font size="1">(3)</font></th>
              											<th style="text-align:center"><font size="1">(4)</font></th>
              										</tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach($form_1_k2_a as $f1k2av): ?>
              										<tr>
              											<td align="center"><?php echo $f1k2av->uraian1; ?></td>
              											<td align="right" ><?php echo "Rp ".number_format( $f1k2av->k221 , 2 , ',' , '.' );?></td>
              											<td align="right" ><?php echo "Rp ".number_format( $f1k2av->k231 , 2 , ',' , '.' ) ;?></td>
              											<td align="center">
                                      <a class="btn default btn-sm blue-hoki" data-toggle="modal" href="#k2_modal" onClick="ubah_k2_modal('<?php echo $f1k2av->uraian1;?>','<?php echo $f1k2av->k221;?>','<?php echo $f1k2av->k231;?>','<?php echo $f1k2av->idform_1_k2_a;?>')"> Ubah </a>
                                    </td>
              										</tr>
                                  <?php endforeach; ?>
              							  		</tbody>
              									</table>
                              </div>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3" height="50" class="note note-info">
                            <?php
                            $apbd  = $form_1_k2_a[0];
                            $pkp   = $form_1_k2_a[1];
                            // Kalau nilai tidak nol
                            if ($apbd->k231 != 0 || $pkp->k231 != 0) {
                              $rasio      = number_format($pkp->k231/$apbd->k231,2,',','.');
                              $persentase = number_format(($pkp->k231/$apbd->k231)*100,2,',','.');
                            }
                            // Kalau nilai nol
                            else {
                              $rasio      = "...";
                              $persentase = "...";
                            }
                            ?>
                            <strong>Penjelasan:</strong>
                            <p>Anggaran total APBD Provinsi <?php echo $provinsi; ?> terhadap anggaran SKPD/Dinas PKP Provinsi
                            <?php echo $provinsi; ?> dalam tahun yang sama, sehingga didapat persentase perbandingan anggaran urusan PKP,
                            yaitu: <?php echo $rasio." atau ".$persentase."%"; ?></p>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3">
                            <p>2. Uraian Program PKP</p>
                            <a class="btn btn-sm green" data-toggle="modal" href="#k2b_modal" onClick="tambah_k2_b_modal()"> Tambah Data </a>
                            <p></p>
                            <div id="k2_b_div">
                              <table id="user" class="table table-advance table-bordered table-condensed table-striped table-hover">
          										<thead>
            										<tr>
            											<th width="4%"  style="text-align: center" rowspan="2"><strong>No.</strong></th>
            											<th width="33%" style="text-align: center" rowspan="2"><strong>Jenis Kegiatan Urusan PKP</strong></th>
            											<th width="25%" style="text-align: center" colspan="2"><strong>TA. <?=$tahun-1;?></strong></th>
            											<th width="25%" style="text-align: center" colspan="2"><strong>TA. <?=$tahun;?></strong></th>
            											<th width="13%" style="text-align: center" rowspan="2"><strong>Fungsi</strong></th>
            										</tr>
            										<tr>
            											<th width="8%"  style="text-align: center"><strong>Volume/Unit</strong></th>
            											<th width="17%" style="text-align: center"><strong>Biaya (Rp)</strong></th>
            											<th width="8%"  style="text-align: center"><strong>Volume/Unit</strong></th>
            											<th width="17%" style="text-align: center"><strong>Biaya (Rp)</strong></th>
            										</tr>
            										<tr>
            											<th style="text-align: center"><font size="1">(1)</font></th>
            											<th style="text-align: center"><font size="1">(2)</font></th>
            											<th style="text-align: center"><font size="1">(3)</font></th>
            											<th style="text-align: center"><font size="1">(4)</font></th>
            											<th style="text-align: center"><font size="1">(5)</font></th>
            											<th style="text-align: center"><font size="1">(6)</font></th>
            											<th style="text-align: center"><font size="1">(7)</font></th>
            										</tr>
                              </thead>
                              <tbody>
                              <?php $i=0; foreach($form_1_k2_b as $f1k2b3): ?>
          										<tr>
          											<td align="center"><?php $i++; echo $i;?>.</td>
          											<td align="left" ><?php echo $f1k2b3->jenis_kegiatan_urusan_pkp_2;?></td>
          											<td align="right"><?php echo number_format( $f1k2b3->ta_a_vol_unit_3 , 0 , ',' , '.' ) ;?></td>
          											<td align="right"><?php echo "Rp ".number_format( $f1k2b3->ta_a_biaya_4 , 2 , ',' , '.' ) ;?></td>
          											<td align="right"><?php echo number_format( $f1k2b3->ta_a_vol_unit_5 , 0 , ',' , '.' ) ;?></td>
          											<td align="right"><?php echo "Rp ".number_format( $f1k2b3->ta_a_biaya_6 , 2 , ',' , '.' ) ;?> </td>
                                <td align="center">
                                  <a class="btn default btn-sm blue-hoki" data-toggle="modal" href="#k2b_modal" onClick="ubah_k2_b_modal('<?php echo $f1k2b3->jenis_kegiatan_urusan_pkp_2;?>','<?php echo $f1k2b3->ta_a_vol_unit_3;?>','<?php echo $f1k2b3->ta_a_biaya_4;?>','<?php echo $f1k2b3->ta_a_vol_unit_5;?>','<?php echo $f1k2b3->ta_a_biaya_6;?>','<?php echo $f1k2b3->idform_1_k2_b;?>')">Ubah</a>
          									      <a class="btn default btn-sm red-sunglo" href="javascript:confirmDelete2(<?=$f1k2b3->idform_1_k2_b;?>)"> Hapus </a>
                                </td>
          										</tr>
                              <?php endforeach; ?>
                              <?php $i=0; foreach($form_1_k2_b_sum as $f1k2b3sum): ?>
          											<td align="center"></td>
          											<td align="right"><strong>Total</strong></font></td>
          											<td align="right"><strong><?php echo number_format( $f1k2b3sum->total_unit_rumah_1 , 0 , ',' , '.' ) ;?></strong></font></td>
          											<td align="right"><strong><?php echo "Rp ".number_format( $f1k2b3sum->total_biaya_1 , 2 , ',' , '.' ) ;?></strong></td>
          											<td align="right"><strong><?php echo number_format( $f1k2b3sum->total_unit_rumah_2 , 0 , ',' , '.' ) ;?></strong></td>
          											<td align="right"><strong><?php echo "Rp ".number_format( $f1k2b3sum->total_biaya_2 , 2 , ',' , '.' ) ;?></strong></td>
          											<td align="center"></td>
                              <?php endforeach; ?>
          										</tr>
          							  		</tbody>
          									</table>
                              <i>*) <strong>Total unit rumah</strong> dikumpulkan pada acara Forum Sinkronisasi
                                Pendataan Perumahan sebagai data Program Suplai Perumahan</i>
                            </div>
                          </td>
          							</tr>
          							<tr>
          								<td colspan="3" height="50" class="note note-info">
          									 <strong>Penjelasan :</strong>
                             <p>Kegiatan fisik, biasanya rehabilitasi rumah berupa peningkatan kualitas yang
                               bersumber dari APBD Provinsi/Anggaran Dinas PKP Provinsi. Satuannya berupa unit,
                               dihimpun sebagai data pembangunan unit baru lingkup Program Suplai Perumahan</p>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3" height="50">
                            <div id="k3_div">
          								    <h4><strong>K.3 PEMBANGUNAN PERUMAHAN</strong></h4>
                              <p>Dokumen perencanaan dan sumber dana untuk pembangunan perumahan:</p>
                              <p><?php $i=0; foreach($form_1_k3_count as $f1k3c):
                              if($f1k3c->c==0){?>
                              <a class=" btn btn-sm green" data-toggle="modal" href="#k3_modal" onClick="tambah_k3_modal()">Tambah Data</a></p><?php } endforeach;?>
                              <?php $i=0; foreach($form_1_k3 as $f1k3): ?>
                              <p>1. Apakah pemerintah provinsi mempunyai dokumen perencanaan urusan perumahan?</p>
                              <div class="checkbox-list" data-error-container="#form_2_services_error">
                                <p class="indent"><label>
        												<input type="checkbox" name="isi_1a" <?php if($f1k3->isi_1a=='Y') { echo "checked"; } ?> value="Y" disabled/> a. RTRW (sudah perda) </label></p>
        												<p class="indent"><label>
        												<input type="checkbox" name="isi_1b" <?php if($f1k3->isi_1b=='Y') { echo "checked"; } ?> value="Y" disabled/> b. RDTR (sudah perda) </label></p>
        												<p class="indent"><label>
        												<input type="checkbox" name="isi_1c" <?php if($f1k3->isi_1c=='Y') { echo "checked"; } ?> value="Y" disabled/> c. RP3KP/RP4D </label></p>
        												<p class="indent"><label>
        												<input type="checkbox" name="isi_1d" <?php if($f1k3->isi_1d=='Y') { echo "checked"; } ?> value="Y" disabled/> d. RPIJM </label></p>
        												<p class="indent"><label>
        												<input type="checkbox" name="isi_1e" <?php if($f1k3->isi_1e=='Y') { echo "checked"; } ?> value="Y" disabled/> e. Renstra Dinas PKP </label></p>
        												<p class="indent"><label>
        												<input type="checkbox" name="isi_1f" <?php if($f1k3->isi_1f=='Y') { echo "checked"; } ?> value="Y" disabled/>	f. Lainnya (tuliskan):
        												<?php echo $f1k3->isi_1f_keterangan;?></label></p>
                              </div>
      											  <p></p>
                              <p>2. Dalam pelaksanaan urusan perumahan, pemerintah provinsi memanfaatkan sumber dana yang berasal dari:</p>
                              <div class="checkbox-list" data-error-container="#form_2_services_error">
        												<p class="indent">
        												<label><input type="checkbox" name="isi_2a" <?php if($f1k3->isi_2a=='Y') { echo "checked"; } ?> value="Y" disabled/> a. APBD </label></p>
        												<p class="indent">
        												<label><input type="checkbox" name="isi_2b" <?php if($f1k3->isi_2b=='Y') { echo "checked"; } ?> value="Y" disabled/> b. Loan (pinjaman) dari badan/bank luar negeri </label></p>
        												<p class="indent">
        												<label><input type="checkbox" name="isi_2c" <?php if($f1k3->isi_2c=='Y') { echo "checked"; } ?> value="Y" disabled/> c. CSR (uraikan):
        												<?php echo $f1k3->isi_2c_keterangan;?></label></p>
        												<p class="indent">
        												<label><input type="checkbox" name="isi_2d" <?php if($f1k3->isi_2d=='Y') { echo "checked"; } ?> value="Y" disabled/> d. Swasta (uraikan):
        												<?php echo $f1k3->isi_2d_keterangan;?></label></p>
        												<p class="indent">
                                <label><input type="checkbox" name="isi_2e" <?php if($f1k3->isi_2e=='Y') { echo "checked"; } ?> value="Y" disabled/> e. Lainnya (tuliskan): APBN </label></p>
                                <p><a class="btn btn-sm blue-hoki" data-toggle="modal" href="#k3_modal" onClick="ubah_k3_modal(<?php if($f1k3->isi_1a=="Y"){echo 1;}else{echo 0;}?>,<?php if($f1k3->isi_1b=="Y"){echo 1;}else{echo 0;}?>,<?php if($f1k3->isi_1c=="Y"){echo 1;}else{echo 0;}?>,<?php if($f1k3->isi_1d=="Y"){echo 1;}else{echo 0;}?>,<?php if($f1k3->isi_1e=="Y"){echo 1;}else{echo 0;}?>,<?php if($f1k3->isi_1f=="Y"){echo 1;}else{echo 0;}?>,'<?=$f1k3->isi_1f_keterangan;?>',<?php if($f1k3->isi_2a=="Y"){echo 1;}else{echo 0;}?>,<?php if($f1k3->isi_2b=="Y"){echo 1;}else{echo 0;}?>,<?php if($f1k3->isi_2c=="Y"){echo 1;}else{echo 0;}?>,<?php if($f1k3->isi_2d=="Y"){echo 1;}else{echo 0;}?>,<?php if($f1k3->isi_2e=="Y"){echo 1;}else{echo 0;}?>,'<?=$f1k3->isi_2c_keterangan;?>','<?=$f1k3->isi_2d_keterangan;?>',<?php echo $f1k3->idform_1_k3;?>)">&emsp; Ubah &emsp;</a></p>
                                <?php endforeach;?>
                              </div>
                            </div>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3" height="50" class="note note-info">
                            <strong>Penjelasan:</strong>
                            <p>Kelengkapan dokumen kelengkapan/pendukung program/kegiatan
                              Perumahan dan Kawasan Permukiman yang telah ada dasar legalisasinya
                              seperti perda atau pergub, dan juga sumber dana yang pernah
                              digunakan untuk pembangunan/rehabilitasi rumah di provinsi</p>
          								</td>
          							</tr>
          							<tr>
          							  <td colspan="3" height="50">
                            <div id="k4_1_div">
          									  <h4><strong>K.4 BACKLOG PERUMAHAN</strong></h4>
          									  <p>1. Jumlah Rumah Berdasarkan Status Kepemilikan Tempat Tinggal </p>
                              <?php $i=0; foreach($form_1_k4_1_count as $f1k41c): if($f1k41c->f1k41count==0){?>
                              <p><a class="btn btn-sm green" data-toggle="modal" href="#k4_1_modal" onClick="tambah_k4_1_modal()">Tambah Data</a></p><?php } endforeach;?>
                              <?php $i=0; foreach($form_1_k4_1 as $f1k41): ?>
                              <p><a class="btn btn-sm blue-hoki" data-toggle="modal" href="#k4_1_modal" onClick="ubah_k4_1_modal(<?php echo $f1k41->k4141;?>,<?php echo $f1k41->k4142;?>,<?php echo $f1k41->k4143;?>,<?php echo $f1k41->k4144;?>,<?php echo $f1k41->k4145;?>,'<?php echo $f1k41->k4151;?>','<?php echo $f1k41->k4152;?>','<?php echo $f1k41->k4153;?>','<?php echo $f1k41->k4154;?>','<?php echo $f1k41->k4155;?>','<?php echo $f1k41->status1;?>','<?php echo $f1k41->status2;?>','<?php echo $f1k41->status3;?>','<?php echo $f1k41->status4;?>','<?php echo $f1k41->status5;?>',<?php echo $f1k41->idform_1_k4_1;?>)">Ubah</a></p>
                              <table id="user" class="table table-advance table-bordered table-striped table-hover">
            										<thead>
            										<tr>
            											<th style="width:5%" align="center"><strong>No.</strong></th>
            											<th colspan="2" align="center" style="width:45%"><strong>Status Kepemilikan Tempat Tinggal</strong></th>
            											<th style="width:25%" align="center"><strong>Jumlah (unit)</strong></th>
            											<th style="width:25%" align="center"><strong>Sumber Data</strong></th>
            										</tr>
            										<tr>
            											<th align="center"><font size="1">(1)</font></th>
            											<th colspan="2" align="center"><font size="1">(2)</font></th>
            											<th align="center"><font size="1">(3)</font></th>
            											<th align="center"><font size="1">(4)</font></th>
            										</tr>
                                </thead>
                                <tbody>
            										<tr>
            											<td align="center">1.</td>
            											<td colspan="2"   ><?php echo $f1k41->status1;?></font></td>
            											<td align="center"><?php echo number_format( $f1k41->k4141 , 0 , ',' , '.' ) ;?></td>
            											<td align="center"><?php echo $f1k41->k4151;?></td>
            										</tr>
            										<tr>
            											<td align="center">2.</td>
            											<td colspan="2"   ><?php echo $f1k41->status2;?></td>
            											<td align="center"><?php echo number_format( $f1k41->k4142 , 0 , ',' , '.' ) ;?></td>
            											<td align="center"><?php echo $f1k41->k4152;?></td>
            										</tr>
            										<tr>
            											<td align="center">3.</td>
            											<td colspan="2"   ><?php echo $f1k41->status3;?></td>
            											<td align="center"><?php echo number_format( $f1k41->k4143 , 0 , ',' , '.' ) ;?></td>
            											<td align="center"><?php echo $f1k41->k4153;?></td>
            										</tr>
            										<tr>
            											<td align="center">4.</td>
            											<td colspan="2"   ><?php echo $f1k41->status4;?></td>
            											<td align="center"><?php echo number_format( $f1k41->k4144 , 0, ',' , '.' ) ;?></td>
            											<td align="center"><?php echo $f1k41->k4154;?></td>
            										</tr>
            										<tr>
            											<td align="center">5.</td>
            											<td colspan="2"   ><?php echo $f1k41->status5;?></td>
            											<td align="center"><?php echo number_format( $f1k41->k4145 , 0 , ',' , '.' ) ;?></td>
            											<td align="center"><?php echo $f1k41->k4155;?></td>
            										</tr>
            										<tr>
            											<td align="center"></td>
            											<td colspan="2" align="right"><strong>Total A (2+3+4+5)</strong></td>
            											<td align="center"><?php echo number_format( $f1k41->total_a , 0 , ',' , '.' ) ;?></td>
            											<td align="center">&nbsp;</td>
            										</tr>
            										<tr>
            											<td align="center"></td>
            											<td colspan="2" align="right"><strong>Total B (3)</strong></td>
            											<td align="center"><?php echo number_format( $f1k41->total_b , 0 , ',' , '.' ) ;?></td>
            											<td align="center">&nbsp;</td>
            										</tr>
            							  		</tbody>
              								</table>
                              <p>Backlog Kepemilikan&nbsp;= <strong>(Total A) = <?php echo number_format( $f1k41->total_a , 0 , ',' , '.' ) ;?> unit</strong></p>
                              <p>Backlog Penghunian&nbsp;&nbsp;= <strong>(Total B) = <?php echo number_format( $f1k41->total_b , 0 , ',' , '.' ) ;?> unit</strong></p>
                            <?php endforeach;?>
                            </div>
                          </td>
          							</tr>
          							<tr>
          								<td colspan="3" class="note note-info">
                            <strong>Penjelasan:</strong>
                            <p>Data backlog kepemilikan dan backlog penghunian kabupaten ditentukan oleh Dinas PKP, data dari BPS dan BKKBN hanya sebagai benchmark data yang perlu divalidasi oleh Dinas PKP.</p>
                            <strong>Catatan:</strong>
                            <p>data BKKBN bersumber dari sensus Pendataan Keluarga 2015 oleh BKKBN, yang telah menandatangani Kesepakatan Bersama dengan Kementerian PUPR Nomor:1/KSM/G2/2017 Nomor:01/PKS/M/2017 tentang Peningkatan Program Kependudukan, Keluarga Berencana dan Pembangunan Keluarga dalam Pembangunan Infrastruktur Pekerjaan Umum dan Perumahan Rakyat, tanggal 11 Januari 2017</p>
          								</td>
          							</tr>
          							<tr>
          							  <td colspan="3">
                            <div id="k4_2_div">
          									<p>2. Jenis Fisik Bangunan Rumah</p>
                            <?php $i=0; foreach($form_1_k4_2_count as $f1k42c): if($f1k42c->f1k42count==0){?>
                            <p><a class="btn btn-sm green" data-toggle="modal" href="#k4_2_modal" onClick="tambah_k4_2_modal()">Tambah Data</a><?php } endforeach;?>
                            <?php $i=0; foreach($form_1_k4_2 as $f1k42): ?>
                            <a class="btn btn-sm blue-hoki" data-toggle="modal" href="#k4_2_modal" onClick="ubah_k4_2_modal('<?php echo $f1k42->k4231;?>','<?php echo $f1k42->k4232;?>','<?php echo $f1k42->k4241;?>','<?php echo $f1k42->k4242;?>','<?php echo $f1k42->jenis1;?>','<?php echo $f1k42->jenis2;?>',<?php echo $f1k42->idform_1_k4_2;?>)">Ubah</a></p>
                            <table id="user" class="table table-advance table-bordered table-striped table-hover">
          										<thead>
          										<tr>
          											<th style="width:5%"  align="center"><strong>No.</strong></th>
          											<th style="width:45%" align="center"><strong>Jenis Fisik Bangunan Rumah</strong></th>
          										  <th style="width:25%" align="center"><strong>Jumlah (unit)</strong></th>
          										  <th style="width:25%" align="center"><strong>Sumber Data</strong></th>
          										</tr>
          										<tr>
          											<th align="center"><font size="1">(1)</font></th>
          											<th align="center"><font size="1">(2)</font></th>
          											<th align="center"><font size="1">(3)</font></th>
                                <th align="center"><font size="1">(4)</font></th>
          										</tr>
                              </thead>
                              <tbody>
          										<tr>
          											<td align="center">1.</td>
          											<td align="left"  ><?php echo $f1k42->jenis1;?></td>
          											<td align="center"><?php echo number_format( $f1k42->k4231 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo $f1k42->k4241;?></td>
          										</tr>
          										<tr>
          											<td align="center">2.</td>
          											<td align="left"  ><?php echo $f1k42->jenis2;?></td>
          											<td align="center"><?php echo number_format( $f1k42->k4232 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo $f1k42->k4242;?></td>
          										</tr>
          										<tr>
          											<td align="center"></td>
          											<td align="right" ><strong>Total</strong></td>
          											<td align="center"><strong><?php echo number_format( $f1k42->total , 0 , ',' , '.' ) ;?></strong></td>
          											<td align="center">&nbsp;</td>
          										</tr>
          							  		</tbody>
          							    </table>
                            <?php endforeach;?>
                            </div>
                          </td>
          							</tr>
          							<tr>
          								<td colspan="3" class="note note-info">
          									<strong>Penjelasan:</strong>
                            <p>Jumlah fisik bangunan rumah, bukan data Rumah Tangga atau data
                              Kepala Keluarga, tapi merupakan unit bangunan rumah baik terhuni
                              maupun tidak.</p>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3">
                            <div id="k4_3_div">
          									<p>3. Jumlah Kepala Keluarga (KK) dalam satu rumah</p>
                            <?php $i=0; foreach($form_1_k4_3_count as $f1k43c): if($f1k43c->f1k43count==0){?>
                            <p><a class="btn btn-sm green" data-toggle="modal" href="#k4_3_modal" onClick="tambah_k4_3_modal()">Tambah Data</a><?php } endforeach;?>
          									<?php $i=0; foreach($form_1_k4_3 as $f1k43): ?>
                            <a class="btn btn-sm blue-hoki" data-toggle="modal" href="#k4_3_modal" onClick="ubah_k4_3_modal('<?php echo $f1k43->k4331;?>','<?php echo $f1k43->k4332;?>','<?php echo $f1k43->k4341;?>','<?php echo $f1k43->k4342;?>','<?php echo $f1k43->fungsi1;?>','<?php echo $f1k43->fungsi2;?>',<?php echo $f1k43->idform_1_k4_3;?>)">Ubah</a></p>
                            <table id="user" class="table table-advance table-bordered table-striped table-hover">
          										<thead>
          										<tr>
          											<th style="width:5%"  align="center"><strong>No.</strong></th>
          											<th style="width:45%" align="center"><strong>Fungsi Rumah</strong></th>
          										  <th style="width:25%" align="center"><strong>Jumlah (unit)</strong></th>
          										  <th style="width:25%" align="center"><strong>Sumber Data</strong></th>
          										</tr>
          										<tr>
          											<th align="center"><font size="1">(1)</font></th>
          											<th align="center"><font size="1">(2)</font></th>
          											<th align="center"><font size="1">(3)</font></th>
                                <th align="center"><font size="1">(4)</font></th>
          										</tr>
                              </thead>
                              <tbody>
          										<tr>
          											<td align="center" >1.</td>
          											<td align="left"><?php echo $f1k43->fungsi1;?></td>
          											<td align="center" ><?php echo number_format( $f1k43->k4331 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo $f1k43->k4341;?></td>
          										</tr>
          										<tr>
          											<td align="center" >2.</td>
          											<td align="left"><?php echo $f1k43->fungsi2;?></td>
          											<td align="center" ><?php echo number_format( $f1k43->k4332 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo $f1k43->k4342;?></td>
          										</tr>
          							  		</tbody>
          								  </table>
                            <?php endforeach;?>
                            </div>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3" class="note note-info">
                            <strong>Penjelasan:</strong>
          								  <p>Jumlah KK lebih dari 1 (satu) dalam satu rumah merupakan bagian dari data backlog penghunian.</p>
          								</td>
          							</tr>
                        <tr>
          								<td colspan="3">
                            <div id="k4_4_div">
          								  <p>4. Jumlah sambungan listrik rumah (PLN)</p>
                            <p><a class="btn btn-sm green" data-toggle="modal" href="#k4_4_modal" onclick="tambah_k4_4_modal()">Tambah Data</a></p>
                            <table id="user" class="table table-advance table-bordered table-striped table-hover">
          										<thead>
          										<tr>
          											<th style="width:5%"  align="center" rowspan="2"><strong>No.</strong></th>
          											<th style="width:30%" align="center" rowspan="2"><strong>Kabupaten/Kota</strong></th>
          										  <th style="width:25%" align="center" colspan="2"><strong>Jumlah rumah (unit)</strong></th>
          										  <th style="width:25%" align="center" rowspan="2"><strong>Sumber Data</strong></th>
          										  <th style="width:30%" align="center" rowspan="2"><strong>Fungsi</strong></th>
          										</tr>
          										<tr>
          											<th align="center"><strong><?=$tahun-1;?></strong></th>
          											<th align="center"><strong><?=$tahun;  ?></strong></th>
          										</tr>
          										<tr>
          											<th align="center"><font size="1">(1)</font></th>
          											<th align="center"><font size="1">(2)</font></th>
          											<th align="center"><font size="1">(3)</font></th>
                                <th align="center"><font size="1">(4)</font></th>
                                <th align="center"><font size="1">(5)</font></th>
                                <th align="center"><font size="1">(6)</font></th>
          										</tr>
                              </thead>
                              <tbody>
                              <?php $i=0; foreach($form_1_k4_4 as $f1k44): ?>
          										<tr>
          											<td align="center"><?php $i++; echo $i;?>.</td>
          											<td align="left"  ><?php echo $f1k44->kabupaten_kota;?></td>
          											<td align="center"><?php echo number_format( $f1k44->jumlah_rumah_3 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo number_format( $f1k44->jumlah_rumah_4 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo $f1k44->sumber_data_5;?></td>
                                <td align="center">
                                  <a class="btn btn-sm blue-hoki" data-toggle="modal" href="#k4_4_modal" onClick="ubah_k4_4_modal('<?php echo $f1k44->idkabupaten_kota;?>',<?php echo $f1k44->jumlah_rumah_3;?>,<?php echo $f1k44->jumlah_rumah_4;?>,'<?php echo $f1k44->sumber_data_5;?>',<?php echo $f1k44->idform_1_k4_4;?>)">Ubah</a>
          									      <a href="javascript:confirmDelete5(<?=$f1k44->idform_1_k4_4;?>)" class="btn btn-sm red-sunglo">Hapus</a>
                                </td>
          										</tr>
                              <?php endforeach;?>
          										<tr>
                                <?php $i=0; foreach($form_1_k4_4_sum as $f1k44sum): ?>
          											<td colspan="2" align="right"><strong>Total</strong></td>
          											<td align="center"><strong><?php echo number_format( $f1k44sum->total_1 , 0 , ',' , '.' ) ;?></strong></td>
          											<td align="center"><strong><?php echo number_format( $f1k44sum->total_2 , 0 , ',' , '.' ) ;?></strong></td>
                                <td colspan="2">&nbsp;</td>
                                <?php endforeach;?>
          										</tr>
          							  		</tbody>
          								  </table>
                            </div>
          								</td>
          							</tr>
          							<tr>
          								<td colspan="3" class="note note-info">
          									<strong>Penjelasan:</strong>
                            <p>Data sambungan listrik berasal dari PLN, per-kabupaten/kota.</p>
          								</td>
          							</tr>
                        <tr>
          								<td colspan="3">
                            <div id="k4_5_div">
          									<p>5. Jumlah Pembangunan Rumah dalam 1 Tahun</p>
                            <p><a class="btn btn-sm green" data-toggle="modal" href="#k4_5_modal" onClick="tambah_k4_5_modal()">Tambah Data</a></p>
                            <table id="user" class="table table-advance table-bordered table-striped table-hover">
          										<thead>
          										<tr>
          											<th style="width:5%"  align="center" rowspan="2"><strong>No.</strong></th>
          											<th style="width:20%" align="center" colspan="2"><strong>Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun-1;?> (unit)</strong></th>
          										  <th style="width:20%" align="center" colspan="2"><strong>Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun;?> (unit)</strong></th>
                                <th style="width:20%" align="center" colspan="2"><strong>Jumlah Pembangunan Rumah Berdasarkan Non IMB (unit).....Tahun</font></strong></th>
          										  <th align="center" rowspan="2"><strong>Sumber Data</strong></th>
          										  <th align="center" rowspan="2"><strong>Fungsi</strong></th>
          										</tr>
          										<tr>
          											<th align="center">Non MBR</th>
          											<th align="center">MBR</th>
          											<th align="center">Non MBR</th>
          											<th align="center">MBR</th>
          											<th align="center">Non MBR</th>
          											<th align="center">MBR</th>
          										</tr>
          										<tr>
          											<th align="center"><font size="1">(1)</font></th>
          											<th align="center"><font size="1">(2)</font></th>
          											<th align="center"><font size="1">(3)</font></th>
                                <th align="center"><font size="1">(4)</font></th>
          											<th align="center"><font size="1">(5)</font></th>
                                <th align="center"><font size="1">(6)</font></th>
          											<th align="center"><font size="1">(7)</font></th>
                                <th align="center"><font size="1">(8)</font></th>
                                <th align="center"><font size="1">(9)</font></th>
          										</tr>
                              </thead>
                              <tbody>
                              <?php $i=0; foreach($form_1_k4_5 as $f1k45): ?>
          										<tr>
          											<td align="center"><?php $i++; echo $i;?>.</td>
          											<td align="center"><?php echo number_format( $f1k45->non_mbr_2 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo number_format( $f1k45->mbr_3 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo number_format( $f1k45->non_mbr_4 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo number_format( $f1k45->mbr_5 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo number_format( $f1k45->non_mbr_6 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo number_format( $f1k45->mbr_7 , 0 , ',' , '.' ) ;?></td>
          											<td align="center"><?php echo $f1k45->sumber_data_8;?></td>
                                <td align="center">
                                  <a class="btn btn-sm blue-hoki" data-toggle="modal" href="#k4_5_modal" onClick="ubah_k4_5_modal('<?php echo $f1k45->non_mbr_2;?>','<?php echo $f1k45->mbr_3;?>','<?php echo $f1k45->non_mbr_4;?>','<?php echo $f1k45->mbr_5;?>','<?php echo $f1k45->non_mbr_6;?>','<?php echo $f1k45->mbr_7;?>','<?php echo $f1k45->sumber_data_8;?>','<?php echo $f1k45->idform_1_k4_5;?>')"> Ubah </a>
          									      <a href="javascript:confirmDelete6(<?=$f1k45->idform_1_k4_5;?>)" class="btn btn-sm red-sunglo"> Hapus </a>
                                </td>
          										</tr>
                              <?php endforeach;?>
          							  		</tbody>
          								  </table>
                              <p>*)<strong>Jumlah Pembangunan Rumah Berdasarkan IMB 2016 (unit)</strong> dikumpulkan pada acara Forum Sinkronisasi Pendataan Perumahan sebagai data Program Suplai Perumahan (identifikasi awal perkiraan pembangunan rumah)</p>
                            </div>
                          </td>
          							</tr>
          							<tr>
          								<td colspan="3" class="note note-info">
                            <strong>Penjelasan:</strong>
                            <p>Data pembangunan unit rumah per-tahun oleh seluruh stakeholder perumahan
                              terdata dalam IMB melalui Dinas PTSP, dan jumlah yang tidak terdata dalam
                              IMB dianalisis/diasumsikan oleh Dinas PKP, sehingga didapat perkiraan
                              pembangunan unit rumah satu kabupaten dalam satu tahun. Ini disebut data
                              supply perumahan. Data total dari seluruh Kabupaten/Kota</p>
          								</td>
          							</tr>
          							<tr>
          							  <td colspan="3">
                          <div id="k5_div">
                            <h4><strong>K.5 RUMAH TIDAK LAYAK HUNI</strong></h4>
                            <p><a class="btn btn-sm green" data-toggle="modal" href="#k5_modal" onClick="tambah_k5_modal()">Tambah Data</a></p>
                              <table id="user" class="table table-advance table-bordered table-striped table-hover">
            										<thead>
            										<tr>
            											<th style="width:5%"  align="center"><strong>No.</strong></th>
            											<th style="width:25%" align="center"><strong>Kab/Kota</strong></th>
            											<th style="width:15%" align="center"><strong>Jumlah  KK/RT</strong></th>
            											<th style="width:15%" align="center"><strong>Jumlah  RTLH Versi BDT (unit)</strong></th>
            											<th style="width:15%" align="center"><strong>Jumlah RTLH  Verifikasi Pemda (unit)</strong></th>
                                  <th align="center"><strong>Sumber Data</strong></th>
                                  <th align="center"><strong>Fungsi</strong></th>
            										</tr>
            										<tr>
            											<th align="center"><font size="1">(1)</font></th>
            											<th align="center"><font size="1">(2)</font></th>
            											<th align="center"><font size="1">(3)</font></th>
            											<th align="center"><font size="1">(4)</font></th>
            											<th align="center"><font size="1">(5)</font></th>
            											<th align="center"><font size="1">(6)</font></th>
            											<th align="center"><font size="1">(7)</font></th>
            										</tr>
                                </thead>
                                <tbody>
                                <?php $i=0; foreach($form_1_k5 as $f1k5): ?>
            										<tr>
                                  <td align="center"><?php $i++; echo $i;?>.</td>
                                  <td align="left"  ><?php echo $f1k5->kabupaten_kota;?></td>
                                  <td align="center"><?php echo number_format( $f1k5->jumlah_kk_rt_3 , 0 , ',' , '.' ) ;?></td>
                                  <td align="center"><?php echo number_format( $f1k5->jumlah_rtlh_versi_bdt_4 , 0 , ',' , '.' ) ;?></td>
                                  <td align="center"><?php echo number_format( $f1k5->jumlah_rtlh_verifikasi_pemda_5 , 0 , ',' , '.' ) ;?></td>
                                  <td align="center"><?php echo $f1k5->sumber_data_6;?></td>
                                  <td align="center">
                                    <a class="btn btn-sm blue-hoki" data-toggle="modal" href="#k5_modal" onClick="ubah_k5_modal('<?php echo $f1k5->idkabupaten_kota;?>','<?php echo $f1k5->jumlah_kk_rt_3;?>','<?php echo $f1k5->jumlah_rtlh_versi_bdt_4;?>','<?php echo $f1k5->jumlah_rtlh_verifikasi_pemda_5;?>','<?php echo $f1k5->sumber_data_6;?>','<?php echo $f1k5->idform_1_k5;?>')">Ubah</a>
                									  <a class="btn btn-sm red-sunglo" href="javascript:confirmDelete7(<?=$f1k5->idform_1_k5;?>)"> Hapus </a>
                                  </td>
            										</tr>
                                <?php endforeach;?>
            										<tr>
                                  <?php $i=0; foreach($form_1_k5_sum as $f1k5sum): ?>
            											<td align="right" colspan="2"><strong>Total</strong></td>
            											<td align="center"><strong> <?php echo number_format( $f1k5sum->total_1 , 0 , ',' , '.' ) ;?></strong></td>
            											<td align="center"><strong> <?php echo number_format( $f1k5sum->total_2 , 0 , ',' , '.' ) ;?></strong></td>
            											<td align="center"><strong> <?php echo number_format( $f1k5sum->total_3 , 0 , ',' , '.' ) ;?></strong></td>
                                  <td colspan="2">&nbsp;</td>
                                  <?php endforeach;?>
            										</tr>
            							  		</tbody>
            								  </table>
                          </div>
          							  </td>
          							</tr>
          							<tr>
          								<td colspan="3" class="note note-info">
          									<strong>Penjelasan:</strong>
                            <p>Data RTLH merupakan data yang sama dengan Sistem Informasi
                              Rumah Swadaya, yang bersumber dari data olahan BDT 2015</p>
          								</td>
          							</tr>
                        <tr>
          							  <td colspan="3">
                            <div id="k6_div">
                              <h4><strong>K.6 KAWASAN KUMUH</strong></h4>
                              <p><a class="btn btn-sm green" data-toggle="modal" href="#k6_modal" onClick="tambah_k6_modal()">Tambah Data</a></p>
                              <table id="user" class="table table-advance table-bordered table-striped table-hover">
            										<thead>
            										<tr>
            											<th style="width:5%"  align="center"><strong>No.</strong></th>
            											<th style="width:20%" align="center"><strong>Kabupaten/Kota</strong></th>
            											<th style="width:20%" align="center"><strong>Kecamatan</strong></th>
            											<th style="width:15%" align="center"><strong>Luas Wilayah Kumuh (Ha)</strong></th>
            											<th style="width:15%" align="center"><strong>Jumlah RTLH dalam Wilayah Kumuh (unit)</strong></th>
                                  <th align="center"><strong>Sumber Data</strong></th>
                                  <th align="center"><strong>Fungsi</strong></th>
            										</tr>
            										<tr>
            											<th align="center"><font size="1">(1)</font></th>
            											<th align="center"><font size="1">(2)</font></th>
            											<th align="center"><font size="1">(3)</font></th>
            											<th align="center"><font size="1">(4)</font></th>
            											<th align="center"><font size="1">(5)</font></th>
            											<th align="center"><font size="1">(6)</font></th>
            											<th align="center"><font size="1">(7)</font></th>
            										</tr>
                                </thead>
                                <tbody>
                                <?php $i=0; foreach($form_1_k6 as $f1k6): ?>
            										<tr>
                                  <td align="center"><?php $i++; echo $i;?>.</td>
                                  <td align="left"><input name="idform_1_k6_<?php echo $i;?>" type="hidden" class="form-control" value="<?php echo $f1k6->idform_1_k6;?>"/><?php echo $f1k6->kabupaten_kota;?></td>
                                  <td >
                                  <?php
                                  		$this->db->select('idkecamatan,kecamatan');
                                      $this->db->where('idform_1_k6',$f1k6->idform_1_k6);
                                      $this->db->from('form_1_k6_kecamatan_view');
                                      $query = $this->db->get();
                                  		$x=0;
                                        foreach ($query->result() as $value) {
                                  		  $x++;
                                    		  if ($x<$query->num_rows()){
                                              echo $value->kecamatan.", ";
                                    		  }
                                    		  else
                                    		  {
                                              echo $value->kecamatan;
                                    		  }
                                        }
                                  ?>
                                  </td>
                                  <td align="center"><?php echo $f1k6->luas_wilayah_kumuh_4;?></td>
                                  <td align="center" ><?php echo $f1k6->jumlah_rtlh_dalam_wilayah_kumuh_5;?></td>
                                  <td align="center"><?php echo $f1k6->sumber_data_6;?></td>
                                  <td align="center">
                                    <a class="btn btn-sm blue-hoki" data-toggle="modal" href="#k6_modal" onClick="ubah_k6_modal('<?php echo $f1k6->luas_wilayah_kumuh_4;?>','<?php echo $f1k6->jumlah_rtlh_dalam_wilayah_kumuh_5;?>','<?php echo $f1k6->sumber_data_6;?>','<?php echo $f1k6->idform_1_k6;?>')">Ubah</a>
                                  	<a class="btn btn-sm red-sunglo" href="javascript:confirmDelete8(<?=$f1k6->idform_1_k6;?>)"> Hapus </a>
                                  </td>
            										</tr>
                                <?php endforeach;?>
            										<tr>
                                <?php $i=0; foreach($form_1_k6_sum as $f1k6sum): ?>
            											<td align="right" colspan="3"><strong>Total</strong></td>
            											<td align="center"><strong><?php echo number_format( $f1k6sum->total_luas_wilayah_kumuh , 0 , ',' , '.' ) ;?></strong></td>
            											<td align="center"><strong><?php echo number_format( $f1k6sum->total_jumlah_rtlh_dalam_wilayah_kumuh , 0 , ',' , '.' ) ;?></strong></td>
            											<td align="center" colspan="2">&nbsp;</td>
                                <?php endforeach;?>
            										</tr>
            							  		</tbody>
              								</table>
                            </div>
          							  </td>
          							</tr>
          							<tr>
          								<td colspan="3" class="note note-info">
                            <strong>Penjelasan:</strong>
                            <p>Jumlah RTLH dalam kawasan kumuh dapat diperoleh dari Satker
                              Kotaku, data ini untuk integrasi penanganan kumuh, Ditjen Penyediaan
                              Perumahan dengan program BSPS atau Rusunawa, Ditjen Cipta Karya
                              dengan penanganan infrastruktur di dalam satu kawasan. Uraian
                              per-Kabupaten/Kota</p>
          								</td>
          							</tr>
          							</tbody>
        							</table>
                  <hr />
                  <p>Demikian hasil pelaksanaan pendataan perumahan dan kawasan permukiman, dilakukan untuk dipergunakan sebagaimana mestinya.</p>
                  <?php $tanggal = date("d-M-Y"); ?>
                  <p><strong><?php echo $tanggal;?></strong></p>
                  <p>Tanda Bukti Penyetujuan,</p></br>
                  <p><strong><?php echo isset($r->kepala_dinas);?></strong></p>
                  <p>Kepala <?php echo $nama_dinas;?></p>
                  <form action="<?php echo site_url('main/upload3'); ?>" class="form-horizontal" enctype="multipart/form-data" id="form-upload2" method="post" />
                  <table width="300">
                    <tr>
                      <td>
                      <input type="hidden" name="idform" value="<?=$idform_1;?>" class="form-control" />
                      <input type="file" name="image2" class="form-control" id="gambar2"/>
                      </td>
                      <td>
                       <input type="submit" class="btn blue" value="Selesai" id="submit-button2" style="border-top-left-radius:0 !important; border-bottom-left-radius:0 !important">
                      </td>
                    </tr>
                  </table>
                  </form>
    						</div>
    					</div>
    		    </div>
    	    </div>
      	<!-- END PAGE CONTENT -->
        </div>
      </div>
    </div>
    <!-- BEGIN PORTLET MODAL FORM-->
    <!-- BEGIN modal form K1 -->
    <div class="modal fade" id="basic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Unggah Gambar Struktur Organisasi</h4>
          </div>
          <div class="modal-body">
            <!-- KOSONG -->
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn blue">Simpan Perubahan</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- END modal form K1 -->
    <!-- BEGIN modal form K2 -->
    <div id="k2_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action="" class="form-horizontal" method="post" id="k2a_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title"><strong>K.2 ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN PERMUKIMAN (PKP)</strong></h5>
            </div>
            <div class="modal-body">
              <p>
            <table id="user" class="table table-bordered table-striped form-1">
            <tbody>
              <tr>
                <td style="width:40%" align="center"><strong>Uraian</strong></td>
                <td style="width:30%" align="center"><strong>Tahun <?=$tahun-1;?> (Rp)</strong></td>
                <td style="width:30%" align="center"><strong>Tahun <?=$tahun;?> (Rp)</strong></td>
              </tr>
              <tr>
                <td align="center" >
                  <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                  <input name="idform_1_k2_a" type="hidden" class="form-control" id="idform_1_k2_a" />
                  <input name="uraian1" type="text" readonly class="form-control" id="uraian1"/>
                </td>
                <td align="center">
                  <input name="k221" type="text" class="form-control" style="background-color:#E8F3FF" id="k221" />
                </td>
                <td align="center">
                  <input name="k231" type="text" class="form-control" style="background-color:#E8F3FF" id="k231" />
                </td>
              </tr>
            </tbody>
            </table>
              </p>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn green" value="Simpan">
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div id="k2b_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action=""  class="form-horizontal" method="post" id="k2b_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title uppercase"><strong>K.2.2 Jenis Kegiatan Urusan PKP</strong></h5>
            </div>
            <div class="modal-body">
              <table id="user" class="table table-bordered table-striped form-1">
                <tbody>
                  <tr>
                    <td><strong>Jenis Kegiatan Urusan PKP</strong></td>
                    <td align="center">
                      <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                      <input name="idform_1_k2_b" id="idform_1_k2_b" type="hidden" class="form-control" />
                      <input name="jenis_kegiatan_urusan_pkp_2" id="jenis_kegiatan_urusan_pkp_2" type="text" class="form-control" style="background-color:#E8F3FF"/>
                    </td>
                  </tr>
                  <tr>
                    <td><strong>Volume/unit (TA. <?=$tahun-1;?>)</strong></td>
                    <td align="center"><input name="ta_a_vol_unit_3" id="ta_a_vol_unit_3" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                    <td><strong>Biaya (TA. <?=$tahun-1;?>)</strong></td>
                    <td align="center"><input name="ta_a_biaya_4" id="ta_a_biaya_4" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                    <td><strong>Volume/unit (TA. <?=$tahun;?>)</strong></td>
                    <td align="center"><input name="ta_a_vol_unit_5" id="ta_a_vol_unit_5" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                    <td><strong>Biaya (TA. <?=$tahun;?>)</strong></td>
                    <td align="center"><input name="ta_a_biaya_6" id="ta_a_biaya_6" type="text" class="form-control" style="background-color:#E8F3FF"/> </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn green" value="Simpan">
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <!-- END modal form K2 -->
    <!-- BEGIN modal form K3 -->
    <div id="k3_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <form action=""  class="form-horizontal" method="post" id="k3_form"/>
       <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title"><strong>K.3 PEMBANGUNAN PERUMAHAN</strong></h5>
            </div>
            <div class="modal-body">
              <input name="idform_1" type="hidden" class="form-control" value="<?=$idform_1;?>"/>
              <input name="idform_1_k3" id="idform_1_k3" type="hidden" class="form-control" />
              <p>1. Apakah Pemerintah Provinsi mempunyai Dokumen Perencanaan Urusan Perumahan? </p>
                <p class="indent"><label>
                <input type="checkbox" name="isi_1a" id="isi_1a" value="Y"/> a. RTRW (sudah perda) </label></p>
                <p class="indent"><label>
                <input type="checkbox" name="isi_1b" id="isi_1b" value="Y"/> b. RDTR (sudah perda) </label></p>
                <p class="indent"><label>
                <input type="checkbox" name="isi_1c" id="isi_1c" value="Y"/> c. RP3KP/RP4D </label></p>
                <p class="indent"><label>
                <input type="checkbox" name="isi_1d" id="isi_1d" value="Y"/> d. RPIJM </label></p>
                <p class="indent"><label>
                <input type="checkbox" name="isi_1e" id="isi_1e" value="Y"/> e. Renstra Dinas PKP </label></p>
                <p class="indent"><label>
                <input type="checkbox" name="isi_1f" id="isi_1f" value="Y"/> f. Lainnya (tuliskan):
                <input name="isi_1f_keterangan" id="isi_1f_keterangan" type="text" class="form-control" /></label></p></br>
              <p>2. Dalam pelaksanaan Urusan Perumahan, Pemerintah Provinsi memanfaatkan sumber dana yang berasal dari:</p>
                <p class="indent">
                <label><input type="checkbox" name="isi_2a" id="isi_2a" value="Y"/> a. APBD </label></p>
                <p class="indent">
                <label><input type="checkbox" name="isi_2b"  id="isi_2b" value="Y"/> b. Loan (pinjaman) dari badan/bank luar negeri </label></p>
                <p class="indent">
                <input type="checkbox"  name="isi_2c"  id="isi_2c" value="Y"/> c. CSR (uraikan) :
                <label><input name="isi_2c_keterangan" id="isi_2c_keterangan" type="text" class="form-control" /></label></p>
                <p class="indent">
                <input type="checkbox"  name="isi_2d"  id="isi_2d" value="Y"/> d. Swasta (uraikan) :
                <label><input name="isi_2d_keterangan" id="isi_2d_keterangan" type="text" class="form-control" /></label></p>
                <p class="indent">
                <input type="checkbox"  name="isi_2e" id="isi_2e" value="Y"/> e. Lainnya (tuliskan): APBN	</p>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn green" value="Simpan">
                <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <!-- END modal form K3 -->
    <!-- BEGIN modal form K4 -->
    <div id="k4_1_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action=""  class="form-horizontal" method="post" id="k4_1_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title uppercase"><strong>k.4.1 Jumlah Rumah Berdasarkan Status Kepemilikan Tempat Tinggal</strong></h5>
            </div>
            <div class="modal-body">
              <table id="user" class="table table-bordered table-striped form-1">
                <tbody>
                  <tr>
                    <td style="width:5%" align="center"><strong>No.</strong></td>
                    <td colspan="2" align="center" style="width:45%"><strong>Status Kepemilikan Tempat Tinggal</strong></td>
                    <td style="width:25%" align="center"><strong>Jumlah (unit)</strong></td>
                    <td style="width:25%" align="center"><strong>Sumber Data</strong></td>
                  </tr>
                  <tr>
                    <td align="center">1.</td>
                    <td colspan="2" align="center">
                      <input name="idform_1" id="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                      <input name="idform_1_k4_1" id="idform_1_k4_1" type="hidden" class="form-control" />
                      <input name="status1" id="status1" readonly type="text" class="form-control" value="Milik Sendiri"/></td>
                    <td align="center"><input name="k4141" id="k4141" maxlength="20" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                    <td align="center"><input name="k4151" id="k4151" maxlength="8" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                    <td align="center">2.</td>
                    <td colspan="2" align="center">
                      <input name="status2" id="status2" readonly type="text" class="form-control" value="Kontrak/Sewa"/></td>
                    <td align="center"><input name="k4142" id="k4142" maxlength="20" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                    <td align="center"><input name="k4152" id="k4152" maxlength="8" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                    <td align="center">3.</td>
                    <td colspan="2" align="center">
                      <input name="status3" id="status3" readonly type="text" class="form-control" value="Bebas Sewa"/></td>
                    <td align="center"><input name="k4143" id="k4143" maxlength="20" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                    <td align="center"><input name="k4153" id="k4153" maxlength="8" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                    <td align="center">4.</td>
                    <td colspan="2" align="center">
                      <input name="status4" id="status4" readonly type="text" class="form-control" value="Dinas"/></td>
                    <td align="center"><input name="k4144" id="k4144" maxlength="20" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                    <td align="center"><input name="k4154" id="k4154" maxlength="8" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                    <td align="center">5.</td>
                    <td colspan="2" align="center">
                      <input name="status5" id="status5" readonly type="text" class="form-control" value="Lainnya"/></td>
                    <td align="center"><input name="k4145" id="k4145" maxlength="20" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                    <td align="center"><input name="k4155" id="k4155" maxlength="8" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn green" value="Simpan">
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div id="k4_2_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action=""  class="form-horizontal" method="post" id="k4_2_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title uppercase"><strong>k.4.2 Jenis Fisik Bangunan Rumah</strong></h5>
            </div>
            <div class="modal-body">
              <table id="user" class="table table-bordered table-striped form-1">
                <tbody>
                <tr>
                  <td style="width:5%"  align="center"><strong>No.</strong></td>
                  <td style="width:45%" align="center"><strong>Jenis Fisik Bangunan Rumah</strong></td>
                  <td style="width:25%" align="center"><strong>Jumlah (unit)</strong></td>
                  <td style="width:25%" align="center"><strong>Sumber Data</strong></td>
                </tr>
                <tr>
                  <td align="center">1.</td>
                  <td align="left">
                    <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                    <input name="idform_1_k4_2" id="idform_1_k4_2" type="hidden" class="form-control"/>
                    <input name="jenis1" id="jenis1" readonly type="text" class="form-control"/></td>
                  <td align="center" ><input name="k4231" id="k4231" maxlength="8" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  <td align="center"><input name="k4241" id="k4241" maxlength="8" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                </tr>
                <tr>
                  <td align="center">2.</td>
                  <td align="left"  ><input name="jenis2" id="jenis2" readonly type="text" class="form-control"/></td>
                  <td align="center"><input name="k4232" id="k4232" maxlength="8" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  <td align="center"><input name="k4242" id="k4242" maxlength="8" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn green" value="Simpan">
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div id="k4_3_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action=""  class="form-horizontal" method="post" id="k4_3_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title uppercase"><strong>k.4.3 Jumlah Kepala Keluarga (KK) dalam Satu Rumah</strong></h5>
            </div>
            <div class="modal-body">
              <table id="user" class="table table-bordered table-striped form-1">
                <tbody>
                <tr>
                  <td style="width:5%"  align="center"><strong>No.</strong></td>
                  <td style="width:45%" align="center"><strong>Fungsi Rumah</strong></td>
                  <td style="width:25%" align="center"><strong>Jumlah (unit)</strong></td>
                  <td style="width:25%" align="center"><strong>Sumber Data</strong></td>
                </tr>
                <tr>
                  <td align="center">1.</td>
                  <td align="left"  >
                    <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                    <input name="idform_1_k4_3" id="idform_1_k4_3" type="hidden" class="form-control" />
                    <input name="fungsi1" id="fungsi1" readonly type="text" class="form-control" value="Rumah Dengan 1 (satu) KK"/></td>
                  <td align="center"><input name="k4331" id="k4331" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  <td align="center"><input name="k4341" id="k4341" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                </tr>
                <tr>
                  <td align="center">2.</td>
                  <td align="left"  ><input name="fungsi2" id="fungsi2" readonly type="text" class="form-control" value="Rumah dengan lebih dari 1 (satu) KK"/></td>
                  <td align="center"><input name="k4332" id="k4332" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                  <td align="center"><input name="k4342" id="k4342" type="text" class="form-control" style="background-color:#E8F3FF"/></td>
                </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn green" value="Simpan">
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div id="k4_4_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action=""  class="form-horizontal" method="post" id="k44_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title uppercase"><strong>k.4.4 Jumlah Sambungan Listrik Rumah (PLN)</strong></h5>
            </div>
            <div class="modal-body">
              <table id="user" class="table table-bordered table-striped">
                <tbody>
                <tr>
                  <td><strong>Kabupaten/Kota</strong>
                    <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"  style="background-color:#E8F3FF"/>
                    <input name="idform_1_k4_4" id="idform_1_k4_4" type="hidden" class="form-control"/>
                  </td>
                  <td><select name="idkabupaten_kota" id="idkabupaten_kota" class="form-control">
                    <?php foreach($kabupaten_kota_combo as $r): ?>
                    <option value="<?php echo $r->idkabupaten_kota;?>"  style="background-color:#E8F3FF"><?php echo $r->kabupaten_kota;?> </option>
                    <?php endforeach; ?>
                      </select>
                  </td>
                </tr>
                <tr>
                  <td><strong>Jumlah Rumah (unit) <?=$tahun-1;?></strong></td>
                  <td><input name="jumlah_rumah_3" id="jumlah_rumah_3" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
                </tr>
                <tr>
                  <td><strong>Jumlah Rumah (unit) <?=$tahun;?></strong></td>
                  <td align="center"><input name="jumlah_rumah_4" id="jumlah_rumah_4" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
                </tr>
                  <td><strong>Sumber Data</strong></td>
                  <td align="center"><input name="sumber_data_5" id="sumber_data_5" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
                </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn green" value="Simpan">
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div id="k4_5_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action=""  class="form-horizontal" method="post" id="k45_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title uppercase"><strong>k.4.5 Jumlah Pembangunan Rumah dalam 1 Tahun</strong></h5>
            </div>
            <div class="modal-body">
              <table id="user" class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <td style="width:27%" align="center" colspan="2"><strong>Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun-1;?> (unit)</strong></td>
                  </tr>
                  <tr>
                    <td align="center">Non-MBR</td>
                    <td align="center">MBR</td>
                  </tr>
                  <tr>
                    <td align="center">
                      <input name="idform_1_k4_5" id="idform_1_k4_5" type="hidden"/>
                      <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                      <input name="non_mbr_2" id="non_mbr_2" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
                    <td align="center"><input name="mbr_3" id="mbr_3" type="text" class="form-control"  style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                    <td style="width:27%" align="center" colspan="2"><strong>Jumlah Pembangunan Rumah Berdasarkan IMB <?=$tahun;?> (unit)</strong>  (unit).....Tahun</font></strong></td>
                  </tr>
                  <tr>
                    <td align="center">Non-MBR</td>
                    <td align="center">MBR</td>
                  </tr>
                  <tr>
                    <td align="center"><input name="non_mbr_4" id="non_mbr_4" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
                    <td align="center"><input name="mbr_5" id="mbr_5" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                    <td style="width:27%" align="center" colspan="2"><strong><font color="#FF0000">Jumlah Pembangunan Rumah Berdasarkan Non IMB (unit).....Tahun</font></strong></td>
                  </tr>
                  <tr>
                    <td align="center">Non-MBR</td>
                    <td align="center">MBR</td>
                  </tr>
                  <tr>
                    <td align="center"><input name="non_mbr_6" id="non_mbr_6" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
                    <td align="center"><input name="mbr_7" id="mbr_7" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
                  </tr>
                  <tr>
                     <td align="center" colspan="2"><strong>Sumber Data</strong></td>
                  </tr>
                  <tr>
                    <td align="center" colspan="2"><input name="sumber_data_8" id="sumber_data_8" type="text" class="form-control"   style="background-color:#E8F3FF"/></td>
                  </tr>
                </tbody>
              </table>
              </p>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn green" value="Simpan">
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <!-- END modal form K4 -->
    <!-- BEGIN modal form K5 -->
    <div id="k5_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action=""  class="form-horizontal" method="post" id="k5_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title"><strong>K.5 RUMAH TIDAK LAYAK HUNI</strong></h5>
            </div>
            <div class="modal-body">
              <table id="user" class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <td style="width:25%" align="center"><strong>Kabupaten/Kota</strong></td><td align="left">
                      <select name="idkabupaten_kota" id="idkabupaten_kota"  class="form-control" >
                      <?php foreach($kabupaten_kota_combo as $r): ?>
                      <option value="<?php echo $r->idkabupaten_kota;?> "  style="background-color:#E8F3FF"><?php echo $r->kabupaten_kota;?> </option>
                      <?php endforeach; ?>
                      </select>
                    </td>
                  <tr>
                  </tr>
                    <td style="width:15%" align="center"><strong>Jumlah KK/RT</strong></td>
                    <td align="center">
                      <input name="idform_1_k5" id="idform_1_k5" type="hidden"/>
                      <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                      <input name="jumlah_kk_rt_3" id="jumlah_kk_rt_3" type="text" class="form-control"/>
                    </td>
                  <tr>
                  </tr>
                    <td style="width:15%" align="center"><strong>Jumlah RTLH Versi BDT (unit)</strong></td>
                    <td align="center">
                      <input name="jumlah_rtlh_versi_bdt_4" id="jumlah_rtlh_versi_bdt_4" type="text" class="form-control"/>
                    </td>
                  <tr>
                  </tr>
                    <td style="width:15%" align="center"><strong>Jumlah RTLH Verifikasi Pemda (unit)</strong></td>
                    <td align="center" >
                      <input name="jumlah_rtlh_verifikasi_pemda_5" id="jumlah_rtlh_verifikasi_pemda_5" type="text" class="form-control" />
                    </td>
                  <tr>
                  </tr>
                    <td align="center"><strong>Sumber Data</strong></td>
                    <td align="center">
                      <input name="sumber_data_6" id="sumber_data_6" type="text" class="form-control" />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn green" value="Simpan">
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <!-- END modal form K5 -->
    <!-- BEGIN modal form K6 -->
    <div id="k6_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
      <form action=""  class="form-horizontal" method="post" id="k6_form"/>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h5 class="modal-title"><strong>K.6. KAWASAN KUMUH </strong></h5>
            </div>
            <div class="modal-body">
              <table id="user" class="table table-bordered table-striped">
                <tbody>
                <tr>
                  <td style="width:40%"><strong>Kabupaten/Kota</strong></td>
                  <td align="left">
                    <input name="idform_1" type="hidden" class="form-control"  value="<?=$idform_1;?>"/>
                    <input name="idform_1_k6" id="idform_1_k6" type="hidden"/>
                    <select name="kabupaten_kota_select" id="kabupaten_kota_select" class="form-control">
                      <option value=""  style="background-color:#E8F3FF">Pilih Kabupaten/Kota</option>
                      <?php foreach($kabupaten_kota_combo as $r): ?>
                      <option value="<?php echo $r->idkabupaten_kota;?> "  style="background-color:#E8F3FF"><?php echo $r->kabupaten_kota;?> </option>
                      <?php endforeach; ?>
                   </select>
                  </td>
                </tr>
                <tr>
                  <td><strong>Kecamatan</strong></td>
                  <td>
                    <select name="kecamatan_combo[]" id="kecamatan_combo"  multiple="multiple" class="form-control">
                      <option value=""  style="background-color:#E8F3FF">Kecamatan</option>
                      <?php foreach($kecamatan as $r): ?>
                      <option value="<?php echo $r->idkecamatan;?>" style="background-color:#E8F3FF"><?php echo $r->kecamatan;?> </option>
                      <?php endforeach; ?>
                    </select>
                    <p>Tekan Control (ctrl) pada keyboard untuk memilih lebih dari satu kecamatan</p>
                  </td>
                </tr>
                <tr>
                  <td style="width:15%"><strong>Luas wilayah kumuh (Ha)</strong></td>
                  <td align="center">
                    <input name="luas_wilayah_kumuh_4" id="luas_wilayah_kumuh_4" type="text" class="form-control"/>
                  </td>
                </tr>
                <tr>
                  <td style="width:15%"><strong>Jumlah RTLH dalam wilayah kumuh (unit)</strong></td>
                  <td align="center">
                    <input name="jumlah_rtlh_dalam_wilayah_kumuh_5" id="jumlah_rtlh_dalam_wilayah_kumuh_5" type="text" class="form-control" />
                  </td>
                </tr>
                <tr>
                  <td><strong>Sumber Data</strong></td>
                  <td align="center">
                    <input name="sumber_data_6" id="s6" type="text" class="form-control"/>
                  </td>
                </tr>
              </tbody>
            </table>
            </div>
            <div class="modal-footer">
              <input type="submit" class="btn green" value="Simpan">
              <button type="button" data-dismiss="modal" class="btn default">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <!-- END modal form K6 -->
    <!-- /.modal -->
    <!-- END PORTLET MODAL FORM-->
  </div>
</div>
<!-- END PAGE CONTAINER -->
<?php include "footer2.php";?>
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="</?php echo base_url('assets/js/jquery-1.7.2.min.js'); ?>"></script> -->
<script src="../../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../../assets/global/scripts/jquery.form.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="../../../assets/global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="../../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../../assets/admin/pages/scripts/form-validation.js"></script>
<script src="../../../assets/admin/pages/scripts/components-form-tools.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
jQuery(document).ready(function() {
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
   Layout.init(); // init current layout
   Demo.init(); // init demo features
   FormValidation.init();
   ComponentsFormTools.init();
   Custom.init();
});
</script>
<script type="text/javascript">
  // var jnoc = jQuery.noConflict();
  // jnoc(document).ready(function() {
  // jnoc('#form-upload').on('submit', function(e) {
  // e.preventDefault();
  // jnoc('#submit-button').attr('disabled', '');
  // jnoc("#output").html('<div style="padding:10px"><img src="<?php echo base_url('assets/images/loading.gif'); ?>" alt="Please Wait"/> <span>Mengunggah...</span></div>');
  // jnoc(this).ajaxSubmit({
  // target: '#output',
  // success:  sukses
  //        });
  //    });
  // });
  // function sukses()  {
  // jnoc('#form-upload').resetForm();
  // jnoc('#submit-button').removeAttr('disabled');
  //
  //		}
</script>
<script>
  $(document).ready(function(){
      $("#kabupaten_kota_select").change(function (){
                var url = "<?php echo site_url('main/add_ajax_kec');?>/"+$(this).val();
                $('#kecamatan_combo').load(url);
                return false;
            })
			// Handler for .ready() called.
			$('html, body').animate({
				scrollTop: $('#<?php echo $_GET["dv"];?>').offset().top
			}, 'slow');
		});
</script>
<script type="text/javascript">
function k2a_form_submit()
{
	  document.getElementById('k2a_form').submit();
}
function tambah_k2_modal()
{
	  document.getElementById('uraian1').value="";
	  document.getElementById('k221').value="";
	  document.getElementById('k231').value="";
	  document.getElementById('idform_1_k2_a').value="";
    document.getElementById('k2a_form').action="<?php echo base_url('main/Form1k2_a_insert');?>";
}
function ubah_k2_modal(a,b,c,d)
{
	  document.getElementById('uraian1').value=a;
	  document.getElementById('k221').value=b;
	  document.getElementById('k231').value=c;
	  document.getElementById('idform_1_k2_a').value=d;
    document.getElementById('k2a_form').action="<?php echo base_url('main/Form1k2_a_update');?>";
}
function tambah_k3_modal()
{
	  //document.getElementById('idform_1_k3')="";
//	  document.getElementById('isi_1a').checked=false;
//	  document.getElementById('isi_1b').checked=false;
//	  document.getElementById('isi_1c').checked=false;
//	  document.getElementById('isi_1d').checked=false;
//	  document.getElementById('isi_1e').checked=false;
//	  document.getElementById('isi_1f').checked=false;
//	  document.getElementById('isi_1f_keterangan').value="";
//	  document.getElementById('isi_2a').checked=false;
//	  document.getElementById('isi_2b').checked=false;
//	  document.getElementById('isi_2c').checked=false;
//	  document.getElementById('isi_2d').checked=false;
//	  document.getElementById('isi_2e').checked=false;
//	  document.getElementById('isi_2c_keterangan').value="";
//	  document.getElementById('isi_2d_keterangan').value="";
        document.getElementById('k3_form').action="<?php echo base_url('main/Form1k3_insert');?>";
}
function ubah_k3_modal(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o)
{
	  document.getElementById('idform_1_k3').value=o;
	  if (a==1) {
	  document.getElementById('isi_1a').click();
	  }
	  if (b==1) {
	  document.getElementById('isi_1b').click();
	  }
	  if (c==1) {
	  document.getElementById('isi_1c').click();
	  }
	  if (d==1) {
	  document.getElementById('isi_1d').click();
	  }
	  if (e==1) {
	  document.getElementById('isi_1e').click();
	  }
	  if (f==1) {
	  document.getElementById('isi_1f').click();
	  }
	  document.getElementById('isi_1f_keterangan').value=g;
	  if (h==1) {
	  document.getElementById('isi_2a').click();
	  }
	  if (i==1) {
	  document.getElementById('isi_2b').click();
	  }
	  if (j==1) {
	  document.getElementById('isi_2c').click();
	  }
	  if (k==1) {
	  document.getElementById('isi_2d').click();
	  }
	  if (l==1) {
	  document.getElementById('isi_2e').click();
	  }
	  document.getElementById('isi_2c_keterangan').value=m;
	  document.getElementById('isi_2d_keterangan').value=n;
        document.getElementById('k3_form').action="<?php echo base_url('main/Form1k3_update');?>";
}
</script>
<script type="text/javascript">
function k5_form_submit()
{
	  document.getElementById('k5_form').submit();
}
function tambah_k5_modal()
{
	  document.getElementById('idkabupaten_kota').value="";
	  document.getElementById('jumlah_kk_rt_3').value="";
	  document.getElementById('jumlah_rtlh_versi_bdt_4').value="";
	  document.getElementById('jumlah_rtlh_verifikasi_pemda_5').value="";
	  document.getElementById('sumber_data_6').value="";
	  document.getElementById('idform_1_k5').value="";
        document.getElementById('k5_form').action="<?php echo base_url('main/Form1k5_insert');?>";
}
function ubah_k5_modal(a,b,c,d,e,f)
{
	  document.getElementById('idkabupaten_kota').value=a;
	  document.getElementById('jumlah_kk_rt_3').value=b;
	  document.getElementById('jumlah_rtlh_versi_bdt_4').value=c;
	  document.getElementById('jumlah_rtlh_verifikasi_pemda_5').value=d;
	  document.getElementById('sumber_data_6').value=e;
	  document.getElementById('idform_1_k5').value=f;
        document.getElementById('k5_form').action="<?php echo base_url('main/Form1k5_update');?>";
}
function confirmDelete7(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k5');?>/"+delUrl;
  }
}
//
	  //document.getElementById('idkabupaten_kota').value=a;
	  //document.getElementById('idkecamatan').value=b;
function tambah_k6_modal()
{
	  document.getElementById('luas_wilayah_kumuh_4').value="";
	  document.getElementById('jumlah_rtlh_dalam_wilayah_kumuh_5').value="";
	  document.getElementById('s6').value="";
	  document.getElementById('idform_1_k6').value="";
        document.getElementById('k6_form').action="<?php echo base_url('main/Form1k6_insert');?>";
}
function ubah_k6_modal(a,b,c,d)
{
	  document.getElementById('luas_wilayah_kumuh_4').value=a;
	  document.getElementById('jumlah_rtlh_dalam_wilayah_kumuh_5').value=b;
	  document.getElementById('s6').value=c;
	  document.getElementById('idform_1_k6').value=d;
        document.getElementById('k6_form').action="<?php echo base_url('main/Form1k6_update');?>";
}
function confirmDelete8(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k6');?>/"+delUrl;
  }
}

function confirmDelete(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k2_a');?>/"+delUrl;
  }
}
//
function tambah_k4_4_modal()
{
	  document.getElementById('idkabupaten_kota').value="";
	  document.getElementById('jumlah_rumah_3').value="";
	  document.getElementById('jumlah_rumah_4').value="";
	  document.getElementById('sumber_data_5').value="";
	  document.getElementById('idform_1_k4_4').value="";
        document.getElementById('k44_form').action="<?php echo base_url('main/Form1k4_4_insert');?>";
}
function ubah_k4_4_modal(a,b,c,d,e)
{
	  document.getElementById('idkabupaten_kota').value=a;
	  document.getElementById('jumlah_rumah_3').value=b;
	  document.getElementById('jumlah_rumah_4').value=c;
	  document.getElementById('sumber_data_5').value=d;
	  document.getElementById('idform_1_k4_4').value=e;
        document.getElementById('k44_form').action="<?php echo base_url('main/Form1k4_4_update');?>";
}
function confirmDelete(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k2_a');?>/"+delUrl;
  }
}
//
function tambah_k4_5_modal()
{
	  document.getElementById('non_mbr_2').value="";
	  document.getElementById('mbr_3').value="";
	  document.getElementById('non_mbr_4').value="";
	  document.getElementById('mbr_5').value="";
	  document.getElementById('non_mbr_6').value="";
	  document.getElementById('mbr_7').value="";
	  document.getElementById('sumber_data_8').value="";
	  document.getElementById('idform_1_k4_5').value="";
        document.getElementById('k45_form').action="<?php echo base_url('main/Form1k4_5_insert');?>";
}
function ubah_k4_5_modal(a,b,c,d,e,f,g,h)
{
	  document.getElementById('non_mbr_2').value=a;
	  document.getElementById('mbr_3').value=b;
	  document.getElementById('non_mbr_4').value=c;
	  document.getElementById('mbr_5').value=d;
	  document.getElementById('non_mbr_6').value=e;
	  document.getElementById('mbr_7').value=f;
	  document.getElementById('sumber_data_8').value=g;
	  document.getElementById('idform_1_k4_5').value=h;
        document.getElementById('k45_form').action="<?php echo base_url('main/Form1k4_5_update');?>";
}
function confirmDelete6(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k4_5');?>/"+delUrl;
  }
}
//
function tambah_k2_b_modal()
{
	  document.getElementById('jenis_kegiatan_urusan_pkp_2').value="";
	  document.getElementById('ta_a_vol_unit_3').value="";
	  document.getElementById('ta_a_biaya_4').value="";
	  document.getElementById('ta_a_vol_unit_5').value="";
	  document.getElementById('ta_a_biaya_6').value="";
	  document.getElementById('idform_1_k2_b').value="";
        document.getElementById('k2b_form').action="<?php echo base_url('main/Form1k2_b_insert');?>";
}
function ubah_k2_b_modal(a,b,c,d,e,f)
{
	  document.getElementById('jenis_kegiatan_urusan_pkp_2').value=a;
	  document.getElementById('ta_a_vol_unit_3').value=b;
	  document.getElementById('ta_a_biaya_4').value=c;
	  document.getElementById('ta_a_vol_unit_5').value=d;
	  document.getElementById('ta_a_biaya_6').value=e;
	  document.getElementById('idform_1_k2_b').value=f;
        document.getElementById('k2b_form').action="<?php echo base_url('main/Form1k2_b_update');?>";
}
function confirmDelete2(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k2_b');?>/"+delUrl;
  }
}
//
function tambah_k4_2_modal()
{
	  document.getElementById('k4231').value="";
	  document.getElementById('k4232').value="";
	  document.getElementById('k4241').value="";
	  document.getElementById('k4242').value="";
	  document.getElementById('jenis1').value="";
	  document.getElementById('jenis2').value="";
	  document.getElementById('idform_1_k4_2').value="";
        document.getElementById('k4_2_form').action="<?php echo base_url('main/Form1k4_2_insert');?>";
}
function ubah_k4_2_modal(a,b,c,d,e,f,g)
{
	  document.getElementById('k4231').value=a;
	  document.getElementById('k4232').value=b;
	  document.getElementById('k4241').value=c;
	  document.getElementById('k4242').value=d;
	  document.getElementById('jenis1').value=e;
	  document.getElementById('jenis2').value=f;
	  document.getElementById('idform_1_k4_2').value=g;
        document.getElementById('k4_2_form').action="<?php echo base_url('main/Form1k4_2_update');?>";
}
//
function tambah_k4_3_modal()
{
	  document.getElementById('k4331').value="";
	  document.getElementById('k4332').value="";
	  document.getElementById('k4341').value="";
	  document.getElementById('k4342').value="";
	  document.getElementById('fungsi1').value="";
	  document.getElementById('fungsi2').value="";
	  document.getElementById('idform_1_k4_3').value="";
        document.getElementById('k4_3_form').action="<?php echo base_url('main/Form1k4_3_insert');?>";
}
function ubah_k4_3_modal(a,b,c,d,e,f,g)
{
	  document.getElementById('k4331').value=a;
	  document.getElementById('k4332').value=b;
	  document.getElementById('k4341').value=c;
	  document.getElementById('k4342').value=d;
	  document.getElementById('fungsi1').value=e;
	  document.getElementById('fungsi2').value=f;
	  document.getElementById('idform_1_k4_3').value=g;
        document.getElementById('k4_3_form').action="<?php echo base_url('main/Form1k4_3_update');?>";
}
//
function tambah_k4_1_modal()
{
	  document.getElementById('idform_1_k4_1').value="";
	  document.getElementById('k4141').value="";
	  document.getElementById('k4142').value="";
	  document.getElementById('k4143').value="";
	  document.getElementById('k4144').value="";
	  document.getElementById('k4145').value="";
	  document.getElementById('k4151').value="";
	  document.getElementById('k4152').value="";
	  document.getElementById('k4153').value="";
	  document.getElementById('k4154').value="";
	  document.getElementById('k4155').value="";
	  document.getElementById('status1').value="";
	  document.getElementById('status2').value="";
	  document.getElementById('status3').value="";
	  document.getElementById('status4').value="";
	  document.getElementById('status5').value="";
    document.getElementById('k4_1_form').action="<?php echo base_url('main/Form1k4_1_insert');?>";
}
function ubah_k4_1_modal(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p)
{
	  document.getElementById('idform_1_k4_1').value=p;

	  document.getElementById('k4141').value=a;
	  document.getElementById('k4142').value=b;
	  document.getElementById('k4143').value=c;
	  document.getElementById('k4144').value=d;
	  document.getElementById('k4145').value=e;
	  document.getElementById('k4151').value=f;
	  document.getElementById('k4152').value=g;
	  document.getElementById('k4153').value=h;
	  document.getElementById('k4154').value=i;
	  document.getElementById('k4155').value=j;
	  document.getElementById('status1').value=k;
	  document.getElementById('status2').value=l;
	  document.getElementById('status3').value=m;
	  document.getElementById('status4').value=n;
	  document.getElementById('status5').value=o;
        document.getElementById('k4_1_form').action="<?php echo base_url('main/Form1k4_1_update');?>";
}
function confirmDelete4(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k4_1');?>/"+delUrl;
  }
}
function confirmDelete5(delUrl) {
  if (confirm("Yakin akan menghapus?")) {
    document.location = "<?=BASE_URL('main/hapus_form_1_k4_4');?>/"+delUrl;
  }
}
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>
<!-- END BODY -->
</html>
