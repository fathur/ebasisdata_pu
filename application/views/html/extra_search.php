<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.1.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Metronic | Search Results</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<link href="../../assets/admin/pages/css/search.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css">
<link href="../../assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../../assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../../assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body>
<!-- BEGIN HEADER -->
<?php include "header.php";?>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Search Results <small>search results</small></h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
			<div class="page-toolbar">
				<!-- BEGIN THEME PANEL -->
				<div class="btn-group btn-theme-panel">
					<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
					<i class="icon-settings"></i>
					</a>
					<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<h3>THEME COLORS</h3>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li class="theme-color theme-color-default" data-theme="default">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Default</span>
											</li>
											<li class="theme-color theme-color-blue-hoki" data-theme="blue-hoki">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Hoki</span>
											</li>
											<li class="theme-color theme-color-blue-steel" data-theme="blue-steel">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Steel</span>
											</li>
											<li class="theme-color theme-color-yellow-orange" data-theme="yellow-orange">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Orange</span>
											</li>
											<li class="theme-color theme-color-yellow-crusta" data-theme="yellow-crusta">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Yellow Crusta</span>
											</li>
										</ul>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li class="theme-color theme-color-green-haze" data-theme="green-haze">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Green Haze</span>
											</li>
											<li class="theme-color theme-color-red-sunglo" data-theme="red-sunglo">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Sunglo</span>
											</li>
											<li class="theme-color theme-color-red-intense" data-theme="red-intense">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Intense</span>
											</li>
											<li class="theme-color theme-color-purple-plum" data-theme="purple-plum">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Plum</span>
											</li>
											<li class="theme-color theme-color-purple-studio" data-theme="purple-studio">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Studio</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 seperator">
								<h3>LAYOUT</h3>
								<ul class="theme-settings">
									<li>
										 Theme Style
										<select class="theme-setting theme-setting-style form-control input-sm input-small input-inline tooltips" data-original-title="Change theme style" data-container="body" data-placement="left">
											<option value="boxed" selected="selected">Square corners</option>
											<option value="rounded">Rounded corners</option>
										</select>
									</li>
									<li>
										 Layout
										<select class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips" data-original-title="Change layout type" data-container="body" data-placement="left">
											<option value="boxed" selected="selected">Boxed</option>
											<option value="fluid">Fluid</option>
										</select>
									</li>
									<li>
										 Top Menu Style
										<select class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change top menu dropdowns style" data-container="body" data-placement="left">
											<option value="dark" selected="selected">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Top Menu Mode
										<select class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) top menu" data-container="body" data-placement="left">
											<option value="fixed">Fixed</option>
											<option value="not-fixed" selected="selected">Not Fixed</option>
										</select>
									</li>
									<li>
										 Mega Menu Style
										<select class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change mega menu dropdowns style" data-container="body" data-placement="left">
											<option value="dark" selected="selected">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Mega Menu Mode
										<select class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) mega menu" data-container="body" data-placement="left">
											<option value="fixed" selected="selected">Fixed</option>
											<option value="not-fixed">Not Fixed</option>
										</select>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END THEME PANEL -->
			</div>
			<!-- END PAGE TOOLBAR -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="extra_search.html">Pages</a>
					<i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Search Results
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom tabbable-noborder">
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#tab_2_2">
								Booking Search </a>
							</li>
							<li>
								<a data-toggle="tab" href="#tab_1_3">
								Classic Search </a>
							</li>
							<li>
								<a data-toggle="tab" href="#tab_1_4">
								Company Search </a>
							</li>
							<li>
								<a data-toggle="tab" href="#tab_1_5">
								User Search </a>
							</li>
						</ul>
						<div class="tab-content">
							<div id="tab_2_2" class="tab-pane active">
								<div class="row">
									<div class="col-md-8">
										<div class="booking-search">
											<form action="javascript:;" role="form">
												<div class="row form-group">
													<div class="col-md-12">
														<label class="control-label">Distanation</label>
														<div class="input-icon">
															<i class="fa fa-map-marker"></i>
															<input class="form-control" type="text" placeholder="Canada, Malaysia, Russia ...">
														</div>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-md-6">
														<label class="control-label">Check-in:</label>
														<div class="input-icon">
															<i class="fa fa-calendar"></i>
															<input class="form-control date-picker" size="16" type="text" value="12-02-2012" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years"/>
														</div>
													</div>
													<div class="col-md-6">
														<label class="control-label">Check-out:</label>
														<div class="input-icon">
															<i class="fa fa-calendar"></i>
															<input class="form-control date-picker" size="16" type="text" value="12-02-2012" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years"/>
														</div>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-md-6">
														<label class="control-label">Persons</label>
														<div class="input-icon">
															<i class="fa fa-user"></i>
															<input class="form-control" size="16" type="text" placeholder="1 Room, 2 Adults, 0 Children"/>
														</div>
													</div>
													<div class="col-md-6">
														<label class="control-list">Options:</label>
														<div class="checkbox-list">
															<label class="checkbox-inline">
															<input type="checkbox" name="optionsRadios2" value="option1"/>
															Option1 </label>
															<label class="checkbox-inline">
															<input type="checkbox" name="optionsRadios2" value="option2" checked/>
															Option2 </label>
														</div>
													</div>
												</div>
												<button class="btn blue btn-block margin-top-20">SEARCH <i class="m-icon-swapright m-icon-white"></i></button>
											</form>
										</div>
									</div>
									<!--end booking-search-->
									<div class="col-md-4">
										<div class="booking-app">
											<a href="javascript:;">
											<i class="fa fa-mobile-phone pull-left"></i>
											<span>
											Get our mobile app! </span>
											</a>
										</div>
										<div class="booking-offer">
											<img src="../../assets/admin/pages/media/search/1.jpg" class="img-responsive" alt="">
											<div class="booking-offer-in">
												<span>
												London, UK </span>
												<em>Sign Up Today and Get 30% Discount!</em>
											</div>
										</div>
									</div>
									<!--end col-md-4-->
								</div>
								<div class="row booking-results">
									<div class="col-md-6">
										<div class="booking-result">
											<div class="booking-img">
												<img src="../../assets/admin/pages/media/gallery/image4.jpg" alt="">
												<ul class="list-unstyled price-location">
													<li>
														<i class="fa fa-money"></i> From $126
													</li>
													<li>
														<i class="fa fa-map-marker"></i> Tioman, Malaysia
													</li>
												</ul>
											</div>
											<div class="booking-info">
												<h2>
												<a href="javascript:;">
												Here Any Title </a>
												</h2>
												<ul class="stars list-inline">
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star-empty"></i>
													</li>
												</ul>
												<p>
													 At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum. <a href="javascript:;">
													read more... </a>
												</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="booking-result">
											<div class="booking-img">
												<img src="../../assets/admin/pages/media/gallery/image1.jpg" alt="">
												<ul class="list-unstyled price-location">
													<li>
														<i class="fa fa-money"></i> From $126
													</li>
													<li>
														<i class="fa fa-map-marker"></i> Tioman, Malaysia
													</li>
												</ul>
											</div>
											<div class="booking-info">
												<h2>
												<a href="javascript:;">
												Here Any Title </a>
												</h2>
												<ul class="stars list-inline">
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star-empty"></i>
													</li>
												</ul>
												<p>
													 At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum. <a href="javascript:;">
													read more... </a>
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="row booking-results">
									<div class="col-md-6">
										<div class="booking-result">
											<div class="booking-img">
												<img src="../../assets/admin/pages/media/gallery/image2.jpg" alt="">
												<ul class="list-unstyled price-location">
													<li>
														<i class="fa fa-money"></i> From $126
													</li>
													<li>
														<i class="fa fa-map-marker"></i> Tioman, Malaysia
													</li>
												</ul>
											</div>
											<div class="booking-info">
												<h2>
												<a href="javascript:;">
												Here Any Title </a>
												</h2>
												<ul class="stars list-inline">
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star-empty"></i>
													</li>
												</ul>
												<p>
													 At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum. <a href="javascript:;">
													read more... </a>
												</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="booking-result">
											<div class="booking-img">
												<img src="../../assets/admin/pages/media/gallery/image3.jpg" alt="">
												<ul class="list-unstyled price-location">
													<li>
														<i class="fa fa-money"></i> From $126
													</li>
													<li>
														<i class="fa fa-map-marker"></i> Tioman, Malaysia
													</li>
												</ul>
											</div>
											<div class="booking-info">
												<h2>
												<a href="javascript:;">
												Here Any Title </a>
												</h2>
												<ul class="stars list-inline">
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star-empty"></i>
													</li>
												</ul>
												<p>
													 At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum. <a href="javascript:;">
													read more... </a>
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="row booking-results">
									<div class="col-md-6">
										<div class="booking-result">
											<div class="booking-img">
												<img src="../../assets/admin/pages/media/gallery/image5.jpg" alt="">
												<ul class="list-unstyled price-location">
													<li>
														<i class="fa fa-money"></i> From $126
													</li>
													<li>
														<i class="fa fa-map-marker"></i> Tioman, Malaysia
													</li>
												</ul>
											</div>
											<div class="booking-info">
												<h2>
												<a href="javascript:;">
												Here Any Title </a>
												</h2>
												<ul class="stars list-inline">
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star-empty"></i>
													</li>
												</ul>
												<p>
													 At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum. <a href="javascript:;">
													read more... </a>
												</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="booking-result">
											<div class="booking-img">
												<img src="../../assets/admin/pages/media/gallery/image1.jpg" alt="">
												<ul class="list-unstyled price-location">
													<li>
														<i class="fa fa-money"></i> From $126
													</li>
													<li>
														<i class="fa fa-map-marker"></i> Tioman, Malaysia
													</li>
												</ul>
											</div>
											<div class="booking-info">
												<h2>
												<a href="javascript:;">
												Here Any Title </a>
												</h2>
												<ul class="stars list-inline">
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star"></i>
													</li>
													<li>
														<i class="fa fa-star-empty"></i>
													</li>
												</ul>
												<p>
													 At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum. <a href="javascript:;">
													read more... </a>
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="margin-top-20">
									<ul class="pagination pagination-circle">
										<li>
											<a href="javascript:;">
											Prev </a>
										</li>
										<li>
											<a href="javascript:;">
											1 </a>
										</li>
										<li>
											<a href="javascript:;">
											2 </a>
										</li>
										<li class="active">
											<a href="javascript:;">
											3 </a>
										</li>
										<li>
											<a href="javascript:;">
											4 </a>
										</li>
										<li>
											<a href="javascript:;">
											5 </a>
										</li>
										<li>
											<a href="javascript:;">
											Next </a>
										</li>
									</ul>
								</div>
							</div>
							<!--end tab-pane-->
							<div id="tab_1_3" class="tab-pane">
								<div class="row">
									<div class="col-md-12">
										<form action="javascript:;" class="alert alert-warning alert-borderless">
											<div class="input-group">
												<div class="input-cont">
													<input type="text" placeholder="Search..." class="form-control"/>
												</div>
												<span class="input-group-btn">
												<button type="button" class="btn green-haze">
												Search &nbsp; <i class="m-icon-swapright m-icon-white"></i>
												</button>
												</span>
											</div>
										</form>
									</div>
								</div>
								<div class="search-classic">
									<h4>
									<a href="javascript:;">
									Metronic - Responsive Admin Dashboard Template </a>
									</h4>
									<a href="javascript:;">
									http://www.keenthemes.com </a>
									<p>
										 Metronic is a responsive admin dashboard template powered with Twitter Bootstrap Framework for admin and backend applications. Metronic has a clean and intuitive metro style design which makes your next project look awesome and yet user friendly..
									</p>
								</div>
								<div class="search-classic">
									<h4>
									<a href="javascript:;">
									Conquer - Responsive Admin Dashboard Template </a>
									</h4>
									<a href="javascript:;">
									http://www.keenthemes.com </a>
									<p>
										 Conquer is a responsive admin dashboard template created mainly for admin and backend applications(CMS, CRM, Custom Admin Application, Admin Dashboard). Conquer template powered with Twitter Bootstrap Framework..
									</p>
								</div>
								<div class="search-classic">
									<h4>
									<a href="javascript:;">
									Metronic - Responsive Admin Dashboard Template </a>
									</h4>
									<a href="javascript:;">
									http://www.keenthemes.com </a>
									<p>
										 Metronic is a responsive admin dashboard template powered with Twitter Bootstrap Framework for admin and backend applications. Metronic has a clean and intuitive metro style design which makes your next project look awesome and yet user friendly..
									</p>
								</div>
								<div class="search-classic">
									<h4>
									<a href="javascript:;">
									Conquer - Responsive Admin Dashboard Template </a>
									</h4>
									<a href="javascript:;">
									http://www.keenthemes.com </a>
									<p>
										 Conquer is a responsive admin dashboard template created mainly for admin and backend applications(CMS, CRM, Custom Admin Application, Admin Dashboard). Conquer template powered with Twitter Bootstrap Framework..
									</p>
								</div>
								<div class="search-classic">
									<h4>
									<a href="javascript:;">
									Conquer - Responsive Admin Dashboard Template </a>
									</h4>
									<a href="javascript:;">
									http://www.keenthemes.com </a>
									<p>
										 Conquer is a responsive admin dashboard template created mainly for admin and backend applications(CMS, CRM, Custom Admin Application, Admin Dashboard). Conquer template powered with Twitter Bootstrap Framework..
									</p>
								</div>
								<div class="search-classic">
									<h4>
									<a href="javascript:;">
									Metronic - Responsive Admin Dashboard Template </a>
									</h4>
									<a href="javascript:;">
									http://www.keenthemes.com </a>
									<p>
										 Metronic is a responsive admin dashboard template powered with Twitter Bootstrap Framework for admin and backend applications. Metronic has a clean and intuitive metro style design which makes your next project look awesome and yet user friendly..
									</p>
								</div>
								<div class="search-classic">
									<h4>
									<a href="javascript:;">
									Conquer - Responsive Admin Dashboard Template </a>
									</h4>
									<a href="javascript:;">
									http://www.keenthemes.com </a>
									<p>
										 Conquer is a responsive admin dashboard template created mainly for admin and backend applications(CMS, CRM, Custom Admin Application, Admin Dashboard). Conquer template powered with Twitter Bootstrap Framework..
									</p>
								</div>
								<div class="margin-top-20">
									<ul class="pagination pagination-circle">
										<li>
											<a href="javascript:;">
											Prev </a>
										</li>
										<li>
											<a href="javascript:;">
											1 </a>
										</li>
										<li>
											<a href="javascript:;">
											2 </a>
										</li>
										<li class="active">
											<a href="javascript:;">
											3 </a>
										</li>
										<li>
											<a href="javascript:;">
											4 </a>
										</li>
										<li>
											<a href="javascript:;">
											5 </a>
										</li>
										<li>
											<a href="javascript:;">
											Next </a>
										</li>
									</ul>
								</div>
							</div>
							<!--end tab-pane-->
							<div id="tab_1_4" class="tab-pane">
								<div class="row">
									<div class="col-md-12">
										<form action="javascript:;" class="alert alert-success alert-borderless">
											<div class="input-group">
												<div class="input-cont">
													<input type="text" placeholder="Search..." class="form-control"/>
												</div>
												<span class="input-group-btn">
												<button type="button" class="btn green-haze">
												Search &nbsp; <i class="m-icon-swapright m-icon-white"></i>
												</button>
												</span>
											</div>
										</form>
									</div>
								</div>
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr>
										<th>
											<i class="fa fa-briefcase"></i> Company
										</th>
										<th>
											<i class="fa fa-question"></i> Descrition
										</th>
										<th>
											<i class="fa fa-bookmark"></i> Amount
										</th>
										<th>
										</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>
											<a href="javascript:;">
											Pixel Ltd </a>
										</td>
										<td>
											 Server hardware purchase
										</td>
										<td>
											 52560.10$ <span class="label label-success label-sm">
											Paid </span>
										</td>
										<td>
											<a class="btn default btn-xs green-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											Smart House </a>
										</td>
										<td>
											 Office furniture purchase
										</td>
										<td>
											 5760.00$ <span class="label label-warning label-sm">
											Pending </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											FoodMaster Ltd </a>
										</td>
										<td>
											 Company Anual Dinner Catering
										</td>
										<td>
											 12400.00$ <span class="label label-success label-sm">
											Paid </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											WaterPure Ltd </a>
										</td>
										<td>
											 Payment for Jan 2013
										</td>
										<td>
											 610.50$ <span class="label label-danger label-sm">
											Overdue </span>
										</td>
										<td>
											<a class="btn default btn-xs red-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											Smart House </a>
										</td>
										<td>
											 Office furniture purchase
										</td>
										<td>
											 5760.00$ <span class="label label-warning label-sm">
											Pending </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											FoodMaster Ltd </a>
										</td>
										<td>
											 Company Anual Dinner Catering
										</td>
										<td>
											 12400.00$ <span class="label label-success label-sm">
											Paid </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											WaterPure Ltd </a>
										</td>
										<td>
											 Payment for Jan 2013
										</td>
										<td>
											 610.50$ <span class="label label-danger label-sm">
											Overdue </span>
										</td>
										<td>
											<a class="btn default btn-xs red-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											Pixel Ltd </a>
										</td>
										<td>
											 Server hardware purchase
										</td>
										<td>
											 52560.10$ <span class="label label-success label-sm">
											Paid </span>
										</td>
										<td>
											<a class="btn default btn-xs green-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											Smart House </a>
										</td>
										<td>
											 Office furniture purchase
										</td>
										<td>
											 5760.00$ <span class="label label-warning label-sm">
											Pending </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											FoodMaster Ltd </a>
										</td>
										<td>
											 Company Anual Dinner Catering
										</td>
										<td>
											 12400.00$ <span class="label label-success label-sm">
											Paid </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											Pixel Ltd </a>
										</td>
										<td>
											 Server hardware purchase
										</td>
										<td>
											 52560.10$ <span class="label label-success label-sm">
											Paid </span>
										</td>
										<td>
											<a class="btn default btn-xs green-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											Smart House </a>
										</td>
										<td>
											 Office furniture purchase
										</td>
										<td>
											 5760.00$ <span class="label label-warning label-sm">
											Pending </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<a href="javascript:;">
											FoodMaster Ltd </a>
										</td>
										<td>
											 Company Anual Dinner Catering
										</td>
										<td>
											 12400.00$ <span class="label label-success label-sm">
											Paid </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									</tbody>
									</table>
								</div>
								<div class="margin-top-20">
									<ul class="pagination pagination-circle">
										<li>
											<a href="javascript:;">
											Prev </a>
										</li>
										<li>
											<a href="javascript:;">
											1 </a>
										</li>
										<li>
											<a href="javascript:;">
											2 </a>
										</li>
										<li class="active">
											<a href="javascript:;">
											3 </a>
										</li>
										<li>
											<a href="javascript:;">
											4 </a>
										</li>
										<li>
											<a href="javascript:;">
											5 </a>
										</li>
										<li>
											<a href="javascript:;">
											Next </a>
										</li>
									</ul>
								</div>
							</div>
							<!--end tab-pane-->
							<div id="tab_1_5" class="tab-pane">
								<div class="row">
									<div class="col-md-12">
										<form action="javascript:;" class="alert alert-danger alert-borderless">
											<div class="input-group">
												<div class="input-cont">
													<input type="text" placeholder="Search..." class="form-control"/>
												</div>
												<span class="input-group-btn">
												<button type="button" class="btn green-haze">
												Search &nbsp; <i class="m-icon-swapright m-icon-white"></i>
												</button>
												</span>
											</div>
										</form>
									</div>
								</div>
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-advance table-hover">
									<thead>
									<tr>
										<th>
											 Photo
										</th>
										<th>
											 Fullname
										</th>
										<th>
											 Username
										</th>
										<th>
											 Joined
										</th>
										<th>
											 Points
										</th>
										<th>
											 Status
										</th>
										<th>
										</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar1.jpg" alt=""/>
										</td>
										<td>
											 Mark Nilson
										</td>
										<td>
											 makr124
										</td>
										<td>
											 19 Jan 2012
										</td>
										<td>
											 1245
										</td>
										<td>
											<span class="label label-sm label-success">
											Approved </span>
										</td>
										<td>
											<a class="btn default btn-xs red-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar2.jpg" alt=""/>
										</td>
										<td>
											 Filip Rolton
										</td>
										<td>
											 jac123
										</td>
										<td>
											 09 Feb 2012
										</td>
										<td>
											 15
										</td>
										<td>
											<span class="label label-sm label-info">
											Pending </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar3.jpg" alt=""/>
										</td>
										<td>
											 Colin Fox
										</td>
										<td>
											 col
										</td>
										<td>
											 19 Jan 2012
										</td>
										<td>
											 245
										</td>
										<td>
											<span class="label label-sm label-warning">
											Suspended </span>
										</td>
										<td>
											<a class="btn default btn-xs green-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar2.jpg" alt=""/>
										</td>
										<td>
											 Nick Stone
										</td>
										<td>
											 sanlim
										</td>
										<td>
											 11 Mar 2012
										</td>
										<td>
											 565
										</td>
										<td>
											<span class="label label-sm label-danger">
											Blocked </span>
										</td>
										<td>
											<a class="btn default btn-xs red-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar1.jpg" alt=""/>
										</td>
										<td>
											 Edward Cook
										</td>
										<td>
											 sanlim
										</td>
										<td>
											 11 Mar 2012
										</td>
										<td>
											 45245
										</td>
										<td>
											<span class="label label-sm label-danger">
											Blocked </span>
										</td>
										<td>
											<a class="btn default btn-xs green-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar2.jpg" alt=""/>
										</td>
										<td>
											 Nick Stone
										</td>
										<td>
											 sanlim
										</td>
										<td>
											 11 Mar 2012
										</td>
										<td>
											 24512
										</td>
										<td>
											<span class="label label-sm label-danger">
											Blocked </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar1.jpg" alt=""/>
										</td>
										<td>
											 Elis Lim
										</td>
										<td>
											 makr124
										</td>
										<td>
											 11 Mar 2012
										</td>
										<td>
											 145
										</td>
										<td>
											<span class="label label-sm label-success">
											Approved </span>
										</td>
										<td>
											<a class="btn default btn-xs red-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar2.jpg" alt=""/>
										</td>
										<td>
											 King Paul
										</td>
										<td>
											 king123
										</td>
										<td>
											 11 Mar 2012
										</td>
										<td>
											 456
										</td>
										<td>
											<span class="label label-sm label-info">
											Pending </span>
										</td>
										<td>
											<a class="btn default btn-xs blue-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar3.jpg" alt=""/>
										</td>
										<td>
											 Filip Larson
										</td>
										<td>
											 fil
										</td>
										<td>
											 11 Mar 2012
										</td>
										<td>
											 12453
										</td>
										<td>
											<span class="label label-sm label-warning">
											Suspended </span>
										</td>
										<td>
											<a class="btn default btn-xs green-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar2.jpg" alt=""/>
										</td>
										<td>
											 Martin Larson
										</td>
										<td>
											 larson12
										</td>
										<td>
											 01 Apr 2011
										</td>
										<td>
											 2453
										</td>
										<td>
											<span class="label label-sm label-danger">
											Blocked </span>
										</td>
										<td>
											<a class="btn default btn-xs red-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									<tr>
										<td>
											<img src="../../assets/admin/pages/media/profile/avatar1.jpg" alt=""/>
										</td>
										<td>
											 King Paul
										</td>
										<td>
											 sanlim
										</td>
										<td>
											 11 Mar 2012
										</td>
										<td>
											 905
										</td>
										<td>
											<span class="label label-sm label-danger">
											Blocked </span>
										</td>
										<td>
											<a class="btn default btn-xs green-stripe" href="javascript:;">
											View </a>
										</td>
									</tr>
									</tbody>
									</table>
								</div>
								<div class="margin-top-20">
									<ul class="pagination pagination-circle">
										<li>
											<a href="javascript:;">
											Prev </a>
										</li>
										<li>
											<a href="javascript:;">
											1 </a>
										</li>
										<li>
											<a href="javascript:;">
											2 </a>
										</li>
										<li class="active">
											<a href="javascript:;">
											3 </a>
										</li>
										<li>
											<a href="javascript:;">
											4 </a>
										</li>
										<li>
											<a href="javascript:;">
											5 </a>
										</li>
										<li>
											<a href="javascript:;">
											Next </a>
										</li>
									</ul>
								</div>
							</div>
							<!--end tab-pane-->
						</div>
					</div>
				</div>
				<!--end tabbable-->
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<div class="page-prefooter">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
				<h2>About</h2>
				<p>
					 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam dolore.
				</p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs12 footer-block">
				<h2>Subscribe Email</h2>
				<div class="subscribe-form">
					<form action="javascript:;">
						<div class="input-group">
							<input type="text" placeholder="mail@email.com" class="form-control">
							<span class="input-group-btn">
							<button class="btn" type="submit">Submit</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
				<h2>Follow Us On</h2>
				<ul class="social-icons">
					<li>
						<a href="javascript:;" data-original-title="rss" class="rss"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="facebook" class="facebook"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="twitter" class="twitter"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="googleplus" class="googleplus"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="youtube" class="youtube"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="vimeo" class="vimeo"></a>
					</li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
				<h2>Contacts</h2>
				<address class="margin-bottom-40">
				Phone: 800 123 3456<br>
				 Email: <a href="mailto:info@metronic.com">info@metronic.com</a>
				</address>
			</div>
		</div>
	</div>
</div>
<!-- END PRE-FOOTER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="container">
		 2014 &copy; Metronic by keenthemes. <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
	</div>
</div>
<div class="scroll-to-top">
	<i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="../../assets/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../../assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="../../assets/admin/pages/scripts/search.js"></script>
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
   Search.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>