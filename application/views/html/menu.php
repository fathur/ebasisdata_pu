<div class="hor-menu ">
    <ul class="nav navbar-nav">
        <?php foreach ($menu as $r):
            $query = $this->db->query('SELECT * FROM menu where idparent=' . $r->idmenu);
            if ($query->num_rows() > 0) {
                if ($r->kind == "classic-menu-dropdown") { ?> <!-- IF TYPE OF MENU = CLASSIC -->
                    <!-- BEGIN CLASSIC DROPDOWN -->
                    <li class="menu-dropdown <?php echo $r->kind; ?>">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            <?php echo $r->name; ?> <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu pull-left">
                            <?php $querysub3 = $this->db->query('SELECT * FROM menu where idparent=' . $r->idmenu);
                            if ($querysub3->num_rows() > 0) {
                                foreach ($query->result_array() as $rowsub2):
                                    echo '<li class="dropdown-submenu ' . $rowsub2['csubmenu_caret'] . '">';
                                    echo '<a href="' . BASE_URL($rowsub2['link']) . '" ' . $rowsub2['atribut'] . '>';
                                    echo '<i class="' . $rowsub2['icon'] . '"></i> ';
                                    echo $rowsub2['name'] . "</a>";
                                    $querysub4 = $this->db->query('SELECT * FROM menu where idparent=' . $rowsub2['idmenu']);
                                    if ($querysub4->num_rows() > 0) {
                                        if ($querysub4->num_rows() > 0) {
                                            echo '<ul class="dropdown-menu">';
                                            foreach ($querysub4->result_array() as $rowsub3):
                                                echo '<li>';
                                                echo '<a href="' . BASE_URL($rowsub3['link']) . '" ' . $rowsub3['atribut'] . '>';
                                                echo $rowsub3['name'] . "</a>";
                                                echo '</li>';
                                            endforeach;
                                            echo '</ul>';
                                        }
                                    }
                                    echo '</li>';
                                endforeach;
                            } ?>
                        </ul>
                    </li>
                    <!-- END CLASSIC DROPDOWN -->
                    <?php
                } else { ?> <!-- IF TYPE OF MENU = MEGAMENU DROPDOWN -->
                    <!-- BEGIN MEGAMENU DROPDOWN -->
                    <li class="menu-dropdown <?php echo $r->kind; ?>">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;" class="dropdown-toggle">
                            <?php echo $r->name; ?> <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu" style="min-width: 710px">
                            <li>
                                <div class="mega-menu-content">
                                    <div class="row">
                                        <?php foreach ($query->result_array() as $row): ?>
                                            <div class="col-md-4">
                                                <ul class="mega-menu-submenu">
                                                    <!--<li>
													<h3><?php //echo $row['name']?> </h3>
												</li>-->
                                                    <?php $querysub = $this->db->query('SELECT * FROM menu where idparent=' . $row['idmenu']);
                                                    if ($querysub->num_rows() > 0) { ?>
                                                        <?php foreach ($querysub->result_array() as $rowsub): ?>
                                                            <li>
                                                                <a href="<?php echo BASE_URL($rowsub['link']) ?>"
                                                                   class="iconify">
                                                                    <i class="<?php echo $rowsub['icon'] ?>"></i>
                                                                    <?php echo $rowsub['name'] ?>
                                                                    <?php $querydiff = $this->db->query('select datediff(current_date(), `date`) as diff from menu where idmenu=' . $row['idmenu']);
                                                                    foreach ($querydiff->result_array() as $rowdiff):
                                                                        if ($rowdiff['diff'] < 30) { ?>
                                                                            <span class="badge badge-round badge-danger">baru</span><?php } endforeach; ?>
                                                                </a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    <?php } else { ?>
                                                        <li>
                                                            <a href="<?php echo BASE_URL($row['link']) ?>"
                                                               class="iconify">
                                                                <i class="<?php echo $row['icon'] ?>"></i>
                                                                <?php echo $row['name'] ?>
                                                                <?php $querydiff = $this->db->query('select datediff(current_date(), `date`) as diff from menu where idmenu=' . $row['idmenu']);
                                                                foreach ($querydiff->result_array() as $rowdiff):
                                                                    if ($rowdiff['diff'] < 30) { ?>
                                                                        <span class="badge badge-round badge-danger">baru</span><?php } endforeach; ?>
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- END MEGAMENU DROPDOWN -->
                    <?php
                }
            } else {
                ?>
                <li <?php echo 'class="' . $r->active . '"'; ?>>
                    <a href="<?php echo BASE_URL($r->link); ?>" <?php echo $r->atribut; ?>><?php echo $r->name; ?></a>
                </li>
            <?php } ?>
        <?php endforeach; ?>
    </ul>
</div>
