<!DOCTYPE html>
<!-- saved from url=(0048)http://localhost/edbpupr/application/views/html/ -->
<html lang="en" class="no-js"><!-- BEGIN HEAD --><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/css" rel="stylesheet" type="text/css">
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/simple-line-icons.min.css" rel="stylesheet" type="text/css">
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/uniform.default.css" rel="stylesheet" type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jqvmap.css" rel="stylesheet" type="text/css">
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/morris.css" rel="stylesheet" type="text/css">
    <!-- END PAGE LEVEL PLUGIN STYLES -->
    <!-- BEGIN PAGE STYLES -->
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/tasks.css" rel="stylesheet" type="text/css">
    <!-- END PAGE STYLES -->
    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/components.css" id="style_components" rel="stylesheet" type="text/css">
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/plugins.css" rel="stylesheet" type="text/css">
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/layout.css" rel="stylesheet" type="text/css">
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/default.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/custom.css" rel="stylesheet" type="text/css">
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="http://localhost/edbpupr/application/views/html/favicon.ico">
  <style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style><script type="text/javascript">try {
var AG_onLoad=function(func){if(document.readyState==="complete"||document.readyState==="interactive")func();else if(document.addEventListener)document.addEventListener("DOMContentLoaded",func);else if(document.attachEvent)document.attachEvent("DOMContentLoaded",func)};
var AG_removeElementById = function(id) { var element = document.getElementById(id); if (element && element.parentNode) { element.parentNode.removeChild(element); }};
var AG_removeElementBySelector = function(selector) { if (!document.querySelectorAll) { return; } var nodes = document.querySelectorAll(selector); if (nodes) { for (var i = 0; i < nodes.length; i++) { if (nodes[i] && nodes[i].parentNode) { nodes[i].parentNode.removeChild(nodes[i]); } } } };
var AG_each = function(selector, fn) { if (!document.querySelectorAll) return; var elements = document.querySelectorAll(selector); for (var i = 0; i < elements.length; i++) { fn(elements[i]); }; };
var AG_removeParent = function(el, fn) { while (el && el.parentNode) { if (fn(el)) { el.parentNode.removeChild(el); return; } el = el.parentNode; } };
var AdFox_getCodeScript = function() {};
AG_onLoad(function() { AG_each('iframe[id^="AdFox_iframe_"]', function(el) { if (el && el.parentNode) { el.parentNode.removeChild(el); } }); });
try { Object.defineProperty(window, 'noAdsAtAll', { get: function() { return true; } }); } catch (ex) {}
window.wcs_add = {}; window.wcs_do = function() { };
} catch (ex) { console.error('Error executing AG js: ' + ex); }</script></head>
  <!-- END HEAD -->
  <!-- BEGIN BODY -->
  <!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
  <!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
  <body>
    <?php include "header2.php;"?>
      <!-- BEGIN HEADER MENU -->
      <div class="page-header-top">
        <div class="container">
          <!-- BEGIN LOGO -->
          <div class="page-logo">
            <span class="pull-left"><a href="http://localhost/edbpupr/application/views/html/index.html">
                <img src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/logo_header.png" alt="Logo PUPR" class="logo-default">
              </a></span>
            <span style="pull-right"><section class="text-header-kementerian">
                Direktorat Jenderal Penyediaan Perumahan
                <br>Kementerian Pekerjaan Umum dan Perumahan Rakyat
              </section><section class="site-title">e-BASISDATA PERUMAHAN</section></span>
          </div>
          <!-- END LOGO -->
          <!-- BEGIN RESPONSIVE MENU TOGGLER -->
          <a href="javascript:;" class="menu-toggler"></a>
          <!-- END RESPONSIVE MENU TOGGLER -->
          <!-- BEGIN TOP NAVIGATION MENU -->
          <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
              <!-- DISABLED BEGIN INBOX DROPDOWN
					<li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="circle">3</span>
						<span class="corner"></span>
						</a>
						<ul class="dropdown-menu">
							<li class="external">
								<h3>You have <strong>7 New</strong> Messages</h3>
								<a href="javascript:;">view all</a>
							</li>
							<li>
								<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../../assets/admin/layout3/img/avatar2.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Lisa Wong </span>
										<span class="time">Just Now </span>
										</span>
										<span class="message">
										Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
										</a>
									</li>
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../../assets/admin/layout3/img/avatar3.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Richard Doe </span>
										<span class="time">16 mins </span>
										</span>
										<span class="message">
										Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
										</a>
									</li>
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../../assets/admin/layout3/img/avatar1.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Bob Nilson </span>
										<span class="time">2 hrs </span>
										</span>
										<span class="message">
										Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
										</a>
									</li>
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../../assets/admin/layout3/img/avatar2.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Lisa Wong </span>
										<span class="time">40 mins </span>
										</span>
										<span class="message">
										Vivamus sed auctor 40% nibh congue nibh... </span>
										</a>
									</li>
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../../assets/admin/layout3/img/avatar3.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Richard Doe </span>
										<span class="time">46 mins </span>
										</span>
										<span class="message">
										Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					END INBOX DROPDOWN -->
              <!-- BEGIN USER LOGIN DROPDOWN -->
              <li class="dropdown dropdown-user dropdown-dark">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                  <span style="text-align: right; padding: 0 10px 0 5px" class="username username-hide-mobile">Provinsi<br>Nanggroe Aceh Darussalam</span>
                  <img alt="" class="img-circle" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar_oepaij.png">
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                  <li>
                    <a href="http://localhost/edbpupr/application/views/html/extra_profile.html"> <i class="icon-user"></i> My Profile </a>
                  </li>
                  <li>
                    <a href="http://localhost/edbpupr/application/views/html/login.html"> <i class="icon-key"></i> Log Out </a>
                  </li>
                </ul>
              </li>
              <!-- END USER LOGIN DROPDOWN -->
              <!-- DISABLED BEGIN USER LOGIN DROPDOWN
					<li class="dropdown dropdown-extended quick-sidebar-toggler">
	          <span class="sr-only">Toggle Quick Sidebar</span>
            <i class="icon-logout"></i>
          </li>
					END USER LOGIN DROPDOWN -->
              <li class="droddown dropdown-separator">
                <span class="separator"></span>
              </li>
              <!-- BEGIN NOTIFICATION DROPDOWN -->
              <li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="icon-bell"></i> <span class="badge badge-default">7</span></a>
                <ul class="dropdown-menu">
                  <li class="external">
                    <h3>You have <strong>12 pending</strong> tasks</h3>
                    <a href="javascript:;">view all</a>
                  </li>
                  <li>
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;"><ul class="dropdown-menu-list scroller" style="height: 250px; overflow: hidden; width: auto;" data-handle-color="#637283" data-initialized="1">
                      <li>
                        <a href="javascript:;"><span class="time">just now</span><span class="details"><span class="label label-sm label-icon label-success"> <i class="fa fa-plus"></i> </span> New user registered.</span></a>
                      </li>
                      <li>
                        <a href="javascript:;"><span class="time">3 mins</span><span class="details"><span class="label label-sm label-icon label-danger"> <i class="fa fa-bolt"></i> </span> Server #12 overloaded.</span></a>
                      </li>
                      <li>
                        <a href="javascript:;"><span class="time">14 hrs</span><span class="details"><span class="label label-sm label-icon label-info"> <i class="fa fa-bullhorn"></i> </span>Application error.</span></a>
                      </li>
                    </ul><div class="slimScrollBar" style="background: rgb(99, 114, 131); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                  </li>
                </ul>
              </li>
              <!-- END NOTIFICATION DROPDOWN -->
              <!-- DISABLED BEGIN TO DO DROPDOWN
          <li class="dropdown dropdown-extended dropdown-dark dropdown-tasks" id="header_task_bar">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <i class="icon-calendar"></i>
            <span class="badge badge-default">3</span>
            </a>
            <ul class="dropdown-menu extended tasks">
              <li class="external">
                <h3>You have <strong>12 pending</strong> tasks</h3>
                <a href="javascript:;">view all</a>
              </li>
              <li>
                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                  <li>
                    <a href="javascript:;">
                    <span class="task">
                    <span class="desc">New release v1.2 </span>
                    <span class="percent">30%</span>
                    </span>
                    <span class="progress">
                    <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">40% Complete</span></span>
                    </span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                    <span class="task">
                    <span class="desc">Application deployment</span>
                    <span class="percent">65%</span>
                    </span>
                    <span class="progress">
                    <span style="width: 65%;" class="progress-bar progress-bar-danger" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">65% Complete</span></span>
                    </span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                    <span class="task">
                    <span class="desc">Mobile app release</span>
                    <span class="percent">98%</span>
                    </span>
                    <span class="progress">
                    <span style="width: 98%;" class="progress-bar progress-bar-success" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">98% Complete</span></span>
                    </span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                    <span class="task">
                    <span class="desc">Database migration</span>
                    <span class="percent">10%</span>
                    </span>
                    <span class="progress">
                    <span style="width: 10%;" class="progress-bar progress-bar-warning" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">10% Complete</span></span>
                    </span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                    <span class="task">
                    <span class="desc">Web server upgrade</span>
                    <span class="percent">58%</span>
                    </span>
                    <span class="progress">
                    <span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">58% Complete</span></span>
                    </span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                    <span class="task">
                    <span class="desc">Mobile development</span>
                    <span class="percent">85%</span>
                    </span>
                    <span class="progress">
                    <span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">85% Complete</span></span>
                    </span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                    <span class="task">
                    <span class="desc">New UI release</span>
                    <span class="percent">38%</span>
                    </span>
                    <span class="progress progress-striped">
                    <span style="width: 38%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100"><span class="sr-only">38% Complete</span></span>
                    </span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
           -->
            </ul>
          </div>
          <!-- END TOP NAVIGATION MENU -->
        </div>
      </div>
      <div class="page-header-menu">
        <div class="container">

          <!-- END HEADER SEARCH BOX -->
          <!-- BEGIN MEGA MENU -->
          <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
          <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
          <div class="hor-menu">
            <ul class="nav navbar-nav">
              <li class="active">
                <a href="http://localhost/edbpupr/application/views/html/index.html">Dashboard</a>
              </li>
              <li class="menu-dropdown classic-menu-dropdown ">
                <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="hover-initialized">
                  Lihat Data<i class="fa fa-angle-down"></i> </a>
                <ul class="dropdown-menu pull-left">
                  <li class=" dropdown-submenu">
                    <a href="http://localhost/edbpupr/application/views/html/:;"> <i class="icon-docs"></i> Kebutuhan Perumahan </a>
                    <ul class="dropdown-menu">
                      <li class=" ">
                        <a href="http://localhost/edbpupr/application/views/html/data_backlog.html"> Backlog Perumahan </a>
                      </li>
                      <li class=" ">
                        <a href="http://localhost/edbpupr/application/views/html/data_rtlh.html"> Rumah Tak Layak Huni </a>
                      </li>
                    </ul>
                  </li>
                  <li class=" dropdown-submenu">
                    <a href="http://localhost/edbpupr/application/views/html/:;"> <i class="icon-docs"></i> Penyediaan Perumahan </a>
                    <ul class="dropdown-menu">
                      <li class=" ">
                        <a href="http://localhost/edbpupr/application/views/html/data_imb.html"> Data IMB </a>
                      </li>
                      <li class=" ">
                        <a href="http://localhost/edbpupr/application/views/html/data_non_imb.html"> Data Non-IMB </a>
                      </li>
                      <li class=" ">
                        <a href="http://localhost/edbpupr/application/views/html/data_program_apbn.html"> Program APBN <span class="badge badge-roundless badge-danger">baru</span> </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="menu-dropdown classic-menu-dropdown ">
                <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="hover-initialized">
                  Input Data<i class="fa fa-angle-down"></i> </a>
                <ul class="dropdown-menu pull-left">
                  <li class=" dropdown-submenu">
                    <a href="http://localhost/edbpupr/application/views/html/:;"> <i class="icon-note"></i> Formulir I </a>
                    <ul class="dropdown-menu">
                      <li class=" ">
                        <a href="http://localhost/edbpupr/application/views/html/form_1a.html"> Formulir IA (Provinsi)</a>
                      </li>
                      <li class=" ">
                        <a href="http://localhost/edbpupr/application/views/html/form_1b.html"> Formulir IB (Kabupaten/Kota)</a>
                      </li>
                    </ul>
                  </li>
                  <li class=" dropdown-submenu">
                    <a href="http://localhost/edbpupr/application/views/html/:;"> <i class="icon-note"></i> Formulir Suplai Perumahan </a>
                    <ul class="dropdown-menu">
                      <li class=" ">
                        <a href="http://localhost/edbpupr/application/views/html/form_imb.html"> Formulir IMB </a>
                      </li>
                      <li class=" ">
                        <a href="http://localhost/edbpupr/application/views/html/form_non_imb.html"> Formulir Non-IMB </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="menu-dropdown classic-menu-dropdown ">
                <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="hover-initialized">
                  Laporan<i class="fa fa-angle-down"></i> </a>
                <ul class="dropdown-menu pull-left">
                  <li class="mega-menu-content">
                    <a href="http://localhost/edbpupr/application/views/html/lap_rekapitulasi.html"> <i class="icon-grid"></i> Rekapitulasi </a>
                  </li>
                  <li class="mega-menu-content">
                    <a href="http://localhost/edbpupr/application/views/html/lap_infografik.html"> <i class="icon-bar-chart"></i> Infografik </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <!-- END MEGA MENU -->
        </div>
      </div>
      <!-- END HEADER MENU -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
      <!-- BEGIN PAGE HEAD -->
      <div class="page-head">
        <div class="container">
          <!-- BEGIN PAGE TITLE -->
          <div class="page-title">
            <h1>Dashboard <small class="page-title-tag">rangkuman data perumahan</small></h1>
          </div>
          <!-- END PAGE TITLE -->
          <!-- BEGIN PAGE TOOLBAR -->
          <div class="page-toolbar">
            <!-- DISABLED BEGIN THEME PANEL
          <div class="btn-group btn-theme-panel">
            <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown"> <i class="icon-settings"></i> </a>
            <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <h3>THEME COLORS</h3>
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <ul class="theme-colors">
                        <li class="theme-color theme-color-default" data-theme="default">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Default</span>
                        </li>
                        <li class="theme-color theme-color-blue-hoki" data-theme="blue-hoki">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Blue Hoki</span>
                        </li>
                        <li class="theme-color theme-color-blue-steel" data-theme="blue-steel">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Blue Steel</span>
                        </li>
                        <li class="theme-color theme-color-yellow-orange" data-theme="yellow-orange">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Orange</span>
                        </li>
                        <li class="theme-color theme-color-yellow-crusta" data-theme="yellow-crusta">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Yellow Crusta</span>
                        </li>
                      </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <ul class="theme-colors">
                        <li class="theme-color theme-color-green-haze" data-theme="green-haze">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Green Haze</span>
                        </li>
                        <li class="theme-color theme-color-red-sunglo" data-theme="red-sunglo">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Red Sunglo</span>
                        </li>
                        <li class="theme-color theme-color-red-intense" data-theme="red-intense">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Red Intense</span>
                        </li>
                        <li class="theme-color theme-color-purple-plum" data-theme="purple-plum">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Purple Plum</span>
                        </li>
                        <li class="theme-color theme-color-purple-studio" data-theme="purple-studio">
                          <span class="theme-color-view"></span>
                          <span class="theme-color-name">Purple Studio</span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 seperator">
                  <h3>LAYOUT</h3>
                  <ul class="theme-settings">
                    <li>
                      Theme Style
                      <select class="theme-setting theme-setting-style form-control input-sm input-small input-inline tooltips" data-original-title="Change theme style" data-container="body" data-placement="left">
                        <option value="boxed" selected="selected">Square corners</option>
                        <option value="rounded">Rounded corners</option>
                      </select>
                    </li>
                    <li>
                      Layout
                      <select class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips" data-original-title="Change layout type" data-container="body" data-placement="left">
                        <option value="boxed" selected="selected">Boxed</option>
                        <option value="fluid">Fluid</option>
                      </select>
                    </li>
                    <li>
                      Top Menu Style
                      <select class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change top menu dropdowns style" data-container="body" data-placement="left">
                        <option value="dark" selected="selected">Dark</option>
                        <option value="light">Light</option>
                      </select>
                    </li>
                    <li>
                      Top Menu Mode
                      <select class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) top menu" data-container="body" data-placement="left">
                        <option value="fixed">Fixed</option>
                        <option value="not-fixed" selected="selected">Not Fixed</option>
                      </select>
                    </li>
                    <li>
                      Mega Menu Style
                      <select class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change mega menu dropdowns style" data-container="body" data-placement="left">
                        <option value="dark" selected="selected">Dark</option>
                        <option value="light">Light</option>
                      </select>
                    </li>
                    <li>
                      Mega Menu Mode
                      <select class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) mega menu" data-container="body" data-placement="left">
                        <option value="fixed" selected="selected">Fixed</option>
                        <option value="not-fixed">Not Fixed</option>
                      </select>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          END THEME PANEL -->
          </div>
          <!-- END PAGE TOOLBAR -->
        </div>
      </div>
      <!-- END PAGE HEAD -->
      <!-- BEGIN PAGE CONTENT -->
      <div class="page-content">
        <div class="container">
          <!-- BEGIN PAGE BREADCRUMB -->
          <ul class="page-breadcrumb breadcrumb">
            <li>
              <a href="http://localhost/edbpupr/application/views/html/#">Home</a>
              <i class="fa fa-circle"></i>
            </li>
            <li class="active">
              Dashboard
            </li>
          </ul>
          <!-- END PAGE BREADCRUMB -->

          <!-- BAGIAN ISI -->

          <!-- BEGIN PAGE CONTENT INNER -->
          <div class="row margin-top-10">
            <div class="col-md-12 col-sm-12">
              <!-- BEGIN PORTLET-->
              <div class="portlet light ">
                  <div class="tabbable-custom ">
    								<ul class="nav nav-tabs">
    									<li class="active">
    										<a href="http://localhost/edbpupr/application/views/html/#tab11" data-toggle="tab"><i class="icon-bar-chart theme-font hide"></i>Backlog Kepemilikan</a>
    									</li>
    									<li>
    										<a href="http://localhost/edbpupr/application/views/html/#tab12" data-toggle="tab"><i class="icon-bar-chart theme-font hide"></i>Backlog Penghunian</a>
    									</li>
    									<li>
    										<a href="http://localhost/edbpupr/application/views/html/#tab13" data-toggle="tab"><i class="icon-bar-chart theme-font hide"></i>RTLH</a>
    									</li>
    									<li>
    										<a href="http://localhost/edbpupr/application/views/html/#tab14" data-toggle="tab"><i class="icon-bar-chart theme-font hide"></i>Suplai Perumahan</a>
    									</li>
    								</ul>
    								<div class="tab-content">
    									<div class="tab-pane active" id="tab11">
    										<p>
                          </p><div class="portlet-body">
                           <div id="chart_5" class="chart" style="height: 400px;">
                           </div>
                         </div>
    										<p></p>
    									</div>
    									<div class="tab-pane" id="tab12">
    										<p>
                          </p><div class="portlet-body">
                           <div id="chart_5" class="chart" style="height: 400px;">
                           </div>
                         </div>
    										<p></p>
    									</div>
    									<div class="tab-pane" id="tab13">
    										<p>
                          </p><div class="portlet-body">
                           <div id="chart_5" class="chart" style="height: 400px;">
                           </div>
                         </div>
    										<p></p>
    									</div>
    									<div class="tab-pane" id="tab14">
    										<p>
                          </p><div class="portlet-body">
                           <div id="chart_5" class="chart" style="height: 400px;">
                           </div>
                         </div>
    										<p></p>
    									</div>
    								</div>
    							</div>
    					</div>
              <!-- END PORTLET-->
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12">
      					<!-- BEGIN PORTLET-->
      					<div class="portlet light ">
      						<div class="portlet-title">
                    <table>
                      <tbody><tr>
                      	<td width="200">Backlog Kepemilikan </td> <td width="100" align="right">0-1000</td><td bgcolor="#CCCCCC" width="20"> </td>
                      	<td width="100" align="right">1000-3000 </td> <td bgcolor="#FFCCCC" width="20"> </td>
                      	<td width="100" align="right">3000-6000 </td> <td bgcolor="#FF9999" width="20"> </td>
                      	<td width="100" align="right">6000-9000 </td> <td bgcolor="#FF6666" width="20"> </td>
                      	<td width="100" align="right">9000-12000 </td> <td bgcolor="#FF3333" width="20"> </td>
                      	<td width="100" align="right">&gt;12000 </td> <td bgcolor="#FF0000" width="20"> </td>
                      </tr>
                    </tbody></table>
      						</div>
      						<div class="portlet-body">
                  <style>
                     #mapindonesia { height: 500px; }
                  </style>
                  <div id="mapindonesia">
                  </div>
      						</div>
      					</div>
      					<!-- END PORTLET-->
      			</div>
          </div>
          <!-- END PAGE CONTENT INNER -->
        </div>
        <!-- BEGIN QUICK SIDEBAR -->
        <a href="javascript:;" class="page-quick-sidebar-toggler"><i class="icon-login"></i></a>
        <div class="page-quick-sidebar-wrapper">
          <div class="page-quick-sidebar">
            <div class="nav-justified">
              <ul class="nav nav-tabs nav-justified">
                <li class="active">
                  <a href="http://localhost/edbpupr/application/views/html/#quick_sidebar_tab_1" data-toggle="tab">
							Users <span class="badge badge-danger">2</span> </a>
                </li>
                <li>
                  <a href="http://localhost/edbpupr/application/views/html/#quick_sidebar_tab_2" data-toggle="tab">
							Alerts <span class="badge badge-success">7</span> </a>
                </li>
                <li class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							More<i class="fa fa-angle-down"></i> </a>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                      <a href="http://localhost/edbpupr/application/views/html/#quick_sidebar_tab_3" data-toggle="tab"> <i class="icon-bell"></i> Alerts </a>
                    </li>
                    <li>
                      <a href="http://localhost/edbpupr/application/views/html/#quick_sidebar_tab_3" data-toggle="tab"> <i class="icon-info"></i> Notifications </a>
                    </li>
                    <li>
                      <a href="http://localhost/edbpupr/application/views/html/#quick_sidebar_tab_3" data-toggle="tab"> <i class="icon-speech"></i> Activities </a>
                    </li>
                    <li class="divider">
</li>
                    <li>
                      <a href="http://localhost/edbpupr/application/views/html/#quick_sidebar_tab_3" data-toggle="tab"> <i class="icon-settings"></i> Settings </a>
                    </li>
                  </ul>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
                  <div class="page-quick-sidebar-list" style="position: relative; overflow: hidden; width: auto; height: 512px;"><div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list" data-height="512" data-initialized="1" style="overflow: hidden; width: auto; height: 512px;">
                    <h3 class="list-heading">Staff</h3>
                    <ul class="media-list list-items">
                      <li class="media">
                        <div class="media-status">
                          <span class="badge badge-success">8</span>
                        </div>
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar3.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Bob Nilson</h4>
                          <div class="media-heading-sub">
                            Project Manager
</div>
                        </div>
                      </li>
                      <li class="media">
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar1.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Nick Larson</h4>
                          <div class="media-heading-sub">
                            Art Director
</div>
                        </div>
                      </li>
                      <li class="media">
                        <div class="media-status">
                          <span class="badge badge-danger">3</span>
                        </div>
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar4.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Deon Hubert</h4>
                          <div class="media-heading-sub">
                            CTO
</div>
                        </div>
                      </li>
                      <li class="media">
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar2.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Ella Wong</h4>
                          <div class="media-heading-sub">
                            CEO
</div>
                        </div>
                      </li>
                    </ul>
                    <h3 class="list-heading">Customers</h3>
                    <ul class="media-list list-items">
                      <li class="media">
                        <div class="media-status">
                          <span class="badge badge-warning">2</span>
                        </div>
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar6.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Lara Kunis</h4>
                          <div class="media-heading-sub">
                            CEO, Loop Inc
</div>
                          <div class="media-heading-small">
                            Last seen 03:10 AM
</div>
                        </div>
                      </li>
                      <li class="media">
                        <div class="media-status">
                          <span class="label label-sm label-success">new</span>
                        </div>
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar7.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Ernie Kyllonen</h4>
                          <div class="media-heading-sub">
                            Project Manager,
                            <br>
                            SmartBizz PTL
                          </div>
                        </div>
                      </li>
                      <li class="media">
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar8.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Lisa Stone</h4>
                          <div class="media-heading-sub">
                            CTO, Keort Inc
</div>
                          <div class="media-heading-small">
                            Last seen 13:10 PM
</div>
                        </div>
                      </li>
                      <li class="media">
                        <div class="media-status">
                          <span class="badge badge-success">7</span>
                        </div>
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar9.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Deon Portalatin</h4>
                          <div class="media-heading-sub">
                            CFO, H&amp;D LTD
</div>
                        </div>
                      </li>
                      <li class="media">
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar10.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Irina Savikova</h4>
                          <div class="media-heading-sub">
                            CEO, Tizda Motors Inc
</div>
                        </div>
                      </li>
                      <li class="media">
                        <div class="media-status">
                          <span class="badge badge-danger">4</span>
                        </div>
                        <img class="media-object" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar11.jpg" alt="...">
                        <div class="media-body">
                          <h4 class="media-heading">Maria Gomez</h4>
                          <div class="media-heading-sub">
                            Manager, Infomatic Inc
</div>
                          <div class="media-heading-small">
                            Last seen 03:10 AM
</div>
                        </div>
                      </li>
                    </ul>
                  </div><div class="slimScrollBar" style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 300.279px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(221, 221, 221); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                  <div class="page-quick-sidebar-item">
                    <div class="page-quick-sidebar-chat-user">
                      <div class="page-quick-sidebar-nav">
                        <a href="javascript:;" class="page-quick-sidebar-back-to-list"><i class="icon-arrow-left"></i>Back</a>
                      </div>
                      <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 408px;"><div class="page-quick-sidebar-chat-user-messages" data-height="408" data-initialized="1" style="overflow: hidden; width: auto; height: 408px;">
                        <div class="post out">
                          <img class="avatar" alt="" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar3.jpg">
                          <div class="message">
                            <span class="arrow"></span>
                            <a href="javascript:;" class="name">Bob Nilson</a>
                            <span class="datetime">20:15</span>
                            <span class="body">
												When could you send me the report ? </span>
                          </div>
                        </div>
                        <div class="post in">
                          <img class="avatar" alt="" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar2.jpg">
                          <div class="message">
                            <span class="arrow"></span>
                            <a href="javascript:;" class="name">Ella Wong</a>
                            <span class="datetime">20:15</span>
                            <span class="body">
												Its almost done. I will be sending it shortly </span>
                          </div>
                        </div>
                        <div class="post out">
                          <img class="avatar" alt="" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar3.jpg">
                          <div class="message">
                            <span class="arrow"></span>
                            <a href="javascript:;" class="name">Bob Nilson</a>
                            <span class="datetime">20:15</span>
                            <span class="body">
												Alright. Thanks! :) </span>
                          </div>
                        </div>
                        <div class="post in">
                          <img class="avatar" alt="" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar2.jpg">
                          <div class="message">
                            <span class="arrow"></span>
                            <a href="javascript:;" class="name">Ella Wong</a>
                            <span class="datetime">20:16</span>
                            <span class="body">
												You are most welcome. Sorry for the delay. </span>
                          </div>
                        </div>
                        <div class="post out">
                          <img class="avatar" alt="" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar3.jpg">
                          <div class="message">
                            <span class="arrow"></span>
                            <a href="javascript:;" class="name">Bob Nilson</a>
                            <span class="datetime">20:17</span>
                            <span class="body">
												No probs. Just take your time :) </span>
                          </div>
                        </div>
                        <div class="post in">
                          <img class="avatar" alt="" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar2.jpg">
                          <div class="message">
                            <span class="arrow"></span>
                            <a href="javascript:;" class="name">Ella Wong</a>
                            <span class="datetime">20:40</span>
                            <span class="body">
												Alright. I just emailed it to you. </span>
                          </div>
                        </div>
                        <div class="post out">
                          <img class="avatar" alt="" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar3.jpg">
                          <div class="message">
                            <span class="arrow"></span>
                            <a href="javascript:;" class="name">Bob Nilson</a>
                            <span class="datetime">20:17</span>
                            <span class="body">
												Great! Thanks. Will check it right away. </span>
                          </div>
                        </div>
                        <div class="post in">
                          <img class="avatar" alt="" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar2.jpg">
                          <div class="message">
                            <span class="arrow"></span>
                            <a href="javascript:;" class="name">Ella Wong</a>
                            <span class="datetime">20:40</span>
                            <span class="body">
												Please let me know if you have any comment. </span>
                          </div>
                        </div>
                        <div class="post out">
                          <img class="avatar" alt="" src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/avatar3.jpg">
                          <div class="message">
                            <span class="arrow"></span>
                            <a href="javascript:;" class="name">Bob Nilson</a>
                            <span class="datetime">20:17</span>
                            <span class="body">
												Sure. I will check and buzz you if anything needs to be corrected. </span>
                          </div>
                        </div>
                      </div><div class="slimScrollBar" style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 267.627px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                      <div class="page-quick-sidebar-chat-user-form">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="Type a message here...">
                          <div class="input-group-btn">
                            <button type="button" class="btn blue">
                              <i class="icon-paper-clip"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane page-quick-sidebar-alerts" id="quick_sidebar_tab_2">
                  <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 512px;"><div class="page-quick-sidebar-alerts-list" data-height="512" data-initialized="1" style="overflow: hidden; width: auto; height: 512px;">
                    <h3 class="list-heading">General</h3>
                    <ul class="feeds list-items">
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-info">
                                <i class="fa fa-shopping-cart"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                New order received with
                                <span class="label label-sm label-danger">
														Reference Number: DR23923 </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                            30 mins
</div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-success">
                                <i class="fa fa-user"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                You have 5 pending membership that requires a quick review.
</div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                            24 mins
</div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-danger">
                                <i class="fa fa-bell-o"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                Web server hardware needs to be upgraded.
                                <span class="label label-sm label-warning">
														Overdue </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                            2 hours
</div>
                        </div>
                      </li>
                      <li>
                        <a href="javascript:;">
                          <div class="col1">
                            <div class="cont">
                              <div class="cont-col1">
                                <div class="label label-sm label-default">
                                  <i class="fa fa-briefcase"></i>
                                </div>
                              </div>
                              <div class="cont-col2">
                                <div class="desc">
                                  IPO Report for year 2013 has been released.
</div>
                              </div>
                            </div>
                          </div>
                          <div class="col2">
                            <div class="date">
                              20 mins
</div>
                          </div>
                        </a>
                      </li>
                    </ul>
                    <h3 class="list-heading">System</h3>
                    <ul class="feeds list-items">
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-info">
                                <i class="fa fa-shopping-cart"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                New order received with
                                <span class="label label-sm label-success">
														Reference Number: DR23923 </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                            30 mins
</div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-success">
                                <i class="fa fa-user"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                You have 5 pending membership that requires a quick review.
</div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                            24 mins
</div>
                        </div>
                      </li>
                      <li>
                        <div class="col1">
                          <div class="cont">
                            <div class="cont-col1">
                              <div class="label label-sm label-warning">
                                <i class="fa fa-bell-o"></i>
                              </div>
                            </div>
                            <div class="cont-col2">
                              <div class="desc">
                                Web server hardware needs to be upgraded.
                                <span class="label label-sm label-default ">
														Overdue </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col2">
                          <div class="date">
                            2 hours
</div>
                        </div>
                      </li>
                      <li>
                        <a href="javascript:;">
                          <div class="col1">
                            <div class="cont">
                              <div class="cont-col1">
                                <div class="label label-sm label-info">
                                  <i class="fa fa-briefcase"></i>
                                </div>
                              </div>
                              <div class="cont-col2">
                                <div class="desc">
                                  IPO Report for year 2013 has been released.
</div>
                              </div>
                            </div>
                          </div>
                          <div class="col2">
                            <div class="date">
                              20 mins
</div>
                          </div>
                        </a>
                      </li>
                    </ul>
                  </div><div class="slimScrollBar" style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                </div>
                <div class="tab-pane page-quick-sidebar-settings" id="quick_sidebar_tab_3">
                  <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 512px;"><div class="page-quick-sidebar-settings-list" data-height="512" data-initialized="1" style="overflow: hidden; width: auto; height: 512px;">
                    <h3 class="list-heading">General Settings</h3>
                    <ul class="list-items borderless">
                      <li>
                        Enable Notifications
                        <input type="checkbox" class="make-switch" checked="" data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF">
                      </li>
                      <li>
                        Allow Tracking
                        <input type="checkbox" class="make-switch" data-size="small" data-on-color="info" data-on-text="ON" data-off-color="default" data-off-text="OFF">
                      </li>
                      <li>
                        Log Errors
                        <input type="checkbox" class="make-switch" checked="" data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF">
                      </li>
                      <li>
                        Auto Sumbit Issues
                        <input type="checkbox" class="make-switch" data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF">
                      </li>
                      <li>
                        Enable SMS Alerts
                        <input type="checkbox" class="make-switch" checked="" data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF">
                      </li>
                    </ul>
                    <h3 class="list-heading">System Settings</h3>
                    <ul class="list-items borderless">
                      <li>
                        Security Level
                        <select class="form-control input-inline input-sm input-small">
                          <option value="1">Normal</option>
                          <option value="2" selected="">Medium</option>
                          <option value="e">High</option>
                        </select>
                      </li>
                      <li>
                        Failed Email Attempts
                        <input class="form-control input-inline input-sm input-small" value="5">
                      </li>
                      <li>
                        Secondary SMTP Port
                        <input class="form-control input-inline input-sm input-small" value="3560">
                      </li>
                      <li>
                        Notify On System Error
                        <input type="checkbox" class="make-switch" checked="" data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF">
                      </li>
                      <li>
                        Notify On SMTP Error
                        <input type="checkbox" class="make-switch" checked="" data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF">
                      </li>
                    </ul>
                    <div class="inner-content">
                      <button class="btn btn-success">
                        <i class="icon-settings"></i> Save Changes
                      </button>
                    </div>
                  </div><div class="slimScrollBar" style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END QUICK SIDEBAR -->
      </div>
      <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->
    <!-- AKHIR BAGIAN ISI -->


   <?php include "footer2.php";?>
    <!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
<script src="../../../assets/global/plugins/respond.min.js"></script>
<script src="../../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.min.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery-migrate.min.js.download" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery-ui.min.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/bootstrap.min.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/bootstrap-hover-dropdown.min.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.slimscroll.min.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.blockui.min.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.cokie.min.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.uniform.min.js.download" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.vmap.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.vmap.russia.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.vmap.world.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.vmap.europe.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.vmap.germany.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.vmap.usa.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.vmap.sampledata.js.download" type="text/javascript"></script>
    <!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/morris.min.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/raphael-min.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/jquery.sparkline.min.js.download" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/metronic.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/layout.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/quick-sidebar.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/demo.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/index3.js.download" type="text/javascript"></script>
    <script src="./eBasisdata Perumahan _ Direktorat Jenderal Penyediaan Perumahan_files/tasks.js.download" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo(theme settings page)
   QuickSidebar.init(); // init quick sidebar
   Index.init(); // init index page
   Tasks.initDashboardWidget(); // init tash dashboard widget
});
</script>
    <!-- END JAVASCRIPTS -->

  <!-- END BODY -->

</body></html>
