<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Route for administer users
 */
$route['users/provinsi'] = 'User/province';
$route['users/provinsi/create'] = 'User/province_create';
$route['users/provinsi/store'] = 'User/province_store';
$route['users/provinsi/(:num)/edit'] = 'User/province_edit/$1';
$route['users/provinsi/(:num)/update'] = 'User/province_update/$1';
$route['users/provinsi/(:num)/kabupaten'] = 'User/district/$1';
$route['users/provinsi/(:num)/kabupaten/create'] = 'User/district_create/$1';
$route['users/provinsi/(:num)/kabupaten/store'] = 'User/district_store/$1';
$route['users/provinsi/(:num)/kabupaten/(:num)/edit'] = 'User/district_edit/$1/$2';
$route['users/provinsi/(:num)/kabupaten/(:num)/update'] = 'User/district_update/$1/$2';

$route['users/(:num)/password'] = 'User/password/$1';
$route['users/(:num)/update-password'] = 'User/update_password/$1';

/**
 * Route for Form 1A
 */
$route['form-1a'] = 'Form_A';
$route['form-1a/data'] = 'Form_A/data';
$route['form-1a/store'] = 'Form_A/store';
$route['form-1a/(:num)/edit'] = 'Form_A/edit/$1';
$route['form-1a/(:num)/cetak'] = 'Form_A/cetak/$1';
$route['form-1a/(:num)/delete'] = 'Form_A/delete/$1';

$route['form-1a/(:num)/k1/upload'] = 'Form_A_K1/upload/$1';

$route['form-1a/(:num)/k2/data-a'] = 'Form_A_K2/data_a/$1';
$route['form-1a/(:num)/k2/data-b'] = 'Form_A_K2/data_b/$1';

$route['form-1a/(:num)/k2a/(:num)/edit'] = 'Form_A_K2/edit_a/$1/$2';
$route['form-1a/(:num)/k2a/(:num)/update'] = 'Form_A_K2/update_a/$1/$2';

$route['form-1a/(:num)/k2b/create'] = 'Form_A_K2/create_b/$1';
$route['form-1a/(:num)/k2b/store'] = 'Form_A_K2/store_b/$1';
$route['form-1a/(:num)/k2b/(:num)/edit'] = 'Form_A_K2/edit_b/$1/$2';
$route['form-1a/(:num)/k2b/(:num)/update'] = 'Form_A_K2/update_b/$1/$2';
$route['form-1a/(:num)/k2b/(:num)/delete'] = 'Form_A_K2/delete_b/$1/$2';

$route['form-1a/(:num)/k3/create'] = 'Form_A_K3/create/$1';
$route['form-1a/(:num)/k3/(:num)/view'] = 'Form_A_K3/view/$1/$2';
$route['form-1a/(:num)/k3/(:num)/edit'] = 'Form_A_K3/edit/$1/$2';
$route['form-1a/(:num)/k3/(:num)/update'] = 'Form_A_K3/update/$1/$2';

$route['form-1a/(:num)/k4/data-a'] = 'Form_A_K4/data_a/$1';
$route['form-1a/(:num)/k4a/(:any)/edit'] = 'Form_A_K4/edit_a/$1/$2';
$route['form-1a/(:num)/k4a/(:num)/update'] = 'Form_A_K4/update_a/$1/$2';

$route['form-1a/(:num)/k4/data-b'] = 'Form_A_K4/data_b/$1';
$route['form-1a/(:num)/k4b/(:any)/edit'] = 'Form_A_K4/edit_b/$1/$2';
$route['form-1a/(:num)/k4b/(:num)/update'] = 'Form_A_K4/update_b/$1/$2';

$route['form-1a/(:num)/k4/data-c'] = 'Form_A_K4/data_c/$1';
$route['form-1a/(:num)/k4c/(:any)/edit'] = 'Form_A_K4/edit_c/$1/$2';
$route['form-1a/(:num)/k4c/(:num)/update'] = 'Form_A_K4/update_c/$1/$2';

$route['form-1a/(:num)/k4/data-d'] = 'Form_A_K4/data_d/$1';
$route['form-1a/(:num)/k4d/create'] = 'Form_A_K4/create_d/$1';
$route['form-1a/(:num)/k4d/(:num)/edit'] = 'Form_A_K4/edit_d/$1/$2';
$route['form-1a/(:num)/k4d/(:num)/update'] = 'Form_A_K4/update_d/$1/$2';

$route['form-1a/(:num)/k4/data-e'] = 'Form_A_K4/data_e/$1';
$route['form-1a/(:num)/k4e/create'] = 'Form_A_K4/create_e/$1';
$route['form-1a/(:num)/k4e/(:num)/edit'] = 'Form_A_K4/edit_e/$1/$2';
$route['form-1a/(:num)/k4e/(:num)/update'] = 'Form_A_K4/update_e/$1/$2';

$route['form-1a/(:num)/k5/data'] = 'Form_A_K5/data/$1';
$route['form-1a/(:num)/k5/create'] = 'Form_A_K5/create/$1';
$route['form-1a/(:num)/k5/(:num)/edit'] = 'Form_A_K5/edit/$1/$2';
$route['form-1a/(:num)/k5/(:num)/update'] = 'Form_A_K5/update/$1/$2';

$route['form-1a/(:num)/k6/data'] = 'Form_A_K6/data/$1';
$route['form-1a/(:num)/k6/create'] = 'Form_A_K6/create/$1';
$route['form-1a/(:num)/k6/(:num)/edit'] = 'Form_A_K6/edit/$1/$2';
$route['form-1a/(:num)/k6/(:num)/update'] = 'Form_A_K6/update/$1/$2';

$route['form-1a/(:num)/pengesahan/upload'] = 'Form_A/upload_pengesahan/$1';
$route['form-1a/(:num)/pengesahan/check'] = 'Form_A/check_pengesahan/$1';

/**
 * Route for Form 1B
 */
$route['form-1b'] = 'Form_B';
$route['form-1b/data'] = 'Form_B/data';
$route['form-1b/store'] = 'Form_B/store';
$route['form-1b/(:num)/edit'] = 'Form_B/edit/$1';
$route['form-1b/(:num)/cetak'] = 'Form_B/cetak/$1';
$route['form-1b/(:num)/delete'] = 'Form_B/delete/$1';

$route['form-1b/(:num)/k1/upload'] = 'Form_B_K1/upload/$1';

$route['form-1b/(:num)/k2/data-a'] = 'Form_B_K2/data_a/$1';
$route['form-1b/(:num)/k2/data-b'] = 'Form_B_K2/data_b/$1';

$route['form-1b/(:num)/k2a/(:num)/edit'] = 'Form_B_K2/edit_a/$1/$2';
$route['form-1b/(:num)/k2a/(:num)/update'] = 'Form_B_K2/update_a/$1/$2';

$route['form-1b/(:num)/k2b/create'] = 'Form_B_K2/create_b/$1';
$route['form-1b/(:num)/k2b/store'] = 'Form_B_K2/store_b/$1';
$route['form-1b/(:num)/k2b/(:num)/edit'] = 'Form_B_K2/edit_b/$1/$2';
$route['form-1b/(:num)/k2b/(:num)/update'] = 'Form_B_K2/update_b/$1/$2';
$route['form-1b/(:num)/k2b/(:num)/delete'] = 'Form_B_K2/delete_b/$1/$2';

$route['form-1b/(:num)/k3/create'] = 'Form_B_K3/create/$1';
$route['form-1b/(:num)/k3/(:num)/view'] = 'Form_B_K3/view/$1/$2';
$route['form-1b/(:num)/k3/(:num)/edit'] = 'Form_B_K3/edit/$1/$2';
$route['form-1b/(:num)/k3/(:num)/update'] = 'Form_B_K3/update/$1/$2';

$route['form-1b/(:num)/k4/data-a'] = 'Form_B_K4/data_a/$1';
$route['form-1b/(:num)/k4a/(:any)/edit'] = 'Form_B_K4/edit_a/$1/$2';
$route['form-1b/(:num)/k4a/(:num)/update'] = 'Form_B_K4/update_a/$1/$2';

$route['form-1b/(:num)/k4/data-b'] = 'Form_B_K4/data_b/$1';
$route['form-1b/(:num)/k4b/(:any)/edit'] = 'Form_B_K4/edit_b/$1/$2';
$route['form-1b/(:num)/k4b/(:num)/update'] = 'Form_B_K4/update_b/$1/$2';

$route['form-1b/(:num)/k4/data-c'] = 'Form_B_K4/data_c/$1';
$route['form-1b/(:num)/k4c/(:any)/edit'] = 'Form_B_K4/edit_c/$1/$2';
$route['form-1b/(:num)/k4c/(:num)/update'] = 'Form_B_K4/update_c/$1/$2';

$route['form-1b/(:num)/k4/data-d'] = 'Form_B_K4/data_d/$1';
$route['form-1b/(:num)/k4d/create'] = 'Form_B_K4/create_d/$1';
$route['form-1b/(:num)/k4d/(:num)/edit'] = 'Form_B_K4/edit_d/$1/$2';
$route['form-1b/(:num)/k4d/(:num)/update'] = 'Form_B_K4/update_d/$1/$2';

$route['form-1b/(:num)/k4/data-e'] = 'Form_B_K4/data_e/$1';
$route['form-1b/(:num)/k4e/create'] = 'Form_B_K4/create_e/$1';
$route['form-1b/(:num)/k4e/(:num)/edit'] = 'Form_B_K4/edit_e/$1/$2';
$route['form-1b/(:num)/k4e/(:num)/update'] = 'Form_B_K4/update_e/$1/$2';

$route['form-1b/(:num)/k5/data'] = 'Form_B_K5/data/$1';
$route['form-1b/(:num)/k5/create'] = 'Form_B_K5/create/$1';
$route['form-1b/(:num)/k5/(:num)/edit'] = 'Form_B_K5/edit/$1/$2';
$route['form-1b/(:num)/k5/(:num)/update'] = 'Form_B_K5/update/$1/$2';

$route['form-1b/(:num)/k6/data'] = 'Form_B_K6/data/$1';
$route['form-1b/(:num)/k6/create'] = 'Form_B_K6/create/$1';
$route['form-1b/(:num)/k6/(:num)/edit'] = 'Form_B_K6/edit/$1/$2';
$route['form-1b/(:num)/k6/(:num)/update'] = 'Form_B_K6/update/$1/$2';

$route['form-1b/(:num)/pengesahan/upload'] = 'Form_B/upload_pengesahan/$1';
$route['form-1b/(:num)/pengesahan/check'] = 'Form_B/check_pengesahan/$1';

$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
