<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sejuta_rumah extends CI_Model
{
    public function form_imb_view($iddinas)
    {
        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah');
        $this->db->where('iddinas=' . $iddinas);
        $this->db->where('jenis="IMB"');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_imb_view2($iddinas)
    {
        $this->db->select('*');
        $this->db->from('pengembang_view');
        $this->db->where('iddinas=' . $iddinas);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_imb_view3($iddinas)
    {
        $this->db->select('*');
        $this->db->from('perbankan');
        $this->db->where('iddinas=' . $iddinas);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_imb_lap($iddinas)
    {
        $this->db->select('*');
        $this->db->from('satu_juta_rumah_imb');
        $this->db->where('iddinas=' . $iddinas);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_non_imb_lap($iddinas)
    {
        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah_non_imb_view_lap');
        $this->db->where('iddinas=' . $iddinas);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_non_imb_view($iddinas)
    {
        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah');
        $this->db->where('iddinas=' . $iddinas);
        $this->db->where('jenis="Non IMB"');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_imb($idform_satu_juta_rumah)
    {
        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah_imb');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_non_imb($idform_satu_juta_rumah)
    {
        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah_non_imb');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_unit($idform_satu_juta_rumah)
    {
        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah_unit');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_insert($data)
    {
        $iddinas = $data['iddinas'];
        //$query = $this->db->query('SELECT * FROM form_satu_juta_rumah where bulan=0 and iddinas='.$iddinas);
        //if ($query->num_rows()<1) {
        $this->db->insert('form_satu_juta_rumah', $data);
        //}
    }


    public function fimb_insert($data)
    {
        $this->db->insert('form_satu_juta_rumah_imb', $data);
    }

    public function form_pengembang_insert($data)
    {
        $this->db->insert('pengembang', $data);
    }

    public function form_perbankan_insert($data)
    {
        $this->db->insert('perbankan', $data);
    }


    public function fimb_update($data, $id)
    {
        $this->db->where('idform_satu_juta_rumah_imb', $id);
        $this->db->update('form_satu_juta_rumah_imb', $data);
    }

    public function hapus_fsejuta_rumah($id)
    {
        $this->db->where('idform_satu_juta_rumah', $id);
        $this->db->delete('form_satu_juta_rumah');
    }

    public function hapus_fpengembang_view($id)
    {
        $this->db->where('idpengembang', $id);
        $this->db->delete('pengembang');
    }

    public function hapus_fimb($id)
    {
        $this->db->where('idform_satu_juta_rumah_imb', $id);
        $this->db->delete('form_satu_juta_rumah_imb');
    }

    public function fimb_unit_insert($data)
    {
        $this->db->insert('form_satu_juta_rumah_unit', $data);
    }

    public function fpengembang_mbr_insert($data)
    {
        $this->db->insert('pengembang_mbr', $data);
    }

    public function fperbankan_mbr_insert($data)
    {
        $this->db->insert('perbankan_detail', $data);
    }


    public function fimb_unit_update($data, $id)
    {
        $this->db->where('idform_satu_juta_rumah_unit', $id);
        $this->db->update('form_satu_juta_rumah_unit', $data);
    }

    public function form_satu_juta_rumah_update($data, $id)
    {
        $this->db->where('idform_satu_juta_rumah', $id);
        $this->db->update('form_satu_juta_rumah', $data);
    }

    public function pengembang_update($data, $id)
    {
        $this->db->where('idpengembang', $id);
        $this->db->update('pengembang', $data);
    }

    public function perbankan_update($data, $id)
    {
        $this->db->where('idperbankan', $id);
        $this->db->update('perbankan', $data);
    }


    public function kecamatan_combo($data)
    {
        $iddinas = $data['iddinas'];
        $idkabupaten_kota = $data['idkabupaten_kota'];
        $this->db->select('*');
        $this->db->from('kecamatan');
        $this->db->where('idkabupaten_kota=' . $idkabupaten_kota);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function hapus_pengembang_1($id)
    {
        $this->db->where('idpengembang_mbr', $id);
        $this->db->delete('pengembang_mbr');
    }

    public function hapus_perbankan($id)
    {
        $this->db->where('idperbankan', $id);
        $this->db->delete('perbankan');
    }

    public function hapus_unit_fimb($id)
    {
        $this->db->where('idform_satu_juta_rumah_unit', $id);
        $this->db->delete('form_satu_juta_rumah_unit');
    }

    public function form_imb_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $result = $query->result();
    }


    public function form_pengembang_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idpengembang) as idpengembang FROM pengembang where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idpengembang = $row['idpengembang'];
                endforeach;
            }
        } else $idpengembang = $id;

        $this->db->select('*');
        $this->db->from('pengembang_mbr_view');
        $this->db->where('idpengembang=' . $idpengembang);
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function pengembang_view($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idpengembang) as idpengembang FROM pengembang where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idpengembang = $row['idpengembang'];
                endforeach;
            }
        } else $idpengembang = $id;

        $this->db->select('*');
        $this->db->from('pengembang');
        $this->db->where('idpengembang=' . $idpengembang);
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_perbankan_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idperbankan) as idperbankan FROM perbankan where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idperbankan = $row['idperbankan'];
                endforeach;
            }
        } else $idperbankan = $id;

        $this->db->select('*');
        $this->db->from('perbankan_detail_view');
        $this->db->where('idperbankan=' . $idperbankan);
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_imb_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah_imb');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        //$this->db->limit(1,0);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_non_imb_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $result = $query->result();
    }


    public function pengembang_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idpengembang) as idpengembang FROM pengembang where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idpengembang = $row['idpengembang'];
                endforeach;
            }
        } else $idpengembang = $id;

        $this->db->select('*');
        $this->db->from('pengembang_mbr_view');
        $this->db->where('idpengembang=' . $idpengembang);
        $this->db->where('jenis="MBR"');
        $query = $this->db->get();
        return $result = $query->result();
    }


    public function form_perbankan_view($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idperbankan) as idperbankan FROM perbankan where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idperbankan = $row['idperbankan'];
                endforeach;
            }
        } else $idperbankan = $id;

        $this->db->select('*');
        $this->db->from('perbankan');
        $this->db->where('idperbankan=' . $idperbankan);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function perbankan_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idperbankan) as idperbankan FROM perbankan where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idperbankan = $row['idperbankan'];
                endforeach;
            }
        } else $idperbankan = $id;

        $this->db->select('*');
        $this->db->from('perbankan_detail_view');
        $this->db->where('idperbankan=' . $idperbankan);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function pengembang_view_detail2($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idpengembang) as idpengembang FROM pengembang where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idpengembang = $row['idpengembang'];
                endforeach;
            }
        } else $idpengembang = $id;

        $this->db->select('*');
        $this->db->from('pengembang_mbr_view');
        $this->db->where('idpengembang=' . $idpengembang);
        $this->db->where('jenis<>"MBR"');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_non_imb_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah_unit_view');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_unit_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah_unit_view');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_non_unit_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('*');
        $this->db->from('form_satu_juta_rumah_unit');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }


    public function form_satu_juta_rumah_imb_count($data, $id)
    {
        $iddinas = $data['iddinas'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('count(idform_satu_juta_rumah) as fcount');
        $this->db->from('form_satu_juta_rumah_imb');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_non_imb_count($data, $id)
    {
        $iddinas = $data['iddinas'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('count(idform_satu_juta_rumah) as fcount');
        $this->db->from('form_satu_juta_rumah_imb');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_unit_count($data, $id)
    {
        $iddinas = $data['iddinas'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('count(idform_satu_juta_rumah) as fcount');
        $this->db->from('form_satu_juta_rumah_unit');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_satu_juta_rumah_non_unit_count($data, $id)
    {
        $iddinas = $data['iddinas'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_satu_juta_rumah) as idform_satu_juta_rumah FROM form_satu_juta_rumah where iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform_satu_juta_rumah = $row['idform_satu_juta_rumah'];
                endforeach;
            }
        } else $idform_satu_juta_rumah = $id;

        $this->db->select('count(idform_satu_juta_rumah) as fcount');
        $this->db->from('form_satu_juta_rumah_unit');
        $this->db->where('idform_satu_juta_rumah=' . $idform_satu_juta_rumah);
        $query = $this->db->get();
        return $result = $query->result();
    }

}

?>