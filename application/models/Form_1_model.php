<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_1_model extends CI_Model
{
    public function form_1_view($user_id)
    {
        $this->db->select('f.idform_1,p.provinsi,f.iddinas,date_format(f.tanggal_buat,"%m-%d-%Y") as tanggal_buat,date_format(f.tanggal_buat,"%h:%m") as waktu_buat,f.tahun,f.status');
        $this->db->from('form_1 f');
        $this->db->join('provinsi p', 'f.idprovinsi=p.idprovinsi');
        $this->db->where('f.iddinas=' . $user_id);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_view_admin($user_id)
    {
        $this->db->select('provinsi.provinsi,form_1.idform_1,date_format(form_1.tanggal_buat,"%m-%d-%Y %h:%m") as tanggal_buat,form_1.tahun,form_1.status');
        $this->db->from('form_1');

        $this->db->join('provinsi', 'provinsi.idprovinsi= form_1.idprovinsi');
        $this->db->order_by('form_1.tahun', 'desc');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_view_admin2($user_id)
    {
        $this->db->select('kabupaten_kota.kabupaten_kota,form_1.idform_1,date_format(form_1.tanggal_buat,"%m-%d-%Y %h:%m") as tanggal_buat,form_1.tahun,form_1.status');
        $this->db->from('form_1');
        $this->db->where('id_privilage=3');
        $this->db->join('dinas', 'dinas.iddinas= form_1.iddinas');
        $this->db->join('kabupaten_kota', 'kabupaten_kota.iddinas= dinas.iddinas');
        $this->db->order_by('form_1.tahun', 'desc');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_progres($user_id)
    {
        $query = $this->db->query('SELECT iddinas FROM dinas where iddinas=' . $user_id);
        foreach ($query->result_array() as $row):
            $iddinas = $row['iddinas'];
        endforeach;
        $this->db->select('idform_1,tahun,iddinas,persen,status');
        $this->db->from('form_1_progres');
        //$this->db->limit(1,0);
        if (isset($iddinas)) {
            $this->db->where('iddinas=' . $iddinas);
        }
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_progres_nr($user_id)
    {
        $query = $this->db->query('SELECT iddinas FROM dinas where iddinas=' . $user_id);
        foreach ($query->result_array() as $row):
            $iddinas = $row['iddinas'];
        endforeach;
        $this->db->select('(count(idform_1)) as c');
        $this->db->from('form_1_progres');
        if (isset($iddinas)) {
            $this->db->where('iddinas=' . $iddinas);
        }
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_view2($user_id)
    {
        $query = $this->db->query('SELECT iddinas FROM dinas_view where iduser=' . $user_id);
        foreach ($query->result_array() as $row):
            $iddinas = $row['iddinas'];
        endforeach;
        $this->db->select('date_format(tanggal_buat,"%m-%d-%Y") as tanggal_buat,date_format(tanggal_buat,"%h:%i %p") as jam_buat,date_format(tanggal_pengesahan,"%m-%d-%Y") as tanggal_pengesahan,tahun,status');
        $this->db->from('form_1');
        $this->db->where('iddinas=' . $iddinas . ' and (status="Belum Disetujui" or status="Disetujui" or status="Butuh Revisi")');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function rtlh($user_id, $tahun)
    {
        $this->db->select('*');
        $this->db->from('rtlh');
        $this->db->where('tahun=' . $tahun);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kabupaten_kota($idprovinsi)
    {
        $this->db->select('*');
        $this->db->from('kabupaten_kota');
        $this->db->where('idprovinsi=' . $idprovinsi);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function cari_email($email, $data)
    {
        $this->db->where('email=', $email);
        $this->db->update('dinas', $data);
    }


    public function kecamatan($idkabupaten_kota)
    {
        $this->db->select('*');
        $this->db->from('kecamatan');
        $this->db->where('idkabupaten_kota=' . $idkabupaten_kota);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function rtlh_kabupaten_kota($user_id, $tahun, $idprovinsi)
    {
        $this->db->select('*');
        $this->db->from('form_1_k5_view');
        $this->db->where('tahun=' . $tahun);
        if ($user_id != 1) {
            $this->db->where('idprovinsi=' . $idprovinsi);
        }
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function rtlh_kecamatan($user_id, $tahun, $iddinas)
    {
        $this->db->select('*');
        $this->db->from('rtlh_all');
        $this->db->join('kecamatan', 'rtlh_all.idkecamatan = kecamatan.idkecamatan');
        $this->db->where('tahun=' . $tahun);
        $this->db->where('iddinas=' . $iddinas);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function status_kepemilikan_rumah($user_id, $tahun, $idprovinsi)
    {
        $this->db->select('*');
        $this->db->from('status_kepemilikan_rumah');
        $this->db->where('idprovinsi=' . $idprovinsi . ' and tahun=' . $tahun);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function status_kepemilikan_rumah_kabupaten_kota($user_id, $tahun, $iddinas)
    {
        $this->db->select('*');
        $this->db->from('status_kepemilikan_rumah_backlog');
        $this->db->where('iddinas=' . $iddinas . ' and tahun=' . $tahun);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function status_kepemilikan_rumah2($tahun)
    {
        $this->db->select('*');
        $this->db->from('status_kepemilikan_rumah');
        $this->db->where('tahun=' . $tahun);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function status_kepemilikan_rumah3($tahun)
    {
        $this->db->select('sum(kepemilikan) as kepemilikan,sum(penghunian) as penghunian');
        $this->db->from('status_kepemilikan_rumah');
        $this->db->where('tahun=' . $tahun);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function Form1_insert($data)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $tahun = $data['tahun'];
        $query = $this->db->query('SELECT * FROM form_1 where (tahun=0 or tahun=' . $tahun . ') and idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
        if ($query->num_rows() < 1) {
            $this->db->insert('form_1', $data);
        }
        return $this->db->insert_id();
    }

    public function form_1_view_detail($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;
        $this->db->select('idform_1,date_format(tanggal_pengesahan,"%d/%m/%Y %h:%m") as tanggal_pengesahan,formulir,tahun,(tahun-1) as tahun_awal');
        $this->db->from('form_1');
        $this->db->where('idform_1=' . $idform);
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k1($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;
        $this->db->select('idform_1,struktur_dinas');
        $this->db->from('form_1_k1');
        $this->db->where('idform_1=' . $idform);
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k1_count($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('count(idform_1) as f1k1count');
        $this->db->from('form_1_k1');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k4_1_count($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('count(idform_1) as f1k41count');
        $this->db->from('form_1_k4_1');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k4_2_count($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('count(idform_1) as f1k42count');
        $this->db->from('form_1_k4_2');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k4_3_count($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('count(idform_1) as f1k43count');
        $this->db->from('form_1_k4_3');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k2_a($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1_k2_a,idform_1,uraian1,k221,k231');
        $this->db->from('form_1_k2_a');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k2_b($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1_k2_b,idform_1,jenis_kegiatan_urusan_pkp_2,ta_a_vol_unit_3,ta_a_biaya_4,ta_a_vol_unit_5,ta_a_biaya_6');
        $this->db->from('form_1_k2_b');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k2_b_sum($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1,sum(ta_a_vol_unit_3) as total_unit_rumah_1,sum(ta_a_vol_unit_5) as total_unit_rumah_2,sum(ta_a_biaya_4) as total_biaya_1,sum(ta_a_biaya_6) as total_biaya_2');
        $this->db->from('form_1_k2_b');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k3($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1_k3,idform_1,isi_1a,isi_1b,isi_1c,isi_1d,isi_1e,isi_1f,isi_1f_keterangan,isi_2a,isi_2b,isi_2c,isi_2d,isi_2e,isi_2c_keterangan,isi_2d_keterangan,isi_2e_keterangan');
        $this->db->from('form_1_k3');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k3_count($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('count(*) as c');
        $this->db->from('form_1_k3');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k4_1($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('status1,status2,status3,status4,status5,idform_1_k4_1,idform_1,k4141,k4142,k4143,k4144,k4145,k4151,k4152,k4153,k4154,k4155,(k4142+k4143+k4144+k4145) as total_a,(k4143) as total_b');
        $this->db->from('form_1_k4_1');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k4_2($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select(',jenis1,jenis2,idform_1_k4_2,idform_1,k4231,k4232,k4241,k4242,sum(k4231+k4232) as total');
        $this->db->from('form_1_k4_2');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k4_3($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1_k4_3,idform_1,k4331,k4332,k4341,k4342,fungsi1,fungsi2');
        $this->db->from('form_1_k4_3');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k4_4($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1,idform_1_k4_4,idkabupaten_kota,idkecamatan,idkelurahan,jumlah_rumah_3,jumlah_rumah_4,sumber_data_5,kelurahan,kecamatan,kabupaten_kota');
        $this->db->from('form_1_k4_4_view');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k4_4_sum($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1,sum(jumlah_rumah_3) as total_1,sum(jumlah_rumah_4) as total_2');
        $this->db->from('form_1_k4_4_view');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k4_5($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1_k4_5,idform_1,non_mbr_2,mbr_3,non_mbr_4,mbr_5,non_mbr_6,mbr_7,sumber_data_8');
        $this->db->from('form_1_k4_5');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k5($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1,idform_1_k5,idkabupaten_kota,idkecamatan,jumlah_kk_rt_3,jumlah_rtlh_versi_bdt_4,jumlah_rtlh_verifikasi_pemda_5,sumber_data_6,kabupaten_kota,kecamatan');
        $this->db->from('form_1_k5_view');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k5_sum($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1,sum(jumlah_kk_rt_3) as total_1,sum(jumlah_rtlh_versi_bdt_4) as total_2,sum(jumlah_rtlh_verifikasi_pemda_5) as total_3');
        $this->db->from('form_1_k5_view');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k6($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1,idform_1_k6,idkabupaten_kota,idkecamatan,idkelurahan,idkabupaten_kota,luas_wilayah_kumuh_4,jumlah_rtlh_dalam_wilayah_kumuh_5,sumber_data_6,kabupaten_kota,kecamatan,kelurahan');
        $this->db->from('form_1_k6_view');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k6_sum($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1,sum(luas_wilayah_kumuh_4) as total_luas_wilayah_kumuh,sum(jumlah_rtlh_dalam_wilayah_kumuh_5) as total_jumlah_rtlh_dalam_wilayah_kumuh');
        $this->db->from('form_1_k6_view');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k6_b($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1,idform_1_k6,idkabupaten_kota,idkecamatan,idkelurahan,idkabupaten_kota,luas_wilayah_kumuh_4,jumlah_rtlh_dalam_wilayah_kumuh_5,sumber_data_6,kabupaten_kota,kecamatan,kelurahan');
        $this->db->from('form_1_k6_view_b');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function form_1_k6_sum_b($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);

        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;

        $this->db->select('idform_1,sum(luas_wilayah_kumuh_4) as total_luas_wilayah_kumuh,sum(jumlah_rtlh_dalam_wilayah_kumuh_5) as total_jumlah_rtlh_dalam_wilayah_kumuh');
        $this->db->from('form_1_k6_view_b');
        $this->db->where('idform_1=' . $idform);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kelengkapan($data, $id)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        if ($id == "0") {
            $query = $this->db->query('SELECT max(idform_1) as idform FROM form_1 where idprovinsi="' . $idprovinsi . '" and iddinas=' . $iddinas);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row):
                    $idform = $row['idform'];
                endforeach;
            }
        } else $idform = $id;
        $this->db->select('*');
        $this->db->from('kelengkapan');
        $this->db->where('k4_3=1 and k4_4=1 and k4_5=1 and k6=1 and idform_1=' . $idform);
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kabupaten_kota_combo($data)
    {
        $iddinas = $data['iddinas'];
        $idprovinsi = $data['idprovinsi'];
        $this->db->select('*');
        $this->db->from('kabupaten_kota');
        $this->db->where('idprovinsi=' . $idprovinsi);
        $this->db->order_by('idkabupaten_kota', "asc");
        $query = $this->db->get();
        return $result = $query->result();
    }

    //public function kecamatan_combo(){
    //$idkabupaten_kota=1;
    //	 $this->db->select('*');
    //     $this->db->from('kecamatan');
    //     $query = $this->db->get();
    //    return $result = $query->result();
    //}

    public function kecamatan_combo($idkabupaten_kota)
    {
        //$idkabupaten_kota=1;
        $this->db->select('*');
        $this->db->from('kecamatan');
        $this->db->where('idkabupaten_kota=' . $idkabupaten_kota);
        $query = $this->db->get();
        return $result = $query->result();
    }

    //johns
    public function Form1k2_a_insert($data)
    {
        $this->db->insert('form_1_k2_a', $data);
    }

    public function Form1k2_b_insert($data)
    {
        $this->db->insert('form_1_k2_b', $data);
    }

    public function Form1k3_insert($data)
    {
        $this->db->insert('form_1_k3', $data);
    }

    public function Form1k4_1_insert($data)
    {
        $this->db->insert('form_1_k4_1', $data);
    }

    public function Form1k4_2_insert($data)
    {
        $this->db->insert('form_1_k4_2', $data);
    }

    public function Form1k4_3_insert($data)
    {
        $this->db->insert('form_1_k4_3', $data);
    }

    public function form1k4_4_insert($data)
    {
        $this->db->insert('form_1_k4_4', $data);
    }

    public function form1k4_5_insert($data)
    {
        $this->db->insert('form_1_k4_5', $data);
    }

    public function form1k4_6_insert($data)
    {
        $this->db->insert('form_1_k4_5', $data);
    }

    public function form1k5_insert($data)
    {
        $this->db->insert('form_1_k5', $data);
    }

    public function form1k6_insert($data)
    {
        $this->db->insert('form_1_k6', $data);
    }

    public function Form1k6_kecamatan_insert($data)
    {
        $this->db->insert('form_1_k6_kecamatan', $data);
    }

    public function form1k1_insert($data)
    {
        $this->db->insert('form_1_k1', $data);
    }

    public function Form1k3_update($data, $id)
    {
        $this->db->where('idform_1_k3', $id);
        $this->db->update('form_1_k3', $data);
    }

    public function pengembang_imb_update($data, $id)
    {
        $this->db->where('idpengembang_mbr', $id);
        $this->db->update('pengembang_mbr', $data);
    }

    public function Form1k2_a_update($data, $id)
    {
        $this->db->where('idform_1_k2_a', $id);
        $this->db->update('form_1_k2_a', $data);
    }

    public function Form1k2_b_update($data, $id)
    {
        $this->db->where('idform_1_k2_b', $id);
        $this->db->update('form_1_k2_b', $data);
    }

    public function Form1k4_1_update($data, $id)
    {
        $this->db->where('idform_1_k4_1', $id);
        $this->db->update('form_1_k4_1', $data);
    }

    public function Form1k4_2_update($data, $id)
    {
        $this->db->where('idform_1_k4_2', $id);
        $this->db->update('form_1_k4_2', $data);
    }

    public function Form1k4_3_update($data, $id)
    {
        $this->db->where('idform_1_k4_3', $id);
        $this->db->update('form_1_k4_3', $data);
    }

    public function Form1k4_4_update($data, $id)
    {
        $this->db->where('idform_1_k4_4', $id);
        $this->db->update('form_1_k4_4', $data);
    }

    public function Form1k4_5_update($data, $id)
    {
        $this->db->where('idform_1_k4_5', $id);
        $this->db->update('form_1_k4_5', $data);
    }

    public function Form1k5_update($data, $id)
    {
        $this->db->where('idform_1_k5', $id);
        $this->db->update('form_1_k5', $data);
    }

    public function Form1k6_update($data, $id)
    {
        $this->db->where('idform_1_k6', $id);
        $this->db->update('form_1_k6', $data);
    }

    public function Form1_update($data, $idform_1)
    {
        $this->db->where('idform_1', $idform_1);
        $this->db->update('form_1', $data);
    }

    public function hapus_form_1($id)
    {
        $this->db->where('idform_1', $id);
        $this->db->delete('form_1');
    }

    public function hapus_gambar($id)
    {
        $this->db->where('idform_1', $id);
        $this->db->delete('form_1_k1');
    }

    public function hapus_form_1_k2_a($id)
    {
        $this->db->where('idform_1_k2_a', $id);
        $this->db->delete('form_1_k2_a');
    }

    public function hapus_form_1_k4_4($id)
    {
        $this->db->where('idform_1_k4_4', $id);
        $this->db->delete('form_1_k4_4');
    }

    public function hapus_form_1_k4_5($id)
    {
        $this->db->where('idform_1_k4_5', $id);
        $this->db->delete('form_1_k4_5');
    }

    public function hapus_form_1_k5($id)
    {
        $this->db->where('idform_1_k5', $id);
        $this->db->delete('form_1_k5');
    }

    public function hapus_form_1_k6($id)
    {
        $this->db->where('idform_1_k6', $id);
        $this->db->delete('form_1_k6');
    }

    public function hapus_pengembang_1($id)
    {
        $this->db->where('idpengembang_mbr', $id);
        $this->db->delete('pengembang_mbr');
    }

    public function hapus_form_1_k2_b($id)
    {
        $this->db->where('idform_1_k2_b', $id);
        $this->db->delete('form_1_k2_b');
    }

    public function hapus_kecamatan($id)
    {
        $this->db->where('idkecamatan', $id);
        $this->db->delete('kecamatan');
    }

    public function hapus_form_1_k4_1($id)
    {
        $this->db->where('idform_1_k4_1', $id);
        $this->db->delete('form_1_k4_1');
    }

    public function hapus_form_1_k4_2($id)
    {
        $this->db->where('idform_1_k4_2', $id);
        $this->db->delete('form_1_k4_2');
    }

    function rtlh_grafik()
    {
        $this->db->select('provinsi,rtlh');
        $this->db->from('rtlh');
        $query = $this->db->get();

        return $result = $query->result();
        //return $query -> result_array();
    }

    public function status_kepemilikan_rumah_map($tahun)
    {
        $this->db->select('*');
        $this->db->from('backlog_kepemilikan_dan_penghunian_view');
        //$this->db->where('tahun='.$tahun);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function status_kepemilikan_rumah_map_detail($tahun)
    {
        $this->db->select('*');
        $this->db->from('backlog_kepemilikan_dan_penghunian_kab_view');
        $this->db->where('idprovinsi=' . $tahun);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function rtlh_map($tahun)
    {
        $this->db->select('*');
        $this->db->from('rtlh');
        $this->db->where('tahun=' . $tahun);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function rtlh_map_prov($tahun)
    {
        $this->db->select('*');
        $this->db->from('backlog_kepemilikan_dan_penghunian_view');
        $query = $this->db->get();
        return $result = $query->result();
    }
}

?>
