<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 19/08/18
 * Time: 05.55
 */

namespace Model\View;


use Illuminate\Database\Eloquent\Model;

class UserAdmin extends Model
{
    protected $table = 'user_admin';

    protected $primaryKey = 'id';
}