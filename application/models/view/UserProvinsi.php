<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 19/08/18
 * Time: 06.13
 */

namespace Model\View;


use Illuminate\Database\Eloquent\Model;

class UserProvinsi extends Model
{
    protected $table = 'user_provinsi';

    protected $primaryKey ='iddinas';
}