<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 19/08/18
 * Time: 06.18
 */

namespace Model\View;


use Illuminate\Database\Eloquent\Model;

class UserKabupatenKota extends Model
{
    protected $table = 'user_kabupaten_kota';

    protected $primaryKey  = 'iddinas';
}