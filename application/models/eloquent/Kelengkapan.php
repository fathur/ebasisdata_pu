<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 27/08/18
 * Time: 07.39
 */

namespace Model\Eloquent;


use Illuminate\Database\Eloquent\Model;

class Kelengkapan extends Model
{
    public $timestamps = false;

    protected $table = 'kelengkapan';
    protected $primaryKey = 'id';

    protected $fillable = ['struktur_dinas',
        'k2_a','k2_b',
        'k3',
        'k4_1','k4_2','k4_3','k4_4','k4_5',
        'k5','k6'];
}