<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.57
 */

namespace Model\Eloquent\Region;


use Illuminate\Database\Eloquent\Model;
use Model\Eloquent\Dinas;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';
    protected $primaryKey = 'idkecamatan';
    protected $appends = ['id','name'];

    public function getIdAttribute()
    {
        return (int)$this->attributes['idkecamatan'];
    }

    public function getNameAttribute()
    {
        return $this->attributes['kecamatan'];
    }

    public function scopeKabupatenKota($query, $id)
    {
        return $query->where('idkabupaten_kota', '=', $id);
    }

    public function dinas()
    {
        return $this->belongsTo(Dinas::class, 'iddinas', 'iddinas');
    }
}