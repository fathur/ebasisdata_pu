<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.57
 */

namespace Model\Eloquent\Region;


use Illuminate\Database\Eloquent\Model;
use Model\Eloquent\Dinas;

class KabupatenKota extends Model
{
    protected $table = 'kabupaten_kota';
    protected $primaryKey = 'idkabupaten_kota';
    protected $appends = ['id', 'name', 'kepala_daerah', 'foto_kepala_daerah', 'wakil_kepala_daerah',
                          'foto_wakil_kepala_daerah', 'luas_wilayah', 'luas_wilayah_daratan', 'luas_wilayah_lautan',
                          'letak_geografis', 'jumlah_penduduk', 'pertumbuhan_penduduk', 'tingkat_kepadatan_penduduk',
                          'jumlah_penduduk_miskin_kota', 'jumlah_penduduk_miskin_desa'];
    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idkabupaten_kota'];
    }

    public function getNameAttribute()
    {
        return $this->attributes['kabupaten_kota'];
    }

    public function scopeProvinsi($query, $id)
    {
        return $query->where('idprovinsi', '=', $id);
    }

    public function dinas()
    {
        return $this->belongsTo(Dinas::class, 'iddinas', 'iddinas');
    }

    public function province()
    {
        return $this->belongsTo(Provinsi::class, 'idprovinsi', 'idprovinsi');

    }
}