<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.58
 */

namespace Model\Eloquent\Region;


use Illuminate\Database\Eloquent\Model;
use Model\Eloquent\Dinas;

class Provinsi extends Model
{
    protected $table = 'provinsi';
    protected $primaryKey = 'idprovinsi';
    protected $appends = ['id', 'name'];
    protected $fillable = ['provinsi', 'logo_prov'];
    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idprovinsi'];
    }

    /**
     * Supayan manggilnya gampang, alias jadi name.
     * @return mixed
     */
    public function getNameAttribute()
    {
        return $this->attributes['provinsi'];
    }

    public function dinas()
    {
        return $this->belongsTo(Dinas::class, 'iddinas', 'iddinas');
    }
}