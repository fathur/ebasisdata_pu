<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.57
 */

namespace Model\Eloquent\Region;


use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'kelurahan';
    protected $primaryKey = 'idkelurahan';
    protected $appends = ['id'];

    public function getIdAtrribute()
    {
        return $this->attributes['idkelurahan'];
    }
}