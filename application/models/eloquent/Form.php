<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 24/08/18
 * Time: 13.23
 */

namespace Model\Eloquent;


use Illuminate\Database\Eloquent\Model;
use Model\Eloquent\Form\K1;
use Model\Eloquent\Form\K2a;
use Model\Eloquent\Form\K2b;
use Model\Eloquent\Form\K3;
use Model\Eloquent\Form\K4a;
use Model\Eloquent\Form\K4b;
use Model\Eloquent\Form\K4c;
use Model\Eloquent\Form\K4d;
use Model\Eloquent\Form\K4e;
use Model\Eloquent\Form\K5;
use Model\Eloquent\Form\K6;
use Model\Eloquent\Region\KabupatenKota;
use Model\Eloquent\Region\Kecamatan;
use Model\Eloquent\Region\Provinsi;

class Form extends Model
{
    public $timestamps = false;

    protected $table = 'form_1';
    protected $primaryKey = 'idform_1';
    protected $appends = ['id'];
    protected $fillable = ['iddinas', 'idprovinsi', 'tahun', 'tanggal_pengesahan', 'formulir', 'status'];


    public function getIdAttribute()
    {
        return (int)$this->attributes['idform_1'];
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'idprovinsi', 'idprovinsi');
    }

//    public function kabupatenKota()
//    {
//        return $this->belongsTo(KabupatenKota::class, 'idkabupaten_kota', 'idkabupaten_kota');
//    }

    public function dinas()
    {
        return $this->belongsTo(Dinas::class, 'iddinas', 'iddinas');

    }

    public function k1()
    {
        return $this->hasOne(K1::class, 'idform_1', 'idform_1');
    }

    public function k2a()
    {
        return $this->hasMany(K2a::class, 'idform_1', 'idform_1');
    }

    public function k2b()
    {
        return $this->hasMany(K2b::class, 'idform_1', 'idform_1');
    }

    public function k3()
    {
        return $this->hasOne(K3::class, 'idform_1', 'idform_1');

    }

    public function k4a()
    {
        return $this->hasOne(K4a::class, 'idform_1', 'idform_1');
    }

    public function k4b()
    {
        return $this->hasOne(K4b::class, 'idform_1', 'idform_1');
    }

    public function k4c()
    {
        return $this->hasOne(K4c::class, 'idform_1', 'idform_1');

    }

    public function k4d()
    {
        return $this->hasMany(K4d::class, 'idform_1', 'idform_1');

    }

    public function k4e()
    {
        return $this->hasOne(K4e::class, 'idform_1', 'idform_1');

    }

    public function k5()
    {
        return $this->hasMany(K5::class, 'idform_1', 'idform_1');
    }

    public function k6()
    {
        return $this->hasMany(K6::class, 'idform_1', 'idform_1');
    }

    public function kelengkapan()
    {
        return $this->hasOne(Kelengkapan::class, 'idform_1', 'idform_1');
    }

    public function scopeOptionYears($query)
    {
        return $query->distinct('tahun')->orderBy('tahun')->pluck('tahun');
    }

    public function prepareK2a($type)
    {
        if ($type == '1a') {

        } elseif ($type == '1b') {

            # Update field because already filled with trigger

            $apbd = $this->k2a()->where('uraian1','=','Total APBD Provinsi')->first();
            $pkp = $this->k2a()->where('uraian1','=','Anggaran Dinas PKP Provinsi')->first();

            $apbd->uraian1 = 'Total APBD Kota/Kabupaten';
            $apbd->save();

            $pkp->uraian1 = 'Anggaran Dinas PKP Kota/Kabupaten';
            $pkp->save();
        }
    }

    public function prepareK3()
    {
        $k3 = new K3();
        $this->k3()->save($k3);
    }

    public function prepareK4d($type)
    {
        if ($type == '1a') {

            if (!is_null($this->provinsi)) {
                // Kumpulkan semua kabupaten di provinsi ini
                $kabs = KabupatenKota::provinsi($this->provinsi->id)->get();

                // Insert semua kab kot
                foreach ($kabs as $kab) {
                    $this->k4d()->create([
                        'idkabupaten_kota' => $kab->id
                    ]);
                }

            }
        } elseif ($type == '1b') {
            if (!is_null($this->dinas->kabupatenKota)) {
                // Kumpulkan semua kabupaten di provinsi ini
                $kecamatans = Kecamatan::kabupatenKota($this->dinas->kabupatenKota->id)->get();

                // Insert semua kab kot
                foreach ($kecamatans as $kecamatan) {
                    $this->k4d()->create([
                        'idkabupaten_kota' => $this->dinas->kabupatenKota->id,
                        'idkecamatan'      => $kecamatan->id
                    ]);
                }

            }
        }
    }

    public function prepareK4e()
    {
        $k4e = new K4e(['sumber_data_8' => '-']);
        $this->k4e()->save($k4e);
    }

    public function prepareK5($type)
    {
        if ($type == '1a') {

            if (!is_null($this->provinsi)) {
                // Kumpulkan semua kabupaten di provinsi ini
                $kabs = KabupatenKota::provinsi($this->provinsi->id)->get();

                // Insert semua kab kot
                foreach ($kabs as $kab) {
                    $this->k5()->create([
                        'idkabupaten_kota' => $kab->id
                    ]);
                }

            }
        } elseif ($type == '1b') {
            if (!is_null($this->dinas->kabupatenKota)) {
                // Kumpulkan semua kabupaten di provinsi ini
                $kecamatans = Kecamatan::kabupatenKota($this->dinas->kabupatenKota->id)->get();

                // Insert semua kab kot
                foreach ($kecamatans as $kecamatan) {
                    $this->k5()->create([
                        'idkabupaten_kota' => $this->dinas->kabupatenKota->id,
                        'idkecamatan'      => $kecamatan->id
                    ]);
                }

            }
        }
    }

    public function prepareK6($type)
    {
        if ($type == '1a') {

            if (!is_null($this->provinsi)) {
                // Kumpulkan semua kabupaten di provinsi ini
                $kabs = KabupatenKota::provinsi($this->provinsi->id)->get();

                // Insert semua kab kot
                foreach ($kabs as $kab) {
                    $this->k6()->create([
                        'idkabupaten_kota' => $kab->id
                    ]);
                }

            }

        } elseif ($type == '1b') {
            if (!is_null($this->dinas->kabupatenKota)) {
                // Kumpulkan semua kabupaten di provinsi ini
                $kecamatans = Kecamatan::kabupatenKota($this->dinas->kabupatenKota->id)->get();

                // Insert semua kab kot
                foreach ($kecamatans as $kecamatan) {
                    $this->k6()->create([
                        'idkabupaten_kota' => $this->dinas->kabupatenKota->id,
                        'idkecamatan'      => $kecamatan->id
                    ]);
                }

            }
        }
    }

    public function updateKelengkapan($type, $value = 1)
    {
        switch ($type) {
            case 'k1':
                $field = 'struktur_dinas';
                break;
            case 'k4_a':
                $field = 'k4_1';
                break;

            case 'k4_b':
                $field = 'k4_2';
                break;

            case 'k4_c':
                $field = 'k4_3';
                break;

            case 'k4_d':
                $field = 'k4_4';
                break;

            case 'k4_e':
                $field = 'k4_5';
                break;
            default:
                $field = $type;
        }

        $this->kelengkapan()->update([
            $field => $value
        ]);
    }

    public function isComplete()
    {
        if ($this->kelengkapan) {
            if ($this->kelengkapan->struktur_dinas == 1 and
                $this->kelengkapan->k2_a == 1 and
                $this->kelengkapan->k2_b == 1 and
                $this->kelengkapan->k3 == 1 and
                $this->kelengkapan->k4_1 == 1 and
                $this->kelengkapan->k4_2 == 1 and
                $this->kelengkapan->k4_3 == 1 and
                $this->kelengkapan->k4_4 == 1 and
                $this->kelengkapan->k4_5 == 1 and
                $this->kelengkapan->k5 == 1 and
                $this->kelengkapan->k6 == 1) {
                return true;
            }
        }

        return false;
    }
}