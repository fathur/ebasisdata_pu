<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.57
 */

namespace Model\Eloquent;


use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    protected $primaryKey =  'idmenu';

    public function children()
    {
        return $this->hasMany(Menu::class, 'idparent', 'idmenu');
    }

    public function scopeTop($query, $idPrivilege)
    {
        $query->select(['name','idmenu', 'kind', 'link', 'active', 'atribut'])
            ->orderBy('urut', 'asc')
            ->where('idparent', '=', 0)
            ->where('idprivilage', '=', $idPrivilege);
    }

//    public function scopeChildren($query)
//    {
//
//        $query->where('idparent', '=', $this->idmenu);
//    }
}