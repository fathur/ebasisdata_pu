<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 24/08/18
 * Time: 11.02
 */

namespace Model\Eloquent;


use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';

    public function privilege()
    {
        return $this->belongsTo(UserPrivilege::class, 'id_privilage', 'id');
    }
}