<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 24/08/18
 * Time: 11.01
 */

namespace Model\Eloquent;


use Illuminate\Database\Eloquent\Model;

class UserPrivilege extends Model
{
    protected $table = 'user_privilage';

    protected $primaryKey = 'id';

    public function users()
    {
        return $this->hasMany(User::class, 'id_privilage', 'id');
    }

    public function dinas()
    {
        return $this->hasMany(Dinas::class, 'id_privilage', 'id');
    }
}