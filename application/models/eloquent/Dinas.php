<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.49
 */


namespace Model\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Model\Eloquent\Region\KabupatenKota;
use Model\Eloquent\Region\Kecamatan;
use Model\Eloquent\Region\Kelurahan;
use Model\Eloquent\Region\Provinsi;

class Dinas extends Model
{
    protected $table = 'dinas';
    protected $primaryKey = 'iddinas';
    public $timestamps = false;

    protected $appends = ['id', 'is_logged_in'];
    protected $fillable = ['nama_dinas', 'alamat', 'email', 'telepon', 'fax', 'password'];

    public function getIdAttribute()
    {
        return (int)$this->attributes['iddinas'];
    }

    public function getIsLoggedInAttribute()
    {
        return filter_var($this->attributes['isLoggedIn'], FILTER_VALIDATE_BOOLEAN);
    }

    public function privilege()
    {
        return $this->belongsTo(UserPrivilege::class, 'id_privilage', 'id');
    }

    public function provinsi()
    {
        return $this->hasOne(Provinsi::class, 'iddinas', 'iddinas');
    }

    public function kabupatenKota()
    {
        return $this->belongsTo(KabupatenKota::class, 'iddinas', 'iddinas');
    }

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'idkecamatan', 'idkecamatan');
    }

    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'idkelurahan', 'idkelurahan');

    }
}