<?php

namespace Model\Eloquent;
use Illuminate\Database\Eloquent\Model;

class Backlog extends Model
{
    protected $table = 'backlog';
}