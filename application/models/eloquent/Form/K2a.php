<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.53
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K2a extends Model
{
    const PKP = 'Anggaran Dinas PKP';
    const APBD = 'Total APBD';

    public $timestamps = false;

    protected $table = 'form_1_k2_a';
    protected $primaryKey = 'idform_1_k2_a';
    protected $appends = ['id'];
    protected $fillable = ['k231'];

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k2_a'];
    }

    public function scopePkp($query)
    {
        $string =  self::PKP;
        return $query->whereRaw("uraian1 like '%{$string}%'");
    }


    public function scopeApbd($query)
    {
        $string = self::APBD;
        return $query->whereRaw("uraian1 like '%{$string}%'");

    }
}