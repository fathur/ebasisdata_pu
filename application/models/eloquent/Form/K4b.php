<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.54
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K4b extends Model
{
    const BUILDINGS = [
        'jenis1' => 'Rumah Tapak',
        'jenis2' => 'Rumah Susun/Apartemen',
//        'Rumah Panggung'
    ];

    protected $table = 'form_1_k4_2';
    protected $primaryKey = 'idform_1_k4_2';
    protected $appends = ['id'];
    protected $fillable = [
        'jenis1','jenis2',
        'k4242','k4241',
        'k4232','k4231',

    ];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k4_2'];
    }
}