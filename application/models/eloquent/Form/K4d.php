<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.54
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;
use Model\Eloquent\Region\KabupatenKota;

class K4d extends Model
{
    protected $table = 'form_1_k4_4';
    protected $primaryKey = 'idform_1_k4_4';
    protected $appends = ['id'];
    protected $fillable = [
        'idkabupaten_kota',
        'jumlah_rumah_3',
        'jumlah_rumah_4',
        'sumber_data_5',
        'idkelurahan',
        'idkecamatan'
    ];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k4_4'];
    }

    public function kabupatenKota()
    {
        return $this->belongsTo(KabupatenKota::class, 'idkabupaten_kota', 'idkabupaten_kota');
    }
}