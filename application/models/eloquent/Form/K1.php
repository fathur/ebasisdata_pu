<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.53
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K1 extends Model
{
    protected $table = 'form_1_k1';
    protected $primaryKey = 'idform_1_k1';
    protected $appends = ['id'];
    protected $fillable = ['struktur_dinas'];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k1'];
    }
}