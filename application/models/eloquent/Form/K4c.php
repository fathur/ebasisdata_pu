<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.54
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K4c extends Model
{
    const KK = [
        'fungsi1' => 'Rumah Dengan 1 (satu) KK',
        'fungsi2' => 'Rumah dengan lebih dari 1 (satu) KK'
    ];

    protected $table = 'form_1_k4_3';
    protected $primaryKey = 'idform_1_k4_3';
    protected $appends = ['id'];
    protected $fillable = ['fungsi1', 'fungsi2',
        'k4331', 'k4332',
        'k4341', 'k4342'
    ];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k4_3'];
    }
}