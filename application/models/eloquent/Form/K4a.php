<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.53
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K4a extends Model
{
    const OWNERSHIPS = [
        'status1' => 'Milik sendiri',
        'status2' => 'Kontrak / sewa',
        'status3' => 'Bebas sewa',
        'status4' => 'Dinas',
        'status5' => 'lainnya'
    ];

    protected $table = 'form_1_k4_1';
    protected $primaryKey = 'idform_1_k4_1';
    protected $appends = ['id'];
    protected $fillable = [
        'status1','status2','status3','status4','status5',
        'k4141','k4142','k4143','k4144','k4145',
        'k4151','k4152','k4153','k4154','k4155'
    ];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k4_1'];
    }
}