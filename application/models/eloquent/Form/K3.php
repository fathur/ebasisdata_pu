<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.53
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K3 extends Model
{
    const DOCUMENTS = [
        'isi_1a' => 'RTRW (sudah perda)',
        'isi_1b' => 'RDTR (sudah perda)',
        'isi_1c' => 'RP3KP/RP4D',
        'isi_1d' => 'RPIJM',
        'isi_1e' => 'Renstra Dinas PKP',
        'isi_1f' => 'Lainnya'
    ];

    const BUDGETS = [
        'isi_2a' => 'APBD',
        'isi_2b' => 'Loan (pinjaman) dari badan/bank luar negeri',
        'isi_2c' => 'CSR',
        'isi_2d' => 'Swasta',
        'isi_2e' => 'Lainnya'
    ];

    protected $table = 'form_1_k3';
    protected $primaryKey = 'idform_1_k3';
    protected $appends = ['id'];
    protected $fillable = [
        'isi_1a',
        'isi_1b',
        'isi_1c',
        'isi_1d',
        'isi_1e',
        'isi_1f',
        'isi_1f_keterangan',

        'isi_2a' ,
        'isi_2b' ,
        'isi_2c' ,
        'isi_2d' ,
        'isi_2e' ,
        'isi_2c_keterangan' ,
        'isi_2d_keterangan' ,
        'isi_2e_keterangan' ,
    ];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k3'];
    }
}