<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.54
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K5 extends Model
{
    protected $table = 'form_1_k5';
    protected $primaryKey = 'idform_1_k5';
    protected $appends = ['id'];
    protected $fillable = ['idkabupaten_kota','idkecamatan',
        'jumlah_kk_rt_3','jumlah_rtlh_versi_bdt_4',
        'jumlah_rtlh_verifikasi_pemda_5','sumber_data_6'
        ];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k5'];
    }
}