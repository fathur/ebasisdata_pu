<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.54
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K6 extends Model
{
    protected $table = 'form_1_k6';
    protected $primaryKey = 'idform_1_k6';
    protected $appends = ['id'];
    protected $fillable = ['idkabupaten_kota','idkecamatan','idkecamatan','luas_wilayah_kumuh_4',
        'jumlah_rtlh_dalam_wilayah_kumuh_5','sumber_data_6'];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k6'];
    }
}