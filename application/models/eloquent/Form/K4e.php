<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 13/07/18
 * Time: 15.54
 */

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K4e extends Model
{
    protected $table = 'form_1_k4_5';
    protected $primaryKey = 'idform_1_k4_5';
    protected $appends = ['id'];
    protected $fillable = [
        'non_mbr_2',
        'mbr_3',
        'non_mbr_4',
        'mbr_5',
        'non_mbr_6',
        'mbr_7',
        'sumber_data_8'
    ];

    public $timestamps = false;

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k4_5'];
    }
}