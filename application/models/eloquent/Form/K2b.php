<?php

namespace Model\Eloquent\Form;


use Illuminate\Database\Eloquent\Model;

class K2b extends Model
{
    public $timestamps = false;

    protected $table = 'form_1_k2_b';
    protected $primaryKey = 'idform_1_k2_b';
    protected $appends = ['id'];
    protected $fillable = ['jenis_kegiatan_urusan_pkp_2','ta_a_vol_unit_5','ta_a_biaya_6'];

    public function getIdAttribute()
    {
        return $this->attributes['idform_1_k2_b'];
    }
}