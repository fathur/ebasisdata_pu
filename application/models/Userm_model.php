<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Userm_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function provinsi()
    {
        $this->db->select('*');
        $this->db->from('provinsi')->order_by('kode_provinsi', 'ASC');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kabupaten_kota()
    {
        $this->db->select('*');
        $this->db->from('kabupaten_kota')->order_by('idkabupaten_kota', 'ASC');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function pengguna_dinas($iddinas)
    {
        $this->db->select('*');
        $this->db->where('iddinas', $iddinas);
        $this->db->from('dinas');
        $this->db->limit('1');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function update_user_dinas()
    {
        $data = array(
            'firstName' => $this->input->post('firstname'),
            'lastName' => $this->input->post('lastname'),
            'email_user' => $this->input->post('email'),
            'alamat_user' => $this->input->post('alamat'),
            'telepon_user' => $this->input->post('telepon'),
            'nomor_id' => $this->input->post('nomor_id'),
        );
        $this->db->where('iddinas', $this->input->post('iddinas'));
        $this->db->update('dinas', $data);
    }

    public function update_user_dinas_pass()
    {
        $data = array(
            'password' => $this->input->post('password')
        );
        $this->db->where('iddinas', $this->input->post('iddinas'));
        $this->db->update('dinas', $data);
    }
}
