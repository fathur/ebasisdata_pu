<?php

class User_m extends CI_Model
{

    protected $details;

    public function validate_user($user, $password, $tabulasi)
    {
        if ($tabulasi == "1") {
            $this->db->from('user_provinsi');
            $this->db->where('idprovinsi', $user);
        } elseif ($tabulasi == "2") {
            $this->db->from('user_kabupaten_kota');
            $this->db->where('idkabupaten_kota', $user);
        } elseif ($tabulasi == "3") {
            $this->db->from('user');
            $this->db->where('username', $user);
        }

        $this->db->where('password', sha1($password));
        $login = $this->db->get()->result();

        if (is_array($login) && count($login) == 1) {
            $this->details = $login[0];
            $this->set_session();

            return true;
        }

        return false;
    }


    public function update_dinas($data_l4, $iddinas)
    {
        $this->db->where('iddinas', $iddinas);
        $this->db->update('dinas', $data_l4);
    }

    /**
     *
     */
    public function set_session()
    {
        if ($this->details->idprivilage != 1) {
            $this->session->set_userdata(array(
                'iddinas' => $this->details->iddinas,
                'idprivilage' => $this->details->idprivilage,
                'nama_dinas' => $this->details->nama_dinas,
                'provinsi' => $this->details->provinsi,
                'idprovinsi' => $this->details->idprovinsi,
                'alamat' => $this->details->alamat,
                'kabupaten_kota' => $this->details->kabupaten_kota,
                'idkabupaten_kota' => $this->details->idkabupaten_kota,
                'x' => $this->details->x,
                'y' => $this->details->y,
                'z' => $this->details->z,
                'isLoggedIn' => true,
                'title' => "eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan"
            ));
        } else {
            $this->session->set_userdata(array(
                'iduser' => $this->details->id,
                'iddinas' => $this->details->iddinas,
                'idprivilage' => $this->details->idprivilage,
                'nama_dinas' => $this->details->nama_dinas,
                'provinsi' => $this->details->provinsi,
                'idprovinsi' => $this->details->idprovinsi,
                'alamat' => $this->details->alamat,
                'kabupaten_kota' => $this->details->kabupaten_kota,
                'idkabupaten_kota' => $this->details->idkabupaten_kota,
                'isLoggedIn' => true,
                'title' => "eBasisdata Perumahan | Direktorat Jenderal Penyediaan Perumahan"
            ));
        }
    }

    function create_new_user($userData)
    {
        $data['email'] = $userData['email'];
        $data['password'] = sha1($userData['password']);

        return $this->db->insert('user', $data);
    }

    private function getAvatar()
    {
        $avatar_names = array();

        foreach (glob('assets/img/avatars/*.png') as $avatar_filename) {
            $avatar_filename = str_replace("assets/img/avatars/", "", $avatar_filename);
            array_push($avatar_names, str_replace(".png", "", $avatar_filename));
        }

        return $avatar_names[array_rand($avatar_names)];
    }
}
