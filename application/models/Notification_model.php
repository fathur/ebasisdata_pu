<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends CI_Model
{
    public function notification_top()
    {
        $this->db->select('kind,status,waktu,icon,class,link');
        $this->db->from('task_notification');
        $query = $this->db->get();
        return $result = $query->result();
    }


    public function log_pekerjaan()
    {
        $this->db->select('*');
        $this->db->from('log_pekerjaan_view');
        $query = $this->db->get();
        return $result = $query->result();
    }

}

?>