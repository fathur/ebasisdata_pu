<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public function menu_top($idprivilage)
    {
        $this->db->select('name,idmenu,kind,link,active,atribut');
        $this->db->from('menu');
        $this->db->order_by('urut asc');
        $this->db->where('idparent=0 and idprivilage=' . $idprivilage);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function menu_top_active_update($idmenu)
    {
        $data = array(
            'active' => 1
        );
        $this->db->where('idmenu', $idmenu);
        $this->db->update('menu', $data);
        $data = array(
            'active' => 0
        );
        $this->db->where('idmenu !=', $idmenu);
        $this->db->update('menu', $data);
    }
}

?>