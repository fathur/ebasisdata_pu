<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dinas_model extends CI_Model
{
    public function dinas_view($iduser)
    {
        $this->db->select('*');
        $this->db->from('user_provinsi');
        $this->db->where('iddinas=' . $iduser);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function daftar_pengguna_provinsi()
    {
        $this->db->select('*');
        $this->db->from('dinas');
        $this->db->join('provinsi', 'provinsi.iddinas = dinas.iddinas');
        $this->db->where('nama_dinas!="Pusat"');
//        $this->db->order_by('isLoggedIn', 'ASC');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function daftar_pengguna_kabupaten()
    {
        $this->db->select('*');
        $this->db->from('dinas');
        $this->db->join('kabupaten_kota', 'kabupaten_kota.iddinas = dinas.iddinas');
        $this->db->where('nama_dinas!="Pusat"');
//        $this->db->order_by('isLoggedIn', 'ASC');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function dinas_view_admin_prov($iduser)
    {
        $this->db->select('*');
        $this->db->from('user_provinsi');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kab_kot_count($idprovinsi)
    {
        $query = $this->db->query('SELECT * FROM kabupaten_kota where idprovinsi=' . $idprovinsi);
        return $query->num_rows();
    }

    public function kec_kot_count($idprovinsi)
    {
        $query = $this->db->query('SELECT * FROM kecamatan k join kabupaten_kota kk on kk.idkabupaten_kota=k.idkabupaten_kota where kk.idprovinsi=' . $idprovinsi . ' group by k.idkecamatan');
        return $query->num_rows();
    }

    public function dinas_view2($iduser)
    {
        $this->db->select('*');
        $this->db->from('user_kabupaten_kota');
        $this->db->where('iddinas=' . $iduser);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function dinas_view2_admin()
    {
        $this->db->select('*');
        $this->db->from('user_kabupaten_kota');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function dinas_view_all($idprovinsi)
    {
        $this->db->select('*');
        $this->db->from('user_admin');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function dinas_view_update($iduser)
    {
        $data = array(
            'nama_dinas' => $this->input->post('nama_dinas'),
            'alamat' => $this->input->post('alamat'),
            'telepon' => $this->input->post('telepon'),
            'fax' => $this->input->post('fax'),
            'email' => $this->input->post('email'),
            'URL' => $this->input->post('URL')
        );
        $this->db->where('iddinas', $iduser);
        $this->db->update('dinas', $data);
    }
}

?>
