<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_model extends CI_Model
{
    public function kabupaten_kota_count($idprovinsi)
    {
        $this->db->select('(count(idkabupaten_kota)) as countkabkot');
        $this->db->from('kabupaten_kota');
        $this->db->where('idprovinsi=' . $idprovinsi);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kecamatan_count($idkabupaten_kota)
    {
        $this->db->select('count(idkecamatan) as countkecamatan');
        $this->db->from('kecamatan');
        $this->db->where('idkabupaten_kota=' . $idkabupaten_kota);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kelurahan_count($idkabupaten_kota)
    {
        $this->db->select('count(idkelurahan) as countkelurahan');
        $this->db->from('kelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan=kecamatan.idkecamatan');
        $this->db->where('idkabupaten_kota=' . $idkabupaten_kota);
        $this->db->where('idkabupaten_kota=kecamatan.idkabupaten_kota');
        $query = $this->db->get();
        return $result = $query->result();
    }
}

?>
