<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model
{
    public function provinsi()
    {
        $this->db->select('*');
        $this->db->from('provinsi');
        $this->db->order_by('kode_provinsi', 'ASC');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function pesan($iddinas, $judul)
    {
        $this->db->select('*');
        $this->db->from('pesan p');
        $this->db->join('pesan_judul pj', 'pj.idjudul = p.idjudul');
        $this->db->like('pj.judul', $judul, 'after');
        $this->db->order_by('pj.idjudul asc,p.waktu ASC');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function pesan_admin($judul)
    {
        $this->db->select('*');
        $this->db->from('pesan_judul pj');
        $this->db->join('user_provinsi up', 'pj.iddinas = up.iddinas');
        $this->db->like('pj.judul', $judul, 'after');
        $this->db->order_by('pj.tanggal DESC');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function pesan_detail_admin($judul)
    {
        $this->db->select('*');
        $this->db->from('pesan p');
        $this->db->where('p.idjudul', $judul);
        $this->db->order_by('p.idpesan desc');
        $query = $this->db->get();
        return $result = $query->result();
    }


    public function provinsi_matrix($tahun)
    {
        $this->db->select('s.tahun,p.idprovinsi,p.provinsi,s.form1a,s.form1b,(select count(*) from kabupaten_kota where idprovinsi=p.idprovinsi) as jumlah_kk');
        $this->db->from('statusform s');
        $this->db->join('provinsi p', 'p.idprovinsi = s.idprovinsi');
        $this->db->where('s.tahun', $tahun);
        $this->db->order_by('p.kode_provinsi', 'ASC');
        $this->db->group_by('p.kode_provinsi');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function provinsi_matrix_new()
    {
        $this->db->select('p.provinsi');
        $this->db->from('provinsi p');
        $this->db->order_by('p.kode_provinsi', 'ASC');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kabupaten_kota_matrix($tahun, $idprovinsi)
    {
        $this->db->select('(' . $tahun . ') as tahun,idkabupaten_kota,kabupaten_kota,(SELECT form1b FROM statusform_kab where idkabupaten_kota =kk.idkabupaten_kota and tahun = ' . $tahun . ') as form1b');
        $this->db->from('kabupaten_kota kk');
        $this->db->where('kk.idprovinsi', $idprovinsi);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kabupaten_kota()
    {
        $sql = 'select * from kabupaten_kota kk join provinsi p on p.idprovinsi=kk.idprovinsi';
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function backlog_kepemilikan_dan_penghunian_kab_view($tahun, $idprovinsi)
    {
        $sql = 'select * from backlog_kepemilikan_dan_penghunian_kab_view where idprovinsi=' . $idprovinsi . ' and tahun=' . $tahun;
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function backlog_kepemilikan_dan_penghunian_kab_view2($tahun)
    {
        $sql = 'select * from backlog_kepemilikan_dan_penghunian_kab_view where tahun=' . $tahun;
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function backlog_kepemilikan_dan_penghunian_view($tahun)
    {
        $sql = 'select * from backlog_kepemilikan_dan_penghunian_view where tahun=' . $tahun;
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function backlog_kepemilikan_dan_penghunian_view_2($tahun, $id)
    {
        $sql = 'select * from backlog_kepemilikan_dan_penghunian_view where idprovinsi=' . $id . ' and tahun=' . $tahun;
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function kabupaten_kota_dinas($idprovinsi)
    {
        $sql = 'select * from kabupaten_kota kk join provinsi p on p.idprovinsi=kk.idprovinsi where p.idprovinsi=' . $idprovinsi;
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function kecamatan_view($idkabupaten_kota)
    {
        $sql = 'select k.kode as kode,k.kecamatan,k.idkecamatan as idkecamatan,kk.idkabupaten_kota as idkabupaten_kota from kecamatan k join kabupaten_kota kk on kk.idkabupaten_kota=k.idkabupaten_kota where kk.idkabupaten_kota=' . $idkabupaten_kota;
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function kecamatan_view_all($idkabupaten_kota)
    {
        $sql = 'select k.kode as kode,k.kecamatan,k.idkecamatan as idkecamatan,kk.idkabupaten_kota as idkabupaten_kota from kecamatan k join kabupaten_kota kk on kk.idkabupaten_kota=k.idkabupaten_kota';
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    public function dinas()
    {
        $this->db->select('*');
        $this->db->from('dinas_view');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function user_prov_update($idprovinsi, $iduser)
    {
        $data = array(
            'idprovinsi' => $idprovinsi
        );
        $this->db->where('id', $iduser);
        $this->db->update('user', $data);
    }

    public function provinsi_admin($iduser)
    {
        $this->db->select('*');
        $this->db->where('id', $iduser);
        $this->db->from('user');
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function kecamatan_insert($data)
    {
        $this->db->insert('kecamatan', $data);
    }

    public function ganti_password($data, $iddinas)
    {
        $this->db->where('iddinas', ($iddinas));
        $this->db->update('dinas', $data);
    }

    public function kirim_pesan1($data)
    {
        $this->db->insert('pesan_judul', $data);
    }

    public function hapus_kabupaten_kota($id)
    {
        $this->db->where('idkabupaten_kota', $id);
        $this->db->delete('kabupaten_kota');
    }

    public function kirim_pesan2($data2)
    {
        $this->db->insert('pesan', $data2);
    }

    public function kirim_pesan_admin($data2)
    {
        $this->db->insert('pesan_admin', $data2);
    }

    public function kabupaten_kota_insert($data)
    {
        $this->db->insert('kabupaten_kota', $data);
    }

    public function kabupaten_kota_update($data, $id)
    {
        $this->db->where('idkabupaten_kota', $id);
        $this->db->update('kabupaten_kota', $data);
    }

    public function dinas_insert($data)
    {
        $this->db->insert('dinas', $data);
    }

    public function kecamatan_update($data, $id)
    {
        $this->db->where('idkecamatan', $id);
        $this->db->update('kecamatan', $data);
    }

    public function provinsi_update($data, $id)
    {
        $this->db->where('idprovinsi', $id);
        $this->db->update('provinsi', $data);
    }

    public function hapus_pesan_admin($id)
    {
        $this->db->where('idjudul', $id);
        $this->db->delete('pesan_judul');
        $this->db->where('idjudul', $id);
        $this->db->delete('pesan_admin');
    }
}

?>
