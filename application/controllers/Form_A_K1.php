<?php

use Model\Eloquent\Menu;

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Form_A_K1 extends \Repositories\AbstractController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
    }

    public function upload()
    {
        $idForm = $this->input->post('idform_1');
        $form = \Model\Eloquent\Form::find($idForm);

        if (!is_dir('./assets/uploads/' . $this->user->id)) {
            mkdir('./assets/uploads/' . $this->user->id, 0777, true);
        }

        $this->load->library('upload', [
            'encrypt_name' => true,
            'overwrite' => true,
            'upload_path' => "./assets/uploads/{$this->user->id}",
            'allowed_types' => 'gif|jpg|jpeg|png|bmp',
        ]);


        header('Content-Type: application/json');

        if ($this->upload->do_upload('image')) {

            $uploadedData = $this->upload->data();

            $newFile = "assets/uploads/{$this->user->id}/{$uploadedData['file_name']}";

//            $this->form_1_model->hapus_gambar($idForm);
            if ($form->k1) {
                $form->k1()->delete();
            }

//            $this->form_1_model->Form1k1_insert([
//                'idform_1' => $idForm,
//                'struktur_dinas' => $newFile
//            ]);
            $r = $form->k1()->create([
                'struktur_dinas' => $newFile
            ]);

            if ($r) {
                $form->updateKelengkapan('k1');
            }

            echo json_encode([
                'data' => $this->upload->data(),
                'image' => base_url($newFile)
            ]);
        } else {
            echo json_encode([
                'message' => $this->upload->display_errors()
            ]);
        }
    }
}