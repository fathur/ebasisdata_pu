<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class upload_file extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'html', 'url'));

        if (!$this->session->userdata('isLoggedIn')) {
            redirect(base_url() . 'login/show_login');
        }
    }

    public function index()
    {
        $userfile = "userfile";
        $config['upload_path'] = ('./assets/upload/');
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 100000000;
        $config['max_width'] = 10240;
        $config['max_height'] = 7680;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($userfile)) {
            $error = array('error' => $this->upload->display_errors());

            echo $config['upload_path'] . 'error ' . $this->upload->display_errors();
        } else {
            $data = array('upload_data' => $this->upload->data());

            echo $config['upload_path'];
        }
    }

    public function upload_gambar()
    {
        $idform_1 = $this->input->post('idform_1');
        $idprovinsi = $this->session->userdata('idprovinsi');
        ///
        $config['upload_path'] = './assets/upload/' . $idprovinsi . '/' . $idform_1;
        $config['allowed_types'] = 'gif|jpg|png|bmp';
        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        if (!is_dir('./assets/upload')) {
            mkdir('./assets/upload', 0777, true);
        }
        if (!is_dir('./assets/upload/' . $idprovinsi)) {
            mkdir('./assets/upload/' . $idprovinsi, 0777, true);
        }
        if (!is_dir('./assets/upload/' . $idprovinsi . '/' . $idform_1)) {
            mkdir('./assets/upload/' . $idprovinsi . '/' . $idform_1, 0777, true);
        }
        $arr = explode('.', $_FILES["userfile"]['name']);
        unset($arr[0]);
        $filebaru = "../assets/upload/" . $idprovinsi . '/' . $idform_1 . "/" . $idform_1 . '.' . implode($arr);
        if (file_exists("./assets/upload/" . $idprovinsi . '/' . $idform_1 . "/" . $idform_1 . '.' . implode($arr)) == false) {
            if (!$this->upload->do_upload($_FILES["userfile"])) {
                echo $this->upload->display_errors();
            }//
            else {
                $data = array('idform_1' => $idform_1, 'struktur_dinas' => $filebaru);
                $this->form_1_model->Form1k1_insert($data);
                header('Location: ' . BASE_URL('/main/form_1a'));
                exit;
            }
        }
        ////
    }


} 
