<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Form_A_K3 extends \Repositories\AbstractController
{
    public function view($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K3::find($idK);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        header('Content-Type: application/json');

        $view = $this->blade->view()
            ->make('form.k3.view', [
                'form' => $form,
                'k' => $k,
            ])
            ->render();

        echo json_encode([
            'body' => $view
        ]);
    }

    public function edit($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K3::find($idK);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        $view = $this->blade->view()
            ->make('form.k3.edit', [
                'form' => $form,
                'k' => $k,
                'mode' => '1a'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.3 PEMBANGUNAN PERUMAHAN',
            'body' => $view
        ]);
    }

    public function update($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K3::find($idK);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        foreach (range('a', 'f') as $char) {
            $isi1 = "isi_1{$char}";
            if ($this->input->post($isi1))
                $k->{$isi1} = 'Y';
            else
                $k->{$isi1} = 'T';
        }

        if ($this->input->post('isi_1f_keterangan')) {
            $k->isi_1f_keterangan = $this->input->post('isi_1f_keterangan');
        }

        foreach (range('a', 'e') as $char) {
            $isi2 = "isi_2{$char}";
            if ($this->input->post($isi2))
                $k->{$isi2} = 'Y';
            else
                $k->{$isi2} = 'T';
        }

        foreach (range('c', 'e') as $char) {
            $isi2ket = "isi_2{$char}_keterangan";
            if ($this->input->post($isi2ket))
                $k->{$isi2ket} = $this->input->post($isi2ket);
        }


        if ($k->save()) {
            $form->updateKelengkapan('k3');
        }

        header('Content-Type: application/json');

        echo json_encode([
            'id' => $form->id,
            'k3id' => $k->id
        ]);
    }

    public function create($idForm)
    {
        $form = \Model\Eloquent\Form::find($idForm);
        $k3 = $form->k3()->create();

        header('Content-Type: application/json');

        $view = $this->blade->view()
            ->make('form.k3.view', [
                'form' => $form,
                'k' => $k3,
                'mode' => '1a'
            ])
            ->render();

        echo json_encode([
            'body' => $view,
            'k3' => $k3
        ]);
    }
}