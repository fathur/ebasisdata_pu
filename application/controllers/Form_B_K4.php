<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Form_B_K4 extends \Repositories\AbstractController
{
    /**
     * @param int $idForm
     */
    public function data_a($idForm)
    {
        header('Content-Type: application/json');
        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");
        $search = $this->input->post("search");

        $orderColNum = (int)$order[0]['column'];
        $orderColDirection = $order[0]['dir'];
        $orderColumn = $columns[$orderColNum]['name'];

        $form = \Model\Eloquent\Form::find($idForm);

        $count = 5;

        $data = [];
        if ($form->k4a) {
            for ($i = 1; $i <= $count; $i++) {

                $statusI = "status{$i}";
                $jumlahI = "k414{$i}";
                $sumberI = "k415{$i}";

                $key = array_search($form->k4a->{$statusI}, \Model\Eloquent\Form\K4a::OWNERSHIPS);

                $fungsi = '<a class=\'btn btn-sm blue-hoki\' onclick="kModalEdit(\'' . base_url("form-1b/{$idForm}/k4a/{$key}/edit") . '\')">Ubah</a>';


                array_push($data, [
                    'no' => "{$i}.",
                    "status" => $form->k4a->{$statusI},
                    'jumlah' => number_format($form->k4a->{$jumlahI}, 0, ',', '.'),
                    'sumber' => $form->k4a->{$sumberI},
                    'fungsi' => $fungsi
                ]);
            }
        }

        $footer = [
            'total_a' => ($form->k4a) ? number_format($form->k4a->k4142 + $form->k4a->k4143 + $form->k4a->k4144 + $form->k4a->k4145, 0, ',', '.') : 0,
            'total_b' => ($form->k4a) ? number_format($form->k4a->k4141 + $form->k4a->k4142 + $form->k4a->k4144, 0, ',', '.') : 0,
            'total' => ($form->k4a) ? number_format($form->k4a->k4141 + $form->k4a->k4142 + $form->k4a->k4143 + $form->k4a->k4144 + $form->k4a->k4145, 0, ',', '.') : 0
        ];

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $data,
            "footer" => $footer
        );

        echo json_encode($output);
    }

    public function data_b($idForm)
    {
        header('Content-Type: application/json');
        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");
        $search = $this->input->post("search");

        $orderColNum = (int)$order[0]['column'];
        $orderColDirection = $order[0]['dir'];
        $orderColumn = $columns[$orderColNum]['name'];

        $form = \Model\Eloquent\Form::find($idForm);

        $count = 2;


        $data = [];
        if ($form->k4b) {
            for ($i = 1; $i <= $count; $i++) {

                $rumahI = "jenis{$i}";
                $jumlahI = "k423{$i}";
                $sumberI = "k424{$i}";

                $key = array_search($form->k4b->{$rumahI}, \Model\Eloquent\Form\K4b::BUILDINGS);

                $fungsi = '<a class=\'btn btn-sm blue-hoki\' onclick="kModalEdit(\'' . base_url("form-1b/{$idForm}/k4b/{$key}/edit") . '\')">Ubah</a>';


                array_push($data, [
                    'no' => "{$i}.",
                    "rumah" => $form->k4b->{$rumahI},
                    'jumlah' => number_format($form->k4b->{$jumlahI}, 0, ',', '.'),
                    'sumber' => $form->k4b->{$sumberI},
                    'fungsi' => $fungsi

                ]);
            }
        }


        $footer = [
            'total' => ($form->k4b) ? number_format($form->k4b->k4231 + $form->k4b->k4232, 0, ',', '.') : 0
        ];

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $data,
            'footer' => $footer
        );

        echo json_encode($output);
    }

    public function data_c($idForm)
    {
        header('Content-Type: application/json');
        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");
        $search = $this->input->post("search");

        $orderColNum = (int)$order[0]['column'];
        $orderColDirection = $order[0]['dir'];
        $orderColumn = $columns[$orderColNum]['name'];

        $form = \Model\Eloquent\Form::find($idForm);

        $count = 2;

        $data = [];
        if ($form->k4c) {
            for ($i = 1; $i <= $count; $i++) {

                $fungsiI = "fungsi{$i}";
                $jumlahI = "k433{$i}";
                $sumberI = "k434{$i}";

                $key = array_search($form->k4c->{$fungsiI}, \Model\Eloquent\Form\K4c::KK);

                $fungsi = '<a class=\'btn btn-sm blue-hoki\' onclick="kModalEdit(\'' . base_url("form-1b/{$idForm}/k4c/{$key}/edit") . '\')">Ubah</a>';


                array_push($data, [
                    'no' => "{$i}.",
                    "fungsi" => $form->k4c->{$fungsiI},
                    'jumlah' => number_format($form->k4c->{$jumlahI}, 0, ',', '.'),
                    'sumber' => $form->k4c->{$sumberI},
                    'action' => $fungsi
                ]);
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function data_d($idForm)
    {
        header('Content-Type: application/json');
        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");
        $search = $this->input->post("search");

        $orderColNum = (int)$order[0]['column'];
        $orderColDirection = $order[0]['dir'];
        $orderColumn = $columns[$orderColNum]['name'];

        $form = \Model\Eloquent\Form::find($idForm);
        $builder = $form->k4d()->select('*');
//            ->orderBy($orderColumn, $orderColDirection);

        $count = $builder->count();

        $results = $builder->get();


        $data = [];
        $i = 1;
        foreach ($results as $item) {

            $fungsi = '<a class=\'btn btn-sm blue-hoki\' onclick="kModalEdit(\'' . base_url("form-1b/{$form->id}/k4d/{$item->id}/edit") . '\')">Ubah</a>';

            array_push($data, [
                'id' => "{$i}.",
                'region' => optional(\Model\Eloquent\Region\Kecamatan::find($item->idkecamatan))->name,
                'jumlah_rumah_4' => $item->jumlah_rumah_4 ? number_format($item->jumlah_rumah_4, 0, ',', '.') : 0,
                'sumber_data_5' => $item->sumber_data_5,
                'fungsi' => $fungsi
            ]);

            $i++;
        }

        $footer = [
            'total' => ($form->k4d) ? number_format($form->k4d->sum('jumlah_rumah_4'), 0, ',', '.') : 0
        ];

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $data,
            'footer' => $footer
        );

        echo json_encode($output);
    }

    public function data_e($idForm)
    {
        header('Content-Type: application/json');
        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");
        $search = $this->input->post("search");

        $orderColNum = (int)$order[0]['column'];
        $orderColDirection = $order[0]['dir'];
        $orderColumn = $columns[$orderColNum]['name'];

        $form = \Model\Eloquent\Form::find($idForm);


        $count = 1;

        $data = [];
        if ($form->k4e) {
            for ($i = 1; $i <= $count; $i++) {

                $fungsi = '<a class=\'btn btn-sm blue-hoki\' onclick="kModalEdit(\'' . base_url("form-1b/{$form->id}/k4e/{$form->k4e->id}/edit") . '\')">Ubah</a>';

                array_push($data, [
                    "non_mbr_2" => $form->k4e->non_mbr_2 ? (int)$form->k4e->non_mbr_2 : 0,
                    'mbr_3' => $form->k4e->mbr_3 ? (int)$form->k4e->mbr_3 : 0,
                    'non_mbr_4' => $form->k4e->non_mbr_4 ? (int)$form->k4e->non_mbr_4 : 0,
                    'mbr_5' => $form->k4e->mbr_5 ? (int)$form->k4e->mbr_5 : 0,
                    'sumber_data_8' => $form->k4e->sumber_data_8,
                    'fungsi' => $fungsi
                ]);
            }
        }


        $output = array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $data
        );

        echo json_encode($output);
    }

    public function edit_a($idForm, $statusText)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }


        $form = \Model\Eloquent\Form::find($idForm);

        $view = $this->blade->view()
            ->make('form.k4.a.edit', [
                'form' => $form,
                'k' => $form->k4a,
                'statusText' => $statusText,
                'i' => (int)substr($statusText, -1),
                'mode' => '1b'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.4 BACKLOG PERUMAHAN',
            'body' => $view
        ]);
    }

    public function update_a($idForm, $indexK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);

        $statusI = "status{$indexK}";
        $jumlahI = "k414{$indexK}";
        $sumberI = "k415{$indexK}";

        $k = $form->k4a;

        $k->{$jumlahI} = $this->input->post('jumlah') ? $this->input->post('jumlah') : 0;
        $k->{$sumberI} = $this->input->post('sumber') ? $this->input->post('sumber') : 0;

        if ($k->save()) {
            $form->updateKelengkapan('k4_a');
        }

        echo 1;
    }

    public function edit_b($idForm, $statusText)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }


        $form = \Model\Eloquent\Form::find($idForm);

        $view = $this->blade->view()
            ->make('form.k4.b.edit', [
                'form' => $form,
                'k' => $form->k4b,
                'statusText' => $statusText,
                'i' => (int)substr($statusText, -1),

                'mode' => '1b'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.4 BACKLOG PERUMAHAN',
            'body' => $view
        ]);
    }

    public function update_b($idForm, $indexK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);

        $rumahI = "jenis{$indexK}";
        $jumlahI = "k423{$indexK}";
        $sumberI = "k424{$indexK}";

        $k = $form->k4b;

        $k->{$jumlahI} = $this->input->post('jumlah') ? $this->input->post('jumlah') : 0;
        $k->{$sumberI} = $this->input->post('sumber') ? $this->input->post('sumber') : 0;

        if ($k->save())
            $form->updateKelengkapan('k4_b');

        echo 1;
    }

    public function edit_c($idForm, $statusText)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }


        $form = \Model\Eloquent\Form::find($idForm);

        $view = $this->blade->view()
            ->make('form.k4.c.edit', [
                'form' => $form,
                'k' => $form->k4c,
                'statusText' => $statusText,
                'i' => (int)substr($statusText, -1),
                'mode' => '1b'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.4 BACKLOG PERUMAHAN',
            'body' => $view
        ]);
    }

    public function update_c($idForm, $indexK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);

        $fungsiI = "fungsi{$indexK}";
        $jumlahI = "k433{$indexK}";
        $sumberI = "k434{$indexK}";

        $k = $form->k4c;

        $k->{$jumlahI} = $this->input->post('jumlah') ? $this->input->post('jumlah') : 0;
        $k->{$sumberI} = $this->input->post('sumber') ? $this->input->post('sumber') : 0;

        if ($k->save())
            $form->updateKelengkapan('k4_c');

        echo 1;
    }

    public function edit_e($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);

        $view = $this->blade->view()
            ->make('form.k4.e.edit', [
                'form' => $form,
                'k' => $form->k4e,
                'mode' => '1b'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.4 BACKLOG PERUMAHAN',
            'body' => $view
        ]);
    }

    public function update_e($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = $form->k4e;

        $k->non_mbr_2 = $this->input->post('non_mbr_2') ? $this->input->post('non_mbr_2') : 0;
        $k->mbr_3 = $this->input->post('mbr_3') ? $this->input->post('mbr_3') : 0;
        $k->non_mbr_4 = $this->input->post('non_mbr_4') ? $this->input->post('non_mbr_4') : 0;
        $k->mbr_5 = $this->input->post('mbr_5') ? $this->input->post('mbr_5') : 0;
        $k->sumber_data_8 = $this->input->post('sumber_data_8');

        if ($k->save())
            $form->updateKelengkapan('k4_e');

        echo 1;
    }

    public function edit_d($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K4d::find($idK);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        $view = $this->blade->view()
            ->make('form.k4.d.edit', [
                'form' => $form,
                'k' => $k,
                'mode' => '1b'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.4 BACKLOG PERUMAHAN',
            'body' => $view
        ]);
    }

    public function update_d($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K4d::find($idK);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        $k->jumlah_rumah_4 = $this->input->post('jumlah_rumah_4') ? $this->input->post('jumlah_rumah_4') : 0;
        $k->sumber_data_5 = $this->input->post('sumber_data_5');

        if ($k->save())
            $form->updateKelengkapan('k4_d');

        echo 1;
    }

    public function create_d($idForm)
    {
        $form = \Model\Eloquent\Form::find($idForm);

        if (!is_null($form->dinas->kabupatenKota)) {
            // Kumpulkan semua kecamatan di provinsi ini
            $kecamatans = \Model\Eloquent\Region\Kecamatan::kabupatenKota($form->dinas->kabupatenKota->id)->get();

            // Insert semua kab kot
            foreach ($kecamatans as $kecamatan) {
                $form->k4d()->create([
                    'idkabupaten_kota' => $form->dinas->kabupatenKota->id,
                    'idkecamatan'   => $kecamatan->id

                ]);
            }

        }
    }

    public function create_e($idForm)
    {
        $form = \Model\Eloquent\Form::find($idForm);

        $form->k4e()->create();

        echo 1;
    }
}