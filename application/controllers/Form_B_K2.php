<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Form_B_K2 extends \Repositories\AbstractController
{

    public function data_a($idForm)
    {
        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");
        $search = $this->input->post("search");

        $orderColNum = (int)$order[0]['column'];
        $orderColDirection = $order[0]['dir'];
        $orderColumn = $columns[$orderColNum]['name'];

//        $idform1 = $this->input->post('idform1');
        $form = \Model\Eloquent\Form::find($idForm);

        $builder = $form->k2a()->select('*')
            ->orderBy($orderColumn, $orderColDirection);
//            ->offset($start)->take($length);

        $count = $builder->count();

        $form = $builder->get();

        $data = [];
        foreach ($form as $item) {

            $fungsi = '<a class=\'btn btn-sm blue-hoki\' onclick="kModalEdit(\'' . base_url("form-1b/{$idForm}/k2a/{$item->id}/edit") . '\')">Ubah</a>';

            if ((int)$this->user->id_privilage == 3) {

                if (strtolower(trim($item->uraian1)) == 'total apbd provinsi') {
                    $uraian = 'Total APBD Kota/Kabupaten';
                } elseif (strtolower(trim($item->uraian1)) == 'anggaran dinas pkp provinsi') {
                    $uraian = 'Anggaran Dinas PKP Kota/Kabupaten';
                } else {
                    $uraian = $item->uraian1;
                }

            } else {
                $uraian = $item->uraian1;
            }

            array_push($data, [
                'uraian1'    => $uraian,
                'k231'       => number_format($item->k231, 2, ',', '.'),
                'k231_float' => (float)$item->k231,
                'fungsi'     => $fungsi
            ]);
        }

        $output = array(
            "draw"            => $draw,
            "recordsTotal"    => $count,
            "recordsFiltered" => $count,
            "data"            => $data
        );

        echo json_encode($output);
    }

    public function data_b($idForm)
    {
        header('Content-Type: application/json');

        // todo: validation redirection
        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");
        $search = $this->input->post("search");

        $orderColNum = (int)$order[0]['column'];
        $orderColDirection = $order[0]['dir'];
        $orderColumn = $columns[$orderColNum]['name'];

        $form = \Model\Eloquent\Form::find($idForm);

        $builder = $form->k2b()->select('*')
            ->orderBy($orderColumn, $orderColDirection);

        $count = $builder->count();

        $results = $builder->get();

        $data = [];
        foreach ($results as $item) {

            $fungsi = '<a class=\'btn btn-sm blue-hoki\' onclick="kModalEdit(\'' . base_url("form-1b/{$idForm}/k2b/{$item->id}/edit") . '\')">Ubah</a>' .
                '<a class="btn btn-sm btn-danger" onclick="deleteDtRow(this)" data-action="' . base_url("form-1b/{$idForm}/k2b/{$item->id}/delete") . '" data-table="table-k2b">Hapus</a>';


            array_push($data, [
                'jenis_kegiatan_urusan_pkp_2' => $item->jenis_kegiatan_urusan_pkp_2,
                'ta_a_vol_unit_5'             => number_format($item->ta_a_vol_unit_5, 0, ',', '.'),
                'ta_a_biaya_6'                => number_format((float)$item->ta_a_biaya_6, 2, ',', '.'),
                'fungsi'                      => $fungsi
            ]);
        }

        $footer = [
            'total_volume' => number_format($form->k2b->sum('ta_a_vol_unit_5'), 0, ',', '.'),
            'total_biaya'  => number_format($form->k2b->sum('ta_a_biaya_6'), 2, ',', '.')
        ];

        $output = array(
            "draw"            => $draw,
            "recordsTotal"    => $count,
            "recordsFiltered" => $count,
            "data"            => $data,
            "footer"          => $footer
        );

        echo json_encode($output);
    }

    public function edit_a($idForm, $idK2a)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K2a::find($idK2a);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        $view = $this->blade->view()
            ->make('form.k2.a.edit', [
                'form' => $form,
                'k'    => $k,
                'mode' => '1b'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.2. ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN PERMUKIMAN (PKP)',
            'body'  => $view
        ]);
    }

    public function update_a($idForm, $idK2a)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k2a = \Model\Eloquent\Form\K2a::find($idK2a);

        if ($form->id != $k2a->idform_1) {
            show_404();
        }

        $k2a->k231 = $this->input->post('k231');
        if ($k2a->save()) {
            $form->updateKelengkapan('k2_a');
        }


        echo 1;
    }

    public function create_b($idForm)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);

        $view = $this->blade->view()
            ->make('form.k2.b.create', [
                'form' => $form,
                'mode' => '1b'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.2. ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN PERMUKIMAN (PKP)',
            'body'  => $view
        ]);
    }

    public function store_b($idForm)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);

        $r = $form->k2b()->create([
            'jenis_kegiatan_urusan_pkp_2' => $this->input->post('jenis_kegiatan_urusan_pkp_2'),
            'ta_a_vol_unit_5'             => $this->input->post('ta_a_vol_unit_5'),
            'ta_a_biaya_6'                => $this->input->post('ta_a_biaya_6')
        ]);

        if ($r) {
            $form->updateKelengkapan('k2_b');
        }

        return 1;
    }

    public function edit_b($idForm, $idK2b)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K2b::find($idK2b);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        $view = $this->blade->view()
            ->make('form.k2.b.edit', [
                'form' => $form,
                'k'    => $k,
                'mode' => '1b'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.2. ALOKASI APBD UNTUK URUSAN PERUMAHAN DAN KAWASAN PERMUKIMAN (PKP)',
            'body'  => $view
        ]);

    }

    public function update_b($idForm, $idK2b)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K2b::find($idK2b);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        $k->jenis_kegiatan_urusan_pkp_2 = $this->input->post('jenis_kegiatan_urusan_pkp_2');
        $k->ta_a_vol_unit_5 = $this->input->post('ta_a_vol_unit_5');
        $k->ta_a_biaya_6 = $this->input->post('ta_a_biaya_6');
        if ($k->save()) {
            $form->updateKelengkapan('k2_b');
        }

        echo 1;
    }

    public function delete_b($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K2b::find($idK);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        $k->delete();

        if ($form->k2b()->count() == 0) {
            $form->updateKelengkapan('k2_b', 0);
        }

        echo 1;

    }
}