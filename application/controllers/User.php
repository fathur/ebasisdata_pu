<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class User extends \Repositories\AbstractController
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('isLoggedIn')) {
            redirect(base_url() . 'login/show_login');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }
    }

    public function province()
    {
        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'Users', 'url' => base_url('users')],
            ['text' => 'Provinsi', 'url' => base_url('users/provinsi')],
        ];

        $provinces = \Model\Eloquent\Region\Provinsi::with(['dinas'])
            ->orderBy('provinsi', 'asc')
            ->get();

        $data = [
            'title'       => 'Provinsi',
            'menu'        => $this->menu,
            'webHeader'   => $this->webHeader,
            'breadcrumbs' => $breadcrumbs,
            'user'        => $this->user,
            'provinces'   => $provinces
        ];

        echo $this->blade->view()
            ->make('user.province', $data)
            ->render();
    }

    public function province_create()
    {
        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'User', 'url' => base_url('users')],
            ['text' => 'Provinsi', 'url' => base_url('users/provinsi')],
            ['text' => 'Baru', 'url' => base_url('users/provinsi/create')],
        ];

        $data = [
            'title'       => 'Provinsi',
            'menu'        => $this->menu,
            'webHeader'   => $this->webHeader,
            'breadcrumbs' => $breadcrumbs,
            'user'        => $this->user,
        ];

        echo $this->blade->view()
            ->make('user.create-prov', $data)
            ->render();
    }

    public function province_store()
    {
        $dinas = \Model\Eloquent\Dinas::create([
            'id_privilage' => 2,
            'nama_dinas'   => $this->input->post('nama_dinas'),
            'password'     => sha1($this->input->post('password2')),
            'alamat'       => $this->input->post('alamat'),
            'email'        => $this->input->post('email'),
            'telepon'      => $this->input->post('telepon'),
            'fax'          => $this->input->post('fax'),
        ]);

        $prov = new \Model\Eloquent\Region\Provinsi();
        $prov->provinsi = $this->input->post('name');
        $prov->logo_prov = ' ';
        $prov->dinas()->associate($dinas);
        $prov->save();

        redirect(base_url('/users/provinsi'));
    }

    public function province_edit($id)
    {
        $provinsi = \Model\Eloquent\Region\Provinsi::with('dinas')->find($id);
        $withPassword = trim($this->input->get('password'));
        $withPassword = ($withPassword === 'true');

        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'User', 'url' => base_url('users')],
            ['text' => 'Provinsi', 'url' => base_url('users/provinsi')],
            ['text' => $provinsi->provinsi, 'url' => base_url('users/provinsi/' . $provinsi->id . '/edit')],
        ];

        $data = [
            'title'        => 'Provinsi',
            'menu'         => $this->menu,
            'webHeader'    => $this->webHeader,
            'breadcrumbs'  => $breadcrumbs,
            'user'         => $this->user,
            'provinsi'     => $provinsi,
            'withPassword' => $withPassword
        ];

        echo $this->blade->view()
            ->make('user.edit-prov', $data)
            ->render();
    }

    public function province_update($id)
    {
        $prov = \Model\Eloquent\Region\Provinsi::find($id);
//        $prov->provinsi = $this->input->post('name');
        $prov->logo_prov = ' ';

        $dinas = $prov->dinas;
        $refresh = false;
        if (is_null($dinas)) {
            $refresh = true;
            $dinas = new \Model\Eloquent\Dinas();
            $dinas->password = sha1($this->input->post('password2'));
        }


        $dinas->nama_dinas = $this->input->post('nama_dinas');
        $dinas->alamat = $this->input->post('alamat');
        $dinas->email = $this->input->post('email');
        $dinas->telepon = $this->input->post('telepon');
        $dinas->fax = $this->input->post('fax');
        $dinas->save();

        if ($refresh) {
            $prov->dinas()->associate($dinas);
        }

        $prov->save();


        redirect(base_url('/users/provinsi'));

    }

    public function district($provinsiId)
    {
        $provinsi = \Model\Eloquent\Region\Provinsi::find($provinsiId);

        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'Users', 'url' => base_url('users')],
            ['text' => 'Provinsi', 'url' => base_url('users/provinsi')],
            ['text' => 'Kabupaten di ' . $provinsi->provinsi, 'url' => base_url('users/' . $provinsiId . '/kabupaten')],
        ];

        $districts = \Model\Eloquent\Region\KabupatenKota::with(['dinas'])
            ->where('idprovinsi', $provinsi->id)
            ->get();

        $data = [
            'title'       => 'Provinsi',
            'menu'        => $this->menu,
            'webHeader'   => $this->webHeader,
            'breadcrumbs' => $breadcrumbs,
            'user'        => $this->user,
            'districts'   => $districts,
            'province'    => $provinsi
        ];

        echo $this->blade->view()
            ->make('user.district', $data)
            ->render();
    }

    public function district_create($provinsiId)
    {
        $prov = \Model\Eloquent\Region\Provinsi::find($provinsiId);

        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'User', 'url' => base_url('users')],
            ['text' => 'Provinsi', 'url' => base_url('users/provinsi')],
            ['text' => 'Kabupaten di ' . $prov->provinsi, 'url' => base_url('users/provinsi/' . $prov->id . '/kabupaten')],
            ['text' => 'Baru', 'url' => base_url('users/provinsi/' . $prov->id . '/kabupaten/create')],
        ];

        $data = [
            'title'       => 'Provinsi ' . $prov->provinsi,
            'menu'        => $this->menu,
            'webHeader'   => $this->webHeader,
            'breadcrumbs' => $breadcrumbs,
            'user'        => $this->user,
            'province'    => $prov
        ];

        echo $this->blade->view()
            ->make('user.create-district', $data)
            ->render();
    }

    public function district_store($provinsiId)
    {
        $prov = \Model\Eloquent\Region\Provinsi::find($provinsiId);

        $dinas = \Model\Eloquent\Dinas::create([
            'id_privilage' => 3,
            'nama_dinas'   => $this->input->post('nama_dinas'),
            'password'     => sha1($this->input->post('password2')),
            'alamat'       => $this->input->post('alamat'),
            'email'        => $this->input->post('email'),
            'telepon'      => $this->input->post('telepon'),
            'fax'          => $this->input->post('fax'),
        ]);

        $kab = new \Model\Eloquent\Region\KabupatenKota();
        $kab->kabupaten_kota = $this->input->post('name');
        $kab->logo_kabkot = ' ';
        $kab->kepala_daerah = ' ';
        $kab->foto_kepala_daerah = ' ';
        $kab->wakil_kepala_daerah = ' ';
        $kab->foto_wakil_kepala_daerah = ' ';
        $kab->luas_wilayah = 0;
        $kab->luas_wilayah_daratan = 0;
        $kab->luas_wilayah_lautan = 0;
        $kab->letak_geografis = ' ';
        $kab->jumlah_penduduk = 0;
        $kab->pertumbuhan_penduduk = 0;
        $kab->tingkat_kepadatan_penduduk = 0;
        $kab->jumlah_penduduk_miskin_kota = 0;
        $kab->jumlah_penduduk_miskin_desa = 0;

        $kab->dinas()->associate($dinas);
        $kab->province()->associate($prov);
        $kab->save();

        redirect(base_url('/users/provinsi/' . $provinsiId . '/kabupaten'));
    }

    public function district_edit($provinsiId, $districtId)
    {
        $provinsi = \Model\Eloquent\Region\Provinsi::with('dinas')->find($provinsiId);
        $kabupaten = \Model\Eloquent\Region\KabupatenKota::find($districtId);
        $withPassword = trim($this->input->get('password'));
        $withPassword = ($withPassword === 'true');

        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'User', 'url' => base_url('users')],
            ['text' => 'Provinsi', 'url' => base_url('users/provinsi')],
            ['text' => $provinsi->provinsi, 'url' => base_url('users/provinsi/' . $provinsi->id . '/edit')],
            ['text' => $kabupaten->kabupaten_kota, 'url' => base_url('users/provinsi/' . $provinsi->id . '/edit')],
        ];

        $data = [
            'title'        => 'Provinsi',
            'menu'         => $this->menu,
            'webHeader'    => $this->webHeader,
            'breadcrumbs'  => $breadcrumbs,
            'user'         => $this->user,
            'provinsi'     => $provinsi,
            'kabupaten'    => $kabupaten,
            'withPassword' => $withPassword
        ];

        echo $this->blade->view()
            ->make('user.edit-district', $data)
            ->render();
    }

    public function district_update($provinsiId, $districtId)
    {
        $provinsi = \Model\Eloquent\Region\Provinsi::with('dinas')->find($provinsiId);
        $kabupaten = \Model\Eloquent\Region\KabupatenKota::find($districtId);

        $dinas = $kabupaten->dinas;

        $refresh = false;
        if (is_null($dinas)) {
            $refresh = true;
            $dinas = new \Model\Eloquent\Dinas();
            $dinas->password = sha1($this->input->post('password2'));
        }

        $dinas->nama_dinas = $this->input->post('nama_dinas');
        $dinas->alamat = $this->input->post('alamat');
        $dinas->email = $this->input->post('email');
        $dinas->telepon = $this->input->post('telepon');
        $dinas->fax = $this->input->post('fax');
        $dinas->save();

        if ($refresh) {
            $kabupaten->dinas()->associate($dinas);
        }

        $kabupaten->save();


        redirect(base_url('/users/provinsi/' . $provinsi->id . '/kabupaten'));
    }

    public function password($id)
    {
        header('Content-Type: application/json');

        $view = $this->blade->view()
            ->make('user.password', [
                'id'   => $id,
                'type' => $this->input->get('type')
            ])
            ->render();

        echo json_encode([
            'title' => 'Password',
            'body'  => $view
        ]);
    }

    /**
     * @param $id
     * @throws Exception
     */
    public function update_password($id)
    {
        header('Content-Type: application/json');

        $pass1 = $this->input->post('password1');
        $pass2 = $this->input->post('password2');
        $type = $this->input->post('type');

        if ($type == 'province') {
            $region = \Model\Eloquent\Region\Provinsi::find($id);
        } elseif ($type == 'district') {
            $region = \Model\Eloquent\Region\KabupatenKota::find($id);
        } else {
            http_response_code(400);
            echo json_encode([
                'data' => 'Type not valid'
            ]);
        }

        if ($pass1 == $pass2) {
            $dinas = $region->dinas;
            $dinas->password = sha1($pass2);
            $dinas->save();
        } else {
            http_response_code(400);
            echo json_encode([
                'data' => 'Password tidak sama'
            ]);
        }
    }
}