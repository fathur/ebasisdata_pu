<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Form_B_K5 extends \Repositories\AbstractController
{
    public function data($idForm)
    {
        header('Content-Type: application/json');
        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");
        $search = $this->input->post("search");

        $orderColNum = (int)$order[0]['column'];
        $orderColDirection = $order[0]['dir'];
        $orderColumn = $columns[$orderColNum]['name'];

        $form = \Model\Eloquent\Form::find($idForm);

        $builder = $form->k5()->select('*');
//            ->orderBy($orderColumn, $orderColDirection);
//            ->offset($start)->take($length);

        $count = $builder->count();

        $results = $builder->get();

        $data = [];
        $i = 1;
        foreach ($results as $item) {
            $fungsi = '<a class=\'btn btn-sm blue-hoki\' onclick="kModalEdit(\'' . base_url("form-1b/{$form->id}/k5/{$item->id}/edit") . '\')">Ubah</a>';

            array_push($data, [
                'no' => "{$i}.",
                'region' => optional(\Model\Eloquent\Region\Kecamatan::find($item->idkecamatan))->name,
                'jumlah_kk_rt_3' => number_format($item->jumlah_kk_rt_3, 0, ',', '.'),
                'jumlah_rtlh_versi_bdt_4' => number_format($item->jumlah_rtlh_versi_bdt_4, 0, ',', '.'),
                'jumlah_rtlh_verifikasi_pemda_5' => number_format($item->jumlah_rtlh_verifikasi_pemda_5, 0, ',', '.'),
                'sumber_data_6' => $item->sumber_data_6,
                'fungsi' => $fungsi
            ]);

            $i++;
        }

        $footer = [
            'total_kk' => ($form->k5) ? number_format($form->k5->sum('jumlah_kk_rt_3'), 0, ',', '.') : 0,
            'total_rtlh_bdt' => ($form->k5) ? number_format($form->k5->sum('jumlah_rtlh_versi_bdt_4'), 0, ',', '.') : 0,
            'total_rtlh_pemda' => ($form->k5) ? number_format($form->k5->sum('jumlah_rtlh_verifikasi_pemda_5'), 0, ',', '.') : 0
        ];

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $data,
            "footer" => $footer
        );

        echo json_encode($output);
    }

    public function edit($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K5::find($idK);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        $view = $this->blade->view()
            ->make('form.k5.edit', [
                'form' => $form,
                'k' => $k,
                'mode' => '1b'
            ])
            ->render();

        echo json_encode([
            'title' => 'K.5 RUMAH TIDAK LAYAK HUNI',
            'body' => $view
        ]);
    }

    public function update($idForm, $idK)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);
        $k = \Model\Eloquent\Form\K5::find($idK);

        if ($form->id != $k->idform_1) {
            show_404();
        }

        $k->jumlah_kk_rt_3 = $this->input->post('jumlah_kk_rt_3') ? $this->input->post('jumlah_kk_rt_3') : 0;
        $k->jumlah_rtlh_versi_bdt_4 = $this->input->post('jumlah_rtlh_versi_bdt_4') ? $this->input->post('jumlah_rtlh_versi_bdt_4') : 0;
        $k->jumlah_rtlh_verifikasi_pemda_5 = $this->input->post('jumlah_rtlh_verifikasi_pemda_5') ? $this->input->post('jumlah_rtlh_verifikasi_pemda_5') : 0;
        $k->sumber_data_6 = $this->input->post('sumber_data_6');

        if ($k->save())
            $form->updateKelengkapan('k5');

        echo 1;
    }

    public function create($idForm)
    {
        $form = \Model\Eloquent\Form::find($idForm);

        if (!is_null($form->dinas->kabupatenKota)) {
            // Kumpulkan semua kecamatan di provinsi ini
            $kecamatans = \Model\Eloquent\Region\Kecamatan::kabupatenKota($form->dinas->kabupatenKota->id)->get();

            // Insert semua kab kot
            foreach ($kecamatans as $kecamatan) {
                $form->k5()->create([
                    'idkabupaten_kota' => $form->dinas->kabupatenKota->id,
                    'idkecamatan'   => $kecamatan->id

                ]);
            }

        }
    }
}