<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Main extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'html', 'url'));
        $this->load->model('menu_model');
        $this->load->model('notification_model');
        $this->load->model('message_model');
        $this->load->model('dinas_model');
        $this->load->model('form_1_model');
        $this->load->model('userm_model');
        $this->load->model('master_model');
        $this->load->model('profil_model');
        $this->load->model('sejuta_rumah');
        $this->load->model('user_m');

        if (!$this->session->userdata('isLoggedIn')) {
            redirect(base_url() . 'login/show_login');
        }
    }

    function add_ajax_kab($id_prov)
    {
        $query = $this->db->order_by('kabupaten_kota', 'ASC')->get_where('kabupaten_kota', array('idprovinsi' => $id_prov));
        $data = "<option value=''>Kabupaten/Kota</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='" . $value->idkabupaten_kota . "'>" . $value->kabupaten_kota . "</option>";
        }
        echo $data;
    }

    public function index()
    {
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }

        // Jika session isLoggedIn tidak ada maka munculkan halaman login kembali
        if (!$this->session->userdata('isLoggedIn')) {
            redirect(base_url() . 'login/show_login');
        }

        $this->load->model('userm_model');

        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['prov_admin'] = $this->master_model->provinsi_admin($this->session->userdata('iddinas'));
        $user_id = $this->session->userdata('iddinas');
        //if (isset($user_id)) {$user_id=4;}
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['g1'] = $this->input->post('g1');
        $data['g2'] = $this->input->post('g2');
        $data['g3'] = $this->input->post('g3');
        $data['g4'] = $this->input->post('g4');
        $data['g5'] = $this->input->post('g5');
        $data['user_id'] = $user_id;
        if ($this->input->post('provinsi_select') == null) {
            $data['provinsi_select'] = 0;
        } else {
            $data['provinsi_select'] = $this->input->post('provinsi_select');
        }
        if ($this->session->userdata('idprivilage') != "1") {
            if ($this->session->userdata('idprivilage') == "2") {
                $data['backlog_kepemilikan_dan_penghunian_view'] = $this->master_model->backlog_kepemilikan_dan_penghunian_view($tahun);
            } else {
                $data['backlog_kepemilikan_dan_penghunian_view'] = $this->master_model->backlog_kepemilikan_dan_penghunian_view_2($tahun, $this->session->userdata('idprovinsi'));
            }

            $data['backlog_kepemilikan_dan_penghunian_kab_view'] = $this->master_model->backlog_kepemilikan_dan_penghunian_kab_view($tahun, $this->session->userdata('idprovinsi'));
        } else {
            $data['backlog_kepemilikan_dan_penghunian_view'] = $this->master_model->backlog_kepemilikan_dan_penghunian_view($tahun);
            $data['backlog_kepemilikan_dan_penghunian_kab_view'] = $this->master_model->backlog_kepemilikan_dan_penghunian_kab_view2($tahun);
        }
        $data['kategori_1'] = $this->input->post('kategori_1');
        $data['kategori_2'] = $this->input->post('kategori_2');
        if ((strlen($this->input->post('kategori_1')) == 0) or (strlen($this->input->post('kategori_2')) == 0)) {
            $data['kategori_1'] = "1";
        }
        $data['pv2'] = $this->input->post('pv');
        if ((strlen($this->input->post('g1')) == 0) or (strlen($this->input->post('g2')) == 0) or (strlen($this->input->post('g3')) == 0) or (strlen($this->input->post('g4')) == 0) or (strlen($this->input->post('g5')) == 0)) {
            $data['g2'] = "1";
        }
        $data['email'] = $this->session->userdata('email');
        $data['name'] = $this->session->userdata('name');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['avatar'] = $this->session->userdata('avatar');
        $data['tagline'] = $this->session->userdata('tagline');
        $data['teamId'] = $this->session->userdata('teamId');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['log_pekerjaan'] = $this->notification_model->log_pekerjaan();
        $data['inbox'] = $this->message_model->inbox();
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['user_id'] = $this->session->userdata('iddinas');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['x'] = $this->session->userdata('x');
        $data['y'] = $this->session->userdata('y');
        $data['z'] = $this->session->userdata('z');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['idkabupaten_kota'] = $this->session->userdata('idkabupaten_kota');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);

        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        $data['prov_matrix_new'] = $this->master_model->provinsi_matrix_new();
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $pilih_prov_text);

            $query = $this->db->query('SELECT x,y,z from provinsi where idprovinsi=' . $pilih_prov_text);
            foreach ($query->result_array() as $row):
                $data['x'] = $row['x'];
                $data['y'] = $row['y'];
                $data['z'] = $row['z'];
            endforeach;
        }

        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['tahun'] = $tahun;
        $data['pilih_prov_text'] = $pilih_prov_text;

        $data['status_kepemilikan_rumah2'] = $this->form_1_model->status_kepemilikan_rumah2($tahun);
        $data['status_kepemilikan_rumah3'] = $this->form_1_model->status_kepemilikan_rumah3($tahun);
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $data['kabkot_form_1b'] = $this->userm_model->kabupaten_kota();

        $data['kabupaten_kota_p'] = $this->master_model->kabupaten_kota();


        $this->load->view('html/index', $data);
    }

    public function update_user_admin()
    {
        $data = array(
            $this->input->post('provinsi_select')
        );
        $this->master_model->user_prov_update($this->input->post('provinsi_select'), $this->session->userdata('iddinas'));
        header('Location: ' . BASE_URL('/main/index'));
        exit;
    }

    public function tentang()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $title = $this->session->userdata('title');
        $data['title'] = $title;
        $this->load->model('userm_model');
        $iddinas = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $data['pengguna'] = $this->userm_model->pengguna_dinas($iddinas);
        $data['email'] = $this->userm_model->pengguna_dinas($iddinas);
        $data['firstName'] = $this->userm_model->pengguna_dinas($iddinas);
        $data['lastName'] = $this->userm_model->pengguna_dinas($iddinas);
        $data['avatar'] = $this->session->userdata('avatar');
        $data['tagline'] = $this->session->userdata('tagline');
        $data['teamId'] = $this->session->userdata('teamId');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['privilage'] = $this->session->userdata('idprivilage');

        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }


        $this->load->view('html/tentang', $data);
    }

    public function kontak()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $title = $this->session->userdata('title');
        $data['title'] = $title;
        $this->load->model('userm_model');
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $iddinas;
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        if ($this->input->post('text_cari_pesan') == null || $this->input->post('text_cari_pesan') == "") {
            $judul = "";
            $data['pesan_personal'] = 0;
        } else {
            $judul = $this->input->post('text_cari_pesan');
            $data['pesan_personal'] = 1;
        }
        $data['pesan'] = $this->master_model->pesan($iddinas, $judul);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $data['pengguna'] = $this->userm_model->pengguna_dinas($iddinas);
        $data['email'] = $this->userm_model->pengguna_dinas($iddinas);
        $data['firstName'] = $this->userm_model->pengguna_dinas($iddinas);
        $data['lastName'] = $this->userm_model->pengguna_dinas($iddinas);
        $data['avatar'] = $this->session->userdata('avatar');
        $data['tagline'] = $this->session->userdata('tagline');
        $data['teamId'] = $this->session->userdata('teamId');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['privilage'] = $this->session->userdata('idprivilage');

        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $this->load->view('html/kontak', $data);
    }

    public function kontak_admin()
    {

        $data['user_id'] = $this->session->userdata('iddinas');
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $user_id = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['tahun'] = $this->input->post('tahun');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $data['rtlh_kabupaten_kota'] = $this->form_1_model->rtlh_kabupaten_kota($user_id, $tahun, $this->session->userdata('idprovinsi'));
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }
        if (strlen($this->input->post('text_idpesan') != 0)) {
            $text_idpesan = $this->input->post('text_idpesan');
            $text_judul = $this->input->post('text_judul');
            $text_dinas = $this->input->post('text_dinas');
        } else {
            $text_idpesan = "0";
            $text_judul = "";
            $text_dinas = "";
        }
        if ($this->input->post('text_pesan') != "") {
            $data2 = array(
                'pesan' => $this->input->post('text_pesan'),
                'idjudul' => $this->input->post('text_idpesan'),
                'status_pesan' => 'out',
                'waktu' => date('Y-m-d h:i')
            );
            $this->master_model->kirim_pesan_admin($data2);
            $data3 = array(
                'pesan' => $this->input->post('text_pesan'),
                'idjudul' => $this->input->post('text_idpesan'),
                'status_pesan' => 'in',
                'waktu' => date('Y-m-d h:i')
            );
            $this->master_model->kirim_pesan2($data3);
        }
        if (isset($judul)) {
            $judul = "";
        } else {
            $judul = "";
        }
        $data['text_idpesan'] = $text_idpesan;
        $data['text_judul'] = $text_judul;
        $data['text_dinas'] = $text_dinas;
        $data['pesan_admin'] = $this->master_model->pesan_admin($judul);
        $data['pesan_detail_admin'] = $this->master_model->pesan_detail_admin($text_idpesan);
        $data['pilih_pesan'] = 0;
        $this->load->view('html/pesan_admin', $data);
    }

    public function Form1_insert()
    {
        if ($this->session->userdata('idprivilage') == "1") {
            $iddinas = $this->input->post('iddinas');
            $data['iddinas'] = $this->input->post('iddinas');
        } else {
            $iddinas = $this->session->userdata('iddinas');
            $data['iddinas'] = $this->session->userdata('iddinas');
        }
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $iddinas,
            'idprovinsi' => $this->session->userdata('idprovinsi'),
            'tahun' => $this->input->post('tahun')
        );
        /////$data['form_1_view_detail']
        $idf = $this->form_1_model->Form1_insert($form_1_data);

        header('Location: ' . BASE_URL('/main/form_1a/' . $iddinas . '/' . $idf));
        exit;
    }

    public function Form1_insert2()
    {
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'idprovinsi' => $this->session->userdata('idprovinsi'),
            'tahun' => $this->input->post('tahun')
        );
        /////k1
        $data['form_1_view_detail'] = $this->form_1_model->Form1_insert($form_1_data);

        header('Location: ' . BASE_URL('/main/form_1b'));
        exit;
    }

    public function form1a_cetak()
    {

        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idform1')) == 0) {
            $idform1 = 0;
        } else {
            $idform1 = $this->input->post('idform1');
        }
        $data['idform1'] = $idform1;
        $iddinas = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($iddinas);
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'idprovinsi' => $this->session->userdata('idprovinsi')
        );
        $data['form_1_view_detail'] = $this->form_1_model->form_1_view_detail($form_1_data, $idform1);
        $data['form_1_k1_count'] = $this->form_1_model->form_1_k1_count($form_1_data, $idform1);
        $data['form_1_k1'] = $this->form_1_model->form_1_k1($form_1_data, $idform1);
        //k2
        $data['form_1_k2_a'] = $this->form_1_model->form_1_k2_a($form_1_data, $idform1);
        $data['form_1_k2_b'] = $this->form_1_model->form_1_k2_b($form_1_data, $idform1);
        $data['form_1_k2_b_sum'] = $this->form_1_model->form_1_k2_b_sum($form_1_data, $idform1);
        $data['form_1_k3'] = $this->form_1_model->form_1_k3($form_1_data, $idform1);
        $data['form_1_k3_count'] = $this->form_1_model->form_1_k3_count($form_1_data, $idform1);
        $data['form_1_k4_1'] = $this->form_1_model->form_1_k4_1($form_1_data, $idform1);
        $data['form_1_k4_1_count'] = $this->form_1_model->form_1_k4_1_count($form_1_data, $idform1);
        $data['form_1_k4_2'] = $this->form_1_model->form_1_k4_2($form_1_data, $idform1);
        $data['form_1_k4_2_count'] = $this->form_1_model->form_1_k4_2_count($form_1_data, $idform1);
        $data['form_1_k4_3'] = $this->form_1_model->form_1_k4_3($form_1_data, $idform1);
        $data['form_1_k4_3_count'] = $this->form_1_model->form_1_k4_3_count($form_1_data, $idform1);
        $data['form_1_k4_4'] = $this->form_1_model->form_1_k4_4($form_1_data, $idform1);
        $data['form_1_k4_4_sum'] = $this->form_1_model->form_1_k4_4_sum($form_1_data, $idform1);
        $data['form_1_k4_5'] = $this->form_1_model->form_1_k4_5($form_1_data, $idform1);
        $data['form_1_k5'] = $this->form_1_model->form_1_k5($form_1_data, $idform1);
        $data['form_1_k5_sum'] = $this->form_1_model->form_1_k5_sum($form_1_data, $idform1);
        $data['form_1_k6'] = $this->form_1_model->form_1_k6($form_1_data, $idform1);
        $data['form_1_k6_sum'] = $this->form_1_model->form_1_k6_sum($form_1_data, $idform1);
        $data['kabupaten_kota_combo'] = $this->form_1_model->kabupaten_kota_combo($form_1_data, $idform1);
        // $data['kecamatan_combo']=$this->form_1_model->kecamatan_combo();
        $data['error'] = ' ';
        $this->load->view('html/form1a_cetak', $data);
        ////pdf
        if (strlen($this->input->post('pdf_cetak')) == 0) {
            $pdf_cetak = 0;

        } else {
            $pdf_cetak = $this->input->post('pdf_cetak');
        }
        if ($pdf_cetak == 1) {
            $this->load->view('html/form1a_cetak', $data);
            $sumber = $this->load->view('html/form1a_cetak', $data, TRUE);
            $html = $sumber;


            $this->load->library('M_pdf');
            $pdfFilePath = "Form1A.pdf";

            $pdf = $this->m_pdf->load();

            $pdf->AddPage('L');
            $pdf->WriteHTML($stylesheet, 1);
            $pdf->WriteHTML($html);

            $pdf->Output($pdfFilePath, "D");
            exit();
        }
    }

    public function form1a_cetak_admin()
    {

        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idform1')) == 0) {
            $idform1 = 0;
        } else {
            $idform1 = $this->input->post('idform1');
        }
        $iddinas = $this->session->userdata('iddinas');

        $user_id = $this->session->userdata('iddinas');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'idprovinsi' => $this->session->userdata('idprovinsi')
        );
        $data['form_1_view_detail'] = $this->form_1_model->form_1_view_detail($form_1_data, $idform1);
        $data['form_1_k1_count'] = $this->form_1_model->form_1_k1_count($form_1_data, $idform1);
        $data['form_1_k1'] = $this->form_1_model->form_1_k1($form_1_data, $idform1);
        //k2
        $data['form_1_k2_a'] = $this->form_1_model->form_1_k2_a($form_1_data, $idform1);
        $data['form_1_k2_b'] = $this->form_1_model->form_1_k2_b($form_1_data, $idform1);
        $data['form_1_k2_b_sum'] = $this->form_1_model->form_1_k2_b_sum($form_1_data, $idform1);
        $data['form_1_k3'] = $this->form_1_model->form_1_k3($form_1_data, $idform1);
        $data['form_1_k3_count'] = $this->form_1_model->form_1_k3_count($form_1_data, $idform1);
        $data['form_1_k4_1'] = $this->form_1_model->form_1_k4_1($form_1_data, $idform1);
        $data['form_1_k4_1_count'] = $this->form_1_model->form_1_k4_1_count($form_1_data, $idform1);
        $data['form_1_k4_2'] = $this->form_1_model->form_1_k4_2($form_1_data, $idform1);
        $data['form_1_k4_2_count'] = $this->form_1_model->form_1_k4_2_count($form_1_data, $idform1);
        $data['form_1_k4_3'] = $this->form_1_model->form_1_k4_3($form_1_data, $idform1);
        $data['form_1_k4_3_count'] = $this->form_1_model->form_1_k4_3_count($form_1_data, $idform1);
        $data['form_1_k4_4'] = $this->form_1_model->form_1_k4_4($form_1_data, $idform1);
        $data['form_1_k4_4_sum'] = $this->form_1_model->form_1_k4_4_sum($form_1_data, $idform1);
        $data['form_1_k4_5'] = $this->form_1_model->form_1_k4_5($form_1_data, $idform1);
        $data['form_1_k5'] = $this->form_1_model->form_1_k5($form_1_data, $idform1);
        $data['form_1_k5_sum'] = $this->form_1_model->form_1_k5_sum($form_1_data, $idform1);
        $data['form_1_k6'] = $this->form_1_model->form_1_k6($form_1_data, $idform1);
        $data['form_1_k6_sum'] = $this->form_1_model->form_1_k6_sum($form_1_data, $idform1);
        $data['kabupaten_kota_combo'] = $this->form_1_model->kabupaten_kota_combo($form_1_data, $idform1);
        // $data['kecamatan_combo']=$this->form_1_model->kecamatan_combo();
        $data['error'] = ' ';
        $this->load->view('html/form1a_cetak', $data);
    }

    public function form1b_cetak()
    {

        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idform1')) == 0) {
            $idform1 = 0;
        } else {
            $idform1 = $this->input->post('idform1');
        }
        $iddinas = $this->session->userdata('iddinas');

        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($iddinas);
        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['idkabupaten_kota'] = $this->session->userdata('idkabupaten_kota');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'idprovinsi' => $this->session->userdata('idprovinsi')
        );
        $data['form_1_view_detail'] = $this->form_1_model->form_1_view_detail($form_1_data, $idform1);
        $data['form_1_k1_count'] = $this->form_1_model->form_1_k1_count($form_1_data, $idform1);
        $data['form_1_k1'] = $this->form_1_model->form_1_k1($form_1_data, $idform1);
        //k2
        $data['form_1_k2_a'] = $this->form_1_model->form_1_k2_a($form_1_data, $idform1);
        $data['form_1_k2_b'] = $this->form_1_model->form_1_k2_b($form_1_data, $idform1);
        $data['form_1_k2_b_sum'] = $this->form_1_model->form_1_k2_b_sum($form_1_data, $idform1);
        $data['form_1_k3'] = $this->form_1_model->form_1_k3($form_1_data, $idform1);
        $data['form_1_k3_count'] = $this->form_1_model->form_1_k3_count($form_1_data, $idform1);
        $data['form_1_k4_1'] = $this->form_1_model->form_1_k4_1($form_1_data, $idform1);
        $data['form_1_k4_1_count'] = $this->form_1_model->form_1_k4_1_count($form_1_data, $idform1);
        $data['form_1_k4_2'] = $this->form_1_model->form_1_k4_2($form_1_data, $idform1);
        $data['form_1_k4_2_count'] = $this->form_1_model->form_1_k4_2_count($form_1_data, $idform1);
        $data['form_1_k4_3'] = $this->form_1_model->form_1_k4_3($form_1_data, $idform1);
        $data['form_1_k4_3_count'] = $this->form_1_model->form_1_k4_3_count($form_1_data, $idform1);
        $data['form_1_k4_4'] = $this->form_1_model->form_1_k4_4($form_1_data, $idform1);
        $data['form_1_k4_4_sum'] = $this->form_1_model->form_1_k4_4_sum($form_1_data, $idform1);
        $data['form_1_k4_5'] = $this->form_1_model->form_1_k4_5($form_1_data, $idform1);
        $data['form_1_k5'] = $this->form_1_model->form_1_k5($form_1_data, $idform1);
        $data['form_1_k5_sum'] = $this->form_1_model->form_1_k5_sum($form_1_data, $idform1);
        $data['form_1_k6'] = $this->form_1_model->form_1_k6($form_1_data, $idform1);
        $data['form_1_k6_sum'] = $this->form_1_model->form_1_k6_sum($form_1_data, $idform1);
        $data['kabupaten_kota_combo'] = $this->form_1_model->kabupaten_kota_combo($form_1_data, $idform1);
        //$data['kecamatan_combo']=$this->form_1_model->kecamatan_combo2($this->session->userdata('idkabupaten_kota'));
        $data['error'] = ' ';
        $this->load->view('html/form1b_cetak', $data);
    }


    public function form1b_cetak_adminb()
    {

        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idform1')) == 0) {
            $idform1 = 0;
        } else {
            $idform1 = $this->input->post('idform1');
        }
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($data['iddinas']);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['idkabupaten_kota'] = $this->session->userdata('idkabupaten_kota');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'idprovinsi' => $this->session->userdata('idprovinsi')
        );
        $data['form_1_view_detail'] = $this->form_1_model->form_1_view_detail($form_1_data, $idform1);
        $data['form_1_k1_count'] = $this->form_1_model->form_1_k1_count($form_1_data, $idform1);
        $data['form_1_k1'] = $this->form_1_model->form_1_k1($form_1_data, $idform1);
        //k2
        $data['form_1_k2_a'] = $this->form_1_model->form_1_k2_a($form_1_data, $idform1);
        $data['form_1_k2_b'] = $this->form_1_model->form_1_k2_b($form_1_data, $idform1);
        $data['form_1_k2_b_sum'] = $this->form_1_model->form_1_k2_b_sum($form_1_data, $idform1);
        $data['form_1_k3'] = $this->form_1_model->form_1_k3($form_1_data, $idform1);
        $data['form_1_k3_count'] = $this->form_1_model->form_1_k3_count($form_1_data, $idform1);
        $data['form_1_k4_1'] = $this->form_1_model->form_1_k4_1($form_1_data, $idform1);
        $data['form_1_k4_1_count'] = $this->form_1_model->form_1_k4_1_count($form_1_data, $idform1);
        $data['form_1_k4_2'] = $this->form_1_model->form_1_k4_2($form_1_data, $idform1);
        $data['form_1_k4_2_count'] = $this->form_1_model->form_1_k4_2_count($form_1_data, $idform1);
        $data['form_1_k4_3'] = $this->form_1_model->form_1_k4_3($form_1_data, $idform1);
        $data['form_1_k4_3_count'] = $this->form_1_model->form_1_k4_3_count($form_1_data, $idform1);
        $data['form_1_k4_4'] = $this->form_1_model->form_1_k4_4($form_1_data, $idform1);
        $data['form_1_k4_4_sum'] = $this->form_1_model->form_1_k4_4_sum($form_1_data, $idform1);
        $data['form_1_k4_5'] = $this->form_1_model->form_1_k4_5($form_1_data, $idform1);
        $data['form_1_k5'] = $this->form_1_model->form_1_k5($form_1_data, $idform1);
        $data['form_1_k5_sum'] = $this->form_1_model->form_1_k5_sum($form_1_data, $idform1);
        $data['form_1_k6'] = $this->form_1_model->form_1_k6($form_1_data, $idform1);
        $data['form_1_k6_sum'] = $this->form_1_model->form_1_k6_sum($form_1_data, $idform1);
        $data['kabupaten_kota_combo'] = $this->form_1_model->kabupaten_kota_combo($form_1_data, $idform1);
        $data['error'] = ' ';
        $this->load->view('html/form1b_cetak_adminb', $data);
    }


    public function form_1a()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idform1')) == 0) {
            //$idform1=0;
            $idform1 = $this->uri->segment(4);
        } else {
            $idform1 = $this->input->post('idform1');
        }
        if ($this->session->userdata('idprivilage') == 1) {
            if ($this->input->post('iddinas') == "") {
                $iddinas = $this->uri->segment(3);
            } else {
                $iddinas = $this->input->post('iddinas');
            }
            $query = $this->db->query('SELECT kel.kelurahan,kec.kecamatan,kk.kabupaten_kota,d.alamat,p.iddinas,p.provinsi,p.idprovinsi,d.nama_dinas 
        FROM provinsi p 
        join dinas d on p.iddinas=d.iddinas
        left join kecamatan kec on kec.idkecamatan=d.idkecamatan
        left join kelurahan kel on kel.idkelurahan=d.idkelurahan
        left join kabupaten_kota kk on kk.idkabupaten_kota=d.idkabupaten_kota
        where p.iddinas=' . $iddinas . ' group by p.idprovinsi');
            foreach ($query->result_array() as $row):
                $nama_provinsi_from = $row['provinsi'];
                $din = $row['iddinas'];
                $id_prov = $row['idprovinsi'];
                $id_kab = 0;
                $data['nama_dinas'] = $row['nama_dinas'];
                $data['idprovinsi'] = $row['idprovinsi'];
                $data['provinsi'] = $row['provinsi'];
                $data['iddinas'] = $row['iddinas'];
                $data['alamat'] = $row['alamat'];
                $data['kabupaten_kota'] = $row['kabupaten_kota'];
                $data['kecamatan'] = $row['kecamatan'];
                $data['kelurahan'] = $row['kelurahan'];
            endforeach;

        } else {
            $iddinas = $this->session->userdata('iddinas');
            $din = $iddinas;
            $id_prov = $this->session->userdata('idprovinsi');
            $id_kab = $this->session->userdata('iddinas');
            $data['nama_dinas'] = $this->session->userdata('nama_dinas');
            $data['idprovinsi'] = $this->session->userdata('idprovinsi');
            $data['provinsi'] = $this->session->userdata('provinsi');
            $data['iddinas'] = $this->session->userdata('iddinas');
            $data['alamat'] = $this->session->userdata('alamat');
            $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
            $data['kecamatan'] = $this->session->userdata('kecamatan');
            $data['kelurahan'] = $this->session->userdata('kelurahan');
        }

        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($iddinas);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $iddinas,
            'idprovinsi' => $id_prov,
            'idkabupaten_kota' => $id_kab
        );
        $data['form_1_view_detail'] = $this->form_1_model->form_1_view_detail($form_1_data, $idform1);
        $data['form_1_k1_count'] = $this->form_1_model->form_1_k1_count($form_1_data, $idform1);
        $data['form_1_k1'] = $this->form_1_model->form_1_k1($form_1_data, $idform1);
        //k2
        $data['form_1_k2_a'] = $this->form_1_model->form_1_k2_a($form_1_data, $idform1);
        $data['form_1_k2_b'] = $this->form_1_model->form_1_k2_b($form_1_data, $idform1);
        $data['form_1_k2_b_sum'] = $this->form_1_model->form_1_k2_b_sum($form_1_data, $idform1);
        $data['form_1_k3'] = $this->form_1_model->form_1_k3($form_1_data, $idform1);
        $data['form_1_k3_count'] = $this->form_1_model->form_1_k3_count($form_1_data, $idform1);
        $data['form_1_k4_1'] = $this->form_1_model->form_1_k4_1($form_1_data, $idform1);
        $data['form_1_k4_1_count'] = $this->form_1_model->form_1_k4_1_count($form_1_data, $idform1);
        $data['form_1_k4_2'] = $this->form_1_model->form_1_k4_2($form_1_data, $idform1);
        $data['form_1_k4_2_count'] = $this->form_1_model->form_1_k4_2_count($form_1_data, $idform1);
        $data['form_1_k4_3'] = $this->form_1_model->form_1_k4_3($form_1_data, $idform1);
        $data['form_1_k4_3_count'] = $this->form_1_model->form_1_k4_3_count($form_1_data, $idform1);
        $data['form_1_k4_4'] = $this->form_1_model->form_1_k4_4($form_1_data, $idform1);
        $data['form_1_k4_4_sum'] = $this->form_1_model->form_1_k4_4_sum($form_1_data, $idform1);
        $data['form_1_k4_5'] = $this->form_1_model->form_1_k4_5($form_1_data, $idform1);
        $data['form_1_k5'] = $this->form_1_model->form_1_k5($form_1_data, $idform1);
        $data['form_1_k5_sum'] = $this->form_1_model->form_1_k5_sum($form_1_data, $idform1);
        $data['form_1_k6'] = $this->form_1_model->form_1_k6($form_1_data, $idform1);
        $data['form_1_k6_sum'] = $this->form_1_model->form_1_k6_sum($form_1_data, $idform1);
        $data['kelengkapan'] = $this->form_1_model->kelengkapan($form_1_data, $idform1);
        $data['kabupaten_kota_combo'] = $this->form_1_model->kabupaten_kota_combo($form_1_data, $idform1);
        //$data['kecamatan_combo2'] = $this->form_1_model->kecamatan_combo();
        $data['kecamatan_combo'] = $this->form_1_model->kecamatan_combo($this->session->userdata('idkabupaten_kota'));
        $data['error'] = ' ';

        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }


        $this->load->view('html/form1a', $data);
    }

    public function form_1b()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idform1')) == 0) {
            $idform1 = 0;
        } else {
            $idform1 = $this->input->post('idform1');
        }

        $iddinas = $this->session->userdata('iddinas');

        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($iddinas);
        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['idkabupaten_kota'] = $this->session->userdata('idkabupaten_kota');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'idprovinsi' => $this->session->userdata('idprovinsi'),
            'idkabupaten_kota' => $this->session->userdata('iddinas')
        );
        $data['form_1_view_detail'] = $this->form_1_model->form_1_view_detail($form_1_data, $idform1);
        $data['form_1_k1_count'] = $this->form_1_model->form_1_k1_count($form_1_data, $idform1);
        $data['form_1_k1'] = $this->form_1_model->form_1_k1($form_1_data, $idform1);
        //k2
        $data['form_1_k2_a'] = $this->form_1_model->form_1_k2_a($form_1_data, $idform1);
        $data['form_1_k2_b'] = $this->form_1_model->form_1_k2_b($form_1_data, $idform1);
        $data['form_1_k2_b_sum'] = $this->form_1_model->form_1_k2_b_sum($form_1_data, $idform1);
        $data['form_1_k3'] = $this->form_1_model->form_1_k3($form_1_data, $idform1);
        $data['form_1_k3_count'] = $this->form_1_model->form_1_k3_count($form_1_data, $idform1);
        $data['form_1_k4_1'] = $this->form_1_model->form_1_k4_1($form_1_data, $idform1);
        $data['form_1_k4_1_count'] = $this->form_1_model->form_1_k4_1_count($form_1_data, $idform1);
        $data['form_1_k4_2'] = $this->form_1_model->form_1_k4_2($form_1_data, $idform1);
        $data['form_1_k4_2_count'] = $this->form_1_model->form_1_k4_2_count($form_1_data, $idform1);
        $data['form_1_k4_3'] = $this->form_1_model->form_1_k4_3($form_1_data, $idform1);
        $data['form_1_k4_3_count'] = $this->form_1_model->form_1_k4_3_count($form_1_data, $idform1);
        $data['form_1_k4_4'] = $this->form_1_model->form_1_k4_4($form_1_data, $idform1);
        $data['form_1_k4_4_sum'] = $this->form_1_model->form_1_k4_4_sum($form_1_data, $idform1);
        $data['form_1_k4_5'] = $this->form_1_model->form_1_k4_5($form_1_data, $idform1);
        $data['form_1_k5'] = $this->form_1_model->form_1_k5($form_1_data, $idform1);
        $data['form_1_k5_sum'] = $this->form_1_model->form_1_k5_sum($form_1_data, $idform1);
        $data['form_1_k6_b'] = $this->form_1_model->form_1_k6_b($form_1_data, $idform1);
        $data['form_1_k6_sum_b'] = $this->form_1_model->form_1_k6_sum_b($form_1_data, $idform1);
        $data['kelengkapan'] = $this->form_1_model->kelengkapan($form_1_data, $idform1);
        $data['kabupaten_kota_combo'] = $this->form_1_model->kabupaten_kota_combo($form_1_data, $idform1);
        //$data['kecamatan_combo2'] = $this->form_1_model->kecamatan_combo();
        $data['kecamatan_combo'] = $this->form_1_model->kecamatan_combo($this->session->userdata('idkabupaten_kota'));
        $data['error'] = ' ';
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }
        $this->load->view('html/form1b', $data);
    }

    function add_ajax_kecamatan($idkabupaten_kota)
    {
        $query = $this->db->get_where('kecamatan', array('idkabupaten_kota' => $idkabupaten_kota));
        $data = "<option value=''>Kecamatan</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='" . $value->idkecamatan . "'>" . $value->kecamatan . "</option>";
        }
        echo $data;
    }

    function add_ajax_tahun($tahun)
    {
        $data = "";
        $user_id = $this->session->userdata('iddinas');
        $query = $this->db->get_where('form_1', array('tahun' => $tahun, 'iddinas' => $user_id));
        if ($query->num_rows() > 0) {
            $data .= "Tahun tersebut telah ada";
        } else {
            $data .= "";
        }

        echo $data;
    }

    public function form_1a_view()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $user_id = $this->session->userdata('iddinas');
        $data['iddinas'] = $user_id;
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);

        if (strlen($this->input->post('provinsi_form_1a')) == 0 || $this->input->post('provinsi_form_1a') < 1) {
            $u = $user_id;
        } else {
            $query = $this->db->query('SELECT iddinas,provinsi FROM provinsi where idprovinsi=' . $this->input->post('provinsi_form_1a'));
            foreach ($query->result_array() as $row):
                $u = $row['iddinas'];
                $nama_provinsi_from = $row['provinsi'];
            endforeach;
        }

        if (isset($nama_provinsi_from)) {
            $data['nama_provinsi_from'] = $nama_provinsi_from;
        } else {
            $data['nama_provinsi_from'] = "";
        }


        $data['idprovinsi'] = $u;
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($u);
        $data['dinas_view'] = $this->dinas_model->dinas_view($u);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['form_1_view'] = $this->form_1_model->form_1_view($u);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $this->load->model('userm_model');
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $data['kabkot_form_1b'] = $this->userm_model->kabupaten_kota();


        $this->load->view('html/form_1a_view', $data);
    }

    public function form_1a_cetak()
    {

        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->input->post('provinsi_form_1a');
        $data['idprovinsi'] = $this->input->post('provinsi_form_1a');
        $data['form_1_view'] = $this->form_1_model->form_1_view($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['cetak'] = $this->input->post('cet');
        $this->load->view('html/form_1a_cetak', $data);
    }

    public function form_1a_cetak_pdf()
    {

        $user_id = $this->input->post('provinsi_form_1a');
        $data['form_1_view'] = $this->form_1_model->form_1_view($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $this->load->view('html/form_1a_cetak_pdf', $data);
        $sumber = $this->load->view('html/form_1a_cetak_pdf', $data, TRUE);
        $html = $sumber;


        $this->load->library('M_pdf');
        $pdfFilePath = "Form1A.pdf";
        //lokasi file css yang akan di load
        //$css = $this->load->view('admin/css/bootstrap.min.css');
        //$stylesheet = file_get_contents($css);

        $pdf = $this->m_pdf->load();

        $pdf->AddPage('L');
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);

        $pdf->Output($pdfFilePath, "D");
        exit();
    }

    public function cetak_form_1a_view()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);

        if (strlen($this->input->post('provinsi_form_1a')) == 0 || $this->input->post('provinsi_form_1a') < 1) {
            $u = $user_id;
        } else {
            $query = $this->db->query('SELECT iddinas,provinsi FROM provinsi where idprovinsi=' . $this->input->post('provinsi_form_1a'));
            foreach ($query->result_array() as $row):
                $u = $row['iddinas'];
                $nama_provinsi_from = $row['provinsi'];
            endforeach;
        }

        if (isset($nama_provinsi_from)) {
            $data['nama_provinsi_from'] = $nama_provinsi_from;
        } else {
            $data['nama_provinsi_from'] = "";
        }
        $data['dinas_view'] = $this->dinas_model->dinas_view($u);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['form_1_view'] = $this->form_1_model->form_1_view($this->input->post('provinsi_form_1a'));
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $this->load->view('html/cetak_form_1a_view', $data);
    }

    public function form_1a_view_admin()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['form_1_view'] = $this->form_1_model->form_1_view_admin($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $this->load->view('html/form_1a_view_admin', $data);
    }

    public function form_imb_view()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');

        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['form_imb_view'] = $this->sejuta_rumah->form_imb_view($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }
        $this->load->view('html/form_imb_view', $data);
    }

    public function daftar_pengguna()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');

        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        if (($this->input->post('provinsi_select') == "1") or ($this->uri->segment(3) == "1")) {
            $data['provinsi_select'] = "1";
            $data['daftar_pengguna'] = $this->dinas_model->daftar_pengguna_provinsi($user_id);
        } else {
            $data['provinsi_select'] = "0";
            $data['daftar_pengguna'] = $this->dinas_model->daftar_pengguna_kabupaten($user_id);
        }
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $this->load->view('html/daftar_pengguna', $data);
    }

    public function sejuta_rumah_kabupaten_kota()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $user_id = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);

        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['form_imb_view'] = $this->sejuta_rumah->form_imb_lap($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $this->load->view('html/form_imb_lap', $data);
    }

    public function kecamatan()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['kecamatan_view'] = $this->master_model->kecamatan_view($this->session->userdata('idkabupaten_kota'));
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $this->load->view('html/kecamatan_view', $data);
    }

    public function form_non_imb_view()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['form_non_imb_view'] = $this->sejuta_rumah->form_non_imb_view($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['kabupaten_kota_p'] = $this->master_model->kabupaten_kota();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $this->load->view('html/form_non_imb_view', $data);
    }

    public function form_imb()
    {

        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idform_satu_juta_rumah')) == 0) {
            $idform_satu_juta_rumah = 0;
        } else {
            $idform_satu_juta_rumah = $this->input->post('idform_satu_juta_rumah');
        }
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($iddinas);
        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $idprovinsi = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $idkabupaten_kota = $this->session->userdata('idkabupaten_kota');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas')
        );
        $data['kabupaten_kota'] = $this->form_1_model->kabupaten_kota($idprovinsi);
        $data['form_imb_view_detail'] = $this->sejuta_rumah->form_imb_view_detail($form_1_data, $idform_satu_juta_rumah);
        $data['form_satu_juta_rumah_imb_view_detail'] = $this->sejuta_rumah->form_satu_juta_rumah_imb_view_detail($form_1_data, $idform_satu_juta_rumah);
        $data['form_satu_juta_rumah_unit_view_detail'] = $this->sejuta_rumah->form_satu_juta_rumah_unit_view_detail($form_1_data, $idform_satu_juta_rumah);
        $data['form_satu_juta_rumah_imb_count'] = $this->sejuta_rumah->form_satu_juta_rumah_imb_count($form_1_data, $idform_satu_juta_rumah);
        $data['form_satu_juta_rumah_unit_count'] = $this->sejuta_rumah->form_satu_juta_rumah_unit_count($form_1_data, $idform_satu_juta_rumah);
        //$data['kecamatan_combo'] = $this->sejuta_rumah->kecamatan_combo($form_1_data,$idform_satu_juta_rumah);
        $data['error'] = ' ';
        $this->load->view('html/form_imb', $data);
    }


    public function form_imb_cetak()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idpengembang')) == 0) {
            $idpengembang = 0;

        } else {
            $idpengembang = $this->input->post('idpengembang');

        }
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');

        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $idprovinsi = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $idkabupaten_kota = $this->session->userdata('idkabupaten_kota');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas')

        );
        $data['kabupaten_kota'] = $this->form_1_model->kabupaten_kota($idprovinsi);
        $data['form_pengembang_view_detail'] = $this->sejuta_rumah->form_pengembang_view_detail($form_1_data, $idpengembang);
        $data['form_pengembang_view'] = $this->sejuta_rumah->pengembang_view($form_1_data, $idpengembang);
        $data['pengembang_view_detail'] = $this->sejuta_rumah->pengembang_view_detail($form_1_data, $idpengembang);
        $data['pengembang_view_detail2'] = $this->sejuta_rumah->pengembang_view_detail2($form_1_data, $idpengembang);
        $data['form_satu_juta_rumah_unit_view_detail'] = $this->sejuta_rumah->form_satu_juta_rumah_unit_view_detail($form_1_data, $idpengembang);
        $data['form_satu_juta_rumah_imb_count'] = $this->sejuta_rumah->form_satu_juta_rumah_imb_count($form_1_data, $idpengembang);
        $data['form_satu_juta_rumah_unit_count'] = $this->sejuta_rumah->form_satu_juta_rumah_unit_count($form_1_data, $idpengembang);
        //$data['kecamatan_combo'] = $this->sejuta_rumah->kecamatan_combo($form_1_data,$idpengembang);
        $data['error'] = ' ';
        $this->load->view('html/form_pengembang_lihat', $data);
    }

    public function form_satu_juta_rumah_update_bulan()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $data = array(
            'tanggal' => $this->input->post('tanggal'),
            'bulan' => $this->input->post('bulan')
        );
        $this->sejuta_rumah->form_satu_juta_rumah_update($data, $this->input->post('idform_satu_juta_rumah'));
        header('Location: ' . BASE_URL('/main/form_imb'));
        exit;
    }

    public function ganti_password()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $query = $this->db->query('SELECT * FROM dinas where iddinas=' . $this->input->post('iddinas'));//  and password=sha1("'.$this->input->post('current_password').'")'
        if ($query->num_rows() >= 1) {
            foreach ($query->result_array() as $row):
                $u = $row['id_privilage'];
            endforeach;
            $data = array(
                'password' => sha1($this->input->post('pw_baru'))
            );
            $this->master_model->ganti_password($data, $this->input->post('iddinas'));
            header('Location: ' . BASE_URL('/login/logout_user/' . $u . '/' . $this->session->userdata('iddinas')));
            exit;
        } else {
            header('Location: ' . BASE_URL('/main/dinas_view/0'));
            exit;
        }
    }

    public function update_profile()
    {
        if ($this->input->post('priv') == "2") {
            $data = array(
                'wakil_gubernur' => $this->input->post('nama_wakil_gubernur'),
                'gubernur' => $this->input->post('nama_gubernur'),
                'letak_geografis' => $this->input->post('letak_geografis'),
                'luas_wilayah_daratan' => $this->input->post('luas_wilayah_darat'),
                'jumlah_penduduk' => $this->input->post('jumlah_penduduk'),
                'luas_wilayah_lautan' => $this->input->post('luas_wilayah_laut'),
                'pertumbuhan_penduduk' => $this->input->post('pertumbuhan_penduduk'),
                'tingkat_kepadatan_penduduk' => $this->input->post('kepadatan_penduduk'),
                'jumlah_penduduk_miskin_kota' => $this->input->post('jumlah_penduduk_miskin_kota'),
                'jumlah_penduduk_miskin_desa' => $this->input->post('jumlah_penduduk_miskin_desa')
            );
            $this->master_model->provinsi_update($data, $this->input->post('idprovinsi'));
        } elseif ($this->input->post('priv') == "3") {
            $data = array(
                'wakil_kepala_daerah' => $this->input->post('nama_wakil_gubernur'),
                'kepala_daerah' => $this->input->post('nama_gubernur'),
                'letak_geografis' => $this->input->post('letak_geografis'),
                'luas_wilayah_daratan' => $this->input->post('luas_wilayah_darat'),
                'jumlah_penduduk' => $this->input->post('jumlah_penduduk'),
                'luas_wilayah_lautan' => $this->input->post('luas_wilayah_laut'),
                'pertumbuhan_penduduk' => $this->input->post('pertumbuhan_penduduk'),
                'tingkat_kepadatan_penduduk' => $this->input->post('kepadatan_penduduk'),
                'jumlah_penduduk_miskin_kota' => $this->input->post('jumlah_penduduk_miskin_kota'),
                'jumlah_penduduk_miskin_desa' => $this->input->post('jumlah_penduduk_miskin_desa')
            );
            $this->master_model->kabupaten_kota_update($data, $this->input->post('idkabupaten_kota'));
        }

        header('Location: ' . BASE_URL('/main/profil'));
        exit;
    }

    public function pengembang_update_bulan()
    {
        $data = array(
            'tanggal' => $this->input->post('tanggal'),
            'bulan' => $this->input->post('bulan'),
            'tahun' => $this->input->post('tahun'),
            'pengembang' => $this->input->post('nama_pengembang'),
            'alamat' => $this->input->post('alamat'),
            'idpengembang' => $this->input->post('idpengembang'),
            'asosiasi' => $this->input->post('asosiasi')
        );
        $this->sejuta_rumah->pengembang_update($data, $this->input->post('idpengembang'));
        header('Location: ' . BASE_URL('/main/form_pen_insert'));
        exit;
    }

    public function perbankan_update_bulan()
    {
        $data = array(
            'tanggal' => $this->input->post('tanggal'),
            'bulan' => $this->input->post('bulan'),
            'nama' => $this->input->post('nama_perbankan'),
            'alamat' => $this->input->post('alamat'),
            'idperbankan' => $this->input->post('idperbankan'),
            'no_kontrak' => $this->input->post('no_kontrak')
        );
        $this->sejuta_rumah->perbankan_update($data, $this->input->post('idperbankan'));
        header('Location: ' . BASE_URL('/main/form_per_insert'));
        exit;
    }

    public function form_satu_juta_rumah_update_approval()
    {
        /////////
        $idform_1 = $this->input->post('idform_satu_juta_rumah');
        $iddinas = $this->session->userdata('iddinas');
        ///

        if (isset($_FILES['image2'])) {
            $config['upload_path'] = './assets/uploads/' . $iddinas . '/' . $idform_1;
            $config['allowed_types'] = 'gif|jpg|png|bmp';
            $config['file_name'] = $idform_1 . "_penyetujuan_imb";
            $this->load->library('upload', $config);
            if (!is_dir('./assets/uploads')) {
                mkdir('./assets/uploads', 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas)) {
                mkdir('./assets/uploads/' . $iddinas, 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas . '/' . $idform_1)) {
                mkdir('./assets/uploads/' . $iddinas . '/' . $idform_1, 0777, true);
            }
            $arr = explode('.', $_FILES["image2"]['name']);
            unset($arr[0]);
            $filebaru = "./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . "_penyetujuan_imb" . '.' . implode($arr);
            if (file_exists("./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . "_penyetujuan_imb" . '.' . implode($arr)) == false) {
                if ($this->upload->do_upload('image2')) {
                    $data = array('status' => 'Selesai', 'file_approval' => $filebaru);

                    $this->sejuta_rumah->form_satu_juta_rumah_update($data, $idform_1);
                    //$this->form_1_model->Form1_update($data,$idform_1);
                }
            }
            header('Location: ' . BASE_URL('/main/form_imb_view'));
            exit;
        }
        /////////
    }

    public function form_satu_juta_rumah_update_approval_non()
    {
        /////////
        $idform_1 = $this->input->post('idform_satu_juta_rumah');
        $iddinas = $this->session->userdata('iddinas');
        ///

        if (isset($_FILES['image2'])) {
            $config['upload_path'] = './assets/uploads/' . $iddinas . '/' . $idform_1;
            $config['allowed_types'] = 'gif|jpg|png|bmp';
            $config['file_name'] = $idform_1 . "_penyetujuan_imb";
            $this->load->library('upload', $config);
            if (!is_dir('./assets/uploads')) {
                mkdir('./assets/uploads', 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas)) {
                mkdir('./assets/uploads/' . $iddinas, 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas . '/' . $idform_1)) {
                mkdir('./assets/uploads/' . $iddinas . '/' . $idform_1, 0777, true);
            }
            $arr = explode('.', $_FILES["image2"]['name']);
            unset($arr[0]);
            $filebaru = "./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . "_penyetujuan_imb" . '.' . implode($arr);
            if (file_exists("./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . "_penyetujuan_imb" . '.' . implode($arr)) == false) {
                if ($this->upload->do_upload('image2')) {
                    $data = array('status' => 'Selesai', 'file_approval' => $filebaru);

                    $this->sejuta_rumah->form_satu_juta_rumah_update($data, $idform_1);
                    //$this->form_1_model->Form1_update($data,$idform_1);
                }
            }
        }
        header('Location: ' . BASE_URL('/main/form_non_imb_view'));
        exit;
        /////////
    }

    public function form_satu_juta_rumah_update_bulan_non_imb()
    {
        $data = array(
            'tanggal' => $this->input->post('tanggal'),
            'bulan' => $this->input->post('bulan')
        );
        $this->sejuta_rumah->form_satu_juta_rumah_update($data, $this->input->post('idform_satu_juta_rumah'));
        header('Location: ' . BASE_URL('/main/form_non_imb'));
        exit;
    }

    public function rtlh()
    {

        $data['user_id'] = $this->session->userdata('iddinas');
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $user_id = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['tahun'] = $this->input->post('tahun');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $data['tahun'] = $tahun;
        $data['rtlh_kabupaten_kota'] = $this->form_1_model->rtlh_kabupaten_kota($user_id, $tahun, $this->session->userdata('idprovinsi'));
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $this->load->view('html/rtlh_kabupaten_kota', $data);
    }


    public function rtlh_kecamatan()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['idkabupaten_kota'] = $this->session->userdata('idkabupaten_kota');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['tahun'] = $this->input->post('tahun');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $data['rtlh_kecamatan'] = $this->form_1_model->rtlh_kecamatan($user_id, $tahun, $user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $this->load->view('html/rtlh_kecamatan', $data);
    }

    public function status_kepemilikan_rumah()
    {

        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');

        $data['iddinas'] = $this->session->userdata('iddinas');

        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');

        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $data['status_kepemilikan_rumah'] = $this->form_1_model->status_kepemilikan_rumah($user_id, $tahun, $this->session->userdata('idprovinsi'));
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);

        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $this->load->view('html/status_kepemilikan_rumah_provinsi', $data);
    }

    public function status_kepemilikan_rumah_kabupaten_kota()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);

        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['idkabupaten_kota'] = $this->session->userdata('idkabupaten_kota');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $data['status_kepemilikan_rumah_kabupaten_kota'] = $this->form_1_model->status_kepemilikan_rumah_kabupaten_kota($user_id, $tahun, $this->session->userdata('iddinas'));
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }
        $this->load->view('html/status_kepemilikan_rumah_kabupaten_kota', $data);
    }

    public function form_1b_view()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');

        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $u = $this->session->userdata('iddinas');
        if ($this->session->userdata('idprivilage') == 1) {
            if ((strlen($this->input->post('kabupaten_filter')) == 0)) {
                $u = $user_id;
            } else {
                $query = $this->db->query('SELECT iddinas,kabupaten_kota FROM kabupaten_kota where idkabupaten_kota=' . $this->input->post('kabupaten_filter'));
                foreach ($query->result_array() as $row):
                    $u = $row['iddinas'];
                    $nama_kabkot_from = $row['kabupaten_kota'];
                endforeach;
            }
            if (isset($nama_kabkot_from)) {
                $data['nama_kabkot_from'] = $nama_kabkot_from;
            } else {
                $data['nama_kabkot_from'] = "";
            }
        }
        if ($this->session->userdata('idprivilage') == 2) {
            if (strlen($this->input->post('kabupaten_kota_filter')) == 0) {
                $u = $user_id;
            } else {
                $query = $this->db->query('SELECT iddinas,kabupaten_kota FROM kabupaten_kota where idkabupaten_kota=' . $this->input->post('kabupaten_kota_filter'));
                foreach ($query->result_array() as $row):
                    $u = $row['iddinas'];
                    $nama_kabkot_from = $row['kabupaten_kota'];
                endforeach;
            }
            if (isset($nama_kabkot_from)) {
                $data['nama_kabkot_from'] = $nama_kabkot_from;
            } else {
                $data['nama_kabkot_from'] = "-";
            }
        }
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');

        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view2($u);
        $data['dinas_viewx'] = $this->dinas_model->dinas_view($u);
        $data['form_1_view'] = $this->form_1_model->form_1_view($u);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);

        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $this->load->view('html/form_1b_view', $data);
    }

    public function form_1b_view_admin()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');

        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view2($user_id);
        $data['form_1_view_admin2'] = $this->form_1_model->form_1_view_admin2(0);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $this->load->view('html/form_1b_view_admin', $data);
    }

    public function extra_search()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $this->load->view('html/extra_search');
    }


    public function upload_gambar()
    {
        $idform_1 = $this->input->post('idform_1');
        $idprovinsi = $this->session->userdata('idprovinsi');
        ///
        $config['upload_path'] = './assets/upload/' . $idprovinsi . '/' . $idform_1;
        $config['allowed_types'] = 'gif|jpg|png|bmp';
        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        if (!is_dir('./assets/upload')) {
            mkdir('./assets/upload', 0777, true);
        }
        if (!is_dir('./assets/upload/' . $idprovinsi)) {
            mkdir('./assets/upload/' . $idprovinsi, 0777, true);
        }
        if (!is_dir('./assets/upload/' . $idprovinsi . '/' . $idform_1)) {
            mkdir('./assets/upload/' . $idprovinsi . '/' . $idform_1, 0777, true);
        }
        $arr = explode('.', $_FILES["userfile"]['name']);
        unset($arr[0]);
        $filebaru = "../assets/upload/" . $idprovinsi . '/' . $idform_1 . "/" . $idform_1 . '.' . implode($arr);
        if (file_exists("./assets/upload/" . $idprovinsi . '/' . $idform_1 . "/" . $idform_1 . '.' . implode($arr)) == false) {
            if (!$this->upload->do_upload('userfile')) {
                echo $this->upload->display_errors();
            }//
            else {
                $data = array('idform_1' => $idform_1, 'struktur_dinas' => $filebaru);
                $this->form_1_model->Form1k1_insert($data);
                header('Location: ' . BASE_URL('/main/form_1a'));
                exit;
            }
        }
        ////
    }

    public function fimb_insert()
    {
        $data = array(
            'idform_satu_juta_rumah' => $this->input->post('idform_satu_juta_rumah'),
            'perumahan' => $this->input->post('perumahan'),
            'nama_pengembang' => $this->input->post('nama_pengembang'),
            'alamat' => $this->input->post('alamat'),
            'bentuk_rumah' => $this->input->post('bentuk_rumah'),
            'luas' => $this->input->post('luas'),
            'tipe' => $this->input->post('tipe'),
            'jumlah_mbr' => $this->input->post('jumlah_mbr'),
            'jumlah_non_mbr' => $this->input->post('jumlah_non_mbr')
        );
        $this->sejuta_rumah->fimb_insert($data);
        header('Location: ' . BASE_URL('/main/form_imb'));
        exit;
    }

    public function kabupaten_kota_insert()
    {
        $data2 = array(
            'nama_dinas' => $this->input->post('kab_kot'),
            'password' => sha1("password"),
            'id_privilage' => 3
        );
        $this->master_model->dinas_insert($data2);
        $query = $this->db->query('SELECT iddinas as id FROM dinas where nama_dinas="' . $this->input->post('kab_kot') . '"');
        foreach ($query->result_array() as $row):
            $id = $row['id'];
        endforeach;
        $data = array(
            'idprovinsi' => $this->input->post('idprovinsi'),
            'kode' => $this->input->post('kode_kab_kot'),
            'iddinas' => $id,
            'kabupaten_kota' => $this->input->post('kab_kot')
        );
        $this->master_model->kabupaten_kota_insert($data);
        header('Location: ' . BASE_URL('/main/kabupaten_kota'));
        exit;
    }

    public function kabupaten_kota_update()
    {
        $data = array(
            'kode' => $this->input->post('kode_kab_kot'),
            'kabupaten_kota' => $this->input->post('kab_kot')
        );
        $this->master_model->kabupaten_kota_update($data, $this->input->post('idkabupaten_kota'));
        header('Location: ' . BASE_URL('/main/kabupaten_kota'));
        exit;
    }

    public function kirim_pesan()
    {
        $data = array(
            'judul' => $this->input->post('judul_pesan'),
            'iddinas' => $this->input->post('iddinas'),
            'tanggal' => date('Y-m-d h:i')
        );
        $this->master_model->kirim_pesan1($data);

        $query = $this->db->query('SELECT max(idjudul) as id FROM pesan_judul where iddinas=' . $this->input->post('iddinas'));
        foreach ($query->result_array() as $row):
            $id = $row['id'];
        endforeach;
        $data2 = array(
            'pesan' => $this->input->post('isipesan'),
            'idjudul' => $id,
            'waktu' => date('Y-m-d h:i'),
            'status_pesan' => $this->input->post('status_pesan')

        );
        $this->master_model->kirim_pesan2($data2);
        header('Location: ' . BASE_URL('/main/kontak'));
        exit;
    }

    public function fimb_update()
    {
        $data = array(
            'idform_satu_juta_rumah' => $this->input->post('idform_satu_juta_rumah'),
            'perumahan' => $this->input->post('perumahan'),
            'nama_pengembang' => $this->input->post('nama_pengembang'),
            'alamat' => $this->input->post('alamat'),
            'bentuk_rumah' => $this->input->post('bentuk_rumah'),
            'luas' => $this->input->post('luas'),
            'tipe' => $this->input->post('tipe'),
            'jumlah_mbr' => $this->input->post('jumlah_mbr'),
            'jumlah_non_mbr' => $this->input->post('jumlah_non_mbr')
        );
        $this->sejuta_rumah->fimb_update($data, $this->input->post('idform_satu_juta_rumah_imb'));
        header('Location: ' . BASE_URL('/main/form_imb'));
        exit;
    }

    public function hapus_fsejuta_rumah()
    {
        $this->sejuta_rumah->hapus_fsejuta_rumah($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_non_imb_view'));
        exit;
    }

    public function hapus_pesan_admin()
    {
        $this->master_model->hapus_pesan_admin($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/kontak_admin'));
        exit;
    }

    public function hapus_perbankan_view()
    {
        $this->sejuta_rumah->hapus_perbankan($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_perbankan_view'));
        exit;
    }

    public function hapus_fpengembang_view()
    {
        $this->sejuta_rumah->hapus_fpengembang_view($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_pengembang_view'));
        exit;
    }

    public function hapus_fimb()
    {
        $data['hapus_form_1'] = $this->sejuta_rumah->hapus_fimb($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_imb'));
        exit;
    }

    //

    public function fimb_unit_insert()
    {
        $data = array(
            'idform_satu_juta_rumah' => $this->input->post('idform_satu_juta_rumah'),
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'jumlah_mbr' => $this->input->post('jumlah_mbr2'),
            'jumlah_non_mbr' => $this->input->post('jumlah_non_mbr2')
        );
        $this->sejuta_rumah->fimb_unit_insert($data);
        header('Location: ' . BASE_URL('/main/form_imb'));
        exit;
    }

    public function fpengembang_mbr_insert()
    {
        $data = array(
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'idpengembang' => $this->input->post('idpengembang'),
            'nama_perusahaan' => $this->input->post('nama_perusahaan'),
            'alamat' => $this->input->post('alamat'),
            'nama_pengembang' => $this->input->post('nama_pengembang'),
            'bentuk_rumah' => $this->input->post('bentuk_rumah'),
            'luas' => $this->input->post('luas'),
            'tipe' => $this->input->post('tipe'),
            'rencana' => $this->input->post('rencana'),
            'realisasi' => $this->input->post('realisasi'),
            'jenis' => 'MBR',
            'harga' => $this->input->post('harga')
        );
        $this->sejuta_rumah->fpengembang_mbr_insert($data);
        header('Location: ' . BASE_URL('/main/form_pen_insert'));
        exit;
    }

    public function fperbankan_mbr_insert()
    {
        $data = array(
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'idperbankan' => $this->input->post('idperbankan'),
            'pengembang_perorangan' => $this->input->post('pengembang_perorangan'),
            'nama_perumahan' => $this->input->post('nama_perumahan'),
            'lokasi' => $this->input->post('lokasi'),
            'bentuk_rumah' => $this->input->post('bentuk_rumah'),
            'jenis_rumah' => $this->input->post('jenis_rumah'),
            'rencana' => $this->input->post('rencana'),
            'realisasi' => $this->input->post('realisasi'),
            'harga' => $this->input->post('harga'),
            'mbr' => $this->input->post('mbr'),
            'non_mbr' => $this->input->post('non_mbr'),
            'kpr' => $this->input->post('kpr')
        );
        $this->sejuta_rumah->fperbankan_mbr_insert($data);
        header('Location: ' . BASE_URL('/main/form_per_insert'));
        exit;
    }

    public function fpengembang_mbr_insert2()
    {
        $data = array(
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota2'),
            'idpengembang' => $this->input->post('idpengembang2'),
            'nama_perusahaan' => $this->input->post('nama_perusahaan2'),
            'alamat' => $this->input->post('alamat2'),
            'nama_pengembang' => $this->input->post('nama_pengembang2'),
            'bentuk_rumah' => $this->input->post('bentuk_rumah2'),
            'luas' => $this->input->post('luas2'),
            'tipe' => $this->input->post('tipe2'),
            'rencana' => $this->input->post('rencana2'),
            'realisasi' => $this->input->post('realisasi2'),
            'jenis' => 'Non MBR',
            'harga' => $this->input->post('harga2')
        );
        $this->sejuta_rumah->fpengembang_mbr_insert($data);
        header('Location: ' . BASE_URL('/main/form_pen_insert'));
        exit;
    }

    public function fnon_imb_unit_insert()
    {
        $data = array(
            'idform_satu_juta_rumah' => $this->input->post('idform_satu_juta_rumah'),
            'idkecamatan' => $this->input->post('idkecamatan'),
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'jumlah_mbr' => $this->input->post('jumlah_mbr2'),
            'jumlah_non_mbr' => $this->input->post('jumlah_non_mbr2')
        );
        $this->sejuta_rumah->fimb_unit_insert($data);
        header('Location: ' . BASE_URL('/main/form_non_imb'));
        exit;
    }

    public function fimb_unit_update()
    {
        $data = array(
            'idform_satu_juta_rumah' => $this->input->post('idform_satu_juta_rumah'),
            'idkecamatan' => $this->input->post('idkecamatan'),
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'jumlah_mbr' => $this->input->post('jumlah_mbr2'),
            'jumlah_non_mbr' => $this->input->post('jumlah_non_mbr2')
        );
        $this->sejuta_rumah->fimb_unit_update($data, $this->input->post('idform_satu_juta_rumah_unit'));
        header('Location: ' . BASE_URL('/main/form_imb'));
        exit;
    }

    public function fnon_imb_unit_update()
    {
        $data = array(
            'idform_satu_juta_rumah' => $this->input->post('idform_satu_juta_rumah'),
            'idkecamatan' => $this->input->post('idkecamatan'),
            //'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'jumlah_mbr' => $this->input->post('jumlah_mbr2'),
            'jumlah_non_mbr' => $this->input->post('jumlah_non_mbr2')
        );
        $this->sejuta_rumah->fimb_unit_update($data, $this->input->post('idform_satu_juta_rumah_unit'));
        header('Location: ' . BASE_URL('/main/form_non_imb'));
        exit;
    }


    public function hapus_fimb_unit()
    {
        $this->sejuta_rumah->hapus_unit_fimb($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_imb'));
        exit;
    }

    public function hapus_pengembang_1()
    {
        $this->sejuta_rumah->hapus_pengembang_1($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_pen_insert'));
        exit;
    }

    public function hapus_fnonimb_unit()
    {
        $this->sejuta_rumah->hapus_unit_fimb($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_non_imb'));
        exit;
    }

    public function kecamatan_insert()
    {
        $data = array(
            'kode' => $this->input->post('kode'),
            'kecamatan' => $this->input->post('Kecamatan_1'),
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota')
        );
        $this->master_model->kecamatan_insert($data);
        header('Location: ' . BASE_URL('/main/kecamatan'));
        exit;
    }

    public function kecamatan_update()
    {
        $data = array(
            'kode' => $this->input->post('kode'),
            'kecamatan' => $this->input->post('Kecamatan_1'),
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota')
        );
        $this->master_model->kecamatan_update($data, $this->input->post('idkecamatan'));
        header('Location: ' . BASE_URL('/main/kecamatan'));
        exit;
    }

    public function Form1k2_a_insert()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'uraian1' => $this->input->post('uraian1'),
            'k221' => $this->input->post('k221'),
            'k231' => $this->input->post('k231')
        );
        $this->form_1_model->Form1k2_a_insert($data);
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#k2_a_div'));
        exit;
    }

    public function Form1k2_a_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'uraian1' => $this->input->post('uraian1'),
            'k221' => $this->input->post('k221'),
            'k231' => $this->input->post('k231')
        );
        $this->form_1_model->Form1k2_a_insert($data);
        if ($this->input->post('jenisk2') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_1'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_1'));
        }
        exit;
    }

    public function Form1k2_b_insert()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'jenis_kegiatan_urusan_pkp_2' => $this->input->post('jenis_kegiatan_urusan_pkp_2'),
            'ta_a_vol_unit_3' => $this->input->post('ta_a_vol_unit_3'),
            'ta_a_biaya_4' => $this->input->post('ta_a_biaya_4'),
            'ta_a_vol_unit_5' => $this->input->post('ta_a_vol_unit_5'),
            'ta_a_biaya_6' => $this->input->post('ta_a_biaya_6')
        );
        $this->form_1_model->Form1k2_b_insert($data);
        if ($this->input->post('jenisk2b') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_1'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_1'));
        }
        exit;
    }

    public function Form1k2_b_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'jenis_kegiatan_urusan_pkp_2' => $this->input->post('jenis_kegiatan_urusan_pkp_2'),
            'ta_a_vol_unit_3' => $this->input->post('ta_a_vol_unit_3'),
            'ta_a_biaya_4' => $this->input->post('ta_a_biaya_4'),
            'ta_a_vol_unit_5' => $this->input->post('ta_a_vol_unit_5'),
            'ta_a_biaya_6' => $this->input->post('ta_a_biaya_6')
        );
        $this->form_1_model->Form1k2_b_insert($data);
        if ($this->input->post('jenisk2b') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_2'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_2'));
        }
        exit;
    }

    public function Form1k3_insert()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'isi_1a' => $this->input->post('isi_1a'),
            'isi_1b' => $this->input->post('isi_1b'),
            'isi_1c' => $this->input->post('isi_1c'),
            'isi_1d' => $this->input->post('isi_1d'),
            'isi_1e' => $this->input->post('isi_1e'),
            'isi_1f' => $this->input->post('isi_1f'),
            'isi_1f_keterangan' => $this->input->post('isi_1f_keterangan'),
            'isi_2c_keterangan' => $this->input->post('isi_2c_keterangan'),
            'isi_2d_keterangan' => $this->input->post('isi_2d_keterangan'),
            'isi_2e_keterangan' => $this->input->post('isi_2e_keterangan'),
            'isi_2a' => $this->input->post('isi_2a'),
            'isi_2b' => $this->input->post('isi_2b'),
            'isi_2c' => $this->input->post('isi_2c'),
            'isi_2d' => $this->input->post('isi_2d'),
            'isi_2e' => $this->input->post('isi_2e')
        );
        $this->form_1_model->Form1k3_insert($data);
        if ($this->input->post('jenisk3') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_3'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_3'));
        }
        exit;
    }

    public function Form1k3_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'isi_1a' => $this->input->post('isi_1a'),
            'isi_1b' => $this->input->post('isi_1b'),
            'isi_1c' => $this->input->post('isi_1c'),
            'isi_1d' => $this->input->post('isi_1d'),
            'isi_1e' => $this->input->post('isi_1e'),
            'isi_1f' => $this->input->post('isi_1f'),
            'isi_1f_keterangan' => $this->input->post('isi_1f_keterangan'),
            'isi_2c_keterangan' => $this->input->post('isi_2c_keterangan'),
            'isi_2d_keterangan' => $this->input->post('isi_2d_keterangan'),
            'isi_2e_keterangan' => $this->input->post('isi_2e_keterangan'),
            'isi_2a' => $this->input->post('isi_2a'),
            'isi_2b' => $this->input->post('isi_2b'),
            'isi_2c' => $this->input->post('isi_2c'),
            'isi_2d' => $this->input->post('isi_2d'),
            'isi_2e' => $this->input->post('isi_2e')
        );
        $this->form_1_model->Form1k3_insert($data);
        if ($this->input->post('jenisk3') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_3'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_3'));
        }
        exit;
    }

    public function Form1k4_1_insert()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'k4141' => $this->input->post('k4141'),
            'k4142' => $this->input->post('k4142'),
            'k4143' => $this->input->post('k4143'),
            'k4144' => $this->input->post('k4144'),
            'k4145' => $this->input->post('k4145'),
            'k4151' => $this->input->post('k4151'),
            'k4152' => $this->input->post('k4152'),
            'k4153' => $this->input->post('k4153'),
            'k4154' => $this->input->post('k4154'),
            'k4155' => $this->input->post('k4155'),
            'status1' => $this->input->post('status1'),
            'status2' => $this->input->post('status2'),
            'status3' => $this->input->post('status3'),
            'status4' => $this->input->post('status4'),
            'status5' => $this->input->post('status5')
        );
        $this->form_1_model->Form1k4_1_insert($data);
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        exit;
    }

    public function Form1k4_1_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'k4141' => $this->input->post('k4141'),
            'k4142' => $this->input->post('k4142'),
            'k4143' => $this->input->post('k4143'),
            'k4144' => $this->input->post('k4144'),
            'k4145' => $this->input->post('k4145'),
            'k4151' => $this->input->post('k4151'),
            'k4152' => $this->input->post('k4152'),
            'k4153' => $this->input->post('k4153'),
            'k4154' => $this->input->post('k4154'),
            'k4155' => $this->input->post('k4155'),
            'status1' => $this->input->post('status1'),
            'status2' => $this->input->post('status2'),
            'status3' => $this->input->post('status3'),
            'status4' => $this->input->post('status4'),
            'status5' => $this->input->post('status5')
        );
        $this->form_1_model->Form1k4_1_insert($data);
        if ($this->input->post('jenisk41') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_2_insert()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'k4231' => $this->input->post('k4231'),
            'k4232' => $this->input->post('k4232'),
            'k4241' => $this->input->post('k4241'),
            'k4242' => $this->input->post('k4242'),
            'jenis1' => $this->input->post('jenis1'),
            'jenis2' => $this->input->post('jenis2')
        );
        $this->form_1_model->Form1k4_2_insert($data);
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        exit;
    }

    public function Form1k4_2_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'k4231' => $this->input->post('k4231'),
            'k4232' => $this->input->post('k4232'),
            'k4241' => $this->input->post('k4241'),
            'k4242' => $this->input->post('k4242'),
            'jenis1' => $this->input->post('jenis1'),
            'jenis2' => $this->input->post('jenis2')
        );
        $this->form_1_model->Form1k4_2_insert($data);
        if ($this->input->post('jenisk42') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_3_insert()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'k4331' => $this->input->post('k4331'),
            'k4332' => $this->input->post('k4332'),
            'k4341' => $this->input->post('k4341'),
            'k4342' => $this->input->post('k4342'),
            'fungsi1' => $this->input->post('fungsi1'),
            'fungsi2' => $this->input->post('fungsi2')
        );
        $this->form_1_model->Form1k4_3_insert($data);
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        exit;
    }

    public function Form1k4_3_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'k4331' => $this->input->post('k4331'),
            'k4332' => $this->input->post('k4332'),
            'k4341' => $this->input->post('k4341'),
            'k4342' => $this->input->post('k4342'),
            'fungsi1' => $this->input->post('fungsi1'),
            'fungsi2' => $this->input->post('fungsi2')
        );
        $this->form_1_model->Form1k4_3_insert($data);
        if ($this->input->post('jenisk43') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_4_insert()
    {
        $query = $this->db->query('SELECT * FROM kabupaten_kota where idprovinsi=' . $this->input->post('idprovinsi_') . ' order by idkabupaten_kota');
        if ($query->num_rows() >= 1) {
            $xx = 0;
            foreach ($query->result_array() as $row):
                $xx++;
                $data = array(
                    'idform_1' => $this->input->post('idform_1'),
                    'idkabupaten_kota' => $row["idkabupaten_kota"],
                    'jumlah_rumah_4' => $this->input->post('jumlah_rumah_4' . $xx),
                    'sumber_data_5' => $this->input->post('sumber_data_5' . $xx)
                );
                $this->form_1_model->Form1k4_4_insert($data);
            endforeach;
        }
        if ($this->input->post('jenisk44') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_4_insert_kabupaten()
    {
        $query = $this->db->query('SELECT * FROM kecamatan where idkabupaten_kota=' . $this->input->post('idkabupaten_kota') . ' order by idkecamatan');
        if ($query->num_rows() >= 1) {
            $xx = 0;
            foreach ($query->result_array() as $row):
                $xx++;
                $data = array(
                    'idform_1' => $this->input->post('idform_1'),
                    'idkabupaten_kota' => $row["idkabupaten_kota"],
                    'idkecamatan' => $row["idkecamatan"],
                    'jumlah_rumah_4' => $this->input->post('jumlah_rumah_4' . $xx),
                    'sumber_data_5' => $this->input->post('sumber_data_5' . $xx)
                );
                $this->form_1_model->Form1k4_4_insert($data);
            endforeach;
        }
        if ($this->input->post('jenisk44') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k5_tambah()
    {
        $query = $this->db->query('SELECT * FROM kecamatan where idkabupaten_kota=' . $this->input->post('idkabupaten_kota') . ' order by idkecamatan');
        if ($query->num_rows() >= 1) {
            $xx = 0;
            foreach ($query->result_array() as $row):
                $xx++;
                $data = array(
                    'idform_1' => $this->input->post('idform_1'),
                    'idkabupaten_kota' => $row["idkabupaten_kota"],
                    'idkecamatan' => $row["idkecamatan"],
                    'jumlah_kk_rt_3' => $this->input->post('jumlah_kk_rt_3' . $xx),
                    'jumlah_rtlh_versi_bdt_4' => $this->input->post('jumlah_rtlh_versi_bdt_4' . $xx),
                    'jumlah_rtlh_verifikasi_pemda_5' => $this->input->post('jumlah_rtlh_verifikasi_pemda_5' . $xx),
                    'sumber_data_6' => $this->input->post('sumber_data_6' . $xx)
                );
                $this->form_1_model->Form1k5_insert($data);
            endforeach;
        }
        if ($this->input->post('jenisk5') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_5'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_5'));
        }
        exit;
    }

    public function Form1k6_tambah()
    {
        $query = $this->db->query('SELECT * FROM kecamatan where idkabupaten_kota=' . $this->input->post('idkabupaten_kota') . ' order by idkecamatan');
        if ($query->num_rows() >= 1) {
            $xx = 0;
            foreach ($query->result_array() as $row):
                $xx++;
                $data = array(
                    'idform_1' => $this->input->post('idform_1'),
                    'idkabupaten_kota' => $row["idkabupaten_kota"],
                    'idkecamatan' => $row["idkecamatan"],
                    'luas_wilayah_kumuh_4' => $this->input->post('luas_wilayah_kumuh_4' . $xx),
                    'jumlah_rtlh_dalam_wilayah_kumuh_5' => $this->input->post('jumlah_rtlh_dalam_wilayah_kumuh_5' . $xx),
                    'sumber_data_6' => $this->input->post('sumber_data_6' . $xx)
                );
                $this->form_1_model->Form1k6_insert($data);
            endforeach;
        }
        if ($this->input->post('jenisk6') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_6'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_6'));
        }
        exit;
    }

    public function Form1k4_4_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'idkecamatan' => $this->input->post('idkecamatan_select'),
            'jumlah_rumah_3' => $this->input->post('jumlah_rumah_3'),
            'jumlah_rumah_4' => $this->input->post('jumlah_rumah_4'),
            'sumber_data_5' => $this->input->post('sumber_data_5'),
            'idkelurahan' => $this->input->post('idkelurahan')
        );
        $this->form_1_model->Form1k4_4_insert($data);
        if ($this->input->post('jenisk44') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_5_insert()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'non_mbr_2' => $this->input->post('non_mbr_2'),
            'mbr_3' => $this->input->post('mbr_3'),
            'non_mbr_4' => $this->input->post('non_mbr_4'),
            'mbr_5' => $this->input->post('mbr_5'),
            'non_mbr_6' => $this->input->post('non_mbr_6'),
            'mbr_7' => $this->input->post('mbr_7'),
            'sumber_data_8' => $this->input->post('sumber_data_8')
        );
        $this->form_1_model->Form1k4_5_insert($data);
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        exit;
    }

    public function Form1k4_5_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'non_mbr_2' => $this->input->post('non_mbr_2'),
            'mbr_3' => $this->input->post('mbr_3'),
            'non_mbr_4' => $this->input->post('non_mbr_4'),
            'mbr_5' => $this->input->post('mbr_5'),
            'non_mbr_6' => $this->input->post('non_mbr_6'),
            'mbr_7' => $this->input->post('mbr_7'),
            'sumber_data_8' => $this->input->post('sumber_data_8')
        );
        $this->form_1_model->Form1k4_5_insert($data);
        if ($this->input->post('jenisk45') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k5_insert()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'idkecamatan' => $this->input->post('idkecamatan'),
            'jumlah_kk_rt_3' => $this->input->post('jumlah_kk_rt_3'),
            'jumlah_rtlh_versi_bdt_4' => $this->input->post('jumlah_rtlh_versi_bdt_4'),
            'jumlah_rtlh_verifikasi_pemda_5' => $this->input->post('jumlah_rtlh_verifikasi_pemda_5'),
            'sumber_data_6' => $this->input->post('sumber_data_6')
        );
        $this->form_1_model->Form1k5_insert($data);
        if ($this->input->post('jenisk5') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_5'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_5'));
        }
        exit;
    }

    public function Form1k5_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'idkecamatan' => $this->input->post('idkecamatan'),
            'jumlah_kk_rt_3' => $this->input->post('jumlah_kk_rt_3'),
            'jumlah_rtlh_versi_bdt_4' => $this->input->post('jumlah_rtlh_versi_bdt_4'),
            'jumlah_rtlh_verifikasi_pemda_5' => $this->input->post('jumlah_rtlh_verifikasi_pemda_5'),
            'sumber_data_6' => $this->input->post('sumber_data_6')
        );
        $this->form_1_model->Form1k5_insert($data);
        if ($this->input->post('jenisk5') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_5'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_5'));
        }
        exit;
    }

    public function Form1k6_insert()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'luas_wilayah_kumuh_4' => $this->input->post('luas_wilayah_kumuh_4'),
            'jumlah_rtlh_dalam_wilayah_kumuh_5' => $this->input->post('jumlah_rtlh_dalam_wilayah_kumuh_5'),
            'sumber_data_6' => $this->input->post('sumber_data_6')
        );
        $this->form_1_model->Form1k6_insert($data);
        $query = $this->db->query('SELECT max(idform_1_k6) as idform_1_k6 FROM form_1_k6 where idform_1="' . $this->input->post('idform_1') . '" and idkabupaten_kota=' . $this->input->post('idkabupaten_kota'));
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row):
                $idform_1_k6 = $row['idform_1_k6'];
            endforeach;
        }

        $test = $this->input->post('kecamatan_combo') ? $this->input->post('kecamatan_combo') : null; // we also want to check if it's set else we'll get an notice

        if ($test) {
            foreach ($test as $t) {
                $data = array(
                    'idform_1_k6' => $idform_1_k6,
                    'idkecamatan' => $t
                );
                $this->form_1_model->Form1k6_kecamatan_insert($data);
            }
        }
        //echo $kecamatan_combo[0];
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_6'));
        exit;
    }


    public function Form1k6_insert2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'idkecamatan' => $this->input->post('kecamatan_select'),
            'luas_wilayah_kumuh_4' => $this->input->post('luas_wilayah_kumuh_4'),
            'jumlah_rtlh_dalam_wilayah_kumuh_5' => $this->input->post('jumlah_rtlh_dalam_wilayah_kumuh_5'),
            'sumber_data_6' => $this->input->post('sumber_data_6')
        );
        $this->form_1_model->Form1k6_insert($data);

        if ($this->input->post('jenisk6') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_6'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_6'));
        }
        exit;
    }


    public function profil()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $this->load->model('userm_model');
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $provinsi_profil = $this->input->post('provinsi_profil');
        if ($provinsi_profil <= 0) {
            $user_id = $this->session->userdata('iddinas');
            $idprovinsi = $this->session->userdata('idprovinsi');
            $idprovinsiv = $this->session->userdata('idprovinsi');
            $idkabupaten_kota = $this->session->userdata('idkabupaten_kota');
            $data['kecamatan_count'] = $this->profil_model->kecamatan_count($idkabupaten_kota);
            $data['kelurahan_count'] = $this->profil_model->kelurahan_count($idkabupaten_kota);
            $data['kecamatan_view'] = $this->master_model->kecamatan_view($idkabupaten_kota);
        } else {
            $user_id = $provinsi_profil;
            $query = $this->db->query('SELECT iddinas,idprovinsi,provinsi FROM provinsi where idprovinsi=' . $user_id);
            foreach ($query->result_array() as $row):
                $u = $row['iddinas'];
                $up = $row['idprovinsi'];
            endforeach;
            $idprovinsi = $up;
            $idprovinsiv = $u;
        }
        $data['kab_kot_count'] = $this->dinas_model->kab_kot_count($idprovinsi);
        $data['kec_kot_count'] = $this->dinas_model->kec_kot_count($idprovinsi);
        if ($this->session->userdata('idprivilage') == 2) {
            $data['dinas_view2'] = $this->dinas_model->dinas_view2($idprovinsiv);
            $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        } elseif ($this->session->userdata('idprivilage') == 1) {
            $data['dinas_view'] = $this->dinas_model->dinas_view($u);
        } else {
            $data['dinas_view'] = $this->dinas_model->dinas_view2($user_id);
            $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);

        }
        $data['email'] = $this->session->userdata('email');
        $data['name'] = $this->session->userdata('name');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $this->load->model('userm_model');
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($this->session->userdata('iddinas'));
        $data['avatar'] = $this->session->userdata('avatar');
        $data['tagline'] = $this->session->userdata('tagline');
        $data['teamId'] = $this->session->userdata('teamId');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['kabupaten_kota_dinas'] = $this->master_model->kabupaten_kota_dinas($idprovinsi);
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }

        $this->menu_model->menu_top_active_update(3);
        $data['backlog_kepemilikan_dan_penghunian_kab_view'] = $this->master_model->backlog_kepemilikan_dan_penghunian_kab_view($tahun, $idprovinsi);

        $data['kabupaten_kota_p'] = $this->master_model->kabupaten_kota();
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $this->load->view('html/profile', $data);
    }

    public function grafik()
    {
        $user_id = $this->session->userdata('id');
        $idprovinsi = $this->session->userdata('idprovinsi');
        $is_admin = $this->session->userdata('isAdmin');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $this->menu_model->menu_top_active_update(2);
        $rtlh_grafik = $this->form_1_model->rtlh_grafik();
        $this->load->view('html/grafik', $data);
    }

    public function pengguna()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('id');
        $is_admin = $this->session->userdata('isAdmin');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['pengguna'] = $this->userm_model->pengguna();
        $this->load->view('html/pengguna', $data);
    }

    public function pengguna_dinas()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['pengguna_dinas'] = $this->userm_model->pengguna_dinas($user_id);
        $this->load->view('html/pengguna_dinas', $data);
    }

    public function provinsi()
    {
        $user_id = $this->session->userdata('id');
        $is_admin = $this->session->userdata('isAdmin');
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['provinsi'] = $this->master_model->provinsi();
        $this->load->view('html/provinsi', $data);
    }

    public function kab_kota()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('id');
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $is_admin = $this->session->userdata('isAdmin');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['kabupaten_kota'] = $this->master_model->kabupaten_kota();
        $this->load->view('html/kabupaten_kota', $data);
    }

    public function kabupaten_kota()
    {
        $user_id = $this->session->userdata('iddinas');
        $data['user_id'] = $this->session->userdata('iddinas');
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $is_admin = $this->session->userdata('isAdmin');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $idprovinsi = $this->session->userdata('idprovinsi');
        $data['kabupaten_kota_dinas'] = $this->master_model->kabupaten_kota_dinas($idprovinsi);
        $this->load->view('html/kabupaten_kota_dinas', $data);
    }

    public function grafikd()
    {

        $data['tahun'] = $this->uri->segment(4);
        $data['idprovinsi'] = $this->uri->segment(3);
        $this->load->view('html/charts-amcharts_rtlh_kab.js.php', $data);
        //$this->load->view('html/charts-amcharts_prov.js.php',$data);
    }

    public function grafikd_prov()
    {
        $data['iddinas'] = $this->uri->segment(3);

        $data['tahun'] = $this->uri->segment(4);
        $data['idprovinsi'] = $this->uri->segment(5);
        $this->load->view('html/charts-amcharts_prov.js.php', $data);
    }

    public function grafikechart()
    {

        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['tahun'] = $this->uri->segment(3);
        $this->load->view('html/charts-echarts.min.js.php', $data);
    }

    public function grafikechart_prov()
    {

        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $this->load->view('html/charts-echarts_prov.min.js.php', $data);
    }

    public function grafikechart_prov_det()
    {
        $data['idprovinsi'] = $this->uri->segment(3);
        $data['g'] = $this->uri->segment(4);
        $data['tahun'] = $this->uri->segment(5);
        $this->load->view('html/charts-echarts_prov_det.min.js.php', $data);
    }

    public function grafikechartsrmbr()
    {

        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $this->load->view('html/charts-echarts_sr_mbr.min.js.php', $data);
    }

    public function grafikdpenghunian()
    {
        $data['iddinas'] = $this->uri->segment(3);

        $data['tahun'] = $this->uri->segment(4);
        $this->load->view('html/charts-amchartspenghunian.js.php', $data);
    }

    public function grafikdpenghunian_1a()
    {
        $data['iddinas'] = $this->uri->segment(3);

        $data['tahun'] = $this->uri->segment(4);
        $this->load->view('html/charts-amchartspenghunian_1a.js.php', $data);
    }

    public function grafikdpenghunian_1a_det()
    {
        $data['idprovinsi'] = $this->uri->segment(3);
        $data['tahun'] = $this->uri->segment(4);
        $this->load->view('html/charts-amchartspenghunian_1a_det.js.php', $data);
    }

    public function grafikdpenghunian_1a_prov()
    {
        $data['iddinas'] = $this->uri->segment(3);

        $data['tahun'] = $this->uri->segment(4);
        $this->load->view('html/charts-amchartspenghunian_1a_prov.js.php', $data);
    }

    public function grafik_prov_detail()
    {
        $this->load->view('html/charts-amchartsprovdetail.js.php');
    }

    public function grafikdkepemilikan()
    {
        $data['iddinas'] = $this->uri->segment(3);
        $data['tahun'] = $this->uri->segment(4);
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        if ($this->session->userdata('idprivilage') == 2) {
            $this->load->view('html/charts-amchartskepemilikan_1a.js.php', $data);
        } else {
            $this->load->view('html/charts-amchartskepemilikan.js.php', $data);
        }
    }

    public function grafikdsejutarumah()
    {
        $data['iddinas'] = $this->uri->segment(3);
        $this->load->view('html/charts-amchartssr.js.php', $data);
    }

    public function grafikdsejutarumah_detail()
    {
        $data['iddinas'] = $this->uri->segment(3);
        $this->load->view('html/charts-amchartssr_detail.js.php', $data);
    }

    public function grafikmoris()
    {
        $this->load->view('html/charts-morris.min.js.php');
    }

    public function grafikmoris_kab()
    {
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $this->load->view('html/charts-morris_kab.min.js.php', $data);
    }

    public function grafikdsejutarumah2()
    {
        $data['iddinas'] = $this->uri->segment(3);
        $this->load->view('html/charts-amchartssr2.js.php', $data);
    }

    public function grafikd2()
    {

        $this->load->view('html/charts-amcharts2.js.php');
    }

    public function kabupaten_kota_script()
    {

        $this->load->view('html/table-editablekab.js.php');
    }

    public function update_user_dinas()
    {
        $user_id = $this->session->userdata('id');
        $this->userm_model->update_user_dinas($user_id);
        header('Location:' . BASE_URL('/main/pengguna_dinas'));
        exit;
    }

    public function update_daftar_user()
    {
        $data = array(
            'email_user' => $this->input->post('email_user'),
            'alamat_user' => $this->input->post('alamat_user'),
            'telepon_user' => $this->input->post('telepon_user')
        );
        $this->user_m->update_dinas($data, $this->input->post('iddinas_pengguna'));
        header('Location:' . BASE_URL('/main/daftar_pengguna/' . $this->input->post('provinsi_select2')));
        exit;
        //$this->daftar_pengguna();
    }

    public function reset_pengguna()
    {
        $data = array(
            'isLoggedIn' => "false"
        );
        $this->user_m->update_dinas($data, $this->input->post('iddinas_reset'));
        header('Location:' . BASE_URL('/main/daftar_pengguna/' . $this->input->post('provinsi_select1')));
        exit;
    }

    public function ubah_password_daftar_pengguna()
    {
        $data = array(
            'password' => sha1($this->input->post('password_baru_user'))
        );
        $this->user_m->update_dinas($data, $this->input->post('iddinasx'));
        header('Location:' . BASE_URL('/main/daftar_pengguna/' . $this->input->post('provinsi_select3')));
        exit;
    }

    public function dinas_view()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $p = 0;
        if (is_null($this->input->post('kabupaten_profile_filter')) || $this->input->post('kabupaten_profile_filter') == "") {
            if (is_null($this->input->post('provinsi_filter2'))) {
                $u = $this->session->userdata('iddinas');
            } else {
                $u = $this->input->post('provinsi_filter2');
                $query = $this->db->query('SELECT iddinas,idprovinsi,provinsi FROM provinsi where idprovinsi=' . $u);
                foreach ($query->result_array() as $row):
                    $u = $row['iddinas'];
                endforeach;
                $p = 1;
            }
        } else {
            $u = $this->input->post('kabupaten_profile_filter');
            $query = $this->db->query('SELECT iddinas,idkabupaten_kota,kabupaten_kota FROM kabupaten_kota where idkabupaten_kota=' . $u);
            foreach ($query->result_array() as $row):
                $u = $row['iddinas'];
            endforeach;
            $p = 2;
        }
        $is_admin = $this->session->userdata('isAdmin');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['iddinas'] = $u;
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        if ($this->uri->segment(3) == "0") {
            $data['ubah_pass'] = 0;
        } else {
            $data['ubah_pass'] = 1;
        }
        $data['name'] = $this->session->userdata('name');
        if ($this->session->userdata('idprivilage') == 2) {
            $data['dinas_view'] = $this->dinas_model->dinas_view($u);
            $data['dinas_view2'] = $this->dinas_model->dinas_view2($u);
        } elseif ($this->session->userdata('idprivilage') == 3) {
            $data['dinas_view'] = $this->dinas_model->dinas_view2($u);
            $data['dinas_view2'] = $this->dinas_model->dinas_view2($u);
        } else {
            if ($p == 2) {
                $data['dinas_view'] = $this->dinas_model->dinas_view2($u);
            } elseif ($p == 0) {
                $data['dinas_view'] = $this->dinas_model->dinas_view2($u);
            } elseif ($p == 1) {
                $data['dinas_view'] = $this->dinas_model->dinas_view($u);
            }
            $data['dinas_view2'] = $this->dinas_model->dinas_view($u);
            $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($this->session->userdata('iddinas'));
        }
        //echo $this->session->userdata('idprivilage');
        $this->load->view('html/dinas_view', $data);
    }

    public function form_imb_insert()
    {
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');

        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'jenis' => "IMB",
            'bulan' => "0"

        );
        /////k1
        $query = $this->db->query('SELECT * FROM form_satu_juta_rumah where bulan="0" and iddinas=' . $iddinas);
        if ($query->num_rows() == 0) {
            $data['form_satu_juta_rumah_insert'] = $this->sejuta_rumah->form_satu_juta_rumah_insert($form_1_data);
        }

        header('Location:' . BASE_URL('/main/form_imb'));
        exit;
    }

    public function fpengembang_imb_insert()
    {
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');

        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'bulan' => "0",
            'status' => "Draft"

        );
        /////k1
        $query = $this->db->query('SELECT * FROM pengembang where bulan="0" and iddinas=' . $iddinas);
        if ($query->num_rows() < 1) {
            $this->sejuta_rumah->form_pengembang_insert($form_1_data);
        }

        header('Location: ' . BASE_URL('/main/form_pen_insert'));
        exit;
    }

    public function fperbankan_imb_insert()
    {
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');

        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'bulan' => "0"

        );
        /////k1
        $query = $this->db->query('SELECT * FROM perbankan where bulan="0" and iddinas=' . $iddinas);
        if ($query->num_rows() <= 1) {
            $this->sejuta_rumah->form_perbankan_insert($form_1_data);
        }

        header('Location: ' . BASE_URL('/main/form_per_insert'));
        exit;
    }

    public function form_non_imb_insert()
    {
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'jenis' => "Non IMB",
            'bulan' => "0"

        );
        /////k1
        $data['form_satu_juta_rumah_insert'] = $this->sejuta_rumah->form_satu_juta_rumah_insert($form_1_data);


        header('Location: ' . BASE_URL('/main/form_non_imb'));
        exit;
    }

    public function form_non_imb()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idform_satu_juta_rumah')) == 0) {
            $idform_satu_juta_rumah = 0;
        } else {
            $idform_satu_juta_rumah = $this->input->post('idform_satu_juta_rumah');
        }
        //echo $idform_satu_juta_rumah;
        $iddinas = $this->session->userdata('iddinas');
        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');

        $data['alamat'] = $this->session->userdata('alamat');

        $data['kabupaten_kota'] = $this->form_1_model->kabupaten_kota($data['idprovinsi']);
        $data['idkabupaten_kota'] = $this->session->userdata('idkabupaten_kota');
        //echo $data['idkabupaten_kota'];
        $data['kecamatan'] = $this->form_1_model->kecamatan($data['idkabupaten_kota']);
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas')
        );
        $data['form_non_imb_view_detail'] = $this->sejuta_rumah->form_non_imb_view_detail($form_1_data, $idform_satu_juta_rumah);
        $data['form_satu_juta_rumah_non_imb_view_detail'] = $this->sejuta_rumah->form_satu_juta_rumah_non_imb_view_detail($form_1_data, $idform_satu_juta_rumah);
        $data['form_satu_juta_rumah_non_unit_view_detail'] = $this->sejuta_rumah->form_satu_juta_rumah_non_unit_view_detail($form_1_data, $idform_satu_juta_rumah);
        $data['form_satu_juta_rumah_non_imb_count'] = $this->sejuta_rumah->form_satu_juta_rumah_non_imb_count($form_1_data, $idform_satu_juta_rumah);
        $data['form_satu_juta_rumah_non_unit_count'] = $this->sejuta_rumah->form_satu_juta_rumah_non_unit_count($form_1_data, $idform_satu_juta_rumah);


        $data['error'] = ' ';
        $this->load->view('html/form_non_imb', $data);
    }

    public function form_pengembang_view()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['form_imb_view'] = $this->sejuta_rumah->form_imb_view2($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
        $data['kabupaten_kota_p'] = $this->master_model->kabupaten_kota();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['kabupaten_kota_p'] = $this->master_model->kabupaten_kota();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }
        $this->load->view('html/form_pengembang_view', $data);
    }


    public function form_perbankan_view()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $user_id = $this->session->userdata('iddinas');
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['iddinas'] = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $is_admin = $this->session->userdata('isAdmin');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['name'] = $this->session->userdata('name');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['form_imb_view'] = $this->sejuta_rumah->form_imb_view3($user_id);
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);

        $data['kabupaten_kota_p'] = $this->master_model->kabupaten_kota();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $this->load->view('html/form_perbankan_view', $data);
    }

    public function form_pen_insert()
    {

        if (strlen($this->input->post('idpengembang')) == 0) {
            $idpengembang = 0;

        } else {
            $idpengembang = $this->input->post('idpengembang');

        }
        $iddinas = $this->session->userdata('iddinas');
        $data['user_id'] = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');

        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $idprovinsi = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $idkabupaten_kota = $this->session->userdata('idkabupaten_kota');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas')

        );
        $data['kabupaten_kota'] = $this->form_1_model->kabupaten_kota($idprovinsi);
        $data['form_pengembang_view_detail'] = $this->sejuta_rumah->form_pengembang_view_detail($form_1_data, $idpengembang);
        $data['form_pengembang_view'] = $this->sejuta_rumah->pengembang_view($form_1_data, $idpengembang);
        $data['pengembang_view_detail'] = $this->sejuta_rumah->pengembang_view_detail($form_1_data, $idpengembang);
        $data['pengembang_view_detail2'] = $this->sejuta_rumah->pengembang_view_detail2($form_1_data, $idpengembang);
        $data['form_satu_juta_rumah_unit_view_detail'] = $this->sejuta_rumah->form_satu_juta_rumah_unit_view_detail($form_1_data, $idpengembang);
        $data['form_satu_juta_rumah_imb_count'] = $this->sejuta_rumah->form_satu_juta_rumah_imb_count($form_1_data, $idpengembang);
        $data['form_satu_juta_rumah_unit_count'] = $this->sejuta_rumah->form_satu_juta_rumah_unit_count($form_1_data, $idpengembang);
        //$data['kecamatan_combo'] = $this->sejuta_rumah->kecamatan_combo($form_1_data,$idpengembang);
        $data['error'] = ' ';
        $this->load->view('html/form_pengembang', $data);
    }


    public function form_per_insert()
    {

        if (strlen($this->input->post('idperbankan')) == 0) {
            $idperbankan = 0;

        } else {
            $idperbankan = $this->input->post('idperbankan');

        }
        $iddinas = $this->session->userdata('iddinas');
        $data['iddinas'] = $this->session->userdata('iddinas');

        $data['dinas_view'] = $this->dinas_model->dinas_view($iddinas);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($iddinas);
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['idprovinsi'] = $this->session->userdata('idprovinsi');
        $idprovinsi = $this->session->userdata('idprovinsi');
        $data['provinsi'] = $this->session->userdata('provinsi');
        $data['privilage'] = $this->session->userdata('idprivilage');
        $data['alamat'] = $this->session->userdata('alamat');
        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['kecamatan'] = $this->session->userdata('kecamatan');
        $idkabupaten_kota = $this->session->userdata('idkabupaten_kota');
        $data['kelurahan'] = $this->session->userdata('kelurahan');
        $is_admin = $this->session->userdata('isAdmin');
        $privilage = $this->session->userdata('idprivilage');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['form_1_progres'] = $this->form_1_model->form_1_progres($iddinas);
        $data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($iddinas);
        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas')

        );
        $data['kabupaten_kota'] = $this->form_1_model->kabupaten_kota($idprovinsi);
        $data['form_perbankan_view_detail'] = $this->sejuta_rumah->form_perbankan_view_detail($form_1_data, $idperbankan);
        $data['form_perbankan_view'] = $this->sejuta_rumah->form_perbankan_view($form_1_data, $idperbankan);
        $data['perbankan_view_detail'] = $this->sejuta_rumah->perbankan_view_detail($form_1_data, $idperbankan);
        $data['form_satu_juta_rumah_unit_view_detail'] = $this->sejuta_rumah->form_satu_juta_rumah_unit_view_detail($form_1_data, $idperbankan);
        $data['form_satu_juta_rumah_imb_count'] = $this->sejuta_rumah->form_satu_juta_rumah_imb_count($form_1_data, $idperbankan);
        $data['form_satu_juta_rumah_unit_count'] = $this->sejuta_rumah->form_satu_juta_rumah_unit_count($form_1_data, $idperbankan);
        //$data['kecamatan_combo'] = $this->sejuta_rumah->kecamatan_combo($form_1_data,$idpengembang);
        $data['error'] = ' ';
        $this->load->view('html/form_perbankan', $data);
    }

    public function hapus_form_1()
    {
        $data['hapus_form_1'] = $this->form_1_model->hapus_form_1($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a_view'));
        exit;
    }

    public function hapus_kabupaten_kota()
    {
        $data['hapus_form_1'] = $this->master_model->hapus_kabupaten_kota($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/kabupaten_kota'));
        exit;
    }

    public function hapus_form_12()
    {
        $data['hapus_form_1'] = $this->form_1_model->hapus_form_1($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b_view'));
        exit;
    }

    public function hapus_form_1_k2_a()
    {
        $data['hapus_form_1_k2_a'] = $this->form_1_model->hapus_form_1_k2_a($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_2'));
        exit;
    }


    public function hapus_kecamatan()
    {
        $this->form_1_model->hapus_kecamatan($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/kecamatan'));
        exit;
    }

    public function hapus_form_1_k2_a2()
    {
        $data['hapus_form_1_k2_a'] = $this->form_1_model->hapus_form_1_k2_a($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->uri->segment(4) . '#tab_k_2'));
        exit;
    }

    public function hapus_form_1_k2_a2_1()
    {
        $data['hapus_form_1_k2_a'] = $this->form_1_model->hapus_form_1_k2_a($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_2'));
        exit;
    }

    public function hapus_form_1_k2_b()
    {
        $data['hapus_form_1_k2_b'] = $this->form_1_model->hapus_form_1_k2_b($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_2'));
        exit;
    }

    public function hapus_form_1_k2_b2()
    {
        $data['hapus_form_1_k2_b'] = $this->form_1_model->hapus_form_1_k2_b($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->uri->segment(4) . '#tab_k_2'));
        exit;
    }

    public function hapus_form_1_k2_b2_1()
    {
        $data['hapus_form_1_k2_b'] = $this->form_1_model->hapus_form_1_k2_b($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_2'));
        exit;
    }

    public function hapus_form_1_k4_4()
    {
        $data['hapus_form_1_k4_4'] = $this->form_1_model->hapus_form_1_k4_4($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_4'));
        exit;
    }

    public function hapus_form_1_k4_42()
    {
        $data['hapus_form_1_k4_4'] = $this->form_1_model->hapus_form_1_k4_4($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->uri->segment(4) . '#tab_k_4'));
        exit;
    }

    public function hapus_form_1_k4_42_1a()
    {
        $data['hapus_form_1_k4_4'] = $this->form_1_model->hapus_form_1_k4_4($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_4'));
        exit;
    }

    public function hapus_form_1_k4_42_1b()
    {
        $data['hapus_form_1_k4_4'] = $this->form_1_model->hapus_form_1_k4_4($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->uri->segment(4) . '#tab_k_4'));
        exit;
    }

    public function hapus_form_1_k4_5()
    {
        $data['hapus_form_1_k4_5'] = $this->form_1_model->hapus_form_1_k4_5($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_4'));
        exit;
    }

    public function hapus_form_1_k4_52()
    {
        $data['hapus_form_1_k4_5'] = $this->form_1_model->hapus_form_1_k4_5($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->uri->segment(4) . '#tab_k_4'));
        exit;
    }

    public function hapus_form_1_k4_52_1a()
    {
        $data['hapus_form_1_k4_5'] = $this->form_1_model->hapus_form_1_k4_5($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_4'));
        exit;
    }

    public function hapus_form_1_k5()
    {
        $data['hapus_form_1_k5'] = $this->form_1_model->hapus_form_1_k5($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_5'));
        exit;
    }

    public function hapus_form_1_k52()
    {
        $data['hapus_form_1_k5'] = $this->form_1_model->hapus_form_1_k5($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->uri->segment(4) . '#tab_k_5'));
        exit;
    }

    public function hapus_form_1_k52_1a()
    {
        $data['hapus_form_1_k5'] = $this->form_1_model->hapus_form_1_k5($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_5'));
        exit;
    }

    public function hapus_form_1_k52_1b()
    {
        $data['hapus_form_1_k5'] = $this->form_1_model->hapus_form_1_k5($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->uri->segment(4) . '#tab_k_5'));
        exit;
    }

    public function hapus_form_1_k6()
    {
        $data['hapus_form_1_k6'] = $this->form_1_model->hapus_form_1_k6($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_6'));
        exit;
    }

    public function hapus_form_1_k62()
    {
        $data['hapus_form_1_k6'] = $this->form_1_model->hapus_form_1_k6($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->uri->segment(4) . '#tab_k_6'));
        exit;
    }

    public function hapus_form_1_k62_1()
    {
        $data['hapus_form_1_k6'] = $this->form_1_model->hapus_form_1_k6($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->uri->segment(4) . '#tab_k_6'));
        exit;
    }

    public function hapus_form_1_k62_1b()
    {
        $data['hapus_form_1_k6'] = $this->form_1_model->hapus_form_1_k6($this->uri->segment(3));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->uri->segment(4) . '#tab_k_6'));
        exit;
    }

    public function Form1_update_tahun()
    {
        $data = array(
            'tahun' => $this->input->post('tahun_form')
        );

        $data['form_1_view_detail'] = $this->form_1_model->Form1_update($data, $this->input->post('idform_1'));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas')));
        exit;
    }

    public function Form1_update_tahun2()
    {
        $data = array(
            'tahun' => $this->input->post('tahun_form')
        );
        $data['form_1_view_detail'] = $this->form_1_model->Form1_update($data, $this->input->post('idform_1'));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas')));
        exit;
    }

    public function Form1_update_tahun3()
    {
        $data = array(
            'tahun' => $this->input->post('tahun_form')
        );
        $data['form_1_view_detail'] = $this->form_1_model->Form1_update($data, $this->input->post('idform_1'));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas')));
        exit;
    }

    public function Form1_update_status2()
    {
        $data = array(
            'status' => 'Disetujui'
        );
        $data['form_1_view_detail'] = $this->form_1_model->Form1_update($data, $this->input->post('idform_1'));
        header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas')));
        exit;
    }

    public function Form1k2_a_update()
    {
        $data = array(
            'uraian1' => $this->input->post('uraian1'),
            'k221' => $this->input->post('k221'),
            'k231' => $this->input->post('k231')
        );
        $data['Form1k2_a_update'] = $this->form_1_model->Form1k2_a_update($data, $this->input->post('idform_1_k2_a'));
        header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_2'));
        exit;
    }

    public function Form1k2_a_update2()
    {
        $data = array(
            'uraian1' => $this->input->post('uraian1'),
            'k221' => $this->input->post('k221'),
            'k231' => $this->input->post('k231')
        );
        $data['Form1k2_a_update'] = $this->form_1_model->Form1k2_a_update($data, $this->input->post('idform_1_k2_a'));

        if ($this->input->post('jenisk2') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '/' . $this->input->post('idform_1') . '#tab_k_2'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '/' . $this->input->post('idform_1') . '#tab_k_2'));
        }
        exit;
    }

    public function Form1k2_b_update()
    {
        $data = array(
            'jenis_kegiatan_urusan_pkp_2' => $this->input->post('jenis_kegiatan_urusan_pkp_2'),
            'ta_a_vol_unit_3' => $this->input->post('ta_a_biaya_4'),
            'ta_a_vol_unit_5' => $this->input->post('ta_a_vol_unit_5'),
            'ta_a_biaya_6' => $this->input->post('ta_a_biaya_6')
        );
        $data['Form1k2_b_update'] = $this->form_1_model->Form1k2_b_update($data, $this->input->post('idform_1_k2_b'));
        if ($this->input->post('jenisk2b') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_2'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_2'));
        }
        exit;
    }

    public function Form1k2_b_update2()
    {
        $data = array(
            'jenis_kegiatan_urusan_pkp_2' => $this->input->post('jenis_kegiatan_urusan_pkp_2'),
            'ta_a_vol_unit_3' => $this->input->post('ta_a_vol_unit_3'),
            'ta_a_vol_unit_5' => $this->input->post('ta_a_vol_unit_5'),
            'ta_a_biaya_6' => $this->input->post('ta_a_biaya_6')
        );
        $data['Form1k2_b_update'] = $this->form_1_model->Form1k2_b_update($data, $this->input->post('idform_1_k2_b'));
        if ($this->input->post('jenisk2b') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_2'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_2'));
        }
        exit;
    }


    public function Form1k3_update()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'isi_1a' => $this->input->post('isi_1a'),
            'isi_1b' => $this->input->post('isi_1b'),
            'isi_1c' => $this->input->post('isi_1c'),
            'isi_1d' => $this->input->post('isi_1d'),
            'isi_1e' => $this->input->post('isi_1e'),
            'isi_1f' => $this->input->post('isi_1f'),
            'isi_1f_keterangan' => $this->input->post('isi_1f_keterangan'),
            'isi_2c_keterangan' => $this->input->post('isi_2c_keterangan'),
            'isi_2d_keterangan' => $this->input->post('isi_2d_keterangan'),
            'isi_2a' => $this->input->post('isi_2a'),
            'isi_2b' => $this->input->post('isi_2b'),
            'isi_2c' => $this->input->post('isi_2c'),
            'isi_2d' => $this->input->post('isi_2d'),
            'isi_2e' => $this->input->post('isi_2e')
        );
        $this->form_1_model->Form1k3_update($data, $this->input->post('idform_1_k3'));
        if ($this->input->post('jenisk3') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a' . $this->input->post('iddinas') . '#tab_k_3'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b' . $this->input->post('iddinas') . '#tab_k_3'));
        }
        exit;
    }

    public function Form1k3_update2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'isi_1a' => $this->input->post('isi_1a'),
            'isi_1b' => $this->input->post('isi_1b'),
            'isi_1c' => $this->input->post('isi_1c'),
            'isi_1d' => $this->input->post('isi_1d'),
            'isi_1e' => $this->input->post('isi_1e'),
            'isi_1f' => $this->input->post('isi_1f'),
            'isi_1f_keterangan' => $this->input->post('isi_1f_keterangan'),
            'isi_2c_keterangan' => $this->input->post('isi_2c_keterangan'),
            'isi_2d_keterangan' => $this->input->post('isi_2d_keterangan'),
            'isi_2a' => $this->input->post('isi_2a'),
            'isi_2b' => $this->input->post('isi_2b'),
            'isi_2c' => $this->input->post('isi_2c'),
            'isi_2d' => $this->input->post('isi_2d'),
            'isi_2e' => $this->input->post('isi_2e')
        );
        $this->form_1_model->Form1k3_update($data, $this->input->post('idform_1_k3'));
        if ($this->input->post('jenisk3') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_3'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_3'));
        }
        exit;
    }


    public function Form1k4_1_update()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'k4141' => $this->input->post('k4141'),
            'k4142' => $this->input->post('k4142'),
            'k4143' => $this->input->post('k4143'),
            'k4144' => $this->input->post('k4144'),
            'k4145' => $this->input->post('k4145'),
            'k4151' => $this->input->post('k4151'),
            'k4152' => $this->input->post('k4152'),
            'k4153' => $this->input->post('k4153'),
            'k4154' => $this->input->post('k4154'),
            'k4155' => $this->input->post('k4155'),
            'status1' => $this->input->post('status1'),
            'status2' => $this->input->post('status2'),
            'status3' => $this->input->post('status3'),
            'status4' => $this->input->post('status4'),
            'status5' => $this->input->post('status5')
        );
        $this->form_1_model->Form1k4_1_update($data, $this->input->post('idform_1_k4_1'));
        if ($this->input->post('jenisk41') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_1_update2()
    {
        $data = array(
            'idform_1' => $this->input->post('idform_1'),
            'k4141' => $this->input->post('k4141'),
            'k4142' => $this->input->post('k4142'),
            'k4143' => $this->input->post('k4143'),
            'k4144' => $this->input->post('k4144'),
            'k4145' => $this->input->post('k4145'),
            'k4151' => $this->input->post('k4151'),
            'k4152' => $this->input->post('k4152'),
            'k4153' => $this->input->post('k4153'),
            'k4154' => $this->input->post('k4154'),
            'k4155' => $this->input->post('k4155'),
            'status1' => $this->input->post('status1'),
            'status2' => $this->input->post('status2'),
            'status3' => $this->input->post('status3'),
            'status4' => $this->input->post('status4'),
            'status5' => $this->input->post('status5')
        );
        $this->form_1_model->Form1k4_1_update($data, $this->input->post('idform_1_k4_1'));
        if ($this->input->post('jenisk41') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_2_update()
    {
        $data = array(
            'k4231' => $this->input->post('k4231'),
            'k4232' => $this->input->post('k4232'),
            'k4241' => $this->input->post('k4241'),
            'k4242' => $this->input->post('k4242'),
            'jenis1' => $this->input->post('jenis1'),
            'jenis2' => $this->input->post('jenis2')
        );
        $this->form_1_model->Form1k4_2_update($data, $this->input->post('idform_1_k4_2'));
        if ($this->input->post('jenisk42') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function fpengembang_mbr_update()
    {
        $data = array(
            //'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'nama_perusahaan' => $this->input->post('nama_perusahaan'),
            'alamat' => $this->input->post('alamat'),
            'nama_pengembang' => $this->input->post('nama_pengembang'),
            'bentuk_rumah' => $this->input->post('bentuk_rumah'),
            'luas' => $this->input->post('luas'),
            'tipe' => $this->input->post('tipe'),
            'rencana' => $this->input->post('rencana'),
            'realisasi' => $this->input->post('realisasi'),
            'harga' => $this->input->post('harga')
        );
        $this->form_1_model->pengembang_imb_update($data, $this->input->post('idpengembang_mbr'));
        header('Location: ' . BASE_URL('/main/form_pen_insert'));
        exit;
    }

    public function Form1k4_2_update2()
    {
        $data = array(
            'k4231' => $this->input->post('k4231'),
            'k4232' => $this->input->post('k4232'),
            'k4241' => $this->input->post('k4241'),
            'k4242' => $this->input->post('k4242'),
            'jenis1' => $this->input->post('jenis1'),
            'jenis2' => $this->input->post('jenis2')
        );
        $this->form_1_model->Form1k4_2_update($data, $this->input->post('idform_1_k4_2'));
        if ($this->input->post('jenisk42') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_3_update()
    {
        $data = array(
            'k4331' => $this->input->post('k4331'),
            'k4332' => $this->input->post('k4332'),
            'k4341' => $this->input->post('k4341'),
            'k4342' => $this->input->post('k4342'),
            'fungsi1' => $this->input->post('fungsi1'),
            'fungsi2' => $this->input->post('fungsi2')
        );
        $this->form_1_model->Form1k4_3_update($data, $this->input->post('idform_1_k4_3'));
        if ($this->input->post('jenisk43') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_3_update2()
    {
        $data = array(
            'k4331' => $this->input->post('k4331'),
            'k4332' => $this->input->post('k4332'),
            'k4341' => $this->input->post('k4341'),
            'k4342' => $this->input->post('k4342'),
            'fungsi1' => $this->input->post('fungsi1'),
            'fungsi2' => $this->input->post('fungsi2')
        );
        $this->form_1_model->Form1k4_3_update($data, $this->input->post('idform_1_k4_3'));
        if ($this->input->post('jenisk43') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_4_update()
    {
        $data = array(
            //	'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            //	'idkecamatan' => $this->input->post('idkecamatan_select'),
            'jumlah_rumah_4' => $this->input->post('jumlah_rumah_4'),
            'sumber_data_5' => $this->input->post('sumber_data_5')
        );
        $this->form_1_model->form1k4_4_update($data, $this->input->post('idform_1_k4_4'));
        if ($this->input->post('jenisk44') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_4_update2()
    {
        $data = array(
            //	'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            'idkecamatan' => $this->input->post('idkecamatan_select'),
            'jumlah_rumah_4' => $this->input->post('jumlah_rumah_4'),
            'sumber_data_5' => $this->input->post('sumber_data_5')
        );
        $this->form_1_model->Form1k4_4_update($data, $this->input->post('idform_1_k4_4'));
        if ($this->input->post('jenisk44') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_5_update()
    {
        $data = array(
            'non_mbr_2' => $this->input->post('non_mbr_2'),
            'mbr_3' => $this->input->post('mbr_3'),
            'non_mbr_4' => $this->input->post('non_mbr_4'),
            'mbr_5' => $this->input->post('mbr_5'),
            'non_mbr_6' => $this->input->post('non_mbr_6'),
            'mbr_7' => $this->input->post('mbr_7'),
            'sumber_data_8' => $this->input->post('sumber_data_8')
        );
        $this->form_1_model->Form1k4_5_update($data, $this->input->post('idform_1_k4_5'));
        if ($this->input->post('jenisk45') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }

    public function Form1k4_5_update2()
    {
        $data = array(
            'non_mbr_2' => $this->input->post('non_mbr_2'),
            'mbr_3' => $this->input->post('mbr_3'),
            'non_mbr_4' => $this->input->post('non_mbr_4'),
            'mbr_5' => $this->input->post('mbr_5'),
            'non_mbr_6' => $this->input->post('non_mbr_6'),
            'mbr_7' => $this->input->post('mbr_7'),
            'sumber_data_8' => $this->input->post('sumber_data_8')
        );
        $this->form_1_model->Form1k4_5_update($data, $this->input->post('idform_1_k4_5'));
        if ($this->input->post('jenisk45') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_4'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_4'));
        }
        exit;
    }


    public function Form1k5_update()
    {
        $data = array(
            //	'idkabupaten_kota' => $this->input->post('idkabupaten_kota'),
            //'idkecamatan' => $this->input->post('idkecamatan'),
            'jumlah_kk_rt_3' => $this->input->post('jumlah_kk_rt_3'),
            'jumlah_rtlh_versi_bdt_4' => $this->input->post('jumlah_rtlh_versi_bdt_4'),
            'jumlah_rtlh_verifikasi_pemda_5' => $this->input->post('jumlah_rtlh_verifikasi_pemda_5'),
            'sumber_data_6' => $this->input->post('sumber_data_6')
        );
        $this->form_1_model->Form1k5_update($data, $this->input->post('idform_1_k5'));
        if ($this->input->post('jenisk5') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_5'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_5'));
        }
        exit;
    }

    public function Form1k5_update2()
    {
        $data = array(
            'idkecamatan' => $this->input->post('idkecamatan'),
            'jumlah_kk_rt_3' => $this->input->post('jumlah_kk_rt_3'),
            'jumlah_rtlh_versi_bdt_4' => $this->input->post('jumlah_rtlh_versi_bdt_4'),
            'jumlah_rtlh_verifikasi_pemda_5' => $this->input->post('jumlah_rtlh_verifikasi_pemda_5'),
            'sumber_data_6' => $this->input->post('sumber_data_6')
        );
        $this->form_1_model->Form1k5_update($data, $this->input->post('idform_1_k5'));
        if ($this->input->post('jenisk5') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_5'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_5'));
        }
        exit;
    }

    public function Form1k6_update()
    {
        $data = array(
            'luas_wilayah_kumuh_4' => $this->input->post('luas_wilayah_kumuh_4'),
            'jumlah_rtlh_dalam_wilayah_kumuh_5' => $this->input->post('jumlah_rtlh_dalam_wilayah_kumuh_5'),
            'sumber_data_6' => $this->input->post('sumber_data_6')
        );
        $this->form_1_model->Form1k6_update($data, $this->input->post('idform_1_k6'));

        if ($this->input->post('jenisk6_1') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_6'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_6'));
        }
        exit;
    }

    public function Form1k6_update2()
    {
        $data = array(
            'idkabupaten_kota' => $this->input->post('kabupaten_kota_select'),
            'luas_wilayah_kumuh_4' => $this->input->post('luas_wilayah_kumuh_4'),
            'jumlah_rtlh_dalam_wilayah_kumuh_5' => $this->input->post('jumlah_rtlh_dalam_wilayah_kumuh_5'),
            'sumber_data_6' => $this->input->post('sumber_data_6')
        );
        $this->form_1_model->Form1k6_update($data, $this->input->post('idform_1_k6'));

        if ($this->input->post('jenisk6') == "1A") {
            header('Location: ' . BASE_URL('/main/form_1a/' . $this->input->post('iddinas') . '#tab_k_6'));
        } else {
            header('Location: ' . BASE_URL('/main/form_1b/' . $this->input->post('iddinas') . '#tab_k_6'));
        }
        exit;
    }

    public function dinas_view_update()
    {

        $user_id = $this->session->userdata('iddinas');
        $this->dinas_model->dinas_view_update($user_id);
        $this->dinas_view();
    }

    public function provinsi2()
    {
        $data['idprovinsi'] = $this->uri->segment(3);
        $this->load->view('html/provinsi2', $data);
    }

    public function provinsi3()
    {
        $data['idprovinsi'] = $this->uri->segment(3);
        $this->load->view('html/provinsi3', $data);
    }

    public function provinsi4()
    {
        $data['idprovinsi'] = $this->uri->segment(3);
        $this->load->view('html/provinsi4', $data);
    }

    public function provinsi5()
    {
        $data['idprovinsi'] = $this->uri->segment(3);
        $this->load->view('html/provinsi5', $data);
    }

    public function provinsi6()
    {
        $data['idprovinsi'] = $this->uri->segment(3);
        $this->load->view('html/provinsi6', $data);
    }

    public function provinsi7()
    {
        $data['idprovinsi'] = $this->uri->segment(3);
        $this->load->view('html/provinsi7', $data);
    }

    public function provinsi8()
    {
        $data['idprovinsi'] = $this->uri->segment(3);
        $this->load->view('html/provinsi8', $data);
    }

    public function petaina()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->status_kepemilikan_rumah_map($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->kepemilikan
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    //
    public function petaina22()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->status_kepemilikan_rumah_map($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->kepemilikan
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    ////
    public function petaina22x()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->status_kepemilikan_rumah_map($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->penghunian
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    //
    public function petaina3()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->rtlh_map($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->rtlh
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    public function petaina3_prov()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->rtlh_map_prov($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->rtlh
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    public function petaina4()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->status_kepemilikan_rumah_map($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->non_mbr_4
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    public function petaina4_detail()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->status_kepemilikan_rumah_map_detail($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->non_mbr_4
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    public function petaina4x()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->status_kepemilikan_rumah_map($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->mbr_5
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    public function petaina5()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->status_kepemilikan_rumah_map($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->non_mbr_6
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    public function petaina5x()
    {
        $user_id = $this->session->userdata('id');

        if (is_null($this->uri->segment(3))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->uri->segment(3);
        }
        $provinsi = $this->form_1_model->status_kepemilikan_rumah_map($tahun);
        $data['provinsi'] = $provinsi;

        $response = array();
        $posts = array();
        foreach ($provinsi as $prov) {
            $posts[] = array(
                "ID" => $prov->kode_provinsi,
                "PROVINSI" => $prov->provinsi,
                "JUMLAHPENDUDUK" => $prov->mbr_7
            );
        }
        $response['provinsi'] = $posts;
        echo json_encode($response, TRUE);
    }

    public function petaina2()
    {
        $this->load->view('html/petaina');
        //$this->load->view('html/data');
    }

    public function faq()
    {
        $data['user_id'] = $this->session->userdata('iddinas');
        $data['provinsi_diagram'] = $this->userm_model->provinsi();
        $data['provinsi_form_1a'] = $this->userm_model->provinsi();
        $user_id = $this->session->userdata('iddinas');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['dinas_view'] = $this->dinas_model->dinas_view($user_id);
        $data['dinas_view_all'] = $this->dinas_model->dinas_view_all($user_id);
        $data['dinas_view2'] = $this->dinas_model->dinas_view2($user_id);
        $data['email'] = $this->session->userdata('email');
        $data['name'] = $this->session->userdata('name');
        $data['avatar'] = $this->session->userdata('avatar');
        $data['tagline'] = $this->session->userdata('tagline');
        $data['teamId'] = $this->session->userdata('teamId');
        $data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
        $data['notification'] = $this->notification_model->notification_top();
        $data['inbox'] = $this->message_model->inbox();
        $data['privilage'] = $this->session->userdata('idprivilage');

        if (is_null($this->input->post('tahun'))) {
            $tahun = date('Y');
        } else {
            $tahun = $this->input->post('tahun');
        }
        $pilih_prov_text = 0;
        $data['prov_matrix'] = $this->master_model->provinsi_matrix($tahun);
        if ($this->session->userdata('idprivilage') == 2 || $this->session->userdata('idprivilage') == 3) {
            $data['kab_matrix'] = $this->master_model->kabupaten_kota_matrix($tahun, $this->session->userdata('idprovinsi'));
        } elseif ($this->session->userdata('idprivilage') == 1) {
            if (strlen($this->input->post('pilih_prov_text')) == 0) {
                $pilih_prov_text = 4;
            } else {
                $pilih_prov_text = $this->input->post('pilih_prov_text');
            }
        }

        $this->load->view('html/faq', $data);
        //$this->load->view('html/data');
    }

    //public function testing()
//	{
//	$user_id = $this->session->userdata('iddinas');
//	$data['g1'] = $this->input->post('g1');
//	$data['g2'] = $this->input->post('g2');
//	$data['g3'] = $this->input->post('g3');
//	$data['g4'] = $this->input->post('g4');
//	if ((strlen($this->input->post('g1'))==0) or (strlen($this->input->post('g2'))==0) or (strlen($this->input->post('g3'))==0) or (strlen($this->input->post('g4'))==0))
//	{
//		$data['g1'] ="1";
//	}
//	$data['email'] = $this->session->userdata('email');
//    $data['name'] = $this->session->userdata('name');
//    $data['nama_dinas'] = $this->session->userdata('nama_dinas');
//    $data['avatar'] = $this->session->userdata('avatar');
//    $data['tagline'] = $this->session->userdata('tagline');
//    $data['teamId'] = $this->session->userdata('teamId');
//		$data['menu'] = $this->menu_model->menu_top($this->session->userdata('idprivilage'));
//		$data['notification'] = $this->notification_model->notification_top();
//		$data['inbox'] = $this->message_model->inbox();
//	  $data['privilage']=$this->session->userdata('idprivilage');
//		$data['form_1_progres'] = $this->form_1_model->form_1_progres($user_id);
//		$data['form_1_progres_nr'] = $this->form_1_model->form_1_progres_nr($user_id);
//		if (is_null($this->input->post('tahun')))
//		{
//			$tahun=date('Y');
//		}
//		else
//		{
//			$tahun=$this->input->post('tahun');
//		}
//		$data['status_kepemilikan_rumah2'] = $this->form_1_model->status_kepemilikan_rumah2($tahun);
//		$data['status_kepemilikan_rumah3'] = $this->form_1_model->status_kepemilikan_rumah3($tahun);
//		$this->load->view('html/testing',$data);
//	}


    public function up()
    {
        $this->load->view('html/upload_gambar');
    }

    public function upload()
    {
        $idform_1 = $this->input->post('idform_1');
        $jenis = $this->input->post('jenis');
        $iddinas = $this->input->post('iddinas');
        ///
        if (isset($_FILES['image'])) {
            $i = 0;
            $dir = "assets/uploads/" . $iddinas . '/' . $idform_1 . "/";
            if (is_dir($dir)) {
                if ($handle = opendir($dir)) {
                    while (($file = readdir($handle)) !== false) {
                        if (!in_array($file, array('.', '..')) && !is_dir($dir . $file))
                            $i++;
                    }
                }
            }
            // prints out how many were in the directory
            $config['upload_path'] = './assets/uploads/' . $iddinas . '/' . $idform_1;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
            $config['file_name'] = $idform_1 . $i;
            $this->load->library('upload', $config);
            if (!is_dir('./assets/uploads')) {
                mkdir('./assets/uploads', 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas)) {
                mkdir('./assets/uploads/' . $iddinas, 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas . '/' . $idform_1)) {
                mkdir('./assets/uploads/' . $iddinas . '/' . $idform_1, 0777, true);
            }
            $arr = explode('.', $_FILES["image"]['name']);
            unset($arr[0]);

            $filebaru = "./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . $i . '.' . implode($arr);
            if (file_exists("./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . $i . '.' . implode($arr)) == false) {
                if ($this->upload->do_upload('image')) {
                    $data = array('idform_1' => $idform_1, 'struktur_dinas' => $filebaru);
                    $this->form_1_model->hapus_gambar($idform_1);
                    $this->form_1_model->Form1k1_insert($data);

                    if ($jenis == "1A") {
                        header('Location: ' . BASE_URL('/main/form_1a/' . $iddinas . '/' . $idform_1));
                    } else {
                        header('Location: ' . BASE_URL('/main/form_1b/' . $iddinas . '/' . $idform_1));
                    }
                    exit;
                }
            } else {

                echo "error upload, ./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . $i . '.' . implode($arr);
            }
        }
    }

    public function upload2()
    {
        $idform_1 = $this->input->post('idform_1');
        $iddinas = $this->session->userdata('iddinas');
        ///
        if (isset($_FILES['image'])) {
            $config['upload_path'] = './assets/uploads/' . $iddinas . '/' . $idform_1;
            $config['allowed_types'] = 'gif|jpg|png|bmp';
            $config['file_name'] = $idform_1;
            $this->load->library('upload', $config);
            if (!is_dir('./assets/uploads')) {
                mkdir('./assets/uploads', 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas)) {
                mkdir('./assets/uploads/' . $iddinas, 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas . '/' . $idform_1)) {
                mkdir('./assets/uploads/' . $iddinas . '/' . $idform_1, 0777, true);
            }
            $arr = explode('.', $_FILES["image"]['name']);
            unset($arr[0]);
            $filebaru = "./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . '.' . implode($arr);
            if (file_exists("./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . '.' . implode($arr)) == false) {
                if ($this->upload->do_upload('image')) {
                    $data = array('idform_1' => $idform_1, 'struktur_dinas' => $filebaru);
                    $this->form_1_model->Form1k1_insert($data);
                }
            }
        }
        header('Location: ' . BASE_URL('/main/form_1b'));
        exit;
    }

    public function upload3()
    {
        $idform_1 = $this->input->post('idform');
        $iddinas = $this->session->userdata('iddinas');
        ///
        if (isset($_FILES['image2'])) {
            $config['upload_path'] = './assets/uploads/' . $iddinas . '/' . $idform_1;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
            $config['file_name'] = $idform_1 . "_penyetujuan";
            $this->load->library('upload', $config);
            if (!is_dir('./assets/uploads')) {
                mkdir('./assets/uploads', 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas)) {
                mkdir('./assets/uploads/' . $iddinas, 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas . '/' . $idform_1)) {
                mkdir('./assets/uploads/' . $iddinas . '/' . $idform_1, 0777, true);
            }
            $arr = explode('.', $_FILES["image2"]['name']);
            unset($arr[0]);
            $filebaru = "./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . "_penyetujuan" . '.' . implode($arr);
            if (file_exists("./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . "_penyetujuan" . '.' . implode($arr)) == false) {
                if ($this->upload->do_upload('image2')) {
                    $data = array('status' => 'Disetujui', 'formulir' => $filebaru);
                    $this->form_1_model->Form1_update($data, $idform_1);
                }
            }
        }
        header('Location: ' . BASE_URL('/main/form_1a_view'));
        exit;
    }

    public function upload4()
    {
        $idform_1 = $this->input->post('idform');
        $iddinas = $this->session->userdata('iddinas');
        ///


        if (isset($_FILES['image2'])) {
            $config['upload_path'] = './assets/uploads/' . $iddinas . '/' . $idform_1;
            $config['allowed_types'] = 'gif|jpg|png|bmp';
            $config['file_name'] = $idform_1 . "_penyetujuan";
            $this->load->library('upload', $config);
            if (!is_dir('./assets/uploads')) {
                mkdir('./assets/uploads', 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas)) {
                mkdir('./assets/uploads/' . $iddinas, 0777, true);
            }
            if (!is_dir('./assets/uploads/' . $iddinas . '/' . $idform_1)) {
                mkdir('./assets/uploads/' . $iddinas . '/' . $idform_1, 0777, true);
            }
            $arr = explode('.', $_FILES["image2"]['name']);
            unset($arr[0]);
            $filebaru = "./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . "_penyetujuan" . '.' . implode($arr);
            if (file_exists("./assets/uploads/" . $iddinas . '/' . $idform_1 . "/" . $idform_1 . "_penyetujuan" . '.' . implode($arr)) == false) {
                if ($this->upload->do_upload('image2')) {
                    $data = array('tanggal_pengesahan' => date('Y-m-d H:i:s'), 'status' => 'Disetujui', 'formulir' => $filebaru);
                    $this->form_1_model->Form1_update($data, $idform_1);
                }
            }
        }
        header('Location: ' . BASE_URL('/main/form_1b_view'));
        exit;
    }
}
