<?php

class login extends CI_Controller
{

    function index()
    {
        if ($this->session->userdata('isLoggedIn') == 'true') {
            //  if( $this->session->userdata('isLoggedIn')) {
            //if( $this->session->userdata('isLoggedIn')=="true" ) {
            redirect('/main/index');
        } else {
            $this->show_login(false);
        }
    }


    function login_user()
    {
        $tabulasi = $this->input->post('tabulasi');
        $this->load->model('user_m');
        $this->load->model('form_1_model');
        $provinsi = $this->input->post('provinsi_select');
        $kab = $this->input->post('kab');
        $username = $this->input->post('username');
        $pass = $this->input->post('password2');

        if ($tabulasi == "1") {
            $user = $provinsi;
            $query = $this->db->query('SELECT d.iddinas as iddinas,d.login_count as login_count FROM dinas d join provinsi p on p.iddinas=d.iddinas where p.idprovinsi="' . $user . '" and d.login_count=0');
        } elseif ($tabulasi == "2") {
            $user = $kab;
            $query = $this->db->query('SELECT d.iddinas as iddinas,d.login_count as login_count FROM dinas d join kabupaten_kota kk on kk.iddinas=d.iddinas where kk.idkabupaten_kota="' . $user . '" and d.login_count=0');
        } elseif ($tabulasi == "3") {
            $user = $username;
            $query = $this->db->query('SELECT * from user');
        }
        if ($pass && $this->user_m->validate_user($user, $pass, $tabulasi)) {
            if (($query->num_rows() == 0) or ($tabulasi == "3")) {
                if ($tabulasi == "1") {
                    $query_prov = $this->db->query('SELECT d.iddinas as iddinas,d.login_count as login_count FROM dinas d join provinsi p on p.iddinas=d.iddinas where p.idprovinsi="' . $user . '"');

                    foreach ($query_prov->result_array() as $row):
                        $iddinas = $row['iddinas'];
                        $lc = $row['login_count'];
                    endforeach;
                    $data = array(
                        'login_count' => $lc + 1
                    );
                    $this->user_m->update_dinas($data, $iddinas);
                } elseif ($tabulasi == "2") {
                    $query_prov = $this->db->query('SELECT d.iddinas as iddinas,d.login_count as login_count FROM dinas d join kabupaten_kota kk on kk.iddinas=d.iddinas where kk.idkabupaten_kota="' . $user . '"');

                    foreach ($query_prov->result_array() as $row):
                        $iddinas = $row['iddinas'];
                        $lc = $row['login_count'];
                    endforeach;
                    $data = array(
                        'login_count' => $lc + 1
                    );
                    $this->user_m->update_dinas($data, $iddinas);
                }
                redirect('/main/index/');
            } elseif ($tabulasi == "1" or $tabulasi == "2") {
                foreach ($query->result_array() as $row):
                    $iddinas = $row['iddinas'];
                    $lc = $row['login_count'];
                endforeach;
                $data = array(
                    'login_count' => $lc + 1
                );
                $this->user_m->update_dinas($data, $iddinas);
                redirect('/main/dinas_view/');
            }


        } else {
            $this->show_login(true);
        }
    }

    function add_ajax_kab($id_prov)
    {
        $query = $this->db->order_by('kabupaten_kota', 'ASC')->get_where('kabupaten_kota', array('idprovinsi' => $id_prov));
        $data = "<option value=''>Kabupaten/Kota</option>";
        foreach ($query->result() as $value) {
            $data .= "<option value='" . $value->idkabupaten_kota . "'>" . $value->kabupaten_kota . "</option>";
        }
        echo $data;
    }

    function lupa_password()
    {
        $query = $this->db->query('SELECT * FROM dinas where email="' . $this->input->post('email') . '"');
        if ($query->num_rows() == 0) {
            $data['emailInvalid'] = "true";
            //$this->load->view('html/login.php#forget-password',$data);
            $this->load->view('html/login', $data);
        } else {
            $this->load->model('form_1_model');
            //Load email library
            $this->load->library('email');
            $karakter = 'ABCDEFGHIJKL1234567890';
            $string = '';
            for ($i = 0; $i < 10; $i++) {
                $pos = rand(0, strlen($karakter) - 1);
                $string .= $karakter{$pos};
            }
            $passbaru = $string;
            $data = array(
                'password' => sha1($passbaru)
            );
            $this->form_1_model->cari_email($this->input->post('email'), $data);
            $from_email = "admin@basisdata-perumahan.com";
            $to_email = $this->input->post('email');

            $this->email->from($from_email, 'Administrator');
            $this->email->to($to_email);
            $this->email->subject('Perubahan Password, silahkan mengganti password Anda setelah login kembali');
            $this->email->message('Password baru: ' . $passbaru);

            //Send mail
            if ($this->email->send())
                $this->session->set_flashdata("email_sent", "Email sent successfully.");
            else
                $this->session->set_flashdata("email_sent", "Error in sending Email.");
            $data['emailInvalid'] = "false";
            $this->load->view('html/login', $data);
        }
    }

    function show_login($show_error = false)
    {
        $this->load->model('userm_model');
        $data['provinsi'] = $this->userm_model->provinsi();
        $data['error'] = $show_error;
        $this->load->helper('form');
        $this->load->view('html/login', $data);
    }

    function logout_user()
    {
        $privilage = $this->uri->segment(3);
        $id = $this->uri->segment(4);
        if ($privilage == "1") {
            $query = $this->db->query('SELECT * FROM user');
        } elseif ($privilage == "2") {
            $query = $this->db->query('SELECT * FROM user_provinsi where iddinas="' . $id . '"');
        } elseif ($privilage == "3") {
            $query = $this->db->query('SELECT * FROM user_kabupaten_kota where iddinas="' . $id . '"');
        }
        if ($query->num_rows() != 0) {
            if ($privilage == "1") {
                //$data['isLoggedIn'] = 'false';
                //$this->db->where('id', $id);
                //$this->db->update('user', $data);
            } else {
                $data['isLoggedIn'] = 'false';
                $this->db->where('iddinas', $id);
                $this->db->update('dinas', $data);
            }
            $this->session->sess_destroy();
            $this->index();
        }
    }

    function showphpinfo()
    {
        echo phpinfo();
    }

}

?>