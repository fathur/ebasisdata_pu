<?php

use Model\Eloquent\Menu;
use Philo\Blade\Blade;
use Repositories\AbstractController;

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Formb extends AbstractController
{

    public function __construct()
    {
        parent::__construct();

        $idPrivilege = $this->session->userdata('idprivilage');
        $this->menu = Menu::with(['children'])->top($idPrivilege)->get();

        $this->load->helper('url');

        $this->load->model('form_1_model');

    }

    /**
     *
     */
    public function index()
    {
        $idPrivilege = (int)$this->session->userdata('idprivilage');

        // Redirect
        if ($idPrivilege == 2) {
            redirect(base_url('/main/index'));
        }

        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'Rekapitulasi Formulir IB', 'url' => base_url('main/form_1b_view')],
            ['text' => 'Formulir 1B']
        ];

        $data = [
            'menu' => $this->menu,
            'webHeader' => $this->webHeader,
            'breadcrumbs' => $breadcrumbs,
        ];

        $data['kabupaten_kota'] = $this->session->userdata('kabupaten_kota');
        $data['nama_dinas'] = $this->session->userdata('nama_dinas');
        $data['alamat'] = $this->session->userdata('alamat');

        $data['user_id'] = $this->session->userdata('iddinas');
        if (strlen($this->input->post('idform1')) == 0) {
            $idform1 = 0;
        } else {
            $idform1 = $this->input->post('idform1');
        }

        $form_1_data = array(
            'iddinas' => $this->session->userdata('iddinas'),
            'idprovinsi' => $this->session->userdata('idprovinsi'),
            'idkabupaten_kota' => $this->session->userdata('iddinas')
        );
        $data['form_1_view_detail'] = $this->form_1_model->form_1_view_detail($form_1_data, $idform1)[0];

//        $tahun = is_null($this->input->post('tahun')) ? \Carbon\Carbon::now()->year : $this->input->post('tahun');

        echo $this->blade->view()
            ->make('form1b.index', $data)
            ->render();
    }

    /**
     * For Upload   Image Form B1
     */
    public function upload()
    {
        $idDinas = $this->session->userdata('iddinas');
        $idForm = $this->input->post('idform_1');

        $fileNameExt = explode('.', $_FILES["image"]['name']);
        $ext = end($fileNameExt);

        if (!is_dir('./assets/uploads/' . $idDinas)) {
            mkdir('./assets/uploads/' . $idDinas, 0777, true);
        }

        $this->load->library('upload', [
            'encrypt_name' => true,
            'overwrite' => true,
            'upload_path' => "./assets/uploads/{$idDinas}",
            'allowed_types' => 'gif|jpg|jpeg|png|bmp',
        ]);


        header('Content-Type: application/json');

        if ($this->upload->do_upload('image')) {

            $uploadedData = $this->upload->data();

            $newFile = "assets/uploads/{$idDinas}/{$uploadedData['file_name']}";

            $this->form_1_model->hapus_gambar($idForm);
            $this->form_1_model->Form1k1_insert([
                'idform_1' => $idForm,
                'struktur_dinas' => $newFile
            ]);


            echo json_encode([
                'data'  => $this->upload->data(),
                'image' => base_url($newFile)
            ]);
        } else {
            echo json_encode([
                'message' => $this->upload->display_errors()
            ]);
        }


    }

    public function data()
    {
        header('Content-Type: application/json');

        echo \Yajra\DataTables\DataTables::of(\Model\View\UserProvinsi::query())->make();
    }
}