<?php
/**
 * Created by PhpStorm.
 * User: fathur
 * Date: 20/08/18
 * Time: 22.59
 */


defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Form_A extends \Repositories\AbstractController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
    }

    /**
     *
     */
    public function index()
    {
        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'Rekapitulasi Formulir IA', 'url' => base_url('form-1a')],
        ];

        $distinctYears = \Model\Eloquent\Form::where('iddinas', '=', $this->user->id)
            ->optionYears()->toArray();

        $years = [];
        for ($y = \Carbon\Carbon::now()->year; $y > \Carbon\Carbon::now()->year - 10; $y--) {
            $enable = in_array($y, $distinctYears) ? true : false;
            $years[$y] = $enable;
        }

        $data = [
            'title'       => 'Formulir IA',
            'menu'        => $this->menu,
            'webHeader'   => $this->webHeader,
            'breadcrumbs' => $breadcrumbs,
            'user'        => $this->user,
            'dinas'       => $this->dinas,
            'years'       => $years,
            'mode'        => '1a'
        ];

        if ($this->user->privilege->id == 1) {
            $data['provinces'] = \Model\Eloquent\Region\Provinsi::all();
        }

        echo $this->blade->view()
            ->make('1a.index', $data)
            ->render();
    }

    /**
     *
     */
    public function data()
    {
        header('Content-Type: application/json');

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $columns = $this->input->post("columns");
        $search = $this->input->post("search");

        $orderColNum = (int)$order[0]['column'];
        $orderColDirection = $order[0]['dir'];
        $orderColumn = $columns[$orderColNum]['name'];

        if (in_array($this->user->privilege->id, [2, 3])) {

            $builder = \Model\Eloquent\Form::select('*')
                ->where('form_1.iddinas', '=', $this->dinas->id)
                ->orderBy($orderColumn, $orderColDirection)
                ->offset($start)->take($length);

        } elseif ($this->user->privilege->id == 1) {

            $provinsiId = $this->input->post('provinsi');
            $provinsi = \Model\Eloquent\Region\Provinsi::find($provinsiId);

            if ($provinsi) {
                $builder = \Model\Eloquent\Form::select('*')
                    ->where('form_1.iddinas', '=', $provinsi->dinas->id)
                    ->orderBy($orderColumn, $orderColDirection)
                    ->offset($start)->take($length);
            } else {
                $builder = \Model\Eloquent\Form::select('*')
                    ->whereRaw("1 = 2");
            }

        }


        $count = $builder->count();

        $form = $builder->get();


        $data = [];
        foreach ($form as $item) {

            $status = '';
            if ($item->status == 'Draft') {
                $status = "<span class='label label-md label-warning'> Draft </span>";
            } elseif ($item->status == 'Disetujui') {
                $status = "<span class='label label-md label-success'> Disetujui </span>";
            }

            $lihat = '<a href="' . base_url("form-1a/{$item->id}/cetak") . '" class="btn btn-xs grey-cascade" style="width:60px;">Lihat</a>';


            if ((int)$this->user->privilege->id == 2) {
                $fungsiEdit = '<a href="' . base_url("form-1a/{$item->id}/edit") . '" class="btn btn-xs grey-cascade" style="width:60px;">Ubah</a>';

                $fungsiDelete = '<a onclick="deleteDtRow(this)" class="btn btn-xs red-sunglo" style="width:60px;" data-action="' . base_url("form-1a/{$item->id}/delete") . '" data-table="list-form-1a">Hapus</a>';
            } else {
                $fungsiEdit = '';
                $fungsiDelete = '';
            }

            $fungsi = $fungsiEdit . $fungsiDelete;


            array_push($data, [
                'id'      => $item->id,
                'iddinas' => $item->iddinas,
                'tanggal' => \Carbon\Carbon::parse($item->tanggal_buat)->format("d-m-Y"),
                'tahun'   => $item->tahun,
                'status'  => $status,
                'lihat'   => $lihat,
                'fungsi'  => $fungsi
            ]);
        }

        $output = array(
            "draw"            => $draw,
            "recordsTotal"    => $count,
            "recordsFiltered" => $count,
            "data"            => $data
        );

        echo json_encode($output);
    }

    public function store()
    {
        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        if (is_null($this->user->provinsi)) {
            redirect('/');
        }

        $form = \Model\Eloquent\Form::create([
            'iddinas'      => $this->user->id,
            'idprovinsi'   => $this->user->provinsi->id,
            'tahun'        => $this->input->post('tahun'),
            'tanggal_buat' => \Carbon\Carbon::now()
        ]);

        // Prepare K* form after created
        if ($form) {
            $form->prepareK2a('1a');
            $form->prepareK3();
            $form->prepareK4d('1a');
            $form->prepareK4e();
            $form->prepareK5('1a');
            $form->prepareK6('1a');
        }

        redirect("form-1a/{$form->id}/edit");
    }

    public function edit($idForm)
    {
        $form = \Model\Eloquent\Form::with(['k1'])->find($idForm);

        if (is_null($form)) {
            show_404();
        }

        if ($this->user->privilege->id == 1) {
            $dinas = $form->dinas;
        } elseif (in_array($this->user->privilege->id, [2, 3])) {
            $dinas = $this->dinas;
        }

//        var_dump($form->toArray());
        if ($form->provinsi->id != $dinas->provinsi->id) {
            show_404();
        }

        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'Rekapitulasi Formulir IA', 'url' => base_url('form-1a')],
            ['text' => 'Formulir IA']
        ];

        $data = [
            'title'       => 'Formulir IA',
            'menu'        => $this->menu,
            'webHeader'   => $this->webHeader,
            'breadcrumbs' => $breadcrumbs,
            'mode'        => '1a',
            'editable'    => true,

            'user'  => $this->user,
            'dinas' => $dinas,
            'form'  => $form,

            'ratioK2A'      => is_float($this->ratioK2A($form)) ? number_format($this->ratioK2A($form), 2, ',', '.') : '-',
            'percentageK2A' => is_float($this->ratioK2A($form)) ? number_format($this->ratioK2A($form) * 100, 2, ',', '.') : '-',
        ];

        echo $this->blade->view()
            ->make('form.edit', $data)
            ->render();
    }


    public function cetak($idForm)
    {
        $breadcrumbs = [
            ['text' => 'Beranda', 'url' => base_url('/')],
            ['text' => 'Rekapitulasi Formulir IA', 'url' => base_url('form-1a')],
            ['text' => 'Formulir IA Cetak']
        ];

        $form = \Model\Eloquent\Form::with([
            'k1', 'k2a', 'k2b', 'k3', 'k4a', 'k4b', 'k4c', 'k4d', 'k4e',
            'k5', 'k6'
        ])
            ->find($idForm);

        if ($this->user->privilege->id == 1) {
            $dinas = $form->dinas;
        } elseif (in_array($this->user->privilege->id, [2, 3])) {
            $dinas = $this->dinas;
        }

        $data = [
            'title'       => 'Formulir IA',
            'menu'        => $this->menu,
            'webHeader'   => $this->webHeader,
            'breadcrumbs' => $breadcrumbs,
            'user'        => $this->user,
            'dinas'       => $dinas,
            'form'        => $form,
            'mode'        => '1a',
            'editable'    => false,

            'ratioK2A'      => is_float($this->ratioK2A($form)) ? number_format($this->ratioK2A($form), 2, ',', '.') : '-',
            'percentageK2A' => is_float($this->ratioK2A($form)) ? number_format($this->ratioK2A($form) * 100, 2, ',', '.') : '-',

        ];

        echo $this->blade->view()
            ->make('form.cetak', $data)
            ->render();;
    }

    public function delete($idForm)
    {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (is_null($this->user)) {
            redirect('/'); # Redirect to login
        }

        $form = \Model\Eloquent\Form::find($idForm);

        if ($this->user->provinsi->id != $form->provinsi->id) {
//        if ($form->id != $k->idform_1) {
            show_404();
        }

        $form->delete();

        echo 1;

    }

    private function ratioK2A($form)
    {
        $pkp = $form->k2a()->pkp()->first();
        $apbd = $form->k2a()->apbd()->first();

        if ($pkp and $apbd) {
            if ($apbd->k231 > 0) {
                if ($pkp->k231 > 0 || $apbd->k231 > 0) {
                    return $pkp->k231 / $apbd->k231;
                }
            }
        }

        return '-';
    }

    public function upload_pengesahan($idForm)
    {
        $form = \Model\Eloquent\Form::find($idForm);

        if (!is_dir('./assets/uploads/' . $this->user->id)) {
            mkdir('./assets/uploads/' . $this->user->id, 0777, true);
        }

        $this->load->library('upload', [
            'encrypt_name'  => true,
            'overwrite'     => true,
            'upload_path'   => "./assets/uploads/{$this->user->id}",
            'allowed_types' => 'gif|jpg|jpeg|png|bmp|pdf|doc|docx',
        ]);

        header('Content-Type: application/json');

        if ($this->upload->do_upload('image')) {

            $uploadedData = $this->upload->data();

            $newFile = "assets/uploads/{$this->user->id}/{$uploadedData['file_name']}";

            $form->formulir = $newFile;
            $form->status = 'Disetujui';
            $form->tanggal_pengesahan = \Carbon\Carbon::now();
            $form->save();


            echo json_encode([
                'data'  => $this->upload->data(),
                'image' => base_url($newFile)
            ]);
        } else {
            echo json_encode([
                'message' => $this->upload->display_errors()
            ]);
        }
    }

    public function check_pengesahan($idForm)
    {
        header('Content-Type: application/json');

        $form = \Model\Eloquent\Form::find($idForm);

        $view = $this->blade->view()
            ->make('form.pengesahan', [
                'form' => $form,
            ])
            ->render();

        echo json_encode([
            'body' => $view
        ]);
    }
}