<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Kabupaten extends \Repositories\AbstractController
{
    public function lists($idProvinsi)
    {

        $kabs = \Model\Eloquent\Region\KabupatenKota::where('idprovinsi','=', $idProvinsi)->get();

        header('Content-Type: application/json');
        echo json_encode($kabs->toArray());
    }
}